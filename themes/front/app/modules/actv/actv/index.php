<?php
$base_url                     = "http://" . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . preg_replace('@/+$@', '', dirname($_SERVER['SCRIPT_NAME'])) . '/';
$themeUrl     = $base_url . 'themes/admin/app/inc/';

//require_once('setup_service.php');

//$check_con=$setup_service->check_connection_file_identity();
?>
<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->

<head>
	<title><?php echo $seoTitle; ?></title>

	<!-- start: META -->
	<meta charset="utf-8" />
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- end: META -->
	<!-- start: MAIN CSS -->
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/fonts/style.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/main.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/main-responsive.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/theme_light.css" type="text/css" id="skin_color">
	<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/print.css" type="text/css" media="print" />
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
	<!-- end: MAIN CSS -->
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>
<!-- end: HEAD -->
<!-- start: BODY -->

<body class="login example2">
	<div class="main-login col-sm-4 col-sm-offset-4">
		<div class="logo"><?php echo $seoTitle ?></div>
		<!-- start: LOGIN BOX -->
		<div class="box-login">
			<div class="errorHandler alert alert-danger no-display"></div>
			<div class="alert alert-success no-display"></div>
			<?php
			//untuk menampilkan pesan
			if (trim($message) != '' and ($show_notification_alert['message'] != $message)) {
				echo $this->function_lib->show_message($message, $templateName, $themeId, $status);
			}
			?>
			<form class="form-setup" action="" method="post" onsubmit="return confirm('Lanjutkan?');">
				<fieldset>

					<?php
					if ($show_notification_alert['status'] != 200) {
					?>
						<center>
							<p class="alert alert-danger"><?php echo $show_notification_alert['message'] ?>
								<br />
							</p>
						</center>
					<?php
					} else {
					?>
						<div class="form-actions">
							<center style="margin:10px;">
								<p class="alert alert-info"><?php echo $show_notification_alert['message'] ?>
									<br />
								</p>
								<button onclick="window.location.href='<?php echo base_url() ?>admin/dashboard/index';return false;" class="btn btn-info">Login</button>

							</center>
							<hr />
						</div>
					<?php
					}
					?>
					<div class="form-group">
						<span class="input-icon">
							<input type="text" value="<?php echo $this->input->post('activation_id') ?>" name="activation_id" class="form-control activation_id" placeholder="Nomor Aktivasi" />
							<i class="clip-keyhole"></i> </span>
					</div>

					<div class="form-group">
						<span class="input-icon">
							<input type="text" value="<?php echo $this->input->post('activation_pin') ?>" name="activation_pin" class="form-control" placeholder="Pin" />
							<i class="clip-key-3"></i> </span>
					</div>



					<div class="form-actions">
						<input type="hidden" name="submit" value="1">
						<button id="save_configuration" style="float:right;" type="submit" class="btn btn-success pull-right">
							Aktifkan <i class="fa fa-arrow-circle-right"></i>
						</button>
					</div>

				</fieldset>
			</form>



		</div>

		<!-- end: LOGIN BOX -->

		<!-- start: REGISTER BOX -->


		<!-- end: REGISTER BOX -->
		<!-- start: COPYRIGHT -->
		<div class="copyright">Powered by <a href="http://inolabs.net">INOLABS.NET</a>
		</div>
		<!-- end: COPYRIGHT -->
	</div>
	<!-- start: MAIN JAVASCRIPTS -->
	<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
	<!--[if gte IE 9]><!-->
	<script src="<?php echo $base_url ?>addons/jquery/jquery.min.js"></script>
	<!--<![endif]-->
	<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/plugins/less/less-1.5.0.min.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/js/main.js"></script>
	<!-- end: MAIN JAVASCRIPTS -->
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="<?php echo $themeUrl; ?>assets/js/login.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function() {
			$(".activation_id").focus();
			input_keyboard();


			var form = $('.form-setup');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				rules: {
					activation_id: {
						minlength: 2,
						required: true
					},
					activation_pin: {
						minlength: 2,
						required: true
					},
				},
				submitHandler: function(form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function(event, validator) { //display error alert on form submit
					errorHandler.show();
				}
			});

			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function(error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function(element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function(element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function(label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
				highlight: function(element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').addClass('has-error');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function(element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				}
			});
		});


		function input_keyboard() {
			$('.form-control').bind("keydown", function(e) {
				//alert(e.which);
				if (e.altKey && (e.which === 37)) {
					e.preventDefault(); //Skip default behavior of the enter key
					var n = $(".form-control").length;

					var nextIndex = $('.form-control').index(this) - 1;
					if (nextIndex < n && nextIndex >= 0) {
						$('.form-control')[nextIndex].focus();
					}

				}
				if (e.which == 13 || e.altKey && (e.which === 39)) { //Enter key
					e.preventDefault(); //Skip default behavior of the enter key
					var n = $(".form-control").length;

					var nextIndex = $('.form-control').index(this) + 1;
					if (nextIndex < n) {
						$('.form-control')[nextIndex].focus();
					}

				}
			});


		}
	</script>
</body>
<!-- end: BODY -->

</html>