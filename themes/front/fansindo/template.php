<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$w_lib=new widget_lib;
$uri_1=$this->uri->segment(1);
$uri_2=$this->uri->segment(2);
$uri_3=$this->uri->segment(3);
$src_logo= (isset($websiteConfig['config_logo']) AND file_exists(FCPATH.$websiteConfig['config_logo']) AND trim($websiteConfig['config_logo'])!='')?
                                            base_url().$websiteConfig['config_logo']:$themes_inc.'<?php echo $themes_inc; ?>/images/logofix1.png';
$favicon_logo= (isset($websiteConfig['config_favicon']) AND file_exists(FCPATH.$websiteConfig['config_favicon']) AND trim($websiteConfig['config_favicon'])!='')?
                                            base_url().$websiteConfig['config_favicon']:$themes_inc.'<?php echo $themes_inc; ?>/images/logofix1.png';
$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$base_url .= "://".$_SERVER['HTTP_HOST'];
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta property="og:site_name" content="<?php echo isset($seoTitle)?$websiteConfig['config_name'].' - '.$seoTitle:$websiteConfig['config_name'];?>" />
    <meta property="og:title" content="<?php echo isset($seoTitle)?$websiteConfig['config_name'].' - '.$seoTitle:$websiteConfig['config_name'];?>" />
    <meta property="og:image" content="<?php echo (isset($img_content) AND trim($img_content)!='')?$img_content:$src_logo?>" />
    <meta property="og:description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>" />
    <meta property="og:url" content="<?php echo isset($current_url)?$current_url:base_url()?>" />

    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="650" />
    <meta property="og:image:height" content="366" />

    <meta name="thumbnailUrl" content="<?php echo isset($img_content)?$img_content:$src_logo?>" itemprop="thumbnailUrl" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="<?php echo $websiteConfig['config_name']?>" />
    <meta name="twitter:site:id" content="<?php echo $websiteConfig['config_name']?>" />
    <meta name="twitter:creator" content="<?php echo $websiteConfig['config_name']?>" />
    <meta name="twitter:description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>" />
    <meta name="twitter:image:src" content="<?php echo (isset($img_content) AND trim($img_content)!='')?$img_content:$src_logo?>" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="<?php echo $favicon_logo;?>">

    <!-- Set meta informations -->
    <title>
      <?php if (isset($uri_3)) {
        echo ucwords(str_replace('_',' ',$uri_3));
      } ?>
      <?php echo $websiteConfig['config_name']; ?></title>
    <meta name="description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>">
    <meta name="keywords" content="<?php echo function_lib::remove_html_tag((isset($seoKeywords) AND trim($seoKeywords)!='')?$seoKeywords:$websiteConfig['config_keywords']);?>">


    <!-- core CSS -->
    <link href="<?php echo $themes_inc; ?>/css/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_inc; ?>/css/font-awesome-4.7.0/css/font-awesome.css" type="text/css">
    <link href="<?php echo $themes_inc; ?>/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>/css/main.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>/css/responsive.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>/css/custom.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo $themes_inc; ?>/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $themes_inc; ?>/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $themes_inc; ?>/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $themes_inc; ?>/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $themes_inc; ?>/images/ico/apple-touch-icon-57-precomposed.png">

    <script src="<?php echo $themes_inc; ?>/js/jquery.js"></script>
    <script src="<?php echo $themes_inc; ?>/js/plugins.js"></script>
    <script src="<?php echo $themes_inc; ?>/js/functions.js"></script>
    <!--Start of Zendesk Chat Script-->
    <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?4MDBpQV6rjj5icbud9dl6gt1lUFUyJI6";z.t=+new Date;$.
        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
        </script>

        <script type="text/javascript">
        $(function(){
         $('#modal-terms').modal({backdrop:'static',keyboard:false, show:true});
         console.log('run');
        });

    </script>
    <!--End of Zendesk Chat Script-->

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- Document Title
    ============================================= -->
    <!-- <title>Home</title> -->

</head>

<body class="homepage">

    <style type="text/css">
        .foto{
        border-radius:100%;
        border-top:2px solid #cf2031;
        border-right:2px solid #0f7dc8;
        border-bottom:2px solid #2eb31a;
        border-left:2px solid #eab823;
        width:70px;
        height:70px;
        }
        a.menu:link {color: #4c4d4c; text-decoration: none}
        a.menu:visited {color: #4c4d4c; text-decoration: none}
        a.menu:hover {color:#6db606; text-decoration: none} 
        a.menu {FONT-SIZE: 14px;  FONT-FAMILY:sans-serif; FONT-WEIGHT: regular;}
        .huruf{
            FONT-SIZE: 14px;  
            FONT-FAMILY:sans-serif; 
            FONT-WEIGHT: regular;
            color:#595959;
        }
    </style>

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="top-number">
                          <div>
                                <span style="text-align:left;"><i class="fa fa-phone-square"></i> +6281 8065 00070</span>
                                <span style="float:right;"> <a class="link_login" style="color:white;" href="http://fansindo.com/voffice/login">Login</a> | <a style="color:white;" href="http://fansindo.com/registration" class="link_registration">Registrasi</a> </span>
                          </div>
                        </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <?php 
        $w_lib->run('front', 'master_content', 'widget_user_menu'); 
        ?>
        
    </header><!--/header-->

    <?php
    if($uri_1=='' OR $uri_1=='index.php')
    {   ?>
        <section id="main-slider" class="no-margin">
            <?php
                $w_lib->run('front', 'image_slide', 'widget_slider_home');
            ?>
        </section><!--/#main-slider-->
        <?php 
        $w_lib->run('front', 'master_content', 'widget_event'); 
        ?>
        <section class="bg-tab">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-menu-custom">
                            <ul class="nav nav-tabs ">
                                <li class="active"><a data-toggle="tab" href="#menu1"><b><i class="fa fa-bullhorn"></i> Berita dan Pengumuman</b></a></li>
                                <li class="no-active"><a data-toggle="tab" href="#menu2"><b><i class="fa fa-bar-chart"></i> Info Statistik Member</b></a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="menu1" class="tab-pane fade in active">
                                    <div class="row">
                                        <?php 
                                        $w_lib->run('front', 'master_content', 'widget_pengumuman',3);
                                        $w_lib->run('front', 'master_content', 'widget_berita',3);
                                        ?>
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade ">
                                    <div class="row">
                                        <?php $w_lib->run('front', 'master_content', 'widget_member_data',3); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        $w_lib->run('front', 'master_content', 'widget_news_video',3); 
        $w_lib->run('front', 'testimonial', 'widget_testimony',3); 
        $w_lib->run('front', 'master_content', 'widget_sertifikat'); 
    } else {
        $this->load->view($view);
        echo '</div>';
    }   ?>
    <!-- Footer -->
    <section id="content" class="section-footer">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <?php 
            $w_lib->run('front', 'master_content', 'widget_profil_distributor'); 
            $w_lib->run('front', 'master_content', 'widget_customer_service'); 
            $w_lib->run('front', 'master_content', 'widget_daftar_bank'); 
            $w_lib->run('front', 'master_content', 'widget_contact_us'); 
            ?>
        </div>
    </section><!--/#bottom-->
    <?php 
        $w_lib->run('front', 'master_content', 'widget_footer_text'); 
    ?>

    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <script src="<?php echo $themes_inc; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $themes_inc; ?>/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo $themes_inc; ?>/js/jquery.isotope.min.js"></script>
    <script src="<?php echo $themes_inc; ?>/js/main.js"></script>
    <script src="<?php echo $themes_inc; ?>/js/wow.min.js"></script>
    <!--<script type="text/javascript" src="http://arrow.scrolltotop.com/arrow28.js"></script>-->
    <script type="text/javascript">
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

    });
    
    
    function accepted_terms()
    {
        $('#modal-terms').modal('hide');
        return false;
    }
  
  
    function get_new_member()
    {
        var api_url='http://fansindo.com/web_service/get_new_member?jsoncallback=?';
        var table_element=$("#div_member_baru");
        var tbody_content='';
        $.getJSON(api_url,function(response){
            var status=response['status'];
            var message=response['message'];
            var data=response['data'];

            if(status==200)
            {
                table_element.html("");
                $.each(data,function(index,rowArr){
                    tbody_content='<li><div class="circle-img-2"><img src='+rowArr['img_link']+' alt=""></div><div class="konten-img-2"><h3 class="color-grey">'+rowArr['member_id']+'</h3><h3 class=" color-primary">'+rowArr['username']+'</h3><p><i class="fa fa-map-marker text-danger"></i> <b>Kota</b> -</p></div></li>';
                              
                    table_element.append(tbody_content);          
                });
            }

        }); 
    }

    function get_top_income()
    {
            var api_url='http://fansindo.com/web_service/get_top_income?jsoncallback=?';
            var table_element=$("#div_top_income");
            var tbody_content='';
            $.getJSON(api_url,function(response){
                var status=response['status'];
                var message=response['message'];
                var data=response['data'];

                if(status==200)
                {
                    table_element.html("");
                    $.each(data,function(index,rowArr){
                        tbody_content='<li><div class="circle-img-2"><img src='+rowArr['img_link']+' alt=""></div><div class="konten-img-2"><h3 class="color-grey">'+rowArr['member_id']+'</h3><h3 class=" color-primary">'+rowArr['username']+'</h3><p><i class="fa fa-map-marker text-danger"></i> <b>Kota</b> -</p></div></li>';
                                  
                        table_element.append(tbody_content);          
                    });
                }

            }); 
    }

    function get_top_sponsor()
    {
            var api_url='http://fansindo.com/web_service/get_top_sponsor?jsoncallback=?';
            var table_element=$("#div_top_sponsor");
            var tbody_content='';
            $.getJSON(api_url,function(response){
                var status=response['status'];
                var message=response['message'];
                var data=response['data'];

                if(status==200)
                {
                    table_element.html("");
                    $.each(data,function(index,rowArr){
                        tbody_content='<li><div class="circle-img-2"><img src='+rowArr['img_link']+' alt=""></div><div class="konten-img-2"><h3 class="color-grey">'+rowArr['member_id']+'</h3><h3 class=" color-primary">'+rowArr['username']+'</h3><p><i class="fa fa-map-marker text-danger"></i> <b>Kota</b> -</p></div></li>';         
                        table_element.append(tbody_content);    
                    });
                }

            }); 
    }

    function get_top_ro()
    {
            var api_url='http://fansindo.com/web_service/get_top_ro?jsoncallback=?';
            var table_element=$("#div_top_ro");
            var tbody_content='';
            $.getJSON(api_url,function(response){
                var status=response['status'];
                var message=response['message'];
                var data=response['data'];

                if(status==200)
                {
                    table_element.html("");
                    $.each(data,function(index,rowArr){
                        tbody_content='<li><div class="circle-img-2"><img src='+rowArr['img_link']+' alt=""></div><div class="konten-img-2"><h3 class="color-grey">'+rowArr['member_id']+'</h3><h3 class=" color-primary">'+rowArr['username']+'</h3><p><i class="fa fa-map-marker text-danger"></i> <b>Kota</b> -</p></div></li>';         
                        table_element.append(tbody_content);    
                    });
                }

            }); 
    }


    var api_url='http://fansindo.com/web_service/get_member_info?jsoncallback=?';
    var main_url='fansindo.co.id/susu_2.html';//url saat redirect jika sistem melakukan pengacakan referal 
    var link_registration='fansindo.com/registration';//url registrasi web jaringan
    var link_login='fansindo.com/voffice/login';//url login web jaringan 

    function get_subdomain() {
        var regexParse = new RegExp('[a-z\-0-9]{2,63}\.[a-z\.]{2,5}$');
        var urlParts = regexParse.exec(window.location.hostname);
        var subdomain=window.location.hostname.replace(urlParts[0],'').slice(0, -1);       
        return subdomain.replace("www.", "");
    }

    function process_sudomain()
    {
        var subdomain=get_subdomain();
        $.getJSON(api_url+'&username='+subdomain,function(response){
            var status=response['status'];
            var message=response['message'];
            var data=response['data'];

            if(status==300) //response redirect krn tidak ditemukan subdomain terkait, dan sistem otomatis mencarikan subdomain baru
            {
                //console.log('http://'+data['username']+'.'+main_url);
                window.location.href='http://'+data['username']+'.'+main_url;
                return false;
            }
            else if(status==200)
            {
                //replace link registration
                 document.getElementsByClassName('link_registration')[0].setAttribute("href", 'http://'+data['username']+'.'+link_registration);
                 //replace link login
            document.getElementsByClassName('link_login')[0].setAttribute("href", 'http://'+data['username']+'.'+link_login);
            //replace img_link
             document.getElementsByClassName('img_link')[0].setAttribute("src", data['img_link']);
             var html_img='<center> <a class="link_registration" href="http://'+data['username']+'.fansindo.com/registration">'
                    +'<img style="width:100px;height:100px;" src="'+data['img_link']+'"></a></center>';

             $(".img_link").html(html_img);
             $(".full_name").html(data['full_name']);
             $(".member_id").html(data['member_id']);
             $(".mobile_phone_number").html(data['mobile_phone_number']);


            }

        }); 
    }

    $(function(){
        get_new_member();
        get_top_income();
        get_top_sponsor();
        process_sudomain();
    });
    </script>
    <?php
    $w_lib->run('front', 'master_content', 'widget_flashscreen'); 
    ?>
</body>
</html>
