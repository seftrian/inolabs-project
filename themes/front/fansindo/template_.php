<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$w_lib=new widget_lib;
$uri_1=$this->uri->segment(1);
$uri_2=$this->uri->segment(2);
$uri_3=$this->uri->segment(3);
$src_logo= (isset($websiteConfig['config_logo']) AND file_exists(FCPATH.$websiteConfig['config_logo']) AND trim($websiteConfig['config_logo'])!='')?
                                            base_url().$websiteConfig['config_logo']:$themes_inc.'images/logofix1.png';
$favicon_logo= (isset($websiteConfig['config_favicon']) AND file_exists(FCPATH.$websiteConfig['config_favicon']) AND trim($websiteConfig['config_favicon'])!='')?
                                            base_url().$websiteConfig['config_favicon']:$themes_inc.'images/logofix1.png';

?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta property="og:site_name" content="<?php echo isset($seoTitle)?$websiteConfig['config_name'].' - '.$seoTitle:$websiteConfig['config_name'];?>" />
    <meta property="og:title" content="<?php echo isset($seoTitle)?$websiteConfig['config_name'].' - '.$seoTitle:$websiteConfig['config_name'];?>" />
    <meta property="og:image" content="<?php echo (isset($img_content) AND trim($img_content)!='')?$img_content:$src_logo?>" />
    <meta property="og:description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>" />
    <meta property="og:url" content="<?php echo isset($current_url)?$current_url:base_url()?>" />

	<meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="650" />
    <meta property="og:image:height" content="366" />

    <meta name="thumbnailUrl" content="<?php echo isset($img_content)?$img_content:$src_logo?>" itemprop="thumbnailUrl" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="<?php echo $websiteConfig['config_name']?>" />
    <meta name="twitter:site:id" content="<?php echo $websiteConfig['config_name']?>" />
    <meta name="twitter:creator" content="<?php echo $websiteConfig['config_name']?>" />
    <meta name="twitter:description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>" />
    <meta name="twitter:image:src" content="<?php echo (isset($img_content) AND trim($img_content)!='')?$img_content:$src_logo?>" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="<?php echo $favicon_logo;?>">

    <!-- Set meta informations -->
    <title><?php echo isset($seoTitle) ? $seoTitle:$websiteConfig['config_name']; ?></title>
    <meta name="description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>">
    <meta name="keywords" content="<?php echo function_lib::remove_html_tag((isset($seoKeywords) AND trim($seoKeywords)!='')?$seoKeywords:$websiteConfig['config_keywords']);?>">

    <!-- Stylesheets
	============================================= -->
	<link rel="shortcut icon" href="<?php echo $themes_inc; ?>images/favicon.png" />
	<!--<link href="<?php echo $themes_inc; ?>http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />-->
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/font-awesome-4.7.0/css/font-awesome.css" type="text/css">
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/open-sans/open-sans.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/ubuntu-font-family-0.83/ubuntu.css" type="text/css" />
	<!-- <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/nivo-slider.css" type="text/css" /> -->
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/camera.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/custom.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $themes_inc; ?>css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- Document Title
	============================================= -->
	<title>Home</title>

</head>

<body class="stretched no-transition">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
		<div id="top-bar" class="hidden-xs">

			<div class="container container-custom clearfix">

				<div class="col_half col_last fright nobottommargin">

					<!-- Top Links
					============================================= -->
					<div class="top-links">
						<ul>
							<li>
								<a href="registrasi.html" >Registrasi</a>
							</li>
							<li>
								<a href="login.html"> Login</a>
							</li>
						</ul>
					</div><!-- .top-links end -->

				</div>

			</div>

		</div><!-- #top-bar end -->

		<!-- Header
		============================================= -->
		<header id="header" style="background: url('images/bg-menu.png');background-repeat: no-repeat;background-position: right;">

			<div id="header-wrap">

				<div class="container container-custom clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder white"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.html" class="standard-logo" data-dark-logo="images/logo-alhaya.png"><img src="<?php echo $themes_inc; ?>images/logo-sks.png" alt="sks Logo"></a>
						<a href="index.html" class="retina-logo" data-dark-logo="images/logo-sks.png"><img src="<?php echo $themes_inc; ?>images/logo-sks.png" alt="sks Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">
						<ul>
							<li class="current">
								<a href="index.html"><div>Home</div></a>
							</li>
							<li class="balance-menu">
								<a href="#"><div>Transaksi</div></a>
								<ul style="display: none;">
									<li class="medium-list"><a href="paket-umrah.html" class="sf-with-ul"><div>Paket Umrah</div></a></li>
									<li class="medium-list"><a href="paket-umrah-group.html" class="sf-with-ul"><div>Paket Umrah Group</div></a></li>
									<li class="medium-list"><a href="pemesanan-hotel.html" class="sf-with-ul"><div>Pemesanan Hotel</div></a></li>
									<li class="medium-list"><a href="produk-lain.html" class="sf-with-ul"><div>Produk Lain</div></a></li>
									<li class="medium-list"><a href="visa.html" class="sf-with-ul"><div>Visa</div></a></li>
									<li class="medium-list"><a href="paket-la.html" class="sf-with-ul"><div>Paket LA</div></a></li>
									<li class="medium-list"><a href="paket-katering.html" class="sf-with-ul"><div>Paket Katering</div></a></li>
									<li class="medium-list"><a href="tiket-pesawat.html" class="sf-with-ul"><div>Tiket Pesawat</div></a></li>
								</ul>
							</li>
							<li class="balance-menu">
								<a href="keagenan.html"><div>Keagenan</div></a>
							</li>
							<li class="balance-menu">
								<a href="promo.html"><div>Promo</div></a>
							</li>
							<li class="balance-menu">
								<a href="brosur.html"><div>Brosur</div></a>
							</li>
							<li class="balance-menu">
								<a href="contact.html"><div>Contact</div></a>
							</li>
							<div class="row">
								<div class="col-xs-6 menu-mobile">
									<a href="registrasi.html" class="button button-dark"><div>Register</div></a>
								</div>
								<div class="col-xs-6 menu-mobile">
									<a href="login.html" class="button button-dark"><div>Login</div></a>
								</div>
							</div>
						</ul>
					</nav><!-- #primary-menu end -->
				</div>
			</div>
		</header><!-- #header end -->

		<!--slider-->
		<!-- <section id="slider" class="boxed-slider p-t-0">
			<div class="nivoSlider">
				<img src="images/slider/slider-1.jpg" alt="Slider 1" title="#nivocaption1" />
				<img src="images/slider/slider-2.jpg" alt="Slider 2" title="#nivocaption2" />
				<img src="images/slider/slider-3.jpg" alt="Slider 3" title="#nivocaption3" />
				<img src="images/slider/slider-4.jpg" alt="Slider 4" title="#nivocaption4" />
			</div>
		</section> -->

		<section id="slider" class="slider-parallax clearfix">

			<div class="camera_wrap" id="camera_wrap_1">
				<div data-src="<?php echo $themes_inc; ?>images/slider/1.jpg">
					<!-- <div class="camera_caption fadeFromBottom flex-caption slider-caption-bg" style="left: 0; border-radius: 0; max-width: none;">
						<div class="container">Powerful Layout with Responsive functionality that can be adapted to any screen size.</div>
					</div> -->
				</div>
				<div data-src="<?php echo $themes_inc; ?>images/slider/2.jpg">
					<!-- <div class="camera_caption fadeFromBottom flex-caption slider-caption-bg" style="left: 0; border-radius: 0; max-width: none;">
						<div class="container">Looks beautiful &amp; ultra-sharp on Retina Screen Displays.</div>
					</div> -->
				</div>
				<div data-src="<?php echo $themes_inc; ?>images/slider/3.jpg">
					<!-- <div class="camera_caption fadeFromBottom flex-caption slider-caption-bg" style="left: 0; border-radius: 0; max-width: none;">
						<div class="container">Included 20+ custom designed Slider Pages with Premium Sliders like Layer, Revolution, Swiper &amp; others.</div>
					</div> -->
				</div>
				<div data-src="<?php echo $themes_inc; ?>images/slider/4.jpg">
					<!-- <div class="camera_caption fadeFromBottom flex-caption slider-caption-bg" style="left: 0; border-radius: 0; max-width: none;">
						<div class="container">You have easy control on each &amp; every element that provides endless customization possibilities.</div>
					</div> -->
				</div>
				<div data-src="<?php echo $themes_inc; ?>images/slider/slider-3.jpg">

				</div>
			</div>

		</section>
		<!--/slider-->

		<!-- Content
		============================================= -->
		<section id="content">

			<!-- <div class="content-wrap" style="background:url('images/bg/symphony.png');background-repeat: repeat" >
				<div class="container clearfix">
					<div class="col_full m-b-0">
						<div class="panel panel-search m-t-1">
							<div class="tabs tabs-bb clearfix m-b-0" id="tab-9">
								<ul class="tab-nav clearfix">
									<li><a href="#tabs-13"><i class="fa fa-file-text"></i> Paket Umrah</a></li>
									<li><a href="#tabs-14"><i class="fa fa-file-text-o"></i> Paket LA</a></li>
									<li><a href="#tabs-15"><i class="fa fa-plane"></i> Tiket Pesawat</a></li>
								</ul>
								<div class="tab-container">
									<div class="tab-content clearfix" id="tabs-13">
										<h4 class="m-b-1 m-t-1">Cari Paket Umrah Terbaik Termurah</h4>
										<form action="" class="m-b-0">
											<div class="row">
												<div class="col-md-11">
													<div class="row">
														<div class="col-md-3">
															<div class="form-group">
															    <label for="formGroupExampleInput">Keberangkatan</label>
															    <select name="" id="" class="form-control">
															    	<option value="">Pilih Data</option>
															    	<option value="">Maret 2018</option>
															    	<option value="">April 2018</option>
															    	<option value="">Mei 2018</option>
															    </select>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
															    <label for="formGroupExampleInput">Jenis Paket</label>
															    <select name="" id="" class="form-control">
															    	<option value="">Pilih Data</option>
															    	<option value="">Reguler</option>
															    	<option value="">Ekonomis</option>
															    	<option value="">Eksekutif</option>
															    	<option value="">Super Ekonomis</option>
															    </select>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
															    <label for="formGroupExampleInput">Maskapai</label>
															    <select name="" id="" class="form-control">
															    	<option value="">Pilih Data</option>
															    	<option value="">Air Asia</option>
															    	<option value="">Citylink</option>
															    	<option value="">Garuda</option>
															    	<option value="">Batik Air</option>
															    </select>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
															    <label for="formGroupExampleInput">Embarkasi</label>
															    <select name="" id="" class="form-control">
															    	<option value="">Pilih Data</option>
															    	<option value="">Surabaya</option>
															    	<option value="">Makasar</option>
															    	<option value="">Solo</option>
															    	<option value="">Pekanbaru</option>
															    </select>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-1">
													<button type="button" class="btn btn-primary m-t-search"><i class="fa fa-search"></i></button>
												</div>
											</div>
										</form>
									</div>
									<div class="tab-content clearfix" id="tabs-14">
										<h4 class="m-b-1 m-t-1">Pesan Paket Land Arrangement Terbaik Termurah</h4>
										<form action="" class="m-b-0">
											<div class="row">
												<div class="col-md-11">
													<div class="row">
														<div class="col-md-4">
															<div class="form-group">
															    <label for="formGroupExampleInput">Hotel Makkah</label>
															    <select name="" id="" class="form-control">
															    	<option value="">Pilih Data</option>
															    	<option value="">Afwaj Al Taubah</option>
															    	<option value="">Ajyad Al Azahbi</option>
															    </select>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form-group">
															    <label for="formGroupExampleInput">Hotel madinah</label>
															    <select name="" id="" class="form-control">
															    	<option value="">Pilih Data</option>
															    	<option value="">Al Ansor Al Jadid</option>
															    	<option value="">Al Haram</option>
															    </select>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form-group">
															    <label for="formGroupExampleInput">Jenis Paket</label>
															    <select name="" id="" class="form-control">
															    	<option value="">Pilih Data</option>
															    	<option value="">Paket Ekonomis</option>
															    	<option value="">Paket Regular</option>
															    	<option value="">Paket Eksekutif</option>
															    </select>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-1">
													<button type="button" class="btn btn-primary m-t-search"><i class="fa fa-search"></i></button>
												</div>
											</div>
										</form>
									</div>
									<div class="tab-content clearfix" id="tabs-15">
										<h4 class="m-b-1 m-t-1">Pesan Tiket Pesawat Terbaik Termurah</h4>
										<form action="" class="m-b-0">
											<div class="row">
												<div class="col-md-11">
													<div class="col_one_fifth">
														<div class="form-group m-b-0">
														    <label for="formGroupExampleInput">Keberangkatan</label>
														    <select name="" id="" class="form-control">
														    	<option value="">Semua Waktu Keberangkatan</option>
														    	<option value="">Januari 2018</option>
														    	<option value="">Februari 2018</option>
														    </select>
														</div>
													</div>
													<div class="col_one_fifth">
														<div class="form-group m-b-0">
														    <label for="formGroupExampleInput">Maskapai</label>
														    <select name="" id="" class="form-control">
														    	<option value="">Semua Maskapai</option>
														    	<option value="">Garuda</option>
														    	<option value="">Citylink</option>
														    </select>
														</div>
													</div>
													<div class="col_one_fifth">
														<div class="form-group m-b-0">
														    <label for="formGroupExampleInput">Jenis Penerbangan</label>
														    <select name="" id="" class="form-control">
														    	<option value="">Semua Jenis Penerbangan</option>
														    	<option value="">Satu Arah</option>
														    	<option value="">Pulang Pergi</option>
														    </select>
														</div>
													</div>
													<div class="col_one_fifth">
														<div class="form-group m-b-0">
														    <label for="formGroupExampleInput">Sektor Berangkat</label>
														    <select name="" id="" class="form-control">
														    	<option value="">Semua Sektor Berangkat</option>
														    	<option value="">Sub-Jed</option>
														    	<option value="">Sub-Bwn-Jed</option>
														    </select>
														</div>
													</div>
													<div class="col_one_fifth col_last">
														<div class="form-group m-b-0">
														    <label for="formGroupExampleInput">Sektor Kembali</label>
														    <select name="" id="" class="form-control">
														    	<option value="">Semua Sektor Kembali</option>
														    	<option value="">Jed-Sub</option>
														    	<option value="">Jed-Bwn-Sub</option>
														    </select>
														</div>
													</div>
												</div>
												<div class="col-md-1">
													<button type="button" class="btn btn-primary m-t-search"><i class="fa fa-search"></i></button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div> -->
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_half m-b-0">
						<div class="fancy-title title-dotted-border title-center">
							<h3>Paket Umrah Promo</h3>
						</div>
						<div class="col_full m-b-1">
							<a href="detail-promo-umrah.html" class="panel-hover no-hover-color">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon" data-animate="bounceIn" data-delay="200">
										<img src="<?php echo $themes_inc; ?>images/promo/promo.jpg" alt="">
									</div>
									<h3 class="m-b-1">EKO BY LION 9 HARI SURABAYA-JEDDAH</h3>
									<div class="between">
										<div><small><i class="icon-calendar3"></i> 13 Maret 2018</small></div>
										<div><h3 class="color-brown">IDR 25.000.000</h3></div>
									</div>
								</div>
							</a>
							<hr class="hr-dashed m-b-0 m-t-1">
						</div>
						<div class="col_full m-b-1">
							<a href="detail-promo-umrah.html" class="panel-hover no-hover-color">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon" data-animate="bounceIn" data-delay="200">
										<img src="<?php echo $themes_inc; ?>images/promo/promo.jpg" alt="">
									</div>
									<h3 class="m-b-1">EKO BY LION 9 HARI SURABAYA-MADINAH</h3>
									<div class="between">
										<div><small><i class="icon-calendar3"></i> 13 Maret 2018</small></div>
										<div><h3 class="color-brown">IDR 25.000.000</h3></div>
									</div>
								</div>
							</a>
							<hr class="hr-dashed m-b-0 m-t-1">
						</div>
						<div class="col_full m-b-1">
							<a href="detail-promo-umrah.html" class="panel-hover no-hover-color">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon" data-animate="bounceIn" data-delay="200">
										<img src="<?php echo $themes_inc; ?>images/promo/promo.jpg" alt="">
									</div>
									<h3 class="m-b-1">EKONOMIS PROGRAM 9 HARI (TRANSIT)</h3>
									<div class="between">
										<div><small><i class="icon-calendar3"></i> 13 Maret 2018</small></div>
										<div><h3 class="color-brown">IDR 25.000.000</h3></div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col_half m-b-0 col_last">
						<div class="fancy-title title-dotted-border title-center">
							<h3>Paket Promo LA</h3>
						</div>
						<div class="col_full m-b-1">
							<a href="detail-promo-umrah.html" class="panel-hover no-hover-color">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon" data-animate="bounceIn" data-delay="200">
										<img src="<?php echo $themes_inc; ?>images/promo/promo.jpg" alt="">
									</div>
									<h3 class="m-b-1">REGULER 7 MALAM 40-45 PAX(4 MEKKAH,3 MADINAH)</h3>
									<div class="between">
										<div><small><i class="icon-calendar3"></i> 13 Maret 2018</small></div>
										<div><h3 class="color-brown">IDR 25.000.000</h3></div>
									</div>
								</div>
							</a>
							<hr class="hr-dashed m-b-0 m-t-1">
						</div>
						<div class="col_full m-b-1">
							<a href="detail-promo-umrah.html" class="panel-hover no-hover-color">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon" data-animate="bounceIn" data-delay="200">
										<img src="<?php echo $themes_inc; ?>images/promo/promo.jpg" alt="">
									</div>
									<h3 class="m-b-1">REGULER 7 MALAM 35-39 PAX(4 MEKKAH,3 MADINAH)</h3>
									<div class="between">
										<div><small><i class="icon-calendar3"></i> 13 Maret 2018</small></div>
										<div><h3 class="color-brown">IDR 25.000.000</h3></div>
									</div>
								</div>
							</a>
							<hr class="hr-dashed m-b-0 m-t-1">
						</div>
						<div class="col_full m-b-1">
							<a href="detail-promo-umrah.html" class="panel-hover no-hover-color">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon" data-animate="bounceIn" data-delay="200">
										<img src="<?php echo $themes_inc; ?>images/promo/promo.jpg" alt="">
									</div>
									<h3 class="m-b-1">REGULER 7 MALAM 30-34 PAX(4 MEKKAH,3 MADINAH)</h3>
									<div class="between">
										<div><small><i class="icon-calendar3"></i> 13 Maret 2018</small></div>
										<div><h3 class="color-brown">IDR 25.000.000</h3></div>
									</div>
								</div>
							</a>
							<hr class="hr-dashed m-b-0 m-t-1">
						</div>
						<div class="col_full m-b-1">
							<a href="detail-promo-umrah.html" class="panel-hover no-hover-color">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon" data-animate="bounceIn" data-delay="200">
										<img src="<?php echo $themes_inc; ?>images/promo/promo.jpg" alt="">
									</div>
									<h3 class="m-b-1">REGULER 7 MALAM 25-29 PAX(4 MEKKAH,3 MADINAH)</h3>
									<div class="between">
										<div><small><i class="icon-calendar3"></i> 13 Maret 2018</small></div>
										<div><h3 class="color-brown">IDR 25.000.000</h3></div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="content-wrap p-t-n">
				<div class="container clearfix">
					<div class="row">
						<div class="col-md-12 m-b-0">
							<div class="fancy-title title-dotted-border title-center">
								<h3>Mengapa Harus Sinar Ka'bah Semesta (SKS)?</h3>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 m-b-0">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Harga Terbaik</h3>
									<i class="fa fa-usd fa-absolute"></i>
								</div>
								<div class="panel-body panel-body-custom">
									Dapatkan harga paket Umrah dan Haji maupun komponen lainnya dengan penawaran harga terbaik dari kami. Harga kami jujur dan bersaing.
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 m-b-0">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Pemesanan Mudah</h3>
									<i class="fa fa-desktop fa-absolute"></i>
								</div>
								<div class="panel-body panel-body-custom">
									Pemesanan paket Umrah dan Haji serta komponennya dengan sangat mudah. Hanya 3 langkah mudah kurang dari 5 menit, transaksi anda akan selesai.
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 m-b-0">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Layanan Luas</h3>
									<i class="fa fa-globe fa-absolute"></i>
								</div>
								<div class="panel-body panel-body-custom">
									Kami tidak hanya melayani paket Umrah dan Haji tapi kami juga melayani kebutuhan komponen lainnya seperti LA, Tiket, perlengkapan Umrah Haji dan sebagainya.
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 m-b-0">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Travel Terpercaya</h3>
									<i class="fa fa-check-circle fa-absolute"></i>
								</div>
								<div class="panel-body panel-body-custom">
									Kami merupakan perusahaan travel resmi penyelenggara Umrah yang menjunjung tinggi amanah dan kepercayaan jamaah sebagai tanggung jawab kami dalam melayani jamaah.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content-wrap p-t-n p-b-n">
				<div class="container clearfix">
					<div class="col_full m-b-0">
						<div class="fancy-title title-dotted-border title-center">
							<h3 class="text-center">Legalitas</h3>
						</div>
						<ol type="1" class="">
							<li class="li-list-border">
								Akte Notaris Pendirian no. 3 tanggal 11 April 2011 notaris/PPAT Fatimah Anwar SH.
							</li>
							<li class="li-list-border">
								<b>No. SK Kemenhumham:</b> AHU-24671.AH.01.01 tahun 2011.
							</li>
							<li class="li-list-border">
								Akte Notaris Perubahan no. 1 tanggal 29 Juli 2016 notaris/PPAT Fatimah Anwar SH.
							</li>
							<li class="li-list-border">
								<b>No. SK Kemenhumham:</b> AHU-0014276.AH.01.02 tahun 2016.
							</li>
							<li class="li-list-border">
								Berita Acara Perubahan no. 8 tanggal 8 Maret 2017 notaris /PPAT Gatot Triwaluyo SH.
							</li>
							<li class="li-list-border">
								<b>No. SK Kemenhumham:</b> AHU-0006499.AH.01.02 tahun 2017.
							</li>
							<li class="li-list-border">
								<b>No. Penyelenggara Perjalanan Ibadah Umrah (PPIU):</b> 976/2017
							</li>
							<li class="li-list-border">
								<b>No. Pokok Wajib Pajak (NPWP):</b> 03.135.719.7-001.000
							</li>
							<li class="li-list-border">
								<b>No. Tanda Daftar Perusahaan (TDP):</b> Pemkot Surabaya 13.01.1.79.35331 tanggal 29 Mei 2022.
							</li>
							<li class="li-list-border">
								<b>No. Tanda Daftar Usaha Pariwisata (TDUP):</b> Pemkot Surabaya 503.08/287/436.7.19/2017.
							</li>
							<li class="li-list-border">
								<b>No. Surat Keterangan Domisili:</b> 000/59/436.9.24.4./2017.
							</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="content-wrap p-t-n p-b-n">
				<div class="container clearfix">
					<div class="col_full m-b-0">
						<div class="fancy-title title-dotted-border title-center">
							<h3 class="text-center">Video & Foto</h3>
						</div>
					</div>
					<div class="col_full m-b-0 m-auto">
						<div class="tabs tabs-bordered clearfix" id="tab-2">
							<ul class="tab-nav clearfix">
								<li><a href="#tabs-5"><i class="fa fa-play-circle-o"></i> Video</a></li>
								<li><a href="#tabs-6"><i class="fa fa-camera-retro"></i> Foto</a></li>
							</ul>

							<div class="tab-container">

								<div class="tab-content clearfix" id="tabs-5">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-4">
													<div class="panel-parent">
														<div class="panel panel-royal m-b-1">
															<iframe width="560" height="315" src="https://www.youtube.com/embed/wPzmYRjuAG4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
														</div>
														<div class="panel-block text-center">
															<a href="#myModal1" data-lightbox="inline" class="btn btn-danger btn-lg btn-play"><i class="fa fa-play"></i></a>
														</div>
													</div>
													<h5 class="text-center">Manasik SKS </h5>
												</div>
												<div class="col-md-4">
													<div class="panel-parent">
														<div class="panel panel-royal m-b-1">
															<iframe width="560" height="315" src="https://www.youtube.com/embed/wPzmYRjuAG4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
														</div>
														<div class="panel-block text-center">
															<button type="button" class="btn btn-danger btn-lg btn-play"><i class="fa fa-play"></i></button>
														</div>
													</div>
													<h5 class="text-center">Teknis Perjalanan Umrah</h5>
												</div>
												<div class="col-md-4">
													<div class="panel-parent">
														<div class="panel panel-royal m-b-1">
															<iframe width="560" height="315" src="https://www.youtube.com/embed/VfJMYlBr3F4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
														</div>
														<div class="panel-block text-center">
															<a href="#myModal3" data-lightbox="inline" class="btn btn-danger btn-lg btn-play"><i class="fa fa-play"></i></a>
														</div>
													</div>
													<h5 class="text-center">5 Pasti Umrah</h5>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-4">
													<div class="panel-parent">
														<div class="panel panel-royal m-b-1">
															<iframe width="560" height="315" src="https://www.youtube.com/embed/wPzmYRjuAG4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
														</div>
														<div class="panel-block text-center">
															<button type="button" class="btn btn-danger btn-lg btn-play"><i class="fa fa-play"></i></button>
														</div>
													</div>
													<h5 class="text-center">Company Profile</h5>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-content clearfix" id="tabs-6">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-4">
													<div class="panel-parent">
														<div class="panel panel-royal m-b-1">
															<a href="<?php echo $themes_inc; ?>images/pendukung-4.jpg" data-lightbox="image">
																<img src="<?php echo $themes_inc; ?>images/pendukung-4.jpg" class="img-gallery img-effect" alt="">
															</a>
														</div>
													</div>
													<h5 class="text-center">Jamaah Umrah</h5>
												</div>
												<div class="col-md-4">
													<div class="panel-parent">
														<div class="panel panel-royal m-b-1">
															<a href="<?php echo $themes_inc; ?>images/pendukung-4.jpg" data-lightbox="image">
																<img src="<?php echo $themes_inc; ?>images/pendukung-4.jpg" class="img-gallery img-effect" alt="">
															</a>
														</div>
													</div>
													<h5 class="text-center">Jamaah Umrah</h5>
												</div>
												<div class="col-md-4">
													<div class="panel-parent">
														<div class="panel panel-royal m-b-1">
															<a href="<?php echo $themes_inc; ?>images/pendukung-4.jpg" data-lightbox="image">
																<img src="<?php echo $themes_inc; ?>images/pendukung-4.jpg" class="img-gallery img-effect" alt="">
															</a>
														</div>
													</div>
													<h5 class="text-center">Jamaah Umrah</h5>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--modal 1-->
							<div class="modal1 mfp-hide" id="myModal1">
								<div class="block divcenter" style="background-color: #FFF;">
									<div class="feature-box fbox-center fbox-effect nobottomborder nobottommargin" style="padding: 40px;">
										<iframe width="560" height="315" src="https://www.youtube.com/embed/wPzmYRjuAG4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
									</div>
								</div>
							</div>
							<!--/modal 1-->
							<!--modal 3-->
							<div class="modal3 mfp-hide" id="myModal3">
								<div class="block divcenter" style="background-color: #FFF;">
									<div class="feature-box fbox-center fbox-effect nobottomborder nobottommargin" style="padding: 40px;">
										<iframe width="560" height="315" src="https://www.youtube.com/embed/VfJMYlBr3F4" frameborder="0" allow="no-repeat; encrypted-media" allowfullscreen></iframe>
									</div>
								</div>
							</div>
							<!--/modal 3-->
						</div>
					</div>
				</div>
			</div>
			<div class="content-wrap p-t-n p-b-n">
				<div class="container clearfix">
					<div class="col_full m-b-0">
						<div class="fancy-title title-dotted-border title-center">
							<h3 class="text-center">Pendukung</h3>
						</div>
					</div>
					<div class="col_full m-b-0 m-auto text-center">
						<ul class="ul-inline" type="none">
							<li class="m-l-r">
								<div class="panel-img">
									<img src="<?php echo $themes_inc; ?>images/pendukung-1.jpg" class="img-pendukung" alt="">
								</div>
							</li>
							<li class="m-l-r">
								<div class="panel-img">
									<img src="<?php echo $themes_inc; ?>images/pendukung-2.png" class="img-pendukung" alt="">
								</div>
							</li>
							<li class="m-l-r">
								<div class="panel-img">
									<img src="<?php echo $themes_inc; ?>images/pendukung-3.jpg" class="img-pendukung" alt="">
								</div>
							</li>
							<li class="m-l-r">
								<div class="panel-img">
									<img src="<?php echo $themes_inc; ?>images/pendukung-4.jpg" class="img-pendukung" alt="">
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="content-wrap p-t-n p-b-n bg-blue-smoth">
				<div class="container clearfix">
					<div class="col_full m-b-0 text-center">
						<div id="oc-clients-full" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-nav="true" data-pagi="false" data-loop="true" data-autoplay="5000" data-items-xxs="3" data-items-xs="3" data-items-sm="5" data-items-md="6" data-items-lg="5">
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/garuda.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/citylink.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/lion-air.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/etihad.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/saudi-arabia.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/oman-air.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/royal-brunei.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/nesma-airlines.png" alt="Brands"></a></div>
							<div class="oc-item"><a href="#"><img src="<?php echo $themes_inc; ?>images/plane/malaysia-airlines.png" alt="Brands"></a></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container container-custom-footer" >

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">
					<div class="col-md-3">
						<h4 class="m-b-1 border-bottom">INFORMASI</h4>
						<div class="widget widget_links clearfix m-t-1">
							<ul type="none" class="m-b-m">
								<li class="li-konten-foter">
									<a href="tentang-kami.html">Tentang Kami</a>
								</li>
								<li class="li-konten-foter">
									<a href="metode-pembayaran.html">Metode Pembayaran</a>
								</li>
								<li class="li-konten-foter">
									<a href="contact.html">Hubungi Kami</a>
								</li>
								<li class="li-konten-foter">
									<a href="gallery.html">Gallery</a>
								</li>
								<li class="li-konten-foter">
									<a href="syarat-ketentuan.html">Syarat & Ketentuan</a>
								</li>
								<li class="li-konten-foter">
									<a href="kebijakan-privasi.html">Kebijakan Privasi</a>
								</li>
								<li class="li-konten-foter">
									<a href="https://eservices.haj.gov.sa/eservices3/pages/VisaPaymentInquiry/VisaInquiry.xhtml?dswid=-7810" target="_blank">Cek Visa (biaya tambahan)</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-3">
						<h4 class="m-b-1 border-bottom">PANDUAN</h4>
						<div class="widget widget_links clearfix m-t-1">
							<ul type="none" class="m-b-m">
								<li class="li-konten-foter">
									<a href="panduan-pembayaran-online.html">Pembayaran Online</a>
								</li>
								<li class="li-konten-foter">
									<a href="panduan-registrasi-keagenan.html">Registrasi Keagenan</a>
								</li>
								<li class="li-konten-foter">
									<a href="panduan-paket-umrah.html">Panduan Transaksi Paket Umrah</a>
								</li>
								<li class="li-konten-foter">
									<a href="panduan-paket-umrah.html">Panduan Transaksi Paket Umrah Group</a>
								</li>
								<li class="li-konten-foter">
									<a href="panduan-paket-umrah.html">Panduan Transaksi Visa</a>
								</li>
								<li class="li-konten-foter">
									<a href="panduan-paket-umrah.html">Panduan Transaksi Paket LA</a>
								</li>
								<li class="li-konten-foter">
									<a href="panduan-paket-umrah.html">Panduan Transaksi Tiket Pesawat</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-3">
						<h4 class="m-b-1 border-bottom">TAUTAN LAIN</h4>
						<div class="widget widget_links clearfix m-t-1">
							<ul type="none" class="m-b-m">
								<li class="li-konten-foter">
									<a href="#">Insan Islami</a>
								</li>
								<li class="li-konten-foter">
									<a href="#">Iphi Travel</a>
								</li>
								<li class="li-konten-foter">
									<a href="#">Fefva Travel</a>
								</li>
								<li class="li-konten-foter">
									<a href="#">Ulya Wisata Persada</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<img src="<?php echo $themes_inc; ?>images/logo-footer-sks.png" class="center-mobile" alt="">
						<ul type="none" class="m-t-1">
							<li class="li-inline">
								<a href="#" class="social-icon si-light si-facebook" title="Facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>
							</li>
							<li class="li-inline">
								<a href="#" class="social-icon si-light si-gplus" title="Google Plus">
									<i class="icon-gplus"></i>
									<i class="icon-gplus"></i>
								</a>
							</li>
							<li class="li-inline">
								<a href="#" class="social-icon si-light si-linkedin" title="Linked In">
									<i class="icon-linkedin"></i>
									<i class="icon-linkedin"></i>
								</a>
							</li>
							<li class="li-inline">
								<a href="#" class="social-icon si-light si-youtube" title="Youtube">
									<i class="icon-youtube"></i>
									<i class="icon-youtube"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">
				<div class="container clearfix">
					<div class="col_full text-center m-b-0">
						Copyrights &copy; 2017 Sablan Indonesia All Rights Reserved.<br>
					</div>
				</div>
			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?php echo $themes_inc; ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $themes_inc; ?>js/plugins.js"></script>
	<!-- <script type="text/javascript" src="js/jquery.nivo.js"></script> -->
	<script type="text/javascript" src="<?php echo $themes_inc; ?>js/jquery.camera.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo $themes_inc; ?>js/functions.js"></script>
	<!-- <script type="text/javascript">

		jQuery(document).ready(function($) {

			$('.nivoSlider').nivoSlider({
				effect: 'random',
				slices: 15,
				boxCols: 12,
				boxRows: 6,
				animSpeed: 500,
				pauseTime: 8000,
				directionNav: true,
				controlNav: true,
				pauseOnHover: true,
				prevText: '<i class="icon-angle-left"></i>',
				nextText: '<i class="icon-angle-right"></i>',
				afterLoad: function(){
					$('#slider').find('.nivo-caption').addClass('slider-caption-bg');
				}
			});

		});

	</script> -->
	<script type="text/javascript">

		jQuery(document).ready(function($) {

			$('#camera_wrap_1').camera({
				thumbnails: true,
				height: '40%',
				loader: 'pie',
				loaderPadding: 1,
				loaderStroke: 5,
				onLoaded: function() {
					$('#camera_wrap_1').find('.camera_next').html('<i class="icon-angle-right"></i>');
					$('#camera_wrap_1').find('.camera_prev').html('<i class="icon-angle-left"></i>');
				}
			});

		});

	</script>

</body>


</html>
