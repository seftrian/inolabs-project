<?php
$w_lib=new widget_lib;
?>
<div class="row single-post">
  <div class="col-sm-12">
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url()?>">Home</a></li>
          <li class="active"><a href="#"><?php echo $seoTitle?></a></li>
        </ul>

    
      <article>
			<input type="hidden" id="base_url" value="<?php echo base_url()?>">

      	<div class="tabbable">
												<ul id="myTab4" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
													<li class="active">
														<a href="#polling" data-toggle="tab">
															Polling
														</a>
													</li>
													<li class="">
														<a href="#registration" data-toggle="tab">
															Biodata
														</a>
													</li>
										
												</ul>
												<div class="tab-content">
													<div class="tab-pane active" id="polling">
														<div id="alert-polling" class="alert" style="display:none;"></div>
                               <form method="post" id="polling" class="form-horizontal">
                                   <?php
                                   if(!empty($results))
                                   {
                                    $num=1;
                                    foreach($results AS $index_question=>$rowArr)
                                    {
                                      foreach($rowArr AS $variable=>$value)
                                      {
                                        ${$variable}=$value;
                                      }
                                      $answer_arr=polling_lib::get_answer_by_question_id($polling_question_id);
                                      ?>
                                       <div class="row polling_question">
                                            <div class="form-group" style="margin:0px">
                                              <div class="col-sm-1"><label class="col-sm-offset-11" id="label_number"> <?php echo $num?>. </label></div>
                                              <div class="col-sm-11">
                                                <?php echo $polling_question_name?>
                                              </div>
                                            </div>
                                            <?php
                                            if(!empty($answer_arr))
                                            {
                                              foreach($answer_arr AS $index_answer=>$rowAnswerArr)
                                              {
                                                foreach($rowAnswerArr AS $variableAnswer=>$valueAnswer)
                                                {
                                                  ${$variableAnswer}=$valueAnswer;
                                                }
                                                ?>
                                                <div style="margin-top:0px;margin-right:0px;margin-left:0px;margin-bottom:2px;" class="form-group polling_answer">
                                                  <div class="col-sm-1">&nbsp;</div>
                                                  <div class="col-sm-8">
                                                   <input type="radio" name="spsd_polling_answer_id[<?php echo $polling_question_id?>]" value="<?php echo $polling_answer_id?>" id="answer_<?php echo $polling_answer_id?>"/> <label class="text-muted" for="answer_<?php echo $polling_answer_id?>"><?php echo $polling_answer_name?></label>
                                                  </div>
                                                </div>
                                                <?php
                                              }
                                            }
                                            ?>
                                                <hr />

                                       </div>
                                      <?php
                                      $num++;
                                    } 

                                   }
                                 
                                   ?> 
                                     
                                   <button onclick="validation_polling('<?php echo $category_id?>');return false;" class="btn btn-sm btn-success">Lanjutkan</button>
                             </form>    
													</div>
													<div class="tab-pane" id="registration">
                          <div id="alert-registration" class="alert" style="display:none;"></div>
                          <p class="text-warning" style="margin:5px;">Mohon lengkapi form isian di bawah ini.</p>
                          
                          <form method="post" id="form-registration" class="form-horizontal">
                          <div class="form-group">
                          <div class="col-sm-1">&nbsp; </div>
                          <div class="col-sm-11"><input name="full_name" class="form-control" type="text" placeholder="* Nama Lengkap"></div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-1">&nbsp;</div>
                            <div class="col-sm-11">
                            <input class="form-control" name="email_address" type="text" placeholder="* Email">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-1">&nbsp;</div>
                            <div class="col-sm-11">
                            <input class="form-control" name="phone" type="text" placeholder="* No. Telp">
                            </div>
                          </div>

                          <div class="form-group">
                           <div class="col-sm-1">&nbsp;</div>
                            <div class="col-sm-11">
                            <textarea class="form-control" name="spsh_note" placeholder="Komentar"></textarea>
                            </div>
                          </div>
                           <button class="btn btn-sm btn-warning" onclick="$('.nav-tabs a[href=#polling]').tab('show');return false;">Kembali</button>
                           <button onclick="save_polling('<?php echo $category_id?>');return false;" class="btn btn-sm btn-success">Simpan</button>
                          </form>
													</div>
												
												</div>
											</div>
      </article>
      
    </div>
</div>      