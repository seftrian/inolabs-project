<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$w_lib=new widget_lib;
$uri_1=$this->uri->segment(1);
$uri_2=$this->uri->segment(2);
$uri_3=$this->uri->segment(3);
$themes_arr=$this->config->item('theme');
$themes_inc=base_url().'themes/front/'.$themes_arr['frontend']['name'].'/inc/';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo isset($seoTitle) ? $seoTitle:$websiteConfig['config_name']; ?></title>

    <!-- Css Folder -->
    <link href="<?php echo $themes_inc; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/color.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>style.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/responsive.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/themetypo.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/bxslider.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/datepicker.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo $themes_inc; ?>js/jquery.js"></script>
  </head>
  <body>
    <!--// Wrapper //-->
    <div class="wrapper">

      <!--// Main Content //-->
      <div class="main-content">
           <article class="col-md-12" id="div-form-order" style="display:none;">
           <div class="alert"  style="display:none;"></div>      
           <form id="form-registration-consument" action="" method="post">
            <table class="kd-table kd-tabletwo">
            <tbody>
            <tr>
              <input type="hidden" name="ref" value="no_registration">
              <td style="width:15%;">Email *</td>
              <td style="width:35%;"><input name="member_email" type="email" class="form-control" required/></td>
              <td style="width:15%;">Nama Lengkap *</td>
              <td style="width:35%;"><input type="text" name="person_full_name" class="form-control"  required/></td>
            </tr>
            <tr>
              <td style="width:15%;">Alamat *</td>
              <td style="width:35%;"><textarea name="additional_info_address" class="form-control" required></textarea> </td>
              <td style="width:15%;">Posisi</td>
              <td style="width:35%;">
              <select class="form-control" name="position_id" required>
              </select>
              </td>
            </tr>
            <tr>
            <td>Kebutuhan *</td>
              <td> 
                <input style="width:20%;" name="qty" value="7" type="text" class="form-control" required/>
                <select style="width:70%;float:right;" class="form-control" name="unit" required>
                     <option value="hour">Jam</option>
                     <option value="day" selected>Hari</option>
                     <option value="month">Bulan</option>
                </select>
              </td>
             
              <td style="width:15%;">Agama *</td>
              <td style="width:35%;">
              <input type="text" name="person_religion" class="form-control" required/>
              </td>
            </tr>
            <tr>
              <td>No. Hp. *</td>
              <td> 
                <input name="person_phone" type="text" class="form-control" required/>
              </td>
              <td>Jenis Kelamin *</td>
              <td>
                <select class="form-control" name="person_gender" required>
                     <option value="">Pilih jenis kelamin</option>
                     <option value="L">Laki-laki</option>
                     <option value="P">Perempuan</option>
                </select>
              </td>
            </tr>
             <tr>
               <td>Keterangan Tentang pasien *</td>
              <td colspan="3"><textarea name="note_pasien" class="form-control" required></textarea> </td>
            </tr>
            </tbody>
            </table>
             
           <div class="form-group" style="text-align: right;margin:10px;">
             <a href="javascript:void(0);" onclick="close_form_order();return false;" >Batal</a>
             <button onclick="stored_ecom_medical();return false;" style="margin-left:20px;" class="btn btn-success">Daftar</button>
           </div>
            </form>
          </article>
          <div class="container">
         
            <?php
            $getImagePath=$this->master_perawat_helper_lib->getImagePath();
            $pathImgUrl=$getImagePath['pathUrl'];
            $pathImgLoc=$getImagePath['pathLocation'];
            $total_data=count($shopping_cart_arr);
            if(!empty($shopping_cart_arr))
            {
              ?>
               <p style="margin:10px;">
              <button id="btn-order" onclick="show_form_order($(this));return false;" class="btn btn-success" style="float:right;"><i class="fa fa-shopping-cart"></i> Pesan</button>
              </p>  

              <?php
              $no=1;
              foreach($shopping_cart_arr AS $rowArr)
              {
                foreach($rowArr AS $variable=>$value)
                {
                  ${$variable}=$value;
                }
                if($order_by==1)
                {

                  $profil_verified_arr=$this->master_perawat_helper_lib->get_profil_perawat_verified(' AND medical_master_person_id='.intval($person_id));
                  if(!empty($profil_verified_arr))
                  {
                    foreach($profil_verified_arr AS $variable_ver=>$value_ver)
                    {
                      ${$variable_ver}=$value_ver;
                    }
                    $icon_has_experienced=($medical_master_has_experienced=='Y')?'<i class="fa fa-heart-o"></i> Berpengalaman':'<i class="fa fa-graduation-cap"></i> Baru Lulus';
                    $label_location_duty=$this->master_perawat_helper_lib->label_location_duty($person_id);
                    $label_position=$this->master_perawat_helper_lib->label_position($person_id);
                    $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
                    $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
                    $logo_file=$logo_name.'.'.$logo_ext;
                    
                    $imgProperty=array(
                    'width'=>190,
                    'height'=>190,
                    'imageOriginal'=>$logo_file,
                    'directoryOriginal'=>$pathImgLoc,
                    'directorySave'=>$pathImgLoc.'190190/',
                    'urlSave'=>$pathImgUrl.'190190/',
                    );
                    $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                    $medical_master_experience=($medical_master_has_experienced=='Y')?$medical_master_experience:'';
                    $text_about=strip_tags(html_entity_decode($medical_master_about.' '.$medical_master_experience.' '.$medical_master_skills));
                    $text_about=(strlen($text_about)>206)?substr($text_about,0,206).'...':$text_about;
                    $minimal_rate=function_lib::currency_rupiah($medical_master_rate_min).'/jam';

                    ?>
                      <b>Nakes Utama</b>
                      <p>
                            <table class="kd-table kd-tabletwo">
                              <tbody>
                                <tr>
                                  <td style="width:30%;"><img src="<?php echo $img?>" alt="<?php echo $person_full_name?>"></td>
                                  <td colspan="3">

                                      <p  style="color:#000;text-align:justify;">
                                      <b><?php echo $person_full_name?></b><br />
                                      <span class="text-warning">
                                      <?php echo $medical_master_education?> - Rp <?php echo $minimal_rate?><br>
                                      <?php echo $text_about?>
                                      </span>
                                      </p>
                                        <p class="kd-usernetwork text-warning"  style="color:#000;text-align:justify;"> 
                                             <i class="fa fa-stethoscope"></i> <?php echo $label_position?><br />
                                             <?php echo ($total_data>1)?'<button title="Set menjadi perawat alternatif" class="btn btn-xs btn-warning btn-set" onclick="switch_nakes_utama_to_alternatif();return false;"><i class="fa fa-wrench"></i> Set Alternatif</button>':''?>
                                             <button title="Hapus" onclick="remove_shopping_cart_nakes('<?php echo $order_by?>');return false;" class="btn-remove btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> </button>
                                              </p>

                                    </td>
                                </tr>
                              </tbody>
                            </table>

                      </p>
                    <?php
                  }
                  
                }
                else
                {
                  if($no==2)
                  {
                    ?>

                    <b>Nakes Alternatif</b>
                    <p>
                     <table class="kd-table kd-tabletwo">
                      <tbody>
                    <?php
                  }
                  ?>
                      <?php
                      if($no%2==0)
                      {
                        ?>
                        <tr>
                        <?php
                      }

                       $profil_verified_arr=$this->master_perawat_helper_lib->get_profil_perawat_verified(' AND medical_master_person_id='.intval($person_id));
                  if(!empty($profil_verified_arr))
                  {
                    foreach($profil_verified_arr AS $variable_ver=>$value_ver)
                    {
                      ${$variable_ver}=$value_ver;
                    }
                    $icon_has_experienced=($medical_master_has_experienced=='Y')?'<i class="fa fa-heart-o"></i> Berpengalaman':'<i class="fa fa-graduation-cap"></i> Baru Lulus';
                    $label_location_duty=$this->master_perawat_helper_lib->label_location_duty($person_id);
                    $label_position=$this->master_perawat_helper_lib->label_position($person_id);
                    $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
                    $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
                    $logo_file=$logo_name.'.'.$logo_ext;
                    
                    $imgProperty=array(
                    'width'=>126,
                    'height'=>94,
                    'imageOriginal'=>$logo_file,
                    'directoryOriginal'=>$pathImgLoc,
                    'directorySave'=>$pathImgLoc.'255191/',
                    'urlSave'=>$pathImgUrl.'255191/',
                    );
                    $img=$this->function_lib->resizeImageMoo($imgProperty);
                    $medical_master_experience=($medical_master_has_experienced=='Y')?$medical_master_experience:'';
                    $text_about=strip_tags(html_entity_decode($medical_master_about.' '.$medical_master_experience.' '.$medical_master_skills));
                    $text_about=(strlen($text_about)>206)?substr($text_about,0,206).'...':$text_about;
                    $minimal_rate=function_lib::currency_rupiah($medical_master_rate_min).'/jam';

                    ?>
                    <td style="width:25%;">
                      <img src="<?php echo $img?>" alt="<?php echo $person_full_name?>"></td>
                      <td>  <p  style="color:#000;text-align:justify;">
                        <b><?php echo $person_full_name?></b><br />
                          <span class="text-warning"><?php echo $medical_master_education?> <br /> Rp <?php echo $minimal_rate?></span><br>
                          <button title="Set menjadi perawat utama" onclick="switch_to_nakes_utama('<?php echo $order_by?>');return false;" class="btn btn-xs btn-success btn-set"><i class="fa fa-wrench"></i> Set Utama</button>
                          <button title="Hapus" onclick="remove_shopping_cart_nakes('<?php echo $order_by?>');return false;" class="btn btn-xs btn-danger btn-remove"><i class="fa fa-trash-o"></i> </button>
                      </p></td>
                    <?php
                  }
                      ?>
                      
                      <?php
                      if($no%2!=0 OR $no==$total_data)
                      {
                        ?>
                        </tr>
                        <?php
                      }
                      if($no==$total_data)
                      {
                        ?>
                        </tbody>
                      </table>
                      </p>
                        <?php
                      }
                }

                $no++;
              }
            }
            ?>
          
                  


          </div>
      </div>
  

    </div>


    <!-- jQuery (Necessary For JavaScript Plugins) -->
    <script src="<?php echo $themes_inc; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $themes_inc; ?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo $themes_inc; ?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo $themes_inc; ?>js/waypoints-min.js"></script>
    <script src="<?php echo $themes_inc; ?>js/functions.js"></script>
     <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?php echo base_url()?>addons/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="<?php echo base_url()?>addons/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url()?>addons/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   
    <?php
      echo isset($footerScript) ? $footerScript : '';
    ?>  
  </body>
</html>