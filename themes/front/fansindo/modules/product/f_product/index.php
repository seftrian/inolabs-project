<section class="kd-pagesection" style=" padding: 0px 0px 10px 0px; ">
        <div class="container">
          <div class="row">

            <div class="col-md-12">

              <div class="kd-package-list">
                <div class="row">
                  <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="#"><?php echo $seoTitle?></a></li>
                  </ul>
                  <?php
                   $path='assets/images/';
                  $pathImgUrl=base_url().$path;
                  $pathImgLoc=FCPATH.$path;
                  if(!empty($results))
                  {
                    foreach($results AS $rowArr)
                    {
                      foreach($rowArr AS $variable=>$value)
                      {
                        ${$variable}=$value;
                      }

                      $product_image_name= (trim($product_image)=='')?'-': pathinfo($product_image,PATHINFO_FILENAME);
                      $product_image_name_ext=  pathinfo($product_image,PATHINFO_EXTENSION);
                      $product_image_file=$product_image_name.'.'.$product_image_name_ext;
                      $imgProperty=array(
                      'width'=>360,
                      'height'=>272,
                      'imageOriginal'=>$product_image_file,
                      'directoryOriginal'=>$pathImgLoc.'product/',
                      'directorySave'=>$pathImgLoc.'product/360272/',
                      'urlSave'=>$pathImgUrl.'product/360272/',
                      );
                      $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                      $link_detail=product_lib::link_detail($product_permalink);
                      ?>
                          <article class="col-md-4">
                            <figure><a href="<?php echo $link_detail?>"><img src="<?php echo $img; ?>" alt="<?php echo $product_name?>"></a>
                              <figcaption>
                                <span class="package-price thbg-color">Rp <?php echo function_lib::currency_rupiah($product_current_price)?></span>
                                <div class="kd-bottomelement">
                                  <h5><a href="<?php echo $link_detail?>"><?php echo $product_name?></a></h5>
                                </div>
                              </figcaption>
                            </figure>
                          </article>
                      <?php
                    }
                  }
                  ?>
                  
                </div>
                <div class="pagination-wrap">
                  <div class="pagination">
                    <a href="#"><i class="fa fa-angle-double-left"></i></a>
                    <a href="#">1</a>
                    <a href="#">2</a>
                    <span>3</span>
                    <a href="#">4</a>
                    <a href="#">5</a>
                    <a href="#">6</a>
                    <a href="#"><i class="fa fa-angle-double-right"></i></a>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>