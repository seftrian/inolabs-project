<?php
$discount_value=($product_old_price>$product_current_price)?($product_old_price-$product_current_price):0;
$strike_price=($discount_value>0)?'<span style="color:red;text-align:right;font-size:11px;"><strike>Rp '.function_lib::currency_rupiah($product_old_price).'</strike></span>':'';
          
$path='assets/images/';
$pathImgUrl=base_url().$path;
$pathImgLoc=FCPATH.$path;
$product_image_name= (trim($product_image)=='')?'-': pathinfo($product_image,PATHINFO_FILENAME);
                                        
$product_image_name_ext=  pathinfo($product_image,PATHINFO_EXTENSION);
$product_image_file=$product_image_name.'.'.$product_image_name_ext;
$imgProperty=array(
'width'=>728,
'height'=>400,
'imageOriginal'=>$product_image_file,
'directoryOriginal'=>$pathImgLoc.'product/',
'directorySave'=>$pathImgLoc.'product/728400/',
'urlSave'=>$pathImgUrl.'product/728400/',


);
$img=$this->function_lib->resizeImageMoo($imgProperty,true,true);  
?>
<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
        <div class="container">
          <div class="row">
 						<ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>product/index">Layanan</a></li>
                    <li><a href="#"><?php echo $seoTitle?></a></li>
            </ul>
              
              <!--// Package Detail //-->
              <div class="col-md-8">
              <div class="kd-section-title"><h3><?php echo $seoTitle?></h3></div>
                <div class="kd-package-detail">
                  <div class="kd-pkg-info">
                    <ul>
                      <li><i class="fa fa-tag"></i> <strong>Harga:</strong> Rp <?php echo function_lib::currency_rupiah($product_current_price).' '.$strike_price?></li>
                      <?php
                      echo ($discount_value>0)?' <li><i class="fa fa-cut"></i> <strong>Hemat:</strong> Rp '.function_lib::currency_rupiah($discount_value).'</li>':'';
                      ?>
                     
                    </ul>
                    <button onclick="add_to_cart_product($(this),<?php echo $product_id?>,true);" class="kd-booking-btn custom-btn btn-xs">Pesan Sekarang</button>
                  </div>

                  <figure class="detail-thumb">
                  	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
										  <!-- Indicators -->
										  <ol class="carousel-indicators">
										    <?php
										    if(count($images_arr)>0)
										    {
										    	?>
											    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
										    	<?php
										    }
										    for($i=1;$i<=count($images_arr);$i++)
										    {
										    	?>
											    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i?>"></li>
										    	<?php
										    }
										    ?>
										  </ol>
										 
										  <!-- Wrapper for slides -->
										  <div class="carousel-inner">
										    <div class="item active">
										      <img src="<?php echo $img?>" alt="<?php echo $seoTitle?>">
										    </div>

									      <?php
                        if(!empty($images_arr))
                        {
                            $no=1;
                            foreach($images_arr AS $rowArr)
                            {
                                $product_image=$rowArr['product_more_images_file'];
                                $product_image_name=  pathinfo($product_image,PATHINFO_FILENAME);
                                $product_image_name_ext=  pathinfo($product_image,PATHINFO_EXTENSION);
                                $product_image_file=$product_image_name.'.'.$product_image_name_ext;
                                $imgProperty=array(
                                'width'=>728,
                                'height'=>400,
                                'imageOriginal'=>$product_image_file,
                                'directoryOriginal'=>$pathImgLoc.'product/',
                                'directorySave'=>$pathImgLoc.'product/728400/',
                                'urlSave'=>$pathImgUrl.'product/728400/',


                                );
                                $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                               ?>
                                  <div class="item">
															      <img src="<?php echo $img?>" alt="<?php echo $seoTitle?>">
															    </div>
                               <?php
                               $no++;
                            }
                        }
                        ?>
										  </div>
										 
										   <?php
										    if(count($images_arr)>0)
										    {
										    	?>
										  <!-- Left and right controls -->
										  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
										    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										    <span class="sr-only">Previous</span>
										  </a>
										  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
										    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										    <span class="sr-only">Next</span>
										  </a>
										   	<?php
										    }
										    ?>
										</div> <!-- Carousel -->
                  </figure>

                  <div class="kd-rich-editor">
                  	<p><?php echo html_entity_decode($product_content)?></p>
                  </div>

                </div>
              </div>
              <!--// Package Detail //-->

              <aside class="col-md-4">
                <?php 
                    $this->widget_lib->run('front', 'master_perawat', 'widget_sidebar_search_perawat');
                  ?>
              </aside>

          </div>
        </div>
      </section>