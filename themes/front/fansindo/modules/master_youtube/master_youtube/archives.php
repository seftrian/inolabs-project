<?php
$w_lib=new widget_lib;
?>

    <!-- Page Title
        ============================================= -->
    <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1><?php echo $category_title?></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="#"><?php echo $category_title?></a></li>
                </ol>
            </div>

        </section>


    <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent nobottommargin col_last clearfix">

                        <!-- Posts
                        ============================================= -->
                        


              <?php
                if(empty($results))
                {
                    ?>
                    <div class="single-post nobottommargin">

                            <!-- Single Post
                            ============================================= -->
                            <div class="entry clearfix">

                                <!-- Entry Title
                                ============================================= -->
                                <div class="entry-title">
                                    <h2><center>Artikel belum dimuat</center></h2>
                                </div><!-- .entry-title end -->
                            
                            </div><!-- .entry end -->
                    </div><!-- .post-navigation end -->
                    
                    <?php
                }
                else
                {
                    ?>
                          <div id="posts" class="small-thumbs">

                  <?php 
                   foreach($results AS $rowArr)
                    {
                      foreach($rowArr AS $variable=>$value)
                      {
                        ${$variable}=$value;
                      }
                      $video_link='https://www.youtube.com/embed/'.$youtube_id;
                      $video_watch='https://www.youtube.com/watch?v='.$youtube_id;
                      $link_image='http://img.youtube.com/vi/'.$youtube_id.'/mqdefault.jpg';  
                      $link_image_1='http://img.youtube.com/vi/'.$youtube_id.'/0.jpg';  
                      

                     ?>
                            <div class="entry clearfix">
                                <div class="entry-image">
                                    <a href="<?php echo $video_watch?>" data-lightbox="iframe" style="position: relative;">
                                      <img class="image_fade" src="<?php echo $link_image_1; ?>" alt="<?php echo $video_watch?>">
                                      <span class="i-overlay nobg"><img src="<?php echo base_url().'themes/front/lenteraanak/inc/'; ?>images/icons/video-play.png" alt="Play"></span>
                                    </a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h2><?php echo $youtube_title?></h2>
                                    </div>
                                    
                                    <div class="entry-content">
                                        <p><?php echo html_entity_decode($youtube_description) ?></p>
                                    </div>
                                </div>
                            </div>

                      <?php
                    }
                  ?>
                          </div><!-- #posts end -->
                      <?php
                }
              ?>    
                

                        <!-- Pagination
                        ============================================= -->
                        <!-- ul class="pagination">
                          <li class="disabled"><a href="#"><<</a></li>
                          <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                          <li><a href="#">2</a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li><a href="#">5</a></li>
                          <li><a href="#">>></a></li>
                       </ul -->

                       <?php echo $link_pagination?>
                    </div><!-- .postcontent end -->


                    <?php 
                        $w_lib->run('front', 'master_content', 'widget_popular_new');
                    ?>

                    

                </div>

            </div>

        </section><!-- #content end -->