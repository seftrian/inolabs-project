<?php
$w_lib=new widget_lib;

if(!empty($results)){    
?>
	<div class="content-wrap content-wrap-custom bttm-0 nobttmmrgin tp-bttm-padding-mobile" style="padding-top:42px;padding-bottom:15px;">
      <div class="container-fullwidth clearfix">
      				<div class="col_full m-b-0 tp-mbl">
						<h4 class="h-line color-purple">Gallery</h4>
						<div class="d-line"></div>
					</div>

					<div class="col_full nobottommargin">
						<div class="tabs tabs-bordered clearfix">

							<ul class="tab-nav clearfix">
								<li><a href="#tabs-foto"><i class="icon-picture"></i> Foto</a></li>
								<li><a href="#tabs-video"><i class="icon-play"></i> Video</a></li>
							</ul>

							<div class="tab-container">

								<div class="tab-content clearfix" id="tabs-foto">
									<?php
									foreach ($results as $rowArrPhoto) {
										$w_lib->run('front', 'master_content', 'widget_content_file', 'foto', $rowArrPhoto);
									}
									?>									
								</div>

								<div class="tab-content clearfix" id="tabs-video">
									<?php
									foreach ($results as $rowArrVideo) {
										$w_lib->run('front', 'master_content', 'widget_content_file', 'video', $rowArrVideo);
									}
									?>
								</div>
							</div>
						</div>
					</div>
      </div>
   	</div>

<?php
} else {
    ?>
    <div class="content-wrap content-wrap-mobile notopmargin nobottommargin">
        <div class="container-fullwidth clearfix">
              <div class="entry-title">
                  <h2><center>Artikel belum dimuat</center></h2>
              </div>
        </div>
    </div>
    <?php
}
?>