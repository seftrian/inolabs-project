<?php
$w_lib=new widget_lib;
?>
<!--title-->
<section id="content" class="">
  <div class="content-wrap wrap-top-only" style="background:url('<?php echo $themes_inc; ?>images/bg/gplaypattern.png'); background-repeat:repeat">
    <div class="container clearfix">
      <h2 class="m-b-0 text-center"><?php echo $category_title; ?></h2>
    </div>
  </div>
</section>
<!--/title-->
<!-- Content
============================================= -->
<section id="content">
  <div class="content-wrap p-t-n p-b-n">
    <div class="container clearfix">
      <div class="row">
      <?php
      if (empty($results)) {
      ?>
      <section id="content">
        <div class="content-wrap p-t-n p-b-n">
          <div class="container clearfix">
            <div class="row">
              <div class="col-md-12">
                <h4 class="text-center"><i class="fa fa-danger"></i> Paket Saat Ini Belum Tersedia</h4>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php
      }
      $keyno = 0;
      foreach($results AS $rowArr)
      {
        foreach($rowArr AS $variable=>$value)
        {
          ${$variable}=$value;
        }
        ?>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading panel-heading-2">
              <img src="<?php echo base_url().$news_photo; ?>" class="img-paket" alt="">
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6 col-xs-6 col-sm-6">
                      <!-- <span class="label label-primary label-custom m-b-0 m-t-0">PROMO</span> -->
                      <h4 class="m-b-0">Harga </h4>
                    </div>
                    <div class="col-md-6 col-xs-6 col-sm-6 price">
                      <h4 class="m-b-0"><b><?php echo $news_label_1; ?></b></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <a href="<?php echo base_url('master_content/detail_paket/'.$news_permalink); ?>" class="btn btn-warning btn-block">Detail Paket</a>
            </div>
          </div>
        </div>
        <?php
        $keyno++;
        }
        ?>
      </div>
    </div>
  </div>
</section>
