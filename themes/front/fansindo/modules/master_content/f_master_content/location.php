<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();

$w_lib=new widget_lib;
?>
<div id="crumbs" class="white">
<div class="row large">
    <div class="four columns">
      <!-- page title -->
      <?php
      if(isset($breadcrumbs))
      {
        echo $breadcrumbs; 
      }
      else
      {
        ?>
        <h6 class="sub-heading"><?php echo strtoupper(isset($seoTitle) ? $seoTitle:$websiteConfig['config_name']); ?></h6>
        <?php                    
      }
      ?>
    </div>
    <div class="eight columns text-right">
      <!-- top on-page nav -->
      <?php
       $w_lib->run('front', 'admin_menu', 'widget_menu_user_header');
       ?>
    </div>
  </div>
</div>
			<div id="map"></div>
			<section class="grey stripe-bg">
				<!-- two third / left side -->
				<div class="two-third white">
					<div id="contact-page">
						<div class="content-block">

                 <?php
                  if(empty($row_array))
                  {
                    ?>
                    <p>Maaf, artikel belum dimuat.</p>
                    <?php
                  }
                  else
                  {
                    //initialize variable
                    foreach($row_array AS $variable=>$value)
                    {
                      ${$variable}=$value;
                    }

                    $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
                    $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
                    $logo_file=$logo_name.'.'.$logo_ext;
                    $pathImgUrl=$pathImgArr['pathUrl'];
                    $pathImgLoc=$pathImgArr['pathLocation'];
                    $imgProperty=array(
                    'width'=>1025,
                    'height'=>650,
                    'imageOriginal'=>$logo_file,
                    'directoryOriginal'=>$pathImgLoc,
                    'directorySave'=>$pathImgLoc.'1025650/',
                    'urlSave'=>$pathImgUrl.'1025650/',
                    );
                    $img=$this->function_lib->resizeImageMoo($imgProperty);
                    $dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
                    $news_content=function_lib::remove_html_tag(strip_tags(html_entity_decode($news_content)));
                    ?>
                     <!-- post  img header -->
                     <?php
                     if($news_photo!='')
                     {
                        ?>
                          <div class="post-img">
                            <img src="<?php echo $img?>" alt="<?php echo $news_title?>"/>
                          </div>
                        <?php
                     }
                     ?>
                    
                     	<div class="row">
												<!-- page title / description -->
												<h3 class="bold"><?php echo strtoupper($news_title)?></h3>
												<h4><?php echo $news_content?></h4>
												<hr class="thin grey">
												 <?php
		                      if(!empty($download_arr))
		                      {

		                        foreach($download_arr AS $rowArr)
		                        {
		                          foreach($rowArr AS $variable=>$value)
		                          {
		                            ${$variable}=$value;
		                          }
		                          $download_title_value=$download_title;
		                          $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
		                          $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
		                          $logo_file=$logo_name.'.'.$logo_ext;
		                         
		                          $btn_download='<a class="btn btn-sm" title="Download File" onclick="window.location.href=\''. base_url().'master_file/f_master_file/download_file/'.$download_id.'\';return false;"><span class="fa fa-download"></span></a>';
		                          $download_icon='';
		                          if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
		                          {
		                              $download_icon=$btn_download;
		                              ?>
		                              <?php echo $download_icon.' '.$download_title?><br />
		                              <?php
		                          }
		                        }
		                      }
		                      ?>
											</div>
                    
                     
                    <?php
                  }
                  ?>
                     

						
							<div class="row">
								<!-- contact form -->
								<form method="post" action="#" id="form-message" onsubmit="send_message();return false;">
				        <input id="to" name="to" class="span8" value="<?php echo $websiteConfig['config_email']?>" type="hidden"/>

									<ul>
										<li>
										<p>
											 * All fields are required.
										</p>
										</li>
										<li class="field"><input class="input" type="email" placeholder="Your Email"  id="from" name="from" required></li>
										<li class="field"><input class="input" type="text" placeholder="Subject" id="subject" name="subject"  required></li>
										<li class="field"><textarea class="input textarea" placeholder="Your Message" rows="4" id="message" name="message"  required></textarea></li>
										<li>
										<!-- submit button -->
										<input value="SEND" class="submit" id="submit" name="submit" type="submit" onclick="send_message();return false;">
										</li>
									</ul>
								</form>
								 <div class="alert" style="display:none;"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- one third sidebar -->
				<aside class="one-third grey">
				<div id="sidebar">
					<!-- contact widget -->
					<?php 
            $w_lib->run('front', 'master_content', 'widget_sidebar_content','kontak');
           ?>
				</div>
				</aside>
				<!-- clear floats -->
				<div class="clear">
				</div>
			</section>


		<script type="text/javascript">
              
           var infowindow_content = new google.maps.InfoWindow();
            var map_content;
            function initialize_() {
              var myLatlng = new google.maps.LatLng(<?php echo $websiteConfig['config_lat']?>, <?php echo $websiteConfig['config_lng']?>);
              var mapOptions = {
                zoom: 14, 
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP, 
                scrollwheel: false,
                disableDoubleClickZoom: true
              }
              map_content = new google.maps.Map(document.getElementById('map'), mapOptions);

                var marker_position= myLatlng;
                var marker = new google.maps.Marker({
                    position: marker_position,
                    icon: '<?php echo base_url()?>assets/images/icon_marker.png',
                    title:'',
                });

              // google.maps.event.addListener(marker, 'mouseover', function() {
                  infowindow_content.setContent('<?php echo $websiteConfig['config_name']?>');
                  infowindow_content.setPosition(myLatlng);     
                  infowindow_content.open(map_content);
            //  });
               // To add the marker to the map, call setMap();
              marker.setMap(map_content);
              map_content.setCenter(marker_position);
            }
            google.maps.event.addDomListener(window, 'load', initialize_);

            function send_message()
            {
              //if(confirm('Send a message?'))
              //{
                $("div.alert").show();
                 var jqxhr=$.ajax({
                  url:'<?php echo base_url()?>ask/service_rest/send_message_visitor',
                  type:'post',
                  dataType:'json',
                  data:$("form#form-message").serializeArray(),
                });
                jqxhr.success(function(response){
                  if(response['status']!=200)
                 {
                  $("div.alert").removeClass('success');
                  $("div.alert").addClass('danger');
                  $("div.alert").html(response['message']);
                 }
                 else
                 {
                    $("div.alert").removeClass('danger');
                    $("div.alert").addClass('success');
                    $("div.alert").html(response['message']);
                    $("form#form-message").find('input[type="text"]').val('');
                    $("form#form-message").find('input[type="email"]').val('');
                    $("form#form-message").find('textarea').val('');
                    setTimeout(function(){
                      $("div.alert").slideUp('medium');
                    },5000);
                 }
                });
                jqxhr.error(function(response){
                 alert('an error has occurred, please try again.');
                });
             // }

              return false;
             
            }
</script>				