<section>
  <div class="container">
    <div class="center wow fadeInDown pb-1">
      <h2 class="special-color mb-1">Video</h2>
    </div>
    <?php
    $w_lib=new widget_lib;
    if (!empty($results)) 
    {
      ?>
      <div class="row wow fadeInLeft">
        <?php
        $no = 0;
        $data_modal = '';
        // var_dump($results);
        foreach($results AS $rowArr)
        {
          foreach($rowArr AS $variable=>$value)
          {
            ${$variable}=$value;
          }
          $no++;
          ?>
          <div class="col-lg-3 col-sm-6  area-video">
            <div class="frame-video">
              <iframe width="560" height="315" src="<?php echo $news_external_link; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <a href="#play-<?php echo $no ?>" data-lightbox="inline" class="layer-video"></a>
            </div>
            <!-- <h3 class="color-primary text-center">Fansindo Best Proses</h3> -->
          </div>
          <?php
          $data_modal .= '
          <div class="modal1 mfp-hide" id="play-'.$no.'">
            <div class="block divcenter" style="background-color: #FFF;">
                <div class="feature-box fbox-center fbox-effect nobottomborder nobottommargin" style="padding: 40px;">
                    <div class="box-frame">
                        <iframe width="560" height="315" src="'.$news_external_link.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
          </div>
          ';
        } 
        ?>
        <div class="col-sm-12 text-right">
          <hr class="hr-dashed mb-0">
          <?php echo $link_pagination; ?>
        </div> 
      </div>
      <?php
      echo $data_modal;
    }else{
      echo '
      <div class="row">
        <div class="col-md-12">
          <h4 class="text-center"><i class="fa fa-danger"></i> Video Saat Ini Belum Tersedia</h4>
        </div>
      </div>
      ';
    }
    ?>
  </div>
</section>