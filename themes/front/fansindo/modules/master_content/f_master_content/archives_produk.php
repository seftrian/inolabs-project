<?php
$w_lib=new widget_lib;
?>

    <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1><?php echo $category_title?></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="#"><?php echo $category_title?></a></li>
                </ol>
            </div>

        </section>

        
        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Portfolio Filter
                    ============================================= -->
                    <ul id="portfolio-filter" class="clearfix">

                        <li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
                        <?php                         
                        if(!empty($list_all_category)){
                          foreach ($list_all_category as $keyArr) {
                            foreach($keyArr AS $variable=>$value)
                            {
                              ${$variable}=$value;
                            }
                            ?>
                                <li><a href="#" data-filter=".<?php echo $category_permalink ?>"><?php echo $category_title ?></a></li>
                            <?php
                          }
                        }
                        ?>                        

                    </ul><!-- #portfolio-filter end -->

                   

                    <div class="clear"></div>

                  <?php
                if(empty($results))
                {
                    ?>
                    <div class="single-post nobottommargin">

                            <!-- Single Post
                            ============================================= -->
                            <div class="entry clearfix">

                                <!-- Entry Title
                                ============================================= -->
                                <div class="entry-title">
                                    <h2><center>Artikel belum dimuat</center></h2>
                                </div><!-- .entry-title end -->
                            
                            </div><!-- .entry end -->
                    </div><!-- .post-navigation end -->
                    
                    <?php
                }
                else
                {
                    ?>

                    <!-- Portfolio Items
                    ============================================= -->
                    <div id="portfolio" class="portfolio-1 clearfix">                    

                  <?php 
                  $keyno = 0;
                   foreach($results AS $rowArr)
                    {
                      foreach($rowArr AS $variable=>$value)
                      {
                        ${$variable}=$value;
                      }

                      $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
                      $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
                      $logo_file=$logo_name.'.'.$logo_ext;
                      $pathImgUrl=$pathImgArr['pathUrl'];
                      $pathImgLoc=$pathImgArr['pathLocation'];
                      $imgProperty=array(
                      'width'=>720,
                      'height'=>400,
                      'imageOriginal'=>$logo_file,
                      'directoryOriginal'=>$pathImgLoc,
                      'directorySave'=>$pathImgLoc.'720400/',
                      'urlSave'=>$pathImgUrl.'720400/',
                      );
                      $imgProperty2=array(
                      'width'=>1140,
                      'height'=>500,
                      'imageOriginal'=>$logo_file,
                      'directoryOriginal'=>$pathImgLoc,
                      'directorySave'=>$pathImgLoc.'1140500/',
                      'urlSave'=>$pathImgUrl.'1140500/',
                      );
                      $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                      $img2=$this->function_lib->resizeImageMoo($imgProperty2,true,true);
                      $dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
                      $link_detail=master_content_lib::link_archives_produk($news_permalink);                      
                      $content=strip_tags(html_entity_decode($news_content));
                      $content=(strlen($content)<300)?$content:substr($content,0,300).'...';
                      $isValidUrl=$this->function_lib->isValidUrl($news_external_link);
                      $link_preview=$isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;
                      $link_to_content=(trim($news_external_link)=='' || $news_external_link=='-')?$link_detail:$link_preview;
                      $link_label=(trim($news_external_link_label)=='' || $news_external_link=='-')?'Lihat Detail':$news_external_link_label;
                      $category_permalink_list=$this->master_content_lib->generate_list_category_permalink($news_id);
                     ?>
                      <article class="portfolio-item <?php echo $category_permalink_list ?>  clearfix">
                          <div class="portfolio-image">
                              <a href="<?php echo $link_to_content ?>">
                                  <img src="<?php echo $img ?>" alt="<?php echo $news_title ?>">
                              </a>
                              <div class="portfolio-overlay">
                                  <a href="<?php echo $img2; ?>" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                  <a href="<?php echo $link_to_content ?>" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                              </div>
                          </div>
                          <div class="portfolio-desc">
                              <h3><a href="<?php echo $link_to_content ?>"><?php echo $news_title ?></a></h3>
                              <span><?php echo $category_article[$keyno]['category_title'] ?></span>
                              <p><?php echo $content ?></p>                                
                              <a href="<?php echo $link_to_content ?>" class="button button-3d noleftmargin"><?php echo $link_label ?></a>
                          </div>
                      </article>

                      <?php
                      $keyno++;
                    }
                  ?>
                    <?php echo $link_pagination?>
                          </div><!-- #portfolio end -->
                      <?php
                }
              ?>    

                    


                </div>

            </div>

        </section><!-- #content end -->

                    <!-- Portfolio Script
                    ============================================= -->
                    <script type="text/javascript">

                        jQuery(window).load(function(){

                            var $container = $('#portfolio');

                            $container.isotope({ transitionDuration: '0.65s' });

                            $('#portfolio-filter a').click(function(){
                                $('#portfolio-filter li').removeClass('activeFilter');
                                $(this).parent('li').addClass('activeFilter');
                                var selector = $(this).attr('data-filter');
                                $container.isotope({ filter: selector });
                                return false;
                            });

                            $('#portfolio-shuffle').click(function(){
                                $container.isotope('updateSortData').isotope({
                                    sortBy: 'random'
                                });
                            });

                            $(window).resize(function() {
                                $container.isotope('layout');
                            });

                        });

                    </script><!-- Portfolio Script End -->