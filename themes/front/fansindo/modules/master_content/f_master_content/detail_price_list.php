<?php
$w_lib=new widget_lib;
if(!empty($row_array))
{ ?>
  <section>
      <div class="container">
    <div class="center wow fadeInDown pb-1">
              <h2 class="special-color mb-1">Detail <span>Produk</span></h2>
          </div>
    <div class="skill-wrap clearfix">
              <div class="row wow fadeInLeft">
                  <div class="col-lg-12 col-sm-12">
                      <a href="<?php echo base_url('archives/price_list') ?>" class="btn btn-round-default color-primary mb-1"><i class="fa fa-chevron-left"></i> Kembali </a>
                      <!-- <a href="produk.html" class="btn-back"><i class="fa fa-chevron-left"></i> Kembali</a> -->
                  </div>
                  <div class="col-lg-5 col-sm-5">
                      <div class="box-produk">
                          <img src="<?php echo base_url($row_array["news_photo"]) ?>" alt="">
                      </div>
                  </div>
                  <div class="col-lg-7 col-sm-7">
                        <div class="box-konten-produk">
                            <div class="frame-title">
                                <h2 class="text-green-2"><?php echo $row_array['news_title']; ?></h2>
                                <h3 class="mt-0 mb-0"><b><?php echo $row_array['news_label_1']; ?></b></h3>
                            </div>
                            <?php echo html_entity_decode($row_array['news_content_2']); ?>
                        </div>
                        <div class="box-price">
                            <h2 class="mt-0 mb-0 text-green-2">Rp. <?php echo $row_array['news_harga']; ?></h2>
                        </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                      <div class="box-deskripsi">
                          <?php echo html_entity_decode($row_array['news_content']); ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <?php
} 
else 
{ ?>
  <div class="content-wrap content-wrap-mobile notopmargin nobottommargin">
    <div class="container-fullwidth clearfix">
      <div class="entry-title">
        <h2><center>Price list belum dimuat</center></h2>
      </div>
    </div>
  </div>
  <?php
} ?>