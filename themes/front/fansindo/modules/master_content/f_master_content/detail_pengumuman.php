<?php
$w_lib=new widget_lib;
if(empty($row_array)){
?>
<!-- Entry Title
  ============================================= -->
  <div class="entry-title">
      <h2>Maaf, artikel belum dimuat.</h2>
  </div><!-- .entry-title end -->
  <?php
  } else {
    //initialize variable
    foreach($row_array AS $variable=>$value)
    {
      ${$variable}=$value;
    }

    $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
    $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
    $logo_file=$logo_name.'.'.$logo_ext;
    $pathImgUrl=$pathImgArr['pathUrl'];
    $pathImgLoc=$pathImgArr['pathLocation'];
    $imgProperty=array(
    'width'=>860,
    'height'=>573,
    'imageOriginal'=>$logo_file,
    'directoryOriginal'=>$pathImgLoc,
    'directorySave'=>$pathImgLoc.'860573/',
    'urlSave'=>$pathImgUrl.'860573/',
    );
    $imgProperty2=array(
    'width'=>100,
    'height'=>75,
    'imageOriginal'=>$logo_file,
    'directoryOriginal'=>$pathImgLoc,
    'directorySave'=>$pathImgLoc.'10075/',
    'urlSave'=>$pathImgUrl.'10075/',
    );
    $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
    $img2=$this->function_lib->resizeImageMoo($imgProperty2,true,true);
    $dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
    $news_content=html_entity_decode($news_content);
  ?>
    <section>
        <div class="container">
      <!-- <div class="center wow fadeInDown pb-1">
                <h2 class="special-color mb-1">Pengumuman <span>FANSINDO</span></h2>
            </div> -->
      <div class="skill-wrap clearfix">
        <div class="row">
                    <div class="col-md-12 wow fadeInLeft">
                        <div class="frame-detail-news">
                            <h3><?php echo ucwords($news_title); ?></h3>
                            <p class="p-date"><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($news_update_datetime)); ?></p>
                            <div class="box-image-pengumuman">
                                <img src="<?php echo base_url().$news_photo; ?>" alt="">
                            </div>
                            <p><?php echo $news_content; ?></p>
                        </div>
                    </div>    
                </div>
            </div>

        </div>
    </section>
  <?php 
} ?>
