<?php
$w_lib=new widget_lib;
if(empty($row_array)){
?>
<!-- Entry Title
============================================= -->
<div class="entry-title">
    <h2>Maaf, artikel belum dimuat.</h2>
</div><!-- .entry-title end -->
<?php
} else {
//initialize variable
  foreach($row_array AS $variable=>$value)
  {
    ${$variable}=$value;
  }
  ?>
<!--title-->
<section id="content" class="">
  <div class="content-wrap wrap-top-only" style="background:url('<?php echo $themes_inc; ?>images/bg/gplaypattern.png'); background-repeat:repeat">
    <div class="container clearfix">
      <h2 class="m-b-0 text-center">Paket <?php echo $news_title; ?></h2>
    </div>
  </div>
</section>
<!--/title-->
<!-- Content
============================================= -->
<section id="content">
  <div class="content-wrap p-t-n p-b-n">
    <div class="container clearfix">
      <div class="panel panel-detail">
        <div class="row">
          <div class="col-md-5">
            <div class="product-image">
              <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                <div class="flexslider">
                  <div class="slider-wrap" data-lightbox="gallery">
                    <?php
                    foreach($news_more_images_arr AS $rowArr)
                    {
                      foreach($rowArr AS $variable=>$value)
                      {
                        ${$variable}=$value;
                      }
                    ?>
                    <div class="slide" data-thumb="<?php echo base_url().$news_more_images_file; ?>">
                      <a href="<?php echo base_url().$news_more_images_file; ?>" title="" data-lightbox="gallery-item">
                        <img src="<?php echo base_url().$news_more_images_file; ?>" alt="">
                      </a>
                    </div>
                  <?php
                  }
                  $list_desc = explode(",",$news_meta_tags);
                  ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <h4 class="m-b-1 text-center m-t-m">Detail</h4>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-12 bg-white-detail">
                  <div class="col-md-3">
                    <b>Nama Paket <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $news_title; ?></div>
                </div>
                <div class="col-md-12 bg-grey-detail">
                  <div class="col-md-3">
                    <b>Jenis Paket <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $news_title; ?></div>
                </div>
                <div class="col-md-12 bg-white-detail">
                  <div class="col-md-3">
                    <b>Musim Umroh <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $list_desc[0]; ?></div>
                </div>
                <div class="col-md-12 bg-grey-detail">
                  <div class="col-md-3">
                    <b>Hotel Makkah <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $list_desc[1]; ?></div>
                </div>
                <div class="col-md-12 bg-white-detail">
                  <div class="col-md-3">
                    <b>Hotel Madinah <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $list_desc[2]; ?></div>
                </div>
                <div class="col-md-12 bg-grey-detail">
                  <div class="col-md-3">
                    <b>Lama Hari <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $list_desc[3]; ?></div>
                </div>
                <div class="col-md-12 bg-white-detail">
                  <div class="col-md-3">
                    <b>Embarkasi <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $list_desc[4]; ?></div>
                </div>
                <div class="col-md-12 bg-grey-detail">
                  <div class="col-md-3">
                    <b>Maskapai <span class="value">:</span></b>
                  </div>
                  <div class="col-md-9"><?php echo $list_desc[4]; ?></div>
                </div>
              </div>
            </div>
            <div class="panel panel-royal m-t-1">
              <div class="col_full text-right m-b-0">
                <h3 class="m-b-0 orange"><?php echo $news_label_1; ?></h3>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <a href="http://sinarkabah.com/live/reg" class="btn btn-success">Pesan Paket</a>
              </div>
              <div class="col-md-7 col-sm-7">
                <p class="text-center m-b-1 p-share m-t-m">Bagikan</p>
                <ul type="none" class="ul-align m-b-0">
                  <li class="li-inline-middle li-share">
                    Bagikan :
                  </li>
                  <li class="li-inline-middle">
                    <a href="https://www.facebook.com/Sinar-Kabah-Semesta-178811122752703" class="round-sosmed-2"><i class="fa fa-facebook"></i></a>
                  </li>
                  <li class="li-inline-middle">
                    <a href="https://api.whatsapp.com/send?phone=087756789234" class="round-sosmed-2"><i class="fa fa-whatsapp"></i></a>
                  </li>
                  <li class="li-inline-middle">
                    <a href="https://www.instagram.com/sinarkabah/" class="round-sosmed-2"><i class="fa fa-instagram"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <?php echo html_entity_decode($news_content); ?>
</section>
<?php
}
?>
