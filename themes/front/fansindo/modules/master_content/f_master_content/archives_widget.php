<?php
$w_lib=new widget_lib;

if(!empty($results)){
?>
<section id="content">
	<div class="content-wrap  p-b-n">
		<div class="container clearfix">
			<div class="row">
        <?php
          foreach ($results as $rowArrFile) {
            $w_lib->run('front', 'master_content', 'widget_content_file', 'file', $rowArrFile);
          }
        ?>
      </div>
   	</div>
	</div>
</section>
<?php
} else {
    ?>
    <div class="content-wrap content-wrap-mobile notopmargin nobottommargin">
        <div class="container-fullwidth clearfix">
              <div class="entry-title">
                  <h2><center>Artikel belum dimuat</center></h2>
              </div>
        </div>
    </div>
    <?php
}
?>
