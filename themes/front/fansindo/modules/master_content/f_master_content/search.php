<?php
$w_lib=new widget_lib;
?>
<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
  
        <!--// Blog Large //-->
        <div class="col-md-8">
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url()?>">Home</a></li>
            <li><a href="#">Pencarian Artikel</a></li>
        </ul>

          <div class="kd-blog-list kd-bloggrid">
            <div class="row">
            	<form method="post" action="<?php echo base_url()?>master_content/search/query" style="text-align: right;">
                <input type="text" name="query" value="<?php echo $category_title?>" style="width:80%">
                <button class="btn btn-primary btn-sm" style="width:18%"><span class="fa fa-search" aria-hidden="true"></span> Cari</button>
                </form>
                <br />
                <p>Ditemukan <b><?php echo $total?></b> artikel.</p>
            	<?php
                if(empty($results))
                {
                		?>
                		 <div class="media">
		                 <p>Maaf, pencarian tidak ditemukan.</p>
		                </div>
                		<?php
                }
                else
                {

                	 foreach($results AS $rowArr)
				            {
				              foreach($rowArr AS $variable=>$value)
				              {
				                ${$variable}=$value;
				              }

				              $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
				              $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
				              $logo_file=$logo_name.'.'.$logo_ext;
				              $pathImgUrl=$pathImgArr['pathUrl'];
				              $pathImgLoc=$pathImgArr['pathLocation'];
				              $imgProperty=array(
				              'width'=>350,
				              'height'=>263,
				              'imageOriginal'=>$logo_file,
				              'directoryOriginal'=>$pathImgLoc,
				              'directorySave'=>$pathImgLoc.'350263/',
				              'urlSave'=>$pathImgUrl.'350263/',
				              );
				              $img=$this->function_lib->resizeImageMoo($imgProperty);
				              $dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
				              $link_detail=master_content_lib::link_detail($news_id, $news_permalink);
				            	$content=strip_tags(html_entity_decode($news_content));
				            	$content=(strlen($content)<170)?$content:substr($content,0,170).'...';
				             ?>
				             	 <article class="col-md-6">
				                <div class="bloginner">
				                  <figure><a href="<?php echo $link_detail?>"><img src="<?php echo $img?>" alt="<?php echo $news_title?>"></a>
				                    <figcaption><a href="<?php echo $link_detail?>" class="fa fa-plus-circle"></a></figcaption>
				                  </figure>
				                  <section class="kd-bloginfo">
				                    <h2><a href="<?php echo $link_detail?>"><?php echo $news_title?></a></h2>
				                    <?php
					                    if(strtolower($category_type)=='article')
					                    {
					                      ?>
					                       <ul class="kd-postoption">
						                      <li><?php echo $category_title?></li>
						                      <li><time datetime="<?php echo $dateformat?>">| <?php echo $dateformat?></time></li>
						                    </ul>
					                      <?php
					                    }
					                    ?>
				                    <p><?php echo $content?></p>
				                    <div class="kd-usernetwork">
				                     <?php
					                    if(strtolower($category_type)=='article')
					                    {
					                      ?>
					                      
						                      <ul class="kd-blogcomment">
						                        <li><a href="#" class="thcolorhover"><i class="fa fa-heart-o"></i> <?php echo master_content_lib::get_number_of_read($news_id)?></a></li>
						                      </ul>
					                      <?php
					                    }
					                    ?>
				                    </div>
				                    
				                  </section>
				                </div>
				              </article>
				              <?php
				            }
                	?>
                
                	<?php
                }
                ?>
            </div>
          </div>
          <div class="pagination-wrap">
            <div class="pagination">
              <center><?php echo $link_pagination?></center>
            </div>
          </div>
        </div>
        <!--// Blog Large //-->

        <aside class="col-md-4">
           <?php 
                $w_lib->run('front', 'master_perawat', 'widget_sidebar_search_perawat');
            ?>
           <?php 
            $w_lib->run('front', 'master_content', 'widget_popular_new');
           ?>

           <?php 
            $w_lib->run('front', 'master_content', 'widget_list_category');
           ?>

          
        </aside>

      </div>

    </div>
  </div>
</section>

        <script type="text/javascript">
        $(function(){
        	$('input[name="query"]').select();
        })
        </script>