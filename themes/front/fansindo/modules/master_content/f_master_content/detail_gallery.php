<?php
$w_lib=new widget_lib;

$logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>1025,
'height'=>650,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'1025650/',
'urlSave'=>$pathImgUrl.'1025650/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);

if(strtolower($logo_ext)=='png' OR strtolower($logo_ext)=='gif')
{
  $img=$pathImgUrl.$logo_file;
}
$dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
$news_content=html_entity_decode($news_content);
$isValidUrl=$this->function_lib->isValidUrl($news_external_link);

$link_preview=$isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;
$link_to_content=(trim($news_external_link)=='')?'':$link_preview;
$link_label=(trim($news_external_link_label)=='')?'':$news_external_link;

$original_image=$pathImgUrl.$logo_file;
?>
<div id="crumbs" class="white">
<div class="row large">
    <div class="four columns">
      <!-- page title -->
      <?php
      if(isset($breadcrumbs))
      {
        echo $breadcrumbs; 
      }
      else
      {
        ?>
        <h6 class="sub-heading"><?php echo strtoupper(isset($seoTitle) ? $seoTitle:$websiteConfig['config_name']); ?></h6>
        <?php                    
      }
      ?>
    </div>
    <div class="eight columns text-right">
      <!-- top on-page nav -->
      <?php
       $w_lib->run('front', 'admin_menu', 'widget_menu_user_header');
       ?>
    </div>
  </div>
</div>

<!-- begin project -->
      <div id="single-project">
      <div class="work grey stripe-bg">
        <!-- project slider -->
        <div class="two-third">
          <div class="single-project">
            <div class="flexslider full-slider">
              <ul class="slides">
                <?php
                     if($news_photo!='-' AND file_exists($pathImgLoc.$logo_file) AND trim($news_photo)!='')
                     {
                        ?>
                           <li>
                            <img src="<?php echo $img?>" alt="<?php echo $news_title?>">
                           </li>
                        <?php
                     }
                   ?>
               
                <?php
                if(!empty($news_more_images_arr))
                {
                  foreach($news_more_images_arr AS $rowArr)
                  {
                    foreach($rowArr AS $variable=>$value)
                    {
                      ${$variable}=$value;
                    }
                     $logo_name= (trim($news_more_images_file)=='')?'-': pathinfo($news_more_images_file,PATHINFO_FILENAME);
                     $logo_ext=  pathinfo($news_more_images_file,PATHINFO_EXTENSION);
                     $logo_file=$logo_name.'.'.$logo_ext;
                     $imgProperty=array(
                      'width'=>1025,
                      'height'=>650,
                      'imageOriginal'=>$logo_file,
                      'directoryOriginal'=>$pathImgLoc,
                      'directorySave'=>$pathImgLoc.'1025650/',
                      'urlSave'=>$pathImgUrl.'1025650/',
                      );
                      $img_more=$this->function_lib->resizeImageMoo($imgProperty);

                      if(strtolower($logo_ext)=='png' OR strtolower($logo_ext)=='gif')
                      {
                        $img_more=$pathImgUrl.$logo_file;
                      }

                      $original_image=$pathImgUrl.$logo_file;
                      if(file_exists($pathImgLoc.$logo_file) AND trim($news_more_images_file)!='')
                      {
                        ?>
                        <li>
                         <img src="<?php echo $img_more?>" alt="<?php echo $news_title?>">
                        </li>
                        <?php
                      }

                  }
                }
                ?>
               
              </ul>
            </div>
          </div>
        </div>
        <!-- .two-third -->
        <!-- project info -->
        <aside class="one-third   black-tint dark text-left">
        <div id="sidebar">
          <div class="project-detail">
            <h5 class="stripe-heading"><?php echo $news_title?></h5>
            <div class="project-content">
              <?php echo $news_content?>
              <ul class="project-meta">
                <li><?php echo function_lib::btn_share_this()?></li>
                <li><i class="fa fa-bookmark"></i><?php echo $dateformat?></li>
                <?php
                      if(!empty($download_arr))
                      {

                        foreach($download_arr AS $rowArr)
                        {
                          foreach($rowArr AS $variable=>$value)
                          {
                            ${$variable}=$value;
                          }
                          $download_title_value=$download_title;
                          $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
                          $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
                          $logo_file=$logo_name.'.'.$logo_ext;
                         
                          $btn_download='<a class="btn btn-sm" title="Download File" onclick="window.location.href=\''. base_url().'master_file/f_master_file/download_file/'.$download_id.'\';return false;"><span class="fa fa-download"></span></a>';
                          $download_icon='';
                          if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
                          {
                              $download_icon=$btn_download;
                              ?>
                              <li><?php echo $download_icon.' '.$download_title?></li>
                              <?php
                          }
                        }
                      }
                  ?>
              </ul>
            </div>
          </div>
        </div>
        </aside>
        <div class="clear">
        </div>
      </div>
      </div>
<aside class="hidden-sidebar grey custom-scroll" id="hidden-sidebar">
    <!-- close sidebar button -->
    <a href="" class="switch close-sidebar" gumby-trigger="|#hidden-sidebar"><i class="fa fa-times"></i></a>
    <!-- 'sidebar-scrollarea creates custom scrollbar if height is larger than  viewport -->
    <div id="sidebar-scrollarea">
      <div id="sidebar">
           <?php 
            $w_lib->run('front', 'master_content', 'widget_list_category');
           ?>
           <?php 
            $w_lib->run('front', 'master_content', 'widget_new_article');
           ?>
          <!-- recent posts widget -->
           <?php 
            $w_lib->run('front', 'master_content', 'widget_popular_new');
           ?>
        </div>
      </div>  
</aside>