<?php
$w_lib=new widget_lib;
// var_dump($results);
$uri_param = $this->uri->segment(2);
if(!empty($results))
{ ?>
  <section>
    <div class="container">
      <div class="center wow fadeInDown pb-1">
        <h2 class="special-color mb-1"><?php echo ucwords(str_replace('_', ' ', $category_permalink)); ?> <span>FANSINDO</span></h2>
      </div>
      <div class="skill-wrap clearfix">
        <div class="row">
        <?php
          $no = 0;
          foreach ($results as $rowArr) 
          {
            $content=html_entity_decode($rowArr['news_content']);
            $label_hr = ($no>0)?'<div class="col-md-12 wow fadeInLeft"><hr class="hr-solid"></div>':'';
            echo $label_hr;
            ?>
            <div class="col-md-12 wow fadeInLeft">
                <div class="frame-news">
                    <div class="box-image-news">
                        <img src="<?php echo base_url($rowArr['news_photo']) ?>" alt="">
                    </div>
                      <div class="box-txt-1">
                        <h3><?php echo ucwords($rowArr['news_title']); ?></h3>
                        <p class="p-date"><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($rowArr['news_update_datetime'])); ?></p>
                        <p><?php echo $content; ?></p>
                        <?php  
                        if ($uri_param=='price_list') {
                          $url_target = 'detail_price_list/'.$rowArr['news_permalink'];
                          $label_button = 'Detail Produk';
                        }
                        elseif ($uri_param=='berita') {
                          $url_target = 'detail_berita/'.$rowArr['news_permalink'];
                          $label_button = 'Detail';
                        }
                        else
                        {
                          $url_target = 'detail/'.$rowArr['news_permalink'];
                          $label_button = 'Detail';
                        }
                        ?>
                        <div class="box-btn-footer-1">
                            <a href="<?php echo base_url($url_target) ?>" class="btn btn-round-default color-primary"><?php echo $label_button; ?> <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>   
            <?php
            $no++;
          } 
          ?>
          <div class="col-md-12 text-right">
            <hr class="hr-dashed mb-0">
            <?php echo $link_pagination; ?>
          </div> 
        </div>
      </div>
    </div>
  </section>
  <?php
} 
else 
{ ?>
  <div class="content-wrap content-wrap-mobile notopmargin nobottommargin">
    <div class="container-fullwidth clearfix">
      <div class="entry-title">
        <h2><center>Artikel belum dimuat</center></h2>
      </div>
    </div>
  </div>
  <?php
} ?>