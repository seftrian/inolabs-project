<?php
$w_lib=new widget_lib;
# about
?>
<section id="about-us">
  <div class="container">
  <?php
  if($category_permalink == "about"){ ?>
  <div class="center wow fadeInDown">
    <h2><marquee loop="100"><?php echo $results_content[0]['news_title']; ?></marquee></h2>
    <p class="lead"><blink><?php echo $results_content[0]['news_label_1']; ?></blink></p>
  </div>
  <?php if (!empty($results_content[0]['news_photo'])) {?>
    <!-- about us slider -->
    <div id="about-slider">
      <div id="carousel-slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
          <ol class="carousel-indicators visible-xs">
            <?php
            for ($i=0; $i < count($results_more_images)+1; $i++) {
            ?>
            <li data-target="#carousel-slider" data-slide-to="<?php echo $i; ?>" <?php if ($i==0) { ?>class="active"<?php } ?>></li>
            <?php 
            }
            ?>
          </ol>

        <div class="carousel-inner">
          <div class="item active">
            <img src="<?php echo $base_url.''.$results_content[0]['news_photo']; ?>" class="img-responsive" alt=""> 
          </div>
          <?php
          foreach ($results_more_images as $rowArr) {
            foreach ($rowArr as $variable => $value) {
                ${$variable}=$value;
            }
            if ($news_more_images_news_id==$results_content[0]['news_id']) {
          ?>
          <div class="item">
            <img src="<?php echo $base_url.''.$news_more_images_file; ?>" class="img-responsive" alt=""> 
          </div>
          <?php
              }
            }
          ?>
        </div>
        
        <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
          <i class="fa fa-angle-left"></i> 
        </a>
        
        <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
          <i class="fa fa-angle-right"></i> 
        </a>
      </div> <!--/#carousel-slider-->
    </div><!--/#about-slider-->
  <?php } ?>
  
  <!-- Our Skill -->
    <div class="skill-wrap clearfix">
      <?php echo html_entity_decode(strip_tags(($results_content[0]['news_content']))); ?>
    </div><!--section-->
  </div><!--/.container-->
</section><!--/about-us-->

<?php
# produk c2fast
}else if($category_permalink == "biogen"){
  $w_lib->run('front', 'master_content', 'widget_biogen');
}else if($category_permalink == "c2fast"){
    $w_lib->run('front', 'master_content', 'widget_c2fast');
}
# kode etik
else if($category_permalink == "kode_etik"){
    $w_lib->run('front', 'master_content', 'widget_kode_etik');
}
# produk gouzi cha
else if($category_permalink == "gouzi_cha"){
    $w_lib->run('front', 'master_content', 'widget_gouzi_cha');
}
# marketing plan
else if($category_permalink == "marketing_plan"){
    $w_lib->run('front', 'master_content', 'widget_marketing_plan');
}
# go car
else if($category_permalink == "go_car"){
    $w_lib->run('front', 'master_content', 'widget_go_car');
}
# go vietnam
else if($category_permalink == "go_vietnam"){
    $w_lib->run('front', 'master_content', 'widget_go_vietnam');
}
# modul fs
else if($category_permalink == "modul_fs"){
    $w_lib->run('front', 'master_content', 'widget_support_system');
}
# jadwal_fs
else if($category_permalink == "jadwal_fs"){
    $w_lib->run('front', 'master_content', 'widget_jadwal_fs');
}
# gallery
else if($category_permalink == "gallery"){
    $w_lib->run('front', 'master_content', 'widget_gallery');
}
# testimonial
else if($category_permalink == "testimonial"){
    $w_lib->run('front', 'master_content', 'widget_testimonial');
}
# download
else if($category_permalink == "download"){
    $w_lib->run('front', 'master_content', 'widget_download');
}
# jika bukan semuanya
else {
  ?>
    <div class="content-wrap content-wrap-mobile notopmargin nobottommargin">
        <div class="container-fullwidth clearfix">
              <div class="entry-title">
                  <h2><center>Artikel belum dimuat</center></h2>
              </div>
        </div>
    </div>
  <?php
  }
  ?>
