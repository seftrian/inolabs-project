<?php
$w_lib=new widget_lib;
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$src_logo= (isset($websiteConfig['config_logo']) AND file_exists(FCPATH.$websiteConfig['config_logo']) AND trim($websiteConfig['config_logo'])!='')?
                                            base_url().$websiteConfig['config_logo']:$themes_inc.'images/logofix1.png';

?>
<div id="crumbs" class="white">
<div class="row large">
    <div class="four columns">
      <!-- page title -->
      <?php
      if(isset($breadcrumbs))
      {
        echo $breadcrumbs; 
      }
      else
      {
        ?>
        <h6 class="sub-heading"><?php echo strtoupper(isset($seoTitle) ? $seoTitle:$websiteConfig['config_name']); ?></h6>
        <?php                    
      }
      ?>
    </div>
    <div class="eight columns text-right">
      <!-- top on-page nav -->
      <?php
       $w_lib->run('front', 'admin_menu', 'widget_menu_user_header');
       ?>
    </div>
  </div>
</div>
  <section class="services white">
			<div class="content-block">
				<!--content heading -->
					<div class="row content-heading text-center">
						<div class="ten columns centered">
							<h6><?php echo strtoupper($current_category_title)?></h6>
							<h3><?php echo (function_lib::remove_html_tag($category_content)!='-')?$category_content:''?></h3>
							<span class="border"></span>
						</div>
					</div>  
             <div class="row">
							<div class="block-nav grey">
								<!-- slider caption buttons - number of nav buttons must be same as number of slides-->
								<?php
								if(!empty($list_category_same_type))
								{

									?>
									<ol class="block-slider-nav" id="block-slider-nav"  style="margin:5px;">
										<li class="nav-button <?php echo ($show=='all')?'flex-active':''?>">
										<a href="<?php echo base_url()?>master_content/archives_tag/<?php echo $category_permalink?>/all/">Semua</a>
										</li>
									<?php
									foreach($list_category_same_type AS $rowArr)
									{
										foreach($rowArr AS $variable=>$value)
										{
											${$variable}=$value;
										}
										$link_by_type=master_content_lib::link_archives_by_category($category_permalink);
									
										?>
										<li class="nav-button  <?php echo ($show=='single' AND $current_category_title==$category_title)?'flex-active':''?>">
											<a href="<?php echo $link_by_type?>"><?php echo $category_title?></a>
										</li>
										<?php
									}
									?>
									</ol>
									<?php
								}
								?>
							

									
								
							</div>
						</div>
						<div class="flexslider block-slider">
 			<?php
                if(empty($results))
                {
                		?>
                		 <div class="content-block nopadding" id="single-post">
												<div class="row">
													<!-- begin post container -->
													<ul class="grid alt-posts" id="posts">
														<!-- begin post -->
														<li class="post">
															<div class="post-info bigtoppadding">
																<!-- title -->
																<h1 class="grid-title"><center>Artikel belum dimuat</center></h1>
															</div>
														</li>
													</ul>
												</div>
											</div>	
                		<?php
                }
                else
                {

                	?>
                	<div class="row large">
						       <!-- begin projects -->
              		<ul class="grid times-three basic" id="posts">

                	<?php	
              	   $pathImgUrl=$pathImgArr['pathUrl'];
			             $pathImgLoc=$pathImgArr['pathLocation'];
                   $default_img=base_url().'assets/images/default.png';

                	 foreach($results AS $rowArr)
				            {
				              foreach($rowArr AS $variable=>$value)
				              {
				                ${$variable}=$value;
				              }

				              $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
				              $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
				              $logo_file=$logo_name.'.'.$logo_ext;
				           
				              $imgProperty=array(
				             'width'=>361,
				              'height'=>280,
				              'imageOriginal'=>$logo_file,
				              'directoryOriginal'=>$pathImgLoc,
				              'directorySave'=>$pathImgLoc.'361280/',
				              'urlSave'=>$pathImgUrl.'361280/',
				              );
				              //$img=$this->function_lib->resizeImageMoo($imgProperty);
				              $dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
				              $link_detail=master_content_lib::link_detail($news_id, $news_permalink);
				            	$content=strip_tags(html_entity_decode($news_content));
				            	$content=(strlen($content)<170)?$content:substr($content,0,170).'...';

				            	$original_image=$pathImgUrl.$logo_file;
				            	if(strtolower($logo_ext)!='jpg' AND strtolower($logo_ext)!='jpeg')
				            	{
				            		$img=$original_image;
				            	}
				            	$img=$original_image;
				             ?>
			                  	   <li class="grid-item" >
			                  	   		<div class="grid-post">
			                  	   			<?php
			                  	   			if(trim($news_photo)!='' AND trim($news_photo)!='-')
			                  	   			{
			                  	   				?>
			                  	   				 <!-- project image -->
														          <div class="img-box">
														            <a  href="javascript:void(0)" onclick="load_modal_detail('<?php echo $news_title?>','<?php echo $news_permalink?>');return false;" class="switch" gumby-trigger="#medium-modal1">
														            <img style="height:280px;" src="<?php echo $img?>" alt="<?php echo $news_title?>" class="zoom-on-hover">
														             <!--<img style="height:280px;" class="lazy-load" data-original="<?php echo $img?>" alt="<?php echo $news_title?>" />-->
														            </a>
															          <div class="icon-box  black-tint dark text-left">
															            	<span class="bold"><?php //echo $news_title?></span><br />
															              <span style="margin:10px;"><?php //echo $news_label_1?></span>
															           		<br />
															           		<a class="do_popup" rel="prettyPhoto[pp_gal]"  href="<?php echo $original_image?>" title="<?php echo $news_title?>" alt="<?php echo $news_title?>"><i class="fa fa-plus" title="View"></i></a>
															           		<a  href="javascript:void(0)" onclick="load_modal_detail('<?php echo $news_title?>','<?php echo $news_permalink?>');return false;" class="switch" gumby-trigger="#medium-modal1"><i class="fa fa-share" title="Share"></i></a>
															          </div>
														          </div>
			                  	   				<?php
			                  	   			}	
			                  	   			else
			                  	   			{
			                  	   				?>
			                  	   				 <!--blog post info -->
													          <div class="post-info">
													            <a href="javascript:void(0)" onclick="load_modal_detail('<?php echo $news_title?>','<?php echo $news_permalink?>');return false;"  title="<?php echo $news_title?>" class="switch" gumby-trigger="#medium-modal1"><h1 class="grid-title"><?php echo $news_title?></h1></a>
													            <p>
													              <?php echo $news_label_1?>
													            </p>
													            <div style="text-align:right;"><a href="javascript:void(0)" onclick="load_modal_detail('<?php echo $news_title?>','<?php echo $news_permalink?>');return false;" class="switch" gumby-trigger="#medium-modal1"><i class="fa fa-share"></i></a></div>
													          </div>
			                  	   				<?php
			                  	   			}
			                  	   			?>
												         
												        </div>
											       </li>
				           
				              <?php
				            }
                	?>
                	</ul>
                	</div>
                	<?php
                }
        ?>
        </div>
      <!-- clear floats -->
      <div class="clear">
      </div>
      </div>
</section>


      <!-- pagination -->
      <div class="post-nav grey">
        <div class="row large">
          <div class="eight columns">
          <?php echo $link_pagination?>
          </div>          
        </div>
      </div>

           <?php 
           // $w_lib->run('front', 'master_content', 'widget_popular_new');
           ?>

       

<aside class="hidden-sidebar grey custom-scroll" id="hidden-sidebar">
    <!-- close sidebar button -->
    <a href="" class="switch close-sidebar" gumby-trigger="|#hidden-sidebar"><i class="fa fa-times"></i></a>
    <!-- 'sidebar-scrollarea creates custom scrollbar if height is larger than  viewport -->
    <div id="sidebar-scrollarea">
      <div id="sidebar">
           <?php 
            $w_lib->run('front', 'master_content', 'widget_list_category');
           ?>
           <?php 
            $w_lib->run('front', 'master_content', 'widget_new_article');
           ?>
          <!-- recent posts widget -->
           <?php 
            $w_lib->run('front', 'master_content', 'widget_popular_new');
           ?>
        </div>
      </div>  
</aside>

<!-- modal id is same as gumby-trigger -->
	<div class="modal large" id="medium-modal1">
	  <div class="content">
	  	<div class="modal-header">
			<!-- close button for this modal id -->
				<a class="close switch" gumby-trigger="|#medium-modal1"><i class="fa fa-times" /></i></a>
				<!-- modal heading -->
			<h5 id="body-modal-title"></h5>
			</div>
			<!-- modal content -->
			<div class="modal-body">
				<div class="row" id="body-modal-content" style="width:100%;min-height:400px;">
					
					
				</div>
			</div>	
			<div class="modal-footer">
		<!-- close button for this modal id -->
			<p class="btn info small"  style="text-align:left;display:none;">
			  <a href="#" class="switch" gumby-trigger="|#medium-modal1" >Tutup</a>
			</p>
		 </div>
	  </div>
	</div>

<script type="text/javascript">
/*function load_modal_detail(news_title, news_permalink)
{
	//$("#medium-modal1").modal('show');
	var div_content=$("#body-modal-content");
	$("#body-modal-title").html(news_title);
	div_content.slideUp('fast',function(){
		div_content.html('loading...');
	});
	var jqxhr=$.ajax({
		url:'<?php echo base_url()?>master_content/detail_ajax/'+news_permalink,
		dataType:'html',
		type:'get',
	});
	jqxhr.success(function(response){
		div_content.slideDown('medium',function(){
			div_content.html(response);
		});
	});
}
*/
function load_modal_detail(news_title, news_permalink)
{
	//$("#medium-modal1").modal('show');


	$("#body-modal-title").html(news_title);
	var div_content=$("#body-modal-content");
	div_content.html('<iframe src="<?php echo base_url()?>master_content/detail_ajax/'+news_permalink+'" frameborder="0" style="width:100%;min-height:400px;"></iframe>');

	/*
	var jqxhr=$.ajax({
		url:'<?php echo base_url()?>master_content/detail_ajax/'+news_permalink,
		dataType:'html',
		type:'get',
	});
	jqxhr.success(function(response){
		div_content.slideDown('medium',function(){
			div_content.html(response);
		});
	});
*/
}

</script>