<?php
$w_lib=new widget_lib;

$logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>1140,
'height'=>400,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'1140400/',
'urlSave'=>$pathImgUrl.'1140400/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);

if(strtolower($logo_ext)=='png' OR strtolower($logo_ext)=='gif')
{
  $img=$pathImgUrl.$logo_file;
}
$dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
$news_content=html_entity_decode($news_content);
$isValidUrl=$this->function_lib->isValidUrl($news_external_link);

$link_preview=$isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;
$link_to_content=(trim($news_external_link)=='-')?'':$link_preview;
$link_label=(trim($news_external_link_label)=='-')?'':$news_external_link_label;

$pathImgUrl_category=$pathImgArr_category['pathUrl'];
$pathImgLoc_category=$pathImgArr_category['pathLocation'];
$logo_name_category= (trim($category_photo)=='')?'-': pathinfo($category_photo,PATHINFO_FILENAME);
$logo_ext_category=  pathinfo($category_photo,PATHINFO_EXTENSION);
$logo_file_category=$logo_name_category.'.'.$logo_ext_category;

$original_image=$pathImgUrl.$logo_file;
?>

        <!-- Page Title
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <h1><?php echo $news_title; ?></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>">Home</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#"><?php echo $news_title; ?></a></li>
                </ol>
            </div>

        </section>


        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <div class="col_full">

                      <p><?php echo (function_lib::remove_html_tag($news_content)!='-')?$news_content:''?></p>

                      <!-- Tag Cloud
                                    ============================================= -->
                                    <div class="col_full clearfix bottommargin">
                        <?php
                        if(!empty($download_arr))
                          {

                            foreach($download_arr AS $rowArr)
                            {
                              foreach($rowArr AS $variable=>$value)
                              {
                                ${$variable}=$value;
                              }
                              $download_title_value=$download_title;
                              $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
                              $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
                              $logo_file=$logo_name.'.'.$logo_ext;
                             
                              $btn_download='<a class="button button-3d button-rounded button-green" title="Download File" onclick="window.location.href=\''. base_url().'master_file/f_master_file/download_file/'.$download_id.'\';return false;"><i class="icon-download"></i> &nbsp;'.$download_title.'</a>';
                              $download_icon='';
                              if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
                              {
                                  $download_icon=$btn_download;
                                  ?>
                                  <?php echo $download_icon?>
                                  <?php
                              }
                            }
                          }
                          ?>
                          </div><!-- .tagcloud end -->

                      <div class="clear"></div>

                    </div>


                    <div class="clear"></div>

                </div>


            </div>

        </section><!-- #content end -->
