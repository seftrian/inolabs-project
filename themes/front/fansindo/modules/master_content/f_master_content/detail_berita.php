<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5be41e2f1f57bb64"></script>
<?php
$w_lib=new widget_lib;
if(empty($row_array)){
?>
<!-- Entry Title
  ============================================= -->
  <div class="entry-title">
      <h2>Maaf, artikel belum dimuat.</h2>
  </div><!-- .entry-title end -->
  <?php
  } else {
    //initialize variable
    foreach($row_array AS $variable=>$value)
    {
      ${$variable}=$value;
    }

    $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
    $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
    $logo_file=$logo_name.'.'.$logo_ext;
    $pathImgUrl=$pathImgArr['pathUrl'];
    $pathImgLoc=$pathImgArr['pathLocation'];
    $imgProperty=array(
    'width'=>860,
    'height'=>573,
    'imageOriginal'=>$logo_file,
    'directoryOriginal'=>$pathImgLoc,
    'directorySave'=>$pathImgLoc.'860573/',
    'urlSave'=>$pathImgUrl.'860573/',
    );
    $imgProperty2=array(
    'width'=>100,
    'height'=>75,
    'imageOriginal'=>$logo_file,
    'directoryOriginal'=>$pathImgLoc,
    'directorySave'=>$pathImgLoc.'10075/',
    'urlSave'=>$pathImgUrl.'10075/',
    );
    $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
    $img2=$this->function_lib->resizeImageMoo($imgProperty2,true,true);
    $dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
    $news_content=html_entity_decode($news_content);
    // var_dump($news_more_images_arr);
  ?>
  <div class="breadcrumb">
      <div class="container">
          <a href="<?php echo base_url('archives/'.$category_row['category_permalink']) ?>" class=""><b><?php echo $category_row['category_title']; ?></b></a> <i class="fa fa-caret-right"></i> <?php echo ucwords($news_title); ?>
      </div>
  </div>
  
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 wow fadeInLeft">
          <div class="frame-detail-news">
              <h3><?php echo ucwords($news_title); ?></h3>
              <p class="p-date"><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($news_update_datetime)); ?></p>
              <div class="box-image-detail-news">
                  <img src="<?php echo base_url().$news_photo; ?>" alt="">
              </div>
              <p><?php echo $news_content; ?></p>
          </div>
        </div>  
        <?php 
        if (!empty($news_more_images_arr)) 
        { 
          foreach ($news_more_images_arr as $rowArr) 
          {
            ?>
            <div class="portfolio-item portfolio-custom col-sm-4 col-lg-4">
              <div class="recent-work-wrap">
                  <img class="img-responsive" src="<?php echo base_url($rowArr['news_more_images_file']); ?>" alt="">
                  <div class="overlay">
                    <div class="recent-work-inner">
                      <h3><a href="#">Gambar <?php echo $i; ?></a></h3>
                      <a class="preview" href="<?php echo base_url($rowArr['news_more_images_file']); ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                    </div> 
                  </div>
              </div>
            </div><!--/.portfolio-item-->
            <?php 
          } 
        } ?>
      </div>
    </div>
  </section>
  <?php 
} ?>
