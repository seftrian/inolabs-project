<?php
$w_lib=new widget_lib;

$logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>1025,
'height'=>650,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'1025650/',
'urlSave'=>$pathImgUrl.'1025650/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);

if(strtolower($logo_ext)=='png' OR strtolower($logo_ext)=='gif')
{
  $img=$pathImgUrl.$logo_file;
}
$dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
$news_content=html_entity_decode($news_content);
$isValidUrl=$this->function_lib->isValidUrl($news_external_link);

$link_preview=$isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;
$link_to_content=(trim($news_external_link)=='-')?'':$link_preview;
$link_label=(trim($news_external_link_label)=='-')?'':$news_external_link_label;

$pathImgUrl_category=$pathImgArr_category['pathUrl'];
$pathImgLoc_category=$pathImgArr_category['pathLocation'];
$logo_name_category= (trim($category_photo)=='')?'-': pathinfo($category_photo,PATHINFO_FILENAME);
$logo_ext_category=  pathinfo($category_photo,PATHINFO_EXTENSION);
$logo_file_category=$logo_name_category.'.'.$logo_ext_category;
$img_category='';

if(file_exists($pathImgLoc_category.$logo_file_category) AND trim($category_photo)!='')
{

    
   
    /*$imgProperty_category=array(
    'width'=>1550,
    'height'=>600,
    'imageOriginal'=>$logo_file_category,
    'directoryOriginal'=>$pathImgLoc_category,
    'directorySave'=>$pathImgLoc_category.'1550600/',
    'urlSave'=>$pathImgUrl_category.'1550600/',
    );
    */
    $img_category=$pathImgUrl_category.$logo_file_category;
}
$original_image=$pathImgUrl.$logo_file;

$img_background_style=(trim($img_category)!='')?'background-image:url('.$original_image.');':'';
?>
<div id="crumbs" class="white">
<div class="row large">
    <div class="four columns">
      <!-- page title -->
     
    </div>
    <div class="eight columns text-right">
      <!-- top on-page nav -->
      <?php
       $w_lib->run('front', 'admin_menu', 'widget_menu_user_header');
       ?>
    </div>
  </div>
</div>
<!-- begin project -->
<section id="single-project" class="equals">
  <div class="white">
    <div class="project-detail">
      <div class="row content-heading text-center">
            <div class="ten columns centered">
              <h6><?php echo $news_title?></h6>
              <h3> <?php echo ($news_label_1!='-')?$news_label_1:''?></h3>
              <span class="border"></span>
            </div>
          </div>  
      <div class="row ">
     
               <?php
        if(!empty($list_tab_content))
        {
        ?>
           <div class="six columns">
                          <?php
                            $no=1;
                            foreach($list_tab_content AS $rowArr)
                            {
                              foreach($rowArr AS $variable=>$value)
                              {
                                ${$variable}=$value;
                              }

                              ?>

                          <!-- drawer handle/ title / 1-->
                          <div class="drawer-title">
                            <!-- gumby-trigger should target the id of drawer-content -->
                            <a href="#" onclick="toggle_drawer($(this));" class="toggle" gumby-trigger="#drawer<?php echo $tab_id?>"><?php echo htmlspecialchars_decode($tab_title)?></a>
                          </div>
                          <!-- drawer content / 1 -->
                          <div class="drawer drawer-content " id="drawer<?php echo $tab_id?>">
                            <div class="content-block">
                              <div class="row">
                                <div class="twelve columns">
                                  <p>
                                    <?php echo htmlspecialchars_decode($tab_content)?>
                                  </p>
                                </div>
                            
                              </div>
                            </div>
                          </div>
                      <?php
                      $no++;
                        }
                        ?>
             <div class="clear"></div>
             <div class="clear"></div>
             <br />
             <br />
             <br />
        </div>
                        <?php
                  }
                  ?>

        <div class="five columns redmaroon white-text">
           <div  style="margin:10px;">
           <?php echo (function_lib::remove_html_tag($news_content)!='-')?$news_content:''?>

          <ul class="project-meta purple-text">
           <li> <?php echo function_lib::btn_share_this()?> </li>
          <?php
          if($category_type!='static')
          {
            ?>
            <li><i class="fa fa-bookmark"></i><?php echo convert_datetime_name($news_timestamp,'id')?></li>
            <?php
          }
          ?>
          <?php
          $str_content='';
          
          if(!empty($download_arr))
          {

            foreach($download_arr AS $rowArr)
            {
              foreach($rowArr AS $variable=>$value)
              {
                ${$variable}=$value;
              }
              $download_title_value=$download_title;
              $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
              $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
              $logo_file=$logo_name.'.'.$logo_ext;
             
              $btn_download='<a class="btn btn-sm" title="Download File" onclick="window.location.href=\''. base_url().'master_file/f_master_file/download_file/'.$download_id.'\';return false;">Download '.$download_title.'</a>';
              $download_icon='';
              if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
              {
                  $download_icon=$btn_download;
                  ?>
                  <li><i class="fa fa-download"></i><?php echo $download_icon?></li><br />
                  <?php
              }
            }
          }
          ?>
        </ul>
        </div>
        </div>
        

     

        </div>
        
    </div>
  </div>


       

  <!-- project slider -->
          <div class="slider-box">
            <div class="flexslider full-slider">
              <ul class="slides">
                    <?php
                     if($news_photo!='-' AND file_exists($pathImgLoc.$logo_file) AND trim($news_photo)!='')
                     {
                        ?>
                           <li>
                            <img src="<?php echo $img?>" alt="<?php echo $news_title?>">
                           </li>
                        <?php
                     }
                   ?>
                    
                    <?php
                    if(!empty($news_more_images_arr))
                    {
                      foreach($news_more_images_arr AS $rowArr)
                      {
                        foreach($rowArr AS $variable=>$value)
                        {
                          ${$variable}=$value;
                        }
                         $logo_name= (trim($news_more_images_file)=='')?'-': pathinfo($news_more_images_file,PATHINFO_FILENAME);
                         $logo_ext=  pathinfo($news_more_images_file,PATHINFO_EXTENSION);
                         $logo_file=$logo_name.'.'.$logo_ext;
                         $imgProperty=array(
                          'width'=>1025,
                          'height'=>650,
                          'imageOriginal'=>$logo_file,
                          'directoryOriginal'=>$pathImgLoc,
                          'directorySave'=>$pathImgLoc.'1025650/',
                          'urlSave'=>$pathImgUrl.'1025650/',
                          );
                          $img_more=$this->function_lib->resizeImageMoo($imgProperty);

                          if(strtolower($logo_ext)=='png' OR strtolower($logo_ext)=='gif')
                          {
                            $img_more=$pathImgUrl.$logo_file;
                          }
                          $original_image=$pathImgUrl.$logo_file;

                        if(file_exists($pathImgLoc.$logo_file) AND trim($news_more_images_file)!='')
                        {
                          ?>
                          <li>
                           <img src="<?php echo $img_more?>" alt="<?php echo $news_title?>">
                          </li>
                          <?php
                        }
                      }
                    }
                    ?>

              </ul>
            </div>
          </div>


</section>


<aside class="hidden-sidebar grey custom-scroll" id="hidden-sidebar">
    <!-- close sidebar button -->
    <a href="" class="switch close-sidebar" gumby-trigger="|#hidden-sidebar"><i class="fa fa-times"></i></a>
    <!-- 'sidebar-scrollarea creates custom scrollbar if height is larger than  viewport -->
    <div id="sidebar-scrollarea">
      <div id="sidebar">
           <?php 
            $w_lib->run('front', 'master_content', 'widget_list_category');
           ?>
           <?php 
            $w_lib->run('front', 'master_content', 'widget_new_article');
           ?>
          <!-- recent posts widget -->
           <?php 
            $w_lib->run('front', 'master_content', 'widget_popular_new');
           ?>
        </div>
      </div>  
</aside>

<script type="text/javascript">
function toggle_drawer(elem)
{
   /* $(".drawer-title").find('a').removeClass('active');
    $(".drawer-content").removeClass('active');

    var parent=elem.parent();
    
    parent.find(".drawer-title").find('a').addClass('active');
    parent.find(".drawer-content").addClass('active');
    */
}
</script>