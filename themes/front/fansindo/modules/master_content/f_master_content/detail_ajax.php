<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$w_lib=new widget_lib;
$uri_1=$this->uri->segment(1);
$uri_2=$this->uri->segment(2);
$uri_3=$this->uri->segment(3);
$src_logo= (isset($websiteConfig['config_logo']) AND file_exists(FCPATH.$websiteConfig['config_logo']) AND trim($websiteConfig['config_logo'])!='')?
                                            base_url().$websiteConfig['config_logo']:$themes_inc.'images/logofix1.png';
$favicon_logo= (isset($websiteConfig['config_favicon']) AND file_exists(FCPATH.$websiteConfig['config_favicon']) AND trim($websiteConfig['config_favicon'])!='')?
                                            base_url().$websiteConfig['config_favicon']:$themes_inc.'images/logofix1.png';

?>

                 <?php
                  if(empty($row_array))
                  {
                    ?>
                    <p>Maaf, artikel belum dimuat.</p>
                    <?php
                  }
                  else
                  {
                    //initialize variable
                    foreach($row_array AS $variable=>$value)
                    {
                      ${$variable}=$value;
                    }

                    $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
                    $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
                    $logo_file=$logo_name.'.'.$logo_ext;
                    $pathImgUrl=$pathImgArr['pathUrl'];
                    $pathImgLoc=$pathImgArr['pathLocation'];
                    $imgProperty=array(
                    'width'=>440,
                    'height'=>282,
                    'imageOriginal'=>$logo_file,
                    'directoryOriginal'=>$pathImgLoc,
                    'directorySave'=>$pathImgLoc.'440282/',
                    'urlSave'=>$pathImgUrl.'440282/',
                    );
                    $img=$this->function_lib->resizeImageMoo($imgProperty);
                    $dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
                    $news_content=html_entity_decode($news_content);
                    $original_image=$pathImgUrl.$logo_file;
                    if(strtolower($logo_ext)!='jpg' AND strtolower($logo_ext)!='jpeg')
                    {
                      $img=$original_image;
                    }
                      $img_content=$img=$original_image;
                    ?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product">
<!--<![endif]-->
<head>
<!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="X-UA-Compatible" content="text/html; charset=UTF-8" />

<title><?php echo isset($seoTitle) ? $seoTitle:$websiteConfig['config_name']; ?></title>
<meta name="description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>">
<meta name="keywords" content="<?php echo function_lib::remove_html_tag((isset($seoKeywords) AND trim($seoKeywords)!='')?$seoKeywords:$websiteConfig['config_keywords']);?>">

<meta property="og:site_name" content="<?php echo isset($seoTitle)?$websiteConfig['config_name'].' - '.$seoTitle:$websiteConfig['config_name'];?>" />
<meta property="og:title" content="<?php echo isset($seoTitle)?$websiteConfig['config_name'].' - '.$seoTitle:$websiteConfig['config_name'];?>" />
<meta property="og:image" content="<?php echo (isset($img_content) AND trim($img_content)!='')?$img_content:$src_logo?>" />
<meta property="og:description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>" />
<meta property="og:url" content="<?php echo isset($current_url)?$current_url:base_url()?>" />

<meta property="og:image:type" content="image/jpeg" /> 
<meta property="og:image:width" content="650" /> 
<meta property="og:image:height" content="366" />

<meta name="thumbnailUrl" content="<?php echo isset($img_content)?$img_content:$src_logo?>" itemprop="thumbnailUrl" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="<?php echo $websiteConfig['config_name']?>" />
<meta name="twitter:site:id" content="<?php echo $websiteConfig['config_name']?>" />
<meta name="twitter:creator" content="<?php echo $websiteConfig['config_name']?>" />
<meta name="twitter:description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) AND trim($seoDesc)!='')?$seoDesc:$websiteConfig['config_description']);?>" />
<meta name="twitter:image:src" content="<?php echo (isset($img_content) AND trim($img_content)!='')?$img_content:$src_logo?>" />

<link rel="shortcut icon" href="<?php echo (isset($websiteConfig['config_favicon']) AND file_exists(FCPATH . $websiteConfig['config_favicon']) AND trim($websiteConfig['config_favicon']) != '')?base_url() . $websiteConfig['config_favicon']:''?>" />

<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

</head>
<body>


                     <!-- post  img header -->
                     <div class="twelve columns centered">
                      <center><?php echo function_lib::btn_share_this()?></center>
                     </div>
                     <?php
                     if($news_photo!='' AND $news_photo!='-')
                     {
                        ?>
                          <div class="twelve columns centered">
                            <img  style="width:100%;" src="<?php echo $img?>" alt="<?php echo $news_title?>" class="block-img">
                          </div>    
                        <?php
                     }
                     ?>

                      <div class="clear-20"></div>
                      <div class="twelve columns alpha">
                        <span class="bold"><?php echo ($news_label_1!='-')?function_lib::remove_html_tag($news_label_1):''?></span>
                        <div class="clear-20"></div>

                        <p><?php echo (function_lib::remove_html_tag($news_content)!='-')?$news_content:''?></p>
                      
                      <?php
                      if(strtolower($category_type)=='article')
                      {
                        ?>
                         <ul class="project-meta">
                          <li><span>Posted on: <span><?php echo isset($row_array['news_timestamp'])?convert_datetime_name($row_array['news_timestamp'],'id'):'n/a'?></span></span></li>
                          <li><span>Posted by: <span><?php echo $news_input_by?></span></span></li>
                          <li><span>Read: <span><?php echo master_content_lib::get_number_of_read($news_id)?>x</span></span></li>
                         </ul>
                        <?php
                      }
                      ?>
                      </div>
                    
                     
                      
                      <?php
                      if(!empty($download_arr))
                      {

                        foreach($download_arr AS $rowArr)
                        {
                          foreach($rowArr AS $variable=>$value)
                          {
                            ${$variable}=$value;
                          }
                          $download_title_value=$download_title;
                          $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
                          $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
                          $logo_file=$logo_name.'.'.$logo_ext;
                         
                          $btn_download='<a class="btn btn-sm" title="Download File" onclick="window.location.href=\''. base_url().'master_file/f_master_file/download_file/'.$download_id.'\';return false;"><span class="fa fa-download"></span></a>';
                          $download_icon='';
                          if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
                          {
                              $download_icon=$btn_download;
                              ?>
                              <?php echo $download_icon.' '.$download_title?><br />
                              <?php
                          }
                        }
                      }

?>
                     
</body>
</html>

<?php
                     
                  }
                  ?>

      