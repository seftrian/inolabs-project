<?php
$w_lib=new widget_lib;

$logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>860,
'height'=>573,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'860573/',
'urlSave'=>$pathImgUrl.'860573/',
);
$imgProperty2=array(
'width'=>100,
'height'=>75,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'10075/',
'urlSave'=>$pathImgUrl.'10075/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
$img2=$this->function_lib->resizeImageMoo($imgProperty2,true,true);

if(strtolower($logo_ext)=='png' OR strtolower($logo_ext)=='gif')
{
  $img=$pathImgUrl.$logo_file;
}
$dateformat=convert_datetime_name($news_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
$news_content=html_entity_decode($news_content);
$isValidUrl=$this->function_lib->isValidUrl($news_external_link);

$link_preview=$isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;
$link_to_content=(trim($news_external_link)=='-')?'':$link_preview;
$link_label=(trim($news_external_link_label)=='-')?'':$news_external_link_label;

?>

       <!-- Page Title
        ============================================= -->
      <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Project</h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="<?php echo base_url() ?>master_content/archives_produk/project">Project</a></li>                    
                </ol>
            </div>

        </section>


        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

        <div class="container clearfix">

          <?php
                if(!empty($news_more_images_arr))
                {
                  ?>
                                <!-- Entry Image
                                ============================================= -->
                                <div class="col_two_third portfolio-single-image nobottommargin">
                                    <div class="fslider" data-arrows="false" data-thumbs="true" data-animation="fade">
                                        <div class="flexslider">
                                            <div class="slider-wrap">
                                                <div class="slide" data-thumb="<?php echo $img2 ?>"><img src="<?php echo $img ?>" alt="<?php echo $news_title ?>"></div>
                                    <?php
                  foreach($news_more_images_arr AS $rowArr)
                  {
                    foreach($rowArr AS $variable=>$value)
                    {
                      ${$variable}=$value;
                    }
                     $logo_name= (trim($news_more_images_file)=='')?'-': pathinfo($news_more_images_file,PATHINFO_FILENAME);
                     $logo_ext=  pathinfo($news_more_images_file,PATHINFO_EXTENSION);
                     $logo_file=$logo_name.'.'.$logo_ext;
                     $imgProperty=array(
                      'width'=>860,
                      'height'=>573,
                      'imageOriginal'=>$logo_file,
                      'directoryOriginal'=>$pathImgLoc,
                      'directorySave'=>$pathImgLoc.'860573/',
                      'urlSave'=>$pathImgUrl.'860573/',
                      );
                     $imgProperty2=array(
                      'width'=>100,
                      'height'=>75,
                      'imageOriginal'=>$logo_file,
                      'directoryOriginal'=>$pathImgLoc,
                      'directorySave'=>$pathImgLoc.'10075/',
                      'urlSave'=>$pathImgUrl.'10075/',
                      );
                      $img_more=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                      $img_more2=$this->function_lib->resizeImageMoo($imgProperty2,true,true);

                      if(strtolower($logo_ext)=='png' OR strtolower($logo_ext)=='gif')
                      {
                        $img_more=$pathImgUrl.$logo_file;
                      }
                      $original_image=$pathImgUrl.$logo_file;

                      if(file_exists($pathImgLoc.$logo_file) AND trim($news_more_images_file)!='')
                      {
                        ?>
                        <div class="slide" data-thumb="<?php echo $img_more2 ?>"><img src="<?php echo $img_more ?>" alt="<?php echo $news_title ?>"></div>
                        <?php
                      }
                  }
                  ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                   <?php
                } else {
                ?>

          <!-- Portfolio Single Image
          ============================================= -->
          <div class="col_two_third portfolio-single-image nobottommargin">
            <img src="<?php echo $img ?>" alt="<?php echo $news_title ?>">            
          </div><!-- .portfolio-single-image end -->

          <?php
          } 
          ?>


                      

          <!-- Portfolio Single Content
          ============================================= -->
          <div class="col_one_third portfolio-single-content col_last nobottommargin">

            <!-- Portfolio Single - Description
            ============================================= -->
            <div class="fancy-title title-bottom-border nobottommargin">
              <h2><?php echo $news_title ?></h2>            
            </div>            
            <div class="portfolio-desc">
              <span><?php echo $list_category_produk['category_title']; ?></span>
            </div>
            <p><?php echo $news_content ?></p>            
            <!-- Portfolio Single - Description End -->               

            <!-- Tag Cloud
            ============================================= -->
            <div class="col_full clearfix bottommargin">
            <?php
          if(!empty($download_arr))
          {

            foreach($download_arr AS $rowArr)
            {
              foreach($rowArr AS $variable=>$value)
              {
                ${$variable}=$value;
              }
              $download_title_value=$download_title;
              $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
              $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
              $logo_file=$logo_name.'.'.$logo_ext;
             
              $btn_download='<a class="button button-3d button-rounded button-green" title="Download File" onclick="window.location.href=\''. base_url().'master_file/f_master_file/download_file/'.$download_id.'\';return false;"><i class="icon-download"></i> &nbsp; '.$download_title.'</a>';
              $download_icon='';
              if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
              {
                  $download_icon=$btn_download;
                  ?>
                  <?php echo $download_icon?>
                  <?php
              }
            }
          }
          ?>
          </div>

           <?php
                      if(!empty($list_tab_content))
                      {
                      ?>
                        <div class="tabs tabs-justify nobottommargin clearfix ui-tabs ui-widget ui-widget-content ui-corner-all" id="construction-tabs">

                          <ul class="tab-nav clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                    <?php
                                    $no=1;
                                    foreach($list_tab_content AS $rowArr)
                                    {
                                      foreach($rowArr AS $variable=>$value)
                                      {
                                        ${$variable}=$value;
                                      }
                                      $tabs_active=($no==1)?'ui-tabs-active ui-state-active':'';
                                      ?>
                                       <li class="ui-state-default <?php echo $tabs_active?>" role="tab" tabindex="0" aria-controls="content_tab_<?php echo $tab_id?>"><a href="#content_tab_<?php echo $tab_id?>"><?php echo $tab_title?></a></li>
                                    <?php
                                      $no++;
                                    }
                                    ?>
                          </ul>

                          <div class="tab-container">
                                    <?php
                                    $no=1;
                                    foreach($list_tab_content AS $rowArr)
                                    {
                                      foreach($rowArr AS $variable=>$value)
                                      {
                                        ${$variable}=$value;
                                      }
                                      $tabs_active=($no==1)?' class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="content_tab_'.$tab_id.'" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false" style="display: block;"':'id="content_tab_'.$tab_id.'"';
                                      ?>
                                        <div <?php echo $tabs_active?>>
                                          <p><?php echo html_entity_decode($tab_content)?>
                                          </p>
                                        </div>

                                    <?php
                                      $no++;
                                    }
                                    ?>
                           

                          </div>

                        </div>

                                      <?php
                                }
                                ?>

            <!-- Portfolio Single - Share
            ============================================= -->
            <div class="si-share clearfix">
              <span>Share:</span>
              <div style="margin-top:10px">
                <?php echo function_lib::btn_share_this()?>                
              </div>
            </div>
            <!-- Portfolio Single - Share End -->

          </div><!-- .portfolio-single-content end -->


          <div class="clear"></div>

          <div class="divider divider-center"><i class="icon-circle"></i></div>


          <?php 
              $w_lib->run('front', 'master_content', 'widget_related_article',$news_id,12);
             ?>

          

      </div>

        </section><!-- #content end -->