<?php
$getImagePath=$this->master_perawat_helper_lib->getImagePath();
$pathImgUrl=$getImagePath['pathUrl'];
$pathImgLoc=$getImagePath['pathLocation'];
$logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;

$imgProperty=array(
'width'=>263,
'height'=>263,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'263263/',
'urlSave'=>$pathImgUrl.'263263/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
?> 
 <section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
        <div class="container">
          <div class="row">
             <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>vo_consument/dashboard/profil">Profil</a></li>
                    <li><a href="#">Edit Profil</a></li>
            </ul>
            <div class="col-md-3">
               <?php 
                  $this->widget_lib->run('front', 'vo_consument', 'widget_vo_consument_menu');
                 ?>
            </div>

            <div class="col-md-9">
             <h3><?php echo $seoTitle?></h3>
            <p class="text-warning">
               Mohon isi form dengan tanda (*)
            </p>
            <form action="" method="post" enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-md-12">
                    <?php
                    //untuk menampilkan pesan
                    if (trim($message) != '') 
                    {
                      ?>
                      <div class="alert <?php echo ($status==200)?'alert-success':'alert-danger'?>"><?php echo $message?></div>      
                      <?php
                    }
                    ?>
                       <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                    <span class="symbol required"></span>    Email *
                                    </label>
                                    <input class="form-control" name="additional_info_email_address" value="<?php echo $this->input->post('additional_info_email_address')?$this->input->post('additional_info_email_address'):(
                                                            (isset($additional_info_arr['additional_info_email_address']) AND trim($additional_info_arr['additional_info_email_address'])!='')?$additional_info_arr['additional_info_email_address']:''
                                                            )
                                                            ;?>" type="text" placeholder="Text Field">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                    <span class="symbol required"></span>    Nama Lengkap *
                                    </label>
                                    <input name="person_full_name" value="<?php echo $this->input->post('person_full_name')?$this->input->post('person_full_name'):$person_full_name;?>" class="form-control tooltips" placeholder="Nama Lengkap" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                </div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Jenis Kelamin
                                    </label>
                                   <select name="person_gender" class="form-control">
                                   <option value="L" <?php echo ($this->input->post('person_gender')=='L')?'selected':(
                                    ($person_gender=='L')?'selected':''
                                   )?>>Laki-laki</option>
                                   <option value="P" <?php echo ($this->input->post('person_gender')=='P')?'selected':(
                                    ($person_gender=='P')?'selected':''
                                   )?>>Perempuan</option>
                                   
                                   </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Golongan Darah
                                    </label>
                                    <select name="person_blood_type" class="form-control">
                                    <option value="-" <?php echo ($this->input->post('person_blood_type')=='-')?'selected':(
                                    ($person_blood_type=='-')?'selected':''
                                   )?>>Belum Tahu</option>
                                   <option value="A" <?php echo ($this->input->post('person_blood_type')=='A')?'selected':(
                                    ($person_blood_type=='A')?'selected':''
                                   )?>>A</option>
                                   <option value="B" <?php echo ($this->input->post('person_blood_type')=='B')?'selected':(
                                    ($person_blood_type=='B')?'selected':''
                                   )?>>B</option>
                                   <option value="AB" <?php echo ($this->input->post('person_blood_type')=='AB')?'selected':(
                                    ($person_blood_type=='AB')?'selected':''
                                   )?>>AB</option>
                                   <option value="O" <?php echo ($this->input->post('person_blood_type')=='O')?'selected':(
                                    ($person_blood_type=='O')?'selected':''
                                   )?>>O</option>
                                   </select>
                                </div>
                            </div>

                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                    Agama
                                    </label>
                                    <input name="person_religion" value="<?php echo $this->input->post('person_religion')?$this->input->post('person_religion'):$person_religion;?>" class="form-control tooltips" placeholder="Agama" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                </div>
                            </div>
                        </div>
                        
                    <div class="form-group">      
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        No. Telp.
                                    </label>
                                    <div>
                                       <input class="form-control tooltips" value="<?php echo $this->input->post('person_telephone')?$this->input->post('person_telephone'):$person_telephone;?>" name="person_telephone" placeholder="Telephone" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        No. HP.
                                    </label>
                                    <div>
                                          <input class="form-control tooltips" value="<?php echo $this->input->post('person_phone')?$this->input->post('person_phone'):$person_phone;?>" name="person_phone" placeholder="Phone" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                    </div>
                                </div>
                            </div>
                        </div>                          
                         
                    </div>
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Tanggal Lahir <em>(yyyy-mm-dd)</em>
                                    </label>
                                      <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control tooltips date-picker" name="person_birth_date" value="<?php echo $this->input->post('person_birth_date')?$this->input->post('person_birth_date'):$person_birth_date;?>" placeholder="Tgl Lahir" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Tempat Lahir
                                    </label>
                                 <input class="form-control tooltips" value="<?php echo $this->input->post('person_birth_place')?$this->input->post('person_birth_place'):$person_birth_place;?>" name="person_birth_place" placeholder="Tempat Lahir" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Provinsi
                                    </label>
                                    <div>
                                    <select name="person_address_province_id" class="form-control" onchange="get_sub_area_by_area_id($(this).val(),$(this).attr('name'));">
                                    </select>       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label">
                                        Kota / Kabupaten 
                                    </label>
                                    <div>
                                    <select name="person_address_city_id" class="form-control" onchange="get_sub_area_by_area_id($(this).val(),$(this).attr('name'));">
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label">
                                    Kecamatan
                                </label>
                                <div>
                                 <select name="person_address_district_id" class="form-control">
                                 </select>
                                 </div>
                             </div>
                            </div>
                        </div>
                         <div class="row">
                             <div class="col-md-6">
                                 <div class="form-group">    
                                    <label class="control-label">
                                        Alamat
                                    </label>
                                    <div>
                                        <textarea placeholder="Alamat" name="person_address_value"  class="form-control"><?php echo $this->input->post('person_address_value',true)?$this->input->post('person_address_value'):$person_address_value; ?></textarea> 
                                    </div>
                                 </div>
                             </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label>
                                      Foto
                                    </label>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;"><img src="<?php echo $img;?>" />
                                        </div>
                                        
                                        <div class="user-edit-image-buttons">
                                            <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                <input type="hidden" name="person_photo_old" value="<?php $this->input->post('person_photo_old')?$this->input->post('person_photo_old'):$person_photo?>">
                                                <input type="file" name="person_photo">
                                            </span>
                                           
                                        </div>
                                    </div>
                                </div>
                             </div>
                         </div>
                         
                         <div class="row">
                            <input type="hidden" name="save" value="1">
                            <div class="col-md-1">
                            <a href="<?php echo base_url()?>vo_consument/dashboard/profil">Batal</a>
                            </div>
                            <div class="col-md-4">
                                <button onclick="return confirm('Simpan?');" class="btn btn-info">Simpan</button>
                            </div>
                            <div class="col-md-7">
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
               
               <hr />                        
            </form>                            
            </div>
          </div>
        </div>
      </section>