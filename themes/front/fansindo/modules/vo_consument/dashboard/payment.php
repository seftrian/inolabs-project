<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
    <div class="container">
      <div class="row">
         <ul class="breadcrumb">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>vo_consument/dashboard/index">Dashboard</a></li>
                <li><a href="#">Transaksi</a></li>
        </ul>
        <div class="col-md-3">
           <?php 
              $this->widget_lib->run('front', 'vo_consument', 'widget_vo_consument_menu');
             ?>
        </div>
        <div class="col-md-9">
          <div class="kd-rich-editor">
            <h3><?php echo $seoTitle?></h3>
            <p class="text-warning">
                Jika telah melakukan pembayaran, anda dapat melakukan konfirmasi melalui form di bawah
                untuk mempercepat proses pengecekan data.
            </p>
            <article style="margin-top:10px;">
             <form action="" id="form-search" method="post" class="form-horizontal" onsubmit="return confirm('Kirim?');" >
              <div class="col-sm-12">  
               <?php
                    //untuk menampilkan pesan
                    if (trim($message) != '') 
                    {
                      ?>
                      <div class="alert <?php echo ($status==200)?'alert-success':'alert-danger'?>"><?php echo $message?></div>      
                      <?php
                    }
                    ?> 
                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Nomor Invoice *
                    </label>
                    <div class="col-sm-3">
                        <input name="payment_transaction_id" value="<?php echo $this->input->post('payment_transaction_id')?>" type="hidden" class="form-control" />
                        <input name="code" onchange="get_termin_from_transaction();return false;" value="<?php echo $this->input->post('code')?$this->input->post('code'):$code?>" type="text" id="code" class="form-control"  required/>
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Keterangan Pembayaran *
                    </label>
                    <div class="col-sm-3">
                        <select name="payment_termin_id" class="form-control" required>
                        </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Tgl. Transfer *
                    </label>
                    <div class="col-sm-3">
                          <input name="payment_date" value="<?php echo $this->input->post('payment_date')?$this->input->post('payment_date'):date('Y-m-d')?>"  data-date-format="yyyy-mm-dd" data-date-viewmode="years" type="text" id="start_date" class="form-control date-picker" required>
                    </div>
                    
                 </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Nominal (Rp) *
                    </label>
                    <div class="col-sm-3">
                        <input name="payment_value" value="<?php echo $this->input->post('payment_value')?>" type="text" class="form-control currency_rupiah" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Rekening Tujuan *
                    </label>
                    <div class="col-sm-10">
                        <select name="bank_account_id"  class="form-control" required>
                        <option value="">Pilih Rekening Tujuan</option>
                        <?php
                        if(!empty($bank_admin_arr))
                        {
                            foreach($bank_admin_arr AS $rowArr)
                            {
                                foreach($rowArr AS $variable=>$value)
                                {
                                    ${$variable}=$value;
                                }
                                $bank_name=$this->function_lib->get_one('bank_name','ref_bank','bank_id='.intval($bank_account_bank_id));
                                $label=$bank_name.'/No: '.$bank_account_no.'/ a.n: '.$bank_account_name;
                                $selected=($label==$this->input->post('bank_account_id'))?'selected':'';
                                
                                ?>

                                <option value="<?php echo $label?>" <?php echo $selected?>><?php echo $label?></option>
                                <?php
                            }
                        }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Bank Pengirim *
                    </label>
                    <div class="col-sm-3">
                        <select name="bank_id" class="form-control" required>
                        <option value="">Pilih Bank</option>
                            <?php
                            if(!empty($bank_arr))
                            {
                                foreach($bank_arr AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                $selected=($bank_id==$this->input->post('bank_id'))?'selected':'';

                                    ?>
                                    <option value="<?php echo $bank_id?>" <?php echo $selected?>><?php echo $bank_name?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        No. Rek. Pengirim *
                    </label>
                    <div class="col-sm-3">
                        <input name="account_no" value="<?php echo $this->input->post('account_no')?>" type="text" class="form-control"  required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                       Nama Pengirim *
                    </label>
                    <div class="col-sm-3">
                        <input name="sender" value="<?php echo $this->input->post('sender')?>" type="text" class="form-control" required/>
                    </div>
                </div>
                </div>
              
                <div class="col-sm-12"> 
                    <div class="form-group">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="send" value="1">
                            <a href="<?php echo base_url().'vo_consument/dashboard/payment'?>">Batal</a>
                            <button style="float:right;" class="btn btn-info" onclick="grid_reload();return false;" ><i class=" clip-search"></i> Kirim</button>
                        </div>       
                    </div>  
                </div>
            </form>

            </article>
          </div>
        </div>

      </div>
    </div>
</section>
<script src="<?php echo base_url();?>assets/js/autoNumeric.js"></script>

<script type="text/javascript">
$(function(){
    get_termin_from_transaction();
    currency_rupiah();
    $('.date-picker').datepicker({autoclose:true
                  });
});

function currency_rupiah()
{
    var myOptions = {aSep: '.', aDec: ',',vMin: '-999999999.99'};
    $('.currency_rupiah').autoNumeric('init', myOptions); 
}

function get_termin_from_transaction()
{
    var code=$("input[name='code']").val();
    if($.trim(code)!='')
    {
        $.getJSON('<?php echo base_url()?>vo_consument/dashboard/get_termin_from_transaction/'+code,function(response_arr){
            var termin_arr=response_arr['termin_arr'];
            $("input[name='payment_transaction_id']").val(response_arr['transaction_id']); 
            $("select[name='payment_termin_id']").html('<option value="'+termin_arr['termin_id']+'">'+termin_arr['termin_label']+'</option>');
        });
    }
  
}
</script>