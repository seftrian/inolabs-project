    <!-- Page Title
    ============================================= -->
    <section id="page-title">

      <div class="container clearfix">
        <h1><?php echo $seoTitle; ?></h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url(); ?>">Home</a></li>
          <li class="active"><?php echo $seoTitle; ?></li>
        </ol>
      </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

      <div class="content-wrap">

        <div class="container clearfix">

          <!-- Post Content
          ============================================= -->
          <div class="postcontent nobottommargin col_last clearfix">

        <div class="row" style="margin-bottom: 34px;">
          <form method="get" action="<?php echo $base_link; ?>">
          <div class="col-md-10">
            <input class="typehead sm-form-control tt-input" type="text" name="query" placeholder="Pencarian" value="<?php echo (isset($_GET['query']) AND trim($_GET['query'])!='')?$_GET['query']:''; ?>"/>
            <span style="float: left; padding: 12px; <?php echo (isset($_GET['query']) AND trim($_GET['query'])!='')?'':'display: none;'; ?>">Search Result : <?php echo (!empty($results))?count($results):0; ?> of <?php echo $total_rows; ?></span>
          </div>
          <div class="col-md-2">
            <input type="submit" class="button button-rounded button-white button-light nomargin" style="background: #a08347; color: #fff; width: 150px;" value="Cari">
          </div>
          </form>
        </div>


            <!-- Posts
            ============================================= -->
            <div id="posts" class="small-thumbs">

                      <?php
                             $pathImgUrl=$struktur_data['pathImgArr']['pathUrl'];
                             $pathImgLoc=$struktur_data['pathImgArr']['pathLocation'];
                            if(!empty($results))
                            {
                                foreach($results AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                      $logo_name= (trim($struktur_foto)=='')?'-': pathinfo($struktur_foto,PATHINFO_FILENAME);
                                      $logo_ext=  pathinfo($struktur_foto,PATHINFO_EXTENSION);
                                      $logo_file=$logo_name.'.'.$logo_ext;
                                    
                                      $imgProperty=array(
                                      'width'=>300,
                                      'height'=>225,
                                      'imageOriginal'=>$logo_file,
                                      'directoryOriginal'=>$pathImgLoc,
                                      'directorySave'=>$pathImgLoc.'300225/',
                                      'urlSave'=>$pathImgUrl.'300225/',
                                      );
                                      $img=$this->function_lib->resizeImageMoo($imgProperty);
                                    ?>
              <div class="entry clearfix">
                <div class="entry-image">
                  <a href="<?php echo $img; ?>" data-lightbox="image"><img class="image_fade" src="<?php echo $img; ?>" alt="<?php echo $struktur_nama_kepala?>"></a>
                  <div style="width: 50%; margin: 9px auto;">
                                                <?php
                                                if(trim($struktur_facebook)!='')
                                                {
                                                    ?>
                                    <a target="_blank" href="http://facebook.com/<?php echo $struktur_facebook?>" class="social-icon si-rounded si-small si-light si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>

                                                <?php
                                                }
                                                ?>
                                                 <?php
                                                if(trim($struktur_twitter)!='')
                                                {
                                                    ?>
                                    <a target="_blank" href="http://twitter.com/<?php echo $struktur_twitter?>" class="social-icon si-rounded si-small si-light si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <?php
                                                }
                                                ?>
                                                 <?php
                                                if(trim($struktur_gplus)!='')
                                                {
                                                    ?>
                                    <a target="_blank" href="http://plus.google.com/<?php echo $struktur_gplus?>" class="social-icon si-rounded si-small si-light si-gplus">
                                        <i class="icon-gplus"></i>
                                        <i class="icon-gplus"></i>
                                    </a>
                                    <?php
                                                }
                                                ?>
                                                 <?php
                                                if(trim($struktur_linkedin)!='')
                                                {
                                                    ?>
                                    <a target="_blank" href="https://id.linkedin.com/in/<?php echo $struktur_linkedin?>" class="social-icon si-rounded si-small si-light si-linkedin">
                                        <i class="icon-linkedin"></i>
                                        <i class="icon-linkedin"></i>
                                    </a>
                                    <?php
                                                }
                                                ?>
                  </div>
                </div>
                <div class="entry-c">
                  <div class="entry-title">
                    <h2 style="text-align: justify;"><a href="#"><?php echo $struktur_nama_kepala?></a></h2>
                  </div>
                  <div class="entry-content">
                    <p style="text-align: justify;"><?php echo $struktur_profil_singkat?></p>
                  </div>
                </div>
              </div>

                               <?php
                                }
                            }
                        ?>    

            </div><!-- #posts end -->

            <!-- Pagination
            ============================================= -->
            <?php echo $link_pagination?>

          </div><!-- .postcontent end -->

          <!-- Sidebar
          ============================================= -->
          <div class="sidebar nobottommargin clearfix">
            <div class="sidebar-widgets-wrap">
<?php 
$w_lib=new widget_lib;
$w_lib->run('front', 'master_content', 'widget_schedule_patient');
?>
            </div>
          </div><!-- .sidebar end -->

        </div>

      </div>

    </section><!-- #content end -->
