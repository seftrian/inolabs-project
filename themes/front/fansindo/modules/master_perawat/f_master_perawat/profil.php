<?php
$getImagePath=$this->master_perawat_helper_lib->getImagePath();
$pathImgUrl=$getImagePath['pathUrl'];
$pathImgLoc=$getImagePath['pathLocation'];
$icon_has_experienced=($medical_master_has_experienced=='Y')?'<i class="fa fa-heart-o"></i> Berpengalaman':'<i class="fa fa-graduation-cap"></i> Baru Lulus';
$label_location_duty=$this->master_perawat_helper_lib->label_location_duty($person_id);
$label_position=$this->master_perawat_helper_lib->label_position($person_id);
$logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;

$imgProperty=array(
'width'=>263,
'height'=>263,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'263263/',
'urlSave'=>$pathImgUrl.'263263/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
$medical_master_experience=($medical_master_has_experienced=='Y')?$medical_master_experience:'';
$text_about=strip_tags(html_entity_decode($medical_master_about.' '.$medical_master_experience.' '.$medical_master_skills));
$text_about=(strlen($text_about)>206)?substr($text_about,0,206).'...':$text_about;
//$minimal_rate=$this->master_perawat_helper_lib->label_minimal_rate($person_id);
$minimal_rate=function_lib::currency_rupiah($medical_master_rate_min).'/jam';
?> 
 <section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
        <div class="container">
          <div class="row">
             <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>perawat/index">Tenaga Kesehatan</a></li>
                    <li><a href="#"><?php echo $seoTitle?></a></li>
            </ul>
            <div class="col-md-3">
              <div class="kd-team-shortinfo">
                <ul>
                  <li><figure><a href="#"><img src="<?php echo $img?>" alt="<?php echo $person_full_name?>"></a></figure></li>
                  <li><center><button onclick="add_to_cart_nakes($(this),<?php echo $person_id?>,true);" class="custom-btn btn-xs">Pilih</button></center></li>
                  <li>
                    <span><i title="Terverifikasi" class="fa fa-check-circle"></i> <?php echo $person_full_name?></span>
                  </li>
                  <li>
                  <?php echo $medical_master_education?><br />
                  <i class="fa fa-money"></i> <?php echo $minimal_rate?>
                  <br /><?php echo $icon_has_experienced?>

                  </li>
                  
                </ul>
              </div>
            </div>

            <div class="col-md-9">

              <div class="kd-tab kd-horizontal-tab">

                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">Tentang Saya</a></li>
                    <li role="presentation"><a href="#rate_location" aria-controls="rate_location" role="tab" data-toggle="tab">Tarif &amp; Penempatan</a></li>
                    <li role="presentation"><a href="#experience" aria-controls="experience" role="tab" data-toggle="tab">Pengalaman</a></li>
                    <li role="presentation"><a href="#skills" aria-controls="skills" role="tab" data-toggle="tab">Keahlian</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="about">
                      <p><?php echo $medical_master_about;?></p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="rate_location">
                      <?php require_once('rate_position.php');?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="experience">
                    <?php echo ($medical_master_has_experienced=='Y')?$medical_master_experience:$icon_has_experienced;?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="skills">
                      <?php echo html_entity_decode($medical_master_skills)?>
                    </div>
                    
                  </div>

                </div>

            </div>

          </div>
        </div>
      </section>