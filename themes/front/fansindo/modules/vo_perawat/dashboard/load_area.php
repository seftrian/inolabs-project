                                                                    
<form id="form-compose-message" onsubmit="return false;">
<table class="table table-border">
<tr>
<th>Wilayah</th><td><input onkeyup="grid_reload_load_area();" name="area_name" id="area_name" class="form-control" type="text"></td>
</tr>
</table>
</form>
<table id="gridview_load_area" style="display:none;"></table>
<script type="text/javascript">
    function grid_reload_load_area() {
        var person_id='<?php echo $this->input->get('person_id');?>';
        var area_name=$("input#area_name").val();
        var link_service='?&area_name='+area_name+'&person_id='+person_id;
        $("#gridview_load_area").flexOptions({url:'<?php echo base_url(); ?>master_perawat/service_rest/load_area'+link_service}).flexReload();
    }

    $(function(){
       grid_reload_load_area();
       $("input#area_name").focus();
    });

     function load_area_by_id(id,name)
        {
            var current_number_modal=$("#current_number_modal").val();
            $('#area_id_'+current_number_modal).val(id);
            $('#area_name_'+current_number_modal).val(name);
            $('#load-area').modal('toggle');
        }
</script>
<script>
    $("#gridview_load_area").flexigrid({
        dataType: 'json',
        colModel: [
            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
            { display: 'Aksi', name: 'load', width: 50, sortable: true, align: 'center' },
            { display: 'Nama Wilayah', name: 'area_name', width: 380, sortable: true, align: 'center' },                    
        ],
        buttons: [
          
        ],
        buttons_right: [
    //                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
        ],
      
        sortname: "id",
        sortorder: "asc",
        usepager: true,
        title: ' ',
        useRp: true,
        rp: 50,
        showTableToggleBtn: false,
        showToggleBtn: true,
        width: 'auto',
        height: '300',
        resizable: false,
        singleSelect: false
    });


</script>