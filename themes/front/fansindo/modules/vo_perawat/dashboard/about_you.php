                                            <div class="row">
                                                
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Tinggi <em>(cm)</em>
                                                                </label>
                                                                  <input class="form-control" name="medical_master_height" value="<?php echo $this->input->post('medical_master_height')?$this->input->post('medical_master_height'):$medical_master_height;?>" placeholder="Tinggi" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Berat <em>(kg)</em>
                                                                </label>
                                                                  <input class="form-control" name="medical_master_weight" value="<?php echo $this->input->post('medical_master_weight')?$this->input->post('medical_master_weight'):$medical_master_weight;?>" placeholder="Berat" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">    
                                                         <label class="control-label">
                                                            Pendidikan
                                                        </label>
                                                        <div>
                                                        <textarea maxlength="255" placeholder="Informasi pendidikan... [maks 255]" name="medical_master_education"  class="form-control"><?php echo $this->input->post('medical_master_education',true)?$this->input->post('medical_master_education'):$medical_master_education; ?></textarea> 
                                                        </div>
                                                    </div>
                                                  

                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group">    
                                                         <label class="control-label">
                                                            Deskripsi
                                                        </label>
                                                        <div>
                                                        <textarea rows="6" maxlength="255" placeholder="Deskripsi profil... [maks 255]" name="medical_master_about"  class="form-control"><?php echo $this->input->post('medical_master_about',true)?$this->input->post('medical_master_about'):$medical_master_about; ?></textarea> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                <div class="row">
                                                      
                                                      <div class="col-md-6">
                                                        <div class="form-group">    
                                                             <label class="control-label">
                                                                Keahlian
                                                            </label>
                                                            <div>
                                                            <?php echo form_textarea_tinymce('medical_master_skills', ($this->input->post('medical_master_skills')?$this->input->post('medical_master_skills'):$medical_master_skills), 'Standard'); ?>
                                                            </div>
                                                        </div>
                                                      </div>
                                                      <div class="col-md-6">
                                                         <div class="form-group">    
                                                               <label class="control-label">
                                                                  Pengalaman
                                                              </label>
                                                              <div>
                                                              <select name="medical_master_has_experienced" onchange="$('#div_medical_master_experience').toggle();" class="form-control">
                                                                <option value="N" <?php echo ($medical_master_has_experienced=='N')?'selected':''?>>Fresh Graduated</option>
                                                                <option value="Y" <?php echo ($medical_master_has_experienced=='Y')?'selected':''?>>Berpengalaman</option>
                                                              </select>
                                                              </div>
                                                         </div> 
                                                         <div id="div_medical_master_experience" class="form-group" style="<?php echo ($medical_master_has_experienced=='N')?'display:none;':''?>">    
                                                               <label class="control-label">
                                                                 Isi Pengalaman
                                                              </label>
                                                              <div>
                                                              <?php echo form_textarea_tinymce('medical_master_experience', ($this->input->post('medical_master_experience')?$this->input->post('medical_master_experience'):html_entity_decode($medical_master_experience)), 'Standard'); ?>
                                                              </div>
                                                          </div>
                                                      </div>
                                                </div>
                                                </div>
                                            </div>
                                        