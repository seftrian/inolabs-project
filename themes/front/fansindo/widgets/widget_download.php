<section id="about-us">
  <div class="container">
    <div class="center wow fadeInDown">
      <h2 class="text-center">DOWNLOAD</h2>
    </div>

    <div class="row wow fadeInDown">

      <?php
        function dateformat($tanggal){
          $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
          );
          $pecahkan = explode('-', $tanggal);
         
          return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
        if (!empty($results)) {
          $i=0;
          $len=count($results);
          foreach ($results as $rowArr) {
              foreach ($rowArr as $variable => $value) 
              {
                  ${$variable}=$value;
              }
              $day = date('D', strtotime($download_timestamp));
              $dayList = array(
                'Sun' => 'Minggu',
                'Mon' => 'Senin',
                'Tue' => 'Selasa',
                'Wed' => 'Rabu',
                'Thu' => 'Kamis',
                'Fri' => 'Jumat',
                'Sat' => 'Sabtu'
              );
            ?>
            <div class="col-md-12 ">
              <a href="<?php echo $base_url.'master_content/download_file/'.$download_id ?>" class="link" title="<?php echo ucwords($download_title); ?>">
                  <div class="flex">
                      <div class="frame-icon"><i class="fa <?php if(substr($download_file, -3)=="pdf"){echo "fa-file-pdf-o";}else{echo "fa-picture-o"; }?> fa-5x"></i></div>
                      <div class="frame-konten">
                          <p class="mb-1 mt-1 font-title-download"><?php echo ucwords($download_title); ?></p>
                          <p class="mb-1">
                              <i class="fa fa-calendar"></i>
                              <?php echo $dayList[$day].' / '.dateformat(substr($download_timestamp, 0,10)).' / '.substr($download_timestamp, 10,6).' WIB'; ?>
                          </p>
                      </div>
                  </div>
              </a>
            </div>
            <?php if ($i != $len - 1) {?>
            <div class="col-md-12"><hr class="hr-dashed"></div>
            <?php
          }
            $i++; 
          }
        }else{
          echo "<center>Data tidak tersedia</center>";
        }
      ?>
  </div>
  </div>
</section><!--/download-->
<br>
<br>
<br>
<br>