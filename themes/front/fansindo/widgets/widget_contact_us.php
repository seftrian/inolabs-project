<?php if(!empty($results)){ ?>
<div class="col-md-3 col-sm-6">
    <div class="widget">
        <h3 class="mb-1">Contact us</h3>
        <div class="line"></div>
        <p>
            <?php echo $results_config[0]['configuration_value']; ?>, <?php echo $results_config[18]['configuration_value']; ?>, <?php echo $results_config[1]['configuration_value']; ?> <?php echo $results_config[2]['configuration_value']; ?>
        </p>
        <p>
            <div class="flex">
                <div class="contact-footer"><b> P <span class="value">:</span></b></div>
                <div><?php echo $results_config[22]['configuration_value']; ?></div>
            </div>
            <div class="flex">
                <div class="contact-footer"><b> E <span class="value">:</span></b></div>
                <div><?php echo $results_config[4]['configuration_value']; ?></div>
            </div>
            <div class="flex">
                <div class="contact-footer"><b> W <span class="value">:</span></b></div>
                <div><a href="www.fansindo.co.id" class="link">www.fansindo.co.id</a></div>
            </div>
        </p>
    </div>
</div>
<?php } ?>