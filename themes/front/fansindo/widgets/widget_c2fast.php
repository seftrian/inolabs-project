<?php if (!empty($results[0]['news_photo'])) {?>
  <!-- about us slider -->
  <div id="about-slider">
    <div id="carousel-slider" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
        <ol class="carousel-indicators visible-xs">
          <?php
          for ($i=0; $i < count($results_more_images)+1; $i++) {
          ?>
          <li data-target="#carousel-slider" data-slide-to="<?php echo $i; ?>" <?php if ($i==0) { ?>class="active"<?php } ?>></li>
          <?php 
          }
          ?>
        </ol>

      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo $base_url.''.$results[0]['news_photo']; ?>" class="img-responsive" alt=""> 
        </div>
        <?php
        foreach ($results_more_images as $rowArr) {
          foreach ($rowArr as $variable => $value) {
              ${$variable}=$value;
          }
          if ($news_more_images_news_id==$results[0]['news_id']) {
        ?>
        <div class="item">
          <img src="<?php echo $base_url.''.$news_more_images_file; ?>" class="img-responsive" alt=""> 
        </div>
        <?php
            }
          }
        ?>
      </div>
      
      <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
        <i class="fa fa-angle-left"></i> 
      </a>
      
      <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
        <i class="fa fa-angle-right"></i> 
      </a>
    </div> <!--/#carousel-slider-->
  </div><!--/#about-slider-->
<?php } ?>
<div>
  <?php echo html_entity_decode(strip_tags($results[0]['news_content'])); ?>
</div>
