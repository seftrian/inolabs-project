<div class="col-md-3 col-sm-6">
    <div class="widget">
        <h3 class="mb-1">Customer Service</h3>
        <div class="line"></div>
        <div>
            <table >
                <tr>
                    <td><img style="width:120px;" src="<?php echo $base_url."".$results[0]['news_photo'] ?>" title="cs" />
                    <br /><br />
                    </td>
                </tr>
            </table>
            <?php echo html_entity_decode(strip_tags($results[0]['news_content'])); ?>
            <br />
            <p>
                <a class="btn btn-xs btn-default" href="http://www.facebook.com/OfficialFansindo/"><i class="fa fa-facebook"></i></a>
                <a class="btn btn-xs btn-default" href="http://twitter.com/fansindo_office"><i class="fa fa-twitter"></i></a>
                <a class="btn btn-xs btn-default" href="http://instagram.com/fansindo_office"><i class="fa fa-instagram"></i></a>
                <a class="btn btn-xs btn-default" href="#"><i class="fa fa-linkedin"></i></a>
                <a class="btn btn-xs btn-default" href="#"><i class="fa fa-skype"></i></a>
            </p>
        </div>
    </div>    
</div><!--/.col-md-3-->