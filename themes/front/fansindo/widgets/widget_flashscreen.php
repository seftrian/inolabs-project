<?php if (!empty($results) && empty(get_cookie('show_pengumuman'))) 
{ 
  if ($results[0]['news_photo']!='-') 
  { 
    if (file_exists(FCPATH.$results[0]['news_photo']))
    { ?>
      <!-- modal konfirmasi -->
      <div class="modal fade" id="modal-terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content modal-content-custom">
                  <div class="modal-body">
                      <center><img class="img-modal" src="<?php echo $base_url.''.$results[0]['news_photo']; ?>" title="<?php echo $results[0]['news_title']; ?>" alt="<?php echo $results[0]['news_title']; ?>"></center>
                  </div>
                  <!-- <div class="modal-footer">
                      <a href="#" class="btn btn-success" onclick="accepted_terms();return fals;">Tutup</a>
                  </div> -->
              </div>
              <a href="#" class="circle-close" onclick="accepted_terms();return fals;">
                  <i class="fa fa-times"></i>
              </a>
          </div>
      </div>
      <?php
    }
  }
}
$cookie=array(
  'name' => 'show_pengumuman',
  'value' => 'N',
  'expire' => 3600,
  'domain' => '',
  'path' => '/',
  'prefix' => '',
  'secure' => FALSE
);
set_cookie($cookie);
?>