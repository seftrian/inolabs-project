<?php
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
if (!empty($results)) 
{ ?>
  <!-- video -->
  <section>
      <div class="container">
          <div class="center wow fadeInDown">
              <h2>Video</h2>
          </div>
          <div class="row">
            <?php
            $no = 0;
            // var_dump($results);
            foreach ($results as $rowArr) 
            {
              $no++;
              foreach ($rowArr as $variable => $value) 
              {
                  ${$variable}=$value;
              }   
              ?>
              <div class="col-sm-4 area-video">
                <div class="frame-video">
                  <iframe width="560" height="315" src="<?php echo $news_external_link ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <a href="#play-<?php echo $no ?>" data-lightbox="inline" class="layer-video"></a>
                </div>
              </div>
              <?php 
              $data_modal .= '
              <div class="modal1 mfp-hide" id="play-'.$no.'">
                  <div class="block divcenter" style="background-color: #FFF;">
                      <div class="feature-box fbox-center fbox-effect nobottomborder nobottommargin" style="padding: 40px;">
                          <div class="box-frame">
                              <iframe width="560" height="315" src="'.$news_external_link.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                          </div>
                      </div>
                  </div>
              </div>
              ';
            } ?>
            <div class="col-sm-12 text-center mt-2">
              <a href="<?php echo base_url('master_content/news/video') ?>" class="btn btn-round-default color-primary">Video Lainnya <i class="fa fa-chevron-right"></i></a>
            </div>
          </div>
          <?php
          $i = 0;
          // var_dump($results);
          foreach ($results as $rowArr) 
          {
            $i++;
            foreach ($rowArr as $variable => $value) 
            {
                ${$variable}=$value;
            }   
            echo '
            <div class="modal1 mfp-hide" id="play-'.$i.'">
                <div class="block divcenter" style="background-color: #FFF;">
                    <div class="feature-box fbox-center fbox-effect nobottomborder nobottommargin" style="padding: 40px;">
                        <div class="box-frame">
                            <iframe width="560" height="315" src="'.$news_external_link.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            ';
          } ?>
      </div>
  </section>
  <!-- / -->
  <?php 
} ?>