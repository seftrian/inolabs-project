<!-- <div class="col-md-4 wow fadeInDown">
    <div class="panel panel-custom-2">
        <div class="panel-heading heading-custom-2">
            <h3 class="mb-0"><i class="fa fa-bullhorn"></i> Top Income RO September 2018</h3>
        </div>
        <div class="panel-body panel-body-height ul-v-1">
            <marquee class="marquee-1" behavior="scroll" direction="Up" scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">
                <ul type="none" id="div_top_ro">
                </ul>
            </marquee>
        </div>
    </div>
</div> -->
<div class="col-md-12 wow fadeInDown">
    <div class="box-member">
        <div class="row">
            <div class="col-sm-4 padding-member-left">
                <div class="head-member-1">
                    <h3 class="mb-0"><i class="fa fa-user"></i> Member Baru</h3>
                </div>
                <div class="box-member-body ul-v-2">
                    <marquee class="marquee-1" behavior="scroll" direction="Up" scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">
                        <ul type="none" id="div_member_baru">
                        </ul>
                    </marquee>
                </div>
            </div>
            <div class="col-sm-4 padding-member-center">
                <div class="head-member-2">
                    <h3 class="mb-0"><i class="fa fa-money"></i> Top Income</h3>
                </div>
                <div class="box-member-body ul-v-2">
                    <marquee class="marquee-1" behavior="scroll" direction="Up" scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">
                        <ul type="none" id="div_top_income">
                        </ul>
                    </marquee>
                </div>
            </div>
            <div class="col-sm-4 padding-member-right">
                <div class="head-member-3">
                    <h3 class="mb-0"><i class="fa fa-handshake-o"></i> Top Sponsor</h3>
                </div>
                <div class="box-member-body ul-v-2 ">
                    <marquee class="marquee-1" behavior="scroll" direction="Up" scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">
                        <ul type="none" id="div_top_sponsor">

                        </ul>
                    </marquee>
                </div>
            </div>
        </div>
    </div>
</div>
