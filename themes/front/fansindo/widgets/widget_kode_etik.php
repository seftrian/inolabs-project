<?php if (!empty($results[0]['news_photo'])) {?>
  <!-- about us slider -->
  <div id="about-slider">
    <div id="carousel-slider" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
        <ol class="carousel-indicators visible-xs">
          <?php
          for ($i=0; $i < count($results_more_images)+1; $i++) {
          ?>
          <li data-target="#carousel-slider" data-slide-to="<?php echo $i; ?>" <?php if ($i==0) { ?>class="active"<?php } ?>></li>
          <?php 
          }
          ?>
        </ol>

      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo $base_url.''.$results[0]['news_photo']; ?>" class="img-responsive" alt=""> 
        </div>
        <?php
        foreach ($results_more_images as $rowArr) {
          foreach ($rowArr as $variable => $value) {
              ${$variable}=$value;
          }
          if ($news_more_images_news_id==$results[0]['news_id']) {
        ?>
        <div class="item">
          <img src="<?php echo $base_url.''.$news_more_images_file; ?>" class="img-responsive" alt=""> 
        </div>
        <?php
            }
          }
        ?>
      </div>
      
      <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
        <i class="fa fa-angle-left"></i> 
      </a>
      
      <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
        <i class="fa fa-angle-right"></i> 
      </a>
    </div> <!--/#carousel-slider-->
  </div><!--/#about-slider-->
<?php } ?>

<!-- Our Skill -->
<div class="skill-wrap clearfix">
  <div class="justify wow fadeInDown">
        <section id="content" class="shortcode-item">
          <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <h2>KODE ETIK</h2> 
                    <div class="tab-wrap">
                      <div class="media">
                          <div class="parrent pull-left">
                            <ul class="nav nav-tabs nav-stacked">
                                <?php
                                $i=1;
                                foreach ($results_news_tab as $rowArr) {
                                    foreach ($rowArr as $variable => $value) {
                                        ${$variable}=$value;
                                    }
                                ?>
                                <li <?php if($i==1){echo'class="active"';} ?>><a href="#tab<?php echo $i; ?>" data-toggle="tab" class="analistic-<?php if($i<=9){echo "0".$i;}else{echo $i;} ?>"><?php echo $tab_title; ?></a></li>
                                <?php
                                $i++; 
                                }
                                ?>
                            </ul>
                          </div>
                          <div class="parrent media-body">
                              <div class="tab-content">
                                  <?php
                                  $i=1;
                                  foreach ($results_news_tab as $rowArr) {
                                      foreach ($rowArr as $variable => $value) {
                                          ${$variable}=$value;
                                      }
                                  ?>
                                  <div class="tab-pane <?php if($i==1){echo"active";} ?>" id="tab<?php echo $i; ?>">
                                      <div class="active">
                                          <div class="media">
                                              <div class="media-body">
                                                <?php echo html_entity_decode(strip_tags($tab_content)); ?>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <?php
                                  $i++; 
                                  }
                                  ?>
                              </div> <!--/.media-->     
                          </div><!--/.tab-wrap-->               
                      </div><!--/.col-sm-6-->
                    </div>
                </div>
            </div><!--/.row-->
          </div><!--/.container-->
        </section><!--/#content-->
      </div><!--/.col-md-3-->
    </div>
  </div>
</section><!--/#bottom-->