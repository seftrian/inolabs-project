<section id="recent-works">
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>INDONESIA GROWING BUSINESS & GO SINGAPORE</h2>
        </div>

        <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <?php
            if (!empty($results)) {
                foreach ($results as $rowArr) {
                    foreach ($rowArr as $variable => $value) {
                        ${$variable}=$value;
                    }
            ?>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="recent-work-wrap">
                    <img class="img-responsive" src="<?php echo $base_url."".$news_photo; ?>" alt="">
                    <div class="overlay">
                        <div class="recent-work-inner">
                            <h3><a href="#"><?php echo ucfirst($news_title); ?></a> </h3>
                            <a class="preview" href="<?php echo $base_url."".$news_photo; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                        </div> 
                    </div>
                </div>
            </div>
            <?php foreach ($results_more_images as $thisvalue) { 
                if($thisvalue['news_more_images_news_id']==$news_id){?>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="recent-work-wrap">
                    <img class="img-responsive" src="<?php echo $base_url."".$thisvalue['news_more_images_file']; ?>" alt="">
                    <div class="overlay">
                        <div class="recent-work-inner">
                            <h3><a href="#"><?php echo ucfirst($news_title); ?></a> </h3>
                            <a class="preview" href="<?php echo $base_url."".$thisvalue['news_more_images_file']; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                        </div> 
                    </div>
                </div>
            </div>
                <?php } ?> 
            <?php } ?>    
            <?php
                }
            }
            ?>
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#recent-works-->
