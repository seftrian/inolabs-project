<nav class="navbar navbar-inverse" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-header"><a class="navbar-brand" href="<?php echo $base_url; ?>"><img src="<?php echo $base_url.''.$results_config[13]['configuration_value']; ?>" alt="logo"></a></span>
        </div>
		
        <div class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
                <?php
                if (!empty($results)) {
                    foreach ($results as $rowArr) {
                        foreach ($rowArr as $variable => $value) {
                            ${$variable}=$value;
                        }
                    if ($menu_administrator_par_id==0 AND $menu_administrator_is_active==1 AND $menu_administrator_link!="#") {
                ?>
                    <li class="<?php if($url==$base_url.''.$menu_administrator_link){echo"active";} ?>">
                        <a href="<?php echo $base_url.''.$menu_administrator_link; ?>"><?php echo $menu_administrator_title; ?></a>
                    </li>
                    <?php }elseif($menu_administrator_par_id==0 AND $menu_administrator_is_active==1){ ?>
                        <li class="dropdown <?php if($url==$base_url.''.$menu_administrator_link){echo"active";} ?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $menu_administrator_title; ?> <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <?php foreach ($results as $thisvalue) {
                                if ($thisvalue['menu_administrator_par_id']==$menu_administrator_id) {?>
                                    <li class="<?php if($url==$base_url.''.$thisvalue['menu_administrator_link']){echo"active";} ?>"><a href="<?php echo $base_url.''.$thisvalue['menu_administrator_link']; ?>"><?php echo $thisvalue['menu_administrator_title']; ?></a></li>
                                <?php }
                                } ?>
                            </ul>
                        </li>
                    <?php } ?>
                    
                <?php } ?>
            <?php } ?>                
            </ul>
        </div>
    </div><!--/.container-->
</nav><!--/nav-->