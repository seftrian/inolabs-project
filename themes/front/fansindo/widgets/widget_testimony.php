<?php
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
if (!empty($results)) 
{ ?>
    <section id="services" class="service-item">
       <div class="container">
            <div class="center wow fadeInDown">
                <h2 class="">Testimonials</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-testimonial wow fadeInLeft">
                        <div id="myCarousel3" class="carousel slide" data-ride="carousel" >
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <?php
                                $no = 0;
                                foreach ($results as $rowArr) 
                                {
                                    foreach ($rowArr as $variable => $value) 
                                    {
                                        ${$variable}=$value;
                                    }   
                                    $myCarousel3_active = ($no==0)?'class="active"':'';
                                    ?>
                                    <li data-target="#myCarousel3" data-slide-to="0" <?php echo $myCarousel3_active; ?>></li>
                                    <?php
                                    $no++;                                    
                                }   ?>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php
                                $no = 0;
                                $jml_karakter = 200;
                                foreach ($results as $rowArr) 
                                {
                                    $no++;
                                    foreach ($rowArr as $variable => $value) {
                                        ${$variable}=$value;
                                    }   
                                    $class_item_active = ($no==1)?'active':'';
                                    ?>
                                    <div class="item <?php echo $class_item_active ?>">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="circle-image-2">
                                                    <img src="<?php echo base_url($testimonial_avatar_src) ?>" alt="">
                                                </div>
                                                <div class="box-konten-2">
                                                    <h3 class="mb-0 mt-0 title-h3 color-primary-2">
                                                        <?php echo $testimonial_full_name; ?>, <span><?php echo $testimonial_title; ?></span>
                                                    </h3>
                                                    <p><?php echo substr($testimonial_value, 0, $jml_karakter); ?></p>
                                                    <a href="<?php echo base_url('master_content/archives/testimonial?v=static') ?>" class="link-primary"><b>Selengkapnya <i class="fa fa fa-chevron-right"></i></b></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }   ?>
                            </div>

                            <!-- Left and right controls -->
                              <a class="left carousel-control" href="#myCarousel3" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="right carousel-control" href="#myCarousel3" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>                                                
            </div><!--/.row-->
        </div><!--/.container-->
    </section>
    <?php 
}   ?>