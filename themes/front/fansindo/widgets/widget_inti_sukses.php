<section id="feature" >
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>TEMUKAN INTI KESUKSESAN!</h2>
        </div>

        <div class="row">
            <div class="features">
                <?php
                $pathImgUrl=$pathImgArr['pathUrl'];
                $pathImgLoc=$pathImgArr['pathLocation'];
                if (!empty($results)) {
                    foreach ($results as $rowArr) {
                        foreach ($rowArr as $variable => $value) {
                            ${$variable}=$value;
                        }
                ?>
                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="<?php echo $news_external_link_label; ?>"></i>
                        <h2><?php echo $news_title; ?></h2>
                        <h3><?php echo ($news_content); ?></h3>
                    </div>
                </div><!--/.col-md-4-->
                <?php
                    }
                }
                ?>
            </div><!--/.services-->
        </div><!--/.row-->    
    </div><!--/.container-->
</section><!--/#feature-->
