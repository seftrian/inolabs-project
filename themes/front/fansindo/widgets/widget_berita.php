<?php
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
if (!empty($results)) 
{ ?>
  <div class="col-sm-7 wow fadeInDown">
      <div class="panel panel-custom">
          <div class="panel-heading heading-custom">
              <h3 class="mb-0"><i class="fa fa-newspaper-o"></i> berita terbaru</h3>
          </div>
          <div class="panel-body panel-body-height">
              <div class="widget-news">
                  <ul type="none">
                    <?php
                    $no = 0;
                    foreach ($results as $rowArr) 
                    {
                      $no++;
                      foreach ($rowArr as $variable => $value) 
                      {
                          ${$variable}=$value;
                      }   
                      ?>
                      <li>
                          <a href="<?php echo base_url('detail/'.$news_permalink) ?>">
                              <div class="row">
                                  <div class="col-md-3 col-sm-3 news-img">
                                      <div class="img-link-news">
                                          <img src="<?php echo base_url($news_photo) ?>" class="" alt="">
                                      </div>
                                  </div>
                                  <div class="col-md-9 col-sm-9 news-konten">
                                      <h3 class="mb-1"><?php echo $news_title; ?></h3>
                                      <p class="text-muted">
                                          <i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($news_update_datetime)); ?>
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </li>
                      <?php 
                    } ?>
                  </ul>
              </div>
              <hr class="hr-dashed mb-1">
              <center>
                  <a href="<?php echo base_url('archives/berita') ?>" class="btn btn-round-default color-primary">Berita Selengkapnya <i class="fa fa-chevron-right"></i></a>
              </center>
          </div>
      </div>
  </div>
  <?php 
} ?>