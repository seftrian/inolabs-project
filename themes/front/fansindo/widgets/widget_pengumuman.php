<?php
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
if (!empty($results)) 
{   ?>
  <div class="col-sm-5 wow fadeInDown">
      <div class="panel panel-custom">
          <div class="panel-heading heading-custom">
              <h3 class="mb-0"><i class="fa fa-bullhorn"></i> Pengumuman</h3>
          </div>
          <div class="panel-body panel-body-height">
              <div class="slide-berita">
                   <!-- <img src="images/ibadahpuasa.jpg" class="" alt=""> -->
                    <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <?php
                          $no_myCarousel = 0;
                          foreach ($results as $rowArr) 
                          {
                              $myCarousel_active = ($no_myCarousel==0)?'class="active"':'';
                              ?>
                              <li data-target="#myCarousel2" data-slide-to="<?php echo $no ?>" <?php echo $myCarousel_active; ?>></li>
                              <?php 
                              $no_myCarousel++;
                          } ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <?php
                          $no = 0;
                          foreach ($results as $rowArr) 
                          {
                            $no++;
                            foreach ($rowArr as $variable => $value) 
                            {
                                ${$variable}=$value;
                            }   
                            $class_item_active = ($no==1)?'active':'';
                            ?>
                            <div class="item <?php echo $class_item_active ?>">
                                <div class="recent-work-wrap img-slider-2">
                                    <img class="img-responsive" src="<?php echo base_url($news_photo); ?>" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <a href="<?php echo base_url($news_photo); ?>" rel="prettyPhoto" class="icon-custom"><i class="fa fa-search-plus"></i> </a>
                                        </div> 
                                    </div>
                                </div>
                                <div class="conten-slider">
                                    <div class="carousel-caption">
                                        <h3 class="mt-0 mb-0"><a href="<?php echo base_url('detail_pengumuman/'.$news_permalink) ?>"><?php echo ucwords($news_title); ?></a></h3>
                                        <small class="mb-0">
                                            <b>Update :</b>
                                            <i class="fa fa-calendar"></i>
                                            <?php echo date('d M Y',strtotime($news_update_datetime)); ?>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <?php 
                          } ?>
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel2" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>                                                 
                </div>
            </div>
      </div>
  </div>
  <?php
} ?>