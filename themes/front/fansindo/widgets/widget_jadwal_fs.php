<?php if (!empty($results[0]['news_photo'])) {?>
  <!-- about us slider -->
  <div id="about-slider">
    <div id="carousel-slider" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
        <ol class="carousel-indicators visible-xs">
          <?php
          for ($i=0; $i < count($results_more_images)+1; $i++) {
          ?>
          <li data-target="#carousel-slider" data-slide-to="<?php echo $i; ?>" <?php if ($i==0) { ?>class="active"<?php } ?>></li>
          <?php 
          }
          ?>
        </ol>

      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo $base_url.''.$results[0]['news_photo']; ?>" class="img-responsive" alt=""> 
        </div>
        <?php
        foreach ($results_more_images as $rowArr) {
          foreach ($rowArr as $variable => $value) {
              ${$variable}=$value;
          }
          if ($news_more_images_news_id==$results[0]['news_id']) {
        ?>
        <div class="item">
          <img src="<?php echo $base_url.''.$news_more_images_file; ?>" class="img-responsive" alt=""> 
        </div>
        <?php
            }
          }
        ?>
      </div>
      
      <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
        <i class="fa fa-angle-left"></i> 
      </a>
      
      <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
        <i class="fa fa-angle-right"></i> 
      </a>
    </div> <!--/#carousel-slider-->
  </div><!--/#about-slider-->
<?php } ?>
<?php 
if ($results[0]['news_content']!="<p>-</p>") {
  echo html_entity_decode(strip_tags($results[0]['news_content'])); 
}
?>
</div>

<section id="services" class="service-item" style="margin-top: 50px; height: auto; margin-bottom: 50px;padding-bottom: 10px;"><br><br><br>
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>SHARING FS-3 (FANSINDO SENYUM SUPPORT SYSTEM)</h2>
            <p class="lead">Pastikan Anda hadir dalam acara SHARING di kota-kota berikut ini:</p>
        </div>

        <div class="row">
            <?php
            if (!empty($results)) 
            {
              foreach ($results_jadwal as $rowArr) 
              {
                foreach ($rowArr as $variable => $value) 
                {
                  ${$variable}=$value;
                } ?>        
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="" align="center">
                            <img class="img-responsive" src="<?php echo $base_url.''.$news_photo; ?>">
                            <h3 class="media-heading"><?php echo $news_title; ?></h3>
                            <p><?php echo html_entity_decode(strip_tags($news_meta_description)); ?></p>
                            <a href="<?php echo base_url('detail/'.$news_permalink) ?>" class="btn btn-round-2">Selengkapnya <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <?php
              }
            }
            ?>
            <?php
            if($results_jadwal==0){
                $total=3;
            }else{
                $total=3-count($results_jadwal);
            }
            for ($i=0; $i < $total; $i++) 
            { ?>
              <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                      <div class="pull-left">
                          <img class="img-responsive" src="<?php echo $base_url; ?>assets/images/jadwal5.png">
                      </div>
                      <div class="media-body">
                          <h3 class="media-heading"></h3>
                          <p><br></p>
                          <p><br></p>
                      </div>
                  </div>
              </div>
              <?php
            } ?> 
            <div class="col-md-12 text-center">
                <hr class="hr-dashed">
                <a href="<?php echo base_url('archives/event') ?>" class="btn btn-show white">Event Selengkapnya <i class="fa fa-chevron-right"></i></a>
            </div>
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#content-->