<?php if (!empty($results)) { ?>
  <!-- our-team -->
  <div class="team">
    <div class="center wow fadeInDown">
      <h2>TESTIMONIAL</h2>
      <p class="lead">Kesaksian ini diambil tidak ada unsur paksaan dari siapapun</p>
    </div>

    <?php for ($i=0; $i < count($results)-3; $i++) {?>
      <div class="row clearfix">
        <div class="col-md-10 col-md-offset-1">

          <?php if (!empty($results[$i+$i])) {?>
          <div class="col-md-6 col-sm-6"> 
            <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
              <div class="media">
                <div class="pull-left">
                  <a href="#"><img class="media-object" src="<?php echo $base_url.''.$results[$i+$i]['testimonial_avatar_src']; ?>" alt=""></a>
                </div>
                <div class="media-body">
                  <h4><?php echo $results[$i+$i]['testimonial_full_name']; ?></h4>
                  <h5><?php echo $results[$i+$i]['testimonial_title']; ?></h5>
                  <ul class="social_icons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li> 
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div><!--/.media -->
              <div style="text-align: justify;"><?php echo $results[$i+$i]['testimonial_value']; ?></div>
            </div>
          </div><!--/.col-lg-4 -->
          <?php } ?>
          
          <?php if (!empty($results[$i+$i+1])) {?>
          <div class="col-md-6 col-sm-6"> 
            <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
              <div class="media">
                <div class="pull-left">
                  <a href="#"><img class="media-object" src="<?php echo $base_url.''.$results[$i+$i+1]['testimonial_avatar_src']; ?>" alt=""></a>
                </div>
                <div class="media-body">
                  <h4><?php echo $results[$i+$i+1]['testimonial_full_name']; ?></h4>
                  <h5><?php echo $results[$i+$i+1]['testimonial_title']; ?></h5>
                  <ul class="social_icons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li> 
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div><!--/.media -->
              <div style="text-align: justify;"><?php echo $results[$i+$i+1]['testimonial_value']; ?></div>
            </div>
          </div><!--/.col-lg-4 -->
          <?php } ?>

        </div>
      </div> <!--/.row -->
    
    <?php if ($i!=count($results)-4) {?>
      <div class="row team-bar">
        <div class="first-one-arrow hidden-xs">
          <hr>
        </div>
        <div class="first-arrow hidden-xs">
          <hr> <i class="fa fa-angle-up"></i>
        </div>
        <div class="second-arrow hidden-xs">
          <hr> <i class="fa fa-angle-down"></i>
        </div>
        <div class="third-arrow hidden-xs">
          <hr> <i class="fa fa-angle-up"></i>
        </div>
        <div class="fourth-arrow hidden-xs">
          <hr> <i class="fa fa-angle-down"></i>
        </div>
      </div> <!--skill_border--> 
    <?php }
    }?>     

  </div>
<?php } ?>
<br>
<br>
<br><br>
<br>
<br>