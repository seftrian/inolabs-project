    <?php
      $pathImgUrl=$pathImgArr['pathUrl'];
      $pathImgLoc=$pathImgArr['pathLocation'];
      if(!empty($results))
      {
        ?>

    <!-- Slider
    ============================================= -->
    <div class="carousel slide">
        <!-- indicator slider -->
        <ol class="carousel-indicators">
          <?php 
          for ($i=0; $i < count($results); $i++) { 
            if ($i==0) {
          ?>
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
          <?php
            }else{
          ?>
            <li data-target="#main-slider" data-slide-to="<?php echo $i; ?>"></li>
          <?php
            }
          } 
          ?>
        </ol>
        <div class="carousel-inner">
              <?php
              $no = 0;
              foreach($results AS $rowArr){
                $no++;
                foreach($rowArr AS $variable=>$value)
                {
                  ${$variable}=$value;
                }
                $logo_name= (trim($slider_image_src)=='')?'-': pathinfo($slider_image_src,PATHINFO_FILENAME);
                $logo_ext=  pathinfo($slider_image_src,PATHINFO_EXTENSION);
                $logo_file=$logo_name.'.'.$logo_ext;

                $imgProperty=array(
                'width'=>$slider_category_width,
                'height'=>$slider_category_height,
                'imageOriginal'=>$logo_file,
                'directoryOriginal'=>$pathImgLoc,
                'directorySave'=>$pathImgLoc.$slider_category_width.$slider_category_height.'/',
                'urlSave'=>$pathImgUrl.$slider_category_width.$slider_category_height.'/',
                );
                //jika compress
                /*if($slider_category_is_compress=='Y')
                {
                  $img=$this->function_lib->resizeImageMoo($imgProperty);
                }
                else
                {
                  */
                 $img =$pathImgUrl.$logo_file;
                //}
                $valid_url=$this->function_lib->isValidUrl($slider_link);
                $url=($valid_url!=false)?$slider_link:'#';
                $video_link='https://www.youtube.com/embed/'.$slider_youtube_id;
                $description=html_entity_decode($slider_short_description);
              ?>
              
            <div class="item <?php if($no==1){echo"active";} ?>" style="background-image: url(<?php echo $img; ?>)">
                <div class="container">
                    <div class="row slide-margin">
                        <div class="col-sm-4">
                            <div class="carousel-content">
                                <h1 class="animation animated-item-1"><?php echo $slider_title?></h1>
                                <h2 class="animation animated-item-2"><?php echo $description; ?></h2>
                                <a class="btn-slide animation animated-item-3" href="<?php echo $url; ?>">Read More</a>
                            </div>
                        </div>
                        <div class="col-sm-6 hidden-xs animation animated-item-4">
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
            <?php
            }
            ?>
        </div><!--/.carousel-inner-->
    </div><!--/.carousel-->
    <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
        <i class="fa fa-chevron-left"></i>
    </a>
    <a class="next hidden-xs" href="#main-slider" data-slide="next">
        <i class="fa fa-chevron-right"></i>
    </a>
<?php
}
?>
