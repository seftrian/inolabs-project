<?php
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
if (!empty($results)) 
{   ?>
    <section class="">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2 class="special-color"><span>FANSINDO</span> EVENT</h2>
            </div>

            
           <!-- <div class="panel panel-default">
               <div class="panel-heading">
                   adad
               </div>
               <div class="panel-body">
                   adad
               </div>
           </div> -->
            <div class="box-event wow fadeInDown">
                <div class="flex-box-event">
                    <div><h4 class="mb-1 mt-1"><b>Event</b> Terbaru</h4></div>
                    <div><a href="<?php echo base_url('archives/event') ?>" class="btn btn-round-2"><i class="fa fa-calendar"></i> Event Lainnya <i class="fa fa-chevron-right"></i></a></div>
                </div>
            </div>
            <div class="slider-event wow fadeInLeft">
                <div id="myCarousel" class="carousel slide" data-ride="carousel" >
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php
                        $no_myCarousel = 0;
                        foreach ($results as $rowArr) 
                        {
                            $myCarousel_active = ($no_myCarousel==0)?'class="active"':'';
                            ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $no ?>" <?php echo $myCarousel_active; ?>></li>
                            <?php 
                            $no_myCarousel++;
                        } ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $no = 0;
                        foreach ($results as $rowArr) 
                        {
                            $no++;
                            foreach ($rowArr as $variable => $value) {
                                ${$variable}=$value;
                            }   
                            $class_item_active = ($no==1)?'active':'';
                            $day = date('d',strtotime($news_timestamp));
                            $month_year = date('M Y',strtotime($news_timestamp));
                            $datetime1 = new DateTime(date('Y-m-d'));
                            $datetime2 = new DateTime($news_timestamp);
                            $interval = $datetime1->diff($datetime2);
                            $sisa_hari = $interval->format('%a');
                            $tgl_mulai = date('Y-m-d',strtotime($news_timestamp));
                            $label_sisa_hari = (($tgl_mulai==date('Y-m-d'))?'Hari Ini':(($tgl_mulai<date('Y-m-d'))?'Sudah Berlalu':$sisa_hari.' Hari Lagi'));
                            ?>
                            <div class="item <?php echo $class_item_active ?>">
                                <a href="<?php echo base_url('detail/'.$news_permalink) ?>" class="circle-image">
                                    <img src="<?php echo base_url($news_photo) ?>" alt="<?php echo $news_title; ?>">
                                </a>
                                <div class="box-number">
                                    <p class="p-tgl"><?php echo $day; ?></p>
                                    <p class="p-body mb-0"><?php echo $month_year; ?></p>
                                    <p class="p-body"><b><?php echo $label_sisa_hari; ?></b></p>
                                </div>
                                <div class="box-konten">
                                    <h3 class="mb-2 title-h3 color-primary">
                                        <?php echo ucwords($news_title); ?>
                                    </h3>
                                    <p><?php echo $news_meta_description; ?></p>
                                    <br>
                                    <a href="<?php echo base_url('detail/'.$news_permalink) ?>" class="btn btn-show white">Lihat Selengkapnya <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                            <?php
                        }  ?>
                    </div>

                    <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
               
        </div>
    </section>
    <?php
}   ?>
<!-- <section id="feature" >
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>TEMUKAN INTI KESUKSESAN!</h2>
        </div>

        <div class="row">
            <div class="features">
                <?php
                $pathImgUrl=$pathImgArr['pathUrl'];
                $pathImgLoc=$pathImgArr['pathLocation'];
                if (!empty($results)) {
                    foreach ($results as $rowArr) {
                        foreach ($rowArr as $variable => $value) {
                            ${$variable}=$value;
                        }
                ?>
                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="<?php echo $news_external_link_label; ?>"></i>
                        <h2><?php echo $news_title; ?></h2>
                        <h3><?php echo ($news_content); ?></h3>
                    </div>
                </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</section> -->