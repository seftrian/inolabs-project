<section id="portfolio">
    <div class="container">
        <div class="center">
           <h2>FOTO LIPUTAN</h2>
        </div>

        <ul class="portfolio-filter text-center">
            <li><a class="btn btn-default active" href="#" data-filter="*">ALL GALERY</a></li>
            <?php
            if (!empty($results_category)) {
              foreach ($results_category as $rowArr) {
                  foreach ($rowArr as $variable => $value) {
                      ${$variable}=$value;
                  }
              if ($news_label_1!='-') {
            ?>
            <li><a class="btn btn-default" href="#" data-filter=".<?php echo str_replace(' ', '_', strtolower($news_label_1)); ?>"><?php echo strtoupper($news_label_1); ?></a></li>
            <?php } 
                }
              }?>

        </ul><!--/#portfolio-filter-->

    <?php if (!empty($results)) {?>
        <div class="row">
            <div class="portfolio-items">
                <?php foreach ($results as $rowArr) {
                      foreach ($rowArr as $variable => $value) {
                          ${$variable}=$value;
                      }
                ?>
                <div class="portfolio-item <?php echo str_replace(' ', '_', strtolower($news_label_1)); ?> col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="<?php echo $base_url.''.$news_more_images_file; ?>" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#"><?php echo $news_title; ?></a></h3>
                                <a class="preview" href="<?php echo $base_url.''.$news_more_images_file; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                            </div> 
                        </div>
                    </div>
                </div><!--/.portfolio-item-->
                <?php } ?>                                          
            </div>
        </div>
    </div>
  <?php }else{echo "<center>Galeri foto tidak tersedia</center>";}?>
</section><!--/#portfolio-item-->
<br>
<br>
<br>
