<?php

  if(!empty($results['news'])){
    foreach($results['news'] AS $rowArr){
      foreach($rowArr AS $variable=>$value){
        ${$variable}=$value;
      }

      $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
      $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
      $logo_file=$logo_name.'.'.$logo_ext;
      $pathImgUrl=$pathImgArr['pathUrl'];
      $pathImgLoc=$pathImgArr['pathLocation'];

      $content=html_entity_decode($news_content);

      # backgroud
      if($results['bg'] == 'bg-image'){
          $bg_full = "background-image: url('".$pathImgUrl.$logo_file."'); background-size: cover; background-repeat: no-repeat;";
          $bg_color = '';
      } else {
          $bg_full = '';
          $bg_color = $results['bg'];
      }

      # judul color
      if($results['judul_color']){
          $judul_color = 'color-purple';
      } else {
          $judul_color = '';
      }

      # image
      if($results['image']){
          $image_content =  '<img src="'.$pathImgUrl.$logo_file.'" class="img-about m-b-1" alt="'.$news_title.'">';
      } else {
          $image_content = '';
      }

      # sub content
      if(!empty($results['sub_content'])){
          $sub_content = '';
      } else {
          $sub_content = '';
      }



      ?>
      <div class="content-wrap p-t-n p-b-n">
				<div class="container clearfix">
            <?php
            if($results['judul']){
                ?>
                <div class="col_full m-b-0">
      						<div class="fancy-title title-dotted-border title-center">
      							<h2 class="text-center" <?php if($news_external_link_label=="hidden"){ echo "hidden";} ?>><?php echo $news_title; ?></h2>
      						</div>
      					</div>
                <?php
                }
                if(strip_tags($content) != '-'){
                    echo '<p>'.$content.'</p>';
                }
                # sub content
                if(!empty($results['sub_content'])){
                    foreach ($results['sub_content'] as $valArr) {
                ?>
                <p><?php echo $valArr['tab_content']; ?></p>
                <?php
                  }
                }
                $images_contents=$this->master_content_lib->get_more_property($news_id);
          ?>
          <div class="col_full m-b-0">
            <div id="oc-clients-full" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-nav="true" data-pagi="false" data-loop="true" data-autoplay="5000" data-items-xxs="3" data-items-xs="3" data-items-sm="4" data-items-md="4" data-items-lg="5">
              <?php
              if(!empty($images_contents['images'])){
                foreach ($images_contents["images"] as $row_ic) {
              ?>
                  <div class="oc-item">
                    <div class="box-legalitas">
                      <a href="<?php echo base_url().$row_ic['news_more_images_file']; ?>" data-lightbox="image">
                        <img src="<?php echo base_url().$row_ic['news_more_images_file']; ?>" class="img-legalitas img-effect" alt="">
                      </a>
                    </div>
                    <!-- <p class="m-b-0 text-center font-weight-600 m-t-1 legalitas-responsive">Akta</p> -->
                  </div>
              <?php
                }
              }
              ?>
            </div>
          </div>
        </div>
      </div>

      <?php
    }
  }
?>
