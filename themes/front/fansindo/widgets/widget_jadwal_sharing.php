<section id="services" class="service-item">
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>JADWAL SHARING PRODUK</h2>
            <p class="lead">Pastikan Anda hadir dalam acara sharing produk di kota-kota berikut ini:</p>
        </div>

        <div class="row">                              
            <?php
            $pathImgUrl=$pathImgArr['pathUrl'];
            $pathImgLoc=$pathImgArr['pathLocation'];
            if (!empty($results)) {
                foreach ($results as $rowArr) {
                    foreach ($rowArr as $variable => $value) {
                        ${$variable}=$value;
                    }
            ?>
            
            <div class="col-sm-6 col-md-4">
                <div class="media services-wrap wow fadeInDown">
                    <div class="pull-left" align="center">
                        <img class="img-responsive" src="<?php echo $base_url.''.$news_photo; ?>">
                        <h3 class="media-heading"><?php echo $news_title; ?></h3>
                        <?php echo html_entity_decode(strip_tags($news_content)); ?>
                    </div>
                </div>
            </div>
            <?php
                }
            }
            ?>

            <?php
            if($results==0){
                $total=3;
            }else{
                $total=3-count($results);
            }
            for ($i=0; $i < $total; $i++) { 
            ?>
            
            <div class="col-sm-6 col-md-4">
                <div class="media services-wrap wow fadeInDown">
                    <div class="pull-left">
                        <img class="img-responsive" src="<?php echo $base_url; ?>assets/images/jadwal5.png">
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading"></h3>
                        <p><br></p>
                        <p><br></p>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#services-->