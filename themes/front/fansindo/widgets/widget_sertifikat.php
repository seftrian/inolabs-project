
<?php
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
if (!empty($results_2)) 
{ ?>
    <section id="partner" style="margin-bottom:-30px;">
        <div class="container">
            <div class="center wow fadeInDown"  style="padding:30px">   
                <h2>Sertifikasi &amp; Penghargaan</h2>
            </div>   
            <div class="slider-sertifikat wow fadeInLeft desktop">
                <div id="myCarousel4" class="carousel slide" data-ride="carousel" >
                    <ol class="carousel-indicators">
                    </ol>
                    <div class="carousel-inner desktop">
                        <div class="item active">
                            <ul type="none" class="ul-sertifikasi">
                                <?php
                                foreach ($results as $rowArr) 
                                {
                                    foreach ($rowArr as $variable => $value) 
                                    {
                                        ${$variable}=$value;
                                    }      ?>
                                    <li>
                                        <div class="circle-partner">
                                            <img src="<?php echo base_url($news_photo) ?>" alt="">
                                        </div>
                                    </li>
                                    <?php
                                    $no_empat = 1;
                                    foreach ($results_2 as $rowArr) 
                                    {
                                        foreach ($rowArr as $variable => $value) 
                                        {
                                            ${$variable}=$value;
                                        }  
                                        $condition_div = ($no_empat%5==0)?'
                                            </ul>
                                        </div>
                                        <div class="item">
                                            <ul type="none" class="ul-sertifikasi">
                                        ':'';
                                        echo $condition_div; 
                                        ?>
                                        <li>
                                            <div class="circle-partner">
                                                <img src="<?php echo base_url($news_more_images_file) ?>" alt="">
                                            </div>
                                        </li>
                                        <?php 
                                        $no_empat++;
                                    }
                                } ?>
                            </ul>
                        </div>

                    </div>
                    

                    <a class="left carousel-control" href="#myCarousel4" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel4" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div> 

            <div class="slider-sertifikat wow fadeInLeft mobile-1">
                <div id="myCarousel5" class="carousel slide" data-ride="carousel" >
                    <ol class="carousel-indicators">
                        <?php
                        $no_dua = 0;
                        foreach ($results as $rowArr) 
                        {
                            foreach ($rowArr as $variable => $value) 
                            {
                                ${$variable}=$value;
                            }   
                            $myCarousel5 = ($no_satu==0)?'class="active"':'';
                            ?>
                            <li data-target="#myCarousel5" data-slide-to="<?php echo $no_dua ?>" <?php echo $myCarousel5; ?>></li>
                            <?php 
                            $no_dua++;
                        } ?>
                    </ol>
                    
                    <div class="carousel-inner ">
                        <?php
                        $no_tiga = 0;
                        foreach ($results_2 as $rowArr) 
                        {
                            $myCarousel5img = ($no_tiga==0)?'active':'';
                            foreach ($rowArr as $variable => $value) 
                            {
                                ${$variable}=$value;
                            }  ?>                          
                            <div class="item <?php echo $myCarousel5img ?>">
                                <ul type="none" class="ul-sertifikasi">
                                    <li>
                                        <div class="circle-partner">
                                            <img src="<?php echo base_url($news_more_images_file) ?>" alt="">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <?php 
                            $no_tiga++;
                        } ?>
                    </div>
                    <a class="left carousel-control" href="#myCarousel5" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel5" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div><!--/.container-->
    </section><!--/#partner-->
    <?php 
} ?>