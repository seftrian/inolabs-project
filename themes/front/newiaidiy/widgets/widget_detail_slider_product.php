<div class="col-md-12">
    <?php  if(!empty($slider)) { ?>
    <div data-big="3" data-lightbox="gallery">
        <div  class="owl-carousel image-carousel carousel-widget m-b-1 slide-pengumuman" data-margin="60" data-loop="true" data-nav="true" data-autoplay="5000" data-pagi="true" data-items-xxs="1" data-items-xs="1" data-items-sm="1" data-items-md="1" data-items-lg="1" >
            <?php
            foreach ($slider as $key) {?>
                <a href='<?php echo base_url($key['download_file']) ?>' class="box-img-brosur" data-lightbox="gallery-item" title="">
                        <img src="<?php echo base_url($key['download_file']) ?>" alt="">
                            <div class="show-preview" title="Preview Images">
                            <i class="fa fa-search-plus"></i>
                    </div>
                </a>    
           <?php } ?>
        </div>
    </div> 
    <?php } else {?>
        <div class="text-center"><i>Produk Slider tidak ada !</i></div>
  <?php  }?>
</div> 