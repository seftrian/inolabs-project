<?php
$pathImgUrl = $pathImgArr['pathUrl'];
$pathImgLoc = $pathImgArr['pathLocation'];
if (!empty($results)) {
    ?>
    <section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix" height="1000px" data-loop="true" data-autoplay="7000" data-speed="650">
        <div class="slider-parallax-inner">
            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">
                    <?php
                    $no = 0;
                    foreach ($results AS $rowArr) {
                        $no++;
                        foreach ($rowArr AS $variable => $value) {
                            ${$variable} = $value;
                        }
                        $logo_name = (trim($slider_image_src) == '') ? '-' : pathinfo($slider_image_src, PATHINFO_FILENAME);
                        $logo_ext = pathinfo($slider_image_src, PATHINFO_EXTENSION);
                        $logo_file = $logo_name . '.' . $logo_ext;

                        $imgProperty = array(
                            'width' => $slider_category_width,
                            'height' => $slider_category_height,
                            'imageOriginal' => $logo_file,
                            'directoryOriginal' => $pathImgLoc,
                            'directorySave' => $pathImgLoc . $slider_category_width . $slider_category_height . '/',
                            'urlSave' => $pathImgUrl . $slider_category_width . $slider_category_height . '/',
                        );
                        $img = $pathImgUrl . $logo_file;
                        //}
                        $valid_url = $this->function_lib->isValidUrl($slider_link);
                        $url = ($valid_url != false) ? $slider_link : '#';
                        $video_link = 'https://www.youtube.com/embed/' . $slider_youtube_id;
                        $description = html_entity_decode($slider_short_description);
                        ?>
                        <div class="swiper-slide dark" style="background-image: url(<?php echo $img; ?>);"></div>
                        <?php
                    }
                    ?>
                </div>
                <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
                <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
                <div id="slide-number">
                    <div id="slide-number-current"></div><span>/</span>
                    <div id="slide-number-total"></div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
            <div class="bg-shadow"></div>
        </div>
    </section>
<?php
}?>