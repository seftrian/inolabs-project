<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
?>
<header class="header-electronics">
    <div class="default-header">
        <div class="container header-flex">
            <div class="sticky-header">
                <div class="row align-items-start align-items-md-center header-middle ">
                    <!-- logo -->
                    <div class="col-md-5 col-6 order-1 order-md-1 logo-mobile">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url($websiteConfig['config_logo']); ?>"></a>
                        <a href="<?php echo base_url(); ?>" class="logo-menu"><img src="<?php echo base_url($websiteConfig['config_ym_offline']); ?>"></a>
                    </div>
                    <!-- /logo -->
                    <div class="col-md-12 col-12 order-3 order-md-2 d-flex-menu">
                        <a href="<?php echo base_url(); ?>" class="logo-menu"><img src="<?php echo base_url($websiteConfig['config_logo']); ?>"></a>
                        <a href="<?php echo base_url(); ?>" class="logo-menu"><img src="<?php echo base_url($websiteConfig['config_ym_offline']); ?>"></a>
                        <div class="d-flex-menu">
                            <nav>
                                <ul class="main-menu" type="none">
                                    <?php
                                    $uri_1 = $this->uri->segment(1);
                                    $uri_2 = $this->uri->segment(2);
                                    if (!empty($results)) {
                                        foreach ($results as $rowArr) {
                                            foreach ($rowArr as $variable => $value) {
                                                ${$variable} = $value;
                                            }
                                            if ($menu_administrator_par_id == 0 and $menu_administrator_is_active == 1 and $menu_administrator_link != "#") {
                                                $label_title_menu = (strtolower($menu_administrator_title) == 'home') ? '<i class="fa fa-home"></i>' : $menu_administrator_title;
                                                $uri_1 = $this->uri->segment(1);
                                                ?>
                                                <li class="<?php
                                                if ($url == base_url() . '' . $menu_administrator_link) {
                                                    echo"active";
                                                }
                                                ?>">
                                                    <a href="<?php echo base_url() . '' . $menu_administrator_link; ?>"><?php echo $label_title_menu; ?></a>
                                                </li>
                                                <?php
                                            } elseif ($menu_administrator_par_id == 0 and $menu_administrator_is_active == 1) {
                                                $url_convert = strtolower(str_replace(' ', '_', $menu_administrator_title));
                                                if ($uri_2 == $url_convert) {
                                                    $sub_active_status = 'active';
                                                } else {
                                                    $sub_active_status = '';
                                                }
                                                ?>
                                                <li class="dropdown <?php echo $sub_active_status ?>">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $menu_administrator_title; ?> <i class="fa fa-angle-down"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <?php
                                                        foreach ($results as $thisvalue) {
                                                            if ($thisvalue['menu_administrator_par_id'] == $menu_administrator_id) {
                                                                ?>
                                                                <li class="<?php
                                                                if ($url == base_url() . '' . $thisvalue['menu_administrator_link']) {
                                                                    echo"active";
                                                                }
                                                                ?>"><a href="<?php echo base_url() . '' . $thisvalue['menu_administrator_link']; ?>"><?php echo $thisvalue['menu_administrator_title']; ?></a></li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                    </ul>
                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                            </nav>
                            <div class="header-akun">
                                <a href="http://member.zeelora.id" class="btn btn-akun">LOGIN</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</header>