<div class="col-lg-2 col-sm-6">
    <div class="single-footer-widget mb-20">
        <h4 class="menu-footer">Menu</h4>
        <ul type="none">
            <?php foreach ($results as $key) { ?>
                <li><a href="<?php echo base_url($key['menu_administrator_link']) ?>"><?php echo $key['menu_administrator_title'] ?></a></li>
            <?php } ?>            
        </ul>
    </div>
</div>