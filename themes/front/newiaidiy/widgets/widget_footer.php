<?php
$w_lib = new widget_lib;

?>
<footer class="curved-border-footer">
        <div class="container">
            <div class="footer-widget">
                <div class="row">
                    <!-- bagian about -->
                    <?php
                        $w_lib->run('front', 'master_content', 'widget_footer_about');
                        $w_lib->run('front', 'master_content', 'widget_footer_menu');
                        $w_lib->run('front', 'master_content', 'widget_globals', 'contact', 'contact_us');
                        $w_lib->run('front', 'master_content', 'widget_globals', 'footer', 'social_media');
                        $w_lib->run('front', 'master_content', 'widget_footer_rekening');
                    ?>
                     <!-- end -->
                </div>
            </div>
        </div>
</footer>
<div class="footer-bottom">
    <?php
        $websiteConfig = $this->system_lib->getWebsiteConfiguration();
        echo $websiteConfig['config_footer']; 
     ?>
</div>

