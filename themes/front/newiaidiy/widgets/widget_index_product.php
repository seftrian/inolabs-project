<div class="content-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-20 text-judul text-center"><span class="font-weight-light">Produk</span> <span class="font-weight-bold ">Zeelora</span></h2>
                <div class="width-konten text-dark">
                    <p><span class="font-weight-semibold">4 Produk Unggulan</span> yang telah diproduksi dan dipasarkan antara lain :</p>
                </div>
            </div>
            <?php if (!empty($results)) {?>
                <div class="col-md-12">
                    <div data-big="3" data-lightbox="gallery">
                        <div  class="owl-carousel image-carousel carousel-widget m-b-1" data-margin="60" data-loop="true" data-nav="true" data-autoplay="5000" data-pagi="true" data-items-xxs="1" data-items-xs="1" data-items-sm="1" data-items-md="2" data-items-lg="2" >
                                <?php foreach ($results as $key => $rowArr1) { ?>  
                                    <a href="<?php echo!empty($imgSub[$key]['news_more_images_file']) ? base_url($imgSub[$key]['news_more_images_file']) : ''?>" class="box-img-brosur" data-lightbox="gallery-item" title=""> 
                                        <img src="<?php echo base_url($rowArr1['news_photo'])  ?>" alt="<?php $rowArr1['news_title']?>">
                                        <div class="show-preview" title="DMOF">
                                            <i class="fa fa-search-plus"></i>
                                        </div>
                                    </a>  
                                <?php }?>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
</div>

