<?php
$w_lib = new widget_lib;
?>
<section id="content">
        <div class="content-wrap bg-blue-gradation">
        <div class="container">
            <div class="row">
            <div class="col-sm-12 text-center z-index-22">
                <h2 class="mb-40 text-judul font-weight-light text-white"><?php echo $judul[0] ?> <b class="font-weight-bold">&nbsp;<?php echo $judul[1]?> </b></h2>
                <div class="width-konten">
                        <?php
                            $w_lib->run('front', 'master_content', 'widget_globals', 'profil', 'tentang_zeelora');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>