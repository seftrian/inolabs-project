<?php
    $w_lib = new widget_lib;
?>
<div class="col-lg-3 col-sm-6">
    <div class="box-logo-footer">
        <img src="<?php echo base_url($results[0]['news_photo']) ?>" class="logo-footer" alt="">
    </div>
    <div class="alamat-footer text-justify">
        <?php
            $isi = $results[0]['news_content'];
            echo strip_tags(html_entity_decode($isi));
        ?>
    </div>
</div>
