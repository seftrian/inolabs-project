<?php
$pathImgUrl = $pathImgArr['pathUrl'];
$pathImgLoc = $pathImgArr['pathLocation'];
$data_modal = '';
?>
<style type="text/css">
    .modal-promo-popup {
        display: block;
        position: fixed;
        z-index: 1;
        padding-top: 100px;
        left :0;
        top:0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgb(0, 0, 0);
        background-color: rgb(0, 0, 0, 0.9);
    }
    .modal-content {
        margin :auto;
        display: block;
        width:180%;
        max-width: 600px;
        border-radius: 0 !important;
    }

    .caption {
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    .modal-content, .caption {
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    .caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }
    @-webkit-keyframes zoom {
        from {-webkit-transform:scale(0)}
        to {-webkit-transform:scale(1)}
    }
    @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
    }
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 90%;
        }
    }

</style>
<div id="myModal" class="modal-promo-popup">
    <div class="modal-content">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background-image: url(https://www.inolabs.net/assets/images/e7cd5-promo-inohospital-slider.jpg)">
                    <div class="swiper-content" style="margin-left: 19px;">
                        <h3 class="animation animated-item-1 text-white mt-0"><b>Example Data</b></h3>
                        <h4 class="animation animated-item-2 text-white mt-0 font-weight-light">Apoteker Indonesia Bersama Masyarakat Melawan Covid-19</h4>
                    </div>

                </div>
            </div>
            <div class="swiper-slide" style="background-image: url(https://www.inolabs.net/assets/images/2fbae-banner-jogja-menolong-3.jpg)">
                <div class="swiper-content" style="margin-left: 19px;">
                    <h3 class="animation animated-item-1 text-white mt-0"><b>Example Data</b></h3>
                    <h4 class="animation animated-item-2 text-white mt-0 font-weight-light">Apoteker Indonesia Bersama Masyarakat Melawan Covid-19</h4>
                </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next"><i class="fa fa-chevron-right"></i></div>
            <div class="swiper-button-prev"><i class="fa fa-chevron-left"></i></div>
        </div>
    </div>
    <div class="caption">
        <span id="close" style="color:yellow !important;font-size:20px;">
            <i class="fa fa-times-circle" aria-hidden="true"></i> Close
        </span>

    </div>
</div>