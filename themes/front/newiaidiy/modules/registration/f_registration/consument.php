
<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
        <div class="container">
          <div class="row">

            <div class="col-md-12">
            <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="#"><?php echo $seoTitle?></a></li>
                </ul>
            


              <!--// Blog Large //-->
              <div class="col-md-4">
                <div class="kd-section-title"><h3><?php echo $seoTitle?></h3></div>
                <div class="kd-blog-list kd-blogmedium">
                  <div class="row">
                        <article class="col-md-12">
                          <div class="alert"  style="display:none;"></div>      
                           <form id="form-registration-consument" action="" method="post">
                           <div class="form-group">
                             <label>Email *</label> 
                             <input name="member_email" type="email" class="form-control" required/>
                           </div>
                           <div class="form-group">
                             <label>Password *</label> 
                             <input type="password" name="member_password" class="form-control"  required/>
                           </div>
                           <div class="form-group">
                             <label>Konfirmasi Password *</label> 
                             <input type="password" name="member_password_repeat" class="form-control"  required/>
                           </div>
                           <div class="form-group">
                             <label>Nama Lengkap *</label> 
                             <input type="text" name="person_full_name" class="form-control"  required/>
                           </div>
                           <div class="form-group">
                             <label>No. Hp. *</label> 
                             <input type="text" name="person_phone" class="form-control"  required/>
                           </div>
                           <div class="form-group">
                             <label>Jenis Kelamin *</label> 
                             <select class="form-control" name="person_gender" required>
                             <option value="">Pilih jenis kelamin</option>
                             <option value="L">Laki-laki</option>
                             <option value="P">Perempuan</option>
                             </select>
                           </div>
                           <div class="form-group" id="input_verification_code_value" style="display:none;">
                             <label>Kode Verifikasi</label> 
                            <input type="hidden" value="0" name="hidden_verification" />
                            <input type="text" id="input_verification_code_value" name="verification_code_value" class="form-control" placeholder="kode verifikasi" required/>
                           </div>
                           <div class="form-group">
                             <a href="">Batal</a>
                             <button onclick="registration_consument();return false;" style="margin-left:20px;" class="btn btn-success">Daftar</button>
                           </div>
                           </form>
                        </article>
                  </div>
                </div>
                
              </div>
              <!--// Blog Large //-->
               <div class="col-md-4">
                <div class="kd-section-title"><h4>Belum Terdaftar?</h4></div>
                <p class="text-success">
                Dapatkan penawaran terbaik dan nikmati pengalaman memesan lebih cepat dengan menjadi
                member kami. 
                </p>         
              </div>
              <aside class="col-md-4">

                <div class="kd-login-network">
                  <ul>
                    <li><a href="<?php echo base_url()?>vo_consument/login" class="btn btn-info"><i class="fa fa-key"></i> Login</a></li>
                    <li><center><h3>atau</h3></center></li>
                    <li><a href="#" data-original-title="Facebook" class="btn btn-sm"><i class="fa fa-facebook"></i> Login dengan Facebook</a></li>
                  </ul>
                </div>
              </aside>

            </div>

          </div>
        </div>
      </section>
      <!--// Page Section //-->