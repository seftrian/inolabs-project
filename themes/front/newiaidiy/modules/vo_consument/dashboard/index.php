<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
    <div class="container">
      <div class="row">
         <ul class="breadcrumb">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="#">Dashboard</a></li>
        </ul>
        <div class="col-md-3">
           <?php 
              $this->widget_lib->run('front', 'vo_consument', 'widget_vo_consument_menu');
             ?>
        </div>
        <div class="col-md-9">
          <div class="kd-rich-editor">
            <h3><?php echo $seoTitle?></h3>
            <article style="margin-top:10px;">
            <p >
                Halo <?php echo $session_arr['person_full_name']?>, Selamat Datang! <br />
                Login terakhir anda <?php echo convert_datetime($session_arr['member_last_login_datetime'],'','','ina')?>
            </p>
            </article>
          </div>
        </div>

      </div>
    </div>
</section>