
<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
  <ul class="breadcrumb">
              <li><a href="<?php echo base_url()?>">Home</a></li>
              <li><a href="#"><?php echo $seoTitle?></a></li>
          </ul>

        <!--// Blog Large //-->
        <div class="col-md-4">
        
          <div class="kd-section-title"><h3><?php echo $seoTitle?></h3></div>
          <div class="kd-blog-list kd-blogmedium">
            <div class="row">
              
                  <article class="col-md-12">
                    <?php
                    //untuk menampilkan pesan
                    if (trim($message) != '') 
                    {
                      ?>
                      <div class="alert <?php echo ($status==200)?'alert-success':'alert-danger'?>"><?php echo $message?></div>      
                      <?php
                    }
                    ?>

                   
                     <form id="form-registration-consument" action="" method="post">
                     <div class="form-group">
                       <label>Email *</label> 
                       <input name="member_email" value="<?php echo $this->input->post('member_email')?>" type="email" class="form-control" required/>
                     </div>
                     <div class="form-group">
                       <label>Password *</label> 
                       <input type="password" name="member_password" class="form-control"  required/>
                     </div>
                     <?php
                      //jika salah > 2x maka tampilkan captcha
                      if(isset($_SESSION['fail_login_perawat']) AND $_SESSION['fail_login_perawat']>2)
                      {
                        ?>
                        <div class="form-group form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <input placeholder="Kode Unik..." class="form-control qwerty ui-keyboard-input ui-widget-content ui-corner-all" type="text" name="captcha" id="kodeunik" autocomplete="off" aria-haspopup="true" role="textbox" required/>
                                </div>
                                <div class="col-md-6" style="padding:0;">
                                    <a onclick="captcha_reload();return false;" class="btn btn-xs btn-danger" style="cursor:pointer;"><i class="fa fa-refresh"></i></a>
                                    <img id="captcha_image" src="<?php echo site_url('vo_perawat/captcha'); ?>" alt="" style="border:0;" />
                                </div>
                            </div>
                        </div>
                        <?php
                      }
                      ?>
                     <div class="form-group">
                       <input type="hidden" value="1" name="do_login">
                       <div class="row">
                                <div class="col-md-6">
                                     <a href="#">Lupa kata sandi?</a>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                   <button style="margin-left:20px;" class="btn btn-success">Masuk</button>
                                </div>
                        </div>
                     </div>

                     </form>
                  </article>
     
              
            </div>
          </div>
          
        </div>
        <div class="col-md-4">
           <div class="kd-section-title"><h4>Belum Terdaftar?</h4></div>
          <p class="text-success">
          Dapatkan penawaran terbaik dan nikmati pengalaman memesan lebih cepat dengan menjadi
          member kami. 
          </p>         
        </div>
        <div class="col-md-4">
            <div class="kd-login-network">
              <ul>
                <li><a href="<?php echo base_url()?>registration/consument" class="btn btn-info"><i class="fa fa-user"></i> Registrasi</a></li>
                <li><center><h3>atau</h3></center></li>
                <li><a href="#" data-original-title="Facebook" class="btn btn-sm"><i class="fa fa-facebook"></i> Login dengan Facebook</a></li>
              </ul>
            </div>
        </div>

      </div>

    </div>
  </div>
</section>
<!--// Page Section //-->
