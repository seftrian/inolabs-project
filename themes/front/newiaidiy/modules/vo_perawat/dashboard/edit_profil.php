<?php
$getImagePath=$this->master_perawat_helper_lib->getImagePath();
$pathImgUrl=$getImagePath['pathUrl'];
$pathImgLoc=$getImagePath['pathLocation'];
$logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;

$imgProperty=array(
'width'=>263,
'height'=>263,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'263263/',
'urlSave'=>$pathImgUrl.'263263/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
?> 
 <section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
        <div class="container">
          <div class="row">
             <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>vo_perawat/dashboard/profil">Profil</a></li>
                    <li><a href="#">Edit Profil</a></li>
            </ul>
            <div class="col-md-3">
                <?php 
              $this->widget_lib->run('front', 'vo_perawat', 'widget_vo_perawat_menu');
             ?>
            </div>

            <div class="col-md-9">
             <h3><?php echo $seoTitle?></h3>
            <p class="text-warning">
               Mohon isi form dengan tanda (*).
               <br />
               Setiap melakukan perubahan jangan lupa menekan tombol simpan.
            </p>
            <?php
                    //untuk menampilkan pesan
                    if (trim($message) != '') 
                    {
                      ?>
                      <div class="alert <?php echo ($status==200)?'alert-success':'alert-danger'?>"><?php echo $message?></div>      
                      <?php
                    }
                    ?> 
            <div class="alert" id="message-ajax" style="display:none;"></div>
            <div class="tabbable kd-tab kd-horizontal-tab">
                <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue"  role="tablist" id="myTab4">
                    <li class="active">
                        <a data-toggle="tab" href="#panel_edit_account">
                            Profil
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#panel_edit_salary">
                            Gaji
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#panel_edit_area">
                            Wilayah
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#panel_edit_about_you">
                            Tentang Anda
                        </a>
                    </li>
                </ul>
            <form action="" method="post" enctype="multipart/form-data" role="form" id="form">
              <div class="row">
                        <div class="col-md-4" style="margin-top: 50px;">
                            <a href="<?php echo base_url()?>vo_perawat/dashboard/profil" onclick="return confirm('Batalkan?');" style="margin-right: 20px;">Cancel</a>
                            <input type="submit" name="save" value="Simpan" onclick="return confirm('Simpan?');"  class="btn btn-info btn-sm" />
                        </div>
                    </div>
                <div class="tab-content">
                        <div id="panel_edit_account" class="tab-pane in active">
                            <?php require_once('profile.php');?>
                        </div>
                        <div id="panel_edit_salary" class="tab-pane">
                            <?php require_once('salary.php');?>
                        </div>
                        <div id="panel_edit_area" class="tab-pane">
                            <?php require_once('area.php');?>
                        </div>
                        <div id="panel_edit_about_you" class="tab-pane">
                            <?php require_once('about_you.php');?>
                        </div>

                     <div class="row">
                        <div class="col-md-4" style="margin-top: 50px;">
                            <a href="<?php echo base_url()?>vo_perawat/dashboard/profil" onclick="return confirm('Batalkan?');" style="margin-right: 20px;">Cancel</a>
                            <input type="submit" name="save" value="Simpan" onclick="return confirm('Simpan?');"  class="btn btn-info btn-sm" />
                        </div>
                    </div>
                </div>
            </form>

            </div>
            <hr />
                                     
            </div>
          </div>
        </div>
      </section>