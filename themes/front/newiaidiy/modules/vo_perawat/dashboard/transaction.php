<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
    <div class="container">
      <div class="row">
         <ul class="breadcrumb">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>vo_consument/dashboard/index">Dashboard</a></li>
                <li><a href="#">Transaksi</a></li>
        </ul>
        <div class="col-md-3">
            <?php 
              $this->widget_lib->run('front', 'vo_perawat', 'widget_vo_perawat_menu');
             ?>
        </div>
        <div class="col-md-9">
          <div class="kd-rich-editor">
            <h3><?php echo $seoTitle?></h3>
            <article style="margin-top:10px;">
             <form action="" id="form-search" method="get" class="form-horizontal" enctype="multipart/form-data">
              <div class="col-sm-6">   
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Nomor Invoice
                    </label>
                    <div class="col-sm-8">
                        <input name="code" value="<?php echo $this->input->get('code')?>" type="text" id="code" class="form-control" />
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Status Pesanan
                    </label>
                    <div class="col-sm-8">
                        <select name="status" id="status" class="form-control">
                            <option value="">Semua</option>
                            <option value="pending" <?php echo ($this->input->get('status')=='pending')?'selected':''?>>Pending</option>
                            <option value="confirmed" <?php echo ($this->input->get('status')=='confirmed')?'selected':''?>>Confirmed</option>
                            <option value="paid" <?php echo ($this->input->get('status')=='paid')?'selected':''?>>Paid</option>
                            <option value="sent" <?php echo ($this->input->get('status')=='sent')?'selected':''?>>Sent</option>
                            <option value="received" <?php echo ($this->input->get('status')=='received')?'selected':''?>>Received</option>
                        </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Tgl. Pesanan
                    </label>
                    <div class="col-sm-4">
                          <input name="start_date" value="<?php echo $this->input->get('start_date')?>"  data-date-format="yyyy-mm-dd" data-date-viewmode="years" type="text" id="start_date" class="form-control date-picker" placeholder="Tgl. Mulai">
                    </div>
                     <div class="col-sm-4">
                          <input name="end_date" value="<?php echo $this->input->get('end_date')?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years" type="text" id="end_date" class="form-control date-picker" placeholder="Tgl. Akhir">
                    </div>
                 </div>
                 
                </div>
              
                <div class="col-sm-12"> 
                    <div class="form-group">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-2">
                            <a href="<?php echo base_url().'vo_consument/dashboard/transaction/'?>">Clear</a>
                            <button style="float:right;" class="btn btn-info" onclick="grid_reload();return false;" ><i class=" clip-search"></i> Cari</button>
                        </div>       
                    </div>  
                </div>
            </form>

            <table class="table">
            <thead>
            <tr>
                <th style="width:30px;text-align: center;">No.</th>
                <th style="width:30px;text-align: center;">Detil</th>
                <th style="width:130px;text-align: center;">Tgl. Pesanan</th>
                <th style="width:30px;text-align: center;">Status</th>
                <th style="text-align: center;">Kode Pesanan</th>
                <th style="width:130px;text-align: center;">Total Belanja</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($results))
            {
                $no=1+$offset;    
                foreach($results AS $rowArr)
                {
                    foreach($rowArr AS $variable=>$value)
                    {
                        ${$variable}=$value;
                    }
                    $transaction_status=($transaction_status=='confirmed')?'<span style="text-decoration: blink;color:red;">'.$transaction_status.'</span>':$transaction_status;
                    $view='<a title="Lihat detil pesanan" href="'.base_url().'vo_perawat/dashboard/transaction_detail/'.$transaction_id.'" class="btn btn-xs btn-success"><span class="fa fa-cart"></span> Detil</a>';
                    $position_title=$this->function_lib->get_one('transaction_detail_position_title','ecom_transaction_detail','transaction_detail_transaction_id='.intval($transaction_id));
                    $product_title=$this->function_lib->get_one('transaction_detail_product_title','ecom_transaction_detail','transaction_detail_transaction_id='.intval($transaction_id));
                    $label_product=($transaction_type=='service')?$position_title:$product_title;
                   // $link_to_payment=($transaction_status=='pending')?'<a class="btn btn-xs btn-warning" href="'.base_url().'vo_consument/dashboard/payment?code='.$transaction_code.'">'.$transaction_status.'</a>':$transaction_status;
                    $label_class_status=($transaction_status=='pending')?'alert-warning':(
                        ($transaction_status=='canceled')?'alert-danger':'alert-success'
                        );    
                    ?>
                    <tr>
                    <td><?php echo $no?>.</td>
                    <td><?php echo $view?></td>
                    <td><?php echo convert_date($transaction_datetime,'','','ina')?></td>
                    <td><?php echo '<span class="'.$label_class_status.'">'.$transaction_status.'</span>'?></td>
                    <td><?php echo $transaction_code.'<br /><span class="text-warning">'.$label_product.'</span>'?></td>
                    <td style="text-align: right;">Rp <?php echo function_lib::currency_rupiah($transaction_total_due)?></td>
                    </tr>
                    <?php
                    $no++;
                }
            }
            else
            {
                ?>
                <tr>
                    <td colspan="6" style="text-align: center;">Data tidak tersedia</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
             <div class="pagination-wrap">
                  <div class="pagination">
                  <?php echo $link_pagination?>
                  </div>
                </div>
            </article>
          </div>
        </div>

      </div>
    </div>
</section>