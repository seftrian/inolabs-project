<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
    <div class="container">
      <div class="row">
         <ul class="breadcrumb">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>vo_consument/dashboard/transaction">Transaksi</a></li>
                <li><a href="#">Detil</a></li>
        </ul>
        <div class="col-md-3">
            <?php 
              $this->widget_lib->run('front', 'vo_perawat', 'widget_vo_perawat_menu');
             ?>
        </div>
        <div class="col-md-9">
          <div class="kd-rich-editor">
            <h3><?php echo $seoTitle?></h3>
           
           <div class="row">
    <div class="col-md-12">
          
          <div class="invoice">
                        <div class="row invoice-logo">
                            <div class="col-sm-6">
                                <p>
                                    #<?php echo $transaction_code?> / <?php echo convert_datetime($transaction_datetime);?> <span> <?php echo $transaction_note?> </span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Pengguna Jasa:</b>
                                <div class="well">
                                    <address>
                                        <strong><?php echo $person_full_name?></strong>
                                        <br />
                                        <?php echo function_lib::get_person_addres($person_id)?>
                                        <br />
                                        No. Hp. <?php echo function_lib::get_person_phone($person_id)?>
                                        <br />

                                            
                                    </address>
                                    <address>
                                        <strong>E-mail</strong>
                                        <br>
                                        <?php echo function_lib::get_person_email_addres($person_id)?>
                                    </address>
                                </div>
                            </div>
                               <div class="col-sm-4">
                                <b>Konfirmasi Pembayaran Terakhir:</b>
                                <div class="well">
                                
                                        <strong>Tgl Transfer:</strong> <?php echo isset($invoice_payment['payment_from_bank'])?convert_date($invoice_payment['payment_date'],'','','ina'):'-'?>
                                   <br />
                                        <strong>Bank Pengirim:</strong> <?php echo isset($invoice_payment['payment_from_bank'])?$invoice_payment['payment_from_bank']:'-'?>
                                    <br />
                                        <strong>Bank Tujuan:</strong> <?php echo isset($invoice_payment['payment_from_bank'])?$invoice_payment['payment_to_bank']:'-'?>
                                    <br />
                                        <strong>Nominal Transfer:</strong> <?php echo isset($invoice_payment['payment_from_bank'])?'Rp '.function_lib::currency_rupiah($invoice_payment['payment_value']):'-'?>
                                    <br />
                                        <strong>Keterangan:</strong> <?php echo isset($invoice_payment['payment_from_bank'])?$invoice_payment['payment_note']:'-'?>
                                    <br />
                                        <strong>Status:</strong> <span id="payment_status"><?php echo isset($invoice_payment['payment_status'])?$invoice_payment['payment_status']:'-'?></span>
                                        <span id="payment_status_detail" style="<?php echo (isset($invoice_payment['payment_status']) AND $invoice_payment['payment_status']!='pending')?'':'display:none;'?>">
                                         <?php echo isset($invoice_payment['payment_status'])?'by '.$invoice_payment['payment_response_administrator_username'].' at '.convert_datetime($invoice_payment['payment_response_datetime']):''?>
                                        </span>
                                        <br />
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <b> Pembayaran: </b>
                                <div class="well">
                                <?php
                                if(!empty($termin_arr))
                                {
                                    foreach($termin_arr AS $rowArr)
                                    {
                                        foreach($rowArr AS $variable=>$value)
                                        {
                                            ${$variable}=$value;
                                        }
                                        ?>
                                            <strong><?php echo $termin_label?></strong>
                                            <p class="text-warning">
                                                Rp <?php echo function_lib::currency_rupiah($termin_nominal)?> (<?php echo ($termin_status=='paid')?'<b>'.$termin_status.'</b>':$termin_status?>)<br />
                                            Batas Waktu: <?php echo convert_date($termin_start_date,'','','ina')?> s/d <?php echo convert_date($termin_end_date,'','','ina')?>
                                            </p>
                                            <br />
                                        <?php
                                    }
                                }
                               
                                ?>
                                </div>

                            </div>
                           
                        </div>
                        <hr />
                        <div class="row" id="div_load_perawat_by_id">
                                  <?php
                                  if(!empty($perawat_arr))
                                  {
                                    foreach($perawat_arr AS $rowArr)
                                    {
                                        foreach($rowArr AS $variable=>$value)
                                        {
                                            ${$variable}=$value;
                                        }

                                        $pathImgArr=$this->master_perawat_lib->getImagePath();
                                        $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
                                        $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
                                        $logo_file=$logo_name.'.'.$logo_ext;
                                        $pathImgUrl=$pathImgArr['pathUrl'];
                                        $pathImgLoc=$pathImgArr['pathLocation'];
                                        $imgProperty=array(
                                        'width'=>150,
                                        'height'=>150,
                                        'imageOriginal'=>$logo_file,
                                        'directoryOriginal'=>$pathImgLoc,
                                        'directorySave'=>$pathImgLoc.'150150/',
                                        'urlSave'=>$pathImgUrl.'150150/',
                                        );
                                        $img=$this->function_lib->resizeImageMoo($imgProperty);

                                        ?>

                                        <div class="col-sm-6">
                                        <div class="col-md-4">
                                        <span style="text-align: center;"><img src="<?php echo $img?>" alt="<?php echo $person_full_name?>"></span>
                                         <p class="text-warning" style="text-align: center;"><?php echo $medical_master_employment?></p>
                                        </div>
                                        <div class="col-md-8">
                                        Nama : <?php echo $person_full_name?>
                                        <br />No. Hp. : <?php echo $person_phone?>
                                        <br />Jenis Kelamin: <?php echo $person_gender?>

                                        <div id="has-canceled" style="<?php echo ($transaction_perawat_status=='canceled')?'':'display:none;'?>">
                                            <i class="clip-close text-danger"></i> <b class="text-danger">Dibatalkan</b> <br />
                                            <span id="note-canceled" class="text-danger">
                                                <?php echo $transaction_perawat_note?>, <?php echo function_lib::time_elapsed_string($transaction_perawat_datetime)?>.
                                            </span>
                                        </div>
                                        <div id="status-approved" style="<?php echo ($transaction_perawat_status=='approved')?'':'display:none;'?>">
                                        <span class="text-success"><i class="fa fa-check-circle"></i> Telah Disetujui, <?php echo function_lib::time_elapsed_string($transaction_perawat_datetime)?>. </span><br />
                                        </div>  
                                        </div>
                                        
                                      
                                        </div>
                                        <?php

                                    }
                                  }
                                  ?>  
                                  
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:40px;"> # </th>
                                            <th style="text-align: center;width:400px;"> Layanan</th>
                                            <th style="text-align: center;" class="hidden-480"> Harga (Rp)</th>
                                            <th  style="text-align: center;"class="hidden-480"> Diskon (Rp)</th>
                                            <th style="text-align: center;" class="hidden-480"> Harga Akhir (Rp)</th>
                                            <th style="text-align: center;" class="hidden-480"> Jml </th>
                                            <th style="text-align: center;"> Total (Rp)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                   // print_r($invoice_item);
                                    if(!empty($invoice_item))
                                    {
                                        $no=1;
                                        foreach($invoice_item AS $rowArr)
                                        {
                                            foreach($rowArr AS $variable=>$value)
                                            {
                                                ${$variable}=$value;
                                            }
                                            
                                            $label_product=($transaction_type=='service')?$transaction_detail_position_title:$transaction_detail_product_title;
                                            $product_overview=($transaction_type=='service')?'':$this->function_lib->get_one('product_overview','sys_product','product_id='.intval($transaction_detail_product_id));
                                            ?>
                                            <tr>
                                            <td style="text-align: left;"> <?php echo $no?>. </td>
                                            <td > <?php echo $label_product?> <p class="text-warning"><?php echo $product_overview?></p></td>
                                            <td class="hidden-480" style="text-align: right;"> <?php echo function_lib::currency_rupiah($transaction_detail_unit_price)?> </td>
                                            <td class="hidden-480" style="text-align: right;"> <?php echo function_lib::currency_rupiah($transaction_detail_unit_discount)?> </td>
                                            <td class="hidden-480" style="text-align: right;"> <?php echo function_lib::currency_rupiah($transaction_detail_unit_nett_price)?> </td>
                                            <td class="hidden-480" style="text-align: right;"> <?php echo ($transaction_detail_quantity*1)?>  <?php echo $transaction_detail_unit?></td>
                                            <td  style="text-align: right;"> <?php echo function_lib::currency_rupiah($transaction_detail_subtotal_price)?> </td>
                                        </tr>
                                            <?php
                                            $no++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 invoice-block" style="text-align: right;font-size:14px;">
                                        <strong>Total Belanja:</strong> Rp <?php echo function_lib::currency_rupiah($transaction_total_price)?>
                                    <br />
                                        <strong>Total Diskon:</strong> Rp <?php echo function_lib::currency_rupiah($transaction_total_discount)?>
                                    <br />
                                        <strong>Total Tagihan:</strong>  Rp <?php echo function_lib::currency_rupiah($transaction_total_due)?>
                            </div>
                        </div>
                        <hr />
                    </div>

    </div>
</div>



          </div>
        </div>

      </div>
    </div>
</section>