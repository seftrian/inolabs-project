<div class="row">
    <div class="col-md-5 ">
      <p class="text-warning">Cara mengisi wilayah:<br />
      
      Untuk penempatan seluruh Indonesia, dapat memilih Seluruh Wilayah Indonesia.<br />
      Untuk penempatan berdasarkan provinsi, dapat memilih Beberapa Wilayah.
      </p>
      </div>
    <div class="col-md-7">
    <strong>Wilayah Penempatan</strong>
      <div class="row">
          <div class="col-sm-6" style="margin-bottom:20px;">
              <select onchange="show_option_area($(this));" class="form-control" name="medical_location_type">
                <option value="">Pilih Penempatan</option>
                <option value="country" <?php echo (master_perawat_lib::check_location_type($person_id,'country')==true)?'selected':''?>>Seluruh Wilayah Indonesia</option>
                <option value="province" <?php echo (master_perawat_lib::check_location_type($person_id,'province')==true)?'selected':''?>> Beberapa Wilayah</option>
              </select> 
          </div>
      </div>
    	<div class="div_area show_area" style="<?php echo (master_perawat_lib::check_location_type($person_id,'province')==true)?'':'display:none;'?>">
            <input type="hidden" id="current_number_modal" value="0">
            <input type="hidden" id="current_number_area" value="<?php echo (count($load_area_arr)>0)?count($load_area_arr):0?>">
      		  
            <?php
            if(!empty($load_area_arr))
            {
              $no=0;
              foreach($load_area_arr AS $rowArr)
              {
                foreach($rowArr AS $variable=>$value)
                {
                  ${$variable}=$value;
                }
                $area_name=$this->function_lib->get_area_name($medical_location_area_id);
                ?>
                <div class="row">
                  <input type="hidden" name="medical_location_id[<?php echo $no?>]" value="<?php echo $medical_location_id;?>">
                      <div class="col-md-1">
                            <div class="form-group">
                                <label class="control-label">
                               <a href="#" onclick="remove_field($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                      </div>
                       <div class="row col-sm-1">
                          <a  data-toggle="modal" onclick="load_area(<?php echo $no?>);return false;" href="#load-area" href="#" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
                      </div>
                      <div class="col-sm-7">
                        <input type="hidden" name="medical_location_area_id[<?php echo $no?>]" value="<?php echo $medical_location_area_id?>" id="area_id_<?php echo $no?>" class="form-control" /> 
                        <input type="text" class="form-control area_name" name="area_name[<?php echo $no?>]" id="area_name_<?php echo $no?>" value="<?php echo $area_name?>" readonly="" /> 
                      </div>
                     
                </div>
                <?php
                $no++;
              }
            }
            ?>
      </div>   
      
      <div class="row show_area" style="<?php echo (master_perawat_lib::check_location_type($person_id,'province')==true)?'':'display:none;'?>">
          <div class="col-md-6">
              <div class="form-group">
                  <label class="control-label">
          				<a href="#" onclick="add_field_area();return false;" class="btn btn-xs btn-beige"><i class="fa  fa-plus-circle"></i> Tambah Baris</a>
                  </label>
              </div>
          </div>
      </div>
    </div>
   

</div>

<div id="load-area" class="modal fade" data-width="40%" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-body">

    </div>
    <div class="modal-footer">
        <button style="float:left;" type="button" data-dismiss="modal" class="btn btn-danger btn-default">
            Close
        </button>
    </div>
</div>

<script type="text/javascript">
/**fungsi menambah field*/
function add_field_area()
{
  var current_number=$("#current_number_area").val();
  var next_number=parseInt(current_number)+1;

  var html_field= '<div class="row">'
                  +'<input type="hidden" name="medical_location_id['+current_number+']" value="0">'
                  +'    <div class="col-md-1">'
                  +'          <div class="form-group">'
                  +'              <label class="control-label">'
                  +'             <a href="#" onclick="remove_field($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>'
                  +'              </label>'
                  +'          </div>'
                  +'    </div>'
                  +'    <div class="row col-sm-1">'
                  +'        <a  data-toggle="modal" onclick="load_area('+current_number+');return false;" href="#load-area" href="#" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>'
                  +'    </div>'
                  +'    <div class="col-sm-7">'
                  +'      <input type="hidden" name="medical_location_area_id['+current_number+']" value="0" id="area_id_'+current_number+'" class="form-control" /> '
                  +'      <input type="text" class="form-control area_name" name="area_name['+current_number+']" id="area_name_'+current_number+'" value="" readonly="" /> '
                  +'    </div>'
                
                  +'</div>';

    $(".div_area").append(html_field);      
    $("#current_number_area").val(next_number);
    return false;
}


//gunakan area
function load_area(current_number)
{
    var person_id='<?php echo $person_id?>';
    //modal yang sedang aktif
    $("#current_number_modal").val(current_number);
    $.get('<?php echo base_url().'vo_perawat/dashboard'?>/load_area?person_id='+person_id,function(response_html){
        $("#load-area").find(".modal-body").html(response_html);
    });
}

function show_option_area(elem)
{
  var option_value=elem.val();
  var div_area=$(".show_area");
  if(option_value=='province')
  {
    div_area.slideDown('medium');
  }
  else
  {
    div_area.slideUp('medium');
  }
}
</script>