<div class="row">
    <div class="col-md-12">
    <div class="alert alert-info"><strong>Perjam</strong></div>

    	<div class="div_hour">
      <input type="hidden" id="current_number_hour" value="<?php echo (count($salary_hour_arr)>0)?count($salary_hour_arr):0?>">
      	<?php

      	if(!empty($salary_hour_arr))
      	{
      		$no=0;
      		foreach($salary_hour_arr AS $rowArr)
      		{
      			foreach($rowArr AS $variable=>$value)
      			{
      				${$variable}=$value;
      			}
      			?>
      			<div class="row">
        		<input type="hidden" name="medical_rate_id_hour[<?php echo $no?>]" value="<?php echo $medical_rate_id?>">
            <div class="col-md-7">
                <div class="form-group">
                    <label class="control-label">
            				<a href="#" onclick="remove_field($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                    <span class="symbol required"></span>    Posisi
                    </label>
                    <select name="medical_rate_position_id_hour[<?php echo $no?>]" class="form-control">
                    	<option value="-" <?php echo (isset($_POST['medical_rate_position_id_hour'][$no]) AND ($_POST['medical_rate_position_id_hour'][$no])=='-')?'selected':'';?>>Pilih Posisi</option>
                   		<?php
                    	if(!empty($position_arr))
                    	{
                    		foreach($position_arr AS $rowArr)
                    		{
                    			foreach($rowArr AS $variable=>$value)
                    			{
                    				${$variable}=$value;
                    			}
                    			$selected=($medical_rate_position_id==$position_id)?'selected':'';
                    			?>
                    			<option value="<?php echo $position_id?>" <?php echo $selected;?>><?php echo $position_title?></option>
                    			<?php
                    		}
                    	}
                    	?>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">
                    <span class="symbol required"></span>    Nominal (Rp)
                    </label>
                    <input type="text" value="<?php echo isset($_POST['medical_rate_value_hour'][$no])?$_POST['medical_rate_value_hour'][$no]:$medical_rate_value;?>" name="medical_rate_value_hour[<?php echo $no?>]" class="form-control currency_rupiah" />
                </div>
            </div>
        </div>
      			<?php
      			$no++;
      		}
      	}
      
      	?>
        
      </div>   
      
      <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                  <label class="control-label">
          				<a href="#" onclick="add_field('hour');return false;" class="btn btn-xs btn-beige"><i class="fa  fa-plus-circle"></i> Tambah Baris</a>
                  </label>
              </div>
          </div>
      </div>
    </div>
    <hr />
    <div class="col-md-12">
    <div class="alert alert-info"><strong>Perhari</strong></div>
       <div class="div_day">
      <input type="hidden" id="current_number_day" value="<?php echo (count($salary_day_arr)>0)?count($salary_day_arr):0?>">
        <?php
        if(!empty($salary_day_arr))
        {
          $no=0;
          foreach($salary_day_arr AS $rowArr)
          {
            foreach($rowArr AS $variable=>$value)
            {
              ${$variable}=$value;
            }
            ?>
             <div class="row">
            <input type="hidden" name="medical_rate_id_day[<?php echo $no?>]" value="<?php echo $medical_rate_id?>">
            <div class="col-md-7">
                <div class="form-group">
                    <label class="control-label">
                    <a href="#" onclick="remove_field($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                    <span class="symbol required"></span>    Posisi
                    </label>
                    <select name="medical_rate_position_id_day[<?php echo $no?>]" class="form-control">
                      <option value="-" <?php echo (isset($_POST['medical_rate_position_id_day'][$no]) AND ($_POST['medical_rate_position_id_day'][$no])=='-')?'selected':'';?>>Pilih Posisi</option>
                      <?php
                      if(!empty($position_arr))
                      {
                        foreach($position_arr AS $rowArr)
                        {
                          foreach($rowArr AS $variable=>$value)
                          {
                            ${$variable}=$value;
                          }
                          $selected=($medical_rate_position_id==$position_id)?'selected':'';
                          ?>
                          <option value="<?php echo $position_id?>" <?php echo $selected;?>><?php echo $position_title?></option>
                          <?php
                        }
                      }
                      ?>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">
                    <span class="symbol required"></span>    Nominal (Rp)
                    </label>
                    <input type="text" value="<?php echo isset($_POST['medical_rate_value_day'][$no])?$_POST['medical_rate_value_day'][$no]:$medical_rate_value;?>" name="medical_rate_value_day[<?php echo $no?>]" class="form-control currency_rupiah" />
                </div>
            </div>
        </div>
            <?php
            $no++;
          }
        }
       
        ?>
        
      </div>   
      
      <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                  <label class="control-label">
                  <a href="#" onclick="add_field('day');return false;" class="btn btn-xs btn-beige"><i class="fa  fa-plus-circle"></i> Tambah Baris</a>
                  </label>
              </div>
          </div>
      </div>

    </div>
    <hr />
  	<div class="col-md-12">
    <div class="alert alert-info"><strong>Perbulan</strong></div>
      <div class="div_month">
        <input type="hidden" id="current_number_month" value="<?php echo (count($salary_month_arr)>0)?count($salary_month_arr):0?>">
          <?php
          if(!empty($salary_month_arr))
          {
            $no=0;
            foreach($salary_month_arr AS $rowArr)
            {
              foreach($rowArr AS $variable=>$value)
              {
                ${$variable}=$value;
              }
              ?>
               <div class="row">
              <input type="hidden" name="medical_rate_id_month[<?php echo $no?>]" value="<?php echo $medical_rate_id?>">
              <div class="col-md-7">
                  <div class="form-group">
                      <label class="control-label">
                      <a href="#" onclick="remove_field($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                      <span class="symbol required"></span>    Posisi
                      </label>
                      <select name="medical_rate_position_id_month[<?php echo $no?>]" class="form-control">
                        <option value="-" <?php echo (isset($_POST['medical_rate_position_id_month'][$no]) AND ($_POST['medical_rate_position_id_month'][$no])=='-')?'selected':'';?>>Pilih Posisi</option>
                        <?php
                        if(!empty($position_arr))
                        {
                          foreach($position_arr AS $rowArr)
                          {
                            foreach($rowArr AS $variable=>$value)
                            {
                              ${$variable}=$value;
                            }
                            $selected=($medical_rate_position_id==$position_id)?'selected':'';
                            ?>
                            <option value="<?php echo $position_id?>" <?php echo $selected;?>><?php echo $position_title?></option>
                            <?php
                          }
                        }
                        ?>
                      </select>
                  </div>
              </div>
              <div class="col-md-5">
                  <div class="form-group">
                      <label class="control-label">
                      <span class="symbol required"></span>    Nominal (Rp)
                      </label>
                      <input type="text" value="<?php echo isset($_POST['medical_rate_value_month'][$no])?$_POST['medical_rate_value_month'][$no]:$medical_rate_value;?>" name="medical_rate_value_month[<?php echo $no?>]" class="form-control currency_rupiah" />
                  </div>
              </div>
          </div>
              <?php
              $no++;
            }
          }
         
          ?>
          
      </div>   
      
      <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                  <label class="control-label">
                  <a href="#" onclick="add_field('month');return false;" class="btn btn-xs btn-beige"><i class="fa  fa-plus-circle"></i> Tambah Baris</a>
                  </label>
              </div>
          </div>
      </div>
    </div>
</div>

<script src="<?php echo base_url();?>assets/js/autoNumeric.js"></script>

<script type="text/javascript">
$(function(){
	currency_rupiah();

});

function currency_rupiah()
{
    var myOptions = {aSep: '.', aDec: ',',vMin: '-999999999.99'};
    $('.currency_rupiah').autoNumeric('init', myOptions); 
}
function remove_field(elem)
{
	var parent=elem.parent().parent().parent().parent();
	parent.remove();
	return false;
}

var position_arr='<?php echo json_encode($position_arr)?>';
var position_arr_json=jQuery.parseJSON(position_arr);
/**fungsi menambah field*/
function add_field(div_class)
{
	var current_number=$("#current_number_"+div_class).val();
	var next_number=parseInt(current_number)+1;

	var html_field= '  <div class="row"><input type="hidden" name="medical_rate_id_'+div_class+'['+current_number+']" value="0">'
            			+'<div class="col-md-6">'
                	+'<div class="form-group">'
                  +'<label class="control-label">'
            			+'<a href="#" onclick="remove_field($(this));return false;"  class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>'
                  +'<span class="symbol required"></span>    Posisi'
                  +'</label>'
                  +'<select id="medical_rate_position_id_'+div_class+'_'+current_number+'" name="medical_rate_position_id_'+div_class+'['+current_number+']" class="form-control">'
                  +'  	<option value="-">Pilih Posisi</option>'
                  +'  </select>'
                	+'	</div>'
            			+'</div>'
            			+'<div class="col-md-6">'
                	+'<div class="form-group">'
                  +'<label class="control-label">'
                  +'<span class="symbol required"></span>    Nominal (Rp)'
                  +'</label>'
                  +'<input type="text" name="medical_rate_value_'+div_class+'['+current_number+']" class="form-control currency_rupiah" />'
                	+'</div>'
            			+'</div>'
        					+'</div>';

    $(".div_"+div_class).append(html_field);    	
    $("#current_number_"+div_class).val(next_number);
    currency_rupiah();

    /**
      get json master position
    */
    //$.getJSON('<?php echo base_url()?>master_perawat/service_rest/master_position/',function(results){
      $.each(position_arr_json,function(index,rowArr){
        console.log(rowArr);
        $('#medical_rate_position_id_'+div_class+'_'+current_number).append('<option value="'+rowArr['position_id']+'">'+rowArr['position_title']+'</option>');
      });
    //});
		return false;
}

</script>