<?php
$getImagePath=$this->master_perawat_helper_lib->getImagePath();
$pathImgUrl=$getImagePath['pathUrl'];
$pathImgLoc=$getImagePath['pathLocation'];
$logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;

$imgProperty=array(
'width'=>263,
'height'=>263,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'263263/',
'urlSave'=>$pathImgUrl.'263263/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
?> 
 <section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
        <div class="container">
          <div class="row">
             <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>vo_perawat/dashboard/index">Dashboard</a></li>
                    <li><a href="#">Profil</a></li>
            </ul>
            <div class="col-md-3">
               <?php 
              $this->widget_lib->run('front', 'vo_perawat', 'widget_vo_perawat_menu');
             ?>
            </div>

            <div class="col-md-9">

              <div class="kd-rich-editor">
                <h3><?php echo $person_full_name?> <a href="<?php echo base_url()?>vo_perawat/dashboard/edit_profil" class="btn btn-xs btn-info"><i class="fa fa-edit"></i> Edit Profil</a></h3>
                <div class="col-sm-2">
                Email
                </div>
                <div class="col-sm-10">
                : <?php echo function_lib::get_person_email_addres($person_id)?>
                </div>
                <div class="col-sm-2">
                Jenis Kelamin
                </div>
                <div class="col-sm-10">
                : <?php echo ($person_gender=='L')?'Laki-laki':(
                    ($person_gender=='P')?'Perempuan':''
                  )?>
                </div>
                <div class="col-sm-2">
                No. Hp. 
                </div>
                <div class="col-sm-10">
                : <?php echo (trim($person_phone)!='')?$person_phone:''?>
                </div>
                <div class="col-sm-2">
                Agama
                </div>
                <div class="col-sm-10">
                : <?php echo (trim($person_religion)!='')?$person_religion:''?>
                </div>
                <div class="col-sm-2">
                Alamat
                </div>
                <div class="col-sm-10">
                : <?php echo function_lib::get_person_addres($person_id);?>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>