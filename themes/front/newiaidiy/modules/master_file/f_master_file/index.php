<div class="row single-post">
          <div class="col-sm-12">
              <ul class="breadcrumb">
                  <li><a href="<?php echo base_url()?>">Home</a></li>
                  <li><a href="#">Download File</a></li>
                </ul>
                <div class="arsip-section">
                <h2 class="arsip"><i class="glyphicon glyphicon-cloud-download"></i> Download File</h2>
                <?php
                if(empty($results))
                {
                		?>
                		 <div class="media">
		                 <p>Maaf, file belum dimuat.</p>
		                </div>
                		<?php
                }
                else
                {

                	 foreach($results AS $rowArr)
				            {
				              foreach($rowArr AS $variable=>$value)
				              {
				                ${$variable}=$value;
				              }

				              $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
				              $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
				              $logo_file=$logo_name.'.'.$logo_ext;
				              $pathImgUrl=$pathImgArr['pathUrl'];
				              $pathImgLoc=$pathImgArr['pathLocation'];
				            
				              $dateformat=convert_datetime_name($download_timestamp, $lang = 'id', $type = 'text', $formatdate = ' ', $formattime = ':') ;
				          //    $link_detail=master_content_lib::link_detail($news_id, $news_permalink);
				            	$content=strip_tags(html_entity_decode($download_content));
				            	$content=(strlen($content)<170)?$content:substr($content,0,170).'...';
				            	$file_name=(strlen($logo_name)>15)?substr(ucwords($logo_name),0,15).'[...]':ucwords($logo_name);
				            	$btn_download='<button class="btn btn-warning btn-xs" onclick="window.location.href=\''. base_url().'master_file/f_master_file/download_file/'.$download_id.'\';return false;"><span class="glyphicon glyphicon-download"></span> ('.$file_name.'.'.$logo_ext.')</button>';
            					if(file_exists($pathImgLoc.$logo_file) AND trim($logo_file)!='')
            					{
            						?>
            						   	 <div class="media">
			                  <div class="media-body">
			                    <h5 class="media-heading"><?php echo $download_title?> <span style="float:right;"><?php echo $btn_download?></span></h5>
			                    <div class="post-meta">
			                           <?php echo $dateformat?> 
			                  </div>
			                    <?php echo $content;?>
			                  </div>
			                </div>
            						<?php
            					}
            					
				            }
                	?>
                
                	<?php
                }
                ?>
           			<p>
                <nav>
                	<center><?php echo $link_pagination?></center>
                </nav>
                </p>
             </div>
            </div>
        </div>    