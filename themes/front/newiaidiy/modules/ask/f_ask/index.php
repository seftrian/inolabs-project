<?php
$w_lib=new widget_lib;
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
?>
<!--title-->
<section id="content" class="">
  <div class="content-wrap wrap-top-only" style="background:url('<?php echo $themes_inc; ?>images/bg/gplaypattern.png'); background-repeat:repeat">
    <div class="container clearfix">
      <h2 class="m-b-0 text-center">Contact</h2>
    </div>
  </div>
</section>
<!--/title-->
<!-- Content
============================================= -->
<section id="content">
  <div class="content-wrap p-t-n p-b-n">
    <div class="container clearfix">
      <div class="row">
        <div class="col_full">
          <div class="panel panel-map">
            <?php $w_lib->run('front', 'master_content', 'widget_address_map');?>
          </div>
        </div>
        <div class="col_full m-b-0">
          <div class="row">
            <div class="col-md-7 m-b-0">
              <div class="fancy-title title-dotted-border title-center">
                <h3 class="text-center">Comment</h3>
              </div>
              <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>
              <div class="alert" style="display:none;"></div>
              <form id="template-contactform" action="include/sendemail.php" class="m-b-0" method="post">
                <div class="form-group">
                    <label for="formGroupExampleInput">Nama</label>
                    <input type="text" class="form-control" id="full_name" name="full_name">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Email</label>
                        <input type="email" class="form-control" id="from" name="from">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Subjek</label>
                        <select name="service" id="service" class="form-control" >
                          <option value="">Pilih Pertanyaan</option>
                          <option value="keluhan">Keluhan</option>
                          <option value="saran">Saran</option>
                          <option value="kerjasama">Penawaran Kerja Sama</option>
                          <option value="dll">Dll</option>
                        </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput">Pesan Anda</label>
                    <textarea name="message" id="message" cols="30" rows="5" class="form-control"></textarea>
                </div>
                <button onclick="send_message();return false;" name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-3d button-amber nomargin">Send Message</button>
              </form>
            </div>

            <div class="col-md-5 col_last m-b-0">
              <div class="fancy-title title-dotted-border title-center">
                <h3 class="text-center">Contact</h3>
              </div>
              <p class="margin-bottom text-center"><b><?php echo $websiteConfig['config_description'].'<br> '.$websiteConfig['config_address'].'<br> '.$websiteConfig['config_city_name'].'<br> '.$websiteConfig['config_country_name'] ?></b></p>
              <ul type="none" class="font-size-16">
                <li class="li-list">
                  <i class="fa fa-envelope-o icon-list color-brown"></i> <?php echo $websiteConfig['config_email']?>
                </li>
                <hr class="hr-dashed m-b-1 m-t-1">
                <li class="li-list">
                  <i class="fa fa-commenting icon-list color-brown"></i> <?php echo $websiteConfig['config_telephone']?>
                </li>
                <hr class="hr-dashed m-b-1 m-t-1">
                <li class="li-list">
                  <i class="fa fa-tty icon-list color-brown"></i> <?php echo $websiteConfig['config_phone']?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content-wrap p-t-n p-b-n">
    <div class="container clearfix">
      <div class="row">

      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $("#template-contactform").validate({
      submitHandler: function(form) {
          $('.form-process').fadeIn();
          $(form).ajaxSubmit({
              target: '#contact-form-result',
              success: function() {
                  $('.form-process').fadeOut();
                  $('#template-contactform').find('.sm-form-control').val('');
                  $('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
                  SEMICOLON.widget.notifications($('#contact-form-result'));
              }
          });
      }
  });
  function send_message()
  {
      $("div.alert").show();
       var jqxhr=$.ajax({
        url:'<?php echo base_url()?>ask/service_rest/send_message_visitor',
        type:'post',
        dataType:'json',
        data:$("form#template-contactform").serializeArray(),
      });
      jqxhr.success(function(response){
        if(response['status']!=200)
       {
        $("div.alert").removeClass('alert-success');
        $("div.alert").addClass('alert-danger');
        $("div.alert").html(response['message']);
       }
       else
       {
          $("div.alert").removeClass('alert-danger');
          $("div.alert").addClass('alert-success');
          $("div.alert").html(response['message']);
          $("form#template-contactform").find('input[type="text"]').val('');
          $("form#template-contactform").find('input[type="email"]').val('');
          $("form#template-contactform").find('textarea').val('');
          setTimeout(function(){
              $("div.alert").slideUp('medium');
          },5000);
       }
      });
      jqxhr.error(function(response){
       alert('an error has occurred, please try again.');
      });

    return false;

  }
</script>
