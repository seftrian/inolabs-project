<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$w_lib=new widget_lib;
$uri_1=$this->uri->segment(1);
$uri_2=$this->uri->segment(2);
$uri_3=$this->uri->segment(3);
$themes_arr=$this->config->item('theme');
$themes_inc=base_url().'themes/front/'.$themes_arr['frontend']['name'].'/inc/';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo isset($seoTitle) ? $seoTitle:$websiteConfig['config_name']; ?></title>

    <!-- Css Folder -->
    <link href="<?php echo $themes_inc; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/color.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>style.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/responsive.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/themetypo.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/bxslider.css" rel="stylesheet">
    <link href="<?php echo $themes_inc; ?>css/datepicker.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo $themes_inc; ?>js/jquery.js"></script>
  </head>
  <body>
    <!--// Wrapper //-->
    <div class="wrapper">

      <!--// Main Content //-->
      <div class="main-content">


           <article class="col-md-12" id="div-form-order">
           <?php
      if($consument_id>0)
      {
        ?>
        <p>Anda login sebagai <strong><?php echo $person_full_name?></strong>.<br />
        </p>
        <?php
      }
      ?>
           <div class="alert"  style="display:none;"></div>      
           <form id="form-registration-consument" action="" method="post">
            <table class="kd-table kd-tabletwo">
            <tbody>
            <tr>
              <input type="hidden" name="ref" value="no_registration">
              <td style="width:15%;">Layanan *</td>
              <td colspan="2">
              <select name="product_id" onchange="load_detail_product($(this).val());return false;" class="form-control">
              <?php
              if(!empty($product_arr))
              {
                foreach($product_arr AS $rowArr)
                {
                  foreach($rowArr AS $variable=>$value)
                  {
                    ${$variable}=$value;
                  }
                  $selected=($current_product_id==$product_id)?'selected':'';
                  ?>
                  <option value="<?php echo $product_id?>" <?php echo $selected?>><?php echo $product_name?></option>
                  <?php
                }
              }
              ?>
              </select>
              </td>
              <td ><span id="total_due"></span>
              </td>
            </tr>
            <tr>
              <input type="hidden" name="ref" value="no_registration">
              <td style="width:15%;">Email *</td>
              <td style="width:35%;"><input name="member_email" value="<?php echo ($consument_id>0)?$member_email:''?>" type="email" class="form-control" required/></td>
              <td style="width:15%;">Nama Lengkap *</td>
              <td style="width:35%;"><input type="text" value="<?php echo ($consument_id>0)?$person_full_name:''?>" name="person_full_name" class="form-control"  required/></td>
            </tr>
            <tr>
              <td style="width:15%;">Alamat *</td>
              <td style="width:35%;"><textarea name="additional_info_address" class="form-control" required></textarea> </td>
              <td style="width:15%;">Agama *</td>
              <td style="width:35%;">
              <input type="text" name="person_religion"value="<?php echo ($consument_id>0)?$person_religion:''?>"  class="form-control" required/>
              </td>
            </tr>
            <tr>
              <td>No. Hp. *</td>
              <td> 
                <input name="person_phone" value="<?php echo ($consument_id>0)?$person_phone:''?>"  type="text" class="form-control" required/>
              </td>
              <td>Jenis Kelamin *</td>
              <td>
                <select class="form-control" name="person_gender" required>
                     <option value="">Pilih jenis kelamin</option>
                     <option value="L" value="<?php echo ($consument_id>0)?(
                      ($person_gender=='L')?'selected':''
                     ):''?>" >Laki-laki</option>
                     <option value="P" <?php echo ($consument_id>0)?(
                      ($person_gender=='P')?'selected':''
                     ):''?>>Perempuan</option>
                </select>
              </td>
            </tr>
             <tr>
               <td>Keterangan Tentang pasien *</td>
              <td colspan="3"><textarea name="note_pasien" class="form-control" required></textarea> </td>
            </tr>
            </tbody>
            </table>
             
           <div class="form-group" style="text-align: right;margin:10px;">
             <button onclick="stored_ecom_product();return false;" style="margin-left:20px;" class="btn btn-success">Daftar</button>
           </div>
            </form>
          </article>
      </div>
  

    </div>


    <!-- jQuery (Necessary For JavaScript Plugins) -->
    <script src="<?php echo $themes_inc; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $themes_inc; ?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo $themes_inc; ?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo $themes_inc; ?>js/waypoints-min.js"></script>
    <script src="<?php echo $themes_inc; ?>js/functions.js"></script>
     <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?php echo base_url()?>addons/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="<?php echo base_url()?>addons/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url()?>addons/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   
    <?php
      echo isset($footerScript) ? $footerScript : '';
    ?>  
  </body>
</html>