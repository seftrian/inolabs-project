<?php
$results_position_rate=$this->master_perawat_helper_lib->get_position($person_id);
$label_location_duty=$this->master_perawat_helper_lib-> label_location_duty($person_id,'no limit');
?>
<table class="kd-table kd-tabletwo table-hover">
  <thead>
    <tr>
      <th style="width:50%;text-align: center;">Posisi</th>
      <th style="text-align: center;">Per Jam</th>
      <th style="text-align: center;">Per Hari</th>
      <th style="text-align: center;">Per Bulan</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($results_position_rate))
      {
        foreach($results_position_rate AS $rowArr)
        {
          foreach($rowArr AS $variable=>$value)
          {
            ${$variable}=$value;
          }
          $rate_hour=$this->master_perawat_helper_lib->get_rate_by_person_position_type($person_id,$medical_rate_position_id,'hour');
          $rate_day=$this->master_perawat_helper_lib->get_rate_by_person_position_type($person_id,$medical_rate_position_id,'day');
          $rate_month=$this->master_perawat_helper_lib->get_rate_by_person_position_type($person_id,$medical_rate_position_id,'month');
          ?>
          <tr>
            <td><?php echo $position_title?></td>
            <td style="text-align: right;">Rp. <?php echo function_lib::currency_rupiah($rate_hour);?></td>
            <td style="text-align: right;">Rp. <?php echo function_lib::currency_rupiah($rate_day);?></td>
            <td style="text-align: right;">Rp. <?php echo function_lib::currency_rupiah($rate_month);?></td>
          </tr>
          <?php
        }
      }
      ?>
     
    </tbody>
</table>
<?php
echo '<p><i title="Penempatan" class="fa fa-globe"></i> '.$label_location_duty.'</p>';
?>