<?php
$w_lib=new widget_lib;

?>
<section class="kd-pagesection" style=" padding: 0px 0px 0px 0px; background: #ffffff; ">
        <div class="container">
          <div class="row">

            <div class="col-md-12">

              <!--// Blog Large //-->
              <div class="col-md-8">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="#">Perawat</a></li>
                </ul>
                <div class="kd-section-title"><h3>Perawat</h3></div>
                <div class="kd-blog-list kd-blogmedium">
                  <div class="row">
                 
                  <article class="col-md-4">
                    <select name="sort_by" onchange="rekap_search($(this));return false;">
                      <option value="rate_min">Harga - Rendah ke Tinggi</option>
                      <option value="rate_max">Harga - Tinggi ke Rendah</option>
                      <option value="rating">Rating</option>
                    </select>
                    </article>
                    <?php
                    $getImagePath=$this->master_perawat_helper_lib->getImagePath();
                    $pathImgUrl=$getImagePath['pathUrl'];
                    $pathImgLoc=$getImagePath['pathLocation'];
                    if(!empty($results))
                    {
                      foreach($results AS $rowArr)
                      {
                        foreach($rowArr AS $variable=>$value)
                        {
                          ${$variable}=$value;
                        }
                        $icon_has_experienced=($medical_master_has_experienced=='Y')?'<i class="fa fa-heart-o"></i> Berpengalaman':'<i class="fa fa-graduation-cap"></i> Baru Lulus';
                        $label_location_duty=$this->master_perawat_helper_lib->label_location_duty($person_id);
                        $label_position=$this->master_perawat_helper_lib->label_position($person_id);
                        $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
                        $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
                        $logo_file=$logo_name.'.'.$logo_ext;
                        
                        $imgProperty=array(
                        'width'=>255,
                        'height'=>191,
                        'imageOriginal'=>$logo_file,
                        'directoryOriginal'=>$pathImgLoc,
                        'directorySave'=>$pathImgLoc.'255191/',
                        'urlSave'=>$pathImgUrl.'255191/',
                        );
                        $img=$this->function_lib->resizeImageMoo($imgProperty);
                        $medical_master_experience=($medical_master_has_experienced=='Y')?$medical_master_experience:'';
                        $text_about=strip_tags(html_entity_decode($medical_master_about.' '.$medical_master_experience.' '.$medical_master_skills));
                        $text_about=(strlen($text_about)>206)?substr($text_about,0,206).'...':$text_about;
                        $minimal_rate=$this->master_perawat_helper_lib->label_minimal_rate($person_id);
                        $link_profil=master_perawat_helper_lib::link_profil($person_id);
                        ?>
                        <article class="col-md-12">
                          <div class="bloginner">
                            <figure><a href="<?php echo $link_profil?>"><img src="<?php echo $img?>" alt="<?php echo $person_full_name?>"></a>
                             <span class="kd-usernetwork">
                                <ul class="kd-blogcomment">
                                  <li><span title="Tarif mulai dari "><i class="fa fa-money"></i><?php echo $minimal_rate?></span></li>
                                  <li><?php echo $icon_has_experienced?> </li>
                                </ul>
                              </span>
                            </figure>

                            <section class="kd-bloginfo">
                              <h2><a href="<?php echo $link_profil?>"><i title="Terverifikasi" class="fa fa-check-circle"></i> <?php echo $person_full_name?></a> </h2>
                              <ul class="kd-postoption">
                                <li><?php echo $medical_master_education?></li>
                                <li style="float:right;"><button onclick="add_to_cart_nakes($(this),<?php echo $person_id?>,true);" class="custom-btn btn-xs">Pilih</button></li>
                              </ul>
                              
                              <p><?php echo $text_about?><a href="<?php echo $link_profil?>" title="selengkapnya">[selengkapnya]</a></p>
                              
                              <p class="kd-usernetwork"> 
                              <i class="fa fa-globe"></i> <?php echo $label_location_duty?><br />
                              <i class="fa fa-stethoscope"></i> <?php echo $label_position?><br />
                              </p>
                              
                            </section>
                          </div>
                        </article>
                        <?php
                      }
                    }
                    ?>
                    
                    
                  </div>
                </div>
                <div class="pagination-wrap">
                  <div class="pagination">
                    <?php echo $link_pagination?>
                  </div>
                </div>
              </div>
              <!--// Blog Large //-->

              <aside class="col-md-4">
                <!--widget search perawat-->
                  <?php 
                    $w_lib->run('front', 'master_perawat', 'widget_sidebar_search_perawat');
                  ?>
              </aside>

            </div>

          </div>
        </div>
      </section>
      <!--// Page Section //-->