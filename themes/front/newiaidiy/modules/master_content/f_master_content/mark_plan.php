<section id="content">
        <div class="content-wrap">
            <div class="container">  
                <div class="display-pdf-marketing"></div>
            </div>
        </div>
</section>
<script src="<?php echo $themes_inc; ?>js/jquery-1.11.1.min.js"></script>
<script>
getDataMarkPlan();
function getDataMarkPlan() {
    $.ajax({
        type: "post",
        url: "<?php echo base_url() ?>mark-plan/get-data-json-file-mark-plan",
        data: "data",
        dataType: "json",
        success: function (response) {
            if (response['status'] == 200) {
               const docFile = response['data'][0]['download_file'];
               console.log(response);
               if (docFile != '') {
                    html = '';
                    const baSeURL = '<?php echo base_url() ?>'
                    $.each(response['data'], function (i, row) { 
                        html += '<div class="row">'
                        html += '<div class="col-md-12 mb-40">'
                            html += '<h2 class="mb-20 text-judul text-center"><span class="font-weight-bold ">'+ row['news_title'] +'</span></h2>'
                        html += '</div>'
                        html += '<div class="col-sm-12">'
                            html += '<object data=' + baSeURL + row['download_file'] +' type="application/pdf" width="1100" height="1000"></object>'
                        html += '</div>'
                        html += '</div>';
                    });
                    $('.display-pdf-marketing').html(html);
               } else {
                    $('.display-pdf-marketing').html('<div class="row">'
                                    + '<div class="col-md-12 mb-40">'
                                    + '<h2 class="mb-40 text-judul text-center"><span class="font-weight-bold ">Marketing Plan Zeelora</span></h2>'
                                    + '<div class="col-sm-12 mb-100" style="margin-bottom: 190px;"><center><i>Tidak ada dokument. . .</i></center></div>');
               }
            
            } else {
                $('.display-pdf-marketing').html('<div class="row">'
                        + '<div class="col-md-12 mb-40">'
                        + '<h2 class="mb-40 text-judul text-center"><span class="font-weight-bold ">Marketing Plan Zeelora</span></h2>'
                        + '<div class="col-sm-12 mb-100" style="margin-bottom: 190px;"><center><i>Tidak ada dokument. . .</i></center></div>');
            }
        },error: function () {
             console.log("Terjadi kesalahan...");
        }
    });
}
</script>