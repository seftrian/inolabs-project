<?php
$w_lib=new widget_lib;
?>
<section>
	<div class="container">
<!-- Entry Title
  ============================================= --> 
		<div class="center wow fadeInDown pb-1">
			<h2 class="title-large color-dark">Agenda & Event</h2>
		</div>
	  
		<div class="skill-wrap clearfix wow fadeInDown ">
			<div class="row event_list">
				<center><i>Sedang memuat agenda & event. . .</i></center>
			</div>
		</div>
	   
   </div>
</section>
<script>

var _offset;
$(function(){
	_offset=0;
	get_event(0)
});

function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}

function get_event(offset)
{
	$('.event_list').html('<center><i>Sedang memuat javascript . . .</i></center>') 
	 _offset=offset;
	  $.ajax({
		url:'<?php echo base_url(); ?>cron/get_event_list',
		dataType:'json',
		type:'post',
		data:{'offset':offset},
		success: function(response){ 
		console.log(response);
		
			var count=response['count'];
			var limit=response['limit'];
			var page=response['page'];
			if(response['status']==200){
			  if(count>0)
			  {
					html='';index=0;
							$.each(response['data'],function(i,row){
							if(index%2==0)
							{
								html+='<div class="col-md-12">';
									html+='<div class="row">';
							}
										html+='<div class="col-lg-6 col-ms-12">';
											html+='<div class="media">';
												html+='<div class="img-event-left">';
													html+='<img src="'+row['logo']+'">';
												html+='</div>';
												html+='<div class="media-body">';
												html+='<h3 class="mt-0 mb-0 font-weight-normal">'+row['name']+'</h3>';
												html+='<small class="small-date"><i class="fa fa-calendar"></i> '+row['datetime']+'</small>'; 
												html+='<p>'+ decodeEntities(row['event_short_description']) + '</p>'; 
												html+='<a href="<?php echo base_url("event/detail");?>/'+row['id']+'/'+row['permalink']+'" class="font-weight-normal"><i>Selengkapnya <i class="fa fa-angle-right"></i></i></a>';
												html+='</div>';
											html+='</div>';
										html+='</div>';
							index++;
							if(index%2==0&&index>0)
							{
									html+='</div>';
								html+='</div>';
							}		
								
							}); 
							if(page>1)
							{
								current_page=(_offset/limit)+1;
								next=_offset+(_offset+1*limit);
								before=(current_page-2)*limit; 
								html+='<div class="col-md-12 text-right">';
									html+='<hr class="hr-dashed-white mb-0">';
									html+='<ul class="pagination pagination mb-0">';
									if(before>=0)
									{
										html+='<li><a href="#" onclick="get_event('+before+')"><i class="fa fa-angle-left"></i></a></li>';
									}
									// html+='<li><a href="#" onclick="get_event('+before+')"><i class="fa fa-angle-double-left"></i></a></li>';
									for(i=1;i<=page;i++)
									{
										current_offset=(i-1)*limit; 
										kelas=(i==current_page)?'active':'';
										onclicks=(i==current_page)?'':'onclick="get_event('+current_offset+')"';
										html+='<li class="'+kelas+'"><a href="#" '+onclicks+'>'+i+'</a></li>';
									} 
									if(next<=count)
									{
										html+='<li><a href="#" onclick="get_event('+next+')"><i class="fa fa-angle-right"></i></a></li>';
									}
										// html+='<li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>';
									html+='</ul>';
								html+='</div>'; 
							}  
						$('.event_list').html(html)
			  }
			  else{
				  $('.event_list').html('<center><i>Tidak ada data . . .</i></center>')
			  }
		  } else{
			$('.event_list').html('<center><i>Tidak ada data . . .</i></center>') 
		  }
		}, 
		error: function(){
		  alert('an error has occured, please try again.');
		}
	  });
}
</script>