<?php
$w_lib = new widget_lib;
?>
<section id="content">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <h3 class="mb-40 text-center">Kontak Kami</h3>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="frame-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.9374217487043!2d106.82992651435977!3d-6.139109461893493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5f709937b8b%3A0xcc24e8c546bb46a1!2sMangga%202%20Square!5e0!3m2!1sid!2sid!4v1594266165958!5m2!1sid!2sid"
                                width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
                <?php
                $w_lib->run('front', 'master_content', 'widget_globals', 'contact', 'kontak_kami');
                ?>
            </div>
        </div>
    </div>
</section>