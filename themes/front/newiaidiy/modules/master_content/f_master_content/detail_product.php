<?php
    $w_lib = new widget_lib;
?>
<section id="content">
    <div class="bg-breadcrumb ">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                <nav class="d-flex align-items-center justify-content-end">
                    <a href="product.html">Produk<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    <span class=""><?php echo $cur_title ?></span>
                </nav>
            </div>
        </div>
    </div>
    <section class="content-wrap">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-judul mb-40 font-weight-light">Produk <b class="font-weight-bold"><?php echo $name_produk ?></b></h3>
            </div>
           
                <?php  
                    $w_lib->run('front', 'master_content', 'widget_detail_slider_product', $produk_id);
                ?>
                </div>
            </div>
        </div>
    </section>
</section>
