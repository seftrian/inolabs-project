<section id="content">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-40">
                    <h2 class="mb-20 text-judul text-center"><span class="font-weight-bold "><?php echo $news_title ?></span></h2>
                </div>
                <div class="col-sm-12">
                    <object data='<?php echo base_url($download_arr['download_file']) ?>' type="application/pdf" width="100" height="100"></object>
                </div>
            </div>
        </div>
    </div>
</section>