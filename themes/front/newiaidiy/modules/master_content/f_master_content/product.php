<section id="content">
<div class="content-wrap">
        <div class="container">
                <div class="row">
                <div class="col-md-12 mb-40 fadeInDown">
                        <h2 class="mb-20 text-judul text-center"><span class="font-weight-light">Produk</span> <span class="font-weight-bold ">Zeelora</span></h2>
                        <div class="width-konten text-dark">
                            <p><span class="font-weight-semibold">4 Produk Unggulan</span> yang telah diproduksi dan dipasarkan antara lain :</p>
                        </div>
                    </div>
                    <div class="col-sm-12">
                            <div class="row display-list-produk"></div>
                    </div>        
                </div>        
            </div>
        </div>
<section>
<script src="<?php echo $themes_inc; ?>js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
listDataProduct()
.then(response => 
    console.log(response)
).catch(err => console.log(err));

function listDataProduct() { 
    const baseURL = '<?php echo base_url()?>'; 
    return new Promise((resolve, reject) =>{
        $.ajax({
            type: "post",
            url:  baseURL+ "json-list-produk",
            data: "data",
            dataType: "json",
            success: function (response) {
                if (response['sts'] == 200) {
                    const count = response['data'].length;
                    if (count > 0) {
                        html = '';
                        $.each(response['data'], function (i, row) {
                                html += '<div class="col-lg-3 col-sm-6">'
                                     html += '<a href="<?php echo base_url ("product/detail")?>/'+ row['news_id'] + '/' + row['news_permalink'] +'" class="box-link" title="'+ row['news_title'] +'">'
                                     html += '<div class="frame-img">'
                                         html += '<img src="'+ baseURL + row['news_photo'] +'" alt="" class="img-100">'
                                     html += '</div>'
                                     html += '<div class="frame-konten">'
                                        html += '<h4 class="text-center font-weight-semibold mb-0 ">'+ row['news_title'] + '</h4>'
                                     html += '</div>'
                                     html += '<a>'
                                html += '</div>'
                        });
                        $('.display-list-produk').html(html);
                    } else {
                        $('.display-list-produk').html('<i> Data Produk tidak ada ! </i>');
                    }
                    setTimeout(() => {
                        resolve(response['alert-success']);
                     }, 5000);
                } else {
                    $('.display-list-produk').html('<i> Data Produk tidak ada ! </i>');
                }
            },
            error:function() {
                reject('Data produk gagal di ambil');
            }
        });
    })
  
 }

</script>
