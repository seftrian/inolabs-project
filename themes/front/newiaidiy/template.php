<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$w_lib = new widget_lib;
$uri_1 = $this->uri->segment(1);
$uri_2 = $this->uri->segment(2);
$uri_3 = $this->uri->segment(3);
$src_logo = (isset($websiteConfig['config_logo']) and file_exists(FCPATH . $websiteConfig['config_logo']) and trim($websiteConfig['config_logo']) != '') ? base_url() . $websiteConfig['config_logo'] : $themes_inc . '<?php echo $themes_inc; ?>images/logofix1.png';
$favicon_logo = (isset($websiteConfig['config_favicon']) and file_exists(FCPATH . $websiteConfig['config_favicon']) and trim($websiteConfig['config_favicon']) != '') ? base_url() . $websiteConfig['config_favicon'] : $themes_inc . '<?php echo $themes_inc; ?>images/logofix1.png';
$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$base_url .= "://" . $_SERVER['HTTP_HOST'];
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
?>
<!DOCTYPE html>
<html dir="zxx" lang="no-js">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" href="<?php echo $favicon_logo; ?>">
        <title>
            <?php
            if (isset($seotitle)) {
                echo $seotitle;
            } elseif (isset($uri_3)) {
                echo $websiteConfig['config_name'];
            }
            ?>
        </title>
        <meta name="description" content="<?php echo function_lib::remove_html_tag((isset($seoDesc) and trim($seoDesc) != '') ? $seoDesc : $websiteConfig['config_description']); ?>">
        <meta name="keywords" content="<?php echo function_lib::remove_html_tag((isset($seoKeywords) and trim($seoKeywords) != '') ? $seoKeywords : $websiteConfig['config_keywords']); ?>">
        <!-- core CSS -->
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/swiper.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/font/maven-pro/maven-pro.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/font/great-vibes/great-vibes.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/font-awesome-4.7.0/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/open-sans/open-sans.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/linearicons.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/font-icons.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/nice-select.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/animate.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/nouislider.min.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/slicknav.min.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/main.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/style.css">
        <!-- css modal-img -->
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/modal-img/component.css" type="text/css" />
        <!-- / -->
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/responsive.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>css/custom.css">
        <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '503632880323849');
        fbq('track', 'PageView');
    </script>
        <style>
            .default-header{
                background: linear-gradient(45deg, #f3fbff 5%, #49b3e8 80%) !important;
            }
        </style>
    </head>
    <body class="stretched">
        <div class="se-pre-con"></div>
        <?php $w_lib->run('front', 'master_content', 'widget_user_menu'); ?>
        <?php if ($uri_1 == '') { ?>
            <div class="main-container">
                <?php
                $w_lib->run('front', 'image_slide', 'widget_slider_home');
                $w_lib->run('front', 'master_content', 'widget_profil');
                $w_lib->run('front', 'master_content', 'widget_index_product');
                $w_lib->run('front', 'master_content', 'widget_bisnis');
            } else {
                $this->load->view($view);
            }
            ?>
        </div>
        <div class="md-overlay"></div>
        <?php $w_lib->run('front', 'master_content', 'widget_footer'); ?>
        <script src="<?php echo $themes_inc; ?>js/jquery.js"></script>
        <script src="<?php echo $themes_inc; ?>js/plugins.js"></script>
        <script src="<?php echo $themes_inc; ?>js/functions.js"></script>
        <script src="<?php echo $themes_inc; ?>js/vendor/jquery-2.2.4.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/vendor/popper.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/vendor/pdfobject.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/jquery.nice-select.js"></script>
        <script src="<?php echo $themes_inc; ?>js/jquery.ajaxchimp.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/jquery.sticky.js"></script>
        <script src="<?php echo $themes_inc; ?>js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/jquery.slicknav.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/owl.carousel.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/nouislider.min.js"></script>
        <script src="<?php echo $themes_inc; ?>js/main.js"></script>
        <script type="text/javascript" src="<?php echo $themes_inc; ?>js/modal-img/classie.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                window.setTimeout(show_modal_first, 1000);
                function show_modal_first() {
                    $(".show_modal_first").addClass("md-show");
                }

            });
        </script>
        <!-- sub menu -->
        <script>
            $(function() {
                $(".submenu").hide();
                $(".parent").click(function(e) {
                    e.preventDefault();
                    $(".submenu", this).toggle();
                });
            });

            function redirect_link(link) {
                window.location.href = link;
            }
        </script>
        <!-- js menu mobile -->
        <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "290px";
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
        </script>
        <!-- header -->
        <script>
            $(window).scroll(function() {
                var header = $(document).scrollTop();
                var headerHeight = 0;
                var firstSection = $('.main-container div:nth-of-type(1)').outerHeight();

                if (header > headerHeight) {
                    $('.default-header').addClass('fixed');
                } else {
                    $('.default-header').removeClass('fixed');
                }
            });
        </script>
        <script>
            $(window).load(function() {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");
                ;
            });
        </script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89718172-7"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'UA-89718172-7');
        </script>
    </body>
</html>
