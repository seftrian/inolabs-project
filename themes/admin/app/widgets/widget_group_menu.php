<div class="row">
						<div class="col-sm-12">
							<!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-list"></i>
									<?php 
									if($parent_id!=0){
										echo '<a href="'.base_url().'admin/dashboard"><i class="clip-home"></i> Menu Utama</a> | ';
									}
									echo $menu_title?>
									<div class="panel-tools">
										<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
										</a>
										<!-- <a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a> -->
									
									</div>
								</div>
								<div class="panel-body">
									<div class="row">
									<?php
									if(!empty($menu_arr))
									{
										foreach($menu_arr AS $rowArr)
										{
											foreach($rowArr AS $variable=>$value)
											{
												${$variable}=$value;
											}
											$num_submenu=$menu_lib->hasSubmenu($menu_administrator_id);
											$badge_num=($num_submenu>0)?'<span class="badge badge-success"> '.$num_submenu.' </span>':'';
											$link=($num_submenu>0)?base_url().'admin/dashboard/index?parent_id='.$menu_administrator_id:base_url().$menu_administrator_link;
											?>
											<div class="col-sm-4">
												<button class="btn btn-icon btn-block" onclick="window.location.href='<?php echo $link?>';return false;">
													<i class="<?php echo $menu_administrator_class_css?>"></i>
													<?php echo $menu_administrator_title?> <?php echo $badge_num;?>
													<p style="font-size:10px;"><?php echo $menu_administrator_description?></p>
												</button>
												
											</div>

											<?php
										}
									}
									?>
										
									</div>
								</div>
							</div>
							<!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
						</div>
					</div>