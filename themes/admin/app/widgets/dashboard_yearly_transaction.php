<div class="row">
	<div class="col-sm-12">
		<!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				Grafik Transaksi Penjualan Resep dan Non Resep
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
					</a>
					<a class="btn btn-xs btn-link panel-expand" href="#">
						<i class="fa fa-resize-full"></i>
					</a>
				
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
				<div class="col-sm-12">
		  			<div id="chartContainer" style="height: 300px; width: 100%;"></div>
				</div>		
					</div>
				</div>
			</div>
			<!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
		</div>
</div>
<script type="text/javascript">
var jqxhr=$.ajax({
url:'<?php echo base_url()?>dashboard/service_rest/yearly_transaction',
dataType:'json',
type:'get'
});

jqxhr.success(function(response){
		var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text:response['graphic_title']   
      },
      animationEnabled: true,
      axisY: {
        title: response['y_title']  
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme2",
      data: [

      {        
        type: "column",  
        showInLegend: true, 
        legendMarkerColor: "grey",
        dataPoints: response['data_points']
      }   
      ]
    });

    chart.render();
});

</script>					