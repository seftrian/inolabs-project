<div class="row">
            <div class="col-sm-12">
              <!-- start: RESPONSIVE ICONS BUTTONS PANEL -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <i class="fa fa-external-link-square"></i>
                      Grafik Probabilitas 10 Item Teratas
                  <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                      <i class="fa fa-resize-full"></i>
                    </a>
                  
                  </div>
                </div>
                <div class="panel-body">
                  <div class="row">
            
            <div class="col-sm-4">
                <div id="chart_probabilitas_day" style="height: 300px; width: 100%;"></div>
            </div>    
            <div class="col-sm-4">
                <div id="chart_probabilitas_week" style="height: 300px; width: 100%;"></div>
            </div>    
            <div class="col-sm-4">
                <div id="chart_probabilitas_month" style="height: 300px; width: 100%;"></div>
            </div>    

                  </div>
                </div>
              </div>
              <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
            </div>
          </div>

<script type="text/javascript">
window.onload = function () {
  
  //Hari ini
  var jq_day=$.ajax({
    url:'<?php echo base_url()?>dashboard/service_rest/probabilitas?aktif=today',
    dataType:'json',
    type:'get'
  });
  
  jq_day.success(function(response){

      var chart_day = new CanvasJS.Chart("chart_probabilitas_day",
      {
        title:{
          text: response['graphic_title']
        },
        exportFileName: response['export_title'],
        exportEnabled: true,
        animationEnabled: true,
        legend:{
          verticalAlign: "bottom",
          horizontalAlign: "center"
        },
        data: [
        {       
          type: "pie",
          showInLegend: true,
          toolTipContent: "{legendText}",
          indexLabel: "{label}",
          dataPoints: response['data_points']
        }
      ]
      });
      chart_day.render();
  });

  //1 Minggu Terakhir
  var jq_week=$.ajax({
    url:'<?php echo base_url()?>dashboard/service_rest/probabilitas?aktif=week',
    dataType:'json',
    type:'get'
  });
  
  jq_week.success(function(response){

      var chart_week = new CanvasJS.Chart("chart_probabilitas_week",
      {
        title:{
          text: response['graphic_title']
        },
        exportFileName: response['export_title'],
        exportEnabled: true,
        animationEnabled: true,
        legend:{
          verticalAlign: "bottom",
          horizontalAlign: "center"
        },
        data: [
        {       
          type: "pie",
          showInLegend: true,
          toolTipContent: "{legendText}",
          indexLabel: "{label}",
          dataPoints: response['data_points']
        }
      ]
      });
      chart_week.render();
  });

  //3 Bulan Terakhir
  var jq_bln=$.ajax({
    url:'<?php echo base_url()?>dashboard/service_rest/probabilitas?aktif=month',
    dataType:'json',
    type:'get'
  });
  
  jq_bln.success(function(response){

      var chart_month = new CanvasJS.Chart("chart_probabilitas_month",
      {
        title:{
          text: response['graphic_title']
        },
        exportFileName: response['export_title'],
        exportEnabled: true,
        animationEnabled: true,
        legend:{
          verticalAlign: "bottom",
          horizontalAlign: "center"
        },
        data: [
        {       
          type: "pie",
          showInLegend: true,
          toolTipContent: "{legendText}",
          indexLabel: "{label}",
          dataPoints: response['data_points']
        }
      ]
      });
      chart_month.render();
  });
}
</script>