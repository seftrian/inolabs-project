<li class="dropdown" >
							<a class="dropdown-toggle" onclick="fetch_new_notification();return false;" data-close-others="true" data-toggle="dropdown" href="#">
								<i class="fa fa-bullhorn"></i>
					
								<span class="<?php echo ($count>0)?'badge':''?>" id="count_notification"> <?php echo ($count>0)?$count:''?></span>

							</a>
							<ul class="dropdown-menu posts">
							<li>
							<div class="drop-down-wrapper">
								<ul id="fetch_new_notification">
							<?php 
							if(!empty($results))
							{
								foreach($results AS $rowArr)
								{
									foreach($rowArr AS $variable=>$value)
									{
										${$variable}=$value;
									}
									?>
											<li>
												<a href="<?php echo base_url()?>admin/ask/index">
													<div class="clearfix">
														
														<div class="thread-content">
															<span class="author"><?php echo $notification_list_title?></span>
															<span class="preview"><?php echo $notification_list_text?></span>
															<span class="time"><?php echo function_lib::time_elapsed_string($notification_timestamp)?></span>
														</div>
													</div>
												</a>
											</li>
									<?php
								}
							}
							else 
							{
								?>
											<li>
												<a href="<?php echo base_url()?>admin/ask/index">
													<div class="clearfix">
														
														<div class="thread-content">
															<span class="preview">Belum ada pemberitahuan terbaru saat ini.</span>
														</div>
													</div>
												</a>
											</li>
								<?php
							}
							?>

									</ul>
								</div>
							</li>
							
							
								
								<li class="view-all" >
									<a href="<?php echo base_url()?>admin/ask/index" >
										Lihat semua pemberitahuan <i class="fa fa-arrow-circle-o-right"></i>
									</a>
								</li>
							</ul>
						</li>

<script type="text/javascript">
function fetch_new_notification()
{
	$("#fetch_new_notification").html('loading...');

	var jqxhr=$.ajax({
		url:'<?php echo base_url()?>notification/service_rest/fetch_new_notification',
		type:'get',
		dataType:'json',

	});
	jqxhr.success(function(response){
		if(response['count_notification']>0)
		{
				$("#count_notification").addClass('badge');			
				$("#count_notification").html(response['count_notification']);
			
		}
		else
		{
			  $("#count_notification").removeClass('badge');			
				$("#count_notification").html('');
		}
		$("#fetch_new_notification").html('');
		var data=response['data'];
		$.each(data,function(index,rowArr){
			var html='<li>'
									+'			<a href="#">'
									+'				<div class="clearfix">'
									+'					<div class="thread-content">'
									+'						<span class="author">'+rowArr['notification_list_title']+'</span>'
									+'						<span class="preview">'+rowArr['notification_list_text']+'</span>'
									+'						<span class="time">'+rowArr['notification_timestamp']+'</span>'
									+'					</div>'
									+'				</div>'
									+'			</a>'
									+'		</li>';
			$("#fetch_new_notification").append(html);						
		});
	});
}
</script>						