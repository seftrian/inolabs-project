<div class="row">
      <div class="col-sm-12">
        <p class="text-danger"><a href="javascript:void(0);" onclick="get_change_log();return false;" class="btn btn-success btn-sm btn-squared"><i class="clip-file-2"></i> Lihat daftar perubahan aplikasi</a></p>
        <div style="overflow:scroll;height:200px;display:none;" id="content-change-log"></div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
      </div>
</div>

<script type="text/javascript">
  //Hari ini
  var content_change_log=$("#content-change-log");
  function get_change_log()
  {
      if(content_change_log.find('#get-content-change-log').length<1)
      {
          content_change_log.show();
          content_change_log.html('loading...');
          var jqxhr=$.ajax({
            url:'<?php echo base_url()?>dashboard/service_rest/get_change_log',
            dataType:'html',
            type:'get'
          });

          jqxhr.success(function(response){
            content_change_log.html('<p id="get-content-change-log">'+response+'</p>');
          });
      }
      else
      {
        content_change_log.toggle();
      }
     
  }
  

</script>