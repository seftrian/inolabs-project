<?php
$total_notif=$num_pending_stock+$count_top_almost_or_expired+$dont_set_spesialis+$count_defecta;
?>
<div class="row"  style="<?php echo ($total_notif>0)?'':'display:none;'?>">
      <div class="col-sm-12">
      	<b>Informasi Terbaru!</b>
        <div style="overflow:scroll;height:200px;" id="content-change-log">
        	<ul>
               <?php if ($dont_set_spesialis)
            { ?>
                <li class="alert-danger">
                    Ada <?php echo $dont_set_spesialis ?>  NaKes jaga yang belum diset spesialisnya. 
                &nbsp;&nbsp;&nbsp;
                [<a href="<?php echo base_url() ?>admin/people/index/not_spesialist">Selengkapnya</a>]
                </li>
            <?php
            }
            ?>
        	<?php
        	if($num_pending_stock>0)
        	{
        		?>
        		<li class="alert-danger">Ada <?php echo $num_pending_stock?> barang yang belum terhitung ke stok. [<a href="<?php echo base_url().'admin/stok_all/pending_stock'?>">Selengkapnya</a>]</li>
        		<?php
        	}
        	?>
            <?php
            if($count_top_almost_or_expired>=0)
            {
                ?>
                <li style="margin-top:10px;"><span class="<?php echo ($count_top_almost_or_expired>0)?'label label-warning':''?>"><?php echo $count_top_almost_or_expired?> transaksi akan/telah jatuh tempo. [<a class="text-danger" href="<?php echo base_url().'admin/billing_transaction/index'?>">Cek</a>]</span></li>
                <?php
            }
            ?>
             <?php
            if($count_almost_or_expired_to_ro>=0)
            {
                ?>
                <li style="margin-top:10px;"><span class="<?php echo ($count_almost_or_expired_to_ro>0)?'label label-warning':''?>"><?php echo $count_almost_or_expired_to_ro?> transaksi yang akan/telah dilakukan pembelian kembali (<i>repeat order</i>). [<a class="text-danger" href="<?php echo base_url().'admin/reminder_customer/index'?>">Cek</a>]</span></li>
                <?php
            }
            ?>
               <?php
            if($count_defecta>=0)
            {
                ?>
                <li style="margin-top:10px;"><span class="<?php echo ($count_defecta>0)?'label label-warning':''?>"><?php echo number_format($count_defecta,0,'','.')?> data defecta (barang di bawah minimal stok). [<a class="text-danger" href="<?php echo base_url().'admin/defecta/index'?>">Cek</a>]</span></li>
                <?php
            }
            ?>
         
        	</ul>
        </div>
        <!-- end: RESPONSIVE ICONS BUTTONS PANEL -->
      </div>
</div>