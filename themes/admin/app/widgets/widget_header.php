<ul class="nav ace-nav pull-right">
    <li class="light-blue">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <!--<img class="nav-user-photo" src="<?php //echo $themes_inc; ?>assets/avatars/user.jpg" alt="Jason's Photo" />-->
            <span class="user-info"><small>Welcome,</small><?php echo ucfirst($session['detail']['admin_username']); ?></span>
            <i class="icon-caret-down"></i>
        </a>
        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
            <li><a href="<?php echo base_url(); ?>admin/admin_config/view"><i class="icon-cog"></i>Settings</a></li>
            <li><a href="<?php echo base_url(); ?>admin/admin_user/update/<?php echo $session['detail']['admin_id']; ?>"><i class="icon-user"></i>Profile</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url() . 'auth/logout?redirect=' . rawurlencode(base_url() . 'auth/connect_to_admin'); ?>"><i class="icon-off"></i>Logout</a></li>
        </ul>
    </li>
</ul>
<!--/.ace-nav-->