<div class="col-sm-12">

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
       <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Posisi <span class="symbol required"/> 
            </label>
            <div class="row col-sm-5">
                <select name="struktur_par_id" id="form-field-select-3" class="form-control search-select">
                    <option value="">Pilih Posisi</option>
                    <option value="0" <?php echo (isset($_POST['struktur_par_id']) AND $_POST['struktur_par_id']==0)?'selected':''?>>Teratas</option>
                    <?php
                    echo $recursiveDataMenu;
                    ?>
                </select>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Jabatan <span class="symbol required"/>
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="struktur_nama_jabatan" value="<?php echo $this->input->post('struktur_nama_jabatan'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Nama Lengkap
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="struktur_nama_kepala" value="<?php echo $this->input->post('struktur_nama_kepala'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Urutan Ditampilkan
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="struktur_urutan" value="<?php echo $this->input->post('struktur_urutan'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Profil Singkat
            </label>
            <div class="col-sm-5">
                <textarea rows="7" class="form-control" name="struktur_profil_singkat"><?php echo $this->input->post('struktur_profil_singkat'); ?></textarea>
            </div>
        </div>
        <div class="form-group" style="display:none">
            <label class="col-sm-2 control-label" for="form-field-1">
                Pangkat 
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="struktur_pangkat" value="<?php echo $this->input->post('struktur_pangkat'); ?>" />
            </div>
        </div>
        <div class="form-group" style="display:none">
            <label class="col-sm-2 control-label" for="form-field-1">
                NIP 
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="struktur_nip" value="<?php echo $this->input->post('struktur_nip'); ?>" />
            </div>
        </div>

        
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Gambar
            </label>
            <div class="col-sm-5">
                  <input class="form-control" type="file" name="struktur_foto"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Facebook
            </label>
            <div class="col-sm-5">
                https://www.facebook.com/ <input type="text" name="struktur_facebook" placeholder="inolabs.official" value="<?php echo $this->input->post('struktur_facebook'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Twitter
            </label>
            <div class="col-sm-5">
                https://twitter.com/ <input type="text" name="struktur_twitter" placeholder="inolabs" value="<?php echo $this->input->post('struktur_twitter'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Google Plus
            </label>
            <div class="col-sm-5">
                https://plus.google.com/ <input type="text" name="struktur_gplus" placeholder="inolabs" value="<?php echo $this->input->post('struktur_gplus'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                LinkedIn
            </label>
            <div class="col-sm-5">
                https://www.linkedin.com/ <input type="text" name="struktur_linkedin" placeholder="inolabs" value="<?php echo $this->input->post('struktur_linkedin'); ?>" />
            </div>
        </div>
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
            <div class="col-sm-1">
            <a href="<?php echo base_url()?>admin/struktur/index" style="float:left;">Batal</a>
        </div>
        <div class="col-sm-2">
            <input onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">

        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
