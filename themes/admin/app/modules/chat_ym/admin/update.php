<div class="col-sm-12">

   <div class="panel panel-default">
        <div class="panel-heading">
           Form yang bertanda <span class="symbol required"/> wajib diisi.
        </div>
    <div class="panel-body">
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Nama Lengkap <span class="symbol required"/>
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="chat_ym_full_name" value="<?php echo $this->input->post('chat_ym_full_name')?$this->input->post('chat_ym_full_name'):$chat_ym_full_name; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Email Yahoo! <span class="symbol required"/>
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="chat_ym_email" value="<?php echo $this->input->post('chat_ym_email')?$this->input->post('chat_ym_email'):$chat_ym_email; ?>" />
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                No. Hp.
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="chat_ym_mobilephone" value="<?php echo $this->input->post('chat_ym_mobilephone')?$this->input->post('chat_ym_mobilephone'):$chat_ym_mobilephone; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Keterangan
            </label>
            <div class="col-sm-5">
                <input type="text" placeholder="Misal: Marketing, Teknis, ..." class="form-control" name="chat_ym_note" value="<?php echo $this->input->post('chat_ym_note')?$this->input->post('chat_ym_note'):$chat_ym_note; ?>" />
            </div>
        </div>
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
            <input type="reset" value="Reset" class="btn btn-warning" style="float:left;">
        </div>
        <div class="col-sm-6">
            <input style="float: right;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
