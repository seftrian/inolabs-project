<?php
$logo_name= (trim($testimonial_avatar_src)=='')?'-': pathinfo($testimonial_avatar_src,PATHINFO_FILENAME);
$logo_ext=  pathinfo($testimonial_avatar_src,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>350,
'height'=>233,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'350233/',
'urlSave'=>$pathImgUrl.'350233/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
?>
<div class="col-sm-12">
    

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Nama <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="testimonial_full_name" value="<?php echo $this->input->post('testimonial_full_name')?$this->input->post('testimonial_full_name'):$testimonial_full_name; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Judul <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="testimonial_title" value="<?php echo $this->input->post('testimonial_title')?$this->input->post('testimonial_title'):$testimonial_title; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Isi <span class="symbol required"/>

            </label>
            <div class="col-sm-10">
          <?php echo form_textarea_tinymce('testimonial_value', $this->input->post('testimonial_value')?$this->input->post('testimonial_value'):$testimonial_value, 'Standard'); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Status
            </label>
            <div class="col-sm-10">
                <label><input type="radio" name="testimonial_is_published" value="Y" <?php echo ($this->input->post('testimonial_is_published') && $this->input->post('testimonial_is_published')=="Y")?'checked':$testimonial_is_published=="Y"?'checked':'';  ?> /> Y </label> &nbsp;
                <label><input type="radio" name="testimonial_is_published" value="N" <?php echo ($this->input->post('testimonial_is_published') && $this->input->post('testimonial_is_published')=="N")?'checked':$testimonial_is_published=="N"?'checked':'';  ?> /> N </label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Avatar
            </label>
            <div class="col-sm-10">
                  <p class="text-danger">
                      <?php
                      if($logo_name=='-')
                      {
                        echo 'Belum ada gambar.';
                      }
                      else
                      {
                    ?>
                        <img src="<?php echo $img?>" />
                      <br />
                      <a href="<?php echo base_url()?>admin/testimonial/delete_image/<?php echo $testimonial_id?>" onclick="return confirm('Hapus?');return false;">Hapus Gambar</a>
                  
                    <?php      
                      }
                      ?>  
                  </p>  
                  <input type="hidden" name="testimonial_avatar_src" value="<?php echo $testimonial_avatar_src?>"/>
                  <input class="form-control" type="file" name="testimonial_avatar_src"/>
            </div>
        </div>
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
            <input type="reset" value="Reset" class="btn btn-warning" style="float:left;">
        </div>
        <div class="col-sm-6">
            <input style="float: right;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
