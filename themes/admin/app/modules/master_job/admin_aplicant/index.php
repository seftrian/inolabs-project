<div class="row">
            <div class="col-md-12">
            <?php
            //untuk menampilkan pesan
            if(trim($message)!='')
            {
                
                echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
            }
            ?>
			<!-- start: RESPONSIVE TABLE PANEL -->
	        <!-- start: INBOX PANEL -->
            <input type="hidden" id="job_id" value="<?php echo $job_id?>">
            <input type="hidden" id="limit" value="10">
            <input type="hidden" id="offset" value="0">
            <div class="alert" style="display:none;"></div>                                                                       
<div class="tabbable">
    <ul id="myTab4" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
        <li class="active">
            <a href="#panel_tab3_example1" data-toggle="tab">
               Informasi Lowongan
            </a>
        </li>
        <li class="">
            <a href="#panel_tab3_example2" data-toggle="tab">
               Pelamar
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab3_example1">
            <?php
            if(!empty($job_arr))
            {
                foreach($job_arr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }
                echo '<center><h4>'.$job_title.'</h4></center>';
                echo html_entity_decode($job_content);
            }
            ?>
        </div>
        <div class="tab-pane" id="panel_tab3_example2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-envelope-o"></i>
                    Data Pelamar <span id="info-loading" style="display:none;"></span>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                        <a class="btn btn-xs btn-link" onclick="refresh_inbox();return false;" href="#">
                            <i class="fa fa-refresh"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body messages">
                    <input style="width:99%;margin:5px;" type="text" onkeyup="get_list_all_sender(0);return false;" id="query_message" class="form-control" placeholder="Search messages...">
                    <ul class="messages-list panel-scroll ps-container" style="height: 500px;">
                    </ul>
                    <div class="messages-content  panel-scroll ps-container" style="height: 500px;">
                    </div>
                </div>
            </div>     
            <!-- end: RESPONSIVE TABLE PANEL -->
        </div>
   
    </div>
</div>
                           
                </div>
</div>

<div id="compose_message" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-body">
    <div class="alert" style="display:none;"></div>                                                                       
    
    <form id="form-compose-message">
    <input type="hidden" name="save" value="1">
    <input type="hidden" name="ask_id" value="0">
    <table class="table table-border">
    <tr>
    <th style="width:20%;">Pengirim</th><td><input name="from" id="from" class="form-control" type="text" placeholder="pengirim" readonly=""></td>
    </tr>
    <tr>
    <th>Subjek</th><td><input name="subject" id="subject" class="form-control" type="text" placeholder="subjek"></td>
    </tr>
    <tr>
    <th>Tujuan</th><td><input name="to" id="to" class="form-control" type="text" placeholder="tujuan"></td>
    </tr>
    <tr>
    <td colspan="2">
        <textarea name="message" id="message" class="form-control" style="height:100px;"></textarea>
    </td>
    </tr>
    </table>
    </form>
    </div>
    <div class="modal-footer">
        <button style="float:left;" type="button" data-dismiss="modal" class="btn btn-danger btn-default">
            Close
        </button>
        <button onclick="send_message();return false;" type="button" class="btn btn-primary">
            Kirim
        </button>
    </div>
</div>