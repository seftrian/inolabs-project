<?php
$logo_name= (trim($job_photo)=='')?'-': pathinfo($job_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($job_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>70,
'height'=>70,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'7070/',
'urlSave'=>$pathImgUrl.'7070/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
?>
<div class="col-sm-12">
    

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
            <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Waktu Tayang

            </label>
            <div class="col-sm-2">
                <select name="job_show_time" class="form-control" onchange="show_time($(this));return false;">
                <option value="N" <?php echo ($this->input->post('job_show_time')=='N')?'selected':(
                    ($job_show_time=='N')?'selected':''
                )?>>Tanpa Batasan</option>
                <option value="Y" <?php echo ($this->input->post('job_show_time')=='Y')?'selected':(
                    ($job_show_time=='Y')?'selected':''
                )?>>Set Waktu</option>
                </select>
            </div>
             <div class="col-sm-2 div_show_time" style="<?php echo ($this->input->post('job_show_time')=='Y')?'':(
                    ($job_show_time=='Y')?'':'display:none;'
                )?>">
               <input data-date-format="yyyy-mm-dd" data-date-viewmode="years"  name="job_start_date" value="<?php echo $this->input->post('job_start_date')?$this->input->post('job_start_date'):$job_start_date?>" type="text" class="form-control date-picker" placeholder="Tgl. Mulai">
             </div>
             <div class="col-sm-2 div_show_time" style="<?php echo ($this->input->post('job_show_time')=='Y')?'':(
                    ($job_show_time=='Y')?'':'display:none;'
                )?>">
               <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" type="text" name="job_end_date" value="<?php echo $this->input->post('job_end_date')?$this->input->post('job_end_date'):$job_end_date?>" class="form-control date-picker" placeholder="Berakhir">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Judul <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="job_title" value="<?php echo $this->input->post('job_title')?$this->input->post('job_title'):$job_title; ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Isi <span class="symbol required"/>

            </label>
            <div class="col-sm-10">
          <?php echo form_textarea_tinymce('job_content', ($this->input->post('job_content')?$this->input->post('job_content'):html_entity_decode($job_content)), 'PageGenerator'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               Meta Deskripsi
            </label>
            <div class="col-sm-10">
                 <textarea class="form-control"  placeholder="deskripsi singkat konten..." name="job_meta_description"><?php echo $this->input->post('job_meta_description')?$this->input->post('job_meta_description'):$job_meta_description; ?></textarea>
            <p class="text-muted"> maks. 200 karakter dan memuat deskripsi singkat.</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               Meta Tags
            </label>
            <div class="col-sm-10">
                 <textarea class="form-control" name="job_meta_tags" placeholder="kata, kunci..."><?php echo $this->input->post('job_meta_tags')?$this->input->post('job_meta_tags'):$job_meta_tags; ?></textarea>
            <p class="text-muted"> maks. 200 karakter dan memuat kata kunci yang pisahkan dengan koma (,).</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Gambar
            </label>
            <div class="col-sm-6">
                  <p class="text-danger">
                      <?php
                      if($logo_name=='-')
                      {
                        echo 'Belum ada gambar.';
                      }
                      else
                      {
                    ?>
                        <img src="<?php echo $img?>" />
                      <br />
                      <a href="<?php echo base_url()?>admin/master_content/delete_image/<?php echo $job_id?>" onclick="return confirm('Hapus?');return false;">Hapus Gambar</a>
                  
                    <?php      
                      }
                      ?>  
                  </p>  
                  <input type="hidden" name="job_photo_old" value="<?php echo $job_photo?>"/>
                  <input class="form-control" type="file" name="job_photo"/>
            </div>
        </div>
        <input type="hidden" id="num_files" value="<?php echo (count($list_files)>0)?count($list_files):1?>">
        <?php
        if(!empty($list_files))
        {
            $redirect=base_url().'admin/master_content/update/'.$job_id;
            $no=0;
            foreach($list_files AS $index=>$rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }
                $download_title_value=isset($_POST['download_title'][$index])?$_POST['download_title'][$index]:$download_title;
                $download_icon='';
                $remove='<a title="Hapus file" onclick="return confirm(\'Hapus?\');" href="'.base_url().'admin/master_file/delete_data/'.$download_id.'?redirect='.rawurlencode($redirect).'" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>';

                if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
                {
                    $download_icon='<a title="Download file" href="'.base_url().'admin/master_file/download_file/'.$download_id.'" class="btn btn-xs btn-green"><i class="fa fa-cloud-download"></i></a>';
                }
                ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        <?php echo ($index==0)?'File':''?>
                    </label>
                    <div class="col-sm-4">
                           <?php echo $download_icon?>
                           <?php echo $remove?>
                          <input type="hidden" value="<?php echo $download_id?>" name="download_id[<?php echo $index?>]">
                          <input style="width:80%;float:right;" class="form-control" type="file" name="download_file_<?php echo $index?>"/>
                    </div>
                    <div class="col-sm-4">
                          <input class="form-control" value="<?php echo $download_title_value?>" placeholder="Label file" type="text" name="download_title[<?php echo $index?>]"/>
                    </div>

                    
                </div>
                <?php
            }
        }
        else
        {
            ?>
            <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                       File
                    </label>
                    <div class="col-sm-4">
                          <input type="hidden" value="0" name="download_file_id[0]">
                          <input class="form-control" type="file" name="download_file_0"/>
                    </div>
                    <div class="col-sm-4">
                          <input class="form-control" placeholder="Label file" type="text" name="download_title[0]"/>
                    </div>
            </div>
            <?php  
        }
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               
            </label>
            <div class="col-sm-4">
                <a href="#" onclick="add_input_file($(this));return false;" title="Tambah file" class="btn btn-xs btn-info"> [+] File</a>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2">
            </div>
            <div class="col-sm-2">
                <a href="<?php echo base_url()?>admin/master_content/index" onclick="return confirm('Batalkan?');">Batal</a>
                <input style="float: right;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
            </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
