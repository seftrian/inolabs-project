<div class="col-sm-12">

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Waktu Tayang

            </label>
            <div class="col-sm-2">
                <select name="job_show_time" class="form-control" onchange="show_time($(this));return false;">
                <option value="N" <?php echo ($this->input->post('job_show_time')=='N')?'selected':''?>>Tanpa Batasan</option>
                <option value="Y" <?php echo ($this->input->post('job_show_time')=='Y')?'selected':''?>>Set Waktu</option>
                </select>
            </div>
             <div class="col-sm-2 div_show_time" style="<?php echo ($this->input->post('job_show_time')=='Y')?'':'display:none;'?>">
               <input data-date-format="yyyy-mm-dd" data-date-viewmode="years"  name="job_start_date" value="<?php echo $this->input->post('job_start_date')?$this->input->post('job_start_date'):''?>" type="text" class="form-control date-picker" placeholder="Tgl. Mulai">
             </div>
             <div class="col-sm-2 div_show_time" style="<?php echo ($this->input->post('job_show_time')=='Y')?'':'display:none;'?>">
               <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" type="text" name="job_end_date" value="<?php echo $this->input->post('job_end_date')?$this->input->post('job_end_date'):''?>" class="form-control date-picker" placeholder="Berakhir">
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Judul <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="job_title" value="<?php echo $this->input->post('job_title'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Isi <span class="symbol required"/>

            </label>
            <div class="col-sm-10">
          <?php echo form_textarea_tinymce('job_content', $this->input->post('job_content'), 'PageGenerator'); ?>
            </div>
        </div>
        

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               Meta Deskripsi
            </label>
            <div class="col-sm-10">
                 <textarea class="form-control"  placeholder="deskripsi singkat konten..." name="job_meta_description"><?php echo $this->input->post('job_meta_description'); ?></textarea>
            <p class="text-muted"> maks. 200 karakter dan memuat deskripsi singkat.</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               Meta Tags
            </label>
            <div class="col-sm-10">
                 <textarea class="form-control" name="job_meta_tags" placeholder="kata, kunci..."><?php echo $this->input->post('job_meta_tags'); ?></textarea>
            <p class="text-muted"> maks. 200 karakter dan memuat kata kunci yang pisahkan dengan koma (,).</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Gambar
            </label>
            <div class="col-sm-10">
                  <input class="form-control" type="file" name="job_photo"/>
            </div>
        </div>
        <input type="hidden" id="num_files" value="1">
        <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                   File
                </label>
                <div class="col-sm-4">
                      <input type="hidden" value="0" name="download_file_id[0]">
                      <input class="form-control" type="file" name="download_file_0"/>
                </div>
                <div class="col-sm-4">
                      <input class="form-control" placeholder="Label file" type="text" name="download_title[0]"/>
                </div>
        </div>
       
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               
            </label>
            <div class="col-sm-4">
                <a href="#" onclick="add_input_file($(this));return false;" title="Tambah file" class="btn btn-xs btn-info"> [+] File</a>
            </div>
        </div>
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-2">
               <a href="<?php echo base_url()?>admin/master_content/index" onclick="return confirm('Batalkan?');">Batal</a>
            <input style="float: right;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
