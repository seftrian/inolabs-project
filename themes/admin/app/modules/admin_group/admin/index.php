<div class="row">
    <div class="col-md-12">
                        <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
		<!-- start: RESPONSIVE TABLE PANEL -->
		<div class="panel panel-default">
			<div class="panel-heading panel-btn">
				<button type="button" class="btn btn-sm btn-info" onclick="window.location.href='<?php echo base_url();?>admin/admin_group/create';return false;">
                        Tambah Group
                    </button>
                <input  type="submit" class="btn btn-sm btn-success" value="Aktifkan" name="publish_selected_item" disabled/>
                <input  type="submit" class="btn btn-sm btn-light" value="Non Aktifkan" name="unpublish_selected_item" disabled/>
                <input  type="submit" class="btn btn-sm btn-danger" value="Hapus" name="delete_selected_item" onclick="return confirm('Hapus?');" disabled/>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
                                                        
                  <form id="form-horizontal" method="POST" action="">
                                                            
					<table class="table table-bordered table-hover mb-0" id="sample-table-1">
						<thead>
							<tr>
<!--                          <th class="checkbox-column"><input type="checkbox" id="checkAll"></th>-->
                                <th class="center" width="5%">
                                        <label>
                                                <input type="checkbox" id="checkAll" >
                                        </label>
                                </th>
                                <th width="5%" class="text-center">No.</th>
                                <th class="text-center">Nama Group</th>
                                <th class="text-center">Status</th>
                                <th width="15%" class="text-center">#</th>

                            </tr>
						</thead>
						<tbody>
                                                                        <?php
                            
                            if(!empty($dataGroup))
                            {
                                $no=1;
                                foreach($dataGroup AS $val)
                                {
                                    $groupId=$val['admin_group_id'];
                                    $groupTitle=$val['admin_group_title'];
                                    $groupType=$val['admin_group_type'];
                                    
                                    ?>
                                    <tr>
                                        <td class="text-center">
                                            <!-- <div class="checkbox-table"> -->
                                                <label>
                                                       <input type="checkbox" name="admin_group_id[]" value="<?php echo $groupId;?>">
                                                </label>
                                            <!-- </div> -->
                                        </td>
                                         
                                        <td><?php echo $no;?></td>
                                        <td><?php echo $groupTitle;?> <span style="font-size:10px;font-weight: bold;"><?php echo $groupType;?></span></td>
                                        <td width="10%"  style="text-align:center;"><span class="<?php echo ($val['admin_group_is_active']==1)?'icon-ok':'icon-off';?>"></span></span></td>
                                       <td class="da-icon-column text-center">
                                            <a href="<?php echo base_url();?>admin/admin_group/view/<?php echo $groupId;?>" title="Detil" class="btn btn-xs btn-default"><i class="clip-zoom-in"></i></a>
                                            <a href="<?php echo base_url();?>admin/admin_group/update/<?php echo $groupId;?>" title="Edit" class="btn btn-xs btn-default"><i class="clip-pencil-3"></i></a>
                                            <a href="<?php echo base_url();?>admin/admin_group/set_privilege/<?php echo $groupId;?>" title="Set Privilege" class="btn btn-xs btn-default"><i class="clip-key"></i></a>
                                            <a href="<?php echo base_url();?>admin/admin_group/delete/<?php echo $groupId;?>" onclick="return confirm('Hapus?');" title="Hapus" class="btn btn-xs btn-default"><i class="clip-remove"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                            }
                            ?>
							
                                    
						</tbody>
					</table>
                                                            </form>
				</div>
			</div>
			</div>
			<!-- end: RESPONSIVE TABLE PANEL -->
        </div>
</div>






<script type="text/javascript">
$('table th input:checkbox').on('click' , function(){
var that = this;
valueIsChecked=false;
$(this).closest('table').find('tr > td:first-child input:checkbox')
.each(function(){
this.checked = that.checked;
$(this).closest('tr').toggleClass('selected');

});


});
//untuk disable or enable button submit
$("input:checkbox").click(function(event){
valueIsChecked=false;
$("input:checkbox").each(function(){
if ($(this).prop('checked')==true){ 
valueIsChecked=true;
}

});

if(valueIsChecked)
{
$("input[type='submit']").attr('disabled',false);
}
else
{
$("input[type='submit']").attr('disabled',true);
}

if(!$('#checkAll').prop('checked') && this.id=='checkAll')
{
$("input[type='submit']").attr('disabled',true);
}


});
</script>