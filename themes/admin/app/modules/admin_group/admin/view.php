<div class="row">
        <div class="col-md-12">
                            <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
    </div>
    <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default">
                
                <div class="panel-body">
                    <div class="table-responsive">
                       <table class="table table-striped table-bordered table-hover mb-0">
                            <tbody>
                                <tr>
                                    <th>Nama Group</th>
                                    <td><?php echo $rowGroupAdmin['admin_group_title'];?></td>
                                </tr>
                                <tr>
                                    <th>Jenis</th>
                                    <td><?php echo $rowGroupAdmin['admin_group_type'];?></td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td><span class="<?php echo ($rowGroupAdmin['admin_group_is_active']==1)?'icon-ok':'icon-off';?>"></span></td>
                                </tr>
                                <!-- <tr>
                                    <th>Action</th>
                                    <td>
                                        <a href="<?php echo base_url();?>admin/admin_group/update/<?php echo $rowGroupAdmin['admin_group_id']?>" title="Edit" class="btn btn-xs btn-default"><i class="clip-pencil-3"></i></a>
                                        <a href="<?php echo base_url();?>admin/admin_group/set_privilege/<?php echo $rowGroupAdmin['admin_group_id']?>" title="Set Privilege" class="btn btn-xs btn-default"><i class="clip-key"></i></a>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                    
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="<?php echo base_url();?>admin/admin_group/update/<?php echo $rowGroupAdmin['admin_group_id']?>"  class="btn btn-sm btn-info">Ubah</a>
                    <a href="<?php echo base_url();?>admin/admin_group/set_privilege/<?php echo $rowGroupAdmin['admin_group_id']?>"  class="btn btn-sm btn-info">Set Privilege</a>
                </div>
    </div>
    </div>
</div>
