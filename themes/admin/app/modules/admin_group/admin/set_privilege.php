
<style type="text/css">
    ul#treeList{
        color:#000
    }
</style>
<div class="row">
    <div class="col-md-12">
                        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
    </div>
    <div class="col-sm-8 col-sm-offset-2">
    <!-- start: RESPONSIVE TABLE PANEL -->
    <div class="panel panel-default">
        <form role="form" action="" method="post" class="form-horizontal">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                        Nama Grup
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="admin_group_title" value="<?php echo $rowGroupAdmin['admin_group_title']; ?>" class="form-control" disabled/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                        Privilege
                    </label>
                    <div class="col-sm-9">
                        <label class="radio-inline">
                            <?php echo $rowGroupAdmin['admin_group_type']=='superuser'?'Untuk superuser anda berhak mengakses semua menu':'';?>
                        </label>
                    </div>
                </div>

                 
                  <?php
                    if($rowGroupAdmin['admin_group_type']!='superuser')
                    {
                       ?>
                        
                        <div class="control-group">
                            <hr class="hr-dashed">
                            <div id="sidetreecontrol"><a href="?#" class="btn btn-sm btn-default">Expand All</a> <a href="?#" class="btn btn-sm btn-default">Collapse All</a></div>
                            <hr class="hr-dashed">
                            <?php echo $buildTree;?>
                        </div>
                        
                        <?php
                    }
                    ?>
                
            </div>
            <div class="panel-footer">
                <input type="submit" name="save" value="Simpan" class="btn btn-sm btn-success" />
            </div>
        </form>
        </div>
    </div>
</div>



<script type="text/javascript">
//default buka semua tree menu
$(document).ready(function(){
   $("ul#treeList").find('ul').css('display','block'); 
   $("ul#treeList").find('li').css('class','collapsable'); 
});
</script>