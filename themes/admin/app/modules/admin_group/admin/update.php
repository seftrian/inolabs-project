<div class="row">
        <div class="col-md-12">
                            <?php
            //untuk menampilkan pesan
            if(trim($message)!='')
            {
                
                echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
            }
            ?>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
                <!-- start: RESPONSIVE TABLE PANEL -->
                <div class="panel panel-default">
                    <form role="form" action="" method="post" class="form-horizontal">
                    <div class="panel-body">

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="form-field-1">
                                    Nama Grup
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Nama Grup" name="admin_group_title" value="<?php echo $rowGroupAdmin['admin_group_title']; ?>" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="form-field-1">
                                    Jenis
                                </label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="red" name="admin_group_type" value="superuser" id="superuser"  <?php echo (isset($_POST['admin_group_type']) AND $_POST['admin_group_type']=='superuser')?'checked':(
                                                    ($rowGroupAdmin['admin_group_type']=='superuser')?'checked':''
                                                    );?>>
                                        Superuser
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="red" name="admin_group_type" value="administrator" id="administrator" <?php echo (isset($_POST['admin_group_type']) AND $_POST['admin_group_type']=='administrator')?'checked':(
                                                     ($rowGroupAdmin['admin_group_type']=='administrator')?'checked':''
                                                    )
                                                    ?>>
                                        Administrator
                                    </label>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="col-sm-3 control-label" for="form-field-1">
                                    Status
                                </label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="red" name="admin_group_is_active" value="1" id="1"  <?php echo (isset($_POST['admin_group_is_active']) AND $_POST['admin_group_is_active']==1)?'checked':(
                                                    ($rowGroupAdmin['admin_group_is_active']==1)?'checked':''
                                                    )?>>
                                        Aktif
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="red" name="admin_group_is_active" value="0" id="0"  <?php echo (isset($_POST['admin_group_is_active']) AND $_POST['admin_group_is_active']==0)?'checked':(
                                                    ($rowGroupAdmin['admin_group_is_active']==0)?'checked':''
                                                    )?>>
                                        Draft
                                    </label>
                                </div>
                            </div>
                            
                        

                    </div>
                    <div class="panel-footer">
                        <input type="submit" name="save" value="Simpan" class="btn btn-sm btn-success" />
                    </div>
                </form>
        </div>
        <!-- end: RESPONSIVE TABLE PANEL -->
    </div>
</div>





