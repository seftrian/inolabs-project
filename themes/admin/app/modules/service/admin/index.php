<div class="row">
    <div class="col-md-12">
    <div class="col-md-12">
    <div class="panel panel-default ">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
                </div>
            </div>
            <div class="panel-body">
            <div class="col-md-7"> 
            <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Label
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="label"  style="width:100%;" />
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Status
                    </label>
                    <div class="col-sm-8">
                        <select class="form-control" id="status" style="width: 100%;">
                            <option value="">Semua</option>
                            <option value="Y">Aktif</option>
                            <option value="N">Draft</option>
                        </select>
                    </div>
                 </div>
                
                 <div class="form-group">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-3">
                    <button class="btn btn-warning" type="reset" ><i class=" clip-remove"></i> Hapus Form</button>
                    </div>  
                    <div class="col-sm-5" >
                    <button style="float:right;" class="btn btn-info" onclick="grid_reload();return false;"><i class=" clip-search"></i> Cari</button>
                    </div>       
                </div>
            </form>
            </div>
            <div class="col-md-5"> 
            </div> 
        </div>
    </div>
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
        
          <div class="alert" style="display:none;"></div>                                                                       
         <table id="gridview" style="display:none;"></table>

                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>

         <!-- flexigrid starts here -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
        <!-- flexigrid ends here -->

<script type="text/javascript">
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'No', name: 'no', width: 50, sortable: false, align: 'right' },
                            { display: 'Edit', name: 'edit', width: 50, sortable: false, align: 'center' },
                            { display: 'Label', name: 'label', width: 180, sortable: false, align: 'center' },
                            { display: 'Status', name: 'status', width: 50, sortable: false, align: 'center' },
                    
                        ],
                        buttons: [
                            { display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Aktifkan Item Terpilih', name: 'publish', bclass: 'publish', onpress: act_publish },
                            { separator: true },
                            { display: 'NonAktifkan Item Terpilih', name: 'unpublish', bclass: 'unpublish', onpress: act_unpublish },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                        ],

                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '200',
                        resizable: false,
                        singleSelect: false,
                        nowrap:false,
                    });

                    $(document).ready(function() {
                        grid_reload();
                    });

                    function grid_reload() {
                        var label = $('#label').val();
                        var status = $('#status').val();
                        var service_link = '?label='+label+'&status='+status;
                        $("#gridview").flexOptions({url:'<?php echo base_url().$currentModule; ?>/service_rest/get_data'+service_link}).flexReload();
                    }

                    function act_delete(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    function act_publish(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$currentModule; ?>/act_publish',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    function act_unpublish(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$currentModule; ?>/act_unpublish',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }

                    function delete_transaction(id)
                    {
                        if(confirm('Delete?'))
                        {
                             var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$currentModule?>/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            if(response['status']!=200)
                            {
                                alert(response['message']);
                            }
                            grid_reload();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            grid_reload();
                            return false;
                        });
                        }
                        return false;
                       
                    }

</script>