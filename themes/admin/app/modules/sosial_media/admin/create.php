<div class="col-sm-12">

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Account <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="social_media_name" value="<?php echo $this->input->post('social_media_name'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Link
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="social_media_link" value="<?php echo $this->input->post('social_media_link'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Icon
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="social_media_icon_src" value="<?php echo $this->input->post('social_media_icon_src'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Status
            </label>
            <div class="col-sm-10">
                <label><input type="radio" name="social_media_is_published" value="Y" checked> Y </label>
                &nbsp;
                <label><input type="radio" name="social_media_is_published" value="N"> N </label> 
            </div>
        </div>
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
            <input type="reset" value="Reset" class="btn btn-warning" style="float:left;">
        </div>
        <div class="col-sm-6">
            <input style="float: right;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
