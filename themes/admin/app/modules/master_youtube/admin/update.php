<div class="col-sm-12">
    

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               Youtube  ID <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="hidden" name="youtube_id_old" value="<?php echo $this->input->post('youtube_id_old')?$this->input->post('youtube_id_old'):$youtube_id; ?>" />
                <input type="text"  onchange="preview_youtube($(this));return false;" class="form-control" name="youtube_id" value="<?php echo $this->input->post('youtube_id')?$this->input->post('youtube_id'):$youtube_id; ?>" />
            </div>
        </div>
        <div class="form-group" id="div-preview-youtube" style="display:none;">
            <label class="col-sm-2 control-label" for="form-field-1">
                 
            </label>
            <div class="col-sm-1">
            Preview
            </div>
            <div class="col-sm-5">
                <div id="preview-youtube" class="well" style="min-height:100px;">
                    
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Judul <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="youtube_title" value="<?php echo $this->input->post('youtube_title')?$this->input->post('youtube_title'):$youtube_title; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Isi <span class="symbol required"/>

            </label>
            <div class="col-sm-10">
          <?php echo form_textarea_tinymce('youtube_description', ($this->input->post('youtube_description')?$this->input->post('youtube_description'):$youtube_description), 'Standard'); ?>
            </div>
        </div>

        <div class="form-group">
        <div class="col-sm-2">
        </div>
         <div class="col-sm-4">
            <a href="<?php echo base_url()?>admin/image_slide/index" onclick="return confirm('Batalkan penyimpanan?');return false;">Batal</a>
            <input style="margin-left:20px;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
