<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
                                                        <a href="<?php echo base_url().'admin/dashboard'?>">Dashboard</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
                                                
						<li class="active"> Label Item Menu</li>
					</ul><!--.breadcrumb-->

				</div>

<div class="page-content">
                                    
                                <div class="page-header position-relative">
                                        <h1>
                                                Label Item Menu
                                        </h1>
                                </div><!--/.page-header-->
                                
                                <div class="row-fluid">
                                    <div class="span12">

<!--<form id="test" method="post" action="">                                        
        <div class="wysiwyg-editor" id="editor1"></div>
        <textarea id="divEditor1" ></textarea>
<input type="submit" value="Save" />
</form>-->
                                        
<?php 
foreach($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php  echo $output->output; ?>

                                    </div>
                                    </div>
                                    </div>

