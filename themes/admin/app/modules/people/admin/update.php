




<div class="row">
                        <div class="col-sm-12">
                        <?php
                                        //untuk menampilkan pesan
                                        if(trim($message)!='')
                                        {
                                            
                                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                                        }
                                        ?>
                            <div class="tabbable">
                                <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                                  <li class="active">
                                        <a data-toggle="tab" href="#panel_overview">
                                            Overview
                                        </a>
                                    </li>
                                    <li >
                                        <a data-toggle="tab" href="#panel_edit_account">
                                            Ubah Info
                                        </a>
                                    </li>
                                    <li >
                                        <a data-toggle="tab" href="#panel_edit_insurance">
                                            Asuransi
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                <div id="panel_edit_insurance" class="tab-pane">
                                        <div class="row">
                                            <div class="col-md-6">
                                            <form action="" method="post" enctype="multipart/form-data" role="form" id="form-insurance">
                                            <input type="hidden" name="person_insurance_person_id" value="<?php echo $person_id?>" />
                                            <div class="row insurance_field_init">
                                            </div>
                                            <?php
                                            if(!empty($person_insurance_arr))
                                            {
                                                $no=1;
                                                foreach($person_insurance_arr AS $rowArrIns)
                                                {
                                                     foreach($rowArrIns AS $variableIns=>$valueIns)
                                                    {
                                                        ${$variableIns}=$valueIns;
                                                    }
                                                    ?>
                                                     <div class="row insurance_field">
                                                     <input type="hidden" name="person_insurance_id[]" value="<?php echo $person_insurance_id?>" />
                                                        <div class="col-md-6">
                                                        <div class="form-group">
                                                            <select name="person_insurance_insurance_id[]" class="form-control">
                                                            <option value="">Pilih Asuransi</option>
                                                             <?php
                                                            if(!empty($insurance_arr))
                                                            {
                                                                foreach($insurance_arr AS $rowArr)
                                                                {
                                                                    foreach($rowArr AS $variable=>$value)
                                                                    {
                                                                        ${$variable}=$value;
                                                                    }
                                                                    $selected=($person_insurance_insurance_id==$insurance_id)?'selected':'';
                                                                    ?>
                                                                    <option value="<?php echo $insurance_id?>" <?php echo $selected?>><?php echo $insurance_product?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input placeholder="no. asuransi" type="text" name="person_insurance_policy_number[]" value="<?php echo $person_insurance_policy_number?>" class="form-control" />
                                                        </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                        <div class="form-group">
                                                        <a href="#" class="delete_shopping_cart btn btn-xs btn-danger">(-)</a>
                                                        </div>
                                                        </div>

                                                    </div>
                                                    <?php
                                                    $no++;
                                                }
                                            }

                                            ?>
                                           

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a onclick="generate_html();preventDefault();return false;" href="#" class="btn btn-xs btn-green">(+)</a>
                                                </div>
                                            </div>
                                           
                                            <div class="row">
                                             <div class="col-md-8">
                                             </div>
                                                <div class="col-md-4">
                                                    <input onclick="save_person_insurance();return false;" type="submit" name="save" value="Simpan" class="btn btn-teal btn-block" />
                                                </div>
                                            </div>
                                        </form>
                                            </div>
                                        </div>
                                    </div>

                                   <div id="panel_overview" class="tab-pane in active">
                                        <div class="row">
                                            <div class="col-sm-5 col-md-4">
                                                <div class="user-left">
                                                    <div class="center">
                                                        <h4><?php echo $person_full_name?></h4>
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="user-image">
                                                                <div class="fileupload-new thumbnail"><img src="<?php echo $img?>" alt="">
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <p>
                                                            <?php echo isset($additional_info_arr['additional_info_twitter'])?'<a href="http://twitter.com/'.$additional_info_arr['additional_info_twitter'].'" target="_blank" class="btn btn-twitter btn-sm btn-squared">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>':''?>
                                                            <?php echo isset($additional_info_arr['additional_info_facebook'])?'<a href="http://facebook.com/'.$additional_info_arr['additional_info_facebook'].'" target="_blank" class="btn btn-twitter btn-sm btn-squared">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>':''?>
                                                            <?php echo isset($additional_info_arr['additional_info_linkedin'])?'<a href="http://id.linkedin.com/in/'.$additional_info_arr['additional_info_linkedin'].'" target="_blank" class="btn btn-twitter btn-sm btn-squared">
                                                                <i class="fa fa-linkedin"></i>
                                                            </a>':''?>
                                                        

                                                           
                                                        </p>
                                                        <hr>
                                                    </div>
                                                    <table class="table table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="3">Contact Information</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>email:</td>
                                                                <td>
                                                                    <?php echo (isset($additional_info_arr['additional_info_email_address']) AND trim($additional_info_arr['additional_info_email_address'])!='')?'<a href="mailto:'.$additional_info_arr['additional_info_email_address'].'" class="btn btn-twitter btn-sm btn-squared">
                                                                <i class="fa fa-envelope"></i>
                                                            </a>':'-'?>
                                                                </td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>telephone:</td>
                                                                <td> <?php echo $person_telephone?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>phone:</td>
                                                                <td> <?php echo $person_phone?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>skype:</td>
                                                                <td>
                                                                <a href="#">
                                                                <?php echo (isset($additional_info_arr['additional_info_skype']) AND trim($additional_info_arr['additional_info_skype'])!='')?$additional_info_arr['additional_info_skype']:'-'?>
                                                                </a></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="3">General information</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Alamat</td>
                                                                <td><?php echo $person_address_value?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Provinsi</td>
                                                                <td><?php echo $this->function_lib->get_area_name($person_address_province_id)?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Kota</td>
                                                                <td><?php echo $this->function_lib->get_area_name($person_address_city_id)?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Kecamatan</td>
                                                                <td>
                                                                <?php echo $this->function_lib->get_area_name($person_address_district_id)?>
                                                                </td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td><span class="label label-sm label-info"><?php echo $person_is_active=='Y'?'<i class="glyphicon glyphicon-ok-sign"></i>':'<i class="glyphicon glyphicon-remove-circle"></i>'?></span></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="3">Additional information</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                                <td>Jenis Kelamin</td>
                                                                <td><?php echo $person_gender?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tgl Lahir</td>
                                                                <td><?php echo date('d-m-Y',strtotime($person_birth_date))?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                             <tr>
                                                                <td>Tempat Lahir</td>
                                                                <td><?php echo $person_birth_place?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>

                                                             <tr>
                                                                <td>Gol. Darah</td>
                                                                <td><?php echo $person_blood_type?></td>
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-sm-7 col-md-8">
                                               
                                                <div class="panel panel-white">
                                                    <div class="panel-heading">
                                                        <i class="clip-menu"></i>
                                                        Recent Activities
                                                        <div class="panel-tools">
                                                            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                            </a>
                                                          
                                                        </div>
                                                    </div>
                                                    <div class="panel-body panel-scroll" style="height:300px">
                                                        <ul class="activities">
                                                            <li>
                                                                <a class="activity" href="javascript:void(0)">
                                                                    <span class="desc">Belum ada data saat ini.</span>
                                                                    <div class="time">
                                                                        <i class="fa fa-time bigger-110"></i>
                                                                        
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>

                                    <div id="panel_edit_account" class="tab-pane in">
                                        <form action="" method="post" enctype="multipart/form-data" role="form" id="form">
                                            <div class="row">
                                                
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Title
                                                                </label>
                                                               <select name="person_title" class="form-control">
                                                    <?php
                                                    if(!empty($person_title_arr))
                                                    {
                                                        foreach($person_title_arr AS $value)
                                                        {
                                                            $selected=($this->input->post('person_title')==$value)?'selected':(
                                                                ($person_title==$value)?'selected':''
                                                                );
                                                            ?>
                                                            <option value="<?php echo $value;?>" <?php echo $selected;?>><?php echo $value;?></option>
                                                            <?php   
                                                        }
                                                    }
                                                    ?>
                                                    </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                <span class="symbol required"></span>    Nama Lengkap
                                                                </label>
                                                                <input name="person_full_name" value="<?php echo $this->input->post('person_full_name')?$this->input->post('person_full_name'):$person_full_name;?>" class="form-control tooltips" placeholder="Nama Lengkap" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Jenis Kelamin
                                                                </label>
                                                               <select name="person_gender" class="form-control">
                                                               <option value="L" <?php echo ($this->input->post('person_gender')=='L')?'selected':(
                                                                ($person_gender=='L')?'selected':''
                                                               )?>>Laki-laki</option>
                                                               <option value="P" <?php echo ($this->input->post('person_gender')=='P')?'selected':(
                                                                ($person_gender=='P')?'selected':''
                                                               )?>>Perempuan</option>
                                                               
                                                               </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Golongan Darah
                                                                </label>
                                                                <select name="person_blood_type" class="form-control">
                                                               <option value="A" <?php echo ($this->input->post('person_blood_type')=='A')?'selected':(
                                                                ($person_blood_type=='A')?'selected':''
                                                               )?>>A</option>
                                                               <option value="B" <?php echo ($this->input->post('person_blood_type')=='B')?'selected':(
                                                                ($person_blood_type=='B')?'selected':''
                                                               )?>>B</option>
                                                               <option value="AB" <?php echo ($this->input->post('person_blood_type')=='AB')?'selected':(
                                                                ($person_blood_type=='AB')?'selected':''
                                                               )?>>AB</option>
                                                               <option value="O" <?php echo ($this->input->post('person_blood_type')=='O')?'selected':(
                                                                ($person_blood_type=='O')?'selected':''
                                                               )?>>O</option>
                                                               </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Tanggal Lahir <em>(yyyy-mm-dd)</em>
                                                                </label>
                                                                  <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control tooltips date-picker" name="person_birth_date" value="<?php echo $this->input->post('person_birth_date')?$this->input->post('person_birth_date'):$person_birth_date;?>" placeholder="Tgl Lahir" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Tempat Lahir
                                                                </label>
                                                             <input class="form-control tooltips" value="<?php echo $this->input->post('person_birth_place')?$this->input->post('person_birth_place'):$person_birth_place;?>" name="person_birth_place" placeholder="Tempat Lahir" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >

                                                            </div>
                                                        </div>
                                                    </div>
<div class="form-group">                                
                                                      <div class="form-group">
                                                        <label class="control-label">
                                                            No. Telp.
                                                        </label>
                                                        <div>
                                                           <input class="form-control tooltips" value="<?php echo $this->input->post('person_telephone')?$this->input->post('person_telephone'):$person_telephone;?>" name="person_telephone" placeholder="Telephone" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >

                                                        </div>

                                                        </div>
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            No. HP.
                                                        </label>
                                                        <div>
                                                              <input class="form-control tooltips" value="<?php echo $this->input->post('person_phone')?$this->input->post('person_phone'):$person_phone;?>" name="person_phone" placeholder="Phone" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >

                                                        </div>

                                                        </div>
                                                     <div class="form-group">    
                                                         <label class="control-label">
                                                            Alamat
                                                        </label>
                                                        <div>
                                                                <textarea placeholder="Alamat" name="person_address_value"  class="form-control"><?php echo $this->input->post('person_address_value',true)?$this->input->post('person_address_value'):$person_address_value; ?></textarea> 
                                            
                                                        </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                  
                                                    
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            Provinsi
                                                        </label>
                                                        <div>
                                                        <select name="person_address_province_id" class="form-control" onchange="get_sub_area_by_area_id($(this).val(),$(this).attr('name'));">
                                                        </select>       
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            Kota / Kabupaten 
                                                        </label>
                                                        <div>
                                                        <select name="person_address_city_id" class="form-control" onchange="get_sub_area_by_area_id($(this).val(),$(this).attr('name'));">
                                                        </select>
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            Kecamatan
                                                        </label>
                                                        <div>
                                                         <select name="person_address_district_id" class="form-control">
                                                         </select>
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>
                                                            Image Upload
                                                        </label>
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;"><img src="<?php echo $img;?>" />
                                                            </div>
                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;">
                                                                
                                                            </div>
                                                            <div class="user-edit-image-buttons">
                                                                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                                    <input type="hidden" name="person_photo_old" value="<?php $this->input->post('person_photo_old')?$this->input->post('person_photo_old'):$person_photo?>">
                                                                    <input type="file" name="person_photo">
                                                                </span>
                                                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                                                    <i class="fa fa-times"></i> Remove
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                            <label>
                                                Status
                                            </label>
                                            <div >
                                               <select name="person_is_active" class="form-control">
                                                <option value="Y" <?php echo (isset($_POST['person_is_active']) AND $_POST['person_is_active']=='Y')?'selected':(
                                                                ($person_is_active=='Y')?'selected':''
                                                               );?>>Aktif</option>
                                                <option value="N" <?php echo (isset($_POST['person_is_active']) AND $_POST['person_is_active']=='N')?'selected':(
                                                                ($person_is_active=='N')?'selected':''
                                                               );?>>Draft</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3>Additional Info</h3>
                                                    <hr>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                        <label class="control-label">
                                                            Email
                                                        </label>
                                                        <span class="input-icon">
                                                            <input class="form-control" name="additional_info_email_address" value="<?php echo $this->input->post('additional_info_email_address')?$this->input->post('additional_info_email_address'):(
                                                            (isset($additional_info_arr['additional_info_email_address']) AND trim($additional_info_arr['additional_info_email_address'])!='')?$additional_info_arr['additional_info_email_address']:''
                                                            )
                                                            ;?>" type="text" placeholder="Text Field">
                                                            <i class="clip-bubble"></i> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Twitter
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_twitter" value="<?php echo $this->input->post('additional_info_twitter')?$this->input->post('additional_info_twitter'):(
                                                            (isset($additional_info_arr['additional_info_twitter']) AND trim($additional_info_arr['additional_info_twitter'])!='')?$additional_info_arr['additional_info_twitter']:''
                                                            );?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-twitter"></i> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Facebook
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_facebook" value="<?php echo $this->input->post('additional_info_facebook')?$this->input->post('additional_info_facebook'):(
                                                            (isset($additional_info_arr['additional_info_facebook']) AND trim($additional_info_arr['additional_info_facebook'])!='')?$additional_info_arr['additional_info_facebook']:''
                                                            );?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-facebook"></i> </span>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Linkedin
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_linkedin" value="<?php echo $this->input->post('additional_info_linkedin')?$this->input->post('additional_info_linkedin'):(
                                                            (isset($additional_info_arr['additional_info_linkedin']) AND trim($additional_info_arr['additional_info_linkedin'])!='')?$additional_info_arr['additional_info_linkedin']:''
                                                            );?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-linkedin"></i> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Skype
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_skype" value="<?php echo $this->input->post('additional_info_skype')?$this->input->post('additional_info_skype'):(
                                                            (isset($additional_info_arr['additional_info_skype']) AND trim($additional_info_arr['additional_info_skype'])!='')?$additional_info_arr['additional_info_skype']:''
                                                            );?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-skype"></i> </span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            Profesi Medis
                                                        </label>
                                                        <select name="person_type[0]" class="form-control">
                                                        <option value="">Pilihan Profesi</option>
                                                        <?php
                                                        if(!empty($medic_arr))
                                                        {
                                                            foreach($medic_arr AS $rowArr)
                                                            {
                                                                foreach($rowArr AS $variable=>$value)
                                                                {
                                                                    ${$variable}=$value;
                                                                }
                                                                $where='person_type_detail_person_id='.intval($person_id).'
                                                                AND person_type_detail_person_type_id='.intval($person_type_id);
                                                                $is_exist=$this->function_lib->get_one('person_type_detail_person_type_id','sys_person_type_detail',$where);
                                                                $selected=(isset($_POST['person_type'][0]) AND $_POST['person_type'][0]==$person_type_id)?'selected':(
                                                                    ($is_exist==$person_type_id)?'selected':''
                                                                );
                                                                ?>
                                                                <option value="<?php echo $person_type_id?>" <?php echo $selected?>><?php echo $person_type_name?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                        </label>
                                                        <?php
                                                        if(!empty($person_type_arr))
                                                        {
                                                            $index=1;    
                                                            foreach($person_type_arr AS $rowArr)
                                                            {
                                                                foreach($rowArr AS $variable=>$value)
                                                                {
                                                                    ${$variable}=$value;
                                                                }
                                                                 $where='person_type_detail_person_id='.intval($person_id).'
                                                                AND person_type_detail_person_type_id='.intval($person_type_id);
                                                                $is_exist=$this->function_lib->get_one('person_type_detail_person_type_id','sys_person_type_detail',$where);
                                                                
                                                                $checked=(isset($_POST['person_type'][$index]) AND $_POST['person_type'][$index]==$person_type_id)?'checked':(
                                                                    ($is_exist==$person_type_id)?'checked':''
                                                                );

                                                                ?>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input name="person_type[<?php echo $index?>]"  type="checkbox" value="<?php echo $person_type_id?>" class="square-orange" <?php echo $checked;?>>
                                                                            <?php echo $person_type_name?>
                                                                        </label>
                                                                    </div>
                                                                <?php
                                                                $index++;
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                      <div class="form-group">
                                                        <label class="control-label">
                                                           
                                                        </label>
                                                      
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="person_is_employee" value="Y" class="square-orange" <?php echo ($this->input->post('person_is_employee')=='Y')?'checked':(
                                                                    ($person_is_employee=='Y')?'checked':''
                                                                )?>>
                                                                Karyawan
                                                            </label>
                                                        </div>
                                                     
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <p>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="submit" name="save" value="Simpan" class="btn btn-teal btn-block" />
                                        
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>