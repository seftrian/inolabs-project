  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">Data Penduduk</h4>
      </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
              <!-- start: TEXT FIELDS PANEL -->
              
                 <form id="form-cash">
                 <div class="col-sm-12">

                    <table id="gridview" style="display:none;"></table>
                    <form target="_blank" id="form_export" method="post" action="export_data">
                        <input id="export_start_date" type="hidden" name="export_start_date" value="" />
                        <input id="export_end_date" type="hidden" name="export_end_date" value="" />
                    </form>
                 </div>
                 </form>
              <!-- end: TEXT FIELDS PANEL -->
            </div>
          </div>
      
      </div>
  <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">
          Close 
        </button>
      </div>
     
     <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'Actions', name: 'actions', width: 130, sortable: false, align: 'center' },
                    
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Nama Lengkap', name: 'person_full_name', width: 260, sortable: true, align: 'center' },
                            { display: 'Phone', name: 'person_phone', width: 260, sortable: true, align: 'center' },
                            { display: 'Alamat', name: 'person_address_value', width: 260, sortable: true, align: 'center' },
                        
                            { display: 'Jenis Kelamin', name: 'person_gender', width: 100, sortable: true, align: 'center' },
                            { display: 'Tempat / Tgl Lahir', name: 'person_birth_date', width: 260, sortable: true, align: 'center' },
                            { display: 'Foto', name: 'person_photo', width: 150, sortable: false, align: 'center' },
                            { display: 'Tanggal', name: 'person_timestamp', width: 130, sortable: false, align: 'center' },
                           
                        ],
                        buttons: [
                           
                        ],
                        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],
                          searchitems: [
                            { display: 'Nama', name: 'person_full_name', type: 'text', isdefault: true },
                            { display: 'Email', name: 'additional_info_email_address', type: 'text', isdefault: true },
                            { display: 'Telp', name: 'person_telephone', type: 'text', isdefault: true },
                            { display: 'HP', name: 'person_phone', type: 'text', isdefault: true },
                            { display: 'Alamat', name: 'person_address_value', type: 'text', isdefault: true },
                            { display: 'Status', name: 'person_is_active', type: 'select', option: 'Y:Aktif|N:Draft' },
                          // { display: 'Tanggal', name: 'person_timestamp', type: 'date' },
                            
                        ],
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });

                    $(document).ready(function() {
                        grid_reload();
                    });

                   

                    function grid_reload() {
                        $("#gridview").flexOptions({url:'<?php echo base_url().$this->currentModule; ?>/service_rest/get_data_person'}).flexReload();
                    }
                    
              

                function load_data_person_by_id(person_id)
                {
                    var jqxhr=$.ajax({
                        url:'<?php echo base_url()?>dokter/service_rest/load_data_person_by_id/'+person_id,
                        type:'get',
                        dataType:'json',

                    });
                    jqxhr.success(function(response){
                        if(response['status']!=200)
                        {
                            alert('Terjadi kesalahan, mohon dicoba lagi');
                            return false;
                        }
                        else
                        {
                            var data=response['data'];
                            $("input[name='person_id']").val(data['person_id']);
                            $("input[name='person_full_name']").val(data['person_full_name']);
                            $("input[name='person_birth_date']").val(data['person_birth_date']);
                            $("input[name='person_birth_place']").val(data['person_birth_place']);
                            $("input[name='person_telephone']").val(data['person_telephone']);
                            $("input[name='person_phone']").val(data['person_phone']);
                            $("input[name='additional_info_email_address']").val(data['additional_info_email_address']);
                            $("input[name='additional_info_twitter']").val(data['additional_info_twitter']);
                            $("input[name='additional_info_facebook']").val(data['additional_info_facebook']);
                            $("input[name='additional_info_linkedin']").val(data['additional_info_linkedin']);
                            $("input[name='additional_info_skype']").val(data['additional_info_skype']);

                            $("select[name='person_title']").val(data['person_title']);
                            $("select[name='person_gender']").val(data['person_gender']);
                            $("select[name='person_address_province_id']").val(data['person_address_province_id']);
                          //  $("select[name='person_address_city_id']").val(data['person_address_city_id']);
                            get_sub_area_by_area_id(data['person_address_province_id'],'person_address_province_id',data['person_address_city_id']);
                            get_sub_area_by_area_id(data['person_address_city_id'],'person_address_city_id',data['person_address_district_id']);
                  
    //                        $("select[name='person_address_district_id']").val(data['person_address_district_id']);
                            $("div#preview_photo").html('<img src="<?php echo base_url()?>'+data['person_photo']+'" />');
                            $("textarea[name='person_address_value']").val(data['person_address_value']);

                            console.log(data);    
                            $('#ajax-modal').modal('toggle');
                            return false;
                        }


                    });
                    jqxhr.error(function(){
                        alert('an error has occurred, please try again');
                    });
                }    

                    
                </script>  