<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
        <!-- start: RESPONSIVE TABLE PANEL -->
         <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
                </div>
            </div>

         <div class="panel-body">   
            <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
              <div class="col-sm-6">   
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Nama Lengkap
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="person_full_name"  style="width:100%;" />
                    </div>
                 </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                       Email
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="person_email"  style="width:100%;" />
                    </div>
                 </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                       Telepon / Hp.
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="person_phone"  style="width:100%;" />
                    </div>
                 </div>
                </div>
                <div class="col-sm-6"> 
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="form-field-1">
                           Alamat
                        </label>
                        <div class="col-sm-8">
                            <textarea id="person_address" style="width:100%;"></textarea> 
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="form-field-1">
                           Profesi
                        </label>
                        <div class="col-sm-8">
                            <select id="person_type"  style="width:100%;">
                            <option value="">Pilihan Profesi</option>
                            <?php
                            if(!empty($medic_arr))
                            {
                                foreach($medic_arr AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                    ?>
                                    <option value="<?php echo $person_type_id?>"><?php echo $person_type_name?></option>
                                    <?php
                                }
                            }

                            if(!empty($person_type_arr))
                            {
                                $index=1;    
                                foreach($person_type_arr AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                    ?>
                                    <option value="<?php echo $person_type_id?>"><?php echo $person_type_name?></option>
                                    <?php
                                    $index++;
                                }
                            }
                            ?>
                            </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="form-field-1">
                        </label>
                        <div class="col-sm-8">
                        <select id="person_is_employee"  style="width:100%;">
                        <option value="">Semua</option>
                        <option value="Y">Karyawan</option>
                        <option value="N">Bukan Karyawan</option>
                        </select>
                        </div>
                     </div>
                </div>
                <div class="col-sm-12"> 
                    <div class="form-group">
                        <div class="col-sm-2 col-sm-offset-2">
                        <button class="btn btn-warning" type="reset"><i class=" clip-remove"></i> Hapus Form</button>
                        </div>
                        <div class="col-sm-3 col-sm-offset-1">
                        <button class="btn btn-info" onclick="grid_reload();return false;" ><i class=" clip-search"></i> Cari</button>
                        </div>       
                    </div>  
                </div>
            </form>
        </div>
        </div>
	    <table id="gridview" style="display:none;"></table>
        <form target="_blank" id="form_export" method="post" action="export_data">
            <input id="export_start_date" type="hidden" name="export_start_date" value="" />
            <input id="export_end_date" type="hidden" name="export_end_date" value="" />
        </form>
		<!-- end: RESPONSIVE TABLE PANEL -->
    </div>
</div>
