
<div class="row">
                        <div class="col-sm-12">
                        <?php
                                        //untuk menampilkan pesan
                                        if(trim($message)!='')
                                        {
                                            
                                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                                        }
                                        ?>
                            <div class="tabbable">
                                <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                                  
                                    <li class="active">
                                        <a data-toggle="tab" href="#panel_edit_account">
                                            Info
                                        </a>
                                    </li>
                                    
                                </ul>
                                <div class="tab-content">
                                   

                                    <div id="panel_edit_account" class="tab-pane in active">
                                        <form action="" method="post" enctype="multipart/form-data" role="form" id="form">
                                            <input type="hidden" value="" name="person_id">
                                            <div class="row">
                                                
                                                <div class="col-md-6">
                                                       
                                                    <div class="row">

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Title
                                                                </label>
                                                               <select name="person_title" class="form-control">
                                                    <?php
                                                    if(!empty($person_title_arr))
                                                    {
                                                        foreach($person_title_arr AS $value)
                                                        {
                                                            $selected=($this->input->post('person_title')==$value)?'selected':'';
                                                            ?>
                                                            <option value="<?php echo $value;?>" <?php echo $selected;?>><?php echo $value;?></option>
                                                            <?php   
                                                        }
                                                    }
                                                    ?>
                                                    </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                <span class="symbol required"></span>    Nama Lengkap
                                                                </label>
                                                                <input name="person_full_name" value="<?php echo $this->input->post('person_full_name');?>" class="form-control tooltips" placeholder="Nama Lengkap" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Jenis Kelamin
                                                                </label>
                                                               <select name="person_gender" class="form-control">
                                                               <option value="L" <?php echo ($this->input->post('person_gender')=='L')?'selected':''?>>Laki-laki</option>
                                                               <option value="P" <?php echo ($this->input->post('person_gender')=='P')?'selected':''?>>Perempuan</option>
                                                               
                                                               </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Golongan Darah
                                                                </label>
                                                                <select name="person_blood_type" class="form-control">
                                                               <option value="A" <?php echo ($this->input->post('person_blood_type')=='A')?'selected':''?>>A</option>
                                                               <option value="B" <?php echo ($this->input->post('person_blood_type')=='B')?'selected':''?>>B</option>
                                                               <option value="AB" <?php echo ($this->input->post('person_blood_type')=='AB')?'selected':''?>>AB</option>
                                                               <option value="O" <?php echo ($this->input->post('person_blood_type')=='O')?'selected':''?>>O</option>
                                                               </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Tanggal Lahir <em>(yyyy-mm-dd)</em>
                                                                </label>
                                                                  <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control tooltips date-picker" name="person_birth_date" value="<?php echo $this->input->post('person_birth_date')?$this->input->post('person_birth_date'):date('Y-m-d');?>" placeholder="Tgl Lahir" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Tempat Lahir  
                                                                </label>
                                                             <input  class="form-control tooltips" value="<?php echo $this->input->post('person_birth_place');?>" name="person_birth_place" placeholder="Tempat Lahir" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >

                                                            </div>
                                                        </div>
                                                    </div>
<div class="form-group">                                
                                                      <div class="form-group">
                                                        <label class="control-label">
                                                            No. Telp.
                                                        </label>
                                                        <div>
                                                           <input class="form-control tooltips" value="<?php echo $this->input->post('person_telephone');?>" name="person_telephone" placeholder="Telephone" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >

                                                        </div>

                                                        </div>
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            No. HP.
                                                        </label>
                                                        <div>
                                                              <input class="form-control tooltips" value="<?php echo $this->input->post('person_phone');?>" name="person_phone" placeholder="Phone" type="text" data-original-title="" data-rel="tooltip" title="" data-placement="top" >

                                                        </div>

                                                        </div>
                                                     <div class="form-group">    
                                                         <label class="control-label">
                                                            Alamat
                                                        </label>
                                                        <div>
                                                                <textarea placeholder="Alamat" name="person_address_value"  class="form-control"><?php echo $this->input->post('person_address_value',true); ?></textarea> 
                                            
                                                        </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                  
                                                    
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            Provinsi
                                                        </label>
                                                        <div>
                                                        <select name="person_address_province_id" class="form-control" onchange="get_sub_area_by_area_id($(this).val(),$(this).attr('name'));">
                                                        </select>       
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            Kota / Kabupaten 
                                                        </label>
                                                        <div>
                                                        <select name="person_address_city_id" class="form-control" onchange="get_sub_area_by_area_id($(this).val(),$(this).attr('name'));">
                                                        </select>
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="control-label">
                                                            Kecamatan
                                                        </label>
                                                        <div>
                                                         <select name="person_address_district_id" class="form-control">
                                                         </select>
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>
                                                            Image Upload
                                                        </label>
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="fileupload-new thumbnail" id="preview_photo" style="width: 150px; height: 150px;">
                                                            </div>
                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                                            <div class="user-edit-image-buttons">
                                                                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                                    <input type="file" name="person_photo">
                                                                </span>
                                                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                                                    <i class="fa fa-times"></i> Remove
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                            <label>
                                                Status
                                            </label>
                                            <div >
                                               <select name="person_is_active" class="form-control">
                                                <option value="Y" <?php echo (isset($_POST['person_is_active']) AND $_POST['person_is_active']=='Y')?'selected':'';?>>Aktif</option>
                                                <option value="N" <?php echo (isset($_POST['person_is_active']) AND $_POST['person_is_active']=='N')?'selected':'';?>>Draft</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3>Additional Info</h3>
                                                    <hr>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                        <label class="control-label">
                                                            Email
                                                        </label>
                                                        <span class="input-icon">
                                                            <input class="form-control" name="additional_info_email_address" value="<?php echo $this->input->post('additional_info_email_address');?>" type="text" placeholder="Text Field">
                                                            <i class="clip-bubble"></i> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Twitter
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_twitter" value="<?php echo $this->input->post('additional_info_twitter');?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-twitter"></i> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Facebook
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_facebook" value="<?php echo $this->input->post('additional_info_facebook');?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-facebook"></i> </span>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Linkedin
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_linkedin" value="<?php echo $this->input->post('additional_info_linkedin');?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-linkedin"></i> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Skype
                                                        </label>
                                                        <span class="input-icon">
                                                            <input name="additional_info_skype" value="<?php echo $this->input->post('additional_info_skype');?>" class="form-control" type="text" placeholder="Text Field">
                                                            <i class="clip-skype"></i> </span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                        <label class="control-label">
                                                            Profesi Medis
                                                        </label>
                                                        <select name="person_type[0]" class="form-control">
                                                        <option value="">Pilihan Profesi</option>
                                                        <?php
                                                        if(!empty($medic_arr))
                                                        {
                                                            foreach($medic_arr AS $rowArr)
                                                            {
                                                                foreach($rowArr AS $variable=>$value)
                                                                {
                                                                    ${$variable}=$value;
                                                                }
                                                                $selected=(isset($_POST['person_type'][0]) AND $_POST['person_type'][0]==$person_type_id)?'selected':'';
                                                                ?>
                                                                <option value="<?php echo $person_type_id?>" <?php echo $selected?>><?php echo $person_type_name?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                        </label>
                                                        <?php
                                                        if(!empty($person_type_arr))
                                                        {
                                                            $index=1;    
                                                            foreach($person_type_arr AS $rowArr)
                                                            {
                                                                foreach($rowArr AS $variable=>$value)
                                                                {
                                                                    ${$variable}=$value;
                                                                }
                                                                $checked=(isset($_POST['person_type'][$index]) AND $_POST['person_type'][$index]==$person_type_id)?'checked':'';

                                                                ?>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input name="person_type[<?php echo $index?>]"  type="checkbox" value="<?php echo $person_type_id?>" class="square-orange" <?php echo $checked;?>>
                                                                            <?php echo $person_type_name?>
                                                                        </label>
                                                                    </div>
                                                                <?php
                                                                $index++;
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                      <div class="form-group">
                                                        <label class="control-label">
                                                           
                                                        </label>
                                                      
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="person_is_employee" value="Y" class="square-orange" <?php echo ($this->input->post('person_is_employee')=='Y')?'checked':''?>>
                                                                Karyawan
                                                            </label>
                                                        </div>
                                                     
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <p>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="submit" name="save" value="Simpan" class="btn btn-teal btn-block" />
                                        
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<div id="ajax-modal" data-width="60%" class="modal fade" tabindex="-1" data-focus-on="input#payment_cash" style="display: none;"></div>
