<?php
$logo_name= (trim($slider_image_src)=='')?'-': pathinfo($slider_image_src,PATHINFO_FILENAME);
$logo_ext=  pathinfo($slider_image_src,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>350,
'height'=>233,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'350233/',
'urlSave'=>$pathImgUrl.'350233/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
?>
<div class="row">
    <div class="col-sm-12">
       <div class="panel panel-default">
            
        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="panel-body">
                <?php
                //untuk menampilkan pesan
                if(trim($message)!='')
                {
                    echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                }
                ?>
       
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Judul <span class="symbol required"/>
                </label>
                <div class="col-sm-10">
                    <input maxlength="150" type="text" class="form-control" name="slider_title" value="<?php echo $this->input->post('slider_title')?$this->input->post('slider_title'):$slider_title; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kategori Slider <span class="symbol required"/>
                </label>
                <div class="col-sm-6">
                    <select class="form-control" name="slider_category_id" onchange="get_size_slider_category($(this).val());">
                        <option value=""></option>
                        <?php
                        foreach ($slider_category as $rowArr) {
                            foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }            
                                    $selected=(isset($slider_slider_category_id) && $slider_slider_category_id==$slider_category_id)?'selected':'';
                            ?>
                            <option value="<?php echo $slider_category_id ?>" <?php echo $selected ?>><?php echo $slider_category_name ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Gambar <span class="symbol required"/>
                </label>
                <div class="col-sm-10">
                      <p class="text-danger">
                          <?php
                          if($logo_name=='-')
                          {
                            echo 'Belum ada gambar.';
                          }
                          else
                          {
                        ?>
                        <div class="box-slide">
                            <img src="<?php echo $img?>" />
                        </div>
                          <a href="<?php echo base_url()?>admin/image_slide/delete_image/<?php echo $slider_id?>" onclick="return confirm('Hapus?');return false;" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus Gambar</a>
                      
                        <?php      
                          }
                          ?>  
                      </p>  
                      <input type="hidden" name="slider_image_src" value="<?php echo $slider_image_src?>"/>
                      <input class="form-control" type="file" name="slider_image_src"/>
                      <small class="text-muted">File type : gif|jpg|jpeg|png. Max size : 500 KB <br /><em class="image-size">Ukuran gambar yang dianjurkan minimal <?php echo $size_image_slider_category ?></em></small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Link
                </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="slider_link" value="<?php echo $this->input->post('slider_link')?$this->input->post('slider_link'):$slider_link; ?>" />
                    <small class="text-muted"> example : http://www.google.co.id/ dan jika tidak ada sila dikosongkan saja.</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Youtube ID 
                </label>
                <div class="col-sm-2">
                    <input type="text" placeholder="youtube id" onchange="preview_youtube($(this));" class="form-control" name="slider_youtube_id" value="<?php echo $this->input->post('slider_youtube_id')?$this->input->post('slider_youtube_id'):$slider_youtube_id; ?>" />
                </div>
                <div class="col-sm-8">
                    <span class="text-warning">(hanya ID Youtube, contoh: https://www.youtube.com/watch?v=<b><u>Rc2dG7uMlhA</u></b>)</span>
                </div>
            </div>
            <div class="form-group" id="div-preview-youtube" style="display:none;">
                <label class="col-sm-2 control-label" for="form-field-1">
                     
                </label>
                <div class="col-sm-1">
                Preview
                </div>
                <div class="col-sm-5">
                    <div id="preview-youtube" class="well" style="min-height:100px;">
                        
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                   Meta Deskripsi
                </label>
                <div class="col-sm-10">
                     <textarea style="height:200px;" class="form-control"  placeholder="deskripsi singkat slider..." name="slider_short_description"><?php echo $this->input->post('slider_short_description')?$this->input->post('slider_short_description'):$slider_short_description; ?></textarea>
                <small class="text-muted"> Maks. 200 karakter dan memuat deskripsi singkat.</small>
                </div>
            </div>    
        </div>
            <div class="panel-footer">
                <a href="<?php echo base_url()?>admin/image_slide/index" onclick="return confirm('Batalkan penyimpanan?');return false;" class="btn btn-sm btn-warning" >Batal</a>
                <input onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-sm btn-success">
            </div>
        </form>
      </div>
    </div>
</div>