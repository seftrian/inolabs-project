<div class="row">
    <div class="col-sm-12">
        <div class="panel  panel-default mb-1">
            <div class="panel-body">
                <button class="btn btn-sm btn-info" onclick="window.location.href='<?php echo base_url() . 'admin/master_sub_master_satu/create' ?>'"><i class="fa fa-plus"></i> Tambah Kategori</button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover mb-1">
                <thead>
                    <tr>
                        <th style="text-align:center;width:3%;">No.</th>
                        <th style="text-align:center;width:5%;">Judul</th>
                        <th style="text-align:center;width:10%;">Deskripsi</th>
                        <th style="text-align:center;width:10%;">Gambar</th>
                        <th style="text-align:center;width:10%;">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                        </th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1 + $offset; ?>
                    <?php if (!empty($findAll)) { ?>
                        <?php foreach ($findAll as $rowArr) {
                        foreach ($rowArr as $variable => $value) {
                            ${$variable} = $value;
                        }
                        $delete = '<a onclick="return confirm(\'Hapus?\');" href="' . base_url() . 'admin/master_content_category/delete/' . $category_id . '" class="btn btn-xs btn-red"><span class="fa fa-trash-o"></span></a>';
                        $edit = '<a href="' . base_url() . 'admin/master_sub_master_satu/update/' . $category_id . '" class="btn btn-xs btn-warning"><span class="fa fa-edit"></span></a>';
                        $logo_name = (trim($category_photo) == '') ? '-' : pathinfo($category_photo, PATHINFO_FILENAME);
                        $logo_ext =  pathinfo($category_photo, PATHINFO_EXTENSION);
                        $logo_file = $logo_name . '.' . $logo_ext;
                        $pathImgUrl = $pathImgArr['pathUrl'];
                        $pathImgLoc = $pathImgArr['pathLocation'];
                        $imgProperty = array(
                                'width' => 70,
                                'height' => 70,
                                'imageOriginal' => $logo_file,
                                'directoryOriginal' => $pathImgLoc,
                                'directorySave' => $pathImgLoc . '7070/',
                                'urlSave' => $pathImgUrl . '7070/',
                            );
                        $img = $this->function_lib->resizeImageMoo($imgProperty);
                        $category_content = strip_tags($category_content);
                        $category_content = (strlen($category_content) > 200) ? substr($category_content, 200) . '...' : $category_content;
                        $count_news = master_content_lib::count_article_on_category($category_id); ?>
                            <tr>
                                <td style="text-align:center;"><?php echo $no; ?>.</td>
                                <td style="display:none;text-align:center;"><?php echo $delete ?></td>
                                <td><?php echo $category_title ?> <span class="badge badge-<?php echo ($count_news < 1) ? 'danger' : 'default' ?>"><?php echo $count_news ?></span><br />
                                    <span class="text-muted"><?php echo $category_type ?></span>
                                    <p style="float:right;" class="text-muted"><?php echo $category_meta_description ?> <br />
                                        <?php echo $category_meta_tags ?>
                                    </p>
                                </td>
                                <td><?php echo $category_content ?></td>
                                <td style="text-align:center;"><img src="<?php echo $img ?>" /></td>
                                <td style="text-align:center; width:5%;"><?php echo $edit ?></td>
                            </tr>
                            <?php $no++; ?>
                        <?php
                    } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="7" style="text-align: center;"> <i class="fa fa-info-circle text-danger"></i>&nbsp Data belum tersedia</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12 text-center">
            <?php echo $link_pagination; ?>
        </div>
    </div>
</div>