<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                <div class="panel-body">
                    <?php
                    //untuk menampilkan pesan
                    if (trim($msg) != '') {
                        echo $this->function_lib->show_message($msg, $templateName, $themeId, $sts);
                    }
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Jenis <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-5">
                            <select name="category_type" class="form-control">
                                <option value="article" <?php echo ($this->input->post('category_type') == 'article') ? 'selected' : '' ?>>Artikel (Eq. Berita, Tips, Blog)</option>
                                <option value="static" <?php echo ($this->input->post('category_type') == 'static') ? 'selected' : '' ?>>Static (Eq. Tentang, Profil, Company)</option>
                                <option value="product" <?php echo ($this->input->post('category_type') == 'product') ? 'selected' : '' ?>>Produk</option>
                                <option value="gallery" <?php echo ($this->input->post('category_type') == 'gallery') ? 'selected' : '' ?>>Gallery Photo</option>
                                <option value="archives_tag" <?php echo ($this->input->post('category_type') == 'archives_tag') ? 'selected' : '' ?>>Archives Tag</option>
                                <option value="event" <?php echo ($this->input->post('category_type') == 'event') ? 'selected' : '' ?>>Event</option>
                                <option value="widget" <?php echo ($this->input->post('category_type') == 'widget') ? 'selected' : '' ?>>Widget</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Judul <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="category_title" value="<?php echo $this->input->post('category_title'); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Isi <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-10">
                            <?php echo form_textarea_tinymce('category_content', $this->input->post('category_content'), 'Standard'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Meta Deskripsi
                        </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" placeholder="deskripsi singkat konten..." name="category_meta_description"><?php echo $this->input->post('category_meta_description'); ?></textarea>
                            <small class="text-muted"> Maks. 200 karakter dan memuat deskripsi singkat.</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Meta Tags
                        </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="category_meta_tags" placeholder="kata, kunci..."><?php echo $this->input->post('category_meta_tags'); ?></textarea>
                            <small class="text-muted"> Maks. 200 karakter dan memuat kata kunci yang pisahkan dengan koma (,).</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Gambar
                        </label>
                        <div class="col-sm-10">
                            <input class="form-control" type="file" name="category_photo" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <div class="alert alert-warning mb-0">
                                Form yang bertanda <span class="symbol required"></span> wajib diisi.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <input onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-sm btn-success">
                    <a href="<?php echo base_url() ?>admin/master_sub_master_satu/index" onclick="return confirm('Batalkan?')" class="btn btn-sm btn-warning">Batal</a>
                </div>
            </form>
        </div>
    </div>
</div>