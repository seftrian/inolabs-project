<div class="row">
    <div class="col-md-12">
          
          <div class="invoice">
                        <div class="row invoice-logo">
                            <div class="col-sm-6">
                              
                            </div>
                            <div class="col-sm-6">
                                <p>
                                    #<?php echo followup_lib::label_status($medical_header_status)?> / <?php echo convert_date($medical_header_timestamp,'','','ina');?> 
                                </p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>Pemesan:</h4>
                                <div class="well">
                                    <address>
                                        <strong><?php echo $person_full_name?></strong>
                                        <br />
                                        address -
                                        <br />
                                        Kota: - , Kode Pos: -
                                        <br>
                                        Hp.  <?php echo $person_phone?><br>
                                        Agama: <?php echo $person_religion?><br>
                                    </address>
                                    <address>
                                        <strong>E-mail</strong>
                                        <br>
                                        -
                                    </address>
                                </div>
                            </div>
                            
                               <div class="col-sm-8">
                                <h4>Ringkasan:</h4>
                                <div class="well">
                                <ul class="list-unstyled">
                                   <li>
                                        <strong>Kebutuhan:</strong> <?php echo isset($medical_header_qty)?$medical_header_qty.' '.followup_lib::label_unit($medical_header_unit):'-'?>
                                    </li>
                                    <li>
                                        <strong>Posisi:</strong> <?php echo isset($medical_header_position_title)?$medical_header_position_title:'-'?>
                                    </li>
                                    <li>
                                        <strong>Kondisi Pasien:</strong> <?php echo isset($medical_header_note_pasien)?$medical_header_note_pasien:'-'?>
                                    </li>
                                  
                                </ul>
                                </div>
                                <a href="" class="btn btn-info"><i class="fa fa-check-square"></i> Konfirmasi Pemesanan</a>
                            </div>
                           
                        </div>
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-12">

                             <?php
                            $getImagePath=$this->master_perawat_helper_lib->getImagePath();
                            $pathImgUrl=$getImagePath['pathUrl'];
                            $pathImgLoc=$getImagePath['pathLocation'];
                            $total_data=count($medical_item);
                            if(!empty($medical_item))
                            {
                              
                              $no=1;
                              foreach($medical_item AS $rowArr)
                              {
                                foreach($rowArr AS $variable=>$value)
                                {
                                  ${$variable}=$value;
                                }
                                if($medical_detail_order_by==1)
                                {

                                  $profil_verified_arr=$this->master_perawat_helper_lib->get_profil_perawat_verified(' AND medical_master_person_id='.intval($person_id));
                                  if(!empty($profil_verified_arr))
                                  {
                                    foreach($profil_verified_arr AS $variable_ver=>$value_ver)
                                    {
                                      ${$variable_ver}=$value_ver;
                                    }
                                    $icon_has_experienced=($medical_master_has_experienced=='Y')?'<i class="fa fa-heart-o"></i> Berpengalaman':'<i class="fa fa-graduation-cap"></i> Baru Lulus';
                                    $label_location_duty=$this->master_perawat_helper_lib->label_location_duty($person_id);
                                    $label_position=$this->master_perawat_helper_lib->label_position($person_id);
                                    $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
                                    $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
                                    $logo_file=$logo_name.'.'.$logo_ext;
                                    
                                    $imgProperty=array(
                                    'width'=>190,
                                    'height'=>190,
                                    'imageOriginal'=>$logo_file,
                                    'directoryOriginal'=>$pathImgLoc,
                                    'directorySave'=>$pathImgLoc.'190190/',
                                    'urlSave'=>$pathImgUrl.'190190/',
                                    );
                                    $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                                    $medical_master_experience=($medical_master_has_experienced=='Y')?$medical_master_experience:'';
                                    $text_about=strip_tags(html_entity_decode($medical_master_about.' '.$medical_master_experience.' '.$medical_master_skills));
                                    $text_about=(strlen($text_about)>206)?substr($text_about,0,206).'...':$text_about;
                                    $minimal_rate=function_lib::currency_rupiah($medical_master_rate_min).'/jam';

                                    ?>
                                      <b>Nakes Utama</b>
                                      <p>
                                            <table class="kd-table kd-tabletwo">
                                              <tbody>
                                                <tr>
                                                  <td style="width:30%;"><img src="<?php echo $img?>" alt="<?php echo $person_full_name?>"></td>
                                                  <td colspan="3">

                                                      <p  style="color:#000;text-align:justify;">
                                                      <b><?php echo $person_full_name?></b><br />
                                                      <span class="text-warning">
                                                      <?php echo $medical_master_education?> - Rp <?php echo $minimal_rate?><br>
                                                      <?php echo $text_about?>
                                                      </span>
                                                      </p>
                                                        <p class="kd-usernetwork text-warning"  style="color:#000;text-align:justify;"> 
                                                             <i class="fa fa-stethoscope"></i> <?php echo $label_position?><br />
                                                              </p>

                                                    </td>
                                                </tr>
                                              </tbody>
                                            </table>

                                      </p>
                                    <?php
                                  }
                                  
                                }
                                else
                                {
                                  if($no==2)
                                  {
                                    ?>

                                    <b>Nakes Alternatif</b>
                                    <p>
                                     <table class="kd-table kd-tabletwo">
                                      <tbody>
                                    <?php
                                  }
                                  ?>
                                      <?php
                                      if($no%2==0)
                                      {
                                        ?>
                                        <tr>
                                        <?php
                                      }

                                       $profil_verified_arr=$this->master_perawat_helper_lib->get_profil_perawat_verified(' AND medical_master_person_id='.intval($person_id));
                                  if(!empty($profil_verified_arr))
                                  {
                                    foreach($profil_verified_arr AS $variable_ver=>$value_ver)
                                    {
                                      ${$variable_ver}=$value_ver;
                                    }
                                    $icon_has_experienced=($medical_master_has_experienced=='Y')?'<i class="fa fa-heart-o"></i> Berpengalaman':'<i class="fa fa-graduation-cap"></i> Baru Lulus';
                                    $label_location_duty=$this->master_perawat_helper_lib->label_location_duty($person_id);
                                    $label_position=$this->master_perawat_helper_lib->label_position($person_id);
                                    $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
                                    $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
                                    $logo_file=$logo_name.'.'.$logo_ext;
                                    
                                    $imgProperty=array(
                                    'width'=>126,
                                    'height'=>94,
                                    'imageOriginal'=>$logo_file,
                                    'directoryOriginal'=>$pathImgLoc,
                                    'directorySave'=>$pathImgLoc.'255191/',
                                    'urlSave'=>$pathImgUrl.'255191/',
                                    );
                                    $img=$this->function_lib->resizeImageMoo($imgProperty);
                                    $medical_master_experience=($medical_master_has_experienced=='Y')?$medical_master_experience:'';
                                    $text_about=strip_tags(html_entity_decode($medical_master_about.' '.$medical_master_experience.' '.$medical_master_skills));
                                    $text_about=(strlen($text_about)>206)?substr($text_about,0,206).'...':$text_about;
                                    $minimal_rate=function_lib::currency_rupiah($medical_master_rate_min).'/jam';

                                    ?>
                                    <td style="width:25%;">
                                      <img src="<?php echo $img?>" alt="<?php echo $person_full_name?>"></td>
                                      <td>  <p  style="color:#000;text-align:justify;">
                                        <b><?php echo $person_full_name?></b><br />
                                          <span class="text-warning"><?php echo $medical_master_education?> <br /> Rp <?php echo $minimal_rate?></span><br>
                                      </p></td>
                                    <?php
                                  }
                                      ?>
                                      
                                      <?php
                                      if($no%2!=0 OR $no==$total_data)
                                      {
                                        ?>
                                        </tr>
                                        <?php
                                      }
                                      if($no==$total_data)
                                      {
                                        ?>
                                        </tbody>
                                      </table>
                                      </p>
                                        <?php
                                      }
                                }

                                $no++;
                              }
                            }
                            ?>
                            </div>
                        </div>
                    
                    </div>

    </div>
</div>



