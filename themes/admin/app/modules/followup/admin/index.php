<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
        <!-- start: RESPONSIVE TABLE PANEL -->
         <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
                </div>
            </div>

         <div class="panel-body">   
            <form role="form" action="" id="form-search" method="post" class="form-horizontal" enctype="multipart/form-data">
              <div class="col-sm-6">   
               
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Status Pesanan
                    </label>
                    <div class="col-sm-8">
                        <select id="status" class="form-control">
                            <option value="">Semua</option>
                            <option value="unread">Belum Dibaca</option>
                            <option value="read">Telah Dibaca</option>
                            <option value="confirmed">Telah Difollow-up</option>
                            <option value="rejected">Dibatalkan</option>
                        </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Tgl. Pesanan
                    </label>
                    <div class="col-sm-4">
                          <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" type="text" id="start_date" class="form-control date-picker" placeholder="Tgl. Mulai">
                    </div>
                     <div class="col-sm-4">
                          <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" type="text" id="end_date" class="form-control date-picker" placeholder="Tgl. Akhir">
                    </div>
                 </div>
                 
                </div>
              
                <div class="col-sm-12"> 
                    <div class="form-group">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-2">
                            <a href="#" onclick="clear_form('form-search');">Clear</a>
                            <button style="float:right;" class="btn btn-info" onclick="grid_reload();return false;" ><i class=" clip-search"></i> Cari</button>
                        </div>       
                    </div>  
                </div>
            </form>
        </div>
        </div>
        <div class="alert alert-success" style="display:none;"><span class="message">OK</span> <a href="#" onclick="$('.alert').hide();"><i class="btn btn-xs btn-danger">X</i> </a></div>

        <table id="gridview" style="display:none;"></table>
        <form target="_blank" id="form_export" method="post" action="export_data">
            <input id="export_start_date" type="hidden" name="export_start_date" value="" />
            <input id="export_end_date" type="hidden" name="export_end_date" value="" />
        </form>
        <!-- end: RESPONSIVE TABLE PANEL -->
    </div>
</div>
