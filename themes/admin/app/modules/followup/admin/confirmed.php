<?php
$person_address=function_lib::get_person_addres($medical_header_person_id);
$person_phone=function_lib::get_person_phone($medical_header_person_id);
$person_email=function_lib::get_person_email_addres($medical_header_person_id);
?>
<div class="row">
    <div class="col-md-12">
                        <hr />
          
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>Pemesan:</h4>
                                <div class="well">
                                    <address>
                                        <strong><?php echo $person_full_name?></strong>
                                        <br />
                                        <?php echo $person_address?>

                                        <br>
                                        Hp.  <?php echo $person_phone?><br>
                                        Agama: <?php echo $person_religion?><br>
                                    </address>
                                    <address>
                                        <strong>E-mail</strong>
                                        <br>
                                        <?php echo $person_email?>
                                    </address>
                                </div>
                            </div>
                            
                             <div class="col-sm-8">
                                <h4>Ringkasan:</h4>
                                <div class="well">
                                <ul class="list-unstyled">
                                   <li>
                                        <strong>Kebutuhan:</strong> <?php echo isset($medical_header_qty)?$medical_header_qty.' '.followup_lib::label_unit($medical_header_unit):'-'?>
                                    </li>
                                    <li>
                                        <strong>Posisi:</strong> <?php echo isset($medical_header_position_title)?$medical_header_position_title:'-'?>
                                    </li>
                                    <li>
                                        <strong>Kondisi Pasien:</strong> <?php echo isset($medical_header_note_pasien)?$medical_header_note_pasien:'-'?>
                                    </li>
                                  
                                </ul>
                                </div>
                            </div>
                           
                        </div>
                         <hr />
                        <div class="row">
                            <div class="col-sm-4" id="content_get_main_medical">
                                
                             </div>
                             <div class="col-sm-8">
                             <div class="alert" style="display:none;"></div>

                              <h4>&nbsp;</h4>
                              <form class="form-horizontal" id="form-confirmed">
                                <input type="hidden" id="header_id" name="header_id" value="<?php echo $medical_header_id?>">
                                <input type="hidden" id="position_id" name="position_id" value="<?php echo $medical_header_position_id?>">
                                <input type="hidden" id="person_id"  name="person_id" value="">
                                <input type="hidden" id="current_number" value="1">
                             <?php
                             if($medical_header_status=='confirmed')
                             {
                              $transaction_id=followup_lib::get_property_transaction($medical_header_id,'transaction_id');
                              $transaction_code=followup_lib::get_property_transaction($medical_header_id,'transaction_code');
                              $transaction_total_due=followup_lib::get_property_transaction($medical_header_id,'transaction_total_due');
                              ?>
                              <div class="well">
                              <span class="text-success">Transaksi ini telah dikonfirmasi oleh <?php echo ucfirst($medical_header_update_by).' pada tanggal '.convert_datetime($medical_header_last_update_datetime,'ina','')?>.</span>
                                <ul class="list-unstyled">
                                    <li>
                                        <strong>Kode Transaksi:</strong> <a href="<?php echo base_url()?>admin/ecom_transaction/view/<?php echo $transaction_id?>"><?php echo $transaction_code?></a>
                                    </li>
                                    <li>
                                        <strong>Total Biaya:</strong> Rp <?php echo function_lib::currency_rupiah($transaction_total_due)?>
                                    </li>
                                </ul>
                                </div>
                              <?php
                             }
                             else
                             {
                              ?>
                                 
                                    <div class="form-group">
                                      <label class="col-sm-2 control-label" for="form-field-1">
                                         Kebutuhan
                                      </label>
                                      <div class="col-sm-2">
                                        <input type="text" onkeyup="calculate_price();" placeholder="Jml" value="<?php echo $medical_header_qty?>" name="quantity" id="form-field-1" class="form-control currency_rupiah">
                                      </div>
                                      <div class="col-sm-2">
                                        <select class="form-control" onchange="get_rate_by_person_position_type();" name="unit" required>
                                           <option value="hour" <?php echo ($medical_header_unit=='hour')?'selected':''?>>Jam</option>
                                           <option value="day" <?php echo ($medical_header_unit=='day')?'selected':''?>>Hari</option>
                                           <option value="month" <?php echo ($medical_header_unit=='month')?'selected':''?>>Bulan</option>
                                        </select>
                                      </div>
                                      <div class="col-sm-6">
                                       <select name="position_id" class="form-control" onchange="get_rate_by_person_position_type();">
                                       </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-2 control-label" for="form-field-1">
                                        Biaya (Rp)
                                      </label>
                                      <div class="col-sm-4">
                                        <input type="text" placeholder="Biaya" name="unit_price" id="form-field-1" class="form-control currency_rupiah">
                                      </div>
                                    </div>
                                     <div class="form-group">
                                      <label class="col-sm-2 control-label" for="form-field-1">
                                        Diskon 
                                      </label>
                                      <div class="col-sm-4">
                                        <input type="text" onkeyup="discount_product($(this));" placeholder="(Rp)" name="discount_rupiah" id="form-field-1" class="form-control currency_rupiah">
                                      </div>
                                      <div class="col-sm-2">
                                        <input type="text" maxlength="5" onkeyup="discount_product($(this));" placeholder="(%)" name="discount_percent" id="form-field-1" class="form-control currency_rupiah">
                                      </div>
                                    </div>
                                     <div class="form-group">
                                      <label class="col-sm-2 control-label" for="form-field-1">
                                        Total (Rp)
                                      </label>
                                      <div class="col-sm-4">
                                        <input type="text" placeholder="" name="subtotal_price" id="form-field-1" class="form-control currency_rupiah" readonly>
                                      </div>
                                    </div>
                                    
                                    <h5>Termin Pembayaran <span class="action_btn"><a href="javascript:void(0)" onclick="add_field_termin();return false;" class="btn btn-xs btn-warning">[+] Termin</a>  </span>
                                      </h5>
                                    <div id="content_termin">  
                                      <div class="form-group">
                                        <div class="col-sm-3">
                                          <input type="hidden" name="termin_id[0]" value="0" placeholder="label" id="form-field-1" class="form-control">
                                          <input type="text" maxlength="100" value="Pembayaran" name="termin_label[0]" placeholder="label" id="form-field-1" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="termin_nominal[0]" placeholder="Nominal" id="form-field-1" class="form-control currency_rupiah">
                                        </div>

                                        <div class="col-sm-3">
                                          <input type="text" name="termin_start_date[0]" placeholder="Tgl. Mulai" id="form-field-1" class="form-control date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" >
                                        </div>

                                        <div class="col-sm-3">
                                          <input type="text" name="termin_end_date[0]" placeholder="Tgl. Akhir" id="form-field-1" class="form-control date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" >
                                        </div>
                                      </div>
                                    </div>
                             
                                    <div class="form-group action_btn">
                                      <div class="col-sm-2">
                                      </div>
                                      <div class="col-sm-3">
                                        <a href="<?php echo base_url()?>admin/followup/index">Batal</a>
                                        <button onclick="stored($(this));return false;" style="float:right;" class="btn btn-success btn-sm">Kirim</button>
                                      </div>
                                    </div>

                                 
                                  <p class="text-warning">
                                  Termin pembayaran diperlukan untuk memberikan informasi tagihan kepada pengguna jasa. 
                                  Jika pembayaran dapat dilakukan lebih dari 1x, maka dapat menekan tombol <a href="javascript:void(0);" onclick="add_field_termin();return false;" >[+]Termin</a>
                                  untuk menambah baris data.
                                  </p>
                              <?php
                             }
                             ?>
                             </form>
                            
                            </div>
                        </div>
                        <?php
                        if($medical_header_status!='confirmed')
                           {
                        ?>
                        <hr />
                        <div class="row">
                            <div class="col-sm-12">

                            <h4>Perawat Alternatif:</h4>
                            <div id="content_get_alternatif_medical">
                             
                            </div>
                            </div>
                        </div>
                        <hr />

                        <?php
                          }
                        ?>
                        
                        
                        
                    

    </div>
</div>



