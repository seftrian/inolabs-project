<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if (trim($message) != '') {

            echo $this->function_lib->show_message($message, $templateName, $themeId, $status);
        }
        ?>
    </div>
    <div class="col-sm-10">
        <div class="panel panel-default">
            <form role="form" action="" method="post" class="form-horizontal">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Judul Artikel <span class="symbol required" />
                        </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="category_title" value="<?php echo $this->input->post('category_title'); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Deskripsi <span class="symbol required" />
                        </label>
                        <div class="col-sm-10">
                            <?php echo form_textarea_tinymce('category_content', $this->input->post('category_content'), 'Standard', '100%', '200'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Gambar Artikel <span class="symbol required" />
                        </label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <input class="form-control" type="file" name="testimonial_avatar_src" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Status <span class="symbol required" />
                        </label>
                        <div class="col-sm-3">
                            <select name="admin_is_active" class="form-control">
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <input type="submit" name="save" value="Simpan" class="btn btn-sm btn-success" />
                </div>
            </form>
        </div>
    </div>
</div>






<script type="text/javascript">
    $("input[name='admin_username']").bind('change', function() {
        var val = $(this).val().replace(/\s/g, "");
        $(this).val(val)
    });
</script>