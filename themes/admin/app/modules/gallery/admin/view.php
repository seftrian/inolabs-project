<div class="row">
    <div class="col-md-12">
                        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
    </div>
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover mb-0">
                        <tbody>
                            <tr>
                                <th>Nama Group</th>
                                <td><?php
                                $adminGroupName=$this->function_lib->get_one('admin_group_title','site_administrator_group','admin_group_id='.  intval($rowAdmin['admin_group_id']));
                                echo $adminGroupName;?></td>
                            </tr>
                            <tr>
                                <th>Username</th>
                                <td><?php echo $rowAdmin['admin_username'];?></td>
                            </tr>
                            <tr>
                                <th>Last Login</th>
                                <td><?php echo $rowAdmin['admin_last_login'];?></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td><span class="<?php echo ($rowAdmin['admin_is_active']==1)?'icon-ok':'clip-minus-circle';?>"></span></td>
                            </tr>
                             <!-- <tr>
                                <th>Action</th>
                                <td>
                                    <a href="<?php echo base_url();?>admin/admin_user/update/<?php echo $rowAdmin['admin_id']?>" class="btn btn-xs btn-default" title="Edit"><i class="clip-pencil-3"></i></a>
                                           
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                
                </div>
            </div>
            <div class="panel-footer">
                <a href="<?php echo base_url();?>admin/admin_user/update/<?php echo $rowAdmin['admin_id']?>" class="btn btn-sm btn-info" >Ubah</a>
            </div>
        </div>
    </div>
</div>
