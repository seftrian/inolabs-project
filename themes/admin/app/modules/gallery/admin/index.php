<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if (trim($message) != '') {
            echo $this->function_lib->show_message($message, $templateName, $themeId, $status);
        }
        ?>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <form id="form-horizontal" method="POST" action="">
                <div class="panel-heading panel-btn">
                    <button type="button" class="btn btn-sm btn-info" onclick="window.location.href='<?php echo base_url(); ?>admin/artikel/create';return false;">
                        <i class="fa fa-plus"></i>&nbspTambah Artikel
                    </button>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover mb-0">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">No.</th>
                                    <th class="text-center">Judul Artikel</th>
                                    <th class="text-center">Tanggal Posting</th>
                                    <th class="text-center">Status</th>
                                    <th width="15%" class="text-center"><i class="fa fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
        </div>
    </div>
    </form>
</div>

<script type="text/javascript">

</script>