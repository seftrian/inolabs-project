<div class="row">
    <div class="col-md-12">
    <div class="col-md-12">
    <div class="panel panel-default ">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
                </div>
            </div>
            <div class="panel-body">
            <div class="col-md-7"> 
            <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Nama
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="name"  style="width:100%;" />
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Tanggal Order
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="order_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" style="width:100%;" class="date-picker" />
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        No. Telp
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="phone"  style="width:100%;" />
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Email
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="email"  style="width:100%;" />
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Waktu
                    </label>
                    <div class="col-sm-8">
                        <select class="form-control" id="time" style="width: 100%;">
                            <option value="">Semua</option>
                            <?php 
                            if(!empty($request_time)){
                                foreach ($request_time as $rowArr) {
                                    foreach ($rowArr as $key => $value) {
                                        ${$key}=$value;
                                    }
                                    echo '<option value="'.$request_time_label.'">'.$request_time_label.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Status
                    </label>
                    <div class="col-sm-8">
                        <select class="form-control" id="status" style="width: 100%;">
                            <option value="">Semua</option>
                            <option value="read">Sudah Dibaca</option>
                            <option value="unread">Belum Dibaca</option>
                        </select>
                    </div>
                 </div>
                
                 <div class="form-group">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-3">
                    <button class="btn btn-warning" type="reset" ><i class=" clip-remove"></i> Hapus Form</button>
                    </div>  
                    <div class="col-sm-5" >
                    <button style="float:right;" class="btn btn-info" onclick="grid_reload();return false;"><i class=" clip-search"></i> Cari</button>
                    </div>       
                </div>
            </form>
            </div>
            <div class="col-md-5"> 
            </div> 
        </div>
    </div>
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>

    <button class="btn btn-info btn-sm" onclick="download_excel(); return false;"><i class="fa fa-download"></i> Download Excel</button>
                            <!-- start: RESPONSIVE TABLE PANEL -->
        
          <div class="alert" style="display:none;"></div>                                                                       
         <table id="gridview" style="display:none;"></table>

                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>

         <!-- flexigrid starts here -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
        <!-- flexigrid ends here -->

        <link rel="stylesheet" href="<?php echo $themeUrl;?>assets/plugins/datepicker/css/datepicker.css">
        <script src="<?php echo $themeUrl;?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>


        <link rel="stylesheet" href="<?php echo $themeUrl;?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
        <script src="<?php echo $themeUrl;?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>


<script type="text/javascript">
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'No', name: 'no', width: 50, sortable: false, align: 'right' },
                            { display: 'View', name: 'view', width: 50, sortable: false, align: 'center' },
                            { display: 'Nama', name: 'name', width: 230, sortable: false, align: 'left' },
                            { display: 'Email', name: 'email', width: 200, sortable: false, align: 'left' },
                            { display: 'Tanggal Lahir', name: 'birthday', width: 150, sortable: false, align: 'center' },
                            { display: 'Tanggal Janji', name: 'appointment', width: 150, sortable: false, align: 'center' },
                            { display: 'Waktu', name: 'time', width: 120, sortable: false, align: 'center' },
                            { display: 'Poli', name: 'poli', width: 180, sortable: false, align: 'left' },
                            { display: 'Telepon', name: 'phone', width: 150, sortable: false, align: 'center' },
                            { display: 'Dibaca Oleh', name: 'view_by', width: 150, sortable: false, align: 'center' },
                            { display: 'Tanggal Dibaca', name: 'last_view', width: 120, sortable: false, align: 'center' },
                    
                        ],
                      
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '200',
                        resizable: false,
                        singleSelect: false,
                        nowrap:false,
                    });

                    $(document).ready(function() {
                        grid_reload();
                    });

                    function grid_reload() {
                        var name = $('#name').val();
                        var order_date = $('#order_date').val();
                        var phone = $('#phone').val();
                        var email = $('#email').val();
                        var time = $('#time').val();
                        var status = $('#status').val();
                        var service_link = '?name='+name+'&order_date='+order_date+'&phone='+phone+'&time='+time+'&status='+status+'&email='+email;
                        $("#gridview").flexOptions({url:'<?php echo base_url(); ?>order_patient/service_rest/get_data'+service_link}).flexReload();
                    }

        $(function(){
             $('.date-picker').datepicker({
                 autoclose: true
              });
        });

    /**
    * download excel data grid
    */
    function download_excel(){
        var name = $('#name').val();
        var order_date = $('#order_date').val();
        var phone = $('#phone').val();
        var email = $('#email').val();
        var time = $('#time').val();
        var status = $('#status').val();
        var service_link = '?name='+name+'&order_date='+order_date+'&phone='+phone+'&time='+time+'&status='+status+'&email='+email;
        window.location.href="<?php echo base_url(); ?>admin/order_patient/export_order_patient"+service_link;
        return false;
    }

</script>