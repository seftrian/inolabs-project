<div class="row">
    <div class="col-md-12">
    <div class="panel panel-default ">
         
            <div class="panel-body">
            
            <div class="col-md-5"> 
            <table class="table table-hover">
            <tbody>
                <tr>
                    <td style="width:100px;">Tanggal</td>
                    <td>: <?php echo convert_date($order_timestamp,'','','')?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Nama</td>
                    <td>: <?php echo $order_full_name?></td>
                </tr>
                 <tr>
                    <td style="width:100px;">Email</td>
                    <td>: <?php echo $order_email?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Tgl. Lahir</td>
                    <td>: <?php echo convert_date($order_birth_date,'','','')?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Tgl. Janji</td>
                    <td>: <?php echo convert_date($order_appointment_date,'','','')?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Waktu</td>
                    <td>: <?php echo $order_request_time?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Poli</td>
                    <td>: <?php echo $order_service_name?></td>
                </tr>
                <tr>
                    <td style="width:100px;">No Telp</td>
                    <td>: <?php echo $order_phone_number?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Dibaca Oleh</td>
                    <td>: <?php echo (trim($order_view_by)!='')?$order_view_by:'-';?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Tanggal Dibaca</td>
                    <td>: <?php echo (trim($order_view_datetime)!='')?$order_view_datetime:'-';?></td>
                </tr>
                <tr>
                    <td style="width:100px;">Status</td>
                    <td>: <?php echo (trim($order_status)=="read")?'Telah Dibaca':'Belum Dibaca';?></td>
                </tr>
            </tbody>
            </table>
           
            </div> 
            <div class="col-md-7"> 
            <button class="btn btn-danger btn-sm" onclick="print_order_patient('<?php echo $url_print; ?>'); return false;"><i class="fa fa-print"></i> Cetak Pemesanan</button>
            </div>
        </div>
        </div> 
    </div>
</div>

<script type="text/javascript">
        function print_order_patient(url_print) {
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.4;
                var wHeight= $(window).height();
                var dHeight= wHeight * 0.8;
                var x = screen.width/2 - dWidth/2;
                var y = screen.height/2 - dHeight/2;
                window.open(url_print, 'Cetak', 'width='+dWidth+', height='+dHeight+', left='+x+',top='+y);
        }
</script>