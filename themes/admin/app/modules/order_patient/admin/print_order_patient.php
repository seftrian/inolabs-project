<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Cetak Pemesanan</title>
   

    <link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <script type="text/javascript" src="<?php echo base_url()?>addons/jquery/jquery.min.js"></script>
    <script type="text/javascript">

    <?php
    if($printer_mode=='popup')
    {
    ?>
    $(function(){
    cetak();
    });
    <?php
    }
    ?>

    function cetak() {          
        window.print();
        setTimeout(function(){ window.close();},300);
    }
    </script>

</head>
<body>
  <div class="container outer-section">
    <div class="row">
        <div class="col-md-5">
        <div class="panel panel-default ">
             
                <div class="panel-body">
                <h3>Detail Pemesanan</h3>
                <table class="table table-hover">
                <tbody>
                    <tr>
                        <td style="width:100px;">Tanggal</td>
                        <td>: <?php echo convert_date($order_timestamp,'','','')?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Nama</td>
                        <td>: <?php echo $order_full_name?></td>
                    </tr>
                     <tr>
                        <td style="width:100px;">Email</td>
                        <td>: <?php echo $order_email?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Tgl. Lahir</td>
                        <td>: <?php echo convert_date($order_birth_date,'','','')?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Tgl. Janji</td>
                        <td>: <?php echo convert_date($order_appointment_date,'','','')?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Waktu</td>
                        <td>: <?php echo $order_request_time?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Poli</td>
                        <td>: <?php echo $order_service_name?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">No Telp</td>
                        <td>: <?php echo $order_phone_number?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Dibaca Oleh</td>
                        <td>: <?php echo (trim($order_view_by)!='')?$order_view_by:'-';?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Tanggal Dibaca</td>
                        <td>: <?php echo (trim($order_view_datetime)!='')?$order_view_datetime:'-';?></td>
                    </tr>
                    <tr>
                        <td style="width:100px;">Status</td>
                        <td>: <?php echo (trim($order_status)=="read")?'Telah Dibaca':'Belum Dibaca';?></td>
                    </tr>
                </tbody>
                </table>
            </div>
            </div> 
        </div>
    </div>
  </div>
</body>
</html>