<div class="row">
    <div class="col-md-12">
                        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
    </div>
    <div class="col-sm-8 col-sm-offset-2">
                            
        <div class="panel panel-default">
            
            <form role="form" action="" method="post" class="form-horizontal">
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Nama Grup <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-4">
                            <select name="admin_group_id"  class="form-control">
                                    <option value="">== Pilih Group ==</option>
                                <?php
                                if(!empty($groupAdminArr))
                                {
                                    foreach($groupAdminArr AS $key=>$rowGroup)
                                    {
                                        $groupId=$rowGroup['admin_group_id'];
                                        $groupName=$rowGroup['admin_group_title'];
                                        $selected=(isset($_POST['admin_group_id']) AND $_POST['admin_group_id']==$groupId)?'selected':'';
                                        ?>
                                        <option value="<?php echo $groupId;?>" <?php echo $selected;?>><?php echo $groupName;?></option>
                                        <?php
                                    }
                                }
                                ?>
                                </select>
                        </div>
                    </div>
                    <div class="form-group" style="display: none">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Unit/Cabang <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <select name="administrator_store_store_id"  class="form-control">
                                    <option value="">== Pilih Unit/Cabang ==</option>
                                <?php
                                if(!empty($storeArr))
                                {
                                    foreach($storeArr AS $rowArr)
                                    {
                                        foreach($rowArr AS $variable=>$value)
                                        {
                                            ${$variable}=$value;
                                        }
                                        $selected=($this->input->post('administrator_store_store_id')==$store_id)?'selected':'';
                                        
                                        ?>
                                        <option value="<?php echo $store_id;?>" <?php echo $selected;?>><?php echo $store_name;?></option>
                                        <?php
                                    }
                                }
                                ?>
                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                             Username <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="admin_username" value="<?php echo set_value('admin_username'); ?>"  class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group" style="display: none">
                        <label class="col-sm-3 control-label" for="form-field-1">
                         Nama Pengguna
                        </label>
                        <div class="col-sm-9">
                            <select name="administrator_person_id"  class="form-control">
                                    <option value="">== Pilih Karyawan ==</option>
                                <?php
                                if(!empty($personArr))
                                {
                                    foreach($personArr AS $rowArr)
                                    {
                                        foreach($rowArr AS $variable=>$value)
                                        {
                                            ${$variable}=$value;
                                        }
                                        $selected=($this->input->post('administrator_person_id')==$person_id)?'selected':'';
                                        
                                        ?>
                                        <option value="<?php echo $person_id;?>" <?php echo $selected;?>><?php echo $person_full_name;?></option>
                                        <?php
                                    }
                                }
                                ?>
                                </select>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Password <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="password" name="admin_password" value="<?php echo set_value('admin_password'); ?>" class="form-control"/>
                            <small class="text-muted">Minimal 6 karakter</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Ulangi Password  <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                               <input type="password" name="admin_password_repeat" value="<?php echo set_value('admin_password_repeat'); ?>" class="form-control"/>
                          </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Status
                        </label>
                        <div class="col-sm-4">
                            <select name="admin_is_active" class="form-control">
                            <option value="1" <?php echo (isset($_POST['admin_is_active']) AND $_POST['admin_is_active']=='1')?'selected':'';?>>Aktif</option>
                            <option value="0" <?php echo (isset($_POST['admin_is_active']) AND $_POST['admin_is_active']=='0')?'selected':'';?>>Draft</option>
                            </select>
                            
                        </div>
                    </div>
                    

                </div>
                <div class="panel-footer">
                    <input type="submit" name="save" value="Simpan" class="btn btn-sm btn-success" />
                </div>
            </form>
        </div>
        </div>
        </div>






<script type="text/javascript">
$("input[name='admin_username']").bind('change',function(){
    var val = $(this).val().replace(/\s/g, "");
    $(this).val(val)
});
</script>