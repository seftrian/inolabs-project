<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <form id="form-horizontal" method="POST" action="">
            <div class="panel-heading panel-btn">
                <button type="button" class="btn btn-sm btn-info" onclick="window.location.href='<?php echo base_url();?>admin/admin_user/create';return false;">
                    Tambah User
                </button>
                <input  type="submit" class="btn btn-sm btn-success" value="Aktifkan" name="publish_selected_item" disabled/>
                <input  type="submit" class="btn btn-sm btn-light" value="Non Aktifkan" name="unpublish_selected_item" disabled/>
                <input type="submit" class="btn btn-sm btn-danger" value="Hapus" name="delete_selected_item" onclick="return confirm('Hapus?');" disabled/>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover mb-0">
                        <thead>
                            <tr>
                                <th class="checkbox-column text-center"><label><input type="checkbox" id="checkAll"><span class="lbl"></span></label></th>

                                <th width="5%" class="text-center">No.</th>
                                <th class="text-center">Username</th>
                                <th class="text-center">Terakhir Login</th>
                                <th class="text-center">Status</th>
                                <th width="15%" class="text-center">#</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if(!empty($dataGroup))
                            {
                                $no=1;
                                foreach($dataGroup AS $val)
                                {
                                    $adminId=$val['admin_id'];
                                    $adminGroupId=$val['admin_group_id'];
                                    $adminGroupName=$this->function_lib->get_one('admin_group_title','site_administrator_group','admin_group_id='.  intval($adminGroupId));
                                    $adminUsername=$val['admin_username'];
                                    $adminLastLogin=$val['admin_last_login'];
                                    $adminIsActive=$val['admin_is_active'];
                                    $linkGroup=base_url().'admin/admin_group/view/'.$adminGroupId;
                                    ?>
                                    <tr>
                                        <td class="checkbox-column text-center"><label><input type="checkbox" name="admin_id[]" value="<?php echo $adminId;?>"><span class="lbl"></span></label></td>

                                        <td><?php echo $no;?></td>
                                        <td><?php echo $adminUsername;?> /<span style="font-size:10px;font-weight: bold;"><a href="<?php echo $linkGroup?>"><?php echo $adminGroupName;?></a></span></td>
                                        <td class="text-center"><?php echo $adminLastLogin;?></td>
                                        <td width="10%" class="text-center"><span class="<?php echo ($adminIsActive==1)?'icon-ok':'clip-minus-circle';?>"></span></span></td>
                                        <td class="da-icon-column text-center">
                                            <a href="<?php echo base_url();?>admin/admin_user/view/<?php echo $adminId;?>" class="btn btn-xs btn-default" title="Detail"><i class="clip-zoom-in"></i></a>
                                            <a href="<?php echo base_url();?>admin/admin_user/update/<?php echo $adminId;?>" class="btn btn-xs btn-default" title="Edit"><i class="clip-pencil-3" ></i></a>
                                            <a href="<?php echo base_url();?>admin/admin_user/delete/<?php echo $adminId;?>" onclick="return confirm('Hapus?');" class="btn btn-xs btn-default" title="Hapus"><i class="clip-remove"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                            }
                            ?>


                        </tbody>
                            </table>
                                                                                
                                    </div>
                                </div>
                    </div>
                </div>
            </form>
        </div>




<script type="text/javascript">
$('table th input:checkbox').on('click' , function(){
var that = this;
valueIsChecked=false;
$(this).closest('table').find('tr > td:first-child input:checkbox')
.each(function(){
this.checked = that.checked;
$(this).closest('tr').toggleClass('selected');

});


});
//untuk disable or enable button submit
$("input:checkbox").click(function(event){
valueIsChecked=false;
$("input:checkbox").each(function(){
if ($(this).prop('checked')==true){ 
valueIsChecked=true;
}

});

if(valueIsChecked)
{
$("input[type='submit']").attr('disabled',false);
}
else
{
$("input[type='submit']").attr('disabled',true);
}

if(!$('#checkAll').prop('checked') && this.id=='checkAll')
{
$("input[type='submit']").attr('disabled',true);
}


});
</script>