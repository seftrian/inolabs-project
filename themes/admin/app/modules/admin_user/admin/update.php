<div class="row">
    <div class="col-md-12">
                        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
    </div>
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <form role="form" action="" method="post" class="form-horizontal">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                       Nama Grup <span class="symbol required"></span> 
                    </label>
                    <div class="col-sm-4">
                    <?php if($groupTypeLogin!='superuser'){ ?> 
                            <?php
                            if(!empty($groupAdminArr))
                            {
                                foreach($groupAdminArr AS $key=>$rowGroup)
                                {
                                    $groupId=$rowGroup['admin_group_id'];
                                    $groupName=$rowGroup['admin_group_title'];
                                    $selected=(isset($_POST['admin_group_id']) AND $_POST['admin_group_id']==$groupId)?$groupName:(
                                            ($rowAdmin['admin_group_id']==$groupId)?$groupName:''
                                            );
                                    if($selected!=''){
                                    ?>
                                        <input type="text" value="<?php echo $selected ?>"  class="form-control" readonly/>
                                    <?php
                                    }
                                }
                            }
                            ?>
                            
                    <?php } else { ?>
                    <select name="admin_group_id" class="form-control">
                                <option value="">== Pilih Group ==</option>
                            <?php
                            if(!empty($groupAdminArr))
                            {
                                foreach($groupAdminArr AS $key=>$rowGroup)
                                {
                                    $groupId=$rowGroup['admin_group_id'];
                                    $groupName=$rowGroup['admin_group_title'];
                                    $selected=(isset($_POST['admin_group_id']) AND $_POST['admin_group_id']==$groupId)?'selected':(
                                            ($rowAdmin['admin_group_id']==$groupId)?'selected':''
                                            );
                                    ?>
                                    <option value="<?php echo $groupId;?>" <?php echo $selected;?>><?php echo $groupName;?></option>
                                    <?php
                                }
                            }
                            ?>
                            </select>
                        <?php } ?>
                        
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <label class="col-sm-3 control-label" for="form-field-1">
                        Unit/Cabang <span class="symbol required"></span>
                    </label>
                    <div class="col-sm-9">
                    <?php if($groupTypeLogin!='superuser'){ ?> 
                        <?php
                            if(!empty($storeArr))
                            {
                                foreach($storeArr AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                    $selected=($this->input->post('administrator_store_store_id')==$store_id)?$store_name:(
                                        ($current_store_id==$store_id)?$store_name:''
                                        );
                                    
                                    if($selected!=''){
                                    ?>
                                        <input type="text" value="<?php echo $selected ?>"  class="form-control" readonly/>
                                    <?php
                                    }
                                }
                            }
                            ?>
                    <?php } else { ?>
                        <select name="administrator_store_store_id"  class="form-control">
                                <option value="">== Pilih Unit/Cabang ==</option>
                            <?php
                            if(!empty($storeArr))
                            {
                                foreach($storeArr AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                    $selected=($this->input->post('administrator_store_store_id')==$store_id)?'selected':(
                                        ($current_store_id==$store_id)?'selected':''
                                        );
                                    
                                    ?>
                                    <option value="<?php echo $store_id;?>" <?php echo $selected;?>><?php echo $store_name;?></option>
                                    <?php
                                }
                            }
                            ?>
                            </select>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                         Username <span class="symbol required"></span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="admin_username" value="<?php echo isset($_POST['admin_username'])?set_value('admin_username'):$rowAdmin['admin_username']; ?>"  class="form-control"/>
                    </div>
                </div>

                <div class="form-group" style="display: none">
                    <label class="col-sm-3 control-label" for="form-field-1">
                     Nama Pengguna
                    </label>
                    <div class="col-sm-9">
                    <?php if($groupTypeLogin!='superuser'){ ?> 
                        <?php
                            if(!empty($personArr))
                            {
                                foreach($personArr AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                    $selected=($this->input->post('administrator_person_id')==$person_id)?$person_full_name:(
                                        ($current_person_id==$person_id)?$person_full_name:''
                                        );
                                    
                                    if($selected!=''){
                                    ?>
                                        <input type="text" value="<?php echo $selected ?>"  class="form-control" readonly/>
                                    <?php
                                    }
                                }
                            }
                            ?>
                    <?php } else { ?>
                        <select name="administrator_person_id"  class="form-control" >
                                <option value="">== Pilih Karyawan ==</option>
                            <?php
                            if(!empty($personArr))
                            {
                                foreach($personArr AS $rowArr)
                                {
                                    foreach($rowArr AS $variable=>$value)
                                    {
                                        ${$variable}=$value;
                                    }
                                    $selected=($this->input->post('administrator_person_id')==$person_id)?'selected':(
                                        ($current_person_id==$person_id)?'selected':''
                                        );
                                    
                                    ?>
                                    <option value="<?php echo $person_id;?>" <?php echo $selected;?>><?php echo $person_full_name;?></option>
                                    <?php
                                }
                            }
                            ?>
                            </select>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Ganti Password
                        </label>

                        <div class="col-sm-9">
                            <label><input type="radio" id="skip" name="changePass" value="skip" <?php echo !isset($_POST['changePass'])?'checked':(
                                    (isset($_POST['changePass']) AND $_POST['changePass']=='skip')?'checked':''
                                    )?>/> <span class="lbl"><i class="icon-lock" title="Skip"></i></span></label>
                            <label><input type="radio" id="do" name="changePass" value="do" <?php echo  (isset($_POST['changePass']) AND $_POST['changePass']=='do')?'checked':'';?>/> <span class="lbl"><i title="Change" class="icon-unlock"></i></span></label>
                        
                        </div>
                        </div>
                    
                        <?php
                        //set invisible jika user yg diedit bukan superuser
                        $invisible=($groupTypeThisAction!='superuser' AND $groupTypeLogin=='superuser')?true:false;
                        ?>
                    <div class="form-group" id="changePassword">
                        <label class="col-sm-3 control-label" for="form-field-1"> Password Lama <span class="symbol required"></span></label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" name="admin_password_old" value="<?php echo isset($_POST['admin_password_old'])?$_POST['admin_password_old']:''; ?>" <?php echo $invisible?"disabled":"";?>/>
                        </div>
                        </div>
                    
                        <div class="form-group" id="changePassword">
                        <label class="col-sm-3 control-label" for="form-field-1"> Password Baru <span class="symbol required"></span> </label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" name="admin_password" value="<?php echo set_value('admin_password'); ?>" />
                            <small class="text-muted">Min 6 characters</small>
                        </div>
                        </div>
                    <div class="form-group" id="changePassword">
                        <label class="col-sm-3 control-label" for="form-field-1"><span class="symbol required"></span> Ulangi Password Baru </label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" name="admin_password_repeat" value="<?php echo set_value('admin_password_repeat'); ?>" />
                        </div>
                        </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                        Status
                    </label>
                    <div class="col-sm-4">
                    <?php if($groupTypeLogin!='superuser'){ ?> 
                        <?php 
                            $selected = (isset($_POST['admin_is_active']) AND $_POST['admin_is_active']==1)?'Aktif':(
                               ($rowAdmin['admin_is_active']=='1')?'Aktif':'Draft')?>
                            <input type="text" value="<?php echo $selected ?>"  class="form-control" readonly/>
                    <?php } else { ?>
                        <select name="admin_is_active" class="form-control">
                        <option value="1" <?php echo (isset($_POST['admin_is_active']) AND $_POST['admin_is_active']==1)?'checked':(
                               ($rowAdmin['admin_is_active']=='1')?'checked':''
                                                )?>>Aktif</option>
                        <option value="0" <?php echo (isset($_POST['admin_is_active']) AND $_POST['admin_is_active']==0)?'checked':(
                               ($rowAdmin['admin_is_active']=='0')?'checked':''
                                                )?>>Draft</option>
                        </select>
                    <?php } ?>
                        
                    </div>
                </div>
                </div>
                <div class="panel-footer">
                    <input type="submit" name="save" value="Simpan" class="btn btn-sm btn-success" />
                </div>
            </form>
        </div>
    </div>
</div>




<script type="text/javascript">
$("input[name='admin_username']").bind('change',function(){
    var val = $(this).val().replace(/\s/g, "");
    $(this).val(val)
});

$(document).ready(function(){
    
    if($('input:radio#do').prop('checked'))
    {
            $("div#changePassword").show();
    }
    else
    {
        $("div#changePassword").hide();
    }
});


$("input:radio").bind('click',function(){
     var idRadio=this.id;
     if(idRadio=='do')
     {
         $("div#changePassword").slideDown('medium');
         $("input[name='admin_password_old']").focus();
     }
     else if(idRadio=='skip')
     {
          $("div#changePassword").slideUp('medium');
          $("div#changePassword").find('input').val(''); //kosongkan form ketika batal input password
     }
});
</script>