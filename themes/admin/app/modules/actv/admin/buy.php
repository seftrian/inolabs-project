<div class="row">
        <div class="col-md-8">
                            <!-- start: RESPONSIVE TABLE PANEL -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                    </div>
                                </div>
                <div class="panel-body">
                                     <?php
                                        //untuk menampilkan pesan
                                        if(trim($message)!='')
                                        {
                                            
                                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                                        }
                                        ?>
                    <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
               
                                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                Masa Aktif 
                            </label>
                            <div class="col-sm-4">
                                <select name="activation_num_days" style="width:100%">
                                <option value="">Pilih Masa Aktif</option>
                                <?php
                                if(!empty($activation_type))
                                {
                                    foreach($activation_type AS $index=>$value)
                                    {   
                                        $selected=($this->input->post('activation_num_days')==$index)?'selected':'';
                                        ?>
                                        <option value="<?php echo $index?>" <?php echo $selected?>><?php echo $value?></option>
                                        <?php
                                    }
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                Jumlah 
                            </label>
                            <div class="col-sm-2">
                                <input type="text" name="activation_num_generate"  style="width:100%" value="<?php echo $this->input->post('activation_num_generate')?$this->input->post('activation_num_generate'):'10'?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                Nama Pembeli 
                            </label>
                            <div class="col-sm-4">
                                <input type="text" name="person_full_name"  style="width:100%" value="<?php echo $this->input->post('person_full_name')?$this->input->post('person_full_name'):''?>"  required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                Email Pembeli 
                            </label>
                            <div class="col-sm-4">
                                <input type="email" name="additional_info_email_address"  style="width:100%" value="<?php echo $this->input->post('additional_info_email_address')?$this->input->post('additional_info_email_address'):''?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                No. Hp. Pembeli 
                            </label>
                            <div class="col-sm-4">
                                <input type="text" name="person_phone"  style="width:100%" value="<?php echo $this->input->post('person_phone')?$this->input->post('person_phone'):''?>"  required>
                            </div>
                        </div>
                                       
                        <div class="form-actions">
                        <div class="col-sm-2">
                        <input type="hidden" name="save" value="1">
                        </div>
                        <div class="col-sm-3">
                        <a href="<?php echo base_url()?>admin/actv/index" onclick="return confirm('Batalkan?');">Cancel</a>
                        <input style="float:right;" onclick="return confirm('Kirim?');" type="submit" name="save" value="Kirim" class="btn btn-success" />
                        </div>     
                        </div>
                    </form>
                </div>
            </div>
            <!-- end: RESPONSIVE TABLE PANEL -->
        </div>
</div>