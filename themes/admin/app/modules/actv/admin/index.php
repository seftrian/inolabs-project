<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if (trim($message) != '') {
            echo $this->function_lib->show_message($message, $templateName, $themeId, $status);
        }
        ?>
        <!-- start: RESPONSIVE TABLE PANEL -->
        <div class="panel panel-default " style="display:none;">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
                </div>
            </div>
            <div class="panel-body"  style="display:none;">
            <div class="col-md-7"> 
            <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                      Masa Aktif
                    </label>
                    <div class="col-sm-9">
                        <div class="form-group">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <input style="float:left;" data-date-format="dd-mm-yyyy" id="from_pay" class="from" placeholder="Mulai" type="text" name="date_start" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <input  style="float:left;"  data-date-format="dd-mm-yyyy" id="to_pay" class="to" placeholder="Akhir" type="text" name="date_end"  />
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                 </div> 
                 <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                      Nomor Serial
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" />
                    </div>
                 </div>  
               
              
                 
                 <div class="form-group">
                    <label class="col-sm-3 control-label" for="form-field-1">
                        Status
                    </label>
                    <div class="col-sm-9">
                        <select id="status" class="form-control">
                        <option value="">Tampilkan Semua</option>
                        <option value="paid">Aktif</option>
                        <option value="not_paid">Pasif</option>
                        </select>
                    </div>
                 </div>   
                 <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-1">
                    </div>
                    <div class="col-sm-5">
                    <a href="#"><i class=" clip-remove"></i> Hapus Form</a>
                    
                    <button id="btn_search" class="btn btn-info" onclick="grid_reload($(this));return false;" ><i class=" clip-search"></i> Cari</button>
                    </div> 
                </div>
            </form>
            </div>

            
        </div>
       
        <!-- end: RESPONSIVE TABLE PANEL -->
    </div>
     <div class="alert alert-success" style="display:none;"><span class="message">OK</span> <a href="#" onclick="$('.alert').hide();"><i class="btn btn-xs btn-danger">X</i> </a></div>
     <p><button onclick="window.location.href='<?php echo base_url()?>admin/actv/buy';return false;" class="btn btn-sm btn-info"><i class="clip-cart"></i> Pembelian Aktivasi</button></p>   
        <table id="gridview" style="display:none;"></table>
        <form target="_blank" id="form_export" method="post" action="export_data">
            <input id="export_store_id" type="hidden" name="export_store_id" value="" />
            <input id="export_start_date" type="hidden" name="export_start_date" value="" />
            <input id="export_end_date" type="hidden" name="export_end_date" value="" />
            
            <input id="export_sortname" type="hidden" name="params[sortname]" value="" />
            <input id="export_sortorder" type="hidden" name="params[sortorder]" value="" />
            <input id="export_query" type="hidden" name="params[query]" value="" />
            <input id="export_optionused" type="hidden" name="params[optionused]" value="" />
            <input id="export_option" type="hidden" name="params[option]" value="" />
            <input id="export_date_start" type="hidden" name="params[date_start]" value="" />
            <input id="export_date_end" type="hidden" name="params[date_end]" value="" />
            <input id="export_qtype" type="hidden" name="params[qtype]" value="" />
        </form>
</div>