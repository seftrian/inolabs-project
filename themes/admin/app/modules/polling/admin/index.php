<div class="row">
    <div class="col-md-12">
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
          <hr />

         <div  class="row col-sm-12">
         <p style="text-align: right;">
         <a href="<?php echo base_url()?>admin/polling/user/<?php echo $category_id?>" class="btn btn-warning btn-sm"><i class="fa fa-user"></i> User Polling</a>
         <a href="<?php echo base_url()?>admin/polling/statistik/<?php echo $category_id?>" class="btn btn-warning btn-sm"><i class="fa fa-bar-chart-o"></i> Statistik Polling</a>
         </p>
         <div class="alert" style="display:none;"></div>
         <form method="post" id="polling" class="form-horizontal">
         <?php
         if(!empty($results))
         {
          $num=1;
          foreach($results AS $index_question=>$rowArr)
          {
            foreach($rowArr AS $variable=>$value)
            {
              ${$variable}=$value;
            }
            $answer_arr=polling_lib::get_answer_by_question_id($polling_question_id);
            ?>
             <div class="row polling_question">
                 <input type="hidden" name="polling_question_id[<?php echo $index_question?>]" value="<?php echo $polling_question_id?>">
                 <input type="hidden" class="polling_question_order_number" value="<?php echo $index_question?>">
                  <div class="form-group">
                    <div class="col-sm-1"><label class="col-sm-offset-11" id="label_number"> <?php echo $num?>. </label></div>
                    <div class="col-sm-9">
                      <textarea class="form-control" name="polling_question_name[<?php echo $index_question?>]" placeholder="Pertanyaan..."><?php echo $polling_question_name?></textarea>
                    </div>
                    <div class="col-sm-1">
                      <input type="text" name="polling_question_order_number[<?php echo $index_question?>]" value="<?php echo $polling_question_order_number?>" class="form-control input_polling_question_order_number">
                    </div>
                    <a onclick="remove_question($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                  </div>
                  <?php
                  if(!empty($answer_arr))
                  {
                    foreach($answer_arr AS $index_answer=>$rowAnswerArr)
                    {
                      foreach($rowAnswerArr AS $variableAnswer=>$valueAnswer)
                      {
                        ${$variableAnswer}=$valueAnswer;
                      }
                      ?>
                      <div class="form-group polling_answer">
                        <div class="col-sm-2">&nbsp;</div>
                        <div class="col-sm-8">
                          <input type="hidden" name="polling_answer_id[<?php echo $index_question?>][<?php echo $index_answer?>]" value="<?php echo $polling_answer_id?>">
                          <input type="text" value="<?php echo $polling_answer_name?>" class="form-control" name="polling_answer_name[<?php echo $index_question?>][<?php echo $index_answer?>]" placeholder="Jawaban..." />
                        </div>
                        <div class="col-sm-1">
                          <input type="hidden" value="<?php echo $polling_answer_order_number?>" name="polling_answer_order_number[<?php echo $index_question?>][<?php echo $index_answer?>]" class="form-control input_polling_answer_order_number">
                        </div>
                        <a onclick="remove_answer($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                      </div>

                      <?php
                    }
                  }
                  ?>
                                
                  <a onclick="render_answer($(this));return false;" style="float:right;margin-left:50px; margin-bottom: 50px;" class="btn btn-primary btn-xs btn-answer">Tambah Jawaban</a>

             </div>
            <?php
            $num++;
          } 

         }
         else
         {
          ?>
           <div class="row polling_question">
           <input type="hidden" name="polling_question_id[0]" value="0">
           <input type="hidden" class="polling_question_order_number" value="0">
                <div class="form-group">
                  <div class="col-sm-1"><label class="col-sm-offset-11" id="label_number"> 1. </label></div>
                  <div class="col-sm-9">
                    <textarea class="form-control" name="polling_question_name[0]" placeholder="Pertanyaan..."></textarea>
                  </div>
                  <div class="col-sm-1">
                    <input type="text" name="polling_question_order_number[0]" value="1" class="form-control input_polling_question_order_number">
                  </div>
                </div>
                <div class="form-group polling_answer">
                    <div class="col-sm-2">&nbsp;</div>
                    <div class="col-sm-8">
                      <input type="hidden" name="polling_answer_id[0][0]" value="0">
                      <input type="text" class="form-control" name="polling_answer_name[0][0]" placeholder="Jawaban..." />
                    </div>
                    <div class="col-sm-1">
                      <input type="hidden" value="1" name="polling_answer_order_number[0][0]" class="form-control input_polling_answer_order_number">
                    </div>

                </div>
            
                <a onclick="render_answer($(this));return false;" style="float:right;margin-left:50px; margin-bottom: 50px;" class="btn btn-primary btn-xs btn-answer">Tambah Jawaban</a>

           </div>
          <?php
         }
         ?> 
           

         </form>    
           <button style="float:left;" onclick="stored_database($(this),'<?php echo $category_id?>');return false;" class="btn btn-green">Simpan</button>
           <a onclick="render_question($(this));return false;" style="float:right;margin-bottom:20px;" class="btn btn-info btn-sm">Tambah Pertanyaan</a>
         </div>



                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>



