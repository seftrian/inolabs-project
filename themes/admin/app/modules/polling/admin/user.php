<div class="row">
    <div class="col-md-12">
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
          <hr />

         <div  class="row col-sm-12">
         <div class="alert" style="display:none;"></div>
          <table id="gridview" style="display:none;"></table>
            
            <form target="_blank" id="form_export" method="post" action="export_data">
                <input id="export_start_date" type="hidden" name="export_start_date" value="" />
                <input id="export_end_date" type="hidden" name="export_end_date" value="" />
            </form>
         </div>
                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>



  <!-- flexigrid starts here -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
<!-- flexigrid ends here -->
<script type="text/javascript">
    $("#gridview").flexigrid({
        dataType: 'json',
        colModel: [
            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
            { display: 'Nama Lengkap', name: 'full_name', width: 250, sortable: true, align: 'center' },
            { display: 'Email', name: 'email', width: 250, sortable: true, align: 'center' },
            { display: 'Telepon', name: 'phone', width: 150, sortable: true, align: 'center' },
            { display: 'Komentar', name: 'note', width: 300, sortable: true, align: 'center' },
            { display: 'Tgl. Input', name: 'timestamp', width: 150, sortable: true, align: 'center' },
    
        ],
        buttons: [
        
        ],
        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
        ],
      
        sortname: "id",
        sortorder: "asc",
        usepager: true,
        title: ' ',
        useRp: true,
        rp: 50,
        showTableToggleBtn: false,
        showToggleBtn: true,
        width: 'auto',
        height: '300',
        resizable: false,
        singleSelect: false
    });


    $(document).ready(function() {
         grid_reload();
    });

    function grid_reload() {
        var link_service='';
        $("#gridview").flexOptions({url:'<?php echo base_url(); ?>polling/service_rest/get_user/<?php echo $category_id?>'+link_service}).flexReload();
    }
    
</script>