
<div class="row">
    <div class="col-md-12">
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
          <hr />
          <div  class="row col-sm-12">
          <a onclick="javascript:window.print();" class="btn btn-sm btn-teal hidden-print">
                  Print <i class="fa fa-print"></i>
                </a>
          <table class="table ">
          <thead>
          <tr>
            <th style="width:33%"><i class="fa fa-user"></i> <?php echo isset($user_arr['full_name'])?$user_arr['full_name']:'-'?></th>
          
            <th style="width:33%"><i class="fa fa-envelope"></i> <?php echo isset($user_arr['email_address'])?$user_arr['email_address']:'-'?></th>
          
            <th style="width:33%"><i class="fa fa-phone"></i> <?php echo isset($user_arr['phone'])?$user_arr['phone']:'-'?></th>
          </tr>
          </thead>
          </table>

          </div>
         <div  class="row col-sm-12">
         <div class="alert" style="display:none;"></div>
         <form method="post" id="polling" class="form-horizontal">
         <?php
         if(!empty($results))
         {
          $num=1;
          foreach($results AS $index_question=>$rowArr)
          {
            foreach($rowArr AS $variable=>$value)
            {
              ${$variable}=$value;
            }
            $answer_arr=polling_lib::get_answer_by_question_id($polling_question_id);
            ?>
             <div class="row polling_question">
                  <div class="form-group">
                    <div class="col-sm-12"><label class="col-sm-offset-1" id="label_number"> <?php echo $num?>. </label>
                      <?php echo $polling_question_name?>
                    </div>
                   
                  </div>
                  <?php
                  if(!empty($answer_arr))
                  {
                    foreach($answer_arr AS $index_answer=>$rowAnswerArr)
                    {
                      foreach($rowAnswerArr AS $variableAnswer=>$valueAnswer)
                      {
                        ${$variableAnswer}=$valueAnswer;
                      }
                      $answer_user_is_true=polling_lib::get_answer_user($polling_answer_id,$user_id);
                      $class_answer=($answer_user_is_true==true)?'text-danger':'text-muted';
                      ?>
                      <div class="form-group polling_answer">
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3 <?php echo $class_answer;?>">
                          <?php echo ($answer_user_is_true==true)?'<strong><u>'.$polling_answer_name.'</u></strong>':$polling_answer_name?>
                        </div>
                     
                    
                      </div>

                      <?php
                    }
                  }
                  ?>
                                
             </div>
             <hr style="margin-top:5px;margin-bottom: 5px;" />
            <?php
            $num++;
          } 

         }
         else
         {
          ?>
           <div class="row polling_question">
            Belum ada pertanyaan untuk saat ini.
           </div>
          <?php
         }
         ?> 
           

         </form>    
         </div>



                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>



