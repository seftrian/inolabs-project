<div class="row">
    <div class="col-md-12">
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
          <hr />

         <div  class="row col-sm-12">
         <div class="alert" style="display:none;"></div>
         <form method="post" id="polling" class="form-horizontal">
         <?php
         if(!empty($results))
         {
          $num=1;
          
          $where_answer='';
          if($answer_id>0)
          {
            $where_answer=' AND polling_answer_id='.intval($answer_id);
          }
          foreach($results AS $index_question=>$rowArr)
          {
            foreach($rowArr AS $variable=>$value)
            {
              ${$variable}=$value;
            }
            
            $answer_arr=polling_lib::get_answer_by_question_id($polling_question_id,$where_answer);
            $link_statistik=polling_lib::link_to_user_by_question($category_id,$polling_question_id);
          

            ?>
             <div class="row polling_question">
                  <div class="form-group">
                    <div class="col-sm-1"><label class="col-sm-offset-11" id="label_number"> <?php echo $num?>. </label></div>
                    <div class="col-sm-11">
                      <a href="<?php echo $link_statistik?>"><?php echo $polling_question_name?></a>
                    </div>
                   
                  </div>
                  <?php
                  if(!empty($answer_arr))
                  {
                    foreach($answer_arr AS $index_answer=>$rowAnswerArr)
                    {
                      foreach($rowAnswerArr AS $variableAnswer=>$valueAnswer)
                      {
                        ${$variableAnswer}=$valueAnswer;
                      }
                      $link_answer=polling_lib::link_to_user_by_answer($category_id,$polling_question_id,$polling_answer_id);

                      $calculate_statistik_arr=polling_lib::calculate_statistik($polling_question_id,$polling_answer_id);
                      $percentage=number_format($calculate_statistik_arr['percentage'],1);  
                      $percentage_label=($percentage*1);  
                      $count_question=$calculate_statistik_arr['count_question'];  
                      $count_answer=$calculate_statistik_arr['count_answer'];  
                      $progress_style=$calculate_statistik_arr['color_style'];
                      ?>
                      <div class="form-group polling_answer">
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3 text-muted">
                          <a href="<?php echo $link_answer?>"><?php echo $polling_answer_name?></a>
                        </div>
                        <div class="col-sm-2 text-info">
                          <strong><?php echo ($count_answer>0)?$count_answer.'/'.$count_question:0?></strong>
                        </div>
                        <div class="col-sm-5">
                            <div class="progress" style="margin:0px;">
                                <div class="progress-bar <?php echo $progress_style?>" style="width: <?php echo $percentage?>%">
                                    <span class="sr-only"> <?php echo $percentage?> % </span>
                                 <?php echo ($percentage>0)?$percentage_label.'%':''?>    
                                </div>
                            </div>
                        </div>
                      </div>

                      <?php
                    }
                  }
                  ?>
                                
             </div>
             <hr style="margin-top:5px;margin-bottom: 5px;" />
            <?php
            $num++;
          } 

         }
         else
         {
          ?>
           <div class="row polling_question">
            Belum ada pertanyaan untuk saat ini.
           </div>
          <?php
         }
         ?> 
           

         </form>    

          <table id="gridview" style="display:none;"></table>
            
            <form target="_blank" id="form_export" method="post" action="export_data">
                <input id="export_start_date" type="hidden" name="export_start_date" value="" />
                <input id="export_end_date" type="hidden" name="export_end_date" value="" />
            </form>
         </div>



                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>



  <!-- flexigrid starts here -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
<!-- flexigrid ends here -->
<script type="text/javascript">
    $("#gridview").flexigrid({
        dataType: 'json',
        colModel: [
            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
            { display: 'Nama Lengkap', name: 'full_name', width: 250, sortable: true, align: 'center' },
            { display: 'Email', name: 'email', width: 250, sortable: true, align: 'center' },
            { display: 'Telepon', name: 'phone', width: 150, sortable: true, align: 'center' },
          //  { display: 'Komentar', name: 'note', width: 255, sortable: true, align: 'center' },
            { display: 'Tgl. Input', name: 'timestamp', width: 150, sortable: true, align: 'center' },
    
        ],
        buttons: [
        
        ],
        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
        ],
      
        sortname: "id",
        sortorder: "asc",
        usepager: true,
        title: ' ',
        useRp: true,
        rp: 50,
        showTableToggleBtn: false,
        showToggleBtn: true,
        width: 'auto',
        height: '300',
        resizable: false,
        singleSelect: false
    });


    $(document).ready(function() {
         grid_reload();
    });

    function grid_reload() {
        var link_service='';
        $("#gridview").flexOptions({url:'<?php echo base_url(); ?>polling/service_rest/get_statistik_user/<?php echo $question_id.'/'.$answer_id?>'+link_service}).flexReload();
    }
    
</script>