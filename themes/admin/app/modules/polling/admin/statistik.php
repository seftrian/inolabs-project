<div class="row">
    <div class="col-md-12">
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
          <hr />

         <div  class="row col-sm-12">
         <p class="text-warning"><u><a href="<?php echo base_url()?>admin/polling/user/<?php echo $category_id?>">Total Responden : <?php echo polling_lib::count_responden($category_id);?></a></u></p>
         <div class="alert" style="display:none;"></div>
         <form method="post" id="polling" class="form-horizontal">
         <?php
         if(!empty($results))
         {
          $num=1;
          foreach($results AS $index_question=>$rowArr)
          {
            foreach($rowArr AS $variable=>$value)
            {
              ${$variable}=$value;
            }
            $answer_arr=polling_lib::get_answer_by_question_id($polling_question_id);
            $link_statistik=polling_lib::link_to_user_by_question($category_id,$polling_question_id);
            ?>
             <div class="row polling_question">
                  <div class="form-group">
                    <div class="col-sm-1"><label class="col-sm-offset-11" id="label_number"> <?php echo $num?>. </label></div>
                    <div class="col-sm-11">
                      <a href="<?php echo $link_statistik?>"><?php echo $polling_question_name?></a>
                    </div>
                   
                  </div>
                  <?php
                  if(!empty($answer_arr))
                  {
                    foreach($answer_arr AS $index_answer=>$rowAnswerArr)
                    {
                      foreach($rowAnswerArr AS $variableAnswer=>$valueAnswer)
                      {
                        ${$variableAnswer}=$valueAnswer;
                      }
                      $link_answer=polling_lib::link_to_user_by_answer($category_id,$polling_question_id,$polling_answer_id);

                      $calculate_statistik_arr=polling_lib::calculate_statistik($polling_question_id,$polling_answer_id);
                      $percentage=number_format($calculate_statistik_arr['percentage'],1);  
                      $percentage_label=($percentage*1);  
                      $count_question=$calculate_statistik_arr['count_question'];  
                      $count_answer=$calculate_statistik_arr['count_answer'];  
                      $progress_style=$calculate_statistik_arr['color_style'];
                      ?>
                      <div class="form-group polling_answer">
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3 text-muted">
                          <a href="<?php echo $link_answer?>"><?php echo $polling_answer_name?></a>
                        </div>
                        <div class="col-sm-2 text-info">
                          <?php echo ($count_answer>0)?'<span class="badge badge-info">'.$count_answer.'</span>':''?>
                        </div>
                        <div class="col-sm-5">
                            <div class="progress" style="margin:0px;">
                                <div class="progress-bar <?php echo $progress_style?>" style="width: <?php echo $percentage?>%">
                                    <span class="sr-only"> <?php echo $percentage?> % </span>
                                 <?php echo ($percentage>0)?$percentage_label.'%':''?>    
                                </div>
                            </div>
                        </div>
                      </div>

                      <?php
                    }
                  }
                  ?>
                                
             </div>
             <hr style="margin-top:5px;margin-bottom: 5px;" />
            <?php
            $num++;
          } 

         }
         else
         {
          ?>
           <div class="row polling_question">
            Belum ada pertanyaan untuk saat ini.
           </div>
          <?php
         }
         ?> 
           

         </form>    
         </div>



                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>



