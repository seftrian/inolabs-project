<div class="row">
    <div class="col-md-12">
    <?php
    //untuk menampilkan pesan
    if(trim($message)!='')
    {
        
        echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
    }
    ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
        
          <div class="alert" style="display:none;"></div>                                                                       
         <table id="gridview" style="display:none;"></table>

                    <!-- end: RESPONSIVE TABLE PANEL -->
        </div>

         <!-- flexigrid starts here -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
        <!-- flexigrid ends here -->

<script type="text/javascript">
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'No', name: 'no', width: 50, sortable: false, align: 'right' },
                            { display: 'Nama Poli', name: 'schedule_kind_service_name', width: 200, sortable: false, align: 'center' },
                            { display: 'Nama Dokter', name: 'schedule_dokter_name', width: 180, sortable: false, align: 'center' },
                            { display: 'Jam', name: 'schedule_time', width: 150, sortable: false, align: 'center' },
                    
                        ],
                        buttons: [
                            //{ display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                        ],

                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '200',
                        resizable: false,
                        singleSelect: false,
                        nowrap:false,
                    });

                    $(document).ready(function() {
                        grid_reload();
                    });

                    function grid_reload() {
                        $("#gridview").flexOptions({url:'<?php echo base_url(); ?>admin_config/service_rest/get_data_schedule_dokter'}).flexReload();
                    }
                    function act_delete(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }
                        }
                        
                        var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                        if(conf==true){
                            var arr_id = [];
                            var i = 0;
                            $('.trSelected', grid).each(function() {
                                var id = $(this).attr('data-id');
                                arr_id.push(id);
                                i++;
                            });
                            
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo base_url().'admin_config/service_rest/'; ?>act_delete_schedule_dokter',
                                data: com + '=true&item=' + JSON.stringify(arr_id),
                                dataType: 'json',
                                success: function(response) {
                                    grid_reload();
                                    if(response['message'] != '') {
                                        alert(response['message']);
                                    }
                                },
                                error:function(){
                                    alert('an error has occurred, please try again');
                                    grid_reload();    
                                }
                            });
                        }

                    }

</script>