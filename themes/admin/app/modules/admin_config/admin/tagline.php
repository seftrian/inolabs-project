<div class="row">
                                        <div class="col-md-12">
                                        <?php
                                        //untuk menampilkan pesan
                                        if(trim($message)!='')
                                        {
                                            
                                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                                        }
                                        ?>
                            <!-- start: RESPONSIVE TABLE PANEL -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                                                        </div>
                                </div>

                                <div class="panel-body">
                                <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">

                                        <input style="display:none;" type="text" class="form-control" name="config_name" value="<?php echo $config_name;?>" />
                                        <textarea style="display:none;" class="form-control" name="config_description"><?php echo $config_description;?></textarea>
                                        <textarea style="display:none;" class="form-control" name="config_keywords"><?php echo $config_keywords;?></textarea>

                                        
                                         <div class="form-group">
                                            <label class="col-sm-2 control-label" for="form-field-1">
                                              Tagline
                                            </label>
                                            <div class="col-sm-9">
                                            <textarea class="form-control" name="config_tagline_value"><?php echo $this->input->post('config_tagline_value')?$this->input->post('config_tagline_value'):$config_tagline_value;?></textarea>
                                                   
                                             </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="form-field-1">
                                               Link 
                                            </label>
                                            <div class="col-sm-9">
                                                  <input class="form-control" type="text" name="config_tagline_link" value="<?php echo $this->input->post('config_tagline_link')?$this->input->post('config_tagline_link'):$config_tagline_link;?>" />
                                                <span class="text-muted">misal: http://google.com</span>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                        <div class="col-sm-9">
                                        <input type="submit" name="save" value="Simpan" class="btn btn-default" />
                                        </div>     
                                        </div>
                                    </form>

                                </div>
                    </div>
                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>
