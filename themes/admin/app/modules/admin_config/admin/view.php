<div class="row">
    <div class="col-md-12">
                        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
        <div class="panel panel-default">
                <div class="panel-heading panel-btn">
                    <a class="btn btn-info btn-sm" href="<?php echo base_url().'admin/admin_config/update';?>"><i class="icon-pencil"></i> Update</a>
                </div>
                <div class="panel-body">                                
                    <div class="table-responsive">
                      <h4 class="text-center"><b>Lokasi Anda</b></h4>
                        <div class="box-map">
                             <input id="searching-location" class="controls" type="hidden" placeholder="Pencarian wilayah">
                               <div id="map-canvas" >
                               </div> 
                        
                        </div>
                        <div class="form-group" style="display:none;">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Lat 
                            </label>
                            <div class="col-sm-4">
                            <input type="text" class="form-control" id="latitude" name="config_lat" value="<?php echo $this->input->post('config_lat')?$this->input->post('config_lat'):$config_lat?>" >
                            </div>
                            <label class="col-sm-1 control-label" for="form-field-1">
                                Lng 
                            </label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" id="longitude"  name="config_lng" value="<?php echo  $this->input->post('config_lng')?$this->input->post('config_lng'):$config_lng?>" >
                            </div>
                        </div>                      

                       <table id="table-information" class="table table-striped table-bordered table-hover mb-0">
                            <tbody>
                                <tr>
                                    <th>Email</th>
                                    <td><?php echo $config_email;?></td>
                                </tr>
                                <tr>
                                    <th>Nama </th>
                                    <td><?php echo $config_name;?></td>
                                </tr>
                                <tr>
                                    <th>Alamat </th>
                                    <td><?php echo $config_address;?></td>
                                </tr>
                                <tr>
                                    <th>Kota</th>
                                    <td><?php echo $config_city_name;?></td>
                                </tr>
                                <tr>
                                    <th>Provinsi </th>
                                    <td><?php echo $config_province_name;?></td>
                                </tr>
                                <tr>
                                    <th>Negara </th>
                                    <td><?php echo $config_country_name;?></td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td><?php echo $config_description;?></td>
                                </tr>
                                <tr>
                                    <th>Keywords</th>
                                    <td><?php echo $config_keywords;?></td>
                                </tr>
                                <tr>
                                    <th>Footer</th>
                                    <td><?php echo $config_footer;?></td>
                                </tr>
                                <tr>
                                    <th>Logo</th>
                                    <td><?php 
                                        if(file_exists(FCPATH.'/'.$config_logo) AND trim($config_logo)!='')
                                        {
                                            echo '<img src="'.base_url().$config_logo.'" width="100px" />';
                                        }?></td>
                                </tr>
                                <tr>
                                    <th>Favicon</th>
                                        <td><?php 
                                        if(file_exists(FCPATH.'/'.$config_favicon) AND trim($config_favicon)!='')
                                        {
                                            echo '<img src="'.base_url().$config_favicon.'" width="30px" />';
                                        }?></td>
                                </tr>
                                <tr>
                                    <th>Link Registrasi </th>
                                    <td><?php echo $config_link_registrasi;?></td>
                                </tr>
                                <tr>
                                    <th>Link Login </th>
                                    <td><?php echo $config_link_login;?></td>
                                </tr>
                                
                            </tbody>
                        </table>
                    
                    </div>
                </div>
        </div>
    </div>
</div>


