<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
        <div class="panel panel-default">
            <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="panel-body">
                            <h4 class="text-center"><b>Lokasi Anda</b></h4>
                            <div class="box-map">
                                 <input id="searching-location" class="controls form-control" type="text" placeholder="Pencarian wilayah">
                                   <div id="map-canvas" >
                                   </div> 
                            
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label" for="form-field-1">
                                    Lat 
                                </label>
                                <div class="col-sm-4">
                                <input type="text" class="form-control" id="latitude" name="config_lat" value="<?php echo $this->input->post('config_lat')?$this->input->post('config_lat'):$config_lat?>" >
                                </div>
                                <label class="col-sm-1 control-label" for="form-field-1">
                                    Lang 
                                </label>
                                <div class="col-sm-4">
                                  <input type="text" class="form-control" id="longitude"  name="config_lng" value="<?php echo  $this->input->post('config_lng')?$this->input->post('config_lng'):$config_lng?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                    Email Webmaster <span class="symbol required"></span> 
                                </label>
                                <div class="col-sm-9">
                                   <input type="text" class="form-control" name="config_email" value="<?php echo $config_email;?>" />
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                    Nama Aplikasi <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_name" value="<?php echo $config_name;?>" />
                                    </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                   No. Telp. / HP
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_telephone" value="<?php echo $config_telephone;?>" />
                                    </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                   Alamat
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_address" value="<?php echo $config_address;?>" />
                                    </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                   Kota
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_city_name" value="<?php echo $config_city_name;?>" />
                                    </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                   Provinsi
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_province_name" value="<?php echo $config_province_name;?>" />
                                    </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                   Negara
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_country_name" value="<?php echo $config_country_name;?>" />
                                    </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                    Meta Description <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                     <textarea class="form-control" name="config_description"><?php echo $config_description;?></textarea>
                                  </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                  Meta Tags  <span class="symbol required"></span>  
                                </label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="config_keywords"><?php echo $config_keywords;?></textarea>
                                    <small class="text-muted">Pisahkan dengan koma (,)</small>
                                       
                                 </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                    Footer <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                  <input class="form-control" type="text" name="config_footer" value="<?php echo htmlentities($config_footer);?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                    Logo <span class="symbol required"></span> 
                                </label>
                                <div class="col-sm-9">
                                 <?php 
                                        if(file_exists(FCPATH.'/'.$config_logo) AND trim($config_logo)!='')
                                        {
                                            echo '<img src="'.base_url().$config_logo.'" width="100px" />';
                                        }?>
                                        <br />
                                        <input type="file" name="config_logo"/>
                                        <small class="text-muted">File: gif,jpg,png <br /> max:500 KB</small>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                    Favicon <span class="symbol required"></span> 
                                </label>
                                <div class="col-sm-9">
                               <?php 
                                        if(file_exists(FCPATH.'/'.$config_favicon) AND trim($config_favicon)!='')
                                        {
                                            echo '<img src="'.base_url().$config_favicon.'" width="30px" />';
                                        }?>
                                        <br />
                                    <input type="file" name="config_favicon" />
                                    <small class="text-muted">File: gif,jpg,png <br /> max:500 KB</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                   Link Registrasi
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_link_registrasi" value="<?php echo $config_link_registrasi;?>" />
                                    </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                   Link Login
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="config_link_login" value="<?php echo $config_link_login;?>" />
                                    </div>
                            </div>
                            <div class="form-group">
                             
                                <div class="col-sm-9 col-sm-offset-2">
                                    <div class="alert alert-warning mb-0">
                                        Field dengan tanda <span class="text-danger">*</span> wajib diisi.
                                    </div>
                                    <!-- <a href="<?php echo base_url()?>admin/admin_config/view" onclick="return confirm('Batal?');" class="btn btn-default btn-squared">Batal</a>
                                    <input style="margin-left:20px;" type="submit" name="save" value="Simpan" class="btn btn-success btn-squared" /> -->
                                </div>     
                            </div>
                        

                    </div>
                    <div class="panel-footer">
                        <a href="<?php echo base_url()?>admin/admin_config/view" onclick="return confirm('Batal?');" class="btn btn-sm btn-danger ">Batal</a>
                        <input  type="submit" name="save" value="Simpan" class="btn btn-success btn-sm" />
                    </div>
            </form>
        </div>
    </div>
</div>
