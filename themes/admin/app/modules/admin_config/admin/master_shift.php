<div class="row">
                    <div class="col-md-12">
                                                            <?php
                                        //untuk menampilkan pesan
                                        if(trim($message)!='')
                                        {
                                            
                                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                                        }
                        ?>
                         <p>
                            <a class="btn btn-default" href="<?php echo base_url().'admin/admin_config/view';?>"><i class="icon-pencil"></i> Lihat Konfigurasi</a>
                        </p> 
                        <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">

                        <?php                
                        if(!empty($findAll))
                        {
                            $no=1;    
                            foreach($findAll AS $rowArr)
                            {
                                foreach($rowArr AS $variable=>$value)
                                {
                                    ${$variable}=$value;
                                }
                                ?>
                                <div class="col-sm-4">
                                 <!-- start: RESPONSIVE TABLE PANEL -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php echo ($shift_is_active=='Y')?'<span class="btn btn-xs btn-success"><i class="clip-checkmark-2"></i> Shift '.$no.'</span>':'<span class="btn btn-xs btn-danger"><i class="clip-cancel-circle-2"></i> Shift '.$no.'</span>'?> 
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="form-field-1">
                                                        <span class="symbol required"></span> Nama Shift
                                                    </label>
                                                    <div class="col-sm-12">
                                                       <input type="hidden" style="width:100%;" name="shift_id[<?php echo $no?>]" value="<?php echo $shift_id?>" />
                                                       <input type="text" style="width:100%;" name="shift_title[<?php echo $no?>]" value="<?php echo (isset($_POST['shift_title'][$no]))?$_POST['shift_title'][$no]:$shift_title?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="form-field-1">
                                                         Jam Masuk
                                                    </label>
                                                    <div class="col-sm-12">
                                                       <input type="text" style="width:100%;" name="shift_start_time[<?php echo $no?>]" value="<?php echo (isset($_POST['shift_start_time'][$no]))?$_POST['shift_start_time'][$no]:$shift_start_time?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="form-field-1">
                                                         Jam Pulang
                                                    </label>
                                                    <div class="col-sm-12">
                                                       <input type="text" style="width:100%;" name="shift_end_time[<?php echo $no?>]" value="<?php echo (isset($_POST['shift_end_time'][$no]))?$_POST['shift_end_time'][$no]:$shift_end_time?>" />
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="form-field-1">
                                                         Status
                                                    </label>
                                                    <div class="col-sm-12">
                                                       <select name="shift_is_active[<?php echo $no?>]"> 
                                                       <option value="Y" <?php echo (isset($_POST['shift_is_active'][$no]) AND $_POST['shift_is_active'][$no]=='Y')?'selected':(
                                                        ($shift_is_active=='Y')?'selected':''
                                                       )?>>Aktif</option>
                                                       <option value="N" <?php echo (isset($_POST['shift_is_active'][$no]) AND $_POST['shift_is_active'][$no]=='N')?'selected':(
                                                        ($shift_is_active=='N')?'selected':''
                                                       )?>>Non Aktif</option>
                                                       </select>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- end: RESPONSIVE TABLE PANEL -->
                                </div> 
                                <?php
                            $no++;    
                            }
                        }                                   
?>
                        <div class="form-group">  
                            <div class="col-sm-12">  
                            <input type="hidden" name="save" value="1"/>
                            <button class="btn btn-yellow btn-sm" onclick="return confirm('Simpan Konfigurasi?');"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                                       
                           
                </div>
        </div>
