 <div class="row">  
    <div class="col-sm-12">
           <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
    <div class="panel  panel-default mb-1">
        <div class="panel-body">
            <button class="btn btn-sm btn-success" onclick="window.location.href='<?php echo base_url().'admin/master_content_category/create'?>'"><i class="fa fa-plus"></i> Tambah Kategori</button>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover mb-1">
            <thead>
            <tr>
                <th style="text-align:center;width:3%;">No.</th>
                <th style="text-align:center;width:5%;">Ubah</th>
                <th style="display:none;text-align:center;width:5%;">Hapus</th>
                <th style="text-align:center;width:30%;">Judul</th>
                <th style="text-align:center;width:30%;">Deskripsi</th>
                <th style="text-align:center;width:10%;">Gambar</th>

            </tr>
            </thead>
            <tbody>
            <?php
            $no=1+$offset;
            if(!empty($findAll))
            {
                foreach($findAll AS $rowArr)
                {
                    foreach($rowArr AS $variable=>$value)
                    {
                        ${$variable}=$value;
                    }
                    $delete='<a onclick="return confirm(\'Hapus?\');" href="'.base_url().'admin/master_content_category/delete/'.$category_id.'" class="btn btn-xs btn-red"><span class="fa fa-trash-o"></span></a>';
                    $edit='<a href="'.base_url().'admin/master_content_category/update/'.$category_id.'" class="btn btn-xs btn-blue"><span class="fa fa-edit"></span></a>';
                    $logo_name= (trim($category_photo)=='')?'-': pathinfo($category_photo,PATHINFO_FILENAME);
                    $logo_ext=  pathinfo($category_photo,PATHINFO_EXTENSION);
                    $logo_file=$logo_name.'.'.$logo_ext;
                    $pathImgUrl=$pathImgArr['pathUrl'];
                    $pathImgLoc=$pathImgArr['pathLocation'];
                    $imgProperty=array(
                    'width'=>70,
                    'height'=>70,
                    'imageOriginal'=>$logo_file,
                    'directoryOriginal'=>$pathImgLoc,
                    'directorySave'=>$pathImgLoc.'7070/',
                    'urlSave'=>$pathImgUrl.'7070/',
                    );
                    $img=$this->function_lib->resizeImageMoo($imgProperty);
                    $category_content=strip_tags($category_content);    
                    $category_content=(strlen($category_content)>200)?substr($category_content, 200).'...':$category_content;    
                    $count_news=master_content_lib::count_article_on_category($category_id);
                               
                    ?>
                    <tr>
                    <td style="text-align:right;"><?php echo $no;?>.</td>
                    <td style="text-align:center;"><?php echo $edit?></td>
                    <td style="display:none;text-align:center;"><?php echo $delete?></td>
                    <td><?php echo $category_title?> <span class="badge badge-<?php echo ($count_news<1)?'danger':'default'?>"><?php echo $count_news?></span>
                        <br />
                        <span class="text-muted"><?php echo $category_type?></span>
                         <p style="float:right;" class="text-muted"><?php echo $category_meta_description?> <br />
                         <?php echo $category_meta_tags?>
                         </p>
                    </td>
                    <td ><?php echo $category_content?></td>
                    <td style="text-align:center;"><img src="<?php echo $img?>" /></td>

                    </tr>
                    <?php
                    $no++;
                }
            }
            else
            {
                ?>
                <tr>
                    <td colspan="7" style="text-align: center;"> == Data belum tersedia ==</td>
                </tr>  
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12 text-center">
        <?php echo $link_pagination;?>
    </div>
    </div>
</div>
                              
