<?php
        $path='assets/images/';
        $pathImgUrl=base_url().$path;
        $pathImgLoc=FCPATH.$path;
        $product_image_name= (trim($product_image)=='')?'-': pathinfo($product_image,PATHINFO_FILENAME);
                                                
        $product_image_name_ext=  pathinfo($product_image,PATHINFO_EXTENSION);
        $product_image_file=$product_image_name.'.'.$product_image_name_ext;
        $imgProperty=array(
        'width'=>100,
        'height'=>100,
        'imageOriginal'=>$product_image_file,
        'directoryOriginal'=>$pathImgLoc.'product/',
        'directorySave'=>$pathImgLoc.'product/100100/',
        'urlSave'=>$pathImgUrl.'product/100100/',


        );
        $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
?>   
<div class="col-sm-12">

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                         <?php
                        //untuk menampilkan pesan
                            if(trim($message)!='')
                            {
                                
                                echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                            }
                            ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Judul <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input class="form-control" type="text" name="product_name" value="<?php echo $this->input->post('product_name')?$this->input->post('product_name'):$product_name; ?>" required/>
            </div>
        </div>
           
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Harga (Rp)<span class="symbol required"/>
            </label>
            <div class="col-sm-4">
            <input type="text" onkeyup="update_harga_wilayah();" name="product_old_price" class="form-control currency_rupiah main_price" placeholder="Harga" value="<?php echo !isset($_POST['product_old_price'])?function_lib::currency_rupiah($product_old_price):$_POST['product_old_price']; ?>" required/>
            </div>
            <label class="col-sm-2 control-label" for="form-field-1">
                Harga Jual (Rp)
            </label>
            <div class="col-sm-4">
            <input type="text" name="product_current_price" class="form-control currency_rupiah main_price_sale" placeholder="Harga Jual" value="<?php echo !isset($_POST['product_current_price'])?function_lib::currency_rupiah($product_current_price):$_POST['product_current_price']; ?>" readonly/>
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Diskon (%)<span class="symbol required"/>
            </label>
            <div class="col-sm-2">
            <input type="text"  onkeyup="discount_product($(this));" name="product_discount_percent" class="form-control currency_rupiah" placeholder="Harga Sekarang" value="<?php echo !isset($_POST['product_discount_percent'])?function_lib::currency_rupiah($product_discount_percent):$_POST['product_discount_percent']; ?>" required/>
            </div>
            <label class="col-sm-2 control-label" for="form-field-1">
                Diskon (Rp)<span class="symbol required"/>
            </label>
            <div class="col-sm-4">
            <input type="text"  onkeyup="discount_product($(this));" name="product_discount_rupiah" class="form-control currency_rupiah" placeholder="Harga Sekarang" value="<?php echo !isset($_POST['product_discount_rupiah'])?function_lib::currency_rupiah($product_discount_rupiah):$_POST['product_discount_rupiah']; ?>" required/>
            </div>
        </div> 

         <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Deskripsi <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <?php echo form_textarea_tinymce('product_content', ($this->input->post('product_content')?$this->input->post('product_content'):html_entity_decode($product_content)), 'Standard'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Overview <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <textarea class="form-control input_transaction span6" rows="4" name="product_overview" placeholder="deskripsi singkat produk"><?php echo isset($_POST['product_overview'])?$_POST['product_overview']:$product_overview?></textarea>
            </div>
        </div>

         <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Gambar
            </label>
            <div class="col-sm-10">
                <table class="table table-striped table-bordered" id="more_image" style="width:50%;">
                    <thead>
                        <tr><th></th><th></th></tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="center">Gambar Utama</td><td class="center">
                             <?php echo '<img src="'.$img.'" title="'.$product_name.'"/>'; ?>
                            <input type="hidden" name="product_image_old" value="<?php echo $product_image;?>">
                            <input class="form-control" type="file" name="product_image"/>
                            </td>
                        </tr>
                         <?php
                        if(!empty($images_arr))
                        {
                            $no=1;
                            foreach($images_arr AS $rowArr)
                            {
                                $product_image=$rowArr['product_more_images_file'];
                                $product_image_name=  pathinfo($product_image,PATHINFO_FILENAME);
                                $product_image_name_ext=  pathinfo($product_image,PATHINFO_EXTENSION);
                                $product_image_file=$product_image_name.'.'.$product_image_name_ext;
                                $imgProperty=array(
                                'width'=>100,
                                'height'=>100,
                                'imageOriginal'=>$product_image_file,
                                'directoryOriginal'=>$pathImgLoc.'product/',
                                'directorySave'=>$pathImgLoc.'product/100100/',
                                'urlSave'=>$pathImgUrl.'product/100100/',


                                );
                                $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                                $link_delete=base_url().'admin/product/delete_more_images/'.$rowArr['product_more_images_id'].'/'.$product_id;
                               ?>
                                <tr id="tr_more_images_<?php echo $no;?>">
                                    <td class="center"><a href="#" class="btn btn-xs btn-danger delete" onclick="if(confirm('Hapus')){window.location.href='<?php echo $link_delete;?>'}return false;" id="more_images_<?php echo $no;?>"><i class="fa fa-trash-o"></i></a></td><td class="center">

                                        <?php echo '<img src="'.$img.'" title="'.$product_name.'"/>'; ?>
                                        <input type="hidden" name="product_more_images_file<?php echo $no;?>_id" value="<?php echo $rowArr['product_more_images_id'];?>">
                                        <input type="hidden" name="product_more_images_file<?php echo $no;?>_old" value="<?php echo $product_image;?>">
                                        <input class="form-control" type="file" name="product_more_images_file<?php echo $no;?>"/></td>
                                </tr>
                               <?php
                               $no++;
                            }
                        }
                        ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="3" style="text-align: left;">
                                <a href="#" onclick="add_row_table_more_images();return false;" class="btn btn-xs btn-yellow">Tambah Gambar (+)</a>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Meta Tags
            </label>
            <div class="col-sm-10">
            <textarea class="form-control input_transaction span6" rows="4" name="product_meta_tags" placeholder="kata kunci pisah dengan koma (,)"><?php echo isset($_POST['product_meta_tags'])?$_POST['product_meta_tags']:$product_meta_tags?></textarea>
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Meta Deskripsi
            </label>
            <div class="col-sm-10">
                <textarea class="form-control input_transaction span6" rows="4" name="product_meta_desc" placeholder="deskripsi singkat produk"><?php echo isset($_POST['product_meta_desc'])?$_POST['product_meta_desc']:$product_meta_desc?></textarea>
            </div>
        </div> 
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
            <a href="<?php echo base_url()?>admin/product" onclick="return confirm('Batal?');" style="margin-right:20px;">Cancel</a>
            <input onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>

