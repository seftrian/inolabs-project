<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
        <!-- start: RESPONSIVE TABLE PANEL -->
         <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
                </div>
            </div>

         <div class="panel-body">   
            <form role="form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
              <div class="col-sm-6">   
                 <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                        Nama Produk
                    </label>
                    <div class="col-sm-8">
                        <input type="text" id="product_name"  style="width:100%;" />
                    </div>
                 </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1">
                       Deskripsi
                    </label>
                    <div class="col-sm-8">
                        <textarea id="product_content" style="width:100%;"></textarea> 
                    </div>
                 </div>
                 
                </div>
                
                <div class="col-sm-12"> 
                    <div class="form-group">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-3">
                        <a href="#">Hapus Form</a>
                       
                        <button style="float:right;" class="btn btn-info" onclick="grid_reload();return false;" ><i class=" clip-search"></i> Cari</button>
                        </div>       
                    </div>  
                </div>
            </form>
        </div>
        </div>
        <div class="alert alert-success" style="display:none;"><span class="message">OK</span> <a href="#" onclick="$('.alert').hide();"><i class="btn btn-xs btn-danger">X</i> </a></div>

        <table id="gridview" style="display:none;"></table>
        <form target="_blank" id="form_export" method="post" action="export_data">
            <input id="export_start_date" type="hidden" name="export_start_date" value="" />
            <input id="export_end_date" type="hidden" name="export_end_date" value="" />
        </form>
        <!-- end: RESPONSIVE TABLE PANEL -->
    </div>
</div>
