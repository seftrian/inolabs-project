<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if (trim($message) != '') {

            echo $this->function_lib->show_message($message, $templateName, $themeId, $status);
        }
        ?>
    </div>
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <form role="form" action="" method="post" class="form-horizontal">
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Parent <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <select name="menu_administrator_par_id" class="form-control">
                                <option value="">== Pilih Parent ==</option>
                                <option value="0" <?php echo (isset($_POST['menu_administrator_par_id']) and $_POST['menu_administrator_par_id'] == 0) ? 'selected' : '' ?>>Menu Utama</option>
                                <?php
                                echo $recursiveDataMenu;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Nama <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="menu_administrator_title" value="<?php echo set_value('menu_administrator_title'); ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Lokasi <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-4">
                            <?php
                            //                                                    if(isset($_GET['location']) AND trim($_GET['location'])!='')
                            //                                                           {
                            //                                                               $indexLoc= array_search($_GET['location'],$locationArr);
                            //                                                        //array_search($_GET['location'], $locationArr);
                            //                                                               if(trim($indexLoc)!='')
                            //                                                               {
                            //                                                                   $locationArr=array();
                            //                                                                   
                            //                                                               }
                            //                                                               echo $indexLoc;
                            //                                                           }
                            ?>
                            <select class="form-control" name="menu_administrator_location">
                                <?php
                                if (!empty($locationArr)) {

                                    foreach ($locationArr as $value => $label) {
                                        $selected = (isset($_POST['menu_administrator_location']) and $_POST['menu_administrator_location'] == $value) ? 'selected' : (
                                            (isset($_GET['location']) and $_GET['location'] == $value) ? 'selected' : '');

                                        if (isset($_GET['location']) and $_GET['location'] == $value) {
                                ?>
                                            <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $label; ?></option>
                                        <?php
                                        } elseif (!$_GET['location']) {
                                        ?>
                                            <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $label; ?></option>
                                <?php
                                        }
                                    }
                                }
                                ?>
                            </select></div>
                    </div>
                    <div class="form-group" id="ruleAdmin">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Module <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="menu_administrator_module" value="<?php echo set_value('menu_administrator_module'); ?>" />
                            Isikan dengan <b>-</b> jika tidak memiliki module

                        </div>
                    </div>
                    <div class="form-group" id="ruleAdmin">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Controller <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="menu_administrator_controller" value="<?php echo set_value('menu_administrator_controller'); ?>" />
                            Isikan dengan <b>-</b> jika tidak memiliki controller
                        </div>
                    </div>
                    <div class="form-group" id="ruleAdmin">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            <span class="symbol required"></span> Method
                        </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="menu_administrator_method"><?php echo !isset($_POST['menu_administrator_method']) ? 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item' : set_value('menu_administrator_method'); ?></textarea>
                            <small class="text-muted">Isikan dengan <b>-</b> jika tidak memiliki method</small>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Deskripsi
                        </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="menu_administrator_description"><?php echo $this->input->post('menu_administrator_description'); ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Urutan
                        </label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" name="menu_administrator_order_by" value="<?php echo $this->input->post('menu_administrator_order_by'); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Link
                        </label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input id="link" class="form-control" type="text" name="menu_administrator_link" value="<?php echo $this->input->post('menu_administrator_link'); ?>" />
                                <span id="preview_link"></span>
                                <span class="input-group-btn">
                                    <a data-toggle="modal" id="add" onclick="add_modal();return false;" class="btn btn-sm btn-info btn-form" title="Ambil dari website ini"><i class="clip-search"></i></a>
                                </span>
                            </div>
                        </div>
                        <!-- <div class="col-sm-1"><a data-toggle="modal" id="add" onclick="add_modal();return false;" class="btn btn-xs btn-green" title="Ambil dari website ini"><i class="clip-search"></i></a></div> -->

                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Class CSS
                        </label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" name="menu_administrator_class_css" value="<?php echo isset($_POST['menu_administrator_class_css']) ? $_POST['menu_administrator_class_css'] : ''; ?>" />
                            <small class="text-muted">Icon</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Status
                        </label>
                        <div class="col-sm-4">

                            <select name="menu_administrator_is_active" class="form-control">
                                <option value="1" <?php echo (isset($_POST['menu_administrator_is_active']) and $_POST['menu_administrator_is_active'] == '1') ? 'selected' : ''; ?>>Aktif</option>
                                <option value="0" <?php echo (isset($_POST['menu_administrator_is_active']) and $_POST['menu_administrator_is_active'] == '0') ? 'selected' : ''; ?>>Draft</option>
                            </select>

                        </div>
                    </div>



                </div>
                <div class="panel-footer">
                    <input type="submit" name="save" value="Simpan" class="btn btn-sm btn-success" />
                </div>
            </form>
        </div>
    </div>
</div>

<div id="ajax-modal" data-width="70%" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-focus-on="input#item_name" style="display: none;"></div>







<script type="text/javascript">
    $(document).ready(function() {
        var valueLocation = $("select[name='menu_administrator_location']").val();
        if (valueLocation == 'admin') {
            $("div#ruleAdmin").show();
        } else {
            $("div#ruleAdmin").hide();
        }


        var valueParent = $("select[name='menu_administrator_par_id']").val();
        var valueParentService = (valueParent != '') ? valueParent : 0;
        $.get('<?php echo base_url(); ?>admin_menu/service_rest/my_number/' + valueParentService + '/0/0/<?php echo $location; ?>', function(myNumber) {
            $("input[name='menu_administrator_order_by']").val(myNumber);
        }, 'json');
    });

    //untuk mendapatkan nomor urut menu
    $("select[name='menu_administrator_par_id']").bind('change', function() {
        var valueParent = $(this).val();
        var valueParentService = (valueParent != '') ? valueParent : 0;

        $.get('<?php echo base_url(); ?>admin_menu/service_rest/my_number/' + valueParentService + '/0/0/<?php echo $location; ?>', function(myNumber) {
            $("input[name='menu_administrator_order_by']").val(myNumber);
        }, 'json');
    });

    //untuk pemilihan form
    $("select[name='menu_administrator_location']").bind('change', function() {
        var value = $(this).val();
        if (value == 'admin') {
            $("div#ruleAdmin").slideDown('medium');
        } else {
            $("div#ruleAdmin").slideUp('medium');
        }
    });

    $("input[name='menu_administrator_class_css']").change(function() {
        $(this).before().find('i').remove();

        $(this).before('<i class="' + $(this).val() + '"> ' + $(this).val() + '</i>');
        // alert('ss');
    });
</script>