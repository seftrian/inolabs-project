<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if (trim($message) != '') {

            echo $this->function_lib->show_message($message, $templateName, $themeId, $status);
        }
        ?>
        <form id="form-horizontal" method="POST" action="">
            <div class="panel panel-default">
                <div class="panel-heading panel-btn">
                    <a class="btn btn-sm btn-info" href="<?php echo base_url(); ?>admin/admin_menu/create?location=<?php echo $location; ?>"> Tambah Menu </a>
                    <input type="submit" class="btn btn-sm btn-danger" value="Hapus" name="delete_selected_item" onclick="return confirm('Hapus?');" disabled/>
                    <input type="submit" class="btn btn-sm btn-success" value="Aktifkan" name="publish_selected_item" disabled/>
                    <input type="submit" class="btn btn-sm btn-light" value="Non Aktifkan" name="unpublish_selected_item" disabled/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="dTable" class="table table-striped table-bordered table-hover mb-0">
                            <thead>
                                <tr>
                                    <th class="checkbox-column text-center"><input type="checkbox" id="checkAll"></th>

                                    <th class="text-center"></th>
                                    <th class="text-center">Parent</th>
                                    <th class="text-center">Menu</th>
                                    <th class="text-center">Deskripsi</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Aksi</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($dataMenu)) {
                                    $no = 1;
                                    foreach ($dataMenu AS $val) {
                                        $menuId = $val['menu_administrator_id'];
                                        $menuParId = $val['menu_administrator_par_id'];
                                        $menuTitle = $val['menu_administrator_title'];
                                        $menuDesc = $val['menu_administrator_description'];
                                        $menuLocation = $val['menu_administrator_location'];
                                        $menuIsActive = $val['menu_administrator_is_active'];
                                        $myNum = $val['menu_administrator_order_by'];

                                        $msg = base64_encode('Posisi berhasil diatur');
                                        $redirect = rawurlencode(base_url() . 'admin/admin_menu/index?location=' . $menuLocation . '&status=200&msg=' . $msg) . '#' . $menuId;
                                        $upDownMenu = admin_menu::upDownMenu($menuParId, $myNum, $menuId, $redirect, $menuLocation);
                                        ?>
                                        <tr id="<?php echo $menuId; ?>">
                                            <td class="checkbox-column text-center"><label><input  type="checkbox" name="id[]" value="<?php echo $menuId; ?>"><span class="lbl"></span></label></td>

                                            <td class="text-center"><?php echo $upDownMenu; ?></td>
                                            <td ><?php echo admin_menu::getParentName($menuParId); ?> </td>
                                            <td><a href="<?php echo base_url(); ?>admin/admin_menu/view/<?php echo $menuId; ?>"><?php echo $menuTitle; ?> /<span style="font-size:10px;font-weight: bold;"><?php echo $menuLocation; ?></span></a></td>
                                            <td><?php echo $menuDesc; ?></td>
                                            <td width="10%"  style="text-align:center;"><span class="<?php echo ($menuIsActive == 1) ? 'icon-ok' : 'clip-minus-circle'; ?>"></span></td>
                                            <td class="da-icon-column text-center">
                                                <a href="<?php echo base_url(); ?>admin/admin_menu/view/<?php echo $menuId; ?>" class="btn btn-xs btn-default" title="Detail" ><i class="clip-zoom-in"></i></a>
                                                <a href="<?php echo base_url(); ?>admin/admin_menu/update/<?php echo $menuId; ?>?location=<?php echo $menuLocation; ?>" class="btn btn-xs btn-default" title="Edit"><i class="clip-pencil"></i></a>
                                                <a href="<?php echo base_url(); ?>admin/admin_menu/delete/<?php echo $menuId; ?>?location=<?php echo $menuLocation ?>" onclick="return confirm('Hapus?');" class="btn btn-xs btn-default" title="Hapus"><i class="clip-remove"></i></a>
                                            </td>
                                        </tr>
        <?php
        //data recursive
        $adminMenu = new admin_menu;
        $initDataMenu = $adminMenu->initalizeRecursive();
        echo $adminMenu->buildMenu($menuId, $initDataMenu);

        $no++;
    }
}
?>


                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!--<script src="<?php echo $themeUrl; ?>plugins/datatables/jquery.dataTables.min.js"></script>-->

<script type="text/javascript" src="<?php echo $themeUrl; ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $themeUrl; ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>



<script type="text/javascript">
                                                        $(document).ready(function() {
                                                            if ($.fn.dataTable) {
//$('#dTable').dataTable();

//$("table#dTable").dataTable({sPaginationType: "full_numbers"});
                                                                $("table#dTable").dataTable({
                                                                    "aoColumns": [
                                                                        {"bSortable": false},
                                                                        null, null, null, null, null,
                                                                        {"bSortable": false}
                                                                    ]});
                                                            }
                                                        });
//untuk disable or enable button submit
                                                        $("input:checkbox").click(function(event) {
                                                            valueIsChecked = false;
                                                            $("input:checkbox").each(function() {
                                                                if ($(this).prop('checked') == true) {
                                                                    valueIsChecked = true;
                                                                }

                                                            });

                                                            if (valueIsChecked)
                                                            {
                                                                $("input[type='submit']").attr('disabled', false);
                                                            } else
                                                            {
                                                                $("input[type='submit']").attr('disabled', true);
                                                            }

                                                            if (!$('#checkAll').prop('checked') && this.id == 'checkAll')
                                                            {
                                                                $("input[type='submit']").attr('disabled', true);
                                                            }


                                                        });
</script>