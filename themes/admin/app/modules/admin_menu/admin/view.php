<div class="row">
    <div class="col-md-12">
                        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
    </div>
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
                   
                    <div class="panel-body">
                        <div class="table-responsive">
                           <table class="table table-striped table-bordered table-hover mb-0">
                                <tbody>
                                    <tr>
                                        <th>Parent</th>
                                        <td><?php echo admin_menu::getParentName($rowMenu['menu_administrator_par_id']);?></td>
                                    </tr>
                                    <tr>
                                        <th>Nama Menu</th>
                                        <td><?php echo $rowMenu['menu_administrator_title'];?></td>
                                    </tr>
                                    <tr>
                                        <th>Lokasi</th>
                                        <td><?php echo $rowMenu['menu_administrator_location'];?></td>
                                    </tr>
                                    <?php
                                    if($rowMenu['menu_administrator_module']=='admin')
                                    {
                                        ?>
                                    <tr>
                                        <th>Module</th>
                                        <td><?php echo $rowMenu['menu_administrator_module'];?></td>
                                    </tr>
                                    <tr>
                                        <th>Controller</th>
                                        <td><?php echo $rowMenu['menu_administrator_controller'];?></td>
                                    </tr>
                                    <tr>
                                        <th>Method</th>
                                        <td><?php echo $rowMenu['menu_administrator_method'];?></td>
                                    </tr>
                                        <?php
                                    }
                                    ?>
                                    
                                    <tr>
                                        <th>Deskripsi</th>
                                        <td><?php echo $rowMenu['menu_administrator_description'];?></td>
                                    </tr>
                                    <tr>
                                        <th>Urutan</th>
                                        <td><?php echo $rowMenu['menu_administrator_order_by'];?></td>
                                    </tr>
                                    <tr>
                                        <th>Link</th>
                                        <td><?php echo base_url().$rowMenu['menu_administrator_link'];?> <a href="<?php echo base_url().$rowMenu['menu_administrator_link'];?>" target="_blank"><span class="icon-external-link"></span></a></td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td><span class="<?php echo ($rowMenu['menu_administrator_is_active']=='1')?'icon-ok':'clip-minus-circle';?>"></span></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
        </div>
    </div>
</div>

