<?php
/*
$logo_name= (trim($category_photo)=='')?'-': pathinfo($category_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($category_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>70,
'height'=>70,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'7070/',
'urlSave'=>$pathImgUrl.'7070/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);
*/
?>
<div class="col-sm-12">

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Nama <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="slider_category_name" value="<?php echo $this->input->post('slider_category_name')?$this->input->post('slider_category_name'):$slider_category_name; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Lebar
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="slider_category_width" value="<?php echo $this->input->post('slider_category_width')?$this->input->post('slider_category_width'):$slider_category_width; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Tinggi
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="slider_category_height" value="<?php echo $this->input->post('slider_category_height')?$this->input->post('slider_category_height'):$slider_category_height; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Kompres
            </label>
            <div class="col-sm-10">
                <label><input type="radio" name="slider_category_is_compress" value="Y" <?php echo $this->input->post('slider_category_is_compress')?$this->input->post('slider_category_is_compress')=='Y'?'checked':'':$slider_category_is_compress=='Y'?'checked':''; ?>> Y </label>
                &nbsp;
                <label><input type="radio" name="slider_category_is_compress" value="N" <?php echo $this->input->post('slider_category_is_compress')?$this->input->post('slider_category_is_compress')=='N'?'checked':'':$slider_category_is_compress=='N'?'checked':''; ?>> N </label> 
            </div>
        </div>
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
            <input type="reset" value="Reset" class="btn btn-warning" style="float:left;">
        </div>
        <div class="col-sm-6">
            <input style="float: right;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
