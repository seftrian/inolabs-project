    <div class="col-sm-12">
           <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
           
    <div class="panel-body">
    <p><button class="btn btn-green" onclick="window.location.href='<?php echo base_url().'admin/image_slide_category/create'?>'">(+) Tambah Kategori</button></p>

    <table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="text-align:center;width:3%;">No.</th>
        <th style="text-align:center;width:5%;">Ubah</th>
        <th style="display:none;text-align:center;width:5%;">Hapus</th>
        <th style="text-align:center;width:30%;">Judul</th>
        <th style="text-align:center;width:15%;">Lebar</th>
        <th style="text-align:center;width:15%;">Tinggi</th>
        <th style="text-align:center;width:10%;">Kompres</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $no=1+$offset;
    if(!empty($findAll))
    {
        foreach($findAll AS $rowArr)
        {
            foreach($rowArr AS $variable=>$value)
            {
                ${$variable}=$value;
            }
            $delete='<a onclick="return confirm(\'Hapus?\');" href="'.base_url().'admin/image_slide_category/delete/'.$slider_category_id.'" class="btn btn-xs btn-red"><span class="fa fa-trash-o"></span></a>';
            $edit='<a href="'.base_url().'admin/image_slide_category/update/'.$slider_category_id.'" class="btn btn-xs btn-blue"><span class="fa fa-edit"></span></a>';
            /*
            $logo_name= (trim($category_photo)=='')?'-': pathinfo($category_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($category_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$pathImgArr['pathUrl'];
            $pathImgLoc=$pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',
            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);
            $category_content=strip_tags($category_content);    
            $category_content=(strlen($category_content)>200)?substr($category_content, 200).'...':$category_content;    
            */
            
            $count_slide=image_slide_lib::count_slide_on_category($slider_category_id);
                       
            ?>
            <tr>
            <td style="text-align:right;"><?php echo $no;?>.</td>
            <td style="text-align:center;"><?php echo $edit?></td>
            <td style="display:none;text-align:center;"><?php echo $delete?></td>
            <td><?php echo $slider_category_name?> <span class="badge badge-<?php echo ($count_slide<1)?'danger':'default'?>"><?php echo $count_slide?></span></td>
            <td align="center"><?php echo !empty($slider_category_width)?$slider_category_width:'-' ?></td>
            <td align="center"><?php echo !empty($slider_category_height)?$slider_category_height:'-' ?></td>
            <td style="text-align:center;">
                <span class="<?php echo (isset($slider_category_is_compress) && trim($slider_category_is_compress)=='Y')?'icon-ok':'clip-minus-circle' ?>"></span>
            </td>

            </tr>
            <?php
            $no++;
        }
    }
    else
    {
        ?>
        <tr>
            <td colspan="7" style="text-align: center;"> == Data belum tersedia ==</td>
        </tr>  
        <?php
    }
    ?>
    </tbody>
    </table>
    <center>
        <?php echo $link_pagination;?>
    </center>
    </div>
                              
