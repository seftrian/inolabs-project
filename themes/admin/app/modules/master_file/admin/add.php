<div class="col-sm-12">

   <div class="panel panel-default">
        <div class="panel-heading">
                           Form yang bertanda <span class="symbol required"/> wajib diisi.
                        </div>
                        <div class="panel-body">
                        <?php
                        //untuk menampilkan pesan
                        if(trim($message)!='')
                        {
                            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                        }
                        ?>
   
    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Judul <span class="symbol required"/>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="download_title" value="<?php echo $this->input->post('download_title'); ?>" />
            </div>
        </div>
           <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                File
            </label>
            <div class="col-sm-10">
                  <input class="form-control" type="file" name="download_file"/>
                  <p class="text-muted">
                      File: gif|jpg|jpeg|png|pdf|doc|xls|xlsx|ppt <br />
                      Ukuran maksimal : 3,2MB
                  </p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                Isi

            </label>
            <div class="col-sm-10">
          <?php echo form_textarea_tinymce('download_content', $this->input->post('download_content'), 'Standard'); ?>
            </div>
        </div>
        

        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               Meta Deskripsi
            </label>
            <div class="col-sm-10">
                 <textarea class="form-control"  placeholder="deskripsi singkat konten..." name="download_meta_description"><?php echo $this->input->post('download_meta_description'); ?></textarea>
            <p class="text-muted"> maks. 200 karakter dan memuat deskripsi singkat.</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
               Meta Tags
            </label>
            <div class="col-sm-10">
                 <textarea class="form-control" name="download_meta_tags" placeholder="kata, kunci..."><?php echo $this->input->post('download_meta_tags'); ?></textarea>
            <p class="text-muted"> maks. 200 karakter dan memuat kata kunci yang pisahkan dengan koma (,).</p>
            </div>
        </div>
     
        
        <div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
            <input type="reset" value="Reset" class="btn btn-warning" style="float:left;">
        </div>
        <div class="col-sm-6">
            <input style="float: right;" onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success">
        </div>
        </div>
    </form>
   
        </div>
  </div>
</div>
