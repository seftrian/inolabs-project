<?php
$logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
$logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
$logo_file=$logo_name.'.'.$logo_ext;
$pathImgUrl=$pathImgArr['pathUrl'];
$pathImgLoc=$pathImgArr['pathLocation'];
$imgProperty=array(
'width'=>70,
'height'=>70,
'imageOriginal'=>$logo_file,
'directoryOriginal'=>$pathImgLoc,
'directorySave'=>$pathImgLoc.'7070/',
'urlSave'=>$pathImgUrl.'7070/',
);
$img=$this->function_lib->resizeImageMoo($imgProperty);

$isValidUrl=$this->function_lib->isValidUrl($news_external_link);
$link_preview=$isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;
?>
<div class="row">
  <div class="col-sm-12">
     <div class="panel panel-default">
          <div class="panel-heading panel-btn">
              <button class="btn btn-sm btn-info" onclick="window.location.href='<?php echo base_url()?>admin/master_content/add'" ><i class="fa fa-plus"></i> Input Data</button>
              <button class="btn btn-sm btn-info " onclick="window.location.href='<?php echo base_url()?>admin/master_content/tab_content/<?php echo $news_id?>'" ><i class="fa fa-plus"></i> Konten Tab</button>    
          </div>
          <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
              <div class="panel-body">
              <?php
              //untuk menampilkan pesan
              if(trim($message)!='')
              {
                  echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
              }
              ?>
         
          
               <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                      Tgl. Event
                    </label>
                    <div class="col-sm-3">
                      <input name="news_date" type="text" data-date-format="dd-mm-yyyy" class="date-picker date" value="<?php echo $this->input->post('news_date')?$this->input->post('news_date'):date('d-m-Y',strtotime($news_timestamp));?>">
                    </div>
                    <div class="col-sm-2 input-group input-append bootstrap-timepicker">
                        <input name="news_time" value="<?php echo $this->input->post('news_time')?$this->input->post('news_time'):date( 'g:i A', strtotime( $news_timestamp ) )?>" type="text" class="form-control time-picker time">
                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                    </div>
              </div> 
               <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                    Kategori
                  </label>
                  <div class="row col-sm-10">
                      <?php
                      if(!empty($list_news_category))
                      {
                          $index=0;
                          foreach($list_news_category AS $rowArr)
                          {
                              foreach($rowArr AS $variable=>$value)
                              {
                                  ${$variable}=$value;
                              }
                              $where='news_category_id='.intval($category_id).' AND news_id='.intval($news_id);
                              $news_category_id=$this->function_lib->get_one('news_category_id','site_news_relation',$where);
                              $checked=($category_id==$news_category_id)?'checked':'';
                              $count_news=master_content_lib::count_article_on_category($category_id);
                             
                              ?>
                              <div class="col-sm-4">
                                  <label class="checkbox-inline">
                                      <input type="checkbox" onclick="check_same_category($(this));"  class="square-teal content_category category_id_<?php echo $category_id?>" value="<?php echo $category_id?>"  name="news_category_id[<?php echo $index?>]" <?php echo (isset($_POST['news_category_id'][$index]) AND $_POST['news_category_id'][$index]==$category_id)?'checked="checked"':$checked?>>
                                      <?php echo $category_title?> <span class="text-muted"><?php echo $category_type?></span>  <span class="badge badge-<?php echo ($count_news<1)?'danger':(
                                      ($checked=='checked')?'success':'default'
                                      )?>"><?php echo $count_news?></span>
                                  </label>
                              </div>
                              <?php
                              $index++;
                          }
                      }
                      ?>
                    
                  </div>
                  <div class="col-sm-10 col-sm-offset-2">
                      <input onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-sm btn-success mb-1">
                      <button class="btn btn-sm btn-danger mb-1" onclick="$('input:checked.content_category').attr('checked',false);return false;"><i class="fa fa-trash-o"></i> Reset Kategori</button>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                      Judul <span class="symbol required"/>
                  </label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="news_title" value="<?php echo $this->input->post('news_title')?$this->input->post('news_title'):$news_title; ?>" />
                  </div>
              </div>
              <div class="form-group">
                  <!--<div class="col-sm-2">
                     <label class="checkbox-inline" style="float:right;">
                          <input type="checkbox" value="" class="grey">
                          More
                      </label>
                  </div>-->
                  <label class="col-sm-2 control-label field_more" for="form-field-1">
                      Sub Judul
                  </label>
                  <div class="col-sm-10 field_more">
                      <input type="text" maxlength="254" placeholder="" class="form-control" name="news_label_1" value="<?php echo $this->input->post('news_label_1')?$this->input->post('news_label_1'):$news_label_1; ?>" />
                  </div>
                   <label class="col-sm-1 control-label field_more" for="form-field-1" style="display:none;">
                      Label 2 
                  </label>
                  <div class="col-sm-3 field_more" style="display:none;">
                      <input type="text" maxlength="100" placeholder="eq. 21K" class="form-control" name="news_label_2" value="<?php echo $this->input->post('news_label_2')?$this->input->post('news_label_2'):$news_label_2; ?>" />
                  </div>
                   <label class="col-sm-1 control-label field_more" for="form-field-1" style="display:none;">
                      Label 3
                  </label>
                  <div class="col-sm-2 field_more" style="display:none;">
                      <input type="text" maxlength="10" placeholder="eq. price" class="form-control" name="news_label_3" value="<?php echo $this->input->post('news_label_3')?$this->input->post('news_label_3'):$news_label_3; ?>" />
                  </div>

              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                      Isi <span class="symbol required"/>

                  </label>
                  <div class="col-sm-10">
                      <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#isi1">Isi 1</a></li>
                        <li><a data-toggle="tab" href="#isi2">Isi 2</a></li>
                      </ul>

                      <div class="tab-content">
                        <div id="isi1" class="tab-pane fade in active">
                          <?php echo form_textarea_tinymce('news_content', ($this->input->post('news_content')?$this->input->post('news_content'):html_entity_decode($news_content)), 'PageGenerator'); ?>
                        </div>
                        <div id="isi2" class="tab-pane fade">
                          <?php echo form_textarea_tinymce('news_content_dua', ($this->input->post('news_content_dua')?$this->input->post('news_content_dua'):html_entity_decode($news_content_2)), 'PageGenerator'); ?>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                     Link (video/Other)
                  </label>
                  <div class="col-sm-6">
                    <div class="input-group">
                       <input type="text" value="<?php echo $this->input->post('news_external_link')?$this->input->post('news_external_link'):$news_external_link?>" name="news_external_link" id="link" placeholder="eq. https://www.youtube.com/embed/DBxaM8dlEJI" class="form-control" />
                       <span class="input-group-btn ">
                         <a data-toggle="modal" id="add" onclick="add_modal();return false;" class="btn btn-sm btn-info btn-form" title="Ambil dari website ini"><i class="clip-search"></i></a>
                       </span>
                     </div>
                  <small class="text-muted"> Jika ingin menghubungkan dengan sumber lain. <span id="preview_link"><?php echo (trim($news_external_link)!='')?'<a href="'.$link_preview.'" target="_blank"><b>Preview</b></a>':''?></span></small>
                  </div>
                  
                  <div class="col-sm-4">
                       <input type="text" value="<?php echo $this->input->post('news_external_link_label')?$this->input->post('news_external_link_label'):$news_external_link_label?>"  name="news_external_link_label" id="link_label" placeholder="Label Link, default: Selengkapnya" class="form-control" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                      (Desc/Tags)
                  </label>
                  <div class="col-sm-5">
                       <textarea maxlength="200" class="form-control"  placeholder="deskripsi singkat konten..." name="news_meta_description"><?php echo $this->input->post('news_meta_description')?$this->input->post('news_meta_description'):$news_meta_description; ?></textarea>
                  <small class="text-muted"> Maks. 200 karakter dan memuat deskripsi singkat.</small>
                  </div>
                  <div class="col-sm-5">
                    <textarea class="form-control" name="news_meta_tags" placeholder="kata, kunci..."><?php echo $this->input->post('news_meta_tags')?$this->input->post('news_meta_tags'):$news_meta_tags; ?></textarea>
                    <small class="text-muted"> Maks. 200 karakter dan memuat kata kunci yang pisahkan dengan koma (,).</small>
                  </div>
              </div>
              <div class="form-group" style="display: none">
                  <label class="col-sm-2 control-label" for="form-field-1">
                     Harga (Rp)
                  </label>
                  <div class="col-sm-2">
                       <input class="form-control" name="news_harga" value="<?php echo $this->input->post('news_harga')?$this->input->post('news_harga'):$news_harga; ?>">
                      <small class="text-muted"> Tidak wajib. ini untuk kategori Price List.</small>
                  </div>
                  <div class="col-sm-8">
                  </div>
              </div>
           
              <!--<div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                      Gambar
                  </label>
                  <div class="col-sm-6">
                        <p class="text-danger">
                            <?php
                            if($logo_name=='-')
                            {
                              //echo 'Belum ada gambar.';
                            }
                            else
                            {
                          ?>
                              <img src="<?php //echo $img?>" />
                            <br />
                            <a href="<?php //echo base_url()?>admin/master_content/delete_image/<?php //echo $news_id?>" onclick="return confirm('Hapus?');return false;">Hapus Gambar</a>
                        
                          <?php      
                            }
                            ?>  
                        </p>  
                        <input type="hidden" name="news_photo_old" value="<?php //echo $news_photo?>"/>
                        <input class="form-control" type="file" name="news_photo"/>
                  </div>
              </div>
              -->
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                      Gambar
                  </label>
                  <div class="col-sm-10">
                          <p class="text-danger">
                            <?php
                            if($logo_name=='-')
                            {
                              echo 'Belum ada gambar.';
                            }
                            else
                            {
                          ?>
                              <img src="<?php echo $img?>" />
                            <br />
                            <a href="<?php echo base_url()?>admin/master_content/delete_image/<?php echo $news_id?>" onclick="return confirm('Hapus?');return false;">Hapus Gambar</a>
                        
                          <?php      
                            }
                            ?>  
                        </p> 
                        <small class="text-muted">File: gif, jpg, jpeg, png.<br />
                        Ukuran Maksimal : 8 MB
                        </small>
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered" id="more_image" >
                              <thead>
                                  <tr><th></th><th></th></tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td class="center">Gambar Utama</td><td class="center">
                                      <input type="hidden" name="news_photo_old" value="<?php echo $news_photo;?>">
                                      <input class="form-control" type="file" name="news_photo"/>
                                      </td>
                                  </tr>
                                   <?php
                                  if(!empty($images_arr))
                                  {
                                      $no=1;
                                      foreach($images_arr AS $rowArr)
                                      {
                                          $news_image=$rowArr['news_more_images_file'];
                                          $news_image_name=  pathinfo($news_image,PATHINFO_FILENAME);
                                          $news_image_name_ext=  pathinfo($news_image,PATHINFO_EXTENSION);
                                          $news_image_file=$news_image_name.'.'.$news_image_name_ext;
                                          $imgProperty=array(
                                          'width'=>100,
                                          'height'=>100,
                                          'imageOriginal'=>$news_image_file,
                                          'directoryOriginal'=>$pathImgLoc,
                                          'directorySave'=>$pathImgLoc.'100100/',
                                          'urlSave'=>$pathImgUrl.'100100/',

                                          );
                                          $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
                                          $link_delete=base_url().'admin/master_content/delete_more_images/'.$rowArr['news_more_images_id'].'/'.$news_id;
                                         ?>
                                          <tr id="tr_more_images_<?php echo $no;?>">
                                              <td class="center"><a href="#" class="btn btn-xs btn-danger delete" onclick="if(confirm('Hapus')){window.location.href='<?php echo $link_delete;?>'}return false;" id="more_images_<?php echo $no;?>"><i class="fa fa-trash-o"></i></a></td><td class="center">

                                                  <?php echo '<img src="'.$img.'" title="'.$news_title.'"/>'; ?>
                                                  <input type="hidden" name="news_more_images_file<?php echo $no;?>_id" value="<?php echo $rowArr['news_more_images_id'];?>">
                                                  <input type="hidden" name="news_more_images_file<?php echo $no;?>_old" value="<?php echo $news_image;?>">
                                                  <input class="form-control" type="file" name="news_more_images_file<?php echo $no;?>"/></td>
                                          </tr>
                                         <?php
                                         $no++;
                                      }
                                  }
                                  ?>
                              </tbody>

                              <tfoot>
                                  <tr>
                                      <th colspan="3" style="text-align: left;">
                                          <a href="#" onclick="add_row_table_more_images();return false;" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Tambah Gambar </a>
                                      </th>
                                  </tr>
                              </tfoot>
                          </table>
                        </div>
                  </div>
              </div>
              <input type="hidden" id="num_files" value="<?php echo (count($list_files)>0)?count($list_files):1?>">
              <?php
              if(!empty($list_files))
              {
                  $redirect=base_url().'admin/master_content/update/'.$news_id;
                  $no=0;
                  foreach($list_files AS $index=>$rowArr)
                  {
                      foreach($rowArr AS $variable=>$value)
                      {
                          ${$variable}=$value;
                      }
                      $download_title_value=isset($_POST['download_title'][$index])?$_POST['download_title'][$index]:$download_title;
                      $download_icon='';
                      $remove='<a title="Hapus file" onclick="return confirm(\'Hapus?\');" href="'.base_url().'admin/master_file/delete_data/'.$download_id.'?redirect='.rawurlencode($redirect).'" class="btn btn-sm  btn-danger"><i class="fa fa-trash-o"></i></a>';

                      if(file_exists(FCPATH.$download_file) AND trim($download_file)!='')
                      {
                          $download_icon='<a title="Download file" href="'.base_url().'admin/master_file/download_file/'.$download_id.'" class="btn btn-xs btn-green"><i class="fa fa-cloud-download"></i></a>';
                      }
                      ?>
                      <div class="form-group">
                          <label class="col-sm-2 control-label" for="form-field-1">
                              <?php echo ($index==0)?'File':''?>
                          </label>
                          <div class="col-sm-4">
                                 <?php echo $download_icon?>
                                 <?php echo $remove?>
                                <input type="hidden" value="<?php echo $download_id?>" name="download_id[<?php echo $index?>]">
                                <input style="width:80%;float:right;" class="form-control" type="file" name="download_file_<?php echo $index?>"/>
                          </div>
                          <div class="col-sm-4">
                                <input class="form-control" value="<?php echo $download_title_value?>" placeholder="Label file" type="text" name="download_title[<?php echo $index?>]"/>
                          </div>

                          
                      </div>
                      <?php
                  }
              }
              else
              {
                  ?>
                  <div class="form-group">
                          <label class="col-sm-2 control-label" for="form-field-1">
                             File
                          </label>
                          <div class="col-sm-4">
                                <input type="hidden" value="0" name="download_file_id[0]">
                                <input class="form-control" type="file" name="download_file_0"/>
                          </div>
                          <div class="col-sm-4">
                                <input class="form-control" placeholder="Label file" type="text" name="download_title[0]"/>
                          </div>
                  </div>
                  <?php  
              }
              ?>
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                     
                  </label>
                  <div class="col-sm-10">
                      <a href="#" onclick="add_input_file($(this));return false;" title="Tambah file" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> File</a> <br>
                      <small class="text-muted">File: gif, jpg, jpeg, png, pdf, doc, xls, xlsx, ppt, docx, pptx, zip. <br />
                      Ukuran Maksimal : 8MB
                      </small>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                     Publish
                  </label>
                  <div class="col-sm-2">
                      <select class="form-control" name="status_publish">
                          <option value="Y" <?php echo (isset($_POST['status_publish'])?(($_POST['status_publish']=='Y')?'selected':''):(($news_is_publish=='Y')?'selected':'')); ?>>Active</option>
                          <option value="N" <?php echo (isset($_POST['status_publish'])?(($_POST['status_publish']=='N')?'selected':''):(($news_is_publish=='N')?'selected':'')); ?>>Non Active</option>
                      </select>
                  </div>
                  <div class="col-sm-6">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1">
                    Kategori
                  </label>
                  <div class="row col-sm-10">
                      <?php
                      if(!empty($list_news_category))
                      {
                          $index=0;
                          foreach($list_news_category AS $rowArr)
                          {
                              foreach($rowArr AS $variable=>$value)
                              {
                                  ${$variable}=$value;
                              }
                              $where='news_category_id='.intval($category_id).' AND news_id='.intval($news_id);
                              $news_category_id=$this->function_lib->get_one('news_category_id','site_news_relation',$where);
                              $checked=($category_id==$news_category_id)?'checked':'';
                              $count_news=master_content_lib::count_article_on_category($category_id);
                             
                              ?>
                              <div class="col-sm-4">
                                  <label class="checkbox-inline">
                                      <input type="checkbox"  onclick="check_same_category($(this));"  class="square-teal content_category category_id_<?php echo $category_id?>" value="<?php echo $category_id?>"  name="news_category_id[<?php echo $index?>]" <?php echo (isset($_POST['news_category_id'][$index]) AND $_POST['news_category_id'][$index]==$category_id)?'checked="checked"':$checked?>>
                                      <?php echo $category_title?> <span class="text-muted"><?php echo $category_type?></span>  <span class="badge badge-<?php echo ($count_news<1)?'danger':(
                                      ($checked=='checked')?'success':'default'
                                      )?>"><?php echo $count_news?></span>
                                  </label>
                              </div>
                              <?php
                              $index++;
                          }
                      }
                      ?>
                    
                  </div>
                  <div class="col-sm-10 col-sm-offset-2">
                      <button class="btn btn-sm btn-danger" onclick="$('input:checked.content_category').attr('checked',false);return false;"><i class="fa fa-trash-o"></i> Reset Kategori</button>
                  </p>
              </div>
            </div>
              <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-2">
                      <div class="alert alert-warning mb-0">Field dengan tanda <span class="text-danger">*</span> wajib diisi. </div>
                      
                  </div>
              </div>
          </div>
          <div class="panel-footer">
            <a class="btn btn-warning btn-sm" href="<?php echo base_url()?>admin/master_content/index" onclick="return confirm('Batalkan?');">Batal</a>
            <input  onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success btn-sm">
            <input  onclick="return confirm('Simpan?');return false;" type="submit" name="save_return" value="Simpan dan Buat Konten Tab" class="btn btn-success btn-sm">
          </div>
        </form>
    </div>
  </div>
</div>

<div id="ajax-modal" data-width="70%" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-focus-on="input#item_name" style="display: none;"></div>