<div class="row">
    <div class="col-sm-12">

       <div class="panel panel-default">
           <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                <div class="panel-body">
                <?php
                //untuk menampilkan pesan
                if(trim($message)!='')
                {
                    echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
                }
                ?>
           
            
                <div class="form-group">
                      <label class="col-sm-2 control-label" for="form-field-1">
                        Tgl. Event
                      </label>
                      <div class="col-sm-3">
                        <input name="news_date" type="text" data-date-format="dd-mm-yyyy" class="date-picker date form-control" value="<?php echo $this->input->post('news_date')?$this->input->post('news_date'):date('d-m-Y');?>">
                      </div>
                      <div class="col-sm-2 input-group input-append bootstrap-timepicker">
                          <input name="news_time" value="<?php echo $this->input->post('news_time')?>" type="text" class="form-control time-picker time">
                          <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                      </div>
                </div>
                
                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                      Kategori
                    </label>
                    <div class="row col-sm-10">
                        <?php
                        if(!empty($list_news_category))
                        {
                            $index=0;
                            foreach($list_news_category AS $rowArr)
                            {
                                foreach($rowArr AS $variable=>$value)
                                {
                                    ${$variable}=$value;
                                }
                                $count_news=master_content_lib::count_article_on_category($category_id);
                                ?>
                                <div class="col-sm-4">
                                    <label class="checkbox-inline">
                                        <input  onclick="check_same_category($(this));" type="checkbox" class="square-teal content_category category_id_<?php echo $category_id?>" value="<?php echo $category_id?>"  name="news_category_id[<?php echo $index?>]" <?php echo (isset($_POST['news_category_id'][$index]) AND $_POST['news_category_id'][$index]==$category_id)?'checked="checked"':''?>>
                                        <?php echo $category_title?> <span class="text-muted"><?php echo $category_type?></span> <span class="badge badge-default"><?php echo $count_news?></span>
                                    </label>
                                </div>
                                <?php
                                $index++;
                            }
                        }
                        ?>
                    </div>
                    <div class="col-sm-10 col-sm-offset-2">
                        <button class="btn btn-sm btn-danger mb-1" onclick="$('input:checked.content_category').attr('checked',false);return false;"><i class="fa fa-trash-o"></i> Reset Kategori</button>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Judul <span class="symbol required"/>
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="news_title" value="<?php echo $this->input->post('news_title'); ?>" />
                    </div>
                </div>
                <div class="form-group" >
                    <!--<div class="col-sm-2">
                       <label class="checkbox-inline" style="float:right;">
                            <input type="checkbox" value="" class="grey">
                            More
                        </label>
                    </div>-->
                    <label class="col-sm-2 control-label field_more" for="form-field-1">
                        Sub Judul
                    </label>
                    <div class="col-sm-10 field_more" >
                        <input type="text" maxlength="254" placeholder="" class="form-control" name="news_label_1" value="<?php echo $this->input->post('news_label_1'); ?>" />
                    </div>
                     <label class="col-sm-1 control-label field_more" for="form-field-1" style="display:none;">
                        Label 2 
                    </label>
                    <div class="col-sm-3 field_more" style="display:none;">
                        <input type="text" maxlength="100" placeholder="eq. 21K, etc..." class="form-control" name="news_label_2" value="<?php echo $this->input->post('news_label_2'); ?>" />
                    </div>
                     <label class="col-sm-1 control-label field_more" for="form-field-1" style="display:none;">
                        Label 3
                    </label>
                    <div class="col-sm-3 field_more" style="display:none;">
                        <input type="text" maxlength="10" placeholder="eq. price" class="form-control" name="news_label_3" value="<?php echo $this->input->post('news_label_3'); ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Isi <span class="symbol required"/>

                    </label>
                    <div class="col-sm-10">
                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#isi1">Isi 1</a></li>
                          <li><a data-toggle="tab" href="#isi2">Isi 2</a></li>
                        </ul>

                        <div class="tab-content">
                          <div id="isi1" class="tab-pane fade in active">
                            <?php echo form_textarea_tinymce('news_content', $this->input->post('news_content'), 'PageGenerator'); ?>
                          </div>
                          <div id="isi2" class="tab-pane fade">
                            <?php echo form_textarea_tinymce('news_content_dua', $this->input->post('news_content_dua'), 'PageGenerator'); ?>
                          </div>
                        </div>
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                       Link (video/Other)
                    </label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="text" value="<?php echo $this->input->post('news_external_link')?$this->input->post('news_external_link'):''?>" name="news_external_link" id="link" placeholder="eq. https://www.youtube.com/embed/DBxaM8dlEJI" class="form-control" />
                            <span class="input-group-btn">
                                <a data-toggle="modal" id="add" onclick="add_modal();return false;" class="btn btn-sm btn-info btn-form" title="Ambil dari website ini"><i class="clip-search"></i></a>
                            </span>
                         </div>
                    <small class="text-muted"> Jika ingin menghubungkan dengan sumber lain. <span id="preview_link"></span></small>
                    </div>
                    <!-- <div class="col-sm-1"><a data-toggle="modal" id="add" onclick="add_modal();return false;" class="btn btn-xs btn-green" title="Ambil dari website ini"><i class="clip-search"></i></a></div> -->
                    <div class="col-sm-4">
                         <input type="text" value="<?php echo $this->input->post('news_external_link_label')?$this->input->post('news_external_link_label'):''?>"  name="news_external_link_label" id="link_label" placeholder="Label Link, default: Selengkapnya" class="form-control" />
                    </div>
                </div>
                

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                       SEO (Desc/Tags)
                    </label>
                    <div class="col-sm-5">
                         <textarea class="form-control"  placeholder="deskripsi singkat konten..." name="news_meta_description"><?php echo $this->input->post('news_meta_description'); ?></textarea>
                    <small class="text-muted"> Maks. 200 karakter dan memuat deskripsi singkat.</small>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" name="news_meta_tags" placeholder="kata, kunci..."><?php echo $this->input->post('news_meta_tags'); ?></textarea>
                        <small class="text-muted"> Maks. 200 karakter dan memuat kata kunci yang pisahkan dengan koma (,).</small>
                    </div>
                </div>
               
                <div class="form-group" style="display: none">
                    <label class="col-sm-2 control-label" for="form-field-1">
                       Harga (Rp)
                    </label>
                    <div class="col-sm-2">
                        <input class="form-control" name="news_harga" value="<?php echo $this->input->post('news_harga'); ?>">
                        <small class="text-muted"> Tidak wajib. ini untuk kategori Price List.</small>
                    </div>
                    <div class="col-sm-8">
                    </div>
                </div>
               
                <!--<div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Gambar
                    </label>
                    <div class="col-sm-10">
                          <input class="form-control" type="file" name="news_photo"/>
                    </div>
                </div>
                -->
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Gambar
                    </label>
                    <div class="col-sm-10">
                      <small class="text-muted">File: gif, jpg, jpeg, png.<br />
                          Ukuran Maksimal : 8 MB
                      </small>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="more_image" >
                                <thead>
                                    <tr><th></th><th></th></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="center">Gambar Utama</td><td class="center"><input class="form-control" type="file" name="news_photo"/></td>
                                    </tr>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th colspan="3" style="text-align: left;">
                                            <a href="#" onclick="add_row_table_more_images();return false;" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Tambah Gambar</a>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                       File
                    </label>
                    <div class="col-sm-4">
                          <input type="hidden" value="0" name="download_file_id[0]">
                          <input class="form-control" type="file" name="download_file_0"/>
                    </div>
                    <div class="col-sm-4">
                          <input class="form-control" placeholder="Label file" type="text" name="download_title[0]"/>
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                       
                    </label>
                    <div class="col-sm-10">
                        <a href="#" onclick="add_input_file($(this));return false;" title="Tambah file" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> File</a> <br>   
                        <small class="text-muted">File: gif, jpg, jpeg, png, pdf, doc, xls, xlsx, ppt, docx, pptx, zip. <br />
                        Ukuran Maksimal : 8 MB
                        </small>
                    </div>
                </div>
                <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                               Publish
                            </label>
                            <div class="col-sm-2">
                                <select class="form-control" name="status_publish">
                                    <option value="Y" <?php echo ((isset($_POST['status_publish']))?(($_POST['status_publish']=='Y')?'selected':''):''); ?>>Active</option>
                                    <option value="N" <?php echo ((isset($_POST['status_publish']))?(($_POST['status_publish']=='N')?'selected':''):''); ?>>Non Active</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                            </div>
                </div>

          
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                      Kategori
                    </label>
                    <div class="row col-sm-10">
                        <?php
                        if(!empty($list_news_category))
                        {
                            $index=0;
                            foreach($list_news_category AS $rowArr)
                            {
                                foreach($rowArr AS $variable=>$value)
                                {
                                    ${$variable}=$value;
                                }
                                $count_news=master_content_lib::count_article_on_category($category_id);
                                ?>
                                <div class="col-sm-4">
                                    <label class="checkbox-inline">
                                        <input onclick="check_same_category($(this));" type="checkbox" class="square-teal content_category category_id_<?php echo $category_id?>" value="<?php echo $category_id?>"  name="news_category_id[<?php echo $index?>]" <?php echo (isset($_POST['news_category_id'][$index]) AND $_POST['news_category_id'][$index]==$category_id)?'checked="checked"':''?>>
                                        <?php echo $category_title?> <span class="text-muted"><?php echo $category_type?></span> <span class="badge badge-default"><?php echo $count_news?></span>
                                    </label>
                                </div>
                                <?php
                                $index++;
                            }
                        }
                        ?>
                    </div>
                    <div class="col-sm-10 col-sm-offset-2">
                        <button class="btn btn-sm btn-danger mb-1" onclick="$('input:checked.content_category').attr('checked',false);return false;"><i class="fa fa-trash-o"></i> Reset Kategori</button>
                    </div>
                </div>
              
                <div class="form-group">
                
                    <div class="col-sm-10 col-sm-offset-2">
                        <div class="alert alert-warning mb-0">Field dengan tanda <span class="text-danger">*</span> wajib diisi. </div>
                        
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a class="btn btn-warning  btn-sm" href="<?php echo base_url()?>admin/master_content/index" onclick="return confirm('Batalkan?');">Batal</a>
                <input  onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-sm btn-success ">
                <input  onclick="return confirm('Simpan?');return false;" type="submit" name="save_return" value="Simpan dan Buat Konten Tab" class="btn btn-success btn-sm">
            </div>
        </form>
      </div>
    </div>
</div>
<div id="ajax-modal" data-width="70%" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-focus-on="input#item_name" style="display: none;"></div>

