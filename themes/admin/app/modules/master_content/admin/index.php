<div class="row">
    <div class="col-md-12">
        <?php
        //untuk menampilkan pesan
        if(trim($message)!='')
        {
            
            echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
        }
        ?>
                            
      <div class="panel panel-default mb-1">
        <div class="panel-heading">
          <i class="fa fa-search"></i>
          Pencarian
          <div class="panel-tools">
            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
            </a>
          </div>
        </div>
        <div class="panel-body">        
          <div class="row">
              <div class="col-sm-3">
                  <select id="news_category_id" class="form-control" onchange="grid_reload();return false;">
                    <option value="">Tampilkan semua kategori</option>
                       <?php
                        if(!empty($list_news_category))
                        {
                            $index=0;
                            foreach($list_news_category AS $rowArr)
                            {
                                foreach($rowArr AS $variable=>$value)
                                {
                                    ${$variable}=$value;
                                }
                                ?>
                                <option value="<?php echo $category_id?>"><?php echo $category_title?></option>
                                <?php
                                $index++;
                            }
                        }
                        ?>
                  </select>
              </div>
              <div class="col-sm-3">
                  <input onkeyup="grid_reload();return false;" type="text" id="news_title" placeholder="Judul"  class="form-control"/>
              </div>
              <div class="col-sm-4">
                 <input onkeyup="grid_reload();return false;" type="text" id="news_content" placeholder="Isi" class="form-control" />
              </div>
                <button class="btn btn-sm btn-warning" onclick="reset_pencarian();return false;">Reset</button>
            
          </div>       
        </div>
      </div>      
      </div>   
         <div class="col-sm-12">
                                                                 
          <div class="alert" style="display:none;"></div>                                                                       
         <table id="gridview" style="display:none;"></table> <br>
            
            <form target="_blank" id="form_export" method="post" action="export_data">
                <input id="export_start_date" type="hidden" name="export_start_date" value="" />
                <input id="export_end_date" type="hidden" name="export_end_date" value="" />
            </form>
         </div>                           

                    <!-- end: RESPONSIVE TABLE PANEL -->
                </div>
        </div>



