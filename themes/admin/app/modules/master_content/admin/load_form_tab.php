<div class="form-group div_form_tab_content_<?php echo $index?>">
    <label class="col-sm-2 control-label" for="form-field-1">
        Judul Tab <span class="symbol required"/>
    </label>
    <div class="col-sm-8">
        <input type="hidden" name="tab_id[<?php echo $index?>]">
        <input type="text" class="form-control" name="tab_title[<?php echo $index?>]" value="" />
    </div>
    <div class="col-sm-2">
        <a title="Hapus" onclick="remove_tab_content(<?php echo $index?>);return false;"  class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
    </div>
</div>
            
<div class="form-group div_form_tab_content_<?php echo $index?>">
    <label class="col-sm-2 control-label" for="form-field-1">
        Isi Tab <span class="symbol required"/>

    </label>
    <div class="col-sm-8" >
     <textarea class="form-control" name="tab_content[<?php echo $index?>]" style="min-height: 250px;"></textarea>
    </div>
</div>