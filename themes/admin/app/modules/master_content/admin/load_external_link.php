<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        &times;
    </button>
    <h4 class="modal-title">Data Konten Website</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="col-sm-12">
                <div class="row col-sm-12">
                    <div class="col-sm-3">
                        <select id="news_category_id" class="form-control" onchange="grid_reload_external_link();return false;">
                            <option value="">Tampilkan semua kategori</option>
                            <?php
                            if (!empty($list_news_category)) {
                                $index = 0;
                                foreach ($list_news_category AS $rowArr) {
                                    foreach ($rowArr AS $variable => $value) {
                                        ${$variable} = $value;
                                    }
                                    ?>
                                    <option value="<?php echo $category_id ?>"><?php echo $category_title ?></option>
                                    <?php
                                    $index++;
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input onkeyup="grid_reload_external_link();return false;" type="text" id="news_title" placeholder="Judul"  class="form-control"/>
                    </div>
                    <div class="col-sm-4">
                        <input onkeyup="grid_reload_external_link();return false;" type="text" id="news_content" placeholder="Isi" class="form-control" />
                    </div>

                </div>
            </div>
            <div class="col-sm-12">
                <h5><i class="fa fa-search fa-white"></i> Pencarian</h5>

                <table id="grid_reload_external_link" style="display:none;"></table>
                <form target="_blank" id="form_export" method="post" action="export_data">
                    <input id="export_start_date" type="hidden" name="export_start_date" value="" />
                    <input id="export_end_date" type="hidden" name="export_end_date" value="" />
                </form>
            </div>

            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-bricky">
        Batal [Esc]
    </button>
</div>
<script>
    $("#grid_reload_external_link").flexigrid({
        dataType: 'json',
        colModel: [
            {display: 'No', name: 'no', width: 30, sortable: false, align: 'right'},
            //{ display: 'Edit', name: 'edit', width: 50, sortable: false, align: 'center' },
            //{ display: 'Delete', name: 'delete', width: 50, sortable: false, align: 'center' },
            {display: 'Pilih', name: 'load_link', width: 50, sortable: false, align: 'center'},
            {display: 'Judul', name: 'title', width: 300, sortable: false, align: 'left'},
            //{ display: 'Isi', name: 'content', width: 560, sortable: false, align: 'left' },
            //{ display: 'Meta Deskripsi', name: 'meta_description', width: 200, sortable: false, align: 'left' },
            //{ display: 'Meta Tags', name: 'meta_tags', width: 200, sortable: false, align: 'left' },
            {display: 'Gambar', name: 'photo', width: 80, sortable: false, align: 'center'},
            {display: 'Diinput Oleh', name: 'input_by', width: 80, sortable: false, align: 'center'},
        ],
        buttons: [
        ],
        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
        ],
        /*searchitems: [
         { display: 'Nama', name: 'item_nama', type: 'text', isdefault: true },

         { display: 'Status', name: 'item_is_active', type: 'select', option: 'Y:Aktif|N:Draft' },
         // { display: 'Tanggal', name: 'item_timestamp', type: 'date' },

         ],*/
        sortname: "id",
        sortorder: "asc",
        usepager: true,
        title: ' ',
        useRp: true,
        rp: 10,
        showTableToggleBtn: false,
        showToggleBtn: true,
        width: 'auto',
        height: '300',
        resizable: false,
        singleSelect: false,
        nowrap: false,
    });


    $(document).ready(function() {
        grid_reload_external_link();
    });



    function grid_reload_external_link() {
        var news_category_id = $("select#news_category_id").val();
        var news_title = $("input#news_title").val();
        var news_content = $("input#news_content").val();
        var link_service = '?news_category_id=' + news_category_id + '&news_title=' + news_title + '&news_content=' + news_content;
        $("#grid_reload_external_link").flexOptions({url: '<?php echo base_url() . 'admin/' . $this->currentModule; ?>/get_data' + link_service}).flexReload();
    }


</script>