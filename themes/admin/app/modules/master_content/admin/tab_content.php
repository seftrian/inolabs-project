<?php


$isValidUrl=$this->function_lib->isValidUrl($news_external_link);
$link_preview=$isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;
?>
<div class="row">
<div class="col-sm-12">
    
   <div class="panel panel-default">
        <div class="panel-heading panel-btn">
            <button class="btn btn-sm btn-info btn-square" onclick="window.location.href='<?php echo base_url()?>admin/master_content/add'" ><i class="fa fa-plus"></i> Input Data</button>
           
        </div>
        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
            <div class="panel-body">
            <?php
            //untuk menampilkan pesan
            if(trim($message)!='')
            {
                echo $this->function_lib->show_message($message,$templateName,$themeId,$status);
            }
            ?>
       
        
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Judul Artikel <span class="symbol required"/>
                </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="news_title" value="<?php echo $this->input->post('news_title')?$this->input->post('news_title'):$news_title; ?>" readonly/>
                </div>
            </div> 
             <div class="form-group">
                <div class="col-sm-12">
                    <a href="javscript:void(0)" onclick="$('.preview_editor').toggle();return false;"><strong>Gunakan Bantuan Editor</strong></a>    
                </div>
            </div> 
            <div class="form-group preview_editor"  style="max-height:200px;display:none;">
               
                <div class="col-sm-12">
                    
                      <?php echo form_textarea_tinymce('news_content', $this->input->post('news_content'), 'Standard'); ?> <br>
                        <div class="alert alert-info mt-1">
                            Editor ini dapat digunakan untuk membantu dalam pembuatan konten yang lebih bagus. <br />
                            Cara penggunaan: <br />
                            <ol>
                                <li>Isikan tulisan yang akan dibuat pada editor ini dan pilih tools yang tersedian dan diigininkan.</li>
                                <li>Klik tanda <strong>&lt;&gt;</strong>(Preview), lalu copy ke isi tab. </li>
                            </ol>
                        </div>

                </div>
            
            </div>

            <div class="div_tab_content">
            <?php
            if(!empty($list_tab_content))
            {
                $index=0;
                foreach($list_tab_content AS $rowArr)
                {
                    foreach($rowArr AS $variable=>$value)
                    {
                        ${$variable}=$value;
                    }
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Judul Tab  <span class="symbol required"/>
                        </label>
                        <div class="col-sm-8">
                            <input type="hidden" name="tab_id[<?php echo $index?>]" value="<?php echo $tab_id?>">
                            <input type="text" class="form-control" name="tab_title[<?php echo $index?>]" value="<?php echo $tab_title?>"/>
                        </div>
                        <div class="col-sm-2">
                            <a href="<?php echo base_url().'admin/master_content/delete_tab/'.$news_id.'/'.$tab_id?>" title="Hapus" onclick="return confirm('Hapus?');return false;"  class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Isi Tab<span class="symbol required"/>

                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="tab_content[<?php echo $index?>]" style="min-height: 250px;"><?php echo $tab_content?></textarea>
                        </div>
                    </div>
                    <?php
                    $index++;
                }

            } 
            else
            {
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Judul Tab  <span class="symbol required"/>
                        </label>
                        <div class="col-sm-8">
                            <input type="hidden" name="tab_id[0]" value="">
                            <input type="text" class="form-control" name="tab_title[0]"/>
                        </div>

                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Isi Tab<span class="symbol required"/>

                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="tab_content[0]" style="min-height: 250px;"></textarea>
                        </div>
                    </div>
                    <?php
            }
            ?>
                
            </div>    
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                   
                </label>
                <div class="col-sm-4">
                    <a href="#" onclick="add_tab_content($(this));return false;" title="Tambah Tab" class="btn btn-sm btn-info mb-1"> <i class="fa fa-plus "></i> Tab</a>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="alert alert-warning mb-0">
                        Form yang bertanda <span class="symbol required"/> wajib diisi.
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <a href="<?php echo base_url()?>admin/master_content/index" onclick="return confirm('Batalkan?');" class="btn btn-sm btn-warning">Batal</a>
            <input  onclick="return confirm('Simpan?');return false;" type="submit" name="save" value="Simpan" class="btn btn-success btn-sm">
            <input  onclick="return confirm('Simpan?');return false;" type="submit" name="save_return" value="Simpan dan Kembali" class="btn btn-success btn-sm">
        </div>
    </form>
  </div>
</div>
</div>