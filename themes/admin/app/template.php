<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
$w_lib = new widget_lib;
?>
<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->

    <head>
        <title><?php echo isset($seoTitle) ? $seoTitle : $websiteConfig['config_name']; ?></title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/fonts/style.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/css/main-responsive.css">


        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/css/theme_dark.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/css/print.css" type="text/css" media="print" />
        <!--[if IE 7]>
                <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
                <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="shortcut icon" href="<?php echo (isset($websiteConfig['config_favicon']) and file_exists(FCPATH . $websiteConfig['config_favicon']) and trim($websiteConfig['config_favicon']) != '') ? base_url() . $websiteConfig['config_favicon'] : '' ?>" />
        <?php
        echo isset($extra_plugin_css) ? $extra_plugin_css : '';
        ?>
        <?php
        echo isset($extra_head_content) ? $extra_head_content : '';
        ?>
        <link rel="stylesheet" href="<?php echo $themes_inc; ?>assets/css/custom.css">
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->

    <body>
        <!--class="navigation-small"  for small navigation-->
        <input type="hidden" id="base_url" value="<?php echo base_url() ?>">
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
                    <?php
                    $config_logo = '';
                    if (isset($websiteConfig['config_logo']) and file_exists(FCPATH . $websiteConfig['config_logo']) and trim($websiteConfig['config_logo']) != '') {
                        $config_logo = '<img style="width:15%;" src="' . base_url() . $websiteConfig['config_logo'] . '" alt="' . $websiteConfig['config_name'] . '">';
                    }
                    ?>
                    <a class="navbar-brand" href="<?php echo base_url() . 'admin/dashboard/' ?>">
                        <?php echo $websiteConfig['config_name']; ?>
                    </a>
                    <!-- end: LOGO -->
                </div>
                <div class="navbar-tools">
                    <!-- start: TOP NAVIGATION MENU -->
                    <ul class="nav navbar-right">
                        <!-- end: NOTIFICATION DROPDOWN -->
                        <!-- start: MESSAGE DROPDOWN -->
                        <?php
                        //widget_lib::run('admin', 'admin_menu', 'widget_menu');
                        $w_lib->run('admin', 'notification', 'widget_header_notification');
                        ?>
                        <!-- end: MESSAGE DROPDOWN -->
                        <!-- start: USER DROPDOWN -->
                        <li class="dropdown current-user">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                <img src="<?php echo $themes_inc; ?>assets/images/avatar-1-small.jpg" class="circle-img" alt="">
                                <span class="username"><?php echo ucfirst($_SESSION['admin']['detail']['admin_username']); ?></span>
                                <i class="clip-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/admin_user/update/<?php echo $_SESSION['admin']['detail']['admin_id']; ?>">
                                        <i class="clip-user-2"></i>
                                        &nbsp;My Profile
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url() ?>auth/logout" onclick="if (confirm('Logout?')) {
                                                return true;
                                            } else {
                                                return false;
                                            }">
                                        <i class="clip-exit"></i>
                                        &nbsp;Log Out
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end: USER DROPDOWN -->
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                <div class="main-navigation navbar-collapse collapse">
                    <!-- start: MAIN MENU TOGGLER BUTTON -->
                    <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                    </div>
                    <!-- end: MAIN MENU TOGGLER BUTTON -->
                    <!-- start: MAIN NAVIGATION MENU -->
                    <?php
                    //widget_lib::run('admin', 'admin_menu', 'widget_menu');
                    $w_lib->run('admin', 'admin_menu', 'widget_menu');
                    ?>
                    <!-- end: MAIN NAVIGATION MENU -->
                </div>
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">
                <!-- start: PANEL CONFIGURATION MODAL FORM -->

                <!-- /.modal -->
                <!-- end: SPANEL CONFIGURATION MODAL FORM -->
                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: STYLE SELECTOR BOX -->
                            <!-- end: STYLE SELECTOR BOX -->
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <?php echo isset($breadcrumbs) ? $breadcrumbs : ''; ?>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->
                    <?php
                    $this->load->view($view);
                    ?>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <div class="footer clearfix">
            <div class="footer-inner">

            </div>
            <div class="footer-items">
                <span class="go-top"><i class="clip-chevron-up"></i></span>
            </div>
        </div>
        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
                <script src="<?php echo $themes_inc; ?>assets/plugins/respond.min.js"></script>
                <script src="<?php echo $themes_inc; ?>assets/plugins/excanvas.min.js"></script>
                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                <![endif]-->
        <!--[if gte IE 9]><!-->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
        <!--<![endif]-->
        <script src="<?php echo $themes_inc; ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="<?php echo $themes_inc; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $themes_inc; ?>assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="<?php echo $themes_inc; ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
        <script src="<?php echo $themes_inc; ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
        <script src="<?php echo $themes_inc; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
        <script src="<?php echo $themes_inc; ?>assets/plugins/less/less-1.5.0.min.js"></script>
        <script src="<?php echo $themes_inc; ?>assets/js/main.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <script>
                                        $(document).ready(function() {
                                            Main.init();
                                            if ($('.form-control').length > 0) {
                                                $('.form-control')[0].focus();
                                            }
                                            //	Index.init();
                                        });
        </script>
        <?php
        echo isset($footerScript) ? $footerScript : '';
        ?>
    </body>
    <!-- end: BODY -->
</html>