
/**
skrip inisialisasi google map
- drawing
- searching
- marker
@author Tejo Murti
@date 1 April 2015
v1
*/
var mapCenter = new google.maps.LatLng(-7.800799, 110.340667);
var marker;
 var markers = [];
var map;
var myCoordsLenght = 6;
var infowindow = new google.maps.InfoWindow();
 var drawingManager;
var selectedShape;
var polygon;
var base_url=$("#base_url").val();
var marker_drawing;
var old_marker;
function initialize() {
  var mapOptions = {
    zoom: 9,
    center: mapCenter
  };

  map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);

  /*
  marker = new google.maps.Marker({
    map:map,
    draggable:true,
    animation: google.maps.Animation.DROP,
    position: mapCenter
  });
  */

  /** searching location */
   // Create the search box and link it to the UI element.
  var input = /** @type {HTMLInputElement} */(
      document.getElementById('searching-location'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

  // [START region_getplaces]
  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: base_url+'assets/images/icon_marker.png',
        title: place.name,
        position: place.geometry.location
      });
      $("#latitude").val(marker.getPosition().lat());
      $("#longitude").val(marker.getPosition().lng());
      markers.push(marker);

      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
    return false;
  });
  // [END region_getplaces]

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
      

        var polyOptions = {
          strokeWeight: 0,
          fillOpacity: 0.45,
          editable: true,
          draggable: true,
        };

      drawingManager = new google.maps.drawing.DrawingManager({

        //  drawingMode: google.maps.drawing.OverlayType.POLYGON,
           drawingControl: true,
            drawingControlOptions: {
              position: google.maps.ControlPosition.TOP_CENTER,
              drawingModes: [
              google.maps.drawing.OverlayType.MARKER,
            //    google.maps.drawing.OverlayType.CIRCLE,
             //   google.maps.drawing.OverlayType.POLYGON,
            //    google.maps.drawing.OverlayType.POLYLINE,
            //    google.maps.drawing.OverlayType.RECTANGLE
              ]
            },
          markerOptions: {
            icon: base_url+'assets/images/icon_marker.png'
          },
          polylineOptions: {
            editable: true
          },
          rectangleOptions: polyOptions,
          circleOptions: polyOptions,
          polygonOptions: polyOptions,
          map: map
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {

            if (e.type == google.maps.drawing.OverlayType.MARKER)
            {
                $("textarea[name='map_polygon_coordinate']").val('');
                marker_drawing=e.overlay;
                $("#latitude").val(marker_drawing.getPosition().lat());
                $("#longitude").val(marker_drawing.getPosition().lng());
                 google.maps.event.addListener(marker_drawing, 'dragend', function() {
                  //setSelection(newShape);
                });
               
                if(typeof(old_marker.getPosition())!='undefined')
                {
                  old_marker.setMap(null);
                }
                var old_marker=marker_drawing;
            }    

            if (e.type != google.maps.drawing.OverlayType.MARKER) {
              // Switch back to non-drawing mode after drawing a shape.
              drawingManager.setDrawingMode(null);

              // Add an event listener that selects the newly-drawn shape when the user
              // mouses down on it.
              var newShape = e.overlay;

              newShape.type = e.type;

              google.maps.event.addListener(newShape, 'click', function() {
                setSelection(newShape);
              });
               google.maps.event.addListener(newShape, 'dragend', function() {
                setSelection(newShape);
              });
              setSelection(newShape);
            }

        });

        // Clear the current selection when the drawing mode is changed, or when the
        // map is clicked.

        google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
        google.maps.event.addListener(map, 'click', clearSelection);

        //custom_set_center();
        custom_coordinate_polygon();
}

function toggleBounce() {

  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}

  function clearSelection() {
    if (selectedShape) {
      selectedShape.setEditable(false);
      selectedShape = null;
    }
  }

  function setSelection(shape) {
    clearSelection();
    selectedShape = shape;
    shape.setEditable(true);

    var vertices = shape.getPath();
     var contentString = "";
    // Iterate over the vertices.
    for (var i =0; i < vertices.getLength(); i++) {
      var xy = vertices.getAt(i);
      contentString += xy.lat() + ', ' +xy.lng()+'#';
    }

    $("textarea[name='map_polygon_coordinate']").val(contentString);
  }

  function deleteSelectedShape() {
    if (selectedShape) {
      selectedShape.setMap(null);
      $("textarea[name='map_polygon_coordinate']").val('');
    }
  }

  //set center untuk data yang ada koordinat centernya
  function custom_set_center()
  {
    var latitude=$("#latitude").val();
    var longitude=$("#longitude").val();

    if($.trim(latitude)!='' && $.trim(longitude)!='')
    {
        var myLatlng = new google.maps.LatLng(latitude,longitude);
        map.setCenter(myLatlng);
        marker.setPosition(myLatlng);
    }
  }

  /**
  ambil koordinat polygon
  */
  function custom_coordinate_polygon()
  {
     var table_information=$("#table-information").html();
     var lat=$("#latitude").val();
     var lng=$("#longitude").val();
     if($.trim(lat)!='' && $.trim(lng)!='')
     {
          var marker_position= new google.maps.LatLng(lat, lng);
          var marker = new google.maps.Marker({
              position: marker_position,
              icon: base_url+'assets/images/icon_marker.png',
              title:'',
          });

           google.maps.event.addListener(marker, 'mouseover', function() {
            //console.log(table_information);
              infowindow.setContent('<table style="width:300px;heigth:400px;">'+table_information+'</table>');
              infowindow.setPosition(marker_position);     
              infowindow.open(map);
            });

          // To add the marker to the map, call setMap();
          marker.setMap(map);
          map.setCenter(marker_position);
     }
  }

google.maps.event.addDomListener(window, 'load', initialize);