var __module__='item';
var __base_url__=$("#base_url").val(); //ambil dari input data

//untuk ngecek barcode jika telah ada yang menggunakan
        function check_barcode_is_exist(elem,item_id_value)
        {
            item_barcode_value=elem.val();
            if(jQuery.trim(elem.val())!=''){
                var jqxhr=jQuery.ajax({
                    url:__base_url__+__module__+'/service_rest/check_barcode_is_exist',
                    type:'get',
                    dataType:'json',
                    data:{item_barcode:item_barcode_value,item_id:item_id_value}
                });
            
                jqxhr.success(function(response){
                    if(response['status']!=200)
                    {
                        alert(response['message']);
                        elem.val('');
                        elem.focus();

                        return false;
                    }
                })
                jqxhr.error(function(){
                    alert('an error has occurred, plase try again');
                })
            }
        }    

      
         //untuk ngecek golongan margin
        function get_golongan_margin(elem)
        {
            if(jQuery.trim(elem.val())!=''){
                var jqxhr=jQuery.ajax({
                    url:__base_url__+__module__+'/service_rest/get_golongan_margin',
                    type:'get',
                    dataType:'json',
                    data:{item_golongan_margin_id:elem.val()}
                });
            
                jqxhr.success(function(response){
                    if(response['status']==200)
                    {
                        $('input[name="item_margin_resep"]').autoNumeric('set', response['data']['golongan_margin_resep']);
                        $('input[name="item_margin_non_resep"]').autoNumeric('set', response['data']['golongan_margin_non_resep']);
                        calculate_harga_jual();
                      //  jQuery('input[name="item_margin_resep"]').val(response['data']['golongan_margin_resep']);
                   //     jQuery('input[name="item_margin_non_resep"]').val(response['data']['golongan_margin_non_resep']);
                        
                        return false;
                    }
                })
                jqxhr.error(function(){
                    alert('an error has occurred, plase try again');
                })
            }
            else
            {
                jQuery('input[name="item_margin_resep"]').val('');
                jQuery('input[name="item_margin_non_resep"]').val('');
                jQuery('input[name="item_margin_resep"]').focus();
                       
            }
        }

        function currency_rupiah()
        {
            var myOptions = {aSep: '.', aDec: ','};
            $('.currency_rupiah').autoNumeric('init', myOptions); 
        }

        function reset_numeric(elem)
        {
            $('input[name="item_margin_non_resep"]').autoNumeric('get')
        }

        //hapus data        
            $("body").delegate("a.delete_satuan_settings_fields",'click', function(e) {
                //$(this).parent().parent().remove();
                if(confirm('Hapus?')) {   
                    e.preventDefault();
                    var parent = $(this).parent().parent().parent().parent();
                    parent.slideUp('medium',function() {
                        parent.remove();
                    });
                }
                return false;
            });    

             //hapus data        
            $("body").delegate("a.delete_satuan_fields",'click', function(e) {
                //$(this).parent().parent().remove();
                if(confirm('Hapus?')) {   
                    e.preventDefault();
                    var parent = $(this).parent().parent().parent().parent().parent().parent();
                
                    
                     parent.slideUp('medium',function() {
                        value_field=$(this).attr('id')*1;
                        calculate_harga_jual_bertingkat('','',value_field,true);
                        parent.remove();
                     });
                }
                return false;
            });    
        

        function change_element_field(no_satuan_field)
        {
                   //set onchange agar setiap ada penambahan field dapat membuat dynamic harga
                   $("div.container_container_satuan").find('.konversi').attr('onchange','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                   $("div.container_container_satuan").find('.range_start').attr('onchange','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                   $("div.container_container_satuan").find('.range_end').attr('onchange','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                  
                   $("div.container_container_satuan").find('.mr').attr('onchange','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                   $("div.container_container_satuan").find('.mnr').attr('onchange','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                   $("div.container_container_satuan").find('.dr').attr('onchange','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                   $("div.container_container_satuan").find('.dp').attr('onchange','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                   $("form#form-second").find('button[name="button_second"]').attr('onclick','calculate_harga_jual_bertingkat(\'\',\'\','+no_satuan_field+',true);');
                   
                   $("div.container_container_satuan").find('a.delete_satuan_fields').attr('id',no_satuan_field);
               
        }    

        function validate_konversi()
        {
            var value=true;
            //console.log($(".container_container_satuan").find('.konversi').length);
            if($.trim($(".container_container_satuan").find('.konversi').val())==''){
                    alert('Mohon maaf, nilai konversi harap diisi.');
                    value=false;
            }
            
            //console.log($(".container_container_satuan").find('input.konversi').length);
            //if($(".container_container_satuan").find('input[class="konversi"][value=""]').length) return false;
            $(".container_container_satuan").find('.konversi').each( function( key, value ) {
                   if($.trim($(this).val())=='')
                   {
                        alert('Mohon maaf, nilai konversi harap diisi.');
                        $(this).focus();
                        value=false;   
                         $(this).stopPropagation();
                        //$('a[href="#panel_second"]').tab('show');
                        return value=false;       
                   }
                });
            
            return value;
        }

        function onclick_tab(tab_id)
        {
            $('a[href="#'+tab_id+'"]').tab('show');
        }

         /**
        fungsi untuk mendapatkan nama pelanggan
        */
        function get_detail_pabrik_by_name()
        {
            var cacheItem = {};
            $(".pabrik").autocomplete({
               
            minLength: 2,
            appendTo:'#ajax-modal',
            source: function( request, response ) {
            var term = request.term;
            if ( term in cacheItem ) {
            response( cacheItem[ term ] );
            return;
            }
            $.getJSON( __base_url__+'pabrik/service_rest/get_detail_pabrik_by_name/'+term, request, function( data, status, xhr ) {
            cacheItem[ term ] = data;
            response( data );

            });
            },
            select: function (event, ui) {
                return false;
            },
            focus: function (event, ui) {
            $("input[name='item_pabrik_id']").val(ui.item.id);    
            $('.pabrik').val(ui.item.label);
            return false;
            },
            }).on("focus", function () {
                $(this).autocomplete("search", '');
            });

        } 

        <!-- script modal-->
        var UIModals = function () {
            //function to initiate bootstrap extended modals
            var initModals = function () {
                $.fn.modalmanager.defaults.resize = true;
                $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
                    '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                    '<div class="progress progress-striped active">' +
                    '<div class="progress-bar" style="width: 100%;"></div>' +
                    '</div>' +
                    '</div>';
                var $modal = $('#ajax-modal');


                $('#add').on('click', function () {
                    // create the backdrop and wait for next modal to be triggered
                    $('body').modalmanager('loading');

                        
                    setTimeout(function () {
                        $modal.load(__base_url__+'admin/'+__module__+'/add', '', function () {
                            $modal.modal();
                           // Main.init();
                            FormValidator.init();
                            input_keyboard();   
                            currency_rupiah();
                        });
                    }, 100);
                });

                 
                
            };
            return {
                init: function () {
                    initModals();

                }
            };
        }();

        function add_modal()
        {
            var $modal = $('#ajax-modal');
            // create the backdrop and wait for next modal to be triggered
            $('body').modalmanager('loading');

                
            setTimeout(function () {
                $modal.load(__base_url__+'admin/'+__module__+'/add', '', function () {
                    $modal.modal();
                 //   Main.init();
                    FormValidator.init();
                    input_keyboard();   
                    currency_rupiah();
                });
            }, 100);   
        }

        function edit_modal(id)
        {

                var $modal = $('#ajax-modal');
                    // create the backdrop and wait for next modal to be triggered
                    $('body').modalmanager('loading');

                        
                    setTimeout(function () {
                        $modal.load(__base_url__+'admin/'+__module__+'/update/'+id, '', function () {
                            $modal.modal();
                         //   Main.init();
                            FormValidator.init();
                            input_keyboard();   
                            currency_rupiah();
                        });
                    }, 100);
        }

        function post_transaction(form_id,initsession)
        {
             // var formData = new FormData('form#'+form_id);
            if(form_id=='form-third')
            {
                $("button[name='button_third']").text('loading...');    
            }
            var jqxhr=$.ajax({
                url:__base_url__+__module__+'/service_rest/save_item/'+initsession,
                type:'post',
                dataType:'json',
             //   cache:false,
             //   contentType: false,
             //   processData: false,
                data:$('form#'+form_id).serializeArray(),
                //data:formData,
            });

            jqxhr.success(function(response){
                if(response['status']==200 && response['item_id']!='')
                {
                    alert('Success');
                    window.location.href=__base_url__+'admin/'+__module__+'/index';
                }
                else
                {
                    $("button[name='button_third']").html("Save <i class=\"fa fa-arrow-circle-right\"></i>");
                }
            });
            jqxhr.error(function(response){
                alert('An error has occurred, please try again.');
                 $('a[href="#panel_first"]').tab('show');
                 return false;
            });
        }

        function confirm_session(form_id,initsession)
        {
             // var formData = new FormData('form#'+form_id);
            if(form_id=='form-third')
            {
                $("button[name='button_third']").text('loading...');    
            }
            var jqxhr=$.ajax({
                url:__base_url__+__module__+'/service_rest/confirm_session/'+initsession,
                type:'post',
                dataType:'json',
             //   cache:false,
             //   contentType: false,
             //   processData: false,
                data:$('form#'+form_id).serializeArray(),
                //data:formData,
            });

            jqxhr.error(function(response){
                alert('An error has occurred, please try again.');
                 $('a[href="#panel_first"]').tab('show');
                 return false;
            });
        }

        function check_default_kemasan(element)
        {
            //alert('sss');
            //console.log(element);
            if(element.prop('checked')==true)
            {
                $(".container_container_satuan").find('input:radio').attr('checked',false);
            //    alert('sssssss');
                element.attr('checked',true);
            }
        }

        //form validation
        var FormValidator = function () {
                // function to initiate Validation Sample 1
                var runValidator1 = function () {
                    var form1 = $('#form-first');
                    var errorHandler1 = $('.errorHandler', form1);
                    var successHandler1 = $('.successHandler', form1);
                    
                    $("button[name='button_first']").click(function(){
                        if(form1.valid()==false)
                        {
                            form1.validate().focusInvalid();
                        }
                        else
                        {
                            post_transaction('form-first','form-first');
                            $('a[href="#panel_second"]').tab('show');
                        }
                        return false;
                    });
                     $("button[name='button_second']").click(function(){
                        if(form1.valid()==false)
                        {
                            
                            $('a[href="#panel_first"]').tab('show');
                            form1.validate().focusInvalid();
                        }
                        else
                        {
                            //console.log(validate_konversi()+' konversi');    
                            if(validate_konversi()===true)
                            {
                                post_transaction('form-second','form-second');
                                $('a[href="#panel_third"]').tab('show');   
                            }
                        }
                        return false;
                    });
                     $("button[name='button_third']").click(function(){
                        if(form1.valid()==false)
                        {
                            $('a[href="#panel_first"]').tab('show');
                            form1.validate().focusInvalid();
                        }
                        else
                        {
                            if(confirm('Save?'))
                            {
                                confirm_session('form-first','form-first');
                                confirm_session('form-second','form-second');    
                                
                                post_transaction('form-third','form-third');
                            }
                        }
                        return false;
                    });

                    $('#form-first').validate({
                        errorElement: "span", // contain the error msg in a span tag
                        errorClass: 'help-block',
                        errorPlacement: function (error, element) { // render error placement for each input type
                            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                                error.insertAfter($(element).closest('.form-group').children('div').children().last());
                            } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                                error.insertAfter($(element).closest('.form-group').children('div'));
                            } else {
                                error.insertAfter(element);
                                // for other inputs, just perform default behavior
                            }
                        },
                        ignore: "",
                        rules: {
                            item_nama: {
                                minlength: 2,
                                required: true
                            },
                            item_kekuatan: {
                                required: true
                            },
                            item_satuan_id: {
                                required: true
                            },
                            item_hna: {
                                required: true
                            },
                            item_stock_minimal: {
                                required: true
                            },
                            item_hna: {
                                required: true
                            },
                            item_margin_resep: {
                                required: true
                            },
                            item_margin_non_resep: {
                                required: true
                            },
                            item_harga_jual_resep: {
                                required: true
                            },
                            item_harga_jual_non_resep: {
                                required: true
                            },
                        },
                        messages: {
                            //item_name: "Mohon isikan nama item",
                        },

                        invalidHandler: function (event, validator) { //display error alert on form submit
                            successHandler1.hide();
                            errorHandler1.show();
                        },
                        highlight: function (element) {
                            $(element).closest('.help-block').removeClass('valid');
                            // display OK icon
                            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                            // add the Bootstrap error class to the control group
                        },
                        unhighlight: function (element) { // revert the change done by hightlight
                            $(element).closest('.form-group').removeClass('has-error');
                            // set error class to the control group
                        },
                        success: function (label, element) {
                            label.addClass('help-block valid');
                            // mark the current input as valid and display OK icon
                            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                        },
                        submitHandler: function (form) {
                            successHandler1.show();
                            errorHandler1.hide();
                            // submit form
                            //$('#form').submit();
                        }
                    });
                };
                return {
                    //main function to initiate template pages
                    init: function () {
                        runValidator1();
                    }
                };
            }();
     

            jQuery(document).ready(function() {
                UIModals.init();
            });


            //kelas terapi
             function get_kelas_terapi(farmako_terapi_id_val,elem_name,selected_id)
               {
                   // $("select[name='sp_detail_id']").bind('')
//                   console.log(area_id_val);
//                   console.log(elem_name);
                     $.ajax({
                       url:__base_url__+'item/service_rest/get_kelas_terapi',
                       type:'get',
                       dataType:'json',
                       data:{farmako_terapi_id:farmako_terapi_id_val},
                       success:function(response){
                           switch(elem_name)
                           {
                               case 'item_farmako_terapi_id':
                                   elem=$("select[name='item_kelas_terapi_id']");
                                   elem_label='Pilih Kelas Terapi';
                                   current_id=selected_id;
                               break;    
                                    
                               
                           }
                           elem.html('<option value=""> '+elem_label+' </option>');
                           $.each(response,function(i,rowArr){
                               selected=(current_id==rowArr['kelas_terapi_id'])?'selected':'';
                               elem.append('<option value="'+rowArr['kelas_terapi_id']+'" '+selected+'>'+rowArr['kelas_terapi_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
               }
                function get_all_satuan(number_index,selected_id)
               {
                     $.ajax({
                       url:__base_url__+'item/service_rest/get_all_satuan',
                       type:'get',
                       dataType:'json',
                       success:function(response){
                           elem=$("div.container_satuan_"+number_index).find("select[name='item_satuan_besar_satuan_id["+number_index+"]']");
                           elem_label='Pilih Satuan Besar';
                           current_id=selected_id;
                   
                           elem.html('<option value=""> '+elem_label+' </option>');
                           $.each(response,function(i,rowArr){
                               selected=(current_id==rowArr['satuan_id'])?'selected':'';
                               elem.append('<option value="'+rowArr['satuan_id']+'" '+selected+'>'+rowArr['satuan_name']+'</option>');
                           });

                           elem=$("div.container_satuan_"+number_index).find("select[name='item_satuan_kecil_satuan_id["+number_index+"]']");
                           elem_label='Pilih Satuan Kecil';
                           current_id=selected_id;
                   
                           elem.html('<option value=""> '+elem_label+' </option>');
                           $.each(response,function(i,rowArr){
                               selected=(current_id==rowArr['satuan_id'])?'selected':'';
                               elem.append('<option value="'+rowArr['satuan_id']+'" '+selected+'>'+rowArr['satuan_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
               }