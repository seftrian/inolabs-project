<?php
$base_url                     = "http://".(isset($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:''). preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
$themeUrl     = $base_url.'themes/admin/app/inc/';
$config_name='Inolabs Apotek';
include('setup_service.php');

$setup_service=new setup_service;
$setup_service->check_file_identity();
?>
<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title><?php echo $config_name;?></title>
        
                <!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/fonts/style.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/main-responsive.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/theme_light.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/print.css" type="text/css" media="print"/>
		<!--[if IE 7]>
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="login example2">
		<div class="main-login col-sm-4 col-sm-offset-4">
			<div class="logo"><?php echo $config_name;?></div>
			<!-- start: LOGIN BOX -->
			<div class="box-login">
				<h3>Konfigurasi Pengguna</h3>
			
				<div class="errorHandler alert alert-danger no-display"></div>
				<div class="successHandler alert alert-success no-display"></div>

				<form class="form-setup" action="" method="post">
			
					<fieldset>
					
							<div class="form-group">
							<span class="input-icon">
                                                            <input onchange="remove_space($(this));" type="text" value="" name="username" class="form-control username" placeholder="Username Login" />
                                                                
								<i class="fa fa-user"></i> </span>
						</div>
						<div class="form-group">
							<span class="input-icon">
                                                            <input id="password" type="password" value="" name="password" class="form-control" placeholder="Password Login" />
                                                                
								<i class="clip-key-3"></i> </span>
						</div>
						<div class="form-group">
							<span class="input-icon">
                                                            <input type="password" value="" name="password_repeat" class="form-control" placeholder="Ulangi Password" />
                                                                
								<i class="clip-key-3"></i> </span>
						</div>
				
						<p>
							Isikan detil akun di bawah:
						</p>
						
						<div class="form-group">
						<span class="input-icon">
								<input type="text" class="form-control" name="app_name" placeholder="Nama Aplikasi">
						<i class="clip-home"></i> </span>
						</div>

						<div class="form-group">
							<span class="input-icon">
								<input type="email" class="form-control" name="email" placeholder="Email">
								<i class="fa fa-envelope"></i> </span>
						</div>
						
						<div class="form-group">
						<span class="input-icon">
								<input type="text" class="form-control" name="telephone" placeholder="No. Hp.">
						<i class="fa fa-mobile"></i> </span>
						
						</div>

						<div class="form-actions">
						

							<button onclick="if(confirm('Simpan?')){save_configuration();}return false;" type="submit" class="btn btn-success pull-right">
								Lanjutkan <i class="fa fa-arrow-circle-right"></i>
							</button>
							<a href="<?php echo $base_url?>">Login</a>
						</div>
						
					</fieldset>
				</form>
				

			</div>
			<!-- end: LOGIN BOX -->

			<!-- start: REGISTER BOX -->
			
                        
			<!-- end: REGISTER BOX -->
			<!-- start: COPYRIGHT -->
			<div class="copyright">Powered by <a href="http://inolabs.net">INOLABS.NET</a>
			</div>
			<!-- end: COPYRIGHT -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="<?php echo $base_url?>addons/jquery/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
			jQuery(document).ready(function() {
				$(".username").focus();
input_keyboard();


							    var form = $('.form-setup');
					        var errorHandler = $('.errorHandler', form);
					        form.validate({
					            rules: {
					            		username: {
					                    minlength: 4,
					                    required: true
					                },
					            		password: {
					                    minlength: 4,
					                    required: true
					                },
					                password_repeat: {
					                    minlength: 4,
					                    required: true,
                    					equalTo: "#password"
					                },
					                 app_name: {
					                    minlength: 4,
					                    required: true
					                },
					                 email: {
					                    minlength: 4,
					                    required: true,
					                     email: true
					                },
					                 telephone: {
					                    required: true
					                },

					            },
					            submitHandler: function (form) {
					                errorHandler.hide();
					                form.submit();
					            },
					            invalidHandler: function (event, validator) { //display error alert on form submit
					                errorHandler.show();
					            }
					        });

	        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a small tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error');
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').addClass('has-error');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            }
        });
			});

			function save_configuration()
			{
				$("button").html('proses...');
				$("button").attr('disabled',true);
				var jqxhr=$.ajax({
					url:'<?php echo $base_url?>setup_service.php?route=save_user',
					type:'post',
					dataType:'json',
					data:$("form.form-setup").serializeArray()
				});
				jqxhr.success(function(response){
						if(response['status']==200)
						{
							$(".errorHandler").addClass('no-display');
							$(".successHandler").removeClass('no-display');
							$(".successHandler").html(response['message']);

							$("button").html('Lanjutkan <i class="fa fa-arrow-circle-right"></i>');
							$("button").attr('disabled',false);
							window.location.href='<?php echo $base_url?>index.php';
						}
						else
						{
							$(".errorHandler").removeClass('no-display');
							$(".successHandler").addClass('no-display');
							$(".errorHandler").html(response['message']);

							$("button").html('Lanjutkan <i class="fa fa-arrow-circle-right"></i>');
							$("button").attr('disabled',false);

						}
				});
				jqxhr.error(function(){
					$("button").html('Lanjutkan <i class="fa fa-arrow-circle-right"></i>');
							$("button").attr('disabled',false);
					alert('an error has occurred, please try again.');
				});				
			}

			function input_keyboard()
            {
            	  $('.form-control').bind("keydown", function(e) {
            	  	//alert(e.which);
            	  		if(e.altKey && ( e.which === 37 ) )
            	  		{
            	  			e.preventDefault(); //Skip default behavior of the enter key
	                    var n = $(".form-control").length;

                  		var nextIndex = $('.form-control').index(this) - 1;
                  		if(nextIndex < n && nextIndex>=0)
                      {
                          $('.form-control')[nextIndex].focus();
                      }

            	  		}
                    if (e.which == 13 || e.altKey && ( e.which === 39 ) ) 
                    { //Enter key
                    e.preventDefault(); //Skip default behavior of the enter key
                    var n = $(".form-control").length;

                    var nextIndex = $('.form-control').index(this) + 1;
                        if(nextIndex < n)
                        {
                            $('.form-control')[nextIndex].focus();
                        }
                        else
                        {
                            $('.form-control')[nextIndex-1].blur();
                           
																//$('button[type="submit"]').click();
																 if($('.form-setup').valid()==true)
																 {
																 	if(confirm('Lanjutkan ke tahap berikutnya?'))
			                            {
																		save_configuration();
			                            }
														 		  else
			                            {
			                            	$('.form-control')[n-1].focus();
			                            }
																 }

                          return false;
                        }
                    }
                });
                
            
            }


            function remove_space(elem)
            {
            		var value=elem.val().replace(/\ /g, '');

            		elem.val(value);
            		return false;
            }
		</script>
	</body>
	<!-- end: BODY -->
</html>

