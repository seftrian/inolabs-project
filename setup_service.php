<?php
date_default_timezone_set('Asia/Jakarta') ;
$setup_service=new setup_service;
$route=isset($_GET['route'])?$_GET['route']:'';

switch(strtolower($route))
{
	case 'check_connection':
	$res=$setup_service->check_connection();
	echo json_encode($res);	
	break;
	case 'save_user':
	$res=$setup_service->save_user();
	echo json_encode($res);	
	break;
	case 'save_identity':
	$res=$setup_service->save_identity();
	echo json_encode($res);	
	break;
}	

class setup_service{

		protected $dir_folder='assets';
		protected $dir;
		protected $db_file='enusoft_new_apotek_free.sql';
		protected $identity_file='identity.json';
		protected $base_url;
		public function __construct()
		{
				$directory=dirname(__FILE__).'/';
				$this->dir=$directory.$this->dir_folder.'/';
				
				$this->base_url="http://".(isset($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:''). preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
		}

		public function check_connection()
		{
			$status=500;
			$message='No request, please check your fields.';
			$hostname=isset($_POST['hostname'])?addslashes($_POST['hostname']):'';
			$db_name=isset($_POST['database'])?addslashes($_POST['database']):'';
			$db_user=isset($_POST['username'])?addslashes($_POST['username']):'';
			$db_pass=isset($_POST['password'])?addslashes($_POST['password']):'';
	
			if(trim($hostname) AND trim($db_name) AND trim($db_user))
			{

				$con=@mysql_connect($hostname,$db_user,$db_pass);
				if($con)
				{
					$status=200;
					$message='Success'; 

					//cek database telah tersedia atau belum
					$sql='SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME="'.$db_name.'"';
					$exec=mysql_query($sql);
					$row=mysql_fetch_array($exec);
					if(empty($row))
					{
						 //create database
						 
						 // Create database
						$sql="CREATE DATABASE ".$db_name;
						if (mysql_query($sql)) {
							 $status=200;
							 $message='Database berhasil dibuat.';

							// Select MySQL DB
							if(mysql_select_db($db_name, $con)){
								 $resp=$this-> run_database_table($hostname,$db_name, $db_user,$db_pass);
									if($resp==true)
									{
										$status=200;
										$message='Success';
										 //create file identity
										$this->create_file_identity($hostname,$db_name,$db_user,$db_pass);

									}
									else
									{
										$status=500;
										$message='Gagal menjalankan impor database.';
									}
							}
							else
							{
								$status=500;
								$message='Gagal melakukan koneksi ke database '.$db_name.'.';
							}

						} 
						else {
	 						 $status=500;
  						 $message='Error .'.mysql_error($con);
						}

					}
					else 
					{
						 $status=500;
						 $message='Database '.$db_name.' telah tersedia. Untuk mencegah konflik, sebaiknya anda mengisikan dengan nama database lain.';
					}
				}
				else 
				{
					$status=401;
					$message='Gagal terhubung ke server. ';
				}
			}
			
			return array(
				'status'=>$status,
				'message'=>$message,
				);

		}


		/***
		fungsi untuk menghubungkan ke dbase
		*/
		public function run_database_table($host,$db_name, $user,$pass)
		{
				$file=$this->db_file;
				$script_path=$this->dir.$file;
				if(file_exists($script_path) AND trim($script_path)!='')
				{
					$status=$this->MultiQuery($script_path);
				}
				else
				{
					$status=false;
				}
				

				return $status;
		}


		/***
		fungsi untuk menjalankan query database
		*/
		public function MultiQuery($sqlfile, $sqldelimiter = ';') {
		set_time_limit(0);
		$msg='';
		if (is_file($sqlfile) === true) {
		$sqlfile = fopen($sqlfile, 'r');

			if (is_resource($sqlfile) === true) {
				$query = array();
				$msg= "<table cellspacing='3' cellpadding='3' border='0'>";

				while (feof($sqlfile) === false) {
				$query[] = fgets($sqlfile);

				if (preg_match('~' . preg_quote($sqldelimiter, '~') . '\s*$~iS', end($query)) === 1) {
				$query = trim(implode('', $query));

				if (mysql_query($query) === false) {
				  $msg.= '<tr><td>ERROR:</td><td> ' . $query . '</td></tr>';
				} else {
					$msg.= '<tr><td>SUCCESS:</td><td>' . $query . '</td></tr>';
				}

				while (ob_get_level() > 0) {
				ob_end_flush();
				}

				flush();
				}

					if (is_string($query) === true) {
						$query = array();
					}
				}
				$msg.= "</table>";

				 fclose($sqlfile);

				 return true;
			}
		}

		return false;
		}

			/**
		cek file identity.json
		*/
		public function check_connection_file_identity()
		{
				$file_id=$this->dir.$this->identity_file;
				$data=array();
				if(!file_exists($file_id) OR trim($file_id)=='')
				{
					 $status=500;
					 $message='Belum melakukan registrasi.';
				}
				else
				{
						$identity_arr=file_get_contents($this->base_url.$this->dir_folder.'/'.$this->identity_file);
						$conf= json_decode($identity_arr);

						$hostname=$conf->identity->hostname;
						$db_name=$conf->identity->database;
						$db_user=$conf->identity->username;
						$db_pass=$conf->identity->password;
						$con=@mysql_connect($hostname,$db_user,$db_pass);
						$link=@mysql_select_db($db_name,$con);

						$data=array(
							'hostname'=>$hostname,
							'database'=>$db_name,
							'username'=>$db_user,
							'password'=>$db_pass,
							);
						if($link){
							$status=200;
							$message='Aktivasi sukses. Aplikasi telah terhubung ke database.';

						}
						else
						{
							$status=401;
							$message='Kami mendeteksi anda pernah melakukan registrasi, namun aplikasi tidak dapat terhubung ke database.';
						}
				}
					
				return array(
					'status'=>$status,
					'message'=>$message,
					'data'=>$data,
					);
			
		}

		/**
		cek file identity.json
		*/
		public function check_file_identity()
		{
				$file_id=$this->dir.$this->identity_file;
				
				if(!file_exists($file_id) OR trim($file_id)=='')
				{
						//arahkan ke halaman setup
						header('location:'.$this->base_url.'setup.php');
						exit;
				}
				else
				{
						$rest_url=file_get_contents($this->base_url.$this->dir_folder.'/'.$this->identity_file);
						//$encode=json_encode($rest_url);
						$conf=json_decode($rest_url);
					
						/*
						$hostname=$conf->identity->hostname;
						$db_name=$conf->identity->database;
						$db_user=$conf->identity->username;
						$db_pass=$conf->identity->password;
						$con=@mysql_connect($hostname,$db_user,$db_pass);
						$link=@mysql_select_db($db_name,$con);
						if(!$link)
						{
							//arahkan ke halaman setup
							header('location:'.$this->base_url.'setup.php');
							exit;
						}
						*/
						return $conf;
				}
		}

		/**
		jika database berhasil dibuat, maka buat file identity json
		@param string $host
		@param string $db_name
		@param string $user
		@param string $pwd
		*/
		public function create_file_identity($host,$db_name,$user,$pwd)
		{
			$data['identity']['hostname']=$host;
			$data['identity']['database']=$db_name;
			$data['identity']['username']=$user;
			$data['identity']['password']=$pwd;
			$data['identity']['time']=date('Y-m-d H:i:s');
			$file=$this->dir.$this->identity_file;
			$fp = fopen($file, 'w');
			fwrite($fp, json_encode($data));
			fclose($fp);

		}

			/**
		jika database berhasil dibuat, maka buat file identity json
		@param string $host
		@param string $db_name
		@param string $user
		@param string $pwd
		*/
		public function save_identity()
		{

			$hostname=isset($_POST['hostname'])?addslashes($_POST['hostname']):'';
			$db_name=isset($_POST['database'])?addslashes($_POST['database']):'';
			$db_user=isset($_POST['username'])?addslashes($_POST['username']):'';
			$db_pass=isset($_POST['password'])?addslashes($_POST['password']):'';

			$data['identity']['hostname']=$hostname;
			$data['identity']['database']=$db_name;
			$data['identity']['username']=$db_user;
			$data['identity']['password']=$db_pass;
			$data['identity']['time']=date('Y-m-d H:i:s');
			$con=@mysql_connect($hostname,$db_user,$db_pass);
			$db_con=@mysql_select_db($db_name, $con);
			if(!$con)
			{
				$status=401;
				$message='gagal terhubung ke server';
			}
			else if(!$db_con)
			{
				$status=401;
				$message='gagal terhubung ke database';
			}
			else 
			{
				$status=200;
				$message='OK';

				$file=$this->dir.$this->identity_file;
				if(file_exists($file) AND trim($file)!='')
				{
					unlink($file);
				}
				$fp = fopen($file, 'w');
				fwrite($fp, json_encode($data));
				fclose($fp);
			}

			return array(
				'status'=>$status,
				'message'=>$message,
				);
		}

		public function save_user()
		{
					$conf=$this->check_file_identity();
					$hostname=$conf->identity->hostname;
					$db_name=$conf->identity->database;
					$db_user=$conf->identity->username;
					$db_pass=$conf->identity->password;
					$con=@mysql_connect($hostname,$db_user,$db_pass);

					if(!$con)
					{
						return array(
							'status'=>'401',
							'message'=>'Tidak dapat terhubung ke server',
							);
					}

					$db_=@mysql_select_db($db_name, $con);
					if(!$db_)
					{
						return array(
							'status'=>'401',
							'message'=>'Tidak dapat terhubung ke database',
							);
					}
			
					$status=500;
					$message='No request, please check your fields.';
					$username=isset($_POST['username'])?addslashes($_POST['username']):'';
					$password=isset($_POST['password'])?addslashes($_POST['password']):'';
					$password_repeat=isset($_POST['password_repeat'])?addslashes($_POST['password_repeat']):'';
					$app_name=isset($_POST['app_name'])?addslashes($_POST['app_name']):'';
					$email=isset($_POST['email'])?addslashes($_POST['email']):'';
					$telephone=isset($_POST['telephone'])?addslashes($_POST['telephone']):'';

					if(trim($username)=='' OR strlen($username)<4)
					{
						$status=500;
						$message='Username harus diisi minimal 4 karakter';
					}
					else if(trim($username)=='' OR strlen($username)<4)
					{
						$status=500;
						$message='Username harus diisi minimal 4 karakter';
					}
					else if(trim($password)=='' OR strlen($password)<4)
					{
						$status=500;
						$message='Password harus diisi minimal 4 karakter';
					}
					else if($password!=$password_repeat)
					{
						$status=500;
						$message='Password dan password ulangi harus sama';
					}
					else if(trim($app_name)=='' OR strlen($app_name)<4)
					{
						$status=500;
						$message='Nama aplikasi harus diisi minimal 4 karakter';
					}
					else if(trim($email)=='')
					{
						$status=500;
						$message='Email harus diisi';
					}
					else if(trim($telephone)=='')
					{
						$status=500;
						$message='No. HP. harus diisi';
					}
					else 
					{
							$sql='SELECT admin_group_id FROM site_administrator_group WHERE admin_group_is_active="1"
										AND admin_group_type="superuser"';
							$exec=mysql_query($sql);
							$row=mysql_fetch_array($exec);
							
							if(!empty($row))
							{
									$admin_group_id=$row['admin_group_id'];
									$sql="INSERT INTO `site_administrator` ( `admin_group_id`, `admin_username`, `admin_password`, `admin_last_login`, `admin_is_active`) VALUES ( '".$admin_group_id."', '".$username."', '".md5($password)."', '0000-00-00 00:00:00.000000', '1');";
									mysql_query($sql);

									$exec = mysql_query('SELECT LAST_INSERT_ID()');
					        $row_id=mysql_fetch_array($exec);
					        $id_user=!empty($row_id)?$row_id['LAST_INSERT_ID()']:0;

									//app name
									$sql_up='UPDATE site_configuration SET configuration_value="'.$app_name.'"
									WHERE configuration_index="config_name"';
									mysql_query($sql_up);
									//email name
									$sql_up='UPDATE site_configuration SET configuration_value="'.$email.'"
									WHERE configuration_index="config_email"';
									mysql_query($sql_up);
									//telephone
									$sql_up='UPDATE site_configuration SET configuration_value="'.$telephone.'"
									WHERE configuration_index="config_telephone"';
									mysql_query($sql_up);


									//set store
									//1 id pusat
									$sql_store="INSERT INTO `site_administrator_store` ( `administrator_store_administrator_id`, `administrator_store_store_id`) VALUES ( '".$id_user."', '1');";
									mysql_query($sql_store);

									$status=200;
									$message='Success';

							}		
							else {
								$status=500;
								$message='Akun superuser tidak ditemukan. Sila kontak tejo.murti@inolabs.net.';
							}



					}


					return array(
						'status'=>$status,
						'message'=>$message,
						);
		}
}
?>