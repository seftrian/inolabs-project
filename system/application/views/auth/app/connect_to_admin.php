<?php
$websiteConfig = $this->system_lib->getWebsiteConfiguration();
?>

<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>Login <?php echo isset($websiteConfig['config_name']) ? $websiteConfig['config_name'] : ''; ?></title>
        
                <!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/fonts/style.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/main-responsive.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/theme_light.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="<?php echo $themeUrl; ?>assets/css/print.css" type="text/css" media="print"/>
		<!--[if IE 7]>
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="login example2">
		<div class="main-login col-sm-4 col-sm-offset-4">
			<div class="logo"><?php echo $websiteConfig['config_name']?></div>
			<!-- start: LOGIN BOX -->
			<div class="box-login">
				<h3>Masuk ke Virtual Office</h3>
				<p>
					Silakan isi username dan password untuk masuk.
				</p>
				<form class="form-login" action="" method="post">
					<?php
          //untuk menampilkan pesan
          if (trim($message) != '') {

              echo $this->function_lib->show_message($message, $templateName, $themeId, $status);
          }
          ?>
					<fieldset>
						<div class="form-group">
							<span class="input-icon">
                                                            <input type="text" value="<?php echo set_value('username'); ?>" name="username" class="form-control" placeholder="Username" />
                                                                
								<i class="fa fa-user"></i> </span>
						</div>
						<div class="form-group form-actions">
							<span class="input-icon">
                                                            <input type="password" value="<?php echo set_value('password'); ?>" name="password" class="form-control password" placeholder="Password" />
                                                                
								<i class="fa fa-lock"></i>
							<!--	<a class="forgot" href="#">
									I forgot my password
								</a> 
							-->
								</span>
						</div>
						<?php
						//jika salah > 2x maka tampilkan captcha
						if(isset($_SESSION['fail_login_admin']) AND $_SESSION['fail_login_admin']>2)
						{
							?>
	  					<div class="form-group form-actions">
	                <div class="row">
	                    <div class="col-md-6">
	                        <input placeholder="Kode Unik..." class="form-control qwerty ui-keyboard-input ui-widget-content ui-corner-all" type="text" name="captcha" id="kodeunik" autocomplete="off" aria-haspopup="true" role="textbox" required/>
	                    </div>
	                    <div class="col-md-6" style="padding:0;">
	                        <a onclick="captcha_reload();return false;" class="btn btn-xs btn-danger" style="cursor:pointer;"><i class="fa fa-refresh"></i></a>
	                        <img id="captcha_image" src="<?php echo site_url('auth/captcha_admin'); ?>" alt="" style="border:0;" />
	                    </div>
	                </div>
	            </div>
							<?php
						}
						?>
				

						<div class="form-group form-actions" style="display:none">
							<select style="margin-left:20px;width:50%;float:right;" name="shift_user" class="form-control" required>
								<option value="-">Pilih Shift</option>
								<?php	
								foreach($list_master_shift AS $rowArr)
								{
									foreach($rowArr AS $variable=>$value)
									{
										${$variable}=$value;
									}
									$selected=($this->input->post('shift_user')==$shift_id)?'selected':'';
									?>
									<option value="<?php echo $shift_id?>" <?php echo $selected;?>><?php echo $shift_title?></option>
									<?php
								}
								?>
							</select>
						</div>
						<div class="form-actions">
							<!--<label for="remember" class="checkbox-inline">
								<input type="checkbox" class="grey remember" id="remember" name="remember">
								Keep me signed in
							</label>
							-->
		          <input type="hidden" name="doLogin" value="Login">

							<button type="submit" class="btn btn-bricky pull-right">
								Login <i class="fa fa-arrow-circle-right"></i>
							</button>
						</div>
						
					</fieldset>
				</form>

			</div>
			<!-- end: LOGIN BOX -->
		
			<!-- start: COPYRIGHT -->
			<div class="copyright">
				 <?php echo isset($websiteConfig['config_footer']) ? $websiteConfig['config_footer'] : ''; ?>.
                                 Powered by <a href="http://inolabs.net">INOLABS.NET</a>
			</div>
			<!-- end: COPYRIGHT -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo $themeUrl; ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php echo $themeUrl; ?>assets/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
			});
			$(function(){
				$("input:first").focus();
			});
		</script>

		<script>
			function captcha_reload()
			{
            var url = '<?php echo site_url('auth/captcha_admin'); ?>';
            $("#captcha_image").attr('src', url + '?' + Math.random());
			}
        
    </script>
	</body>
	<!-- end: BODY -->
</html>

