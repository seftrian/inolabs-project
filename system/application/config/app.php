<?php

/**
  untuk mendapatkan domain
  @param string $url
 */
if (!function_exists('get_domain')) {

    function get_domain($url) {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 *
 *
 * Disini untuk mengatur nama project, email, database dan template
 */

$config = array();
//setting application
$config['main_domain'] = 'inofarma.com';
$config['database_prefix'] = 'inofacom';
$config['project_owner'] = 'tejomurti';
$config['project_name'] = 'Inolabs Apotek';
$config['email'] = 'tejo.murti@inolabs.net';


$config['base_url'] = "http://" . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . preg_replace('@/+$@', '', dirname($_SERVER['SCRIPT_NAME'])) . '/';

$current_domain = $config['base_url'];
$current_domain = get_domain($current_domain);
$current_domain = ($current_domain == '') ? 'localhost' : $current_domain;


//setting database online
// $db['default']['hostname']              = 'localhost';
// $db['default']['username']              = 'fikaicom_inolabs';
// $db['default']['password']              = 'inolabs2014';
// $db['default']['database']              = 'fikaicom_front_live';

/*
 * setting database localhost yudha
 * @author yudha
 */
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'tomsfront';


//setting backend theme
$config['theme']['backend']['name'] = 'app';
$config['theme']['backend']['url'] = $config['base_url'] . 'themes/admin/' . $config['theme']['backend']['name'] . '/';

//setting frontend theme
$config['theme']['frontend']['name'] = 'newiaidiy'; //bootstrap less
$config['theme']['frontend']['url'] = $config['base_url'] . 'themes/' . $config['theme']['frontend']['name'] . '/views/layouts';
