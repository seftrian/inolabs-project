<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!file_exists(dirname(__FILE__) . '/app.php')) {
    die('file app.php tidak ditemukan');
}

include dirname(__FILE__) . '/app.php';
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

//$route['default_controller'] = "mlm/f_registration/index";
$route['default_controller'] = "master_content/f_master_content/default_controller";
$route['auth/admin'] = "auth/connect_to_admin";
$route['auth/connect_to_admin'] = "auth/connect_to_admin";
$route['auth/logout'] = "auth/logout";
$route['admin/([a-zA-Z_-]+)/(:any)'] = "$1/admin/$2";
$route['admin/([a-zA-Z_-]+)'] = "$1/admin/index";

$route['vo/([a-zA-Z_-]+)/(:any)'] = "$1/vo/$2";
$route['vo/([a-zA-Z_-]+)'] = "$1/vo/index";



if (isset($_SERVER['REQUEST_URI'])) {
    $explode = explode('/', $_SERVER['REQUEST_URI']);
    $special_uri2_arr = array();
    if (!empty($explode)) {

        @mysql_connect($db['default']['hostname'], $db['default']['username'], $db['default']['password']);
        @mysql_select_db($db['default']['database']);

        $num = 1;
        for ($i = 0; $i < count($explode); $i++) {
            if (isset($explode[$i]) AND trim($explode[$i]) != '') {
                $num++;
            }
        }

        $selisih = $num - 3;
        $uri_1 = '-';
        if (isset($explode[$selisih]) AND $explode[$selisih] == 'content') {
            $uri_1 = $explode[$selisih];
            $sql = 'SELECT category_permalink FROM site_news_category WHERE category_permalink!=""';
            $exec = mysql_query($sql);

            $no = 1;
            while ($row = mysql_fetch_array($exec)) {
                $title = $row['category_permalink'];
                $special_uri2_arr[$no] = $title;
                $no++;
            }
        }

        //special uri ke 2 untuk module
        $selisih = ($selisih > 0) ? $selisih : 0;
        $special_module = 'master_' . $explode[$selisih];
        $num = 1;
        for ($i = 0; $i < count($explode); $i++) {
            if (isset($explode[$i]) AND trim($explode[$i]) != '') {
                $num++;
            }
        }

        $selisih = $num - 2;
        if (isset($explode[$selisih]) AND array_search($explode[$selisih], $special_uri2_arr)) {
            //set routing by special uri
            $route[$uri_1 . '/' . $explode[$selisih] . '/(:any)'] = $special_module . "/f_" . $special_module . "/detail/$1";
        }

//        echo '<pre>';
//        print_r($route);
//        exit;
    }
}

$route['cron/([a-zA-Z_-]+)/(:any)'] = "_cron/execute/$1/$2";
$route['cron/([a-zA-Z_-]+)'] = "_cron/execute/$1";
// link membuat event

/*
 * link membuat contact
 * @author yudha
 */

/*
 * menu product
 * @author yudha
 */

$route['contact'] = "master_content/f_master_content/contact";
// marketing plan, kode etik, info
// Info
$route['info'] = "master_content/f_master_content/info";
$route['info/get-data-json-info'] = "master_content/f_master_content/getJsonFileInfo";
// kode etik
$route['kode-etik'] = "master_content/f_master_content/kode_etik";
$route['kode-etik/get-data-json-file-kode-etik'] = "master_content/f_master_content/getJsonFileEtik";
// mark plan
$route['mark-plan'] = 'master_content/f_master_content/mark_plan';
$route['mark-plan/get-data-json-file-mark-plan'] = 'master_content/f_master_content/markPlan';
// list produk
$route['product'] = "master_content/f_master_content/product";
$route['json-list-produk'] = "master_content/f_master_content/jsonListProduk";
$route['product/detail/(:any)'] = "master_content/f_master_content/detail_product/$1";
$route['product/json-data-detail-product_id'] = "master_content/f_master_content/json_data_detail_product_id";


$route['page/(:any)/(:any)'] = "master_content/f_master_content/$1/$2/$3";
$route['404_override'] = '';
/* End of file routes.php */
/* Location: ./application/config/routes.php */