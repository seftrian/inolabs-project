<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller
{

    protected $mainModel;
    //put your code here
    public function __construct()
    {
        parent::__construct();
        //load model menu

        $this->load->model(array('master_bank_lib'));


        $this->mainModel = new master_bank_lib;

        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }

    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {

        //status
        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template



        $this->data['seoTitle'] = 'Bank';
        $description = 'Daftar Bank';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => $this->data['seoTitle'],
            'class' => "clip-book",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);

        template($this->themeId,  get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk menambahkan data
     */
    public function add()
    {

        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';
        if ($this->input->post('save')) {
            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation();
            $status = $isValidationPassed['status'];
            $message = $isValidationPassed['message'];

            if ($status == 200) {

                $columns = array(
                    'bank_name' => $this->input->post('bank_name', true),
                    'bank_account_number' => $this->input->post('bank_account_number', true),
                    'bank_account_owner' => $this->input->post('bank_account_owner', true),
                    'bank_account_branch' => $this->input->post('bank_account_branch', true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved = $this->mainModel->save();
                $id = $this->function_lib->insert_id();
                $status = $hasSaved['status']; //status saat penyimpanan
                if ($status == 200) {

                    //statrt audit trail
                    $admin_username = isset($_SESSION['admin']['detail']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                    $name = $this->function_lib->get_one('bank_name', $this->mainModel->getMainTable(), $this->mainModel->primaryId . '=' . intval($id));
                    $description = $admin_username . ' menambahkan bank ' . $name . ' pada tgl ' . date('Y-m-d H:i:s');
                    $table = $this->mainModel->getMainTable();
                    $column_primary = $this->mainModel->primaryId;
                    $primary_value = $id;
                    function_lib::save_audit_trail($description, $admin_username, $table, $column_primary, $primary_value);
                    //end audit trail

                    //handle logo
                    $logo = $this->mainModel->upload_more_images($id);

                    if ($logo['status'] != 200) {
                        $status = $logo['status'];
                        $message = $logo['message'];
                        $msg = base64_encode('Terjadi error saat upload file. ' . $message);
                        redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=500&msg=' . $msg);
                    } else {
                        $msg = base64_encode('Data berhasil disimpan');
                        redirect(base_url() . 'admin/' . $this->currentModule . '/index?status=200&msg=' . $msg);
                    }
                }
            }
        }
        $this->data['status'] = $status;
        $this->data['message'] = $message;
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Tambah Bank';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Bank',
            'class' => "clip-book",
            'link' => base_url() . 'admin/' . $this->currentModule . '/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => $this->data['seoTitle'],
            'class' => "clip-pencil",
            'link' => '',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);

        template($this->themeId,  get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk update data group via form
     * @param int $id
     */
    public function update($id = 0)
    {

        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }


        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';
        if ($this->input->post('save')) {

            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation($id);
            $status = $isValidationPassed['status'];
            $message = ($status == 500 and $isValidationPassed['message'] == '') ? validation_errors() : $isValidationPassed['message'];

            if ($status == 200) {

                $columns = array(
                    'bank_name' => $this->input->post('bank_name', true),
                    'bank_account_number' => $this->input->post('bank_account_number', true),
                    'bank_account_owner' => $this->input->post('bank_account_owner', true),
                    'bank_account_branch' => $this->input->post('bank_account_branch', true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved = $this->mainModel->save($where = 'bank_id=' . intval($id));
                $status = $hasSaved['status']; //status saat penyimpanan
                if ($status == 200) {

                    //statrt audit trail
                    $admin_username = isset($_SESSION['admin']['detail']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                    $name = $this->function_lib->get_one('bank_name', $this->mainModel->getMainTable(), $this->mainModel->primaryId . '=' . intval($id));
                    $description = $admin_username . ' mengubah bank ' . $name . ' pada tgl ' . date('Y-m-d H:i:s');
                    $table = $this->mainModel->getMainTable();
                    $column_primary = $this->mainModel->primaryId;
                    $primary_value = $id;
                    function_lib::save_audit_trail($description, $admin_username, $table, $column_primary, $primary_value);
                    //end audit trail

                    //handle logo
                    $logo = $this->mainModel->upload_more_images($id);

                    if ($logo['status'] != 200) {
                        $status = $logo['status'];
                        $message = $logo['message'];

                        $msg = base64_encode('Terjadi error saat upload file. ' . $message);
                        redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=500&msg=' . $msg);
                    } else {
                        $msg = base64_encode('Data berhasil disimpan');
                        redirect(base_url() . 'admin/' . $this->currentModule . '/index?status=200&msg=' . $msg);
                    }
                }
            }
        }
        $this->data['status'] = $status;
        $this->data['message'] = $message;
        $dataArr = $this->mainModel->find('bank_id=' .  intval($id));
        //cek ketersediaan group admin
        if (empty($dataArr)) {
            show_404();
        }

        foreach ($dataArr as $variable => $value) {
            $this->data[$variable] = $value;
        }
        $image = $dataArr['bank_image'];
        $getImagePath = $this->mainModel->getImagePath();
        $logo_name = (trim($image) == '') ? '-' : pathinfo($image, PATHINFO_FILENAME);
        $logo_ext =  pathinfo($image, PATHINFO_EXTENSION);
        $logo_file = $logo_name . '.' . $logo_ext;
        $pathImgUrl = $getImagePath['pathUrl'];
        $pathImgLoc = $getImagePath['pathLocation'];
        $imgProperty = array(
            'width' => 150,
            'height' => 150,
            'imageOriginal' => $logo_file,
            'directoryOriginal' => $pathImgLoc,
            'directorySave' => $pathImgLoc . '150150/',
            'urlSave' => $pathImgUrl . '150150/',


        );
        $this->data['img'] = $this->function_lib->resizeImageMoo($imgProperty);


        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Ubah Bank';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Asuransi',
            'class' => "clip-book",
            'link' => base_url() . 'admin/' . $this->currentModule . '/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => $this->data['seoTitle'],
            'class' => "clip-pencil",
            'link' => '',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);

        template($this->themeId,  get_class() . '/' . __FUNCTION__, $this->data);
    }


    /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
     * @param string $method
     * @param array $extraVariable
     * **/
    protected function footerScript($method = '', $extraVariable = array())
    {

        //set variable
        if (!empty($extraVariable)) {
            extract($extraVariable);
        }

        ob_start();

?>

        <link rel="stylesheet" href="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

        <link rel="stylesheet" href="<?php echo $this->themeUrl; ?>assets/plugins/datepicker/css/datepicker.css">
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autoNumeric.js"></script>
        <script type="text/javascript">
            function currency_rupiah() {
                var myOptions = {
                    aSep: '.',
                    aDec: ','
                };
                $('.currency_rupiah').autoNumeric('init', myOptions);
            }

            $(document).ready(function() {
                currency_rupiah();
            });


            function switch_input(elem) {

                if (elem.hasClass('pagu') == true) {
                    $(".reimburse").val('');
                } else if (elem.hasClass('reimburse')) {
                    $(".pagu").val('');
                }
            }
        </script>
        <?php
        switch ($method) {
            case 'index':
        ?>
                <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            {
                                display: 'No',
                                name: 'no',
                                width: 30,
                                sortable: false,
                                align: 'right'
                            },
                            {
                                display: 'Edit',
                                name: 'edit',
                                width: 40,
                                sortable: false,
                                align: 'center'
                            },
                            {
                                display: 'Hapus',
                                name: 'delete',
                                width: 40,
                                sortable: false,
                                align: 'center'
                            },

                            {
                                display: 'Nama Bank',
                                name: 'name',
                                width: 260,
                                sortable: false,
                                align: 'center'
                            },
                            {
                                display: 'Nomor Rekening',
                                name: 'number',
                                width: 160,
                                sortable: false,
                                align: 'right'
                            },
                            {
                                display: 'Nama Pemilik',
                                name: 'owner',
                                width: 160,
                                sortable: false,
                                align: 'center'
                            },
                            {
                                display: 'Cabang',
                                name: 'branch',
                                width: 160,
                                sortable: false,
                                align: 'center'
                            },
                            {
                                display: 'Logo',
                                name: 'image',
                                width: 160,
                                sortable: false,
                                align: 'center'
                            },
                            {
                                display: 'Tanggal',
                                name: 'timestamp',
                                width: 130,
                                sortable: false,
                                align: 'center'
                            },

                        ],
                        buttons: [{
                                display: 'Input Data',
                                name: 'add',
                                bclass: 'add',
                                onpress: add
                            },
                            {
                                separator: true
                            },
                            {
                                display: 'Pilih Semua',
                                name: 'selectall',
                                bclass: 'selectall',
                                onpress: check
                            },
                            {
                                separator: true
                            },
                            {
                                display: 'Batalkan Pilihan',
                                name: 'selectnone',
                                bclass: 'selectnone',
                                onpress: check
                            },
                            {
                                separator: true
                            },

                            {
                                display: 'Hapus Item Terpilih',
                                name: 'delete',
                                bclass: 'delete',
                                onpress: act_delete
                            },
                        ],
                        buttons_right: [
                            //                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],
                        searchitems: [{
                                display: 'Nama Bank',
                                name: 'bank_name',
                                type: 'text',
                                isdefault: true
                            },
                            {
                                display: 'Nomer Rekening',
                                name: 'bank_account_number',
                                type: 'text',
                                isdefault: false
                            },
                            {
                                display: 'Nama Pemilik',
                                name: 'bank_account_owner',
                                type: 'text',
                                isdefault: false
                            },
                            {
                                display: 'Cabang',
                                name: 'bank_account_branch',
                                type: 'text',
                                isdefault: false
                            },
                        ],
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false,
                        nowrap: false,
                    });

                    function act_delete(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if ($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if (conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url() . $this->currentModule; ?>/service_rest/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if (response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error: function() {
                                        alert('an error has occurred, please try again');
                                        grid_reload();
                                    }
                                });
                            }
                        }
                    }



                    $(document).ready(function() {
                        grid_reload();
                    });



                    function grid_reload() {
                        $("#gridview").flexOptions({
                            url: '<?php echo base_url() . $this->currentModule; ?>/service_rest/get_data'
                        }).flexReload();
                    }

                    function delete_transaction(id) {
                        if (confirm('Delete?')) {
                            var jqxhr = $.ajax({
                                url: '<?php echo base_url() . $this->currentModule ?>/service_rest/delete/' + id,
                                type: 'get',
                                dataType: 'json',

                            });
                            jqxhr.success(function(response) {
                                if (response['status'] != 200) {
                                    alert(response['message']);
                                }
                                grid_reload();
                                return false;

                            });
                            jqxhr.error(function() {
                                alert('an error has occurred, please try again.');
                                grid_reload();
                                return false;
                            });
                        }
                        return false;

                    }
                </script>
<?php
                break;
        }

        $footerScript = ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>