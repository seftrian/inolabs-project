<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('master_bank_lib');
        $this->mainModel=new master_bank_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }


    function get_data() {

        $pathImgUrl=$this->pathImgArr['pathUrl'];
        $pathImgLoc=$this->pathImgArr['pathLocation'];
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";
        
        $params['where'] = "
            1
        ";
      
        $params['order_by'] = "
            bank_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $logo_name= (trim($bank_image)=='')?'-': pathinfo($bank_image,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($bank_image,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',


            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$bank_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete='<a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$bank_id.'\');"><i class="clip-remove"></i></a>';
            $entry = array('id' => $bank_id,
                'cell' => array(
                    'no' =>  $no,
                    'edit' =>  $edit,
                    'delete' =>  $delete,
                    'name' =>  $bank_name,
                    'number' =>  $bank_account_number,
                    'owner' =>  $bank_account_owner,
                    'branch' =>  $bank_account_branch,
                    'image' =>  '<img src="'.$img.'" />',
                    'timestamp' =>  convert_datetime($bank_timestamp),
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
    public function delete($id='') {
        $arr_output=array();
        $arr_output['message'] =  ' Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 200;
        $additional_where=' '; //default telah ngecek data dihapus
        if($this->available_data($id,$additional_where))
        {
            //statrt audit trail
            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
            $name=$this->function_lib->get_one('bank_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
            $description=$admin_username.' menghapus bank '.$name.' pada tgl '.date('Y-m-d H:i:s');
            $table=$this->mainModel->getMainTable();
            $column_primary=$this->mainModel->primaryId;
            $primary_value=$id;
            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
            //end audit trail                
            //delete detail
            $this->mainModel->setMainTable($this->mainModel->getMainTable());
            $this->mainModel->delete('bank_id='.intval($id));
            $arr_output['message'] =  ' Data berhasil dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }


     public function act_delete() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' '; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('bank_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                            $description=$admin_username.' menghapus bank '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary=$this->mainModel->primaryId;
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail
    
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                           
                            $this->mainModel->delete('bank_id='.intval($id));

                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

    /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one($this->mainModel->primaryId,$this->mainModel->getMainTable(),
            $this->mainModel->primaryId.'='.intval($id));
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }
}
