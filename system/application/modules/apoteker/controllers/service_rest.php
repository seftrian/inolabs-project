<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('apoteker_lib');
        $this->mainModel=new apoteker_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
        $this->person_type=3; //apoteker
    }

    function load_data_person_by_id($id)
    {
        $this->mainModel->setMainTable('sys_person');
        $join=array(
        '  LEFT JOIN sys_person_address ON person_id=person_address_person_id',
        '  LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id'
        );
      
        $dataArr=$this->mainModel->findCustom("person_id=".  intval($id)."  AND person_was_deleted='N'
            AND person_address_is_active='Y'
            AND person_address_was_deleted='N'
            AND additional_info_is_active='Y'
            AND additional_info_was_deleted='N' ",$join);

        if(!empty($dataArr))
        {
            $status=200;
            $message='Data tersedia';
        }
        else
        {
            $status=500;
            $message='Data tidak ditemukan.';
        }

        echo json_encode(
                array(
                    'status'=>$status,
                    'message'=>$message,
                    'data'=>$dataArr,
                )
            );
    }

    function get_data_person() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*,
            sys_person_address.*,
            sys_person_additional_info.*
        ";
        $params['join'] = "
            LEFT JOIN sys_person_address ON person_id=person_address_person_id
            LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id
        ";
          $where='
        AND person_id NOT IN (SELECT person_type_detail_person_id FROM sys_person_type_detail WHERE 
         person_type_detail_person_type_id='.intval($this->person_type).')
        ';
        $params['where'] = "
            person_was_deleted='N'
            AND person_address_is_active='Y'
            AND person_address_was_deleted='N'
            AND additional_info_is_active='Y'
            AND additional_info_was_deleted='N'
        ".$where;
      
        $params['order_by'] = "
            person_full_name ASC
        ";
        $params['group_by'] = "
            person_id 
        ";
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
        $no++;
         
            $status=($person_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',


            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $type='<br />'.$this->mainModel->list_person_type($person_id);
            $entry = array('id' => $person_id,
                'cell' => array(
                    'actions' =>  '<a href="#" onclick="load_data_person_by_id('.$person_id.'); return false;">Pilih</a>',
                    'no' =>  $no,
                    'person_full_name' =>$person_title.', '.$person_full_name.$type,
                    'person_phone'=>$person_phone,
                    'person_address_value' =>$person_address_value,
                    'person_gender' =>$person_gender,
                    'person_birth_date' =>$person_birth_place.' / '.date("d-m-Y", strtotime($person_birth_date)),
                    'person_photo' =>'<img src="'.$img.'"></img>',
                    'person_timestamp' => date("d-m-Y H:i:s", strtotime($person_timestamp)),
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

    function get_data() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*,
            sys_person_address.*,
            sys_person_additional_info.*
        ";
        $params['join'] = "
            LEFT JOIN sys_person_address ON person_id=person_address_person_id
            LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id
        ";
        $params['where'] = "
            person_was_deleted='N'
            AND person_address_is_active='Y'
            AND person_address_was_deleted='N'
            AND additional_info_is_active='Y'
            AND additional_info_was_deleted='N'
            AND person_id IN (SELECT person_type_detail_person_id FROM sys_person_type_detail
                WHERE person_type_detail_person_type_id=".intval($this->person_type).")
        ";
      
        $params['order_by'] = "
            person_full_name ASC
        ";
        $params['group_by'] = "
            person_id 
        ";
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $status=($person_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',


            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);


            $entry = array('id' => $person_id,
                'cell' => array(
                    'actions' =>  '<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$person_id.'" title="Edit"><i class="clip-pencil"></i></a>
                     <a href="#"  class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$person_id.'\');"><i class="clip-remove"></i></a>
                    ',
                    'no' =>  $no,
                    'person_full_name' =>$person_title.', '.$person_full_name,
                    'additional_info_email_address' =>$additional_info_email_address,
                    'person_telephone'=>$person_telephone,
                    'person_phone'=>$person_phone,
                    'person_address_value' =>$person_address_value,
                   
                    'person_gender' =>$person_gender,
                    'person_blood_type' =>$person_blood_type,
                    'person_birth_date' =>$person_birth_place.' / '.date("d-m-Y", strtotime($person_birth_date)),
                    'person_photo' =>'<img src="'.$img.'"></img>',

                    'person_address_province_id' =>$this->function_lib->get_area_name($person_address_province_id),
                    'person_address_city_id' =>$this->function_lib->get_area_name($person_address_city_id),
                    'person_address_district_id' =>$this->function_lib->get_area_name($person_address_district_id),
                   
                    'person_is_active' => $status,
                    'person_timestamp' => date("d-m-Y H:i:s", strtotime($person_timestamp)),
                    'person_admin_username' => $person_admin_username,
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
    
}
