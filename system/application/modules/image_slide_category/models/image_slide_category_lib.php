<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
//require_once dirname(__FILE__).'/../components/core_menu_lib.php';

//class admin_menu extends core_menu_lib{
class image_slide_category_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_slider_category';
    protected $dir='assets/images/slider_category/';
    public function __construct() {

  //      parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));
                              
        if(!session_id())
        {
            session_start();
        }

        $this->getImagePath();
              
    }
    
      /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->dir;
        $pathLocation=FCPATH.$this->dir;

        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
    
    
    /**
     * handle upload
     */
    public function handleUpload($id,$filesName)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
      
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName);
            $status=$results['status'];
            $message=$results['message'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name)
    {
        $config=array();
        $pathImage=$this->dir;
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
             $where=array(
                'category_id'=>$id,
            );
            $column=array(
                'category_photo'=>$pathImage.$dataImage['file_name'],
            );
            $this->CI->db->update($this->mainTable,$column,$where);
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='category_id ASC')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='';
            $mainConfig = array(
                array(
                        'field'   => 'slider_category_name',
                        'label'   => 'Judul',
                        'rules'   => 'trim|required'
                    ),
                
            );
         
          $config=$mainConfig;
          /**
           * jika lokasi admin, maka module controller dan method wajib diisi
           * main config dan additional config akan digabung
           */
        
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
