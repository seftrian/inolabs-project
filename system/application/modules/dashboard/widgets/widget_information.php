<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_information extends widget_lib{
    //put your code here
    public function run()
    {
        $ci=&get_instance();
        $ci->load->model('stok_all/stok_lib');
        $ci->load->model('trx_pos/trx_pos_lib_utility');
        $ci->load->model('reminder_customer/reminder_customer_lib');
        $ci->load->model('defecta/defecta_lib');

        

        $data=array();
        $data['num_pending_stock']=$ci->stok_lib->count_pending_stock();
       // echo $data['num_pending_stock'];
        $data['dont_set_spesialis']=$this->get_num_nakes_uncomplete();
        $data['count_top_almost_or_expired']=$ci->trx_pos_lib_utility->count_top_almost_or_expired();
        $data['count_almost_or_expired_to_ro']=$ci->reminder_customer_lib->count_almost_or_expired_to_ro();
        $defecta_arr=$this->defecta_lib->get_data_defecta();
        $data['count_defecta']=$defecta_arr['total'];

        $this->render(get_class(),$data);
    }


    protected function get_num_nakes_uncomplete()
    {
        $params['table'] = 'sys_person';
        $params['select'] = '*';
        $params['where'] = 'person_was_deleted="N" AND person_is_employee="Y"';
        $params['where'].=' AND person_id IN (SELECT person_type_detail_person_id 
                                    FROM sys_person_type_detail
                                WHERE person_type_detail_person_type_id="6" AND person_type_detail_person_id=person_id)';
        $params['where'].=' AND person_id NOT IN (SELECT employee_affair_person_id 
                                    FROM sys_employee_affair 
                                WHERE employee_affair_kind_service_id!=0)';
        $total_dokter = $this->function_lib->get_query_data($params, true);

        $value=$total_dokter;
        return $value;
    }


}

?>
