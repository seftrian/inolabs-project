<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
class admin extends admin_controller {

    public $mainModel; //model utama dari module ini
    //
    //put your code here

    public function __construct() {
        parent::__construct();

        //inisialisasi model
        $this->load->model(array('actv/actv_lib'));

        parse_str($_SERVER['QUERY_STRING'], $_GET);
        $this->load->helper('date');
    }

    /**
     * menampilkan daftar data history transfer
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index() {

        $status = (isset($_GET['status']) AND trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) AND trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Dashboard';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        $this->data['session'] = auth_admin_lib::staticGetAdminSession();
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
  
        $this->data['show_notification_alert']=$this->actv_lib->show_notification_alert();
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    

     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        switch($method)
        {
            case 'index':
            ?>
                <script type="text/javascript" src="<?php echo base_url()?>/assets/js/canvasjs-1.5.7/jquery.canvasjs.min.js"></script>
                
            <?php
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>