<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class service_rest extends front_controller{
    
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('report_all/report_lib');
        $this->report_lib=new report_lib;
    }
    
    public function yearly_transaction()
    {
        $response=$this->report_lib->yearly_transaction();
        echo json_encode($response);
    }

    public function probabilitas()
    {
        $aktif=$this->input->get('aktif');
        $response=$this->report_lib->probabilitas($aktif);
        echo json_encode($response);
    }
}

?>
