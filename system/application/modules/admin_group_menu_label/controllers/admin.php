<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author tejomurti
 * Module ini menggunakan plugins grocery crud
 */
class admin extends admin_controller {
    //put your code here
    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');  
        $this->load->model('admin_group_menu_label_lib');
        
        $this->mainModel=new admin_group_menu_label_lib;
        
    }
    
    public function index()
    {
        
        
        $crud = new grocery_CRUD();
        $crud->set_subject('Label Item Menu');
        $crud->set_theme('flexigrid');
        $crud->set_table('site_administrator_privilege_label');
        $crud->columns('privilege_label_module','privilege_link');
        $crud->display_as('privilege_label_name','Label Name')->display_as('privilege_label_desc','Desc')
             ->display_as('privilege_label_module','Module')
             ->display_as('privilege_label_class','Class')
             ->display_as('privilege_label_is_hidden','Simpan untuk? ')
             ->display_as('privilege_label_method','Method');
       
          $crud->field_type('privilege_label_is_hidden','dropdown',
            array(0 => 'Ditampilkan', 1 => 'Disembunyikan',));
        $crud->required_fields('privilege_label_name','privilege_label_is_hidden');
        
        $crud->unset_add_fields('privilege_label_module','privilege_label_class','privilege_label_method');
        $crud->unset_edit_fields('privilege_label_module','privilege_label_class','privilege_label_method');
        $crud->unset_export();
        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_print();
        
        $crud->unset_edit();
        $crud->unset_delete();
        $where='1 GROUP BY privilege_label_module';
        $crud->where($where,'',false);
        
        
        $crud->callback_column('privilege_link',array($this,'ign_index_filter'));

        $this->data['output'] = $crud->render();
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);


    }
    
    public function index_filter($module='')
    {
        
        if(trim($module)=='')
        {
            show_error('Module tidak ditemukan.',500);
        }
        
        $crud = new grocery_CRUD();
        $crud->set_subject('Label Item Menu');
        $crud->set_theme('flexigrid');
        $crud->set_table('site_administrator_privilege_label');
        $crud->columns('privilege_label_name','privilege_label_desc','privilege_label_module','privilege_label_class','privilege_label_method');
        $crud->display_as('privilege_label_name','Label Name')->display_as('privilege_label_desc','Desc')
             ->display_as('privilege_label_module','Module')
             ->display_as('privilege_label_class','Class')
             ->display_as('privilege_label_is_hidden','Simpan untuk? ')
             ->display_as('privilege_label_method','Method');
       
          $crud->field_type('privilege_label_is_hidden','dropdown',
            array('0' => 'Ditampilkan', 1 => 'Disembunyikan',));
        $crud->required_fields('privilege_label_name','privilege_label_is_hidden');
        
        $crud->unset_add_fields('privilege_label_module','privilege_label_class','privilege_label_method');
        $crud->unset_edit_fields('privilege_label_module','privilege_label_class','privilege_label_method');
        $crud->unset_export();
        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_print();
        $crud->where('privilege_label_module',$this->security->sanitize_filename($module));    
        

        $this->data['output'] = $crud->render();
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['title_module']=$module;
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);


    }
    
    public function ign_index_filter($value, $row)
    {   
        
        $admin_group_id=$_SESSION['admin']['detail']['admin_group_id'];
        $link=base_url().'admin/admin_group_menu_label/index_filter/'.$row->privilege_label_module;
        $label_link_menu_label='<a href="'.$link.'">Detail Label</a>';
        $menu_id=$this->function_lib->get_one('menu_administrator_id','site_administrator_menu','menu_administrator_location="admin" 
             AND menu_administrator_module="'.$row->privilege_label_module.'" ');
        $link_group=base_url().'admin/admin_menu/update/'.$menu_id;
        $label_link_menu='<a href="'.$link_group.'">Menu</a>';
        $label_link_group='<a href="'.base_url().'admin/admin_group/set_privilege/'.$admin_group_id.'">Group</a>';
        return $label_link_menu_label.' / '.$label_link_menu.' / '.$label_link_group;
    }
    
    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        switch($method)
        {
            
            case 'index':
                
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
    
    
}

?>
