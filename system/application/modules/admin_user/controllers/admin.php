<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
require_once APPPATH.'modules/admin_user/components/admin_user_lib.php';
require_once APPPATH.'modules/admin_group/components/admin_group_lib.php';
class admin extends admin_controller{
    
    protected $mainModel; //model utama dari module ini
    protected $groupModel;

    //put your code here
    public function __construct() {
        parent::__construct();
        
        $this->load->model(array('store/store_lib'));
        $this->mainModel=new admin_user_lib;
        $this->groupModel=new admin_group_lib;
        $this->storeModel=new store_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['title_method']='Data Admin';
        $this->data['desc_method']='Daftar Administrator';
        
        $this->data['dataGroup']=$this->mainModel->findAll();
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        
        
        //untuk hapus item yang diseleksi
        $this->delete_selected_item();
        
        //aktifkan item yang diseleksi
        $this->publish_selected_item();
        
        //non aktifkan item yang diseleksi
        $this->unpublish_selected_item();
        
        $this->data['seoTitle']='Data Admin';
        $description='Daftar Admin yang tersedia';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-user",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function create()
    {
        
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        
        if($this->input->post('save'))
        {
            $_POST['admin_is_active']=isset($_POST['admin_is_active'])?$_POST['admin_is_active']:'0';
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
            $administrator_person_id = $this->input->post('administrator_person_id');
            $full_name=$this->function_lib->get_one('person_full_name',' sys_person','person_id='.intval($this->input->post('administrator_person_id')));

            if($status==200)
            {
                
                $columns=array(
                    'admin_group_id'=>$this->input->post('admin_group_id'),
                    'admin_username'=>$this->input->post('admin_username'),
                    'admin_password'=>md5($this->input->post('admin_password')),
                 //   'admin_last_login'=>$this->input->post('admin_last_login'),
                    'admin_is_active'=>$this->input->post('admin_is_active'),
                    'admin_person_id'=>0,
                    'admin_person_full_name'=> (isset($full_name)&&trim($full_name)!='')?$full_name:null,
                     
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $id=$this->function_lib->insert_id();
                    //save unit/cabang
                    $column=array(
                        'administrator_store_administrator_id'=>$id,
                        'administrator_store_store_id'=>0,
                        );
                    $this->db->insert('site_administrator_store',$column);

                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/admin_user/index?status=200&msg='.$msg);
                }
            }

        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        //data group admin
        $this->data['groupAdminArr']=$this->groupModel->findAll('1','no limit');

        //data unit/cabang
        $this->data['storeArr']=$this->storeModel->findAll('1','no limit');

        //data karyawan
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = "sys_person";
        $params['join'] = "";
         $params['where'] = "
            person_was_deleted='N'
            AND person_is_active='Y'
            AND person_is_employee='Y'
            ORDER BY person_full_name ASC
        ";

        $query_person = $this->function_lib->get_query_data($params);
        $this->data['personArr'] = $query_person->result();

        $this->data['seoTitle']='Tambah User';
        $description='Tambah User Admin';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );

        $breadcrumbs_array[]= array(
                                    'name'=>'Data Admin',
                                    'class'=>"clip-user",
                                    'link'=>base_url().'admin/admin_user/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data group via form
     * @param int $groupId
     */
    public function update($userId=0)
    {

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        if(!is_numeric($userId) OR $userId==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
         $session=new auth_admin_lib;
        //dapatkan data session untuk memfilter hak akses admin
        //admin dengan type selain superuser hanya dapat update dirinya sendiri
        $getSession=$session->getAdminSession();
        $groupType=$getSession['detail']['admin_group_type'];//group type yg sedang login
        $adminId=$getSession['detail']['admin_id'];
        
        //cek tipe admin yang akan dieksekusi
        $groupTypeThisAction=  admin_user_lib::getAdminType($userId);
        //cek jenis admin
        
            if($groupTypeThisAction=='superuser')
            {
                if($groupType==$groupTypeThisAction AND $userId!=$adminId)
                {
                    $status=500;
                    $msg=  base64_encode('Anda tidak diizinkan mengakses user ini.');
                    redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                }
                elseif($groupType!='superuser')
                {
                    $status=500;
                    $msg=  base64_encode('Anda tidak diizinkan mengakses user ini.');
                    redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                }
            }
//            elseif($adminId==$userId)
//            {
//                $status=500;
//                $msg=  base64_encode('User saat ini sedang digunakan sehingga tidak dapat dihapus.');
//                redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
//            }
        
        
        $this->data['current_store_id']=$this->function_lib->get_one('administrator_store_store_id','site_administrator_store','administrator_store_administrator_id='.intval($userId)); 
        $this->data['current_person_id']=$this->function_lib->get_one('admin_person_id','site_administrator','admin_id='.intval($userId)); 

        if($this->input->post('save'))
        {
            $_POST['admin_is_active']=isset($_POST['admin_is_active'])?$_POST['admin_is_active']:'0';
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($userId,'update');
            $status=$isValidationPassed['status'];
//            $message=$isValidationPassed['message'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
            $full_name=$this->function_lib->get_one('person_full_name',' sys_person','person_id='.intval($this->input->post('administrator_person_id')));

            if($status==200)
            {
                $additionalColumn=(isset($_POST['admin_password']) AND trim($_POST['admin_password'])!='')?array('admin_password'=>md5($this->input->post('admin_password'))):array();
                $columns=array(
                    'admin_group_id'=>$this->input->post('admin_group_id'),
                    'admin_username'=>$this->input->post('admin_username'),
                  //  'admin_last_login'=>$this->input->post('admin_last_login'),
                    'admin_is_active'=>$this->input->post('admin_is_active'),
                    'admin_person_id'=>0,
                    'admin_person_full_name'=> $full_name,
                );
                $this->mainModel->setPostData(array_merge($columns,$additionalColumn));
                $where='admin_id='.  intval($userId);
                $hasSaved=$this->mainModel->save($where);
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    if($this->data['current_store_id']>0)
                    {
                        $where=array('administrator_store_administrator_id'=>intval($userId));
                        $columns=array(
                            'administrator_store_store_id'=>0,
                            );
                        $this->db->update('site_administrator_store',$columns,$where);
                    }
                    else
                    {
                        $columns=array(
                            'administrator_store_store_id'=>$this->input->post('administrator_store_store_id'),
                            'administrator_store_administrator_id'=>$userId,
                            );
                        $this->db->insert('site_administrator_store',$columns);
                    }

                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/admin_user/update/'.$userId.'?status=200&msg='.$msg);
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['rowAdmin']=$this->mainModel->find('admin_id='.  intval($userId));
         if(empty($this->data['rowAdmin']))
        {
            show_404();
        }
        //data group admin
        $this->data['groupAdminArr']=$this->groupModel->findAll('1','no limit');
        $this->data['groupTypeLogin']=$groupType;
        $this->data['adminIdLogin']=$adminId;
        $this->data['groupTypeThisAction']=$groupTypeThisAction;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template


        $this->data['seoTitle']='Ubah User';
        $description='Ubah User Admin';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );

        $breadcrumbs_array[]= array(
                                    'name'=>'Data Admin',
                                    'class'=>"clip-user",
                                    'link'=>base_url().'admin/admin_user/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        $this->data['storeArr']=$this->storeModel->findAll('1','no limit');  

        //data karyawan
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = "sys_person";
        $params['join'] = "";
         $params['where'] = "
            person_was_deleted='N'
            AND person_is_active='Y'
            AND person_is_employee='Y'
            ORDER BY person_full_name ASC
        ";

        $query_person = $this->function_lib->get_query_data($params);
        $this->data['personArr'] = $query_person->result();

        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk update data group
     * @param int $groupId
     */
    public function view($userId=0)
    {
        
        if(!is_numeric($userId) OR $userId==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $message='';
        $status=200;
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['rowAdmin']=$this->mainModel->find('admin_id='.  intval($userId));
        if(empty($this->data['rowAdmin']))
        {
            show_404();
        }

         $this->data['seoTitle']='Detil User';
        $description='Detil User Admin';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );

        $breadcrumbs_array[]= array(
                                    'name'=>'Data Admin',
                                    'class'=>"clip-user",
                                    'link'=>base_url().'admin/admin_user/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-zoom-in",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);

        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    public function delete($id=0,$output='redirect')
    {
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $session=new auth_admin_lib;
        //dapatkan data session untuk memfilter hak akses admin
        //admin dengan type selain superuser hanya dapat update dirinya sendiri
        $getSession=$session->getAdminSession();
        $groupType=$getSession['detail']['admin_group_type'];//group type yg sedang login
        $adminId=$getSession['detail']['admin_id'];
        
        //cek tipe admin yang akan dieksekusi
        $groupTypeThisAction=  admin_user_lib::getAdminType($id);
        //cek jenis admin
        //group type yg sedang login
        if($groupType=='superuser')
        {
            if($adminId==$id)
            {
                $status=500;
                $msg=  base64_encode('User saat ini sedang digunakan sehingga tidak dapat dihapus.');
                redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
            }
            
        }
        else
        {
            if($groupTypeThisAction=='superuser')
            {
                $status=500;
                $msg=  base64_encode('Anda tidak diizinkan menghapus user ini.');
                redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
            }
            elseif($adminId==$id)
            {
                $status=500;
                $msg=  base64_encode('User saat ini sedang digunakan sehingga tidak dapat dihapus.');
                redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
            }
        }
        
        $where='admin_id='.  intval($id);
        $this->data['rowAdmin']=$this->mainModel->find($where);
        if(!empty($this->data['rowAdmin']))
        {
           $isDelete=$this->mainModel->delete($where);
           switch($output)
           {
               case 'json':
                   echo json_encode($isDelete);
               break;    
               case 'array':
                   return $isDelete;
               break;    
               default:
                   redirect(base_url().'admin/admin_user/index?status='.$isDelete['status'].'&msg='.base64_encode($isDelete['message']));
               break;    
           }
        }
        else
        {
            $msg='Permintaan tidak ditemukan';
            redirect(base_url().'admin/admin_user/index?status=500&msg='.base64_encode($msg));
        }
        
        
    }
    
    /**
     * untuk menghapus item yang diseleksi
     */
    public function delete_selected_item()
    {
        
        if($this->input->post('delete_selected_item') AND auth_admin_lib::manualProctection('admin','delete_selected_item'))
        {
            $adminArr=isset($_POST['admin_id'])?$_POST['admin_id']:'';
            if(!empty($adminArr))
            {
                $isDelete=array(
                    'status'=>200,
                    'message'=>'ok',
                );
                foreach($adminArr AS $userId)
                {
                    
                    $isDelete=$this->delete($userId, 'array');
                }
                redirect(base_url().'admin/admin_user/index?status='.$isDelete['status'].'&msg='.  base64_encode($isDelete['message']));
            }
        }
    }
    
    /**
     * untuk menghapus item yang diseleksi
     */
    public function publish_selected_item()
    {
        
                    
        $session=new auth_admin_lib;
        //dapatkan data session untuk memfilter hak akses admin
        //admin dengan type selain superuser hanya dapat update dirinya sendiri
        $getSession=$session->getAdminSession();
      $groupType=$getSession['detail']['admin_group_type'];//group type yg sedang login
        $adminId=$getSession['detail']['admin_id'];
        
        if($this->input->post('publish_selected_item') AND auth_admin_lib::manualProctection('admin','publish_selected_item'))
        {
            $groupAdminArr=isset($_POST['admin_id'])?$_POST['admin_id']:'';
            if(!empty($groupAdminArr))
            {
                $status=200; //status saat penyimpanan
                $message='';
                foreach($groupAdminArr AS $userId)
                {

                    //cek tipe admin yang akan dieksekusi
                    $groupTypeThisAction=  admin_user_lib::getAdminType($userId);
                    //cek jenis admin

                     if($groupTypeThisAction=='superuser' )
                     {
    
                        if($groupType==$groupTypeThisAction AND $adminId==$userId)
                        {
                            $status=500;
                            $msg=  base64_encode('Anda tidak diizinkan mengakses user yang sedang aktif.');
                            redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                        }
                        else
                        {
                            $status=500;
                            $msg=  base64_encode('Anda tidak diizinkan mengakses user ini.');
                            redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                        }
                    }
                    elseif($adminId==$userId)
                    {
                        $status=500;
                        $msg=  base64_encode('User saat ini sedang digunakan sehingga tidak dapat diubah.');
                        redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                    }
                    
                    $columns=array(
                    'admin_is_active'=>1,
                    );

                    $this->mainModel->setPostData($columns);
                     $where='admin_id='.  intval($userId);
                    $hasSaved=$this->mainModel->save($where);
                    $status=$hasSaved['status']; //status saat penyimpanan
                    $message='Data berhasil diaktifkan';
                }
                redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.  base64_encode($message));
            }
        }
    }
    
    /**
     * untuk menghapus item yang diseleksi
     */
    public function unpublish_selected_item()
    {
                     
        $session=new auth_admin_lib;
        //dapatkan data session untuk memfilter hak akses admin
        //admin dengan type selain superuser hanya dapat update dirinya sendiri
        $getSession=$session->getAdminSession();
        $groupType=$getSession['detail']['admin_group_type'];//group type yg sedang login
        $adminId=$getSession['detail']['admin_id'];
        if($this->input->post('unpublish_selected_item') AND auth_admin_lib::manualProctection('admin','unpublish_selected_item'))
        {
            $groupAdminArr=isset($_POST['admin_id'])?$_POST['admin_id']:'';
            if(!empty($groupAdminArr))
            {
                $status=200; //status saat penyimpanan
                $message='';
                foreach($groupAdminArr AS $userId)
                {
                    
                      //cek tipe admin yang akan dieksekusi
                    $groupTypeThisAction=  admin_user_lib::getAdminType($userId);
                    //cek jenis admin

                    if($groupTypeThisAction=='superuser' )
                    {
                        if($groupType==$groupTypeThisAction AND $adminId==$userId)
                        {
                            
                            $status=500;
                            $msg=  base64_encode('Anda tidak diizinkan mengakses user yang sedang aktif.');
                            redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                        }
                        else
                        {
                            $status=500;
                            $msg=  base64_encode('Anda tidak diizinkan mengakses user ini.');
                            redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                        }
                    }
                    elseif($adminId==$userId)
                    {
                        $status=500;
                        $msg=  base64_encode('User saat ini sedang digunakan sehingga tidak dapat diubah.');
                        redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.$msg);
                    }
                    
                    $columns=array(
                    'admin_is_active'=>0,
                    );

                    $this->mainModel->setPostData($columns);
                    $where='admin_id='.  intval($userId);
                    $hasSaved=$this->mainModel->save($where);
                    $status=$hasSaved['status']; //status saat penyimpanan
                    $message='Data berhasil dinonaktifkan';
                }
                redirect(base_url().'admin/admin_user/index?status='.$status.'&msg='.  base64_encode($message));
            }
        }
    }
}

?>
