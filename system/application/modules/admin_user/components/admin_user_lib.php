<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
//require_once 'core_user_lib.php';
//class admin_user_lib extends core_user_lib{
class admin_user_lib{
    //put your code here
    
    protected $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_administrator';
    public function __construct() {
        
  //      parent::__construct();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));

        if(!session_id())
        {
            session_start();
        }
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $userId default 0
     * @param string $action create | update
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($userId=0,$action='create')
    {
         $status=500;
         $message='';
         
         
         if($action=='create')
         {
             $config = array(
               array(
                     'field'   => 'admin_group_id',
                     'label'   => 'Nama Group',
                     'rules'   => 'trim|required'
                  ),
               // array(
               //       'field'   => 'administrator_store_store_id',
               //       'label'   => 'Unit/Cabang',
               //       'rules'   => 'trim|required|numeric'
               //    ),
               array(
                     'field'   => 'admin_username',
                     'label'   => 'Username',
                     'rules'   => 'trim|required|callback_username_format'
                  ),
               array(
                     'field'   => 'admin_password',
                     'label'   => 'Password',
                     'rules'   => 'trim|required|min_length[5]'
                  ),
               array(
                     'field'   => 'admin_password_repeat',
                     'label'   => 'Password Ulangi',
                     'rules'   => 'trim|required|matches[admin_password]'
                  ),
               array(
                     'field'   => 'admin_is_active',
                     'label'   => 'Status',
                     'rules'   => 'trim|required'
                  ),  
             
            );
         }
         else
         {
             //ketika update dan inputkan password
             if($this->CI->input->post('admin_password') AND trim($_POST['admin_password'])!='')
             {
                 $config = array(
                    array(
                            'field'   => 'admin_group_id',
                            'label'   => 'Nama Group',
                            'rules'   => 'trim|required'
                        ),
                    array(
                            'field'   => 'admin_username',
                            'label'   => 'Username',
                            'rules'   => 'trim|required|callback_username_format'
                        ),
                    array(
                            'field'   => 'admin_password',
                            'label'   => 'Password',
                            'rules'   => 'trim|required|min_length[5]'
                        ),
                    array(
                            'field'   => 'admin_password_repeat',
                            'label'   => 'Password Ulangi',
                            'rules'   => 'trim|required|matches[admin_password]'
                        ),
                    array(
                            'field'   => 'admin_is_active',
                            'label'   => 'Status',
                            'rules'   => 'trim|required'
                        ),  

                    );
                 
                //ketika update apakah password lama sudah benar
                $oldPass=($action=='update')?$this->CI->input->post('admin_password_old'):'';
                $isCurrentPassword=$this->isCurrentPassword($oldPass,$userId);
                 
             }
             else
             {
                 $config = array(
                    array(
                            'field'   => 'admin_group_id',
                            'label'   => 'Nama Group',
                            'rules'   => 'trim|required'
                        ),
                    array(
                            'field'   => 'admin_username',
                            'label'   => 'Username',
                            'rules'   => 'trim|required|callback_username_format'
                        ),
                    array(
                            'field'   => 'admin_is_active',
                            'label'   => 'Status',
                            'rules'   => 'trim|required'
                        ),  

                    );
             }
         }
         
      
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
              $query=$this->CI->input->post('admin_username');
              $isExist=$this->isExistUserName($query,$userId);

              $query_person=$this->CI->input->post('administrator_person_id');
              $full_name_person=$this->CI->function_lib->get_one('person_full_name',' sys_person','person_id='.intval($query_person));
              $isExistPerson=$this->isExistPerson($query_person,$userId);              

              if(!$isExist)
              {
                  $status=500;
                  $message='Username '.$query.' sudah pernah diinputkan. Silakan coba dengan yang lain.';
              }

              elseif(!$isExistPerson)
              {
                  $status=500;
                  $message='Nama Penggunakan '.$full_name_person.' sudah pernah diinputkan. Silakan coba dengan yang lain.';
              }
              
              elseif(isset($isCurrentPassword) AND $action=='update' AND !$isCurrentPassword)
              {
                  
                   $session=new auth_admin_lib;
                   //dapatkan data session untuk memfilter hak akses admin
                    //admin dengan type selain superuser hanya dapat update dirinya sendiri
                   $getSession=$session->getAdminSession();
                   $groupType=$getSession['detail']['admin_group_type'];//group type yg sedang login
               //    $adminId=$getSession['detail']['admin_id'];

                    //cek tipe admin yang akan dieksekusi
                   $groupTypeThisAction=  admin_user_lib::getAdminType($userId);
                    
                   //jika admin yg diedit bukan superuser, lewatkan pengcekkan password lama
                    if($groupTypeThisAction!='superuser' AND $groupType=='superuser')
                    {
                        $status=200;
                        $message='';
                    }
                    else
                    {
                        $status=500;
                        $message='Password lama yang diinputkan tidak sesuai.';
                    }
                    
              }
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * check ketersediaan data username
     * @param stirng $query 
     * @param int $userId default 0 saat create
     * @return bool true is valid
     */
    public function isExistUserName($query, $userId=0)
    {
        $value=TRUE;
        //update
        if(is_numeric($userId) AND $userId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_username',$this->mainTable,'admin_username LIKE TRIM(\''.$query.'\') AND admin_id!='.  intval($userId));
        }
        else//create
        {
            $isFounded=$this->CI->function_lib->get_one('admin_username',$this->mainTable,'admin_username LIKE TRIM(\''.$query.'\')');
        }
        
        if($isFounded)
        {
            $value=FALSE;
        }
        
        return $value;
    }

    /**
     * check ketersediaan data pengguna
     * @param stirng $query 
     * @param int $userId default 0 saat create
     * @return bool true is valid
     */
    public function isExistPerson($query, $userId=0)
    {
        $value=TRUE;
        //update
        if(is_numeric($userId) AND $userId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_person_id',$this->mainTable,'admin_person_id=TRIM(\''.$query.'\') AND admin_id!='.  intval($userId));
        }
        else//create
        {
            $isFounded=$this->CI->function_lib->get_one('admin_person_id',$this->mainTable,'admin_person_id=TRIM(\''.$query.'\')');
        }
        
        if($isFounded)
        {
            $value=FALSE;
        }
        
        return $value;
    }
    
    /**
     * check password saat ini
     * @param stirng $query 
     * @param int $userId default 0 saat create
     * @return bool true is valid
     */
    public function isCurrentPassword($query, $userId=0)
    {
        $value=false;
        //update
        if(is_numeric($userId) AND $userId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_password',$this->mainTable,'admin_password="'.md5($query).'" AND admin_id='.  intval($userId));
        }
        
        if($isFounded)
        {
            $value=true;
        }
        
        return $value;
    }
    
    /**
     * format yang diizinkan untuk username
     */
    public function username_format($str)
    {
        
        if (! preg_match("/^([-a-z-0-9_])+$/i", $str))
        {
                $this->form_validation->set_message('username_format', 'The %s field can not be the word "test"');
                return FALSE;
        }
        else
        {
            $this->form_validation->set_message('username_format', 'The %s field can not be the word "test"');
                return FALSE;
                //return TRUE;
        }
      //  return ( ! preg_match("/^([-a-z-0-9_])+$/i", $str)) ? FALSE : TRUE;
    } 
    
    /**
     * check type admin by id
     * @param int $adminId
     */
    public static function getAdminType($adminId=0)
    {
        $adminGroupType='';
        $userLib=new admin_user_lib;
        if(is_numeric($adminId) AND $adminId!=0)
        {
            $admingGroupId=$userLib->CI->function_lib->get_one('admin_group_id',$userLib->mainTable,'admin_id='.intval($adminId));
            $adminGroupType=$userLib->CI->function_lib->get_one('admin_group_type','site_administrator_group','admin_group_id='.  intval($admingGroupId));
        }
        
        return $adminGroupType;
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
