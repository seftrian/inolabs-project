<?php

class master_sub_master_lib
{
    public $CI;
    protected $postData = array(); //set post data
    protected $mainTable = 'site_news_category';
    protected $dir = 'assets/images/master_content_category/';

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library(array('function_lib', 'form_validation'));

        if (!session_id()) {
            session_start();
        }
    }

    public function countAll($where = 1)
    {
        $sql = 'SELECT COUNT(*) AS jml FROM ' . $this->mainTable . '
              WHERE ' . $where . ' 
              ';
        $exec = $this->CI->db->query($sql);
        $row = $exec->row_array();
        return !empty($row) ? $row['jml'] : 0;
    }


    public function findAll($where = 1, $limit = 10, $offset = 0, $orderBy = 'category_id ASC')
    {
        $sql = 'SELECT * FROM ' . $this->mainTable . '
              WHERE ' . $where . ' 
              ';
        if (trim($orderBy) != '') {
            $sql .= ' ORDER BY ' . $orderBy;
        }
        if (is_numeric($limit) and is_numeric($offset)) {
            $sql .= ' LIMIT ' . $offset . ', ' . $limit;
        }
        $exec = $this->CI->db->query($sql);
        return $exec->result_array();
    }

    public function getImagePath()
    {
        $path = base_url() . $this->dir;
        $pathLocation = FCPATH . $this->dir;

        if (!file_exists($pathLocation) and trim($pathLocation) != '') {
            $explode_arr = explode('/', $pathLocation);
            $last_dir = count($explode_arr) - 2;
            //src
            if (!empty($explode_arr)) {
                $src = '';
                $max_src = count($explode_arr) - 2;
                for ($i = 0; $i < count($explode_arr); $i++) {
                    if ($i < $max_src) {
                        $src .= $explode_arr[$i] . '/';
                    }
                }
            }

            $dir = $explode_arr[$last_dir];
            mkdir($src . $dir);
            chmod($src . $dir, 0777);
        }

        return array(
            'pathUrl' => $path,
            'pathLocation' => $pathLocation,
        );
    }

    public function formValidation($id = 0)
    {
        $sts= 500;
        $msg = '';
        $mainConfig = array(
            array(
                'field' => 'category_title',
                'label' => 'judul',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'category_content',
                'label' => 'isi',
                'rules' => 'trim|required',
            ),
        );
        $config = $mainConfig;
        $this->CI->form_validation->set_rules($config);
        if ($this->CI->form_validation->run() == true) {
            // proses melewati validasi form
            $sts = 200;
            $message = 'Form ready to save';
        }
        return array(
            'sts' => $sts,
            'msg' => $msg,
        );
    }

    public function generate_permalink($title, $id = 0)
    {
        $datetime = date('Y-md H:i:s');
        $permalink = $this->CI->function_lib->seo_name($title);
        $where = 'category_permalink="'.$permalink.'"';
        if ($id != 0) {
            $where .= ' AND category_id != ' .intval($id);
        }
        $is_exist = $this->CI->function_lib->get_one('category_id', 'site_news_category', $where);
        if ($is_exist) {
            $permalink = $this->CI->function_lib->seo_name($title).strtotime($datetime);
        }
        return $permalink;
    }

    public function save($where = '')
    {
        $sts = 500;
        $message = 'error';
        if (trim($where) != '') {
            $sql='UPDATE ' .$this->mainTable. ' SET ';
            if (!empty($this->postData)) {
                $no = 1;
                foreach ($this->postData as $colum => $value) {
                    $separated = ($no >=1 and $no < count($this->postData)) ? ',':'';
                    $sql .= ' '.$colum. '="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql .= 'WHERE '. $where;
            $this->CI->db->query($sql);
            $sts = 200;
            $msg = 'OK';
        } else {
            $this->CI->db->insert($this->mainTable, $this->postData);
            $sts = 200;
            $msg = 'OK';
        }
        return array(
            'sts' => $sts,
            'msg' => $message
        );
    }
    public function setPostData($dataArr)
    {
        $this->postData = $dataArr;
    }

    public function doUpload($id, $field_name)
    {
        $config                  = array();
        $pathImage               = $this->dir;
        $config['upload_path']   = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size']      = '10500';                         //KB
        $config['file_name']     = function_lib::seo_name(time());
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        if ($isComplete) {
            $dataImage=$this->CI->upload->data();
            //simpan
            $where=array(
                'category_id'=>$id,
            );
            $column=array(
                'category_photo'=>$pathImage.$dataImage['file_name'],
            );
            $this->CI->db->update($this->mainTable, $column, $where);
            $status=200;
            $message='File berhasil disimpan';
        } else {
            $status=500;
            $message=$this->CI->upload->display_errors();
            ;
        }
        return array(
            'sts'=>$status,
            'msg'=>$message,
        );
    }

    public function handleUpload($id, $filesName)
    {
        $status  = 200;
        $message = 'File berhasil disimpan';
            
        $fileUpload = (isset($_FILES[$filesName]['name']) and trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
      
        if (!empty($fileUpload)) {
            $results = $this->doUpload($id, $filesName);
            $status  = $results['sts'];
            $message = $results['msg'];
        }
        
        return array(
            'sts'  => $status,
            'msg' => $message,
        );
    }

    public function find($where = 1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }

    public function delete($where)
    {
        $sts = 200;
        $msg = '';
        if (trim($where)) {
            $sql = 'DELETE FROM '.$this->mainTable.'
            WHERE '. $where;
            try {
                $this->CI->db->query($sql);
                $sts = 200;
                $msg = 'Data Berhasil di hapus';
            } catch (Exception $e) {
                $sts = 500;
                $msg = 'Terjadi kesalahan, data tidak dapat di hapus.';
            }
        }
        return array(
            'sts' => $sts,
            'msg' => $msg,
        );
    }
}
