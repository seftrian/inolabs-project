<?php
class admin extends admin_controller
{
    /**
         * Description of admin_group
         *
         * @author seftrian
     */
    public $mainModel;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('master_sub_master_lib'));
        $this->load->helper(array('pagination', 'tinymce'));

        $this->mainModel = new master_sub_master_lib;

        parse_str($_SERVER['QUERY_STRING'], $_GET);
        $this->pathImgArr = $this->mainModel->getImagePath();
    }


    public function index()
    {
        $sts = (isset($_GET['sts']) and trim($_GET['sts']) != '') ? $_GET['sts'] : 200;
        $msg = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';
        $this->data['sts'] = $sts;
        $this->data['msg'] = $msg;

        // setting theme get dari class admin_controller
        $this->data['themeId']      = $this->themeId;
        $this->data['themeUrl']     = $this->themeUrl;
        $this->data['templateName'] = $this->templateName;
        $this->data['seoTitle']     = 'Sub Menu Master 1';
        $description                = 'Sub Menu Master 2';

        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'home',
            'class' => 'clip-home-3',
            'link' => base_url() . 'admin/dashboard',
            'current' => false
        );

        $breadcrumbs_array[] = array(
            'name' => 'Sub Master Master 1',
            'class' => 'fa fa-thermometer-half',
            'link' => base_url() . 'admin/master_sub_master_satu/index',
            'current' => true,
        );

        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);

        // get data dari master_sub_master_lib
        $limit      = 10;
        $where      = '1';
        $total_rows = $this->mainModel->countAll($where);
        // make paginasi
        $paginationArr = create_pagination('admin/' . $this->currentModule . '/' . __FUNCTION__, $total_rows, $limit);

        $this->data['offset']          = $paginationArr['limit'][1];
        $this->data['findAll']         = $this->mainModel->findAll($where, $limit, $this->data['offset'], 'category_id DESC');
        $this->data['mainModel']       = $this->mainModel;
        $this->data['link_pagination'] = $paginationArr['links'];
        $this->data['pathImgArr']      = $this->pathImgArr;

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
 
    public function create()
    {
        $sts = (isset($_GET['sts']) and trim($_GET['sts'] !='') ? $_GET['sts'] : 200);
        $msg = (isset($_GET['msg']) and trim($_GET['msg']) !='') ? base64_decode($_GET['msg']) : '';

        $this->data['sts'] = $sts;
        $this->data['msg'] = $msg;

        $this->data['themeId']      = $this->themeId;
        $this->data['themeUrl']     = $this->themeUrl;
        $this->data['templateName'] = $this->templateName;
        /* breadcrumbs */
        $this->data['seoTitle'] = 'Sub Menu Master 1';
        $description = 'Tambah Master 1';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
                'name'    => 'home',
                'class'   => 'clip-home-3',
                'link'    => base_url() . 'admin/dashboard',
                'current' => false
            );

        $breadcrumbs_array[] = array(
                'name'    => 'Form 1',
                'class'   => 'fa fa-thermometer-half',
                'link'    => base_url() . 'admin/master_sub_master_satu/index',
                'current' => true,
            );
        $breadcrumbs_array[] = array(
                'name' => 'Tambah',
                'class' => "clip-pencil",
                'link' => base_url() . 'admin/master_sub_master_Satu/create',
                'current' => true, //boolean
            );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        if ($this->input->post('save')) {
            $isValidationPassed = $this->mainModel->formValidation();
            $status = $isValidationPassed['sts'];
            // validasi erorr
            $msg = ($status == 500 and $isValidationPassed['msg'] == '') ? validation_errors() : $isValidationPassed['msg'];
            // validasi success
            if ($status == 200) {
                $title = $this->input->post('category_title');
                $admin_username = isset($_SESSION['admin']['detail']['admin_username']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                $colum = array(
                        'category_title' => $this->input->post('category_title', true),
                        'category_permalink' => $this->mainModel->generate_permalink($title),
                        'category_content' => $this->input->post('category_content'),
                        'category_input_by' => $admin_username,
                        'category_type' => $this->input->post('category_type'),
                        'category_meta_description' => $this->input->post('category_meta_description', true),
                        'category_meta_tags' => $this->input->post('category_meta_tags', true),
                    );
                $this->mainModel->setPostData($colum);
                $hasSaved = $this->mainModel->save();
                $status = $hasSaved['sts']; // status saat save
                if ($status == 200) {
                    $id = $this->function_lib->insert_id();
                    $logo = $this->mainModel->handleUpload($id, 'category_photo');
                    if ($logo['sts'] != 200) {
                        $sts = $logo['sts'];
                        $msg = $logo['msg'];
                        redirect(base_url() . 'admin/' . $this->currentModule . '/update/'.$id. 'sts=500&msg='.$msg);
                    } else {
                        $msg = base64_encode('Data berhasil disimpan');
                        if (isset($_GET['redirect'])) {
                            redirect(rawurldecode($_GET['redirect']));
                        } else {
                            redirect(base_url() . 'admin/'. $this->currentModule . '/create?sts=200&msg='. $msg);
                        }
                    }
                }
            }
        }
        $this->data['sts'] = $sts;
        $this->data['msg'] = $msg;
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
     * @param string $method
     * @param array $extraVariable
     * **/
    protected function footerScript($method = '', $extraVariable = array())
    {
        //set variable
        if (!empty($extraVariable)) {
            extract($extraVariable);
        }

        ob_start();
        switch ($method) {
            default:
        ?>
            
            <?php
                break;
        }

        $footerScript = ob_get_contents();
        ob_end_clean();
        return $footerScript;
    }

    public function update($id = 0)
    {
        $sts = (isset($_GET['sts']) and trim($_GET['sts']) != '') ? $_GET['sts'] : 200;
        $msg = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['themeId']      = $this->themeId;
        $this->data['themeUrl']     = $this->themeUrl;
        $this->data['templateName'] = $this->templateName;

        $this->data['seoTitle'] = "Sub Menu Master 1";
        $description = 'Update Master 1';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
                'name'    => 'home',
                'class'   => 'clip-home-3',
                'link'    => base_url() . 'admin/dashboard',
                'current' => false
            );

        $breadcrumbs_array[] = array(
                'name'    => 'Form 1',
                'class'   => 'fa fa-thermometer-half',
                'link'    => base_url() . 'admin/master_sub_master_satu/index',
                'current' => true,
            );
        $breadcrumbs_array[] = array(
                'name' => 'Update',
                'class' => "clip-pencil",
                'link' => base_url() . 'admin/master_sub_master_Satu/create',
                'current' => true, //boolean
            );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
       
        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }
        echo $id;
        $rowMenu = $this->mainModel->find('category_id=' .  intval($id));
        // die;
        if (empty($rowMenu)) {
            show_error('Request and tidak valid', 500);
        }
        foreach ($rowMenu as $key => $value) {
            $this->data[$key] = $value;
        }
        $isValidationPassed = $this->mainModel->formValidation($id);
        $sts = $isValidationPassed['sts'];
        $msg = ($sts == 500 and $isValidationPassed['msg'] == '') ? validation_errors() : $isValidationPassed['msg'];
        if ($this->input->post('save')) {
            if ($sts == 200) {
                $title = $this->input->post('category_title');
                $admin_username = isset($_SESSION['admin']['detail']['admin_username']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                $colum = array(
                    'category_title' => $this->input->post('category_title', true),
                    'category_content' => $this->input->post('category_content'),
                    'category_meta_description' => $this->input->post('category_meta_description', true),
                    'category_meta_tags' => $this->input->post('category_meta_tags', true),
                    'category_type' => $this->input->post('category_type'),
                    'category_photo' => $this->input->post('category_photo_old', true),
                   );
                $this->mainModel->setPostData($colum);
                $where = 'category_id='.intval($id);
                $hasSaved = $this->mainModel->save($where);
                $sts = $hasSaved['sts'];
                if ($sts == 200) {
                    $msg = base64_encode('Data berhasil disimpan');
                    $logo = $this->mainModel->handleUpload($id, 'category_photo');
                    if ($logo['sts'] != 200) {
                        $sts = $logo['sts'];
                        $msg = strip_tags($logo['msg']);
                        $msg = base64_encode('Terjadi eror saat upload file. ' . $msg);
                    }
                    redirect(base_url() . 'admin/' . $this->currentModule . '/update/'. $id . '?sts=' . $sts . '&msg=' .$msg);
                }
            }
        }
        $this->data['sts'] = $sts;
        $this->data['msg'] = $msg;

        $this->data['pathImgArr'] = $this->pathImgArr;
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/'.__FUNCTION__, $this->data);
    }

    // delete img

    public function delete_image($id = 0, $output = 'redirect')
    {
        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }
        $where = 'category_id=' .  intval($id);
        $this->data['rowAdmin'] = $this->mainModel->find($where);
        if (!empty($this->data['rowAdmin'])) {
            // mengilangkan file didalam folder link
            if(is_file($this->data['rowAdmin']['category_photo'])){
                unlink($this->data['rowAdmin']['category_photo']);
            }
            $sql = 'UPDATE site_news_category SET category_photo="-" WHERE ' . $where;
            $this->db->query($sql);

            redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=200&msg=' . base64_encode('Gambar berhasil dihapus'));
        } else {
            $msg = 'Data tidak ditemukan';
            redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=500&msg=' . base64_encode($msg));
        }
    }
}
