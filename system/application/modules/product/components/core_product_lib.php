<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of core_testimonial_lib
 *
 * @author tejo murti
 */
class core_product_lib {

    protected $CI;
    protected $myDb;
    protected $dirDefault;
    protected $fileLog='log.txt';
    protected $separatedCommand='|';
    //put your code here
    public function __construct() {
        $this->dirDefault=  dirname(__FILE__).'/';

        $this->CI=&get_instance();
        $this->myDb=$this->CI->db->database;
        //inisialisasi module
        $this->runCommand();

    }

    /**
     * check file siap digunakan atau tidak
     */
    protected function isReadytoCheckFile()
    {
        $myFile='';

        if(file_exists($this->dirDefault.$this->fileLog) AND trim($this->fileLog)!='')
        {
            if (!is_writable($this->dirDefault.$this->fileLog)) {
                show_error('Cannot change the mode of file ('.$this->dirDefault.$this->fileLog.')');
                     //echo "Cannot change the mode of file ($this->dirDefault.$this->fileLog)";
                     exit;
                if (!chmod($this->dirDefault.$this->fileLog, 0666)) {
                     show_error('Cannot change the mode of file ('.$this->dirDefault.$this->fileLog.')');
                    // echo "Cannot change the mode of file ($this->dirDefault.$this->fileLog)";
                     //exit;
                };
            }

            $fh=  fopen($this->dirDefault.$this->fileLog, 'r'); //open file
            $myFile=  fread($fh,  filesize($this->dirDefault.$this->fileLog));
            fclose($fh);
        }

        return $myFile;
    }

    /**
     * bangun array data
     */
    public function buildArrayFromLog()
    {
        $separated=$this->getSeparatedCommand();
        $command= $this->isReadytoCheckFile();
        $explode=array();
        if(trim($command)!='')
        {
            $explode=  explode($separated, $command);
        }
        $commandArr=array();
        if(!empty($explode))
        {
            $no=1;
            $index=0;
            foreach($explode AS $key=>$val)
            {
                switch($no)
                {
                    case 1:
                    $commandArr[$index][]=array(
                        'method'=>$val,
                    );

                    break;
                    case 2:
                    $commandArr[$index][]=array(
                        'status'=>$val,
                    );

                    break;
                    case 3:
                    $commandArr[$index][]=array(
                        'date'=>$val,
                    );

                    break;
                }
                if($no==3)
                {
                    $no=0; //balikan ke 1
                    $index++;
                }
                $no++;
            }
        }


        $newArr=array();
        if(!empty($commandArr))
        {
            foreach($commandArr AS $key=>$dataArr)
            {
                $newArr[$key]=array(
                    'method'=>$dataArr[0]['method'],
                    'status'=>$dataArr[1]['status'],
                    'date'=>$dataArr[2]['date'],
                );
            }
        }
        return $newArr;
    }



    /**
     * dapatkan log file
     */
    public function getFileLog()
    {
        return $this->dirDefault.$this->fileLog;
    }

    public function getSeparatedCommand()
    {
        return $this->separatedCommand;
    }

    /**
     * menjalankan perintah yang telah disiapkan di log
     */
    protected function runCommand()
    {
        $commandArr=$this->buildArrayFromLog();
        if(!empty($commandArr))
        {
          //  chmod($regLib->getFileLog(),0777);

            $no=1;
            $words='';
            foreach($commandArr AS $row)
            {
                $status=$row['status'];
                $method=$row['method'];
                if($no==1)
                {
                      $words=$row['method'].'|1|'.$row['date'];
                }
                else
                {
                      $words.='|'.$row['method'].'|1|'.$row['date'];
                }
                //execute
                if($status==0)
                {
                    if(method_exists($this, $method))
                    {
                        $this->$method();//jalankan method
                    }
                }
                $no++;
            }

            if(trim($words)!='')
            {
                $fh=fopen($this->getFileLog(), 'w+');
                fwrite($fh,$words);
                fclose($fh);
            }
        }
    }

     /**
     * pengecekkan table baru
     */
    protected function runExistMyTable()
    {
        $this->myTable=$this->tableList();
        if(!empty($this->myTable))
        {
            foreach($this->myTable AS $table=>$query)
            {
                $isExecute=$this->isExistMyTable($table);
                if($isExecute)
                {
                    $this->CI->db->query($query);
                    if(count($this->myTable)>1)
                    {
                        sleep(0.5);

                    }
                }
            }
        }
    }

    /**
     * query pengecekkan
     */
    protected function isExistMyTable($table)
    {
        $result=false;
        $sql='SELECT COUNT(*) AS num
                FROM information_schema.tables
                WHERE table_schema = "'.$this->myDb.'"
                AND table_name = "'.$table.'"';

        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        if(!empty($row) AND $row['num']<1)
        {
           $result=true;
        }

        return $result;
    }


    /**
     * daftar table baru
     */
    protected function tableList()
    {
        $myTable=array();
        $myTable['site_support']="

                CREATE TABLE IF NOT EXISTS `site_support` (
                  `support_id` int(15) NOT NULL AUTO_INCREMENT,
                  `support_name` varchar(255) NOT NULL,
                  `support_nick` varchar(255) NOT NULL,
                  `support_phone` varchar(255) NOT NULL,
                  `support_is_active` enum('0','1') NOT NULL DEFAULT '1',
                  PRIMARY KEY (`support_id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

                ";
        return $myTable;

    }

    /**
     * menjalankan query relasi atau penambahan kolom
     * yang dijalankan adalah status 0
     */
    public function runAdditionalQuery()
    {
        $queryArr=$this->listQuery();


        if(!empty($queryArr))
        {
            foreach($queryArr AS $result)
            {
                $query=$result['query'];
                $status=$result['status'];
                if($status==0) //jalankan query dengan status 0
                {
                    $this->CI->db->query($query);
                }
            }
        }
    }

    /**
     * list query yang akan dijalankan
     * pastikan status di set 1 kembali jika telah dijalankan.
     * karena sistem buka tutup baru dari log.txt, belum ke core_lib
     * eq. $query[]=array(
            'query'=>'ALTER TABLE `sys_member`
                      ADD CONSTRAINT `sys_member_ibfk_1` FOREIGN KEY (`member_serial_number`) REFERENCES `sys_serial` (`serial_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                ',
            'status'=>0,
        );
     */
    protected function listQuery()
    {
        $query=array();

        //ALTER TABLE `site_administrator_group` ADD `admin_group_type` ENUM( 'superuser', 'administrator' ) NOT NULL DEFAULT 'administrator'
        return $query;
    }


}

?>
