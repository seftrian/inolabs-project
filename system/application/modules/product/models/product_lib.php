<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of support_lib
 *
 * @author Yusuf Rahmanto
 */
require_once dirname(__FILE__).'/../components/core_product_lib.php';

class product_lib extends core_product_lib{
    //put your code here
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='sys_product';
    protected $primaryKey='product_id';
    protected $imgPath='assets/images/order/';
    protected $session_cart;
    public $is_member=0;
    public function __construct() {

        parent::__construct();

        $this->CI=&get_instance();
        $this->CI->load->library(array('system_lib','function_lib','form_validation'));
        if(!session_id())
        {
            session_start();
        }

        $this->session_cart='product';

    }

    /**
    dapatkan id dari permalink
    @param string $product_permalink
    */
    public function get_product_id_from_permalink($product_permalink)
    {
        $where='product_permalink="'.$this->CI->security->sanitize_filename($product_permalink).'"';
        $product_id=$this->CI->function_lib->get_one('product_id','sys_product',$where);
        return $product_id;
    }

    public static function link_detail($product_permalink)
    {
        $value=base_url().'product/view/'.$product_permalink;
        return $value;
    }

    public function product_permalink($id,$title)
    {
        $seo=function_lib::seo_name($title);
        $permalink=$seo;
        $where='product_permalink="'.$seo.'"';
        if($id!=0)
        {
            $where.=' AND product_id!='.intval($id);
        }
        $is_exist=$this->CI->function_lib->get_one('product_id','sys_product',$where);
        if($is_exist)
        {
            $permalink=$seo.strtotime(date('Y-m-d H:i:s'));
        }
        return $permalink;
    }

    /**
    simpan data
    */
    public function stored($id=0)
    {

      $date=date('Y-m-d');
      $data=array();
      $response_validation=$this->formValidation($id);
      $data['status']=$response_validation['status'];
      $data['message']=$response_validation['message'];
      $adminUsername=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
      if($data['status']==200)
      {
           $product_current_price=$this->CI->input->post('product_current_price',true);
           $product_old_price=$this->CI->input->post('product_old_price',true);
           $product_discount_rupiah=$this->CI->input->post('product_discount_rupiah',true);
           $product_discount_percent=$this->CI->input->post('product_discount_percent',true);
           $product_content=$this->CI->input->post('product_content',true);
           $product_is_active=$this->CI->input->post('product_is_active',true);
           $product_name=($this->CI->input->post('product_name',true)!='')?$this->CI->input->post('product_name',true):'-';
           $permalink=$this->product_permalink($id,$product_name);
           $product_image=$this->CI->input->post('product_image_old'); 
           $columns=array(
                    //'person_title'=>$this->CI->input->post('person_title',true),
                    'product_name'=>$product_name,
                    'product_permalink'=>$permalink,
                    'product_overview'=>$this->CI->input->post('product_overview',true),
                    'product_content'=>htmlentities($product_content),
                    'product_current_price'=>function_lib::remove_currency_rupiah($product_current_price),
                    'product_old_price'=>function_lib::remove_currency_rupiah($product_old_price),
                    'product_discount_rupiah'=>function_lib::remove_currency_rupiah($product_discount_rupiah),
                    'product_discount_percent'=>function_lib::remove_currency_rupiah($product_discount_percent),
                    'product_meta_tags'=>$this->CI->input->post('product_meta_tags',true),
                    'product_meta_desc'=>$this->CI->input->post('product_meta_desc',true),
                    'product_is_active'=>($product_is_active=='N')?'N':'Y',
                    'product_last_update_datetime'=>date('Y-m-d H:i:s'),
                    'product_image'=>($product_image!='')?$product_image:'-',
            );
            $this->setMainTable('sys_product');
            $this->setPostData($columns);
            if($id==0)
            {

              $this->save();
              $id=$this->CI->function_lib->insert_id();
            }
            else
            {
                
              $this->CI->db->update('sys_product',$columns,array('product_id'=>$id));
            }

            $this->create_product_tags($id, $this->CI->input->post('product_meta_tags'));
            
            //handle logo
            $logo=$this->upload_more_images($id);

            $data['status']=200;
            $data['message']='Data berhasil disimpan';

            
            if($logo['status']!=200)
            {
                $data['status']=200;
                $data['message']='Data '.$product_name.' berhasil disimpan, namun terjadi error saat upload foto. ';
            }

      }

      return $data;
    }

    public static function get_product_label($product_criteria_detail_id)
    {
      $lib=new product_lib;
      $label=$lib->CI->function_lib->get_one('product_criteria_detail_label','sys_product_criteria_detail','product_criteria_detail_id='.intval($product_criteria_detail_id));
    
      return $label;
    }

    /** 
    Mengambil nilai stok berdasarkan product id
    @param int $product_id
    @param string $label
    */
    public static function get_stock_by_product_id($product_id, $label){
        $lib=new product_lib;
      $product_criteria_detail_id=$lib->CI->function_lib->get_one('product_criteria_detail_id','sys_product_criteria_detail','product_criteria_detail_product_id='.intval($product_id).' AND product_criteria_detail_label="'.$label.'"');
      $stock=product_lib::get_stock($product_criteria_detail_id);
      return $stock;
    }

    public static function get_stock($product_criteria_detail_id)
    {
      $lib=new product_lib;
      $stock=$lib->CI->function_lib->get_one('item_balance_qty','sys_item_balance','item_balance_product_criteria_detail_id='.intval($product_criteria_detail_id));
    
      return $stock;
    }

    /**
    @param string $type 1=opening balance, 2= pengeluaran order; 3= pemasukan dari pembatalan order; 4=pemasukan dari trx pemasukan
    @param int $ref_trx_id ref transaksi
    @param int $product_criteria_detail_id
    @param int $qty pemasukan dan opening balance plus; pengeluaran minus
    */
    public function save_item_transaction($type,$ref_trx_id,$product_criteria_detail_id,$qty)
    {

        //cek ref trx id and type
        $is_available=$this->CI->function_lib->get_one('item_transaction_id','sys_item_transaction','item_transaction_type="'.$type.'"
          AND item_transaction_reference_detail_id="'.intval($ref_trx_id).'"
          AND item_transaction_product_criteria_detail_id="'.intval($product_criteria_detail_id).'"');

        if(!$is_available)
        {
            
            
            $column=array(
                    'item_transaction_type'=>$type,
                    'item_transaction_reference_detail_id'=>$ref_trx_id,
                    'item_transaction_product_criteria_detail_id'=>$product_criteria_detail_id,
                    'item_transaction_qty'=>$qty,
                    );
            $this->setPostData($column);
            $this->setMainTable('sys_item_transaction');
            $this->save();
        }
        
        //setelah menyimpan transaction, otomatis kalkulasi item balance
        $this->save_item_balance($product_criteria_detail_id,$qty);


    }

    public function save_item_balance($product_criteria_detail_id,$qty)
    {
       //cek ref trx id and type
        $is_available=$this->CI->function_lib->get_one('item_balance_id','sys_item_balance','item_balance_product_criteria_detail_id="'.$product_criteria_detail_id.'"');
        if(!$is_available)
        {
            $column=array(
                      'item_balance_product_criteria_detail_id'=>$product_criteria_detail_id,
                      'item_balance_qty'=>$qty,
                      );
            $this->setPostData($column);
            $this->setMainTable('sys_item_balance');
            $this->save();
        }
        else
        {
            $sql='UPDATE sys_item_balance SET item_balance_qty=item_balance_qty+'.$qty.'
                WHERE item_balance_product_criteria_detail_id='.intval($product_criteria_detail_id).'
            ';
            $this->CI->db->query($sql);
        }
       
    }

    
    /**
     * kategori produk
     * @param int $product_id
     */
    public static function view_product_category($product_id)
    {
        $class_lib=new product_lib;
        $sql='SELECT product_category_name, product_category_id
              FROM sys_product_category_detail
              INNER JOIN sys_product_category ON pcd_product_category_id=product_category_id
              WHERE pcd_product_id='.intval($product_id);
        $exec=$class_lib->CI->db->query($sql);
        $result=$exec->result_array();
        $data=array();
        $html='';
        if(!empty($result))
        {
            $no=1;
            foreach($result AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }
                $html.=($no>1)?', '.$product_category_name:$product_category_name;
            
                $no++;
            }
        }
        
        $data['html']=$html;
        $data['results']=$result;
        return $data;
    }
    
     /**
     * sama dengan method initalizeRecursive(), 
     * tetapi yg ini lebih flexible u/ set parameter where
     * 
     * @param string $where
     */
     public function initalizeRecursiveDynamic($where=1) {
        $this->mainTable='sys_product_category';
        $list_data = $this->findAll($where,'no limit','no offset');
        $data_build = array(
            'items' => array(),
            'parents' => array(),
        );

        foreach ($list_data AS $val) {
            $data_build['items'][$val['product_category_id']] = $val;
            $data_build['parents'][$val['product_category_par_id']][] = $val['product_category_id'];
        }

        return $data_build;
    }
    
    /**
     * recursive tree privilege
     * @param int $parentId 
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     * 
     */
    public function buildTreePrivilege($parentId, $menuData,$no=0,$product_id=0) {
        $id=($no==0)?'class="tree" id="treeList"':'';
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul '.$id.'>';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['product_category_id'];
                $parId = $menuData['items'][$itemId]['product_category_par_id'];
                $name = $menuData['items'][$itemId]['product_category_name'];
                
                $isAllowedAccess=$this->hasChecked($product_id, $id);
                if($isAllowedAccess)
                {
                    $pcd_id=$this->CI->function_lib->get_one('pcd_id','sys_product_category_detail','pcd_product_id='.intval($product_id).'
                        AND pcd_product_category_id='.intval($id));
                    $html.='<input type="hidden" name="pcd_id[]" value="'.$pcd_id.'">';
                }
                $isChecked=$isAllowedAccess?'checked':'';
                $html.='<li>';
                //$html .= '<input type="checkbox" name="menu_administrator_id['.$id.'][id]" value="' . $id . '" id="'.$id.'" '.$isChecked.'> <label for="'.$id.'">'.$name.'</label> '.$editLabel;
                $html .= '<label><input type="checkbox" name="pcd_category_id[]" value="' . $id . '" id="'.$id.'" '.$isChecked.'> <span class="lbl ">'.$name.'</span> </label>';
               
                $no++;
                // find childitems recursively 
                $html .= $this->buildTreePrivilege($itemId, $menuData,$no,$product_id);
                $html.='</li>';
                //$html .= '</tr>'; 
            }
            $html.='</ul>';

        }

        return $html;
    }
    
    /**
     * check has submenu
     * @param int $menuId
     * @return bool true has submenu
     */
    public function hasChecked($product_id,$category_id)
    {
        $value=false;
        $scalar=$this->CI->function_lib->get_one('COUNT(*)','sys_product_category_detail','pcd_product_id='.  intval($product_id).'
            AND pcd_product_category_id='.intval($category_id));
        if(intval($scalar)>0)
        {
            $value=true;
        }
        return $value;
    }
    
    /**
     * product
     * kriteria
     * images
     */
    public function get_more_property($id)
    {
        $sql='
            SELECT * FROM sys_product
            WHERE product_id='.intval($id).'
            ';
        $exec=$this->CI->db->query($sql);
        $results=$exec->result_array();
        
      
        //more images
        $sql_img='
            SELECT * FROM sys_product_more_images
            WHERE product_more_images_product_id='.intval($id).'
            ';
        $exec=$this->CI->db->query($sql_img);
        $results_img=$exec->result_array();
        
        
        return array(
            'product'=>$results,
            'images'=>$results_img,
        );
    }
    
    public function upload_more_images($product_id)
    {
        if(!empty($_FILES))
        {
            $no=1;
            $img_detail_id='';
            foreach($_FILES AS $filesName=>$rowArr)
            {
                $results=$this->handleUpload($product_id,$filesName,$no);
                $post_id=isset($_POST[$filesName.'_id'])?intval($_POST[$filesName.'_id']):0;
                $img_id=($results['id']>0)?$results['id']:$post_id;
                $img_detail_id.=($no>1)?', '.$img_id:$img_id;
                if($results['status']!=200)
                {
//                    $sql='DELETE FROM sys_product_more_images WHERE
//                        product_more_images_product_id='.intval($product_id).'
//                              AND product_more_images_id NOT IN (SELECT spmi.product_more_images_id FROM sys_product_more_images spmi WHERE spmi.product_more_images_product_id='.intval($product_id).')';
//                    $this->CI->db->query($sql);
                    return array(
                        'status'=>500,
                        'message'=>'Gambar '.$_FILES[$filesName]['name'].' gagal diupluoad. ',
                    );
                }
                $no++;
            }
           
            $sql='DELETE FROM sys_product_more_images WHERE product_more_images_id NOT IN ('.$img_detail_id.')
                  AND product_more_images_product_id='.intval($product_id);
            $this->CI->db->query($sql);
            
        }
//        else
//        {
//            if(isset($_POST['']))
//            {
//                
//            }
//        }
        return array(
            'status'=>200,
            'message'=>'success',
        );
    }
    
     /**
     * handle upload
     */
    public function handleUpload($id,$filesName,$no)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
        $id_new=0;
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName,$no);
            $status=$results['status'];
            $message=$results['message'];
            $id_new=$results['id'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$id_new,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name,$no)
    {
        $config=array();
        $pathImage='assets/images/product/';
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        $img_id=0;
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
            if($field_name=='product_image')
            {
                
                if(isset($_POST['product_image_old']) AND trim($_POST['product_image_old'])!='' AND file_exists(FCPATH.$_POST['product_image_old']))
                {
                    unlink(FCPATH.$_POST['product_image_old']);
                }
                
                $where=array(
                'product_id'=>$id,
                );
                $column=array(
                    'product_image'=>$pathImage.$dataImage['file_name'],
                );
                $this->CI->db->update('sys_product',$column,$where);
            }
            else
            {
                if(!isset($_POST[$field_name.'_id']))
                {
                              
                    $column=array(
                        'product_more_images_product_id'=>$id,
                        'product_more_images_file'=>$pathImage.$dataImage['file_name'],
                        'product_more_images_is_active'=>1,
                    );
                    $this->CI->db->insert('sys_product_more_images',$column);
                    $img_id=$this->CI->function_lib->insert_id();
                       
                }
                else
                {
                    $img_id=$_POST[$field_name.'_id'];
                    $old_image=$_POST[$field_name.'_old'];
                    if(file_exists(FCPATH.$old_image) AND trim($old_image)!='')
                    {
                        unlink(FCPATH.$old_image);
                    }
                    $column=array(
                        'product_more_images_product_id'=>$id,
                        'product_more_images_file'=>$pathImage.$dataImage['file_name'],
                        'product_more_images_is_active'=>1,
                    );
                    $where=array(
                        'product_more_images_id'=>$img_id,
                    );
                    $this->CI->db->update('sys_product_more_images',$column,$where);
                    
                }
            }
           
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$img_id,
        );
        
    }
    
    public function validation_product_criteria($status_save,$product_id=null,$function='create')
    {
        if($function=='create')
        {
            $data=array();
        }
        else if($product_id!=null)
        {
            $this->mainTable='sys_product_criteria_detail';
            $where='product_criteria_detail_product_id='.intval($product_id);
            $data=$this->findAll($where, 'no limit', 'offset');
            
        }
        $result=array(
             'status'=>200,
             'message'=>'',
             'data'=>$data,
         );
         if($this->CI->input->post('save'))
         {
             $product_criteria=$this->CI->input->post('product_criteria');
             if($product_criteria=='all')
             {
                 $qty=$this->CI->input->post('product_qty_single_criteria');
                 $data[]=array(
                     'product_criteria_detail_id'=>0,
                     'product_criteria_detail_label'=>'all',
                     'product_criteria_detail_qty'=>$qty,
                 );
             }
             else
             {
                 $label_arr=isset($_POST['product_criteria_detail_label'])?$_POST['product_criteria_detail_label']:array();
                 if(!empty($label_arr))
                 {
                     foreach($label_arr AS $key=>$value)
                     {
                         $id=isset($_POST['product_criteria_detail_id'][$key])?$_POST['product_criteria_detail_id'][$key]:0;
                         $label=isset($_POST['product_criteria_detail_label'][$key])?$_POST['product_criteria_detail_label'][$key]:'-';
                         $qty=isset($_POST['product_criteria_detail_qty'][$key])?$_POST['product_criteria_detail_qty'][$key]:0;
                         if(trim($label)!='')
                         {
                             $data[]=array(
                             'product_criteria_detail_id'=>$id,
                             'product_criteria_detail_label'=>$label,
                             'product_criteria_detail_qty'=>$qty,
                             );
                         }
                         
                     }
                 }
             }

     
             
             if($status_save==200 AND intval($product_id)>0)
             {
                if(!empty($data))
                {
                    $no=1;
                    $id_detail='';
                    foreach($data AS $rowArr)
                    {
                        $column=array(
                            'product_criteria_detail_product_id'=>$product_id,
                            'product_criteria_detail_label'=>$rowArr['product_criteria_detail_label'],
                            'product_criteria_detail_qty'=>$rowArr['product_criteria_detail_qty'],
                        );
                        if(intval($rowArr['product_criteria_detail_id'])==0)
                        {
                                $this->CI->db->insert('sys_product_criteria_detail',$column);
                                $id=$this->CI->function_lib->insert_id();


                                //insert item transaction untuk opening balance
                                $type=1; //type opening balance
                                $ref_trx_id=$id;
                                $product_criteria_detail_id=$id;
                                $qty=$rowArr['product_criteria_detail_qty'];
                                $this->save_item_transaction($type,$ref_trx_id,$product_criteria_detail_id,$qty);
                        }
                        else
                        {
                            $where=array('product_criteria_detail_id'=>intval($rowArr['product_criteria_detail_id']));
                            $this->CI->db->update('sys_product_criteria_detail',$column,$where);
                            $id=$rowArr['product_criteria_detail_id'];
                        }
                        $id_detail.=($no>1)?', '.$id:$id;
                        $no++;
                    }
                    
                    //hapus id yg tak termasuk
                    $sql='DELETE FROM sys_product_criteria_detail WHERE product_criteria_detail_id NOT IN ('.$id_detail.') AND product_criteria_detail_product_id='.intval($product_id);
                    $this->CI->db->query($sql);
                }
                $status=200;
                $message='success';
                $result=array(
                    'status'=>$status,
                    'message'=>$message,
                    'data'=>$data,
                );
             }
             else
             {
                 
                $status=500;
                $message='Gagal melakukan penyimpanan';
                $result=array(
                    'status'=>$status,
                    'message'=>$message,
                    'data'=>$data,
                );
             }
         }
    
         return $result;
    }
    
    public function validation_product_category_detail($product_id=null)
    {
        $data=$this->CI->input->post('pcd_category_id');
       
            
        $result=array(
             'status'=>200,
             'message'=>'',
             'data'=>$data,
         );
         if($this->CI->input->post('save'))
         {
             
                if(!empty($data))
                {
                    $no=1;
                    $id_detail='';
                    foreach($data AS $key=>$value)
                    {
                        $column=array(
                            'pcd_product_id'=>$product_id,
                            'pcd_product_category_id'=>$value,
                        );
                        if(!isset($_POST['pcd_id'][$key]) OR intval($_POST['pcd_id'][$key])<1)
                        {
                                $this->CI->db->insert('sys_product_category_detail',$column);
                                $id=$this->CI->function_lib->insert_id();
                        }
                        else
                        {
                            $where=array('pcd_id'=>intval($_POST['pcd_id'][$key]));
                            $this->CI->db->update('sys_product_category_detail',$column,$where);
                            $id=$_POST['pcd_id'][$key];
                        }
                        $id_detail.=($no>1)?', '.$id:$id;
                        $no++;
                    }
                    
                    //hapus id yg tak termasuk
                    $sql='DELETE FROM sys_product_category_detail WHERE pcd_id NOT IN ('.$id_detail.') AND pcd_product_id='.intval($product_id);
                    $this->CI->db->query($sql);
                }
                $status=200;
                $message='success';
                $result=array(
                    'status'=>$status,
                    'message'=>$message,
                    'data'=>$data,
                );
                
         }
    
         return $result;
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='';
         
         
            $mainConfig = array(
                array(
                        'field'   => 'product_name',
                        'label'   => 'Nama Item',
                        'rules'   => 'trim|required|min_length[4]'
                    ),
                array(
                        'field'   => 'product_current_price',
                        'label'   => 'Harga Sekarang',
                        'rules'   => 'trim|required'
                    ),
                
            );
         
            
         
          $config=$mainConfig;
          /**
           * jika lokasi admin, maka module controller dan method wajib diisi
           * main config dan additional config akan digabung
           */
        
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
          }
          else
          {
            $status=500;
            $message=validation_errors();
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    //dapatkan nomor antrian berdasarkan id order dan id product
    public static function get_waiting_list_number($order_id)
    {
        $ci=&get_instance();
        $ci->load->library('function_lib');
        $waiting_list=$ci->function_lib->get_one('waiting_list_number','sys_order_waiting_list','waiting_list_order_id='.intval($order_id));
        return (trim($waiting_list)!='' AND $waiting_list!=0)?$waiting_list:'-';
    }
    
    public function set_queue_waiting_list($order_id,$status='')
    {
        if($status=='confirmed' OR $status=='rejected')
        {
            $product_id=$this->CI->function_lib->get_one('waiting_list_product_id','sys_order_waiting_list','waiting_list_order_id='.intval($order_id));
            //set 0 no antrian saat ini
            $where='waiting_list_order_id='.intval($order_id).' AND waiting_list_product_id='.intval($product_id);
            $this->mainTable='sys_order_waiting_list';
            $column=array(
                'waiting_list_number'=>0,
                'waiting_list_status'=>$status,
            );
            $this->setPostData($column);
            $this->save($where);

            //set data dibawahnya
            $this->CI->db->query('UPDATE sys_order_waiting_list SET waiting_list_number=waiting_list_number-1 WHERE
                waiting_list_product_id='.intval($product_id).' AND waiting_list_number>1');
        }
        
        
    }
    
    public function current_stock($product_id)
    {
        $saldo_awal=$this->CI->function_lib->get_one('product_opening_balance','sys_product','product_id='.intval($product_id));
        $total_pengeluaran=order_lib::total_pengeluaran_barang($product_id);
        $total_pemasukan=order_lib::total_pemasukan_barang($product_id);
        
        return ($saldo_awal+$total_pemasukan)-$total_pengeluaran;
    }
    
    public function get_waiting_number($product_id)
    {
        $last_id=$this->CI->function_lib->get_one('MAX(waiting_list_number)','sys_order_waiting_list','waiting_list_product_id='.intval($product_id));
        return (trim($last_id)==''?'1':($last_id+1));
    }
    
    public function get_order_waiting_number($order_id, $product_id)
    {
        $last_id=$this->CI->function_lib->get_one('MAX(waiting_list_number)','sys_order_waiting_list','waiting_list_product_id='.intval($product_id));
        return (trim($last_id)==''?'1':($last_id+1));
    }
    
    public static function total_pengeluaran_barang($product_id)
    {
        $ol=new order_lib;
        $query='SELECT SUM(order_detail_quantity) AS total FROM sys_order_detail
            INNER JOIN sys_order ON order_id=order_detail_order_id
            WHERE order_detail_product_id='.intval($product_id).'
                AND order_status NOT IN ("new","rejected")
                GROUP BY order_detail_product_id';
        $exec=$ol->CI->db->query($query);
        $row=$exec->row_array();
        $value=!empty($row)?$row['total']:0;
        return $value;
    }
    public static function total_pemasukan_barang($product_id)
    {
        $ol=new order_lib;
        $query='SELECT SUM(stock_qty) AS total FROM sys_product_stock
            WHERE stock_product_id='.intval($product_id).'
                AND stock_status="pemasukan"
                GROUP BY stock_product_id';
        $exec=$ol->CI->db->query($query);
        $row=$exec->row_array();
        $value=!empty($row)?$row['total']:0;
        return $value;
    }
    
    /**
     * dapatkan data order terakhir berdasarkan member
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $order_by
     * @param int $limit_show_product
     */
    public static function last_member_order_show_label_product($where, $limit, $offset, $orderBy='order_id DESC',$limit_show_product=4)
    {
        $data=array();
        $ol=new order_lib;
        $ol->mainTable='sys_order';
        $join=array(
            'INNER JOIN sys_member ON order_member_id=member_id',
        );
        $select='order_id,order_invoice_no,member_id,member_full_name,order_timestamp';

        $findAll=$ol->findAllCustom($where, $limit, $offset, $orderBy, $select, '', '', $join);
        if(!empty($findAll))
        {
            foreach($findAll AS $key=>$rowArr)
            {
                $order_id=$rowArr['order_id'];
                $order_invoice_no=$rowArr['order_invoice_no'];
                $member_id=$rowArr['member_id'];
                $member_full_name=$rowArr['member_full_name'];
                $order_timestamp=$rowArr['order_timestamp'];
                
                //detail order
                $ol->mainTable='sys_order_detail';
                $where='order_detail_order_id='.intval($order_id);
                $findAll=$ol->findAll($where,'no limit','no offset');
                
                if(!empty($findAll))
                {
                    $num=1;
                    $product='';
                    foreach($findAll AS $rowArr)
                    {
                        foreach($rowArr AS $variable=>$value)
                        {
                            ${$variable}=$value;
                        }
                        if($num<=$limit_show_product)
                        {
                            $product.=($num>1)?', '.$order_detail_product_name:$order_detail_product_name;
                            $num++;
                        }
                    }
                }
                
                $data[$key]['order_id']=$order_id;
                $data[$key]['order_invoice_no']=$order_invoice_no;
                $data[$key]['order_timestamp']=$order_timestamp;
                $data[$key]['member_id']=$member_id;
                $data[$key]['member_full_name']=$member_full_name;
                $data[$key]['product_name']=$product;
                
            }
        }
        return $data;
    }
    
    /**
     * @param array $extraVariable
     * **/
    public function order_confirmation($extraVariable=array())
    {
        $websiteConfig=$this->CI->system_lib->getWebsiteConfiguration();
        $from=$websiteConfig['config_email'];
        $from_name=$websiteConfig['config_name'];
        $is_waiting=isset($websiteConfig['config_ecommerce_waiting'])?$websiteConfig['config_ecommerce_waiting']:0;


            //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        $invoice_no=order_lib::get_invoice_no_from_order_id($order_id);
        $order_billing=$this->show_order_billing($order_id);
        
        $this->setMainTable('sys_order');
        $where='order_id='.intval($order_id);

        $search=array(
                     '{member_email}',
                     '{member_full_name}',
                     '{order_invoice_no}',
                     '{config_site_name}',
                 );
                 $replace=array(
                     $order_billing['member_email'],
                     $order_billing['member_full_name'],
                     $invoice_no,
                     $from_name,
                 );
        $order_note_value=  str_replace($search, $replace, $order_note);
        $postArr=array(
            'order_status'=>$order_status,
            'order_note'=>$order_note_value,
            'order_last_update_datetime'=>date('Y-m-d H:i:s'),
        );
        $this->setPostData($postArr);
        $this->save($where);
        
        if($is_waiting)
        {
            $this->set_queue_waiting_list($order_id, $order_status);
        }
        
        
        if(isset($send_to_email) AND $send_to_email==1)
        {
             $sm=new sendMail;
             if(!empty($order_billing))
             {
                 
                 $subject_value=  str_replace($search, $replace, $subject);
                 $emailProperty=array(
                            'email_to'=>$order_billing['member_email'],
                            'email_subject'=>$subject_value,
                            'email_message'=>$order_note_value,
                            'email_from'=>$from, 
                            'email_from_name'=>$from_name,
                            'email_from_organization'=>$from_name,
                        );
                 $sm->run($emailProperty);
             }
            
        }
        
    }
    
    /**
     * create product tags
     * @param int $product_id
     * @param string $product_meta_tags
     */
    public function create_product_tags($product_id, $product_meta_tags)
    {
        $this->CI->db->trans_start(TRUE); 
        $explode_coma_arr=  explode(',', $product_meta_tags);
        if(!empty($explode_coma_arr))
        {
            //set table
            $no=1;
            $tag_id_combination='';
            foreach($explode_coma_arr AS $key=>$value)
            {
                if(trim($value)!='')
                {
                    //cek jika telah dibuat
                    $tag_id=$this->CI->function_lib->get_one('product_tags_id','sys_product_tags','product_tags_name="'.$this->CI->security->sanitize_filename($value).'"');
                    
                    if(!$tag_id)
                    {
                        //create tag
                        $permalink=  function_lib::seo_name($value);
                        $check_permalink=$this->CI->function_lib->get_one('product_tags_permalink','sys_product_tags','product_tags_permalink="'.$permalink.'"');
                        $new_permalink=$check_permalink?$permalink.time():$permalink;
                        
                            $postArr=array(
                                'product_tags_name'=>$value,
                                'product_tags_permalink'=>$new_permalink,
                            );
                            $this->setPostData($postArr);
                            $this->mainTable='sys_product_tags';

                            $this->save();
                            $tag_id=$this->CI->function_lib->insert_id();
                            
                    }
                    
                    //check has insert in tag detail
                    $check_tag_detail_id=$this->CI->function_lib->get_one('tags_detail_id','sys_product_tags_detail','tags_detail_product_id="'.$product_id.'" AND tags_detail_tags_id='.intval($tag_id));
                    if(!$check_tag_detail_id)
                    {
                         $postArr=array(
                                'tags_detail_tags_id'=>$tag_id,
                                'tags_detail_product_id'=>$product_id,
                            );
                        $this->setPostData($postArr);
                        $this->mainTable='sys_product_tags_detail';
                        $this->save();
                    }
                    
                    
                    
                    $tag_id_combination.=($no>1)?','.$tag_id:$tag_id;
                    
                    $no++;
                }
            }
            if(trim($tag_id_combination)!='')
            {
                
                //delete tags yg tidak terpakai
                $where='tags_detail_tags_id NOT IN('.$tag_id_combination.') AND tags_detail_product_id='.intval($product_id);
                $this->mainTable='sys_product_tags_detail';
                $this->delete($where);
            }
            
            if ($this->CI->db->trans_status() === FALSE)
            {
                $this->CI->db->trans_rollback();
            }
            
            else
            {
               // die('ok1');
               $this->CI->db->trans_complete(); 
            }
        }
    }
     /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->imgPath;
        $pathLocation=FCPATH.$this->imgPath;
        
        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }

     public function getMainTable()
    {
        return $this->mainTable;
    }

    public function setMainTable($mainTable)
    {
        $this->mainTable=$mainTable;
    }

    /**
     * dapatkan img path
     */
    public function getImgPath()
    {
       return $this->imgPath;
    }

    /**
     * dapatkan satu baris data
     * @param string $where
     *
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.'
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='',$having='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.'
              ';
        if(trim($having)!='')
        {
            $sql.=' HAVING '.$having.' ';
        }
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }

        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }

    /**
     * dapatkan semua data dengan fitur join
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT
     * @param string $select
     * @param string $having
     * @param string $groupBy
     * @param array $join
     *          eq. $join=array(
     *              'LEFT JOIN site_administrator_group ON administrator_group_id=admin_group_id',
     *                 );
     * @return array
     */
    public function findAllCustom($where=1,$limit=10,$offset=0,$orderBy='',$select='*',$having='',$groupBy='',$join=array())
    {

        $sql='SELECT '.$select.' FROM '.$this->mainTable.' ';

        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery.' ';
            }
        }

        $sql.=' WHERE '.$where;


        if(trim($groupBy)!='')
        {
            $sql.=' GROUP BY '.$groupBy;
        }

        if(trim($having)!='')
        {
            $sql.=' HAVING '.$having;
        }

        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }

        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);

        return $exec->result_array();
    }


     /**
     * dapatkan satu baris data
     * @param string $where
     *
     * @param array $join
     *          eq. $join=array(
     *              'LEFT JOIN site_administrator_group ON administrator_group_id=admin_group_id',
     *                 );
     * @return array
     */
    public function findCustom($where=1,$join=array())
    {
        $sql='SELECT * FROM '.$this->mainTable.' ';
        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery;
            }
        }
        $sql.=' WHERE '.$where;
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }

    /**
     * dapatkan total data
     * @param string $where
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.'
              ';

        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }

    /**
     * dapatkan total data
     * @param string $select
     * @param string $where
     * @param string $having
     * @param string $groupBy
     * @param string $join
     * @return array
     */
    public function countCustom($select='*',$where='1',$having='', $groupBy='',$join=array())
    {
        $sql='SELECT '.$select.' FROM '.$this->mainTable.' ';
        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery.' ';
            }
        }

        $sql.='WHERE '.$where.' ';

        if(trim($groupBy)!='')
        {
            $sql.=' GROUP BY '.$groupBy;
        }
        if(trim($having)!='')
        {
            $sql.=' HAVING '.$having;
        }
        $exec=$this->CI->db->query($sql);
        $row=$exec->num_rows();
        return is_numeric($row)?$row:0;
    }

    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';

        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;

            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }


        return array(
            'status'=>$status,
            'message'=>$message,
        );

    }


    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {

            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                //ketika klik button save
                $totalData=$this->CI->input->post('save')?count($this->postData)-1:count($this->postData);
                foreach($this->postData AS $column=>$value)
                {
                    if($column!='save')
                    {
                        $separated=($no>=1 AND $no<$totalData)?',':'';
                        $sql.=' '.$column.'="'.$this->CI->security->sanitize_filename($value,true).'"'.$separated;
                        $no++;
                    }

                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }

    /**
     *
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
}
?>
