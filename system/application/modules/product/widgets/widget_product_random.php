<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of widget_support
 *
 * @author Tejo Murti
 */
class widget_product_random extends widget_lib{
    //put your code here
    public function run($module,$view,$output='')
    {
        $ci=&get_instance();
        $ci->load->model(array(
            'product/product_lib',
        ));
        $data=array();
        $model=new product_lib;
        
        $model->setMainTable('sys_product');
        $where='product_is_active="Y"';
        $data['findAll']=$model->findAll($where,3,0,'RAND()');
        $view=  get_class();
        $themes=$ci->config->item('theme');
        $themes_admin_url=base_url().'themes/front/'.$themes['frontend']['name'].'/inc/';
        $data['themes_inc']=$themes_admin_url;
        $this->render($view,$data);
    }
}
?>
