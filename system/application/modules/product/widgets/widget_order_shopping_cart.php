<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of widget_support
 *
 * @author Tejo Murti
 */
class widget_order_shopping_cart extends widget_lib{
    //put your code here
    public function run($module,$view,$output='')
    {
        $ci=&get_instance();
        $ci->load->model(array(
            'order/order_lib',
            'product_catalog/product_catalog_lib',
        ));
        $data=array();
        $model=new order_lib;
        
        $data['resultsArr']=  $model->get_shopping_cart();

      
        $view=  get_class();
     
        $this->render($view,$data);
    }
}
?>
