<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of widget_support
 *
 * @author Tejo Murti
 */
class widget_order_admin_last_status_order extends widget_lib{
    //put your code here
    public function run($module='',$class='',$where='1',$limit=5,$order_by='order_last_update_datetime DESC')
    {
        $ci=&get_instance();
        $ci->load->model(array(
            'order/order_lib',
            'product_catalog/product_catalog_lib',
        ));
        $data=array();
        $model=new order_lib;
        $model->setMainTable('sys_order');
        $data['resultsArr']= $model->findAll($where,$limit,0,$order_by);

      
        $view=  get_class();
     
        $this->render($view,$data);
    }
}
?>
