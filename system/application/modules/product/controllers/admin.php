<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author Tejo Murti
 */
class admin extends admin_controller {
    //put your code here
    protected $mainModel;
    protected $limit=20;
    public function __construct() {
        parent::__construct();
        $this->load->library(array('grocery_CRUD'));
        $this->load->model('product_lib');
        $this->load->helper(array('pagination_helper','date','tinymce'));


        $this->mainModel=new product_lib;
        
    }

    function get_data() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();

        $product_name=$this->input->get('product_name',true);
        $product_content=$this->input->get('product_content',true);
    
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";
        $params['join'] = "
        ";
        $params['where'] = "1";
      
        if(trim($product_name)!='')
        {
            $params['where'].=' AND product_name LIKE "%'.$product_name.'%"';
        }
        if(trim($product_content)!='')
        {
            $params['where'].=' AND product_content LIKE "%'.$product_content.'%"';
        }
       
        $params['order_by'] = "
            product_id DESC
        ";
   
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $status=($product_is_active=='1')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $product_content=html_entity_decode($product_content);
            $product_content_filter=strip_tags($product_content);
            $product_content_filter=(strlen($product_content_filter)<150)?$product_content:substr($product_content_filter,0,150).'...';
           
            $discount_value=($product_old_price>$product_current_price)?($product_old_price-$product_current_price):0;
            $strike_price=($discount_value>0)?'<p style="color:red;text-align:right;font-size:11px;"><strike>'.function_lib::currency_rupiah($product_old_price).'</strike></p>':'';
            $entry = array('id' => $product_id,
                'cell' => array(
                    'actions' =>  '<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$product_id.'" title="Edit"><i class="clip-pencil"></i></a>',
                    'no' =>  $no,
                    'name' =>$product_name,
                    'current_price' =>function_lib::currency_rupiah($product_current_price).$strike_price,
                    'old_price'=>function_lib::currency_rupiah($discount_value),
                    'content'=>$product_content_filter,
                    'timestamp' => date("d-m-Y H:i:s", strtotime($product_timestamp)),
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

     /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {

        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        

        
        $this->data['seoTitle']='Data Produk';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-book",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function add()
    {
       //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
       
        $this->data['seoTitle'] = 'Produk';
        $description = 'Tambah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Produk',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->stored();
            $status=$isValidationPassed['status'];
            $message=$isValidationPassed['message'];

            if($status==200)
            {
               redirect(base_url().'admin/product/add?status='.$status.'&msg='.base64_encode($message));
            }
        }
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    public function update($id=0)
    {
         $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';

        $this->data['seoTitle'] = 'Produk';
        $description = 'Ubah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Produk',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Ubah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        
        if(!is_numeric($id))
        {
            redirect(base_url().  get_class().'/'.$this->currentModule.'/index');
        }
       
        
        $this->data['title_method']='Ubah Produk';
        $this->data['desc_method']='Kategori Produk';
      
        if($this->input->post('save'))
        {     
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->stored($id);
            $status=$isValidationPassed['status'];
            $message=$isValidationPassed['message'];

            if($status==200)
            {
               redirect(base_url().'admin/product/update/'.$id.'?status='.$status.'&msg='.base64_encode($message));
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $results=  $this->mainModel->get_more_property($id);
        if(!empty($results['product']))
        {
            foreach($results['product'] AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    $this->data[$variable]=$value;
                }
            }
        }
        $this->data['images_arr']=$results['images'];
       
        $this->data['footerScript']=$this->footerScript(__FUNCTION__,array(
            'count_img'=>count($this->data['images_arr'])
        ));
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    public function delete_more_images($id=null,$product_id=null)
    {
        $file=$this->function_lib->get_one('product_more_images_file','sys_product_more_images','product_more_images_id='.intval($id));
        $status=500;
        $msg=base64_encode('Gambar tidak ditemukan.');

        if(file_exists(FCPATH.$file) AND trim($file)!='')
        {
            unlink(FCPATH.$file);
            $this->db->query('DELETE FROM sys_product_more_images WHERE product_more_images_id='.intval($id));
            $status=200;
            $msg=base64_encode('Gambar berhasil dihapus.');
        }
        redirect(base_url().'admin/'.$this->currentModule.'/update/'.$product_id.'?status='.$status.'&msg='.$msg);
    }
    
    public function act_delete() {
        $arr_output = array();
        $arr_output['status'] = 500;
        $arr_output['message'] = 'Tidak ada data yang dihapus.';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' '; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('product_name',$this->mainModel->getMainTable(),'product_id='.intval($id));
                            $description=$admin_username.' menghapus produk '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary='product_id';
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail
    
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $this->mainModel->delete('product_id='.intval($id));
                            $deleted_count++;
                        }   
                }
                $arr_output['status'] = 200;
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
            } else {
                $arr_output['status'] = 500;
                $arr_output['message'] = 'Anda belum memilih data.';
            }
        }
        
        echo json_encode($arr_output);
    }

     /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one('product_id',$this->mainModel->getMainTable(),
            'product_id='.intval($id) .' '.$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }
    
    
    /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        ?>
        <script src="<?php echo base_url();?>assets/js/autoNumeric.js"></script>
        

        <!-- flexigrid starts here -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
        <!-- flexigrid ends here -->
    
        <script type="text/javascript">
      
        $(function(){
            currency_rupiah();
        });

        function currency_rupiah()
        {
            var myOptions = {aSep: '.', aDec: ',',vMin: '-999999999.99'};
            $('.currency_rupiah').autoNumeric('init', myOptions); 
        }
        function delete_row(elem)
        {
            var id_tr=elem.attr('id');
            if(confirm('Hapus?'))
            {
                $("tr#tr_"+id_tr).slideUp('medium',function(){
                        $(this).remove();
                });
                return false;
            }
            return false;
        }
        
        var no_tr_more_images=<?php echo isset($count_img)?($count_img+1):'1'?>;    
        function add_row_table_more_images() {
            
            
                    var str= '<tr class="tr_more_images_'+no_tr_more_images+' item_transaction"  id="tr_more_images_'+no_tr_more_images+'">'
                            +'  <td class="center"><a href="#" class="btn btn-mini btn-danger delete" onclick="delete_row($(this));return false;" id="more_images_'+no_tr_more_images+'"><i class="fa fa-trash-o"></i></a></td>'
                            +'  <td class="center td_product_criteria_detail_id">'
                            +'      <input type="file" class="form-control" name="product_more_images_file'+no_tr_more_images+'" />'
                            +'  </td>'
                            
                        +' </tr>';

                $("table#more_image > tbody").fadeIn('medium',function(){
                    $("table#more_image > tbody").append(str);  
                });
                no_tr_more_images++;
            

            return false;
        }
        
        function after_submit()
        {
            var product_overview=$("div#product_overview").html();
            $("textarea[name='product_overview']").val(product_overview);
            var product_description=$("div#product_content").html();
            $("textarea[name='product_content']").val(product_description);
            
            return false;
        }


        function update_harga_wilayah()
        {
            var main_price=$(".main_price").autoNumeric('get')*1;
            var discount_percent=$("input[name='product_discount_percent']").autoNumeric('get')*1;
            var discount_rupiah=$("input[name='product_discount_rupiah']").autoNumeric('get')*1;

            if(discount_percent>0)
            {
                if(discount_percent>100)
                {
                    alert('Nominal diskon tidak boleh melebihi 100%');
                    $("input[name='product_discount_percent']").autoNumeric('set',0);
                    $("input[name='product_discount_rupiah']").autoNumeric('set',0);
                    update_harga_wilayah();
                    return false;
                }
                var discount_rupiah=(discount_percent>0)?(main_price*discount_percent/100):0;
                var new_main_price=main_price-discount_rupiah;
                $("input[name='product_discount_rupiah']").autoNumeric('set',0);
                $(".main_price_sale").autoNumeric('set',new_main_price);

            }
            else
            {
                if(discount_rupiah>main_price)
                {
                    alert('Nominal diskon tidak boleh melebihi harga utama');
                    $("input[name='product_discount_rupiah']").autoNumeric('set',0);
                    $("input[name='product_discount_percent']").autoNumeric('set',0);
                    update_harga_wilayah();
                    return false;
                }
                var discount_percent=(discount_rupiah>0)?((main_price-discount_rupiah)/main_price*100):0;
                var new_main_price=main_price-discount_rupiah;
                $("input[name='product_discount_percent']").autoNumeric('set',0);
                $(".main_price_sale").autoNumeric('set',new_main_price);

            }

            var main_price_sale=$(".main_price_sale").autoNumeric('get')*1;
          
        }

        function discount_product(elem)
        {
            var input_name=elem.attr('name');
            var main_price=$(".main_price").autoNumeric('get')*1;
            var new_main_price=0;
            if(input_name=='product_discount_percent')
            {
                var discount_percent=elem.autoNumeric('get')*1;
                 if(discount_percent>100)
                {
                    alert('Nominal diskon tidak boleh melebihi 100%');
                    $("input[name='product_discount_percent']").autoNumeric('set',0);
                    $("input[name='product_discount_rupiah']").autoNumeric('set',0);
                    return false;
                }
                var discount_rupiah=(discount_percent>0)?(main_price*discount_percent/100):0;
                var new_main_price=main_price-discount_rupiah;
                if(discount_percent>0)
                {
                    $("input[name='product_discount_rupiah']").autoNumeric('set',0);
                }
            }
            else
            {
                var discount_rupiah=elem.autoNumeric('get')*1;
                if(discount_rupiah>main_price)
                {
                    alert('Nominal diskon tidak boleh melebihi harga utama');
                    $("input[name='product_discount_rupiah']").autoNumeric('set',0);
                    $("input[name='product_discount_percent']").autoNumeric('set',0);

                    return false;
                }
                var discount_percent=(discount_rupiah>0)?((main_price-discount_rupiah)/main_price*100):0;
                var new_main_price=main_price-discount_rupiah;
                if(discount_rupiah>0)
                {
                 $("input[name='product_discount_percent']").autoNumeric('set',0);

                }
            }
            $(".main_price_sale").autoNumeric('set',new_main_price);
            update_harga_wilayah();
        }
        </script>
       
        <?php
        
        switch($method)
        {
            case 'create':
            ?>
            <script src="<?php echo base_url()?>assets/js/validate.js"></script>
            <style type="text/css">
            #field { margin-left: .5em; float: left; }
            #field, label { float: left; font-family: Arial, Helvetica, sans-serif; font-size: small; }
            br { clear: both; }
            input { border: 1px solid black; margin-bottom: .5em;  }
            input.error { border: 1px solid red; }
            label.error {
                /* background: url('images/unchecked.gif') no-repeat;*/
                /*   padding-left: 16px;
                    margin-left: .3em;*/
                    display:none !important;
            }
            label.valid {
                    /*background: url('images/checked.gif') no-repeat;*/
                    display: block;
                    width: 16px;
                    height: 16px;
            }
            </style>
            <script type="text/javascript">

              $(document).ready(function(){
                 $(document).bind("keyup",function(e){
                     
                     if(e.which == 119)
                     {
                         $('input[name="save"]').click();
                     }
                });
                
                $('.input_transaction:first').focus();
                input_keyboard();
                // validate the comment form when it is submitted
                $("#form-transaction").validate();
                jQuery.validator.addClassRules("number_format", {
                    required: true,
                    number: true
                });
//                calculate_total_nilai_barang();
//                calculate_nilai_usd();

                
                
            });
            </script>
            <?php
            break;    
            case 'update':
            ?>
            <script src="<?php echo base_url()?>assets/js/validate.js"></script>
            <style type="text/css">
            #field { margin-left: .5em; float: left; }
            #field, label { float: left; font-family: Arial, Helvetica, sans-serif; font-size: small; }
            br { clear: both; }
            input { border: 1px solid black; margin-bottom: .5em;  }
            input.error { border: 1px solid red; }
            label.error {
                /* background: url('images/unchecked.gif') no-repeat;*/
                /*   padding-left: 16px;
                    margin-left: .3em;*/
                    display:none !important;
            }
            label.valid {
                    /*background: url('images/checked.gif') no-repeat;*/
                    display: block;
                    width: 16px;
                    height: 16px;
            }
            </style>
            <script type="text/javascript">

              $(document).ready(function(){
                 $(document).bind("keyup",function(e){
                     
                     if(e.which == 119)
                     {
                         $('input[name="save"]').click();
                     }
                });
                
                $('.input_transaction:first').focus();
                input_keyboard();
                // validate the comment form when it is submitted
                $("#form-transaction").validate();
                jQuery.validator.addClassRules("number_format", {
                    required: true,
                    number: true
                });
//                calculate_total_nilai_barang();
//                calculate_nilai_usd();

                
                
            });
            </script>
            <?php
            break;    
            case 'index':
            ?>
            
            <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Edit', name: 'actions', width: 50, sortable: false, align: 'center' },
                            { display: 'Nama Produk', name: 'name', width: 260, sortable: true, align: 'center' },
                            { display: 'Harga (Rp)', name: 'current_price', width: 160, sortable: true, align: 'right' },
                            { display: 'Diskon', name: 'old_price', width: 160, sortable: true, align: 'right' },
                            { display: 'Deskripsi', name: 'content', width: 460, sortable: true, align: 'center' },
                            { display: 'Tgl. Dibuat', name: 'timestamp', width: 130, sortable: false, align: 'center' },
                           
                        ],
                        buttons: [
                            { display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                            
                        ],
                        buttons_right: [
                        ],
                      
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });
                    
                    function act_delete(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        $("div.alert").show();
                                        if(response['status']==200)
                                        {
                                            $("div.alert").removeClass('alert-error');
                                            $("div.alert").addClass('alert-success');
                                        }
                                        else
                                        {
                                            $("div.alert").removeClass('alert-success');
                                            $("div.alert").addClass('alert-error');
                                        }
                                        $("div.alert").find('.message').html(response['message']);
                                        grid_reload();
                                        return false;

                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    $(document).ready(function() {
                        grid_reload();
                    });

                   

                    function grid_reload() {
                        var product_name=$("#product_name").val();
                        var product_content=$("#product_content").val();
                      
                        var url_service="?product_name="+product_name+"&product_content="+product_content;
                        $("#gridview").flexOptions({url:'<?php echo base_url().'admin/'.$this->currentModule; ?>/get_data'+url_service}).flexReload();
                    }
                    
                    function delete_transaction(id)
                    {
                        if(confirm('Delete?'))
                        {
                             var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$this->currentModule?>/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            $("div.alert").show();

                            if(response['status']==200)
                            {
                                $("div.alert").removeClass('alert-error');
                                $("div.alert").addClass('alert-success');
                            }
                            else
                            {
                                $("div.alert").removeClass('alert-success');
                                $("div.alert").addClass('alert-error');
                            }
                            $("div.alert").find('.message').html(data_arr['message']);

                            grid_reload();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            grid_reload();
                            return false;
                        });
                        }
                        return false;
                       
                    }

                    
                </script>  
            <?php
            break;    
        
        }
        ?>
            <script type="text/javascript">
            function input_keyboard()
            {
                
                $('.input_transaction').bind("keydown", function(e) {
                    if (e.which == 13) 
                    { //Enter key
                    e.preventDefault(); //Skip default behavior of the enter key
                    var n = $(".input_transaction").length;

                    var nextIndex = $('.input_transaction').index(this) + 1;
                        if(nextIndex < n)
                        {
                            if($(this).attr('name')=='input_item_kode')
                            {
                                var class_item='.'+$(this).attr('id');
                                find_item_by_code($(this),class_item);
                            }
                            if($(this).attr('name')=='supplier_kode')
                            {
                                var class_item='.'+$(this).attr('id');
                                find_supplier_by_code($(this));
                            }
                            $('.input_transaction')[nextIndex].focus();
                        }
                        else
                        {
//                            console.log(n);
//                            console.log(nextIndex);
                            $('.input_transaction')[nextIndex-1].blur();
                            $('input[name="save"]').click();
                        }
                    }
                });
                
            
            }
            </script>
        <?php    
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}
?>
