<?php

class f_product extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('product_lib');
        $this->load->helper(array('pagination'));

        $this->mainModel=new product_lib;
        
    }
    
    public function index()
    {

        $where='1';
        $limit=10;
        $total_rows=$this->mainModel->countCustom($where);
        $paginationArr=create_pagination($this->currentModule.'/'.get_class().'/'.__FUNCTION__.'/', $total_rows, $limit );
        $this->data['offset']=$paginationArr['limit'][1];    
        $this->data['pathImgArr']=$this->mainModel->getImagePath();

        $this->data['results']=$this->mainModel->findAllCustom($where,$limit=10,$this->data['offset'],'product_id DESC','*',$having='',$groupBy='',$join=array());
        $this->data['link_pagination']=$paginationArr['links'];
        $title="Paket";

        $desc=$title;
        $key=$title;
        $this->seo($title,$desc,$key);
        template($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }

    public function view($permalink='')
    {

        $where='1';
        $product_id=$this->mainModel->get_product_id_from_permalink($permalink);
        if($product_id<1)
        {
            show_error('Produk tidak ditemukan',404);
        }

        $results=  $this->mainModel->get_more_property($product_id);
        if(!empty($results['product']))
        {
            foreach($results['product'] AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    $this->data[$variable]=$value;
                }
            }
        }
        $this->data['images_arr']=$results['images'];

        $title=$this->data['product_name'];

        $desc=$this->data['product_meta_desc'];
        $key=$this->data['product_meta_tags'];
        $this->seo($title,$desc,$key);

        $this->data['footerScript']=ecom_lib::javascript('add_to_cart_product');
        template($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }

    /**
     * 
     * @author Tejo Murti 
     * @date 2 September 2013
     * memaksimalkan seo dari content yang ada
     * @param string $title
     * @param string $desc
     * @param string $key
     */
    public function seo($title,$desc,$key)
    {
        $seoFunc=function_lib::make_seo($title, $desc, $key);
        $this->data['seoTitle']=$seoFunc['title'];
        $this->data['seoDesc']=$seoFunc['desc'];
        $this->data['seoKeywords']=$seoFunc['keywords'];
    }
}