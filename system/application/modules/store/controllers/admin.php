<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    protected $mainModel;
    //put your code here
    public function __construct() {
        parent::__construct();
        //load model menu

        $this->load->model(array('store_lib'));
   
        
        $this->mainModel=new store_lib;
        
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {

        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        

        
        $this->data['seoTitle']='Toko';
        $description='Daftar Toko/ Cabang';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-book",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function add()
    {

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {

                $columns=array(
                    'store_name'=>$this->input->post('store_name',true),
                    'store_address'=>$this->input->post('store_address',true),
                    'store_telephone_number'=>$this->input->post('store_telephone_number',true),
                    'store_admin_username'=>$this->adminUsername,
                    'store_is_active'=>$this->input->post('store_is_active',true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $id=$this->function_lib->insert_id();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    
                    //statrt audit trail
                    $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                    $name=$this->function_lib->get_one('store_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                    $description=$admin_username.' menambahkan store '.$name.' pada tgl '.date('Y-m-d H:i:s');
                    $table=$this->mainModel->getMainTable();
                    $column_primary=$this->mainModel->primaryId;
                    $primary_value=$id;
                    function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                    //end audit trail

                        $msg=base64_encode('Data berhasil disimpan');
                        redirect(base_url().'admin/'.$this->currentModule.'/index?status=200&msg='.$msg);
                    
                
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Tambah Toko / Cabang';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'store',
                                    'class'=>"clip-book",
                                    'link'=>base_url().'admin/'.$this->currentModule.'/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
      
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data group via form
     * @param int $id
     */
    public function update($id=0)
    {
        
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if($this->input->post('save'))
        {
            
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($id);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {

                $columns=array(
                    'store_name'=>$this->input->post('store_name',true),
                    'store_name'=>$this->input->post('store_name',true),
                    'store_address'=>$this->input->post('store_address',true),
                    'store_telephone_number'=>$this->input->post('store_telephone_number',true),

                    'store_admin_username'=>$this->adminUsername,
                    'store_is_active'=>$this->input->post('store_is_active',true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save($where='store_id='.intval($id));
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {

                    //statrt audit trail
                    $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                    $name=$this->function_lib->get_one('store_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                    $description=$admin_username.' mengubah store '.$name.' pada tgl '.date('Y-m-d H:i:s');
                    $table=$this->mainModel->getMainTable();
                    $column_primary=$this->mainModel->primaryId;
                    $primary_value=$id;
                    function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                    //end audit trail

                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/'.$this->currentModule.'/index?status=200&msg='.$msg);
                    
                
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $dataArr=$this->mainModel->find('store_id='.  intval($id));
        //cek ketersediaan group admin
        if(empty($dataArr))
        {
            show_404();
        }
        foreach($dataArr AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }
       

        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Ubah store';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'store',
                                    'class'=>"clip-book",
                                    'link'=>base_url().'admin/'.$this->currentModule.'/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
       
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    public function delete($id='') {
        $arr_output=array();
        $arr_output['message'] =  ' Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 200;
        $additional_where=' AND store_type="branch"'; //default telah ngecek data dihapus
        if($this->available_data($id,$additional_where))
        {
            //statrt audit trail
            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
            $name=$this->function_lib->get_one('store_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
            $description=$admin_username.' menghapus store '.$name.' pada tgl '.date('Y-m-d H:i:s');
            $table=$this->mainModel->getMainTable();
            $column_primary=$this->mainModel->primaryId;
            $primary_value=$id;
            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
            //end audit trail                
            //delete detail

            $this->mainModel->delete('store_id='.intval($id).' AND store_type="branch"');
            $arr_output['message'] =  ' Data berhasil dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

     public function act_publish() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('publish') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' AND store_is_active="N"';
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('store_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                            $description=$admin_username.' mengaktifkan store '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary=$this->mainModel->primaryId;
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $column=array(                       
                                    'store_is_active'=>'Y'
                                    );
                            $this->mainModel->setPostData($column);
                            $this->mainModel->save('store_id='.intval($id));

                            $deleted_count++;
                        }
                }
                $arr_output['message'] = $deleted_count . ' data berhasil diaktifkan.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

     public function act_unpublish() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('unpublish') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' AND store_is_active="Y" AND store_type="branch"';
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('store_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                            $description=$admin_username.' menonaktifkan store '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary=$this->mainModel->primaryId;
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail

                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $column=array(                       
                                    'store_is_active'=>'N',

                                    );
                            $this->mainModel->setPostData($column);
                            $this->mainModel->save('store_id='.intval($id).'  AND store_type="branch"');

                            $deleted_count++;
                        }
                       
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dinonaktifkan.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

     public function act_delete() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' AND store_type="branch"'; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('store_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                            $description=$admin_username.' menghapus store '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary=$this->mainModel->primaryId;
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail
    

                            $this->mainModel->delete('store_id='.intval($id).' AND store_type="branch"');

                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

    /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one($this->mainModel->primaryId,$this->mainModel->getMainTable(),
            $this->mainModel->primaryId.'='.intval($id).$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }
    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        $store_province_id=isset($store_province_id)?$store_province_id:'';
        $store_city_id=isset($store_city_id)?$store_city_id:'';
        $store_district_id=isset($store_district_id)?$store_district_id:'';
        ?>
             
        <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

        <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/datepicker/css/datepicker.css">
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <?php
        switch($method)
        {
             case 'index':
            ?>
        <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'Actions', name: 'actions', width: 75, sortable: false, align: 'center' },
                    
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Nama', name: 'store_name', width: 260, sortable: true, align: 'center' },
                            { display: 'Jenis', name: 'store_type', width: 260, sortable: true, align: 'center' },
                            { display: 'Telp.', name: 'store_telephone_number', width: 160, sortable: true, align: 'center' },
                            { display: 'Alamat', name: 'store_address', width: 160, sortable: true, align: 'center' },
                            

                            { display: 'Status', name: 'store_is_active', width: 50, sortable: true, align: 'center' },
                            { display: 'Diinput Oleh', name: 'store_admin_username', width: 100, sortable: true, align: 'center' },
                            { display: 'Tanggal', name: 'store_timestamp', width: 130, sortable: false, align: 'center' },
                           
                        ],
                        buttons: [
                            { display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Aktifkan Item Terpilih', name: 'publish', bclass: 'publish', onpress: act_publish },
                            { separator: true },
                            { display: 'NonAktifkan Item Terpilih', name: 'unpublish', bclass: 'unpublish', onpress: act_unpublish },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                        ],
                        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],
                          searchitems: [
                            { display: 'Nama', name: 'store_name', type: 'text', isdefault: true },
                            { display: 'Telp', name: 'store_telephone_number', type: 'text', isdefault: true },
                            { display: 'Alamat', name: 'store_address', type: 'text', isdefault: true },
                            
                            { display: 'Jenis', name: 'store_type', type: 'select', option: 'center:Pusat|branch:Cabang' },
                            { display: 'Status', name: 'store_is_active', type: 'select', option: 'Y:Aktif|N:Draft' },
                          // { display: 'Tanggal', name: 'store_timestamp', type: 'date' },
                            
                        ],
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });
                    
                    function act_delete(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    function act_publish(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_publish',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    function act_unpublish(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_unpublish',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }
                    $(document).ready(function() {
                        grid_reload();
                    });

                   

                    function grid_reload() {
                        $("#gridview").flexOptions({url:'<?php echo base_url().$this->currentModule; ?>/service_rest/get_data'}).flexReload();
                    }
                    
                    function delete_transaction(id)
                    {
                        if(confirm('Delete?'))
                        {
                             var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$this->currentModule?>/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            if(response['status']!=200)
                            {
                                alert(response['message']);
                            }
                            grid_reload();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            grid_reload();
                            return false;
                        });
                        }
                        return false;
                       
                    }

                    
                </script>
            <?php
            break;   
            case 'add'
            ?>
            <script type="text/javascript">
                var coutry_id_default=1; //indonesia
                var elem_default=$("select[name='store_province_id']"); //indonesia
                $(function(){
                    get_province_by_country_id(coutry_id_default,elem_default);
                    get_province_by_country_id(coutry_id_default);
                    //get_sub_area_by_area_id(area_id_val,elem_name);
                    //store province
                    if($.trim('<?=$store_province_id?>')!='')
                    {
                        get_province_by_country_id(coutry_id_default,elem_default,'<?=$store_province_id?>');
                    }
                   
                    if($.trim('<?=$store_city_id?>')!='' || $.trim('<?=$store_province_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$store_province_id?>','store_province_id','<?=$store_city_id?>');
                    }
                   
                    if($.trim('<?=$store_district_id?>')!='' || $.trim('<?=$store_city_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$store_city_id?>','store_city_id','<?=$store_district_id?>');
                    }
                    
                    
                });
                
          

            function get_province_by_country_id(country_id_val,elem,selected_id)
               {
                   // $("select[name='sp_detail_id']").bind('')
                     $.ajax({
                       url:'<?=base_url()?>area/service_rest/get_province_by_country_id',
                       type:'get',
                       dataType:'json',
                       data:{country_id:country_id_val},
                       success:function(response){
                          elem.html('<option value=""> Pilih Provinsi </option>');
                           $.each(response,function(i,rowArr){
                               selected=(selected_id==rowArr['area_id'])?'selected':'';

                               elem.append('<option value="'+rowArr['area_id']+'" '+selected+'>'+rowArr['area_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
               }
            function get_sub_area_by_area_id(area_id_val,elem_name,selected_id)
               {
                   // $("select[name='sp_detail_id']").bind('')
//                   console.log(area_id_val);
//                   console.log(elem_name);
                     $.ajax({
                       url:'<?=base_url()?>area/service_rest/get_sub_area_by_area_id',
                       type:'get',
                       dataType:'json',
                       data:{area_id:area_id_val},
                       success:function(response){
                           switch(elem_name)
                           {
                               case 'store_province_id':
                                   elem=$("select[name='store_city_id']");
                                   elem_label='Pilih Kota/Kabupaten';
                                   current_id=selected_id;
                               break;    
                                 
                               case 'store_city_id':
                                   elem=$("select[name='store_district_id']");
                                   elem_label='Pilih Kecamatan';
                                  current_id=selected_id;

                               break;    
                               
                           }
                           elem.html('<option value=""> '+elem_label+' </option>');
                           $.each(response,function(i,rowArr){
                               selected=(current_id==rowArr['area_id'])?'selected':'';
                               elem.append('<option value="'+rowArr['area_id']+'" '+selected+'>'+rowArr['area_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
               }
        </script>  
            <?php
            break;
            case 'update':
            ?>
            <script type="text/javascript">
                var coutry_id_default=1; //indonesia
                var elem_default=$("select[name='store_province_id']"); //indonesia
                $(function(){
                    get_province_by_country_id(coutry_id_default,elem_default);
                    get_province_by_country_id(coutry_id_default);
                    //get_sub_area_by_area_id(area_id_val,elem_name);
                    //store province
                    if($.trim('<?=$store_province_id?>')!='')
                    {
                        get_province_by_country_id(coutry_id_default,elem_default,'<?=$store_province_id?>');
                    }
                   
                    if($.trim('<?=$store_city_id?>')!='' || $.trim('<?=$store_province_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$store_province_id?>','store_province_id','<?=$store_city_id?>');
                    }
                   
                    if($.trim('<?=$store_district_id?>')!='' || $.trim('<?=$store_city_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$store_city_id?>','store_city_id','<?=$store_district_id?>');
                    }
                    
                    
                });
                
          

            function get_province_by_country_id(country_id_val,elem,selected_id)
               {
                   // $("select[name='sp_detail_id']").bind('')
                     $.ajax({
                       url:'<?=base_url()?>area/service_rest/get_province_by_country_id',
                       type:'get',
                       dataType:'json',
                       data:{country_id:country_id_val},
                       success:function(response){
                          elem.html('<option value=""> Pilih Provinsi </option>');
                           $.each(response,function(i,rowArr){
                               selected=(selected_id==rowArr['area_id'])?'selected':'';

                               elem.append('<option value="'+rowArr['area_id']+'" '+selected+'>'+rowArr['area_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
               }
            function get_sub_area_by_area_id(area_id_val,elem_name,selected_id)
               {
                   // $("select[name='sp_detail_id']").bind('')
//                   console.log(area_id_val);
//                   console.log(elem_name);
                     $.ajax({
                       url:'<?=base_url()?>area/service_rest/get_sub_area_by_area_id',
                       type:'get',
                       dataType:'json',
                       data:{area_id:area_id_val},
                       success:function(response){
                           switch(elem_name)
                           {
                               case 'store_province_id':
                                   elem=$("select[name='store_city_id']");
                                   elem_label='Pilih Kota/Kabupaten';
                                   current_id=selected_id;
                               break;    
                                 
                               case 'store_city_id':
                                   elem=$("select[name='store_district_id']");
                                   elem_label='Pilih Kecamatan';
                                  current_id=selected_id;

                               break;    
                               
                           }
                           elem.html('<option value=""> '+elem_label+' </option>');
                           $.each(response,function(i,rowArr){
                               selected=(current_id==rowArr['area_id'])?'selected':'';
                               elem.append('<option value="'+rowArr['area_id']+'" '+selected+'>'+rowArr['area_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
               }
        </script>  
            <?php
            break;
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
