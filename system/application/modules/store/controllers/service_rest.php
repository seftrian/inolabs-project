<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('store_lib');
        $this->mainModel=new store_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

      function load_data_store_by_id($id)
    {
        $this->mainModel->setMainTable('sys_store');
        $join=array();
      
        $dataArr=$this->mainModel->findCustom("store_id=".  intval($id)." 
            AND store_is_active='Y' ",$join);

        if(!empty($dataArr))
        {
            $status=200;
            $message='Data tersedia';
        }
        else
        {
            $status=500;
            $message='Data tidak ditemukan.';
        }

        echo json_encode(
                array(
                    'status'=>$status,
                    'message'=>$message,
                    'data'=>$dataArr,
                )
            );
    }

     /**
    mendapatkan data produk melalui nama item
    @param string $item_name
    @return json
    */
    public function get_detail_store_by_name($store_name)
    {
        $admin_id=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_id']:0;
        $store_id=function_lib::get_store_id($admin_id);
        $this->mainModel->setMainTable('sys_store');
        $where='store_name LIKE "%'.$this->security->sanitize_filename($store_name).'%"
        AND store_is_active="Y" AND store_id!="'.$store_id.'"';
        $findAll=$this->mainModel->findAll($where,10,0,'store_name ASC');
        $data=array();
        if(!empty($findAll))
        {

            $no=1;
            foreach($findAll AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }
   
                 $data[$no]['id']=$store_id;
                 $data[$no]['label']=$store_name;
                 $data[$no]['value']=$store_name;
                 $no++;

            }
        }
        echo json_encode($data);
    }

    function get_data_store() {
        $admin_id=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_id']:0;
        $store_id=function_lib::get_store_id($admin_id);
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";
      
        $params['where'] = "
            store_is_active='Y'
            AND store_id!='".$store_id."'
        ";  
      
        $params['order_by'] = "
            store_name ASC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $status=($store_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
                     
            $entry = array('id' => $store_id,
                'cell' => array(
                    'actions' =>  '<a href="#" class="btn btn-xs btn-green" onclick="load_data_store_by_id('.$store_id.'); return false;"><i class="clip-checkmark-circle-2"></i></a>',
                    'no' =>  $no,
                    'store_name' =>$store_name,
                    'store_type' =>($store_type=='branch')?'Cabang':'Pusat',
                    'store_telephone_number'=>$store_telephone_number,
                    'store_address'=>$store_address,
                    'store_is_active' => $status,
                    'store_timestamp' => date("d-m-Y H:i:s", strtotime($store_timestamp)),
                    'store_admin_username' => $store_admin_username,
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

    function get_data() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";
        
      
        $params['order_by'] = "
            store_name ASC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $status=($store_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
                     
            $delete_action=($store_type=='branch')?'<a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$store_id.'\');"><i class="clip-remove"></i></a>':'';
            $entry = array('id' => $store_id,
                'cell' => array(
                    'actions' =>  '<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$store_id.'" title="Edit"><i class="clip-pencil"></i></a>
                    '.$delete_action,
                    'no' =>  $no,
                    'store_name' =>$store_name,
                    'store_type' =>($store_type=='branch')?'Cabang':'Pusat',
                    'store_telephone_number'=>$store_telephone_number,
                    'store_address'=>$store_address,
                    'store_is_active' => $status,
                    'store_timestamp' => date("d-m-Y H:i:s", strtotime($store_timestamp)),
                    'store_admin_username' => $store_admin_username,
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
    
}
