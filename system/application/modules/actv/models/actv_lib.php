<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//class admin_privilege extends core_menu_lib{
class actv_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='sys_store';
    public $primaryId='store_id';
    protected $imgPath='assets/images/store/';
    protected $fieldNameImage='store_logo';
    protected $i_key='ifaythankyou';
    protected $dir_folder='assets';
    protected $dir;
    protected $identity_file='identity.json';
    protected $current_server;
    protected $server_activation;
    public function __construct()
    {
        //parent::__construct();
        //$this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation','encrypt','icurl'));
        $directory=FCPATH;
        $this->dir=$directory.$this->dir_folder.'/';
        $this->current_server=base_url().$this->dir_folder.'/'.$this->identity_file;

        //sesuaikan dengan lokasi server
        $this->server_activation='http://inolabs.net/core/';
        $this->limit_generate=50;
    }

      /**
    generate serial
    maksimal hanya 50
    */
    public function buy_activation()
    {
      $data=array();
      $activation_num_days=$this->CI->input->post('activation_num_days');
      $activation_num_generate=$this->CI->input->post('activation_num_generate');
      $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'-';
      $response_validation=$this->formValidationBuy();  
      $data['status']=$response_validation['status'];
      $data['message']=$response_validation['message'];
      if($data['status']==200)
      {
        
            //cek data sebelum disimpan
            $person_full_name=$this->CI->input->post('person_full_name');
            $additional_info_email_address=$this->CI->input->post('additional_info_email_address');
            $person_phone=$this->CI->input->post('person_phone');

            //cek data telah tersedia atau belum
            $where='person_full_name="'.$person_full_name.'" AND person_phone="'.$person_phone.'"';
            $person_id=$this->CI->function_lib->get_one('person_id','sys_person',$where);
            if($person_id<1)
            {
              $column=array(
                'person_full_name'=>$person_full_name,
                'person_phone'=>$person_phone,
                'person_person_type_id'=>0,
                'person_admin_username'=>$admin_username,
              );
              $this->CI->db->insert('sys_person',$column);
              $person_id=$this->CI->function_lib->insert_id();
            }

            $additional_info_id=$this->CI->function_lib->get_one('additional_info_id','sys_person_additional_info','additional_info_person_id='.intval($person_id).' AND additional_info_email_address="'.$additional_info_email_address.'"');
            if($additional_info_id<1)
            {
              //cek email address dan non aktifkan data email sebelumnya
              $sql='UPDATE sys_person_additional_info SET additional_info_is_active="N"
              WHERE additional_info_person_id='.intval($person_id);
              $this->CI->db->query($sql);
              
              //insertkan data yg baru
              $column=array(
                'additional_info_person_id'=>$person_id,
                'additional_info_email_address'=>$additional_info_email_address,
              );
              $this->CI->db->insert('sys_person_additional_info',$column);

            }

            //validasi pembuatan serial
            $buy_time=date("Y-m-d H:i:s");
            $response_serial=$this->generate_activation($person_id,$buy_time);
            $data['status']=$response_serial['status'];
            $data['message']=$response_serial['message'];
            if($data['status']==200)
            {
              //ambil data serial yang telah dibuat dan kirim ke email pembeli dan admin
              $this->send_email_notification($person_id,$additional_info_email_address,$person_full_name,$buy_time);
              $data['status']=200;
              $data['message']='Nomor aktivasi berhasil dikirim.';
            }


           
      }

      return $data;
      
    }

    /**
    kirim serial
    @param int $person_id
    @param $buy_time
    */
    public function send_email_notification($id,$email_address,$full_name,$buy_time)
    {
      $this->CI->load->library('send_mail');
      $sendMail=new send_mail; 
      $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
      $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();
      $this->setMainTable('master_activation');
      $where='activation_person_id='.intval($id).' AND activation_timestamp="'.$buy_time.'"';
      $results=$this->findAll($where,'no limit','no offset','activation_id ASC');
      if(!empty($results))
      {
        $html='<table style="width:300px;">';
        $html.='<tr>';
        $html.='<th style="width:50px;">No.</th>';
        $html.='<th style="width:150px;">Nomor Aktivasi</th>';
        $html.='<th>PIN</th>';
        $html.='<th>Masa Aktif</th>';
        $html.='</tr>';
        $no=1;
        $masa_aktif_arr=$this->activation_type();
        foreach($results AS $rowArr)
        {
          foreach($rowArr AS $variable=>$value)
          {
            ${$variable}=$value;
          }
          $masa_aktif=isset($masa_aktif_arr[$activation_num_days])?$masa_aktif_arr[$activation_num_days]:$activation_num_days;
          $html.='<tr>';
          $html.='<td style="text-align:right;">'.$no.'</td>';
          $html.='<td style="text-align:center;">'.$activation_id.'</td>';
          $html.='<td style="text-align:center;">'.$activation_pin.'</td>';
          $html.='<td style="text-align:center;">'.$masa_aktif.'</td>';
          $html.='</tr>';
          $no++;
        }
        $html.='</table>';

        $message_for_buyer='<p>Dear '.$full_name.',</p>';
        $message_for_buyer.='<p><br /><br />Kami informasikan pembelian nomor aktivasi anda.</p>';
        $message_for_buyer.=$html;

       
         $email_from='noreply@'.$web_name;
         $email_to=$email_address;
         $email_subject='Nomor Aktivasi';
         $email_message=$message_for_buyer;
         $email_to_name=$full_name;
          //email for buyer
         $emailProperty=array(
             'email_to'=>$email_to,
             'email_subject'=>$email_subject,
             'email_message'=>$email_message,
             'email_from'=>'noreply@'.$web_name,
             'email_from_name'=>$web_name,
             'email_from_organization'=>'',
         );
         
         $sendMail->run($emailProperty);


         //send email to admin
         $email_from='noreply@'.$web_name;
         $email_to=$websiteConfig['config_email'];
         $email_subject='Pembelian Nomor Aktivasi';
         $email_message='Pembelian nomor aktivasi berikut oleh '.$full_name.' ('.$email_address.').<br /><br /> '.$html;
         $email_to_name=$web_name;
       
         //email for admin
         $emailProperty=array(
             'email_to'=>$email_to,
             'email_subject'=>$email_subject,
             'email_message'=>$email_message,
             'email_from'=>'noreply@'.$web_name,
             'email_from_name'=>$web_name,
             'email_from_organization'=>'',
         );

         $sendMail->run($emailProperty);
      }

    }

    /**
    generate serial
    maksimal hanya 50
    */
    public function generate_activation($person_id=0,$buy_time='')
    {
      $data=array();
      $activation_num_days=$this->CI->input->post('activation_num_days');
      $activation_num_generate=$this->CI->input->post('activation_num_generate');
      $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'-';
      $response_validation=$this->formValidationGenerate();  
      $data['status']=$response_validation['status'];
      $data['message']=$response_validation['message'];
      if($data['status']==200)
      {
        if($activation_num_generate>50)
        {
          $data['status']=500;
          $data['message']='Maksimal hanya bisa generate '.$this->limit_generate.' aktivasi. ';
        }
        else
        {
          //dapatkan serial terakhir
          $last_activation=$this->CI->function_lib->get_one('activation_id','master_activation','activation_num_days="'.$activation_num_days.'" ORDER BY activation_id DESC');
          if(trim($last_activation)=='')
          {
            $last_activation=$activation_num_days.'00000';
          }

          for($i=1;$i<=$activation_num_generate;$i++)
          {
            $activation_id=number_format(++$last_activation, 0, '', '');

            $column=array(
              'activation_id'=>$activation_id,
              'activation_pin'=>rand(1000,9999),
              'activation_num_days'=>$activation_num_days,
              'activation_created_by'=>$admin_username,
            );
            $column_merge=$column;
            if($person_id!=0 AND strtotime($buy_time)>0)
            {
              $column_merge=array_merge($column,array(
                'activation_person_id'=>$person_id,
                'activation_timestamp'=>$buy_time,
              ));  
            }
            $this->CI->db->insert('master_activation',$column_merge);
            $last_activation=$activation_id;


          }

          $data['status']=200;
          $data['message']='Nomor aktivasi berhasil dibuat.';
        }
      }

      return $data;
      
    }

    /**
    jenis masa aktif
    */
    public static function activation_type()
    {
      $data=array();
      $data['30']='1 Bln.';
      $data['90']='3 Bln.';
      $data['180']='6 Bln.';
      $data['365']='1 Th.';
    
      return $data;
    }

    /**
    cek telah expired atau belum
    */
    public function check_has_expired()
    {
        $today=date('Y-m-d');
        $current_identity=$this->read_identity();
        $data=array();
        $data['status']=200;
        $data['message']='';
        if(strtotime($current_identity)<strtotime($today))
        {
            $show_notification_alert=$this->show_notification_alert();
            $data['status']=$show_notification_alert['status'];
            $data['message']=$show_notification_alert['message'];
        }
        return $data;
    }

    /**
    simpan expired date
    @param string $ed
    */
    public function save_identity($ed)
    {
        
            $status=200;
            $message='OK';
            $data=$ed;
            $file=$this->dir.$this->identity_file;
            if(file_exists($file) AND trim($file)!='')
            {
                unlink($file);
            }
            if(!file_exists($file) AND trim($file)!='')
            {
                $fp = fopen($file, 'w');
                fwrite($fp, json_encode($data));
                fclose($fp);

            }

    }

    /**
    verifikasi ke server
    */

    public function verification_to_server()
    {
        $this->CI->load->library('icurl');   
         $url=$this->server_activation.'actv/verification_to_server';
         $icurl=new icurl;
         $icurl->setUrl($url);
         $icurl->setVerb('post');
         $post_data=array(
            'activation_id'=>$this->CI->input->post('activation_id'),
            'activation_pin'=>$this->CI->input->post('activation_pin'),
            'submit'=>1,
         );
         $icurl->setRequestBody($post_data);
         $icurl->execute();
         $response=  $icurl->getResponseBody();
         $obj_response=json_decode($response);
         if(isset($obj_response->status) AND $obj_response->status==200)
         {
            $activation_expired_date=$obj_response->activation_expired_date;
            $decrypted_string = $this->CI->encrypt->encode($activation_expired_date, $this->i_key);
            $this->save_identity($decrypted_string);
         } 
         return $obj_response;
    }
    
    /**
    dapatkan  id aktivasi
    */
    public function verification_activation()
    {
        $data=array();
        $response=$this->formValidation();
        $data['status']=$response['status'];
        $data['message']=$response['message'];
        $current_identity=$this->read_identity();
        $today=date('Y-m-d');
        $data['activation_expired_date']=$current_identity;
          //dapatkan ip address
        $client_ip=function_lib::get_client_ip();
        $client_ip=($client_ip=='::1')?'127.0.0.1':$client_ip;
        
        if($data['status']==200)
        {
            $activation_id=$this->CI->input->post('activation_id',true);
            $activation_pin=$this->CI->input->post('activation_pin',true);
             
            $where='activation_id="'.$activation_id.'"
            AND activation_pin="'.$activation_pin.'"
            AND activation_has_used="N"';
            $is_exist=$this->CI->function_lib->get_one('activation_num_days','master_activation',$where);
            if(trim($is_exist)!='')
            {
                $add_days=$is_exist;
                //jika hari ini <= dari hari sisa aktivasi 
                if(strtotime($today)<=strtotime($current_identity))
                {
                    $datediff=strtotime($current_identity)-strtotime($today);
                    $add_days+=floor($datediff/(60*60*24));
                }

                $year=substr($today,0,4);
                $month=substr($today,5,2);
                $day=substr($today,8,2);

                $activation_expired_date=date('Y-m-d',mktime(0,0,0,$month,($day+$add_days),$year));
              
                $data['status']=200;
                $data['message']='Sukses! Selamat, aplikasi anda berhasil diperpanjang hingga '.convert_date($activation_expired_date,'','','ina').'.';
                $data['activation_expired_date']=$activation_expired_date;
              //  $decrypted_string = $this->CI->encrypt->encode($activation_expired_date, $this->i_key);
               
               // $this->save_identity($decrypted_string);
                //$read_identity=$this->read_identity();
              
                //update serial telah digunakan
                $sql='UPDATE  master_activation SET activation_has_used="Y",
                                                    activation_expired_date="'.$activation_expired_date.'",
                                                    activation_used_date="'.$today.'",
                                                    activation_ip_address=INET_ATON("'.$client_ip.'")
                                                    WHERE activation_id="'.$activation_id.'"';
                $this->CI->db->query($sql);                                                        
            }
            else
            {
                $data['status']=500;
                $data['message']='Error! Kode aktivasi atau kode pin tidak ditemukan.';
            }
        }


        return $data;
    }

    /**
    notifikasi alert
    @param string $min - tampilkan notifikasi  
    */
    public function show_notification_alert($min=10)
    {
        $data=array();
        $data['status']=200;
        $data['message']='Selamat menggunakan aplikasi. ';
        $read_identity=$this->read_identity();
        $today=date('Y-m-d');
        //$today='2015-06-01';
        //saat expired
        if(strtotime($read_identity)<strtotime($today))
        {
            $data['status']=500;
            $data['message']='Pelanggan yang terhormat, masa aktif pemakaian telah berakhir. 
            Mohon segera melakukan aktivasi untuk dapat menggunakannya kembali.';
        }
        else
        {
            $year=substr($read_identity,0,4);
            $month=substr($read_identity,5,2);
            $day=substr($read_identity,8,2);
            $day_before_expired=date('Y-m-d',mktime(0,0,0,$month,($day-$min),$year));

            if(strtotime($today)>=strtotime($day_before_expired))
            {
                $data['status']=500;
                $data['message']='Masa aktif aplikasi akan berakhir pada tanggal '.convert_date($read_identity,'','','ina').', pastikan anda telah
                melakukan pembelian nomor aktivasi.';
            }
        }

        return $data;
    }

    /**
    baca hasil decrypt
    */
    public function read_identity()
    {
         $this->CI->load->library('icurl');   
         $url=$this->current_server;
         $icurl=new icurl;
         $icurl->setUrl($url);
         $icurl->setVerb('GET');
         $icurl->execute();
         $status=  $icurl->getResponseBody();
         $decode_value='1970-01-01';
         if(trim($status)!='')
         {
            $response=json_decode($status);
            $dec=$this->CI->encrypt->decode($response, $this->i_key);
            if(strtotime($dec)>0)
            {
                $decode_value=$dec;
            }
         }
         return $decode_value;
    }

      /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->imgPath;
        $pathLocation=FCPATH.$this->imgPath;
        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
  
    public function upload_more_images($id)
    {
        if(!empty($_FILES))
        {
            $no=1;
            $img_detail_id='';
            foreach($_FILES AS $filesName=>$rowArr)
            {
                $results=$this->handleUpload($id,$filesName,$no);
                $post_id=isset($_POST[$filesName.'_id'])?intval($_POST[$filesName.'_id']):0;
                $img_id=($results['id']>0)?$results['id']:$post_id;
                $img_detail_id.=($no>1)?', '.$img_id:$img_id;
                if($results['status']!=200)
                {
                   return array(
                        'status'=>500,
                        'message'=>'Gambar '.$_FILES[$filesName]['name'].' gagal diunggah. ',
                    );
                }
                $no++;
            }
            
        }
        return array(
            'status'=>200,
            'message'=>'success',
        );
    }
    
     /**
     * handle upload
     */
    public function handleUpload($id,$filesName,$no)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
        $id_new=0;
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName,$no);
            $status=$results['status'];
            $message=$results['message'];
            $id_new=$results['id'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$id_new,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name,$no)
    {
        $config=array();
        $pathImage=$this->imgPath;
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        $img_id=0;
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
            if($field_name==$this->fieldNameImage)
            {
                
                if(isset($_POST[$this->fieldNameImage.'_old']) AND trim($_POST[$this->fieldNameImage.'_old'])!='' AND file_exists(FCPATH.$_POST[$this->fieldNameImage.'_old']))
                {
                    unlink(FCPATH.$_POST[$this->fieldNameImage.'_old']);
                }
                
                $where=array(
                $this->primaryId=>$id,
                );
                $column=array(
                    $this->fieldNameImage=>$pathImage.$dataImage['file_name'],
                );
                $this->CI->db->update($this->mainTable,$column,$where);
            }

           
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;

        }
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$img_id,
        );
        
    }

     /**
     * form validasi pembelian serial
     */
    public function formValidationBuy()
    {

         $status=500;
         $message='';
         $config = array(
                array(
                     'field'   => 'activation_num_days',
                     'label'   => 'Jenis Aktivasi',
                     'rules'   => 'trim|required'
                ),
                array(
                     'field'   => 'activation_num_generate',
                     'label'   => 'Jumlah',
                     'rules'   => 'trim|required'
                ),
                array(
                     'field'   => 'person_full_name',
                     'label'   => 'Nama Pembeli',
                     'rules'   => 'trim|required'
                ),
                array(
                     'field'   => 'additional_info_email_address',
                     'label'   => 'Email Pembeli',
                     'rules'   => 'trim|required|valid_email'
                ),
                array(
                     'field'   => 'person_phone',
                     'label'   => 'No. Hp. Pembeli',
                     'rules'   => 'trim|required'
                ),

            );
         
           $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {

              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
          }
          else
          {
                $status=500;
                $message=validation_errors();
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }

    /**
     * untuk validasi form 
     * @param int $id default 0
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidationGenerate($id=0)
    {

         $status=500;
         $message='';
         $config = array(
                array(
                     'field'   => 'activation_num_days',
                     'label'   => 'Jenis Aktivasi',
                     'rules'   => 'trim|required'
                ),
                array(
                     'field'   => 'activation_num_generate',
                     'label'   => 'Jumlah',
                     'rules'   => 'trim|required'
                ),

            );
         
           $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {

              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
          }
          else
          {
                $status=500;
                $message=validation_errors();
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }

 /**
     * untuk validasi form 
     * @param int $id default 0
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {

         $status=500;
         $message='';
         $config = array(
                array(
                     'field'   => 'activation_id',
                     'label'   => 'Nomor Aktivasi',
                     'rules'   => 'trim|required'
                ),
                array(
                     'field'   => 'activation_pin',
                     'label'   => 'Nomor PIN',
                     'rules'   => 'trim|required'
                ),

            );
         
           $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {

              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
          }
          else
          {
                $status=500;
                $message=validation_errors();
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }

    public function getMainTable()
    {
        return $this->mainTable;
    }

    /**
     * set main table
     */
    public function setMainTable($table)
    {
        $this->mainTable=$table;
    }
    
   
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }

      /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function findCustom($where=1,$join=array())
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              ';
        if(!empty($join)) 
        {
            foreach($join AS $value)
            {
              $sql.=$value;
            }
        }     
        $sql.= ' WHERE '.$where;
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * proses penyimpanan data
     * @param int $id default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
