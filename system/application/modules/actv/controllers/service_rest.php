<?php
class service_rest extends front_controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('actv_lib');
        $this->mainModel=new actv_lib;
    }


    function get_data() {
        $today=date('Y-m-d');

        $from_pay=$this->input->get('from_pay');
        $to_pay=$this->input->get('to_pay');
        $from_pay_time=$this->input->get('from_pay_time');
        $to_pay_time=$this->input->get('to_pay_time');
        $from_due_date=$this->input->get('from_due_date');
        $to_due_date=$this->input->get('to_due_date');
        $delivery_order_code=$this->input->get('delivery_order_code');
        $status=$this->input->get('status');

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";
        $params['where']='1';
        if($from_pay!='' AND $to_pay!='')
        {
            if($from_pay_time!='' AND $to_pay_time!=''){
                $params['where'].=' AND inkaso_header_last_update_datetime >= "'.date('Y-m-d',strtotime($from_pay)).' '.date('H:i:s',strtotime($from_pay_time)).'"
                                    AND inkaso_header_last_update_datetime <= "'.date('Y-m-d',strtotime($to_pay)).' '.date('H:i:s',strtotime($to_pay_time)).'"';
            } else {
                $params['where'].=' AND DATE(inkaso_header_last_update_datetime) BETWEEN "'.date('Y-m-d',strtotime($from_pay)).'"
                AND "'.date('Y-m-d',strtotime($to_pay)).'"';
            }
        }
        if($from_due_date!='' AND $to_due_date!='')
        {
            $params['where'].=' AND inkaso_header_due_date BETWEEN "'.date('Y-m-d',strtotime($from_due_date)).'"
            AND "'.date('Y-m-d',strtotime($to_due_date)).'"';
        }
        if($delivery_order_code!='')
        {
            $params['where'].=' AND inkaso_header_delivery_order_code LIKE "%'.$delivery_order_code.'%"';
        }
        if($status!='')
        {
            $where_status=($status=='paid')?' AND inkaso_header_rest_of_bill=0':(
                ($status=='not_paid')?' AND inkaso_header_rest_of_bill>0':(
                    ($status=='not_due_date')?' AND inkaso_header_due_date>"'.$today.'"':' AND inkaso_header_due_date<"'.$today.'"'
                    )
                );
            $params['where'].=$where_status;
        }
        $params['where'].=' AND inkaso_header_delivery_order_id IN (
            SELECT delivery_order_id 
            FROM sys_delivery_order
            WHERE delivery_order_id=inkaso_header_delivery_order_id
            AND delivery_order_is_used="Y"
            )';
      
        $params['order_by'] = "
            inkaso_header_last_update_datetime DESC,
            inkaso_header_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);
        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            foreach($row as $variable => $value) {
                ${$variable} = $value;
            }
            $no++;
            $pay='<a href="'.base_url().'admin/'.$this->currentModule.'/pay/'.$inkaso_header_id.'" class="btn btn-xs btn-purple"><i class="fa fa-shopping-cart"></i> Bayar</button>';
            $paid=($inkaso_header_rest_of_bill==0)?'<i class=" clip-checkmark-circle"></i> Lunas</span> ':'<a href="'.base_url().'admin/'.$this->currentModule.'/pay/'.$inkaso_header_id.'" class="btn btn-xs btn-warning"><i class="fa fa-shopping-cart"></i> Bayar</button>';
          //  $view='<a href="'.base_url().'admin/'.$this->currentModule.'/view/'.$inkaso_header_id.'" class="btn btn-xs btn-info"><i class="fa fa-search"></i> Detil</a>';
            $has_due_date=($today>$inkaso_header_due_date AND $inkaso_header_rest_of_bill>0)?'<a href="'.base_url().'admin/'.$this->currentModule.'/pay/'.$inkaso_header_id.'"><span class="btn btn-xs btn-danger">Telah jatuh tempo!</span></a>':'';
            $print_trx='<a class="btn btn-xs btn-default" title="Print" onclick="cetak_struk('.$inkaso_header_id.',0);return false;"><i class="fa fa-print"></i></a>';

             //Pilihan Opsi Print
            $opsi_print ='<div class="dropdown" style="position:absolute;padding:0px;">
                        <div class="btn-group" style="margin-top:-10px;">
                          <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Pilih Opsi
                          </button>
                          <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="text-align:left">
                            <li><a href="#" title="Print" onclick="cetak_struk('.$inkaso_header_id.',0);return false;"><i class="fa fa-print"></i> Nota</a></li>
                            <li><a href="#" title="Print" onclick="cetak_kwitansi('.$inkaso_header_id.',0);return false;"><i class="fa fa-print"></i> Kwitansi</a></li>
                          </ul>
                        </div>
                    </div>';

            //dapatkan data detail
            $sql='SELECT * FROM trx_inkaso_detail WHERE inkaso_detail_inkaso_header_id='.intval($inkaso_header_id);
            $exec=$this->db->query($sql);
            $results=$exec->result_array();
            if(!empty($results))
            {
                $index=1;
                foreach($results AS $rowDetail)
                {
                    foreach($rowDetail AS $variableDetail=>$valueDetail){
                        ${$variableDetail}=$valueDetail;
                    }
                    $delete_inkaso_detail='<a href="#" onclick="delete_transaction('.$inkaso_detail_id.');return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> </a>';
                    $print_trx_detail='<a class="btn btn-xs btn-default" title="Print" onclick="cetak_struk('.$inkaso_header_id.','.$inkaso_detail_id.');return false;"><i class="fa fa-print"></i></a>';
                     $entry = array('id' => $inkaso_header_id.'_'.$inkaso_detail_id,
                        'cell' => array(
                            'no' => ($index==1)? $no . '.':'',
                            'pay' =>   ($index==1)?$paid:'',
                            'print_trx' =>  ($index==1) ? $opsi_print : '',
                            'store_id' => ($index==1)?$inkaso_header_store_id:'',
                            'delivery_order_code' => ($index==1)?$inkaso_header_delivery_order_code.'<br />'.$has_due_date:'',
                            'total_bill' => ($index==1)?function_lib::currency_rupiah($inkaso_header_total_bill):'',
                            'total_paid' => ($index==1)?function_lib::currency_rupiah($inkaso_header_total_paid):'',
                            'rest_of_bill' => ($index==1)?function_lib::currency_rupiah($inkaso_header_rest_of_bill):'',
                            'last_update_datetime' => ($index==1)?date('d/m/Y H:i:s',strtotime($inkaso_header_last_update_datetime)):'',
                            'due_date' => ($index==1)?date('d/m/Y',strtotime($inkaso_header_due_date)):'',
                            'detail_code' => $delete_inkaso_detail.' '.$print_trx_detail.' <i>'.$inkaso_detail_code.'</i>',
                            'detail_amount' => function_lib::currency_rupiah($inkaso_detail_amount),
                            'detail_payment_method' => $inkaso_detail_payment_method,
                            'detail_payment_bank' => $inkaso_detail_payment_bank,
                            'detail_transaction_number' => $inkaso_detail_transaction_number,
                            'detail_note' => $inkaso_detail_note,
                            'detail_admin_username' => $inkaso_detail_admin_username,
                            'detail_datetime' => date('d/m/Y H:i:s',strtotime($inkaso_detail_datetime)),
                        ),
                    );
                    $json_data['rows'][] = $entry;
                    $index++;
                }
            }
            //menampilkan data yg belum ada pembayarannya
            else
            {
                 $entry = array('id' => $inkaso_header_id.'_',
                        'cell' => array(
                            'no' => $no . '.',
                            'pay' =>   $paid,
                            'store_id' => $inkaso_header_store_id,
                            'print_trx' =>  $opsi_print ,
                            'delivery_order_code' => $inkaso_header_delivery_order_code.'<br />'.$has_due_date,
                            'total_bill' => function_lib::currency_rupiah($inkaso_header_total_bill),
                            'total_paid' => function_lib::currency_rupiah($inkaso_header_total_paid),
                            'rest_of_bill' => function_lib::currency_rupiah($inkaso_header_rest_of_bill),
                            'last_update_datetime' => date('d/m/Y H:i:s',strtotime($inkaso_header_last_update_datetime)),
                            'due_date' => date('d/m/Y',strtotime($inkaso_header_due_date)),
                            'detail_code' => 'n/a',
                            'detail_amount' => 'n/a',
                            'detail_payment_method' => 'n/a',
                            'detail_payment_bank' => 'n/a',
                            'detail_transaction_number' => 'n/a',
                            'detail_note' => 'n/a',
                            'detail_admin_username' => 'n/a',
                            'detail_datetime' => 'n/a',
                        ),
                    );
                    $json_data['rows'][] = $entry;
            }
           
        }
        echo json_encode($json_data);
    }

}