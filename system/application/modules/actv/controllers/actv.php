<?php

class actv extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library(array('function_lib','encrypt'));
        $this->load->model('actv_lib');
        $this->mainModel=new actv_lib;

    }

    public function index()
    {
        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        
        
        $this->data['seoTitle']='Aktivasi';
        $description='';
       
        if($this->input->post('submit') AND $this->input->post('submit')==1)
        {

            $response=$this->mainModel->verification_to_server();
         
            $status=isset($response->status)?$response->status:500;
            $message=isset($response->message)?$response->message:'Error! Tidak dapat terhubung ke server. Mohon cek koneksi anda.';
            if($status==200)
            {
                redirect(base_url().'actv/index?status='.$status.'&msg='.base64_encode($message));
                exit;
            }
          
        }

        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['show_notification_alert']=$this->mainModel->show_notification_alert();
        $this->load->view_single($this->themeId,get_class().'/'.__FUNCTION__,$this->data);//at last masukkan semua datanya
    }

    public function verification_to_server()
    {
        $response=$this->mainModel->verification_activation();

        echo json_encode($response);
    }

    public function test()
    {
        $msg = '2015-04-01#2015-04-30';
        $key = 'super-secret-key';

        $encrypted_string = $this->encrypt->encode($msg, $key);
        echo $encrypted_string.'<br /><br />';

        //$msg = 'My secret message';
        //$key = 'super-secret-key';
        //$encrypted_string='VUsNeA59A3RXYwE2ViBYYFIkAHUCOltkUiYAdwE2UmUGMA==';
        $decrypted_string = $this->encrypt->decode($encrypted_string, $key);
        echo $decrypted_string.'<br /><br />';
    }

    /**
    dapatkan kode aktivasi
    */
    public function bank_activation()
    {
        $data=array();
        $data[]=array(
            'activation_id'=>123456789,
            'activation_company_id'=>1,
            'activation_expired_date'=>'2015-05-31',
            'activation_has_used'=>'N',
            'activation_used_date'=>'',
            'activation_create_datetime'=>date('Y-m-d'),
        );

        return $data;
    }

    public function test_expired()
    {
        $response=$this->mainModel->show_notification_alert($min=100);
        // echo '<pre>';
        // print_r($response);
    }
    
}
