<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
class admin extends admin_controller{
    
    protected $mainModel;
    //put your code here
    public function __construct() {
        parent::__construct();
        //load model menu

        $this->load->model(array('actv/actv_lib'));
   
        $this->mainModel=new actv_lib;
        
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }

      /**
     * untuk update data group
     * @param int $groupId
     */
    public function generate()
    {
       
        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
      
        $this->data['seoTitle']='Generate Nomor Aktivasi';
        $description='Pembuatan nomor aktivasi produk.';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );

         $breadcrumbs_array[]= array(
                                    'name'=>'Nomor Aktivasi',
                                    'class'=>"clip-wrench",
                                    'link'=>base_url().'admin/actv/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );

        if($this->input->post('save'))
        {
            $response=$this->mainModel->generate_activation();
            $status=$response['status'];
            $message=$response['message'];

            if($response['status']==200)
            {
                redirect(base_url().'admin/'.$this->currentModule.'/'.__FUNCTION__.'?status='.$status.'&msg='.base64_encode($message));
            }
        } 
          $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $this->data['activation_type']=actv_lib::activation_type();
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

      /**
     * untuk update data group
     * @param int $groupId
     */
    public function buy()
    {
       
        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
      
        $this->data['seoTitle']='Pembelian Nomor Aktivasi';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );

         $breadcrumbs_array[]= array(
                                    'name'=>'Nomor Aktivasi',
                                    'class'=>"clip-wrench",
                                    'link'=>base_url().'admin/actv/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );

        if($this->input->post('save'))
        {
            $response=$this->mainModel->buy_activation();
            $status=$response['status'];
            $message=$response['message'];

            if($response['status']==200)
            {
                redirect(base_url().'admin/'.$this->currentModule.'/'.__FUNCTION__.'?status='.$status.'&msg='.base64_encode($message));
            }
        } 
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $this->data['activation_type']=actv_lib::activation_type();
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

    function get_data() {
        $today=date('Y-m-d');

        $from_pay=$this->input->get('from_pay');
        $to_pay=$this->input->get('to_pay');
        $from_pay_time=$this->input->get('from_pay_time');
        $to_pay_time=$this->input->get('to_pay_time');
        $from_due_date=$this->input->get('from_due_date');
        $to_due_date=$this->input->get('to_due_date');
        $delivery_order_code=$this->input->get('delivery_order_code');
        $status=$this->input->get('status');

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = 'master_activation';
        $params['select'] = "
            master_activation.*
        ";
        $params['where']='1';
        
        $params['where'].='';
      
        $params['order_by'] = "
            activation_id ASC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);
        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        $num_days_arr=actv_lib::activation_type();
        foreach ($query->result() as $row) {
            foreach($row as $variable => $value) {
                ${$variable} = $value;
            }
            $no++;
            $label_num_days=isset($num_days_arr[$activation_num_days])?$num_days_arr[$activation_num_days]:$activation_num_days;
            $entry = array('id' => $activation_id,
                    'cell' => array(
                        'no' => $no . '.',
                        'action' =>   '',
                        'id' =>   $activation_id,
                        'num_days' => $label_num_days,
                        'used_date' => $activation_used_date ,
                        'ip_address' =>  $activation_used_date ,
                    ),
                );
            $json_data['rows'][] = $entry;
           
        }
        echo json_encode($json_data);
    }

    /**
     * untuk update data group
     * @param int $groupId
     */
    public function index()
    {
       
        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
      
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['seoTitle']='Nomor Aktivasi';
        $description='Daftar nomor aktivasi produk.';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );

         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-wrench",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
 
       /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='', $extraVariable=array()) {
        //set variable
        if(!empty($extraVariable)) {
            extract($extraVariable);
        }
        ob_start();
        switch($method) {
            case 'index':
                ?>
                <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/datepicker/css/datepicker.css">
                <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
                <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

                <!-- flexigrid ends here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.date.format.min.js"></script>
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            { display: 'No', name: 'no', width: 40, sortable: false, align: 'right' },
                           // { display: 'Aksi', name: 'action', width: 70, sortable: false, align: 'center' },
                            { display: 'Nomor Aktivasi', name: 'id', width: 150, sortable: false, align: 'center' },
                            { display: 'Masa Aktif', name: 'num_days', width: 150, sortable: false, align: 'center' },
                            { display: 'Tgl. Diaktifkan', name: 'used_date', width: 150, sortable: true, align: 'center' },
                            //{ display: 'IP Address', name: 'ip_address', width: 150, sortable: true, align: 'center' },
                        ],
                        buttons_right: [
                        ],
                        sortname: "activation_id",
                        sortorder: "asc",
                        usepager: true,
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });
                    

                    $(document).ready(function() {
                        grid_reload();
                        
                        $( ".from" ).datepicker({
                          defaultDate: "+1w",
                          changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat:'dd-mm-yy',
                          onClose: function( selectedDate ) {
                            $( ".to" ).datepicker( "option", "minDate", selectedDate );
                          }
                        });
                        $( ".to" ).datepicker({
                          defaultDate: "+1w",
                          changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat:'dd-mm-yy',
                          onClose: function( selectedDate ) {
                            $( ".from" ).datepicker( "option", "maxDate", selectedDate );
                          }
                        });
                    });

                    function grid_reload() {
                        var id=$("#id").val();
                        var start_date=$("#start_date").val();
                        var end_date=$("#end_date").val();
                        var status=$("#status").val();
                        var num_days=$("#num_days").val();
                        var link_service='?id='+id+'&start_date='+start_date
                                                     +'&end_date='+end_date+'&status='+status
                                                     +'&num_days='+num_days
                                                    ;
                        $("#gridview").flexOptions({url:'<?php echo base_url().'admin/'.$this->currentModule; ?>/get_data'+link_service}).flexReload();
                    }
                    
                    /*
                    function export_excel(com, grid) {
                        var store_id = $("#store_id").val();
                        var start_date = $("#start_date").val();
                        var end_date = $("#end_date").val();
                        
                        $("#export_store_id").val(store_id);
                        $("#export_start_date").val(start_date);
                        $("#export_end_date").val(end_date);
                        export_data(com, grid); //fungsi ada di core flexigrid.js
                    }
                    */
                </script>
                <?php
                break;
        
          
        }
        ?>
        <?php
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }   
}

?>
