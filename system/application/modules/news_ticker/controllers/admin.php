<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('news_ticker_lib'));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new news_ticker_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


      //  $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Running Text';
        $description = 'Text berjalan di atas slider';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Running Text',
            'class' => "clip-grid-2",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function add()
    {
   
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template


        $this->data['seoTitle'] = 'Running Text';
        $description = 'Tambah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Running Text',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/master_content/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
        
            if($status==200)
            {
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $columns=array(
                    'news_ticker_title'=>$this->input->post('news_ticker_title',true),
                    'news_ticker_link'=>$this->input->post('news_ticker_link',true),
                    'news_ticker_input_by'=>$admin_username,
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $msg=base64_encode('Data berhasil disimpan.');
                    redirect(base_url().'admin/'.$this->currentModule.'/add?status=200&msg='.$msg);
                }


            }
            //$this->session->set_flashdata('confirmation', '<div class="confirmation alert alert-danger">'.$message.'</div>');
        }

        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        
        $this->data['seoTitle'] = 'Running Text';
        $description = 'Ubah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Running Text',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
           $breadcrumbs_array[] = array(
            'name' => 'Ubah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        $rowMenu=$this->mainModel->find('news_ticker_id="'.$id.'"');
        if(empty($rowMenu))
        {
            show_error('Request anda tidak valid',500);
        }

        foreach($rowMenu AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }

        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($id);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $columns=array(
                    'news_ticker_title'=>$this->input->post('news_ticker_title',true),
                    'news_ticker_link'=>$this->input->post('news_ticker_link',true),
                    'news_ticker_input_by'=>$admin_username,
                );
                $this->mainModel->setPostData($columns);
                $where='news_ticker_id="'.  $id.'"';
                $hasSaved=$this->mainModel->save($where);
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status='.$status.'&msg='.$msg);
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
      
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);

    }
    

     public function act_delete() {
        $arr_output = array();
        $arr_output['status'] = 500;
        $arr_output['message'] = 'Tidak ada aksi yang dilakukan';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' '; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {
                            $this->mainModel->setMainTable('site_news_ticker');
                            $this->mainModel->delete('news_ticker_id='.intval($id));
                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['status'] = 200;
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['status'] = 500;
            }
        }
        
        echo json_encode($arr_output);
    }

     public function delete($id='') {
        $arr_output=array();
        $arr_output['message'] =  'Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 500;
        $additional_where=' '; //default telah ngecek data dihapus
        if($this->available_data($id,$additional_where))
        {

            $this->mainModel->setMainTable('site_news_ticker');
            $this->mainModel->delete('news_ticker_id='.intval($id));
            $arr_output['message'] =  ' Konten telah dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

     /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one('news_ticker_id','site_news_ticker',
            'news_ticker_id="'.$id.'" '.$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }

     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();

        ?>
       
        <?php
        switch($method)
        {
             case 'index':
            ?>
        <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Edit', name: 'edit', width: 50, sortable: true, align: 'center' },
                            { display: 'Delete', name: 'delete', width: 50, sortable: true, align: 'center' },
                            { display: 'Judul', name: 'title', width: 360, sortable: true, align: 'center' },
                            { display: 'Link', name: 'link', width: 360, sortable: true, align: 'center' },
                            { display: 'Diinput Oleh', name: 'input_by', width: 80, sortable: true, align: 'center' },
                    
                        ],
                        buttons: [
                            { display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                        ],
                        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],
                      
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });
                    
                    function act_delete(com, grid) {
                        var div_alert=$("div.alert");

                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        div_alert.slideDown('medium');
                                        if(response['status']!=200)
                                        {
                                            div_alert.addClass('alert-danger');
                                            div_alert.removeClass('alert-success');
                                        }
                                        else
                                        {
                                            div_alert.removeClass('alert-danger');
                                            div_alert.addClass('alert-success');
                                        }
                                        div_alert.html(response['message']);
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    $(document).ready(function() {
                        grid_reload();
                    });

                    function reset_pencarian()
                    {
                       $("input#youtube_id").val('');
                       $("input#youtube_title").val('');
                       $("input#youtube_description").val('');
                       grid_reload() ;
                    }

                    function grid_reload() {
                        var news_ticker_title=$("input#news_ticker_title").val();
                        var news_ticker_link=$("input#news_ticker_link").val();
                        var link_service='?&news_ticker_title='+news_ticker_title+'&news_ticker_link='+news_ticker_link;
                        $("#gridview").flexOptions({url:'<?php echo base_url().$this->currentModule; ?>/service_rest/get_data'+link_service}).flexReload();
                    }
                    
                    function delete_transaction(id)
                    { 
                        var div_alert=$("div.alert");
                        if(confirm('Delete?'))
                        {
                            var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$this->currentModule?>/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            div_alert.slideDown('medium');
                            if(response['status']!=200)
                            {
                                div_alert.addClass('alert-danger');
                                div_alert.removeClass('alert-success');
                            }
                            else
                            {
                                div_alert.removeClass('alert-danger');
                                div_alert.addClass('alert-success');
                            }
                            div_alert.html(response['message']);
                            grid_reload();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            grid_reload();
                            return false;
                        });
                        }
                        return false;
                       
                    }

                    
                </script>
            <?php
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
