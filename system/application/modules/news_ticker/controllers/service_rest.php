<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('news_ticker_lib');
        $this->mainModel=new news_ticker_lib;

       // $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";

        $news_ticker_title=$this->input->get('news_ticker_title',true);
        $news_ticker_title=str_replace('%20', ' ', $news_ticker_title);
        $news_ticker_link=$this->input->get('news_ticker_link',true);
        $news_ticker_link=str_replace('%20', ' ', $news_ticker_link);

        $where=1;

        $where.=' AND news_ticker_title LIKE "%'.$news_ticker_title.'%" AND news_ticker_link LIKE "%'.$news_ticker_link.'%" ';
        $params['where'] =$where;

        $params['order_by'] = "
            news_ticker_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
            $no++;
          
            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$news_ticker_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$news_ticker_id.'\');"><i class="clip-remove"></i></a>';
            $entry = array('id' => $news_ticker_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'title' =>$news_ticker_title,
                    'link' =>(trim($news_ticker_link)=='')?'n/a':'<a target="blank" href="'.$news_ticker_link.'">'.$news_ticker_link.'</a> ',
                    'input_by' => $news_ticker_input_by,
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
