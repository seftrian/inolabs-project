<?php 
/**
* @author Ari Armanda
*/
class widget_form_order_patient extends widget_lib
{
	
	public function run($module,$class)
	{
		$data['waktuArr'] = $this->get_request_time();
		$data['serviceArr'] = $this->get_service();
		$this->render(get_class(),$data);
	}
	function get_service(){
		$ci = & get_instance();

		$sql = 'SELECT * FROM sys_service WHERE service_is_show="Y"';
		$exec = $ci->db->query($sql);
		$results = $exec->result_array();

		return $results;
	}
	function get_request_time(){
		$ci = & get_instance();

		$sql = 'SELECT * FROM sys_request_time WHERE request_time_is_show="Y"';
		$exec = $ci->db->query($sql);
		$results = $exec->result_array();

		return $results;
	}
}
?>