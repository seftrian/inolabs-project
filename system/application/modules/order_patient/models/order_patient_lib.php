<?php 
/**
* @author Ari Armanda
*/
class order_patient_lib
{
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='sys_order';
    protected $dir='assets/images/order_patient/';
    public function __construct() {
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));
        if(!session_id())
        {
            session_start();
        }

        $this->getImagePath();
              
    }
    /**
    * dapatkan data sys_order untuk grid dan excel
    * @param int $id
    */
    public function set_last_view_order_patient($id=0){
      $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
      $isRead = $this->CI->function_lib->get_one('order_status','sys_order','order_id='.intval($id));

      if(trim($isRead)=="unread"){
        $columns = array(
          'order_view_by' => $admin_username,
          'order_view_datetime' => date('Y-m-d H:i:s'),
          'order_status' => 'read',
        );
        // setpostdata
        $this->setPostData($columns);

        // save data
        $this->save('order_id='.intval($id));
      }

      return array(
        'status' => 200,
        'message' => 'OK',
      );
    }
    /**
    * dapatkan data sys_order untuk grid dan excel
    */
    public function get_data_order_patient(){
        $params = isset($_POST) ? $_POST : array();
        $where = 1;

        $name = $this->CI->input->get('name');
        $order_date = $this->CI->input->get('order_date');
        $phone = $this->CI->input->get('phone');
        $email = $this->CI->input->get('email');
        $time = $this->CI->input->get('time');
        $status = $this->CI->input->get('status');

        if(trim($name)!=''){
            $where .= ' AND order_full_name LIKE "%'.$name.'%"';
        }
        if(trim($order_date)!=''){
            $where .= ' AND order_birth_date="'.date('Y-m-d',strtotime($order_date)).'"';
        }
        if(trim($phone)!=''){
            $where .= ' AND order_phone_number="'.$phone.'"';
        }
        if(trim($email)!=''){
            $where .= ' AND order_email="'.$email.'"';
        }
        if(trim($time)!=''){
            $where .= ' AND order_request_time="'.$time.'"';
        }
        if(trim($status)!=''){
            $where .= ' AND order_status="'.$status.'"';
        }

        $params['table'] = $this->getMainTable();
        $params['select'] = "
            ".$this->getMainTable().".*
        ";
        $params['where'] = $where;

        $params['order_by'] = "
            order_id DESC
        ";

        $query = $this->CI->function_lib->get_query_data($params);
        $total = $this->CI->function_lib->get_query_data($params, true);

        return array(
          'query' => $query,
          'total' => $total,
        );
    }
    /**
    * dapatkan data waktu
    */
    function get_request_time(){
      $sql = 'SELECT * FROM sys_request_time WHERE request_time_is_show="Y"';
      $exec = $this->CI->db->query($sql);
      $results = $exec->result_array();

      return $results;
    }
    /**
    * save order patient
    */
    public function prepare_save_order_patient(){
      $columns = (isset($_POST))?$_POST:array();
      
      // change format birth date
      $birthdateArr = array('order_birth_date' => date('Y-m-d',strtotime($columns['order_birth_date'])));
      $columns = array_replace($columns, $birthdateArr);

      // change format Appointment
      $appointmentArr = array('order_appointment_date' => date('Y-m-d',strtotime($columns['order_appointment_date'])));
      $columns = array_replace($columns, $appointmentArr);

      // additional value
      $columns = array_merge($columns,array(
        'order_status' => 'unread',
      ));

      // start send mail
      $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();
      $config_name=$websiteConfig['config_name'];
      $this->CI->load->library('send_mail');
      $sendMail=new send_mail; 
      $web_name=$this->CI->function_lib->get_current_domain();

      //content
      $email_format_content = '
<style type="text/css">
  table{ width: 50%; }
  tr:nth-child(even) {background-color: #f2f2f2}
  th, td {
      padding: 15px;
      text-align: left;
  }

</style>

<h2>Pemesanan</h2>
<table>
  <tr>
    <td style="width: 25%;">Nama</td>
    <td>: '.$columns["order_full_name"].'</td>
  </tr>
  <tr>
    <td style="width: 25%;">Email</td>
    <td>: '.$columns["order_email"].'</td>
  </tr>
  <tr>
    <td style="width: 25%;">Tanggal Lahir</td>
    <td>: '.convert_date($columns["order_birth_date"],'','','').'</td>
  </tr>
  <tr>
    <td style="width: 25%;">Tanggal Janji</td>
    <td>: '.convert_date($columns["order_appointment_date"],'','','').'</td>
  </tr>
  <tr>
    <td style="width: 25%;">Waktu</td>
    <td>: '.$columns["order_request_time"].'</td>
  </tr>
  <tr>
    <td style="width: 25%;">Poli</td>
    <td>: '.$columns["order_service_name"].'</td>
  </tr>
  <tr>
    <td style="width: 25%;">Nomor HP</td>
    <td>: '.$columns["order_phone_number"].'</td>
  </tr>
</table>
      ';


      $email_format_subject = '['.$web_name.'] Data Pendaftaran Online Anda';
      $email_to = $columns['order_email'];

      //email for user
      $emailProperty=array(
        'email_to'=>$email_to,
        'email_subject'=>$email_format_subject,
        'email_message'=>$email_format_content,
        'email_from'=>$config_name.' <noreply@'.$web_name.'>',
        'email_from_name'=>$web_name,
        'email_from_organization'=>'',
      );

      $email_format_subject = '['.$web_name.'] Pendaftaran Pasien';
      $email_to = $websiteConfig['config_email'];

      //email for admin
      $emailProperty2=array(
        'email_to'=>$email_to,
        'email_subject'=>$email_format_subject,
        'email_message'=>$email_format_content,
        'email_from'=>$config_name.' <noreply@'.$web_name.'>',
        'email_from_name'=>$web_name,
        'email_from_organization'=>'',
      );
         
      $sendMail->run($emailProperty);
      $sendMail->run($emailProperty2);
      //end send mail


      //prepare setpostdata
      $this->setPostData($columns);
      //save data
      $response = $this->save();

      $status = $response['status']; $message = ($response['status']==200)?'Pemesanan berhasil terkirim.':$response['message'];

      return array(
        'status' => $status,
        'message' => $message,
      );
    }
      /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->dir;
        $pathLocation=FCPATH.$this->dir;

        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
    
    
    public function getMainTable()
    {
        return $this->mainTable;
    }

    /**
     * set main table
     */
    public function setMainTable($table)
    {
        $this->mainTable=$table;
    }

     public function upload_more_images($news_id)
    {
        if(!empty($_FILES))
        {
            $no=1;
            $img_detail_id='';
            foreach($_FILES AS $filesName=>$rowArr)
            {

                $results=$this->handleUpload($news_id,$filesName,$no);
                $post_id=isset($_POST[$filesName.'_id'])?intval($_POST[$filesName.'_id']):0;
                $img_id=($results['id']>0)?$results['id']:$post_id;
                $img_detail_id.=($no>1)?', '.$img_id:$img_id;

                $explode_arr=explode('_', $field_name);
                $string_1=isset($explode_arr[0])?$explode_arr[0]:'';
                //echo $string_1;
                if($results['status']!=200)
                {
                    return array(
                        'status'=>500,
                        'message'=>'Gambar '.$_FILES[$filesName]['name'].' gagal diupluoad. ',
                    );
                }
                $no++;
            }
           
            $sql='DELETE FROM site_news_more_images WHERE news_more_images_id NOT IN ('.$img_detail_id.')
                  AND news_more_images_news_id='.intval($news_id);
            $this->CI->db->query($sql);
            
        }
//        else
//        {
//            if(isset($_POST['']))
//            {
//                
//            }
//        }
        return array(
            'status'=>200,
            'message'=>'success',
        );
    }
    

     /**
     * handle upload
     */
    public function handleUpload($id,$filesName,$no)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
        $id_new=0;
        if(!empty($fileUpload) AND (isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name']!='')))
        {
            $results=$this->doUpload($id,$filesName,$no);
            $status=$results['status'];
            $message=$results['message'];
            $id_new=$results['id'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$id_new,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name,$no)
    {
        $config=array();
        $pathImage='assets/images/master_content/';
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '10500'; //KB
        $config['file_name']=function_lib::seo_name(time());
        
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        $img_id=0;
        //echo $isComplete.'/'.$field_name;

        $explode_arr=explode('_', $field_name);
        $string_1=isset($explode_arr[0])?$explode_arr[0]:'';
        //echo $string_1;
        //die('sss');


        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
            if($field_name=='news_photo')
            {
                
                if(isset($_POST['news_photo_old']) AND trim($_POST['news_photo_old'])!='' AND file_exists(FCPATH.$_POST['news_photo_old']))
                {
                    unlink(FCPATH.$_POST['news_photo_old']);
                }
                
                $where=array(
                'news_id'=>$id,
                );
                $column=array(
                    'news_photo'=>$pathImage.$dataImage['file_name'],
                );
                $this->CI->db->update('site_news',$column,$where);
            }
            else
            {

                if($string_1=='news')
                {
                   if(!isset($_POST[$field_name.'_id']))
                  {
                                
                      $column=array(
                          'news_more_images_news_id'=>$id,
                          'news_more_images_file'=>$pathImage.$dataImage['file_name'],
                          'news_more_images_is_active'=>1,
                      );
                      $this->CI->db->insert('site_news_more_images',$column);
                      $img_id=$this->CI->function_lib->insert_id();
                         
                  }
                  else
                  {
                      $img_id=$_POST[$field_name.'_id'];
                      $old_image=$_POST[$field_name.'_old'];
                      if(file_exists(FCPATH.$old_image) AND trim($old_image)!='')
                      {
                          unlink(FCPATH.$old_image);
                      }
                      $column=array(
                          'news_more_images_news_id'=>$id,
                          'news_more_images_file'=>$pathImage.$dataImage['file_name'],
                          'news_more_images_is_active'=>1,
                      );
                      $where=array(
                          'news_more_images_id'=>$img_id,
                      );
                      $this->CI->db->update('site_news_more_images',$column,$where);
                      
                  }
                 }
               
            }
           
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$img_id,
        );
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='category_id ASC')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='Semua form wajib terisi/ada kesalahan input data.';
            $mainConfig = array(
                array(
                        'field'   => 'order_full_name',
                        'label'   => 'Nama',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'order_birth_date',
                        'label'   => 'Tanggal Lahir',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'order_appointment_date',
                        'label'   => 'Tanggal Janji',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'order_request_time',
                        'label'   => 'Waktu',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'order_service_name',
                        'label'   => 'Poli',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'order_phone_number',
                        'label'   => 'Nomor HP',
                        'rules'   => 'trim|required|is_numeric'
                    ),
                array(
                        'field'   => 'order_email',
                        'label'   => 'Email',
                        'rules'   => 'trim|required'
                    ),
                
            );
         
          $config=$mainConfig;
          /**
           * jika lokasi admin, maka module controller dan method wajib diisi
           * main config dan additional config akan digabung
           */
        
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
     
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
}
?>