<?php 
/**
* @author Ari Armanda
*/
class admin extends admin_controller
{
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('order_patient_lib'));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new order_patient_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    public function export_order_patient(){
        $results = $this->mainModel->get_data_order_patient();
        $query = $results['query'];
        $total = $results['total'];

        $websiteConfig = $this->system_lib->getWebsiteConfiguration();
        $title = 'Laporan Pemesanan';
        $filename = url_title($title) . '-' . date("YmdHis");
        $unprotected_password = '1q2w3e4r5t';
        
        $this->load->library('Excel_lib');

        $arr_style_title = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'EEEEEE')
            ),
            'alignment' => array(
                'wrap' => true,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );

        $arr_style_content = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'wrap' => true,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );

        $first_column = $cell_column = 'A';
        $first_row = $cell_row = 1;
        $excel = new PHPExcel();
        $excel->getProperties()->setTitle($title)->setSubject($title);
        $excel->getActiveSheet()->setTitle(substr($title, 0, 31));
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $excel->getDefaultStyle()->getFont()->setName('Calibri');
        $excel->getDefaultStyle()->getFont()->setSize(9);
        //$excel->getActiveSheet()->getProtection()->setPassword($unprotected_password);
        //$excel->getActiveSheet()->getProtection()->setSheet(true);
        
        $start_column = $cell_column;

        //title
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
        $excel->setActiveSheetIndex(0)->setCellValue($cell_column . $cell_row, strtoupper($websiteConfig['config_name']));
        $cell_row++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
        $excel->setActiveSheetIndex(0)->setCellValue($cell_column . $cell_row, $title);
        $cell_row++;
        $excel->setActiveSheetIndex(0)->setCellValue($cell_column . $cell_row, 'Tanggal Download : ' . convert_datetime(date("Y-m-d H:i:s"), 'id'));        
        $cell_row++;
       
        $cell_row++;
        $cell_row++;

        $cell_column = $first_column;
        
        $excel->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(25);
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(5)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'NO', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(35)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'NAMA', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(30)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'EMAIL', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(20)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'TANGGAL LAHIR', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(20)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'WAKTU', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(40)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'POLI', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(20)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'DIBACA OLEH', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(20)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'TANGGAL DIBACA', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(false)->setSize(10);
        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_title);
        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(20)->setVisible(true);
        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, 'STATUS', PHPExcel_Cell_DataType::TYPE_STRING);
        $cell_column++;
        
        $end_column = $cell_column;
        $cell_row++;
        
        if($total > 0) {
            $no = 0;

            foreach ($query->result() as $rowArr) {
                foreach ($rowArr as $key => $value) {
                    ${$key}=$value;
                }
                $no++;

                $cell_column = $first_column;                
                        
                        $excel->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(15);

                        //no
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, $no . '.', PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;
                        
                        //nama
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, $order_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;

                        //email
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, $order_email, PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;

                        //tanggal lahir
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, $order_birth_date, PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;

                        //waktu
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, $order_request_time, PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;

                        //poli
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, $order_service_name, PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;

                        //dibaca oleh
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, (trim($order_view_by)!='')?$order_view_by:'-', PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;

                        //tanggal dibaca
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, (trim($order_view_datetime)!='')?$order_view_datetime:'-', PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;

                        //status
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->applyFromArray($arr_style_content);
                        $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, (trim($order_status)!="unread")?'Telah Dibaca':'Belum Dibaca', PHPExcel_Cell_DataType::TYPE_STRING);
                        $cell_column++;
                
                        $cell_row++;
                   
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $write->save('php://output');
        exit;
    }
    /**
    * cetak pemesanan pasien
    */
    public function print_order_patient($id=0){
        $dataArr = $this->mainModel->find('order_id='.intval($id));

        
        if(!empty($dataArr))
        {
            foreach($dataArr AS $variable=>$value)
            {
                $this->data[$variable]=$value;
            }

            $this->data['printer_mode'] = 'popup';
        }
        else
        {
            $link='<br /><a href="'.base_url().'admin/'.$this->currentModule.'/add">Kembali ke Pendaftaran</a>';
            show_error('Maaf data tidak ditemukan. '.$link,404);
        }
        $this->data['id']=$id;
        $this->load->view_single($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }
    /**
    * tampilkan detail data order_patient
    */
    public function view($id=0){
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Detail Pemesanan';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Pemesanan',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Detail Pemesanan',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/view',
            'current' => true, //boolean
        );
            
        // set view
        $this->mainModel->set_last_view_order_patient($id);
        
        $dataArr = $this->mainModel->find('order_id='.intval($id));
        if(!empty($dataArr)){
            foreach ($dataArr as $key => $value) {
                $this->data[$key]=$value;
            }
        } else{
            show_404();
        }

        $this->data['url_print'] = base_url().'admin/order_patient/print_order_patient/'.$id;

        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    /**
    * tampilkan data grid order_patient
    */
    public function index(){
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Pemesanan';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Pemesanan',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );

         $this->data['request_time'] = $this->mainModel->get_request_time();

        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
}
?>