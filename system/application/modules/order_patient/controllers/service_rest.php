<?php 
/**
* @author Ari Armanda
*/
class service_rest extends front_controller
{
    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('order_patient_lib');
        $this->mainModel=new order_patient_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }
    public function get_data(){
        $results = $this->mainModel->get_data_order_patient();
        $query = $results['query'];
        $total = $results['total'];

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;

            $icon = (trim($order_status)=="read")?'fa fa-envelope-open':'fa fa-envelope';
            $status = (trim($order_status)=="read")?'default':'warning';
            $view = '<a href="'.base_url().'admin/order_patient/view/'.$order_id.'" class="btn btn-sm btn-'.$status.'"><i class="'.$icon.'"></i></a>';
            $entry = array('id' => $order_id,
                'cell' => array(
                    'no' =>  $no.'.',
                    'view' => $view,
                    'name' => $order_full_name,
                    'email' => $order_email,
                    'birthday' => convert_date($order_birth_date,'','',''),
                    'appointment' => convert_date($order_appointment_date,'','',''),
                    'time' => $order_request_time,
                    'poli' => $order_service_name,
                    'phone' => $order_phone_number,
                    'view_by' => (isset($order_view_by) AND trim($order_view_by)!='')?$order_view_by:'-',
                    'last_view' => (isset($order_view_datetime) AND trim($order_view_datetime)!='')?date('d-m-Y H:i:s',strtotime($order_view_datetime)):'-',
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    /**
    * ajax order patient
    */
    public function order_patient(){
    	$status = 500; $message = 'Terjadi kesalahan.';

    	if(!empty($_POST)){
    		$response = $this->mainModel->formValidation();
    		if($response['status']==200){
    			$response = $this->mainModel->prepare_save_order_patient();
    		}
    		$status = $response['status']; $message = $response['message'];
    	}

    	$results = array(
    		'status' => $status,
    		'message' => $message,
    	);
    	echo json_encode($results);
    }
}
?>