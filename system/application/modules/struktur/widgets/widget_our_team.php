<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author anggoro
 */
class widget_our_team extends widget_lib{
    //put your code here
    public function run($module,$class_widget)
    {
        $this->load->model('struktur/struktur_lib');
        $lib=new struktur_lib;

        $data=array();
        $sql='SELECT * FROM sys_struktur
              ORDER BY struktur_urutan ASC
              ';
        $exec=$this->db->query($sql);      
        $data['results']=$exec->result_array();
        $data['pathImgArr']=$lib->getImagePath();
        $this->render(get_class(),$data);
    }
}

?>
