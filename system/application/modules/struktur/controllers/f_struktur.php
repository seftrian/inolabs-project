<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class f_struktur extends front_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('struktur_lib',));
        $this->load->helper(array('pagination'));
        
        //inisialisasi model
        $this->mainModel=new struktur_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Dokter';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Dokter',
            'class' => " clip-tree",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $query=(isset($_GET['query']))?$_GET['query']:'';

        $limit=4;
        $where = (isset($query) AND trim($query)!='')?'struktur_nama_kepala LIKE "%'.$query.'%" OR struktur_profil_singkat LIKE "%'.$query.'%"':1;
        $this->data['total_rows']=$this->mainModel->countAll($where);
        $paginationArr=create_pagination_query_string($this->currentModule.'/'.get_class().'/'.__FUNCTION__, $this->data['total_rows'], $limit,4,$query);
        
        $this->data['offset']=$paginationArr['limit'][1];    
        $this->data['results']=$this->mainModel->findAll($where,$limit,$this->data['offset']);
        $this->data['pathImgArr']=$this->mainModel->getImagePath();
        $this->data['link_pagination']=$paginationArr['links'];
        $this->data['base_link']=base_url().$this->currentModule.'/'.get_class().'/'.__FUNCTION__;        


        $initDataMenu=$this->mainModel->initalizeRecursiveDynamic();
        $this->data['generate_tree']=$this->mainModel->generate_tree_front(0, $initDataMenu);
        $this->data['struktur_data'] = $this->mainModel->get_struktur_data();

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
    public function view($id=0)
    {
        $where='struktur_id='.intval($id);
        $row=$this->mainModel->find($where);
        $data=array();
        $data['title']='-';
        $data['content']='data tidak ditemukan.';
        if(!empty($row))
        {
            $pathImgArr=$this->mainModel->getImagePath();
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }

            $logo_name= (trim($struktur_foto)=='')?'-': pathinfo($struktur_foto,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($struktur_foto,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$pathImgArr['pathUrl'];
            $pathImgLoc=$pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>130,
            'height'=>130,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'130130/',
            'urlSave'=>$pathImgUrl.'130130/',
            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $label='<center><div style="text-align:center;width:150px;"><span style="font-size:10px;">
            <img src="'.$img.'" /><br />
            <strong>'.$struktur_nama_kepala.'</strong></span>';
            $label.='<br /><span style="font-size:9px;">'.$struktur_pangkat.'</span>';
            $label.='<hr style="margin:2px;"/>';
            $label.='<span style="font-size:9px;" >NIP. '.$struktur_nip.'</span><br />';
            $label.='</div></center>';
            $data['title']='<center>'.$struktur_nama_jabatan.'</center>';
            $data['content']=$label;
        }

        echo json_encode($data);
    }

    /**
    detail news load modal
    @param string $news_permalink    
    */
    public function load_modal_struktur($id=0)
    {       

        $where='struktur_id='.intval($id);
        $row=$this->mainModel->find($where);
        $data=array();
        $data['struktur_nama_jabatan']='-';
        $data['struktur_nama_kepala']='-';
        $data['struktur_profil_singkat']='-';

        if(!empty($row))
        {
            $pathImgArr=$this->mainModel->getImagePath();
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }

                            $logo_name= (trim($struktur_foto)=='')?'-': pathinfo($struktur_foto,PATHINFO_FILENAME);
                            $logo_ext=  pathinfo($struktur_foto,PATHINFO_EXTENSION);
                            $logo_file=$logo_name.'.'.$logo_ext;
                            $pathImgUrl=$pathImgArr['pathUrl'];
                            $pathImgLoc=$pathImgArr['pathLocation'];
                             
                            $imgProperty=array(
                                                        'width'=>443,
                                                        'height'=>400,
                                                        'imageOriginal'=>$logo_file,
                                                        'directoryOriginal'=>$pathImgLoc,
                                                        'directorySave'=>$pathImgLoc.'443400/',
                                                        'urlSave'=>$pathImgUrl.'443400/',
                                                    );
                            $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);

            $data['struktur_img']=$img;
            $data['struktur_nama_jabatan']=$struktur_nama_jabatan;
            $data['struktur_nama_kepala']=$struktur_nama_kepala;
            $data['struktur_profil_singkat']=$struktur_profil_singkat;


        }
        /**

        if(trim($news_permalink)=='')
        {
            show_error('Halaman tidak ditemukan',404);
        }
        $news_permalink=$this->security->sanitize_filename($news_permalink);
        $this->data['row_array']=$this->mainModel-> get_news_detail_from_permalink($news_permalink);
        if(empty($this->data['row_array']))
        {
            show_error('Halaman tidak ditemukan',404);
        }
        $pathImgArr=$this->mainModel->getImagePath();
        $title=isset($this->data['row_array']['news_title'])?$this->data['row_array']['news_title']:'';
        $desc=isset($this->data['row_array']['news_meta_description'])?$this->data['row_array']['news_meta_description']:'';
        $key=isset($this->data['row_array']['news_meta_tags'])?$this->data['row_array']['news_meta_tags']:'';
        $this->seo($title,$desc,$key);

        $news_id=isset($this->data['row_array']['news_id'])?$this->data['row_array']['news_id']:0;
        //counter
        $this->mainModel->count_number_of_read($news_id);


        foreach($this->data['row_array'] AS $variable=>$value)
        {
            ${$variable}=$value;
        }

                            $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
                            $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
                            $logo_file=$logo_name.'.'.$logo_ext;
                            $pathImgUrl=$pathImgArr['pathUrl'];
                            $pathImgLoc=$pathImgArr['pathLocation'];
                             
                            $imgProperty=array(
                                                        'width'=>770,
                                                        'height'=>514,
                                                        'imageOriginal'=>$logo_file,
                                                        'directoryOriginal'=>$pathImgLoc,
                                                        'directorySave'=>$pathImgLoc.'770514/',
                                                        'urlSave'=>$pathImgUrl.'770514/',
                                                    );
                            $img=$this->function_lib->resizeImageMoo($imgProperty,true,true);
        */
        
        echo json_encode($data);
    }

     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        ?>
        <?php
        switch($method)
        {
            case 'index':
            ?>
            <style type="text/css">
                    /*Now the CSS*/
                * {margin: 0; padding: 0;}

                .tree
                {
                    width: 1200px;
                    margin-left: auto;
                    margin-right: auto;
                }

                .tree ul {
                    padding-top: 20px; position: relative;
                    transition: all 0.5s;
                    -webkit-transition: all 0.5s;
                    -moz-transition: all 0.5s;
                }

                .tree li {
                    float: left; text-align: center;
                    list-style-type: none;
                    position: relative;
                    padding: 20px 5px 0 5px;
                    
                    transition: all 0.5s;
                    -webkit-transition: all 0.5s;
                    -moz-transition: all 0.5s;
                }

                /*We will use ::before and ::after to draw the connectors*/

                .tree li::before, .tree li::after{
                    content: '';
                    position: absolute; top: 0; right: 50%;
                    border-top: 1px solid #ccc;
                    width: 50%; height: 20px;
                }
                .tree li:after{
                    right: auto; left: 50%;
                    border-left: 1px solid #ccc;
                }

                /*We need to remove left-right connectors from elements without 
                any siblings*/
                .tree li:only-child::after, .tree li:only-child::before {
                    display: none;
                }

                /*Remove space from the top of single children*/
                .tree li:only-child{ padding-top: 0;}

                /*Remove left connector from first child and 
                right connector from last child*/
                .tree li:first-child::before, .tree li:last-child::after{
                    border: 0 none;
                }
                /*Adding back the vertical connector to the last nodes*/
                .tree li:last-child::before{
                    border-right: 1px solid #ccc;
                    border-radius: 0 5px 0 0;
                    -webkit-border-radius: 0 5px 0 0;
                    -moz-border-radius: 0 5px 0 0;
                }
                .tree li:first-child::after{
                    border-radius: 5px 0 0 0;
                    -webkit-border-radius: 5px 0 0 0;
                    -moz-border-radius: 5px 0 0 0;
                }

                /*Time to add downward connectors from parents*/
                .tree ul ul::before{
                    content: '';
                    position: absolute; top: 0; left: 50%;
                    border-left: 1px solid #ccc;
                    width: 0; height: 20px;
                    margin-left: -1px;
                }

                .tree li a{
                    border: 1px solid #ccc;
                    padding: 5px 10px;
                    text-decoration: none;
                    color: #666;
                    font-family: arial, verdana, tahoma;
                    font-size: 11px;
                    display: inline-block;
                    
                    width: 150px;
                    border-radius: 5px;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    
                    transition: all 0.5s;
                    -webkit-transition: all 0.5s;
                    -moz-transition: all 0.5s;
                }

                /*Time for some hover effects*/
                /*We will apply the hover effect the the lineage of the element also*/
                .tree li a:hover, .tree li a:hover+ul li a {
                    background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
                }
                /*Connector styles on hover*/
                .tree li a:hover+ul li::after, 
                .tree li a:hover+ul li::before, 
                .tree li a:hover+ul::before, 
                .tree li a:hover+ul ul::before{
                    border-color:  #94a0b4;
                }

                li a.just-line {
                    display: none;
                }
                a.just-line + ul {
                    padding-top: 74px;
                }
                a.just-line + ul:before {
                    height: 74px;
                }

            </style>
            <script type="text/javascript">
            $(function(){
                if ($(".popovers").length) {
                    $('.popovers').popover();
                }
            });
               
            </script>
            <?php
            break;   
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
