<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('struktur_lib');
        $this->mainModel=new struktur_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";

        $news_category_id=$this->input->get('news_category_id',true);
        $news_title=$this->input->get('news_title',true);
        $news_title=str_replace('%20', ' ', $news_title);
        $news_content=$this->input->get('news_content',true);
        $news_content=str_replace('%20', ' ', $news_content);

        $where=1;
        if(trim($news_category_id)!='')
        {
            $where.=' AND news_id IN (SELECT site_news_relation.news_id FROM site_news_relation WHERE site_news_relation.news_category_id='.intval($news_category_id).'
                AND site_news_relation.news_id=news_id)';
        }

        $where.=' AND news_title LIKE "%'.$news_title.'%" AND news_content LIKE "%'.$news_content.'%" ';
        $params['where'] =$where;

        $params['order_by'] = "
            news_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $logo_name= (trim($news_photo)=='')?'-': pathinfo($news_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($news_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',
            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$news_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$news_id.'\');"><i class="clip-remove"></i></a>';
            $content=strip_tags(html_entity_decode($news_content));
            $content=(strlen($content)<150)?$content:substr($content,0,150);

            //kategori
            $generate_list_category=$this->mainModel->generate_list_category($news_id);

            $entry = array('id' => $news_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'title' =>$news_title,
                    'content' =>$content.' <br /><p style="float:right;" class="text-muted">'.$generate_list_category.'</p>',
                    'meta_description' => $news_meta_description,
                    'meta_tags' => $news_meta_tags,
                    'photo' => '<img src="'.$img.'" />',
                    'input_by' => $news_input_by,

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
