<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('struktur_lib',));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new struktur_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Bagan Struktur Organisasi';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Bagan Struktur Organisasi',
            'class' => " clip-tree",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $initDataMenu=$this->mainModel->initalizeRecursiveDynamic();
        $this->data['generate_tree']=$this->mainModel->generate_tree(0, $initDataMenu);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function add()
    {
   
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Bagan Struktur Organisasi';
        $description = 'Tambah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Bagan Struktur Organisasi',
            'class' => " clip-tree",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
           $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/create',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
        
            if($status==200)
            {
                $response_save=$this->mainModel->stored_database();
                $status=$response_save['status']; //status saat penyimpanan
                if($status==200)
                {
                    $id=$this->function_lib->insert_id();
                    //handle logo
                    $logo=$this->mainModel->handleUpload($id,'struktur_foto');
                    if($logo['status']!=200)
                    {
                        $status=$logo['status'];
                        $message=$logo['message'];
                        redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.$msg);
                    }
                    else
                    {
                        $msg=base64_encode('Data berhasil disimpan');
                        if(isset($_GET['redirect']))
                        {
                            redirect(rawurldecode($_GET['redirect']));
                        }
                        else
                        {
                            redirect(base_url().'admin/'.$this->currentModule.'/add?status=200&msg='.$msg);
                        }
                        
                    }
                
                    
                }


            }
        }
        
        $initDataMenu=$this->mainModel->initalizeRecursiveDynamic();
        $this->data['par_id']=$this->input->post('struktur_par_id');
       
        $this->data['recursiveDataMenu']=$this->mainModel->buildMenuDropDown(0, $initDataMenu,'=',$this->data['par_id']);


        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Bagan Struktur Organisasi';
        $description = 'Ubah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Bagan Struktur Organisasi',
            'class' => " clip-tree",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
           $breadcrumbs_array[] = array(
            'name' => 'Ubah',
            'class' => "clip-pencil",
            'link' =>'#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $rowMenu=$this->mainModel->find('struktur_id='.  intval($id));
        if(empty($rowMenu))
        {
            show_error('Request anda tidak valid',500);
        }

        foreach($rowMenu AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }

        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($id);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                $response_save=$this->mainModel->stored_database($id);
                $status=$response_save['status']; //status saat penyimpanan
                if($status==200)
                {
                    $msg=base64_encode('Data berhasil disimpan');
                    //handle logo
                    $logo=$this->mainModel->handleUpload($id,'struktur_foto');
                    if($logo['status']!=200)
                    {
                        $status=$logo['status'];
                        $message=strip_tags($logo['message']);
                        $msg=base64_encode('Terjadi error saat upload file. '.$message);
                    }
                
                    redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status='.$status.'&msg='.$msg);

                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
      
        
        $initDataMenu=$this->mainModel->initalizeRecursiveDynamic($id.' NOT IN (struktur_id,struktur_par_id)');
        $this->data['par_id']=$this->input->post('struktur_par_id')?$this->input->post('struktur_par_id'):$rowMenu['struktur_par_id'];
        $this->data['recursiveDataMenu']=$this->mainModel->buildMenuDropDown(0, $initDataMenu,'=',$this->data['par_id']);
        $this->data['pathImgArr']=$this->pathImgArr;

        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);

    }
    
    
    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    public function delete($id=0)
    {
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }

        $where='struktur_id='.  intval($id);
        $rowAdmin=$this->mainModel->find($where);

        if(!empty($rowAdmin))
        {
           $isDelete=$this->mainModel->delete($where);
           //hapus sub id
           $where='struktur_par_id='.intval($id);
           $isDelete=$this->mainModel->delete($where);

           redirect(base_url().'admin/'.$this->currentModule.'/index?status='.$isDelete['status'].'&msg='.base64_encode($isDelete['message']));
           
        }
        else
        {
            $msg='Data tidak ditemukan';
            redirect(base_url().'admin/'.$this->currentModule.'/index?status=500&msg='.base64_encode($msg));
        }
        
    }

    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    public function delete_image($id=0,$output='redirect')
    {
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        $where='struktur_id='.  intval($id);
        $this->data['rowAdmin']=$this->mainModel->find($where);

        if(!empty($this->data['rowAdmin']))
        {
           $sql='UPDATE sys_struktur SET struktur_foto="-" WHERE '.$where; 
           $this->db->query($sql);
           redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=200&msg='.base64_encode('Gambar berhasil dihapus'));
        }
        else
        {
            $msg='Data tidak ditemukan';
            redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.base64_encode($msg));
        }
        
    }

    
    
    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        ?>
        <link rel="stylesheet" href="<?php echo $this->themeUrl?>assets/plugins/select2/select2.css">
        <script src="<?php echo $this->themeUrl?>assets/plugins/select2/select2.min.js"></script>
        <script type="text/javascript">
        $(function(){
           $(".search-select").select2({
                placeholder: "Select a State",
                allowClear: true
            });
        });
        </script>
        <?php
        switch($method)
        {
            case 'index':
            ?>
            <style type="text/css">
                    /*Now the CSS*/
* {margin: 0; padding: 0;}

.tree
{
    width: 2000px;
    margin-left: auto;
    margin-right: auto;
}

.tree ul {
    padding-top: 20px; position: relative;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
}

.tree li {
    float: left; text-align: center;
    list-style-type: none;
    position: relative;
    padding: 20px 5px 0 5px;
    
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
    content: '';
    position: absolute; top: 0; right: 50%;
    border-top: 1px solid #ccc;
    width: 50%; height: 20px;
}
.tree li:after{
    right: auto; left: 50%;
    border-left: 1px solid #ccc;
}

/*We need to remove left-right connectors from elements without 
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
    display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and 
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
    border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
    border-right: 1px solid #ccc;
    border-radius: 0 5px 0 0;
    -webkit-border-radius: 0 5px 0 0;
    -moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
    border-radius: 5px 0 0 0;
    -webkit-border-radius: 5px 0 0 0;
    -moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
    content: '';
    position: absolute; top: 0; left: 50%;
    border-left: 1px solid #ccc;
    width: 0; height: 20px;
    margin-left: -1px;
}

.tree li a{
    border: 1px solid #ccc;
    padding: 5px 10px;
    text-decoration: none;
    color: #666;
    font-family: arial, verdana, tahoma;
    font-size: 11px;
    display: inline-block;
    
    width: 200px;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
}

/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
    background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after, 
.tree li a:hover+ul li::before, 
.tree li a:hover+ul::before, 
.tree li a:hover+ul ul::before{
    border-color:  #94a0b4;
}

li a.just-line {
    display: none;
}
a.just-line + ul {
    padding-top: 74px;
}
a.just-line + ul:before {
    height: 74px;
}

            </style>
            <?php
            break;   
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
