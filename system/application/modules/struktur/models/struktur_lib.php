<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
//require_once dirname(__FILE__).'/../components/core_menu_lib.php';

//class admin_menu extends core_menu_lib{
class struktur_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='sys_struktur';
    protected $dir='assets/images/struktur/';
    public function __construct() {

  //      parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));
                              
        if(!session_id())
        {
            session_start();
        }

        $this->getImagePath();
              
    }
    /**
    * dapatkan data struktur
    **/
    public function get_struktur_data(){
        $data=array();
        $sql='SELECT * FROM sys_struktur
              ORDER BY struktur_urutan ASC
              ';
        $exec=$this->CI->db->query($sql);      

        return array(
          'pathImgArr' => $this->getImagePath(),
          'results' => $exec->result_array(),
        );
    }
      /**
     * recursive tree privilege
     * @param int $parentId 
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param string $additionalClass 
     * @param int $selectId id menu yang sedang diakses
     * 
     */
    public function generate_tree($parentId, $menuData) {
        $html='';
        $pathImgArr=$this->getImagePath();
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul>';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['struktur_id'];
                $parId = $menuData['items'][$itemId]['struktur_par_id'];
                $name = $menuData['items'][$itemId]['struktur_nama_jabatan'];
                $struktur_nama_kepala = $menuData['items'][$itemId]['struktur_nama_kepala'];
                $struktur_pangkat = $menuData['items'][$itemId]['struktur_pangkat'];
                $struktur_nip = $menuData['items'][$itemId]['struktur_nip'];
                $struktur_foto = $menuData['items'][$itemId]['struktur_foto'];
                $btn_delete='<button class="btn btn-xs btn-danger" onclick="if(confirm(\'Dengan menghapus data ini, sub data juga akan ikut terhapus. Lanjutkan hapus '.$name.'?\')){window.location.href=\''.base_url().'admin/struktur/delete/'.$id.'\'};return false;"><i class="fa fa-trash-o"></i></button>';
                $btn_edit='<button class="btn btn-xs btn-primary" onclick="window.location.href=\''.base_url().'admin/struktur/update/'.$id.'\'"><i class="fa fa-edit"></i></button>';


                $logo_name= (trim($struktur_foto)=='')?'-': pathinfo($struktur_foto,PATHINFO_FILENAME);
                $logo_ext=  pathinfo($struktur_foto,PATHINFO_EXTENSION);
                $logo_file=$logo_name.'.'.$logo_ext;
                $pathImgUrl=$pathImgArr['pathUrl'];
                $pathImgLoc=$pathImgArr['pathLocation'];
                $imgProperty=array(
                'width'=>130,
                'height'=>130,
                'imageOriginal'=>$logo_file,
                'directoryOriginal'=>$pathImgLoc,
                'directorySave'=>$pathImgLoc.'130130/',
                'urlSave'=>$pathImgUrl.'130130/',
                );
                $img=$this->CI->function_lib->resizeImageMoo($imgProperty);

                $label='<span style="font-size:10px;" class="text-info">
                <img src="'.$img.'"><br />
                <strong>'.$struktur_nama_kepala.'</strong></span>';
                $label.='<br /><span style="font-size:9px;" class="text-muted">'.$struktur_pangkat.'</span>';
                $label.='<hr style="margin:2px;"/>';
                $label.='<span style="font-size:8px;" class="text-muted">NIP. '.$struktur_nip.'</span><br />';
              
              
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $html.='<li> ';
                    $html.='<a href="#">
                                  <h5>'.$name.'</h5>
                                  <hr style="margin:2px;" /><br />
                                  '.$label.'
                                   '.$btn_edit.' '.$btn_delete.' 
                                </a>';
                }
                else
                {
                    $html.='<li>';
                    $html.='<a href="#"> 
                                  <h5>'.$name.'</h5>
                                  <hr style="margin:2px;" /><br />
                                  '.$label.'
                                   '.$btn_edit.'  '.$btn_delete.'
                                </a>';
                    
                }
                
                // find childitems recursively 
                $html .= $this->generate_tree($itemId, $menuData);
                $html.='</li>';
            }
            $html.='</ul>';

        }

        return $html;
    }

     public function generate_tree_front($parentId, $menuData) {
        $html='';
        $pathImgArr=$this->getImagePath();
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul>';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['struktur_id'];
                $parId = $menuData['items'][$itemId]['struktur_par_id'];
                $name = $menuData['items'][$itemId]['struktur_nama_jabatan'];
                $struktur_nama_kepala = $menuData['items'][$itemId]['struktur_nama_kepala'];
                $struktur_pangkat = $menuData['items'][$itemId]['struktur_pangkat'];
                $struktur_nip = $menuData['items'][$itemId]['struktur_nip'];
                $struktur_foto = $menuData['items'][$itemId]['struktur_foto'];
                $btn_delete='<button class="btn btn-xs btn-danger" onclick="if(confirm(\'Dengan menghapus data ini, sub data juga akan ikut terhapus. Lanjutkan hapus '.$name.'?\')){window.location.href=\''.base_url().'admin/struktur/delete/'.$id.'\'};return false;"><i class="fa fa-trash-o"></i></button>';
                $btn_edit='<button class="btn btn-xs btn-primary" onclick="window.location.href=\''.base_url().'admin/struktur/update/'.$id.'\'"><i class="fa fa-edit"></i></button>';


                $logo_name= (trim($struktur_foto)=='')?'-': pathinfo($struktur_foto,PATHINFO_FILENAME);
                $logo_ext=  pathinfo($struktur_foto,PATHINFO_EXTENSION);
                $logo_file=$logo_name.'.'.$logo_ext;
                $pathImgUrl=$pathImgArr['pathUrl'];
                $pathImgLoc=$pathImgArr['pathLocation'];
                $imgProperty=array(
                'width'=>130,
                'height'=>130,
                'imageOriginal'=>$logo_file,
                'directoryOriginal'=>$pathImgLoc,
                'directorySave'=>$pathImgLoc.'130130/',
                'urlSave'=>$pathImgUrl.'130130/',
                );
                $img=$this->CI->function_lib->resizeImageMoo($imgProperty);

                $label='<span style="font-size:10px;" class="text-info">
                <img src="'.$img.'" /><br />
                <strong>'.$struktur_nama_kepala.'</strong></span>';
                $label='<br /><span style="font-size:9px;" class="text-muted">'.$struktur_pangkat.'</span>';
                $label.='<hr style="margin:2px;"/>';
                $label.='<span style="font-size:8px;" class="text-muted">NIP. '.$struktur_nip.'</span><br />';
                $label='';
                $btn_action='<span data-original-title="'.$name.'" data-placement="bottom" data-content="'.$label.'" class="popovers" href="javascript:;" onclick="return false;">
                      '.$name.'
                    </span>';
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $html.='<li> ';
                    $html.='<a href="#" onclick="show_popover($(this),\''.$id.'\');return false;">
                                  <strong>'. $name.'</strong>
                                </a>';
                }
                else
                {
                    $html.='<li>';
                    $html.='<a href="#" onclick="show_popover($(this),\''.$id.'\');return false;"> 
                                  <strong>'. $name.'</strong>
                                </a>';
                    
                }
                
                // find childitems recursively 
                $html .= $this->generate_tree_front($itemId, $menuData);
                $html.='</li>';
            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * sama dengan method initalizeRecursive(), 
     * tetapi yg ini lebih flexible u/ set parameter where
     * 
     * @param string $where
     */
     public function initalizeRecursiveDynamic($where=1) {
        $this->mainTable='sys_struktur';
        $list_data = $this->findAll($where,'no limit','no offset','struktur_id ASC');
        $data_build = array(
            'items' => array(),
            'parents' => array(),
        );

        foreach ($list_data AS $val) {
            $data_build['items'][$val['struktur_id']] = $val;
            $data_build['parents'][$val['struktur_par_id']][] = $val['struktur_id'];
        }

        return $data_build;
    }

     /**
     * recursive dengan drop down
     * @param int $parentId 
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param string $nbsp pemisah karakter
     * @param int $parentDataId $id primary data untuk selected ketika update/post
     * 
     */
    public function buildMenuDropDown($parentId, $menuData, $nbsp='', $menuId = 0) {
        $html = '';
        $nbsp.=($parentId == 0) ? '=' : '==';
        if (isset($menuData['parents'][$parentId])) {

            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['struktur_id'];
                $parId = $menuData['items'][$itemId]['struktur_par_id'];
                $name = $menuData['items'][$itemId]['struktur_nama_jabatan'];
              
                $html .= '<option value="' . $id . '" ' . (($id == $menuId) ? 'selected' : '') . '>' . $nbsp . ' ' . $name . '</option>';

                // find childitems recursively 
                $html .= $this->buildMenuDropDown($itemId, $menuData,$nbsp, $menuId);

                //$html .= '</tr>'; 
            }
        }

        return $html;
    }

    /***
    simpan struktur organisasi
    @param int $id -> saat update
    */
    public function stored_database($id=0)
    {
        $struktur_par_id=$this->CI->input->post('struktur_par_id',true);
        $struktur_nama_jabatan=$this->CI->input->post('struktur_nama_jabatan',true);
        $struktur_nama_kepala=$this->CI->input->post('struktur_nama_kepala',true);
        $struktur_pangkat=$this->CI->input->post('struktur_pangkat',true);
        $struktur_nip=$this->CI->input->post('struktur_nip',true);
        $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';

        $struktur_profil_singkat=$this->CI->input->post('struktur_profil_singkat',true);
        $struktur_facebook=$this->CI->input->post('struktur_facebook',true);
        $struktur_twitter=$this->CI->input->post('struktur_twitter',true);
        $struktur_gplus=$this->CI->input->post('struktur_gplus',true);
        $struktur_linkedin=$this->CI->input->post('struktur_linkedin',true);
        $struktur_urutan=$this->CI->input->post('struktur_urutan',true);

        $column=array(
          'struktur_par_id'=>($struktur_par_id!='')?$struktur_par_id:0,
          'struktur_nama_jabatan'=>($struktur_nama_jabatan!='')?$struktur_nama_jabatan:'-',
          'struktur_nama_kepala'=>($struktur_nama_kepala!='')?$struktur_nama_kepala:'-',
          'struktur_pangkat'=>($struktur_pangkat!='')?$struktur_pangkat:'-',
          'struktur_nip'=>($struktur_nip!='')?$struktur_nip:'-',
          'struktur_input_by'=>$admin_username,
          'struktur_profil_singkat'=>$struktur_profil_singkat,
          'struktur_facebook'=>$struktur_facebook,
          'struktur_twitter'=>$struktur_twitter,
          'struktur_gplus'=>$struktur_gplus,
          'struktur_linkedin'=>$struktur_linkedin,
          'struktur_urutan'=>($struktur_urutan!='')?$struktur_urutan:1,
        );
        $this->setPostData($column);
        if($id>0)
        {
          $where='struktur_id='.intval($id);
          $this->save($where);
        }
        else
        {
          $this->save();
        }
        return array(
          'status'=>200,
          'message'=>'OK',
          'id'=>$id,
        );
    }

     /**
     * check has submenu
     * @param int $menuId
     * @return bool true has submenu
     */
    public function hasSubmenu($id)
    {
        $value=false;
        $scalar=$this->CI->function_lib->get_one('COUNT(*)','sys_struktur','struktur_par_id='.  intval($id));
        if(intval($scalar)>0)
        {
            $value=$scalar;
        }
        return $value;
    }

      /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->dir;
        $pathLocation=FCPATH.$this->dir;

        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
    
    
    /**
     * handle upload
     */
    public function handleUpload($id,$filesName)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
      
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName);
            $status=$results['status'];
            $message=$results['message'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name)
    {
        $config=array();
        $pathImage=$this->dir;
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
             $where=array(
                'struktur_id'=>$id,
            );
            $column=array(
                'struktur_foto'=>$pathImage.$dataImage['file_name'],
            );
            $this->CI->db->update($this->mainTable,$column,$where);
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='struktur_id ASC')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='';
            $mainConfig = array(
              
                array(
                        'field'   => 'struktur_par_id',
                        'label'   => 'Posisi',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'struktur_nama_jabatan',
                        'label'   => 'Nama Jabatan',
                        'rules'   => 'trim|required'
                    ),
                
            );
         
          $config=$mainConfig;
          /**
           * jika lokasi admin, maka module controller dan method wajib diisi
           * main config dan additional config akan digabung
           */
        
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
