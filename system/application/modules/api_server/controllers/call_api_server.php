<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author tejomurti
 */
class call_api_server extends api_controller {
    //put your code here
    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->model(array('api_server'));
        $this->mainModel=new api_server;
        
        $disable_access_arr=array(1=>'post_schedule','post_dokter_schedule');

        $status_token_arr=$this->mainModel->validation_token($this->api_data);
        if($status_token_arr['status']!=200 AND !array_search($this->api_method,$disable_access_arr))
        {
            $this->convert_output($status_token_arr);
            exit;
        }
        
    }
    
    public function index()
    {
        //dapatkan array post yang diizinkan dari method yang direquest client
        $allowedPost=$this->mainModel->get_allowed_post($this->api_method);
        
        //cek validasi method
        $isValidThisMethod=$this->is_valid_method($this->api_data, $allowedPost);
        $statusMethod=$isValidThisMethod['status'];
       
        if($statusMethod==200)
        { 
            $destination=$this->api_method;
            if(method_exists($this->mainModel, $destination))
            {
                $response=$this->mainModel->set_allowed_post($this->api_data)->$destination();
            }
            else
            {
                $response=array(
                    'status'=>500,
                    'message'=>'please define your request',
                );
            }
           $this->convert_output($response);   
 
        }
        else
        {
            $this->convert_output($isValidThisMethod);
        }

    }
    
    
}

?>
