<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author tejomurti
 */
class test_api extends api_controller {
    //put your code here
    protected $mainModel;
    public function __construct() {
        parent::__construct();
        
    }
    
    public function index()
    {
        //post yang diizinkan pada method ini
        $allowedPost=array(
            'network_id',
        );
        //pengecekkan request dari client apakah telah sesuai dengan parmeter yang diizinkan server
        $isValidThisMethod=$this->is_valid_method($this->api_data, $allowedPost);

        $statusMethod=$isValidThisMethod['status'];
        $dataMethod=$this->api_data;

        if($statusMethod==200)
        { 
            //extract data jika telah sesuai
            extract($dataMethod);
            $reponse=array(
                'status'=>200,
                'message'=>'Sukses',
            );
            $this->convert_output($reponse);
        }
        else
        {
            $this->convert_output($isValidThisMethod);
        }
    }
    
    
}

?>
