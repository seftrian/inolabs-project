<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 * @author tejomurti
 * 
 * 
 */

class db_builder_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='';
    protected $primaryKey='';
    
    public function __construct() {

        $this->CI=&get_instance();
        $this->CI->load->library(array('system_lib','function_lib','form_validation'));
        if(!session_id())
        {
            session_start();
        }
   
        
    }
    
    
    public function setMainTable($mainTable)
    {
        $this->mainTable=$mainTable;
    }
    
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy=' ',$having='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($having)!='')
        {
            $sql.=' HAVING '.$having.' ';
        }
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
    
    /**
     * dapatkan semua data dengan fitur join
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @param string $select 
     * @param string $having
     * @param string $groupBy
     * @param array $join
     *          eq. $join=array(
     *              'LEFT JOIN site_administrator_group ON administrator_group_id=admin_group_id',
     *                 );
     * @return array
     */
    public function findAllCustom($where=1,$limit=10,$offset=0,$orderBy='',$select='*',$having='',$groupBy='',$join=array())
    {
//     echo $orderBy;   
        $sql='SELECT '.$select.' FROM '.$this->mainTable.' ';
        
        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery.' ';
            }
        }
        
        $sql.=' WHERE '.$where;
        
        
        if(trim($groupBy)!='')
        {  
            $sql.=' GROUP BY '.$groupBy;
        }
        
        if(trim($having)!='')
        {
            $sql.=' HAVING '.$having;
        }
        
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
//        echo $sql;
//        exit;
        $exec=$this->CI->db->query($sql);
        
        return $exec->result_array();
    }
  
    
     /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @param array $join
     *          eq. $join=array(
     *              'LEFT JOIN site_administrator_group ON administrator_group_id=admin_group_id',
     *                 );
     * @return array
     */
    public function findCustom($where=1,$join=array(),$select='*')
    {
        $sql='SELECT '.$select.' FROM '.$this->mainTable.' ';
        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery;
            }
        }
        $sql.=' WHERE '.$where;
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    
    /**
     * dapatkan total data
     * @param string $where
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * dapatkan total data
     * @param string $select
     * @param string $where
     * @param string $having
     * @param string $groupBy
     * @param string $join
     * @return array
     */
    public function countCustom($select='*',$where='1',$having='', $groupBy='',$join=array())
    {
        $sql='SELECT '.$select.' FROM '.$this->mainTable.' ';
        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery.' ';
            }
        }

        $sql.='WHERE '.$where.' ';
        
        if(trim($groupBy)!='')
        {
            $sql.=' GROUP BY '.$groupBy;
        }
        if(trim($having)!='')
        {
            $sql.=' HAVING '.$having;
        }
        $exec=$this->CI->db->query($sql);
        $row=$exec->num_rows();
        return is_numeric($row)?$row:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                //ketika klik button save
                $totalData=$this->CI->input->post('save')?count($this->postData)-1:count($this->postData);
                foreach($this->postData AS $column=>$value)
                {
                    if($column!='save')
                    {
                        $separated=($no>=1 AND $no<$totalData)?',':'';
                        $sql.=' '.$column.'="'.$this->CI->security->sanitize_filename($value,true).'"'.$separated;
                        $no++;
                    }

                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
