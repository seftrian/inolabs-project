<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api_client
 *
 * @author tejomurti
 */
class api_server{
    //put your code here
    public $CI,$allowedPost;
    protected $mainModel;
    public function __construct() {
        
        $this->CI=&get_instance();
        $this->CI->load->models(array('db_builder_lib'));
        $this->mainModel=new db_builder_lib;
        
    
        
    }

    /**
     * fungsi untuk posting jadwal prakek dokter
     */
    public function post_dokter_schedule()
    {
        $allowedPost=array(
           'dokter_id',
           'dokter_name',
           'kind_service_id',
           'kind_service_name',
           'datetime_start',
           'datetime_end',
           'sync_datetime',
        );
        $message='Error, there is no request. ';
        $status=500;
        $num=0;
        $limit=10;
        $data=array();
        if(!empty($this->allowedPost))
        {
            $message='Error. ';
            $status=500;
            extract($this->allowedPost);
            $today=date('Y-m-d H:i:s');


            $column=array(
                'schedule_dokter_id'=>(isset($dokter_id) AND trim($dokter_id)!='')?intval($dokter_id):0,
                'schedule_dokter_name'=>(isset($dokter_name) AND trim($dokter_name)!='')?$dokter_name:'-',
                'schedule_kind_service_id'=>(isset($kind_service_id) AND trim($kind_service_id)!='')?$kind_service_id:0,
                'schedule_kind_service_name'=>(isset($kind_service_name) AND trim($kind_service_name)!='')?$kind_service_name:'-',
                'schedule_datetime_start'=>(isset($datetime_start) AND trim($datetime_start)!='')?$datetime_start:date('Y-m-d H:i:s'),
                'schedule_datetime_end'=>(isset($datetime_end) AND trim($datetime_end)!='')?$datetime_end:date('Y-m-d H:i:s'),
                'schedule_sync_datetime'=>(isset($sync_datetime) AND trim($sync_datetime)!='')?$sync_datetime:date('Y-m-d H:i:s'),
            );

            //cek jika jadwal tidak ada
            $where='schedule_dokter_id='.$dokter_id.' AND schedule_dokter_name="'.$dokter_name.'" AND 
            schedule_kind_service_name="'.$kind_service_name.'" AND DATE(schedule_datetime_start)="'.date('Y-m-d').'"';
            $schedule_id=$this->CI->function_lib->get_one('schedule_id','sys_schedule_dokter',$where);
            if($schedule_id>0)
            {
                $this->CI->db->update('sys_schedule_dokter',$column,array('schedule_id'=>$schedule_id));
            }
            else
            {
                $this->CI->db->insert('sys_schedule_dokter',$column);
            }


            $id=$this->CI->function_lib->insert_id();
            $column['schedule_id']=$id;

            $status=200;
            $message='Success';
            $data=$column;
            
        }
        
        
        
        return array(
            'allowedPost'=>$allowedPost,
            'message'=>$message,
            'status'=>$status,
            'data'=>$data, 
            'num'=>intval($num),
            'limit'=>$limit,
        );        
    }

      /**
     * fungsi untuk posting antrian
     */
    public function post_schedule()
    {
        $allowedPost=array(
           'service_id',
           'service_name',
           'queue_value',
           'dokter_name',
           'datetime',
        );
        $message='Error, there is no request. ';
        $status=500;
        $num=0;
        $limit=10;
        $data=array();
        if(!empty($this->allowedPost))
        {
            $message='Error. ';
            $status=500;
            extract($this->allowedPost);
            $today=date('Y-m-d H:i:s');

            $column=array(
                'schedule_service_id'=>(isset($service_id) AND trim($service_id)!='')?intval($service_id):0,
                'schedule_service_name'=>(isset($service_name) AND trim($service_name)!='')?$service_name:'-',
                'schedule_queue_value'=>(isset($queue_value) AND trim($queue_value)!='')?$queue_value:'-',
                'schedule_dokter_name'=>(isset($dokter_name) AND trim($dokter_name)!='')?$dokter_name:'-',
                'schedule_datetime'=>(isset($datetime) AND trim($datetime)!='')?$datetime:date('Y-m-d H:i:s'),
                'schedule_sync_datetime'=>$today,
            );

            $this->CI->db->insert('sys_schedule',$column);
            $id=$this->CI->function_lib->insert_id();
            $column['schedule_id']=$id;

            $status=200;
            $message='Success';
            $data=$column;
            
        }
        
        
        
        return array(
            'allowedPost'=>$allowedPost,
            'message'=>$message,
            'status'=>$status,
            'data'=>$data, 
            'num'=>intval($num),
            'limit'=>$limit,
        );        
    }
    
    /**
     * generate token
     * @param int $token_owner_id
     */
    public function generate_token($token_owner_id)
    {
        $token=date('ymdHis');
        $expire_date=date('Y-m-d H:i:s',mktime(date('H'),date('i'),date('s'),date('m'),date('d')+5,date('Y')));
        $data=array(
            'token'=>$token,
            'token_owner_id'=>$token_owner_id,
            'token_expire_datetime'=>$expire_date,
        );
        $this->mainModel->setPostData($data);
        $this->mainModel->setMainTable('api_token');
        $this->mainModel->save();
        return $data;
    }

     public function validation_token($post=array())
    {
        $allowedPost=array(
            'network_id',
            'token'
        );
         $message='Error, token is not found or has expire. ';
         $status=500;
         
         if(!empty($post) OR !empty($this->allowedPost))
         {
             $today=date('Y-m-d H:i:s');
             if(!empty($post))
             {
                  extract($post);
             }
             else if(!empty($this->allowedPost))
             {
               extract($this->allowedPost);
             }
             $token_owner_id=isset($network_id)?$this->CI->security->sanitize_filename($network_id):'-';
             $token_value=isset($token)?$this->CI->security->sanitize_filename($token):'-';
             $this->mainModel->setMainTable('api_token');
             $where='token_owner_id="'.$token_owner_id.'"
                  AND token_value="'.$token_value.'" AND token_expire_datetime>="'.$today.'"';
             $find=$this->mainModel->find($where);
             if(!empty($find))
             {
                $message='Success';
                $status=200;
             }
             else
             {
                  $message='Unauthorized';
                  $status=401;
             }
         }
         return array(
             'allowedPost'=>$allowedPost,
              'message'=>$message,
              'status'=>$status,
         );
    }
    
    
    
    
    /**
     * set allowed post
     * @param string @allowed_post
     */ 
    public function set_allowed_post($allowed_post)
    {
        $this->allowedPost=$allowed_post;
        return $this;
    }
    
    
    /**
     * dapat post data yang diizinkan dari method yang direquest
     * @param string $method
     * 
     */
    public function get_allowed_post($method)
    {
        $allowed_post=array();
        if(method_exists($this, $method))
        {
            $call_method=$this->$method();
            $allowed_post=isset($call_method['allowedPost'])?$call_method['allowedPost']:array();
        }
        
        return $allowed_post;
    }
    
    /**
     * convert data
     * @param array $data
     * @param string $output json
     */
    
    protected function convert_output($data,$output='json')
    {
        switch(strtolower($output))
        {
            case 'json':
                echo json_encode($data);
            break;    
        }
    }
}

?>
