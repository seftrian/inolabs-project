<?php

class master_youtube extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model(array('master_content_lib','master_content_category/master_content_category_lib'));
        $this->load->helper(array('pagination'));

        $this->mainModel=new master_content_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }


    public function archives()
    {

        $limit=10;
        $where='1';
        $this->mainModel->setMainTable('site_youtube');
        $total_rows=$this->mainModel->countAll($where);
        $paginationArr=create_pagination($this->currentModule.'/'.__FUNCTION__.'/', $total_rows, $limit );
        $this->data['offset']=$paginationArr['limit'][1];    
        $order_by='youtube_pk_id DESC';
        $this->data['results']=$this->mainModel->findAll($where,$limit,$this->data['offset'],$order_by);
        
        $this->data['link_pagination']=$paginationArr['links'];
        
        $title='Video';
        $desc='Video';
        $key='Video';
        $this->seo($title,$desc,$key);
        $this->data['category_title']=$title;
       
        $view=__FUNCTION__;
        template($this->themeId,  get_class().'/'.$view,$this->data);
    }


    /**
     * 
     * @author Tejo Murti 
     * @date 2 September 2013
     * memaksimalkan seo dari content yang ada
     * @param string $title
     * @param string $desc
     * @param string $key
     */
    public function seo($title,$desc,$key,$img='')
    {
        $seoFunc=function_lib::make_seo($title, $desc, $key);
        $this->data['seoTitle']=$seoFunc['title'];
        $this->data['seoDesc']=$seoFunc['desc'];
        $this->data['seoKeywords']=$seoFunc['keywords'];
        $this->data['img_content']=$img;
        $this->data['current_url']=base_url().uri_string();
    }
    
    
}
