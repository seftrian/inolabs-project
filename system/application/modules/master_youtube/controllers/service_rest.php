<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('master_youtube_lib');
        $this->mainModel=new master_youtube_lib;

       // $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";

        $youtube_id=$this->input->get('youtube_id',true);
        $youtube_title=$this->input->get('youtube_title',true);
        $youtube_title=str_replace('%20', ' ', $youtube_title);
        $youtube_description=$this->input->get('youtube_description',true);
        $youtube_description=str_replace('%20', ' ', $youtube_description);

        $where=1;

        $where.=' AND youtube_id LIKE "%'.$youtube_id.'%" AND youtube_title LIKE "%'.$youtube_title.'%" AND youtube_description LIKE "%'.$youtube_description.'%" ';
        $params['where'] =$where;

        $params['order_by'] = "
            youtube_input_timestamp DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
            $no++;
          
            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$youtube_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$youtube_id.'\');"><i class="clip-remove"></i></a>';
            $content=strip_tags($youtube_description);
            $content=(strlen($youtube_description)<150)?$youtube_description:substr($youtube_description,0,150);
            $preview='<iframe width="300" src="https://www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe>';
            $entry = array('id' => $youtube_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'id' =>$youtube_id,
                    'title' =>$youtube_title.' <br /><p style="float:right;" class="text-muted">'.$youtube_id.'</p>',
                    'description' =>'<p style="text-align:center;">'.$preview.'</p>'.$content,
                    'input_by' => $youtube_input_by,
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
