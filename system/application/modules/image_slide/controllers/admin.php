<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller
{

    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct()
    {
        parent::__construct();

        //load model
        $this->load->model(array('image_slide_lib'));
        $this->load->helper(array('pagination', 'tinymce'));

        //inisialisasi model
        $this->mainModel = new image_slide_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr = $this->mainModel->getImagePath();
    }
    // get tampilan data 
    function get_data()
    {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            " . $this->mainModel->getMainTable() . ".*
        ";


        $where = 1;
        $params['where'] = $where;

        $params['order_by'] = "
            slider_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {

            foreach ($row as $variable => $value) {
                ${$variable} = $value;
            }
            $no++;

            $logo_name = (trim($slider_image_src) == '') ? '-' : pathinfo($slider_image_src, PATHINFO_FILENAME);
            $logo_ext =  pathinfo($slider_image_src, PATHINFO_EXTENSION);
            $logo_file = $logo_name . '.' . $logo_ext;
            $pathImgUrl = $this->pathImgArr['pathUrl'];
            $pathImgLoc = $this->pathImgArr['pathLocation'];
            $imgProperty = array(
                'width' => 140,
                'height' => 95,
                'imageOriginal' => $logo_file,
                'directoryOriginal' => $pathImgLoc,
                'directorySave' => $pathImgLoc . '14095/',
                'urlSave' => $pathImgUrl . '14095/',
            );
            $img = $this->function_lib->resizeImageMoo($imgProperty);
            // button edit
            $edit = '<a class="btn btn-xs btn-primary" href="' . base_url() . 'admin/' . $this->currentModule . '/update/' . $slider_id . '" title="Edit"><i class="clip-pencil"></i></a>';
            // button delete 
            $delete = ' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\'' . $slider_id . '\');"><i class="clip-remove"></i></a>';

            $slider_category_name = $this->function_lib->get_one('slider_category_name', 'site_slider_category', 'slider_category_id=' . $slider_slider_category_id);
            $video = (trim($slider_youtube_id) != '') ? '<br /><iframe style="width:100%" height="315" src="https://www.youtube.com/embed/' . $slider_youtube_id . '" frameborder="0" allowfullscreen></iframe>' : '';
            $entry = array(
                'id' => $slider_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'title' => $slider_title . '<br /><em style="color:#C00"">' . $slider_category_name . '</em>',
                    'meta_description' => $slider_short_description . $video,
                    'photo' => '<img src="' . $img . '" />',

                ),
            );
            $json_data['rows'][] = $entry;
        }

        echo json_encode($json_data);
    }

    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {

        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Image Slider';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Image Slider',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/' . $this->currentModule . '/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk menambahkan data
     */
    public function add()
    {

        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';


        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Image Slider';
        $description = 'Tambah Slider';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );

        $breadcrumbs_array[] = array(
            'name' => 'Image Slider',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/image_slide/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url() . 'admin/imege_slide/add',
            'current' => true, //boolean
        );

        $this->data['slider_category'] = $this->mainModel->get_slider_category();
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);

        if ($this->input->post('save')) {
            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation();
            $status = $isValidationPassed['status'];
            $message = ($status == 500 and $isValidationPassed['message'] == '') ? validation_errors() : $isValidationPassed['message'];

            if ($status == 200) {
                $title = $this->input->post('slider_title');
                $admin_username = isset($_SESSION['admin']['detail']['admin_username']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                $slider_short_description = $this->input->post('slider_short_description');
                $columns = array(
                    'slider_slider_category_id' => $this->input->post('slider_category_id', true),
                    'slider_title' => $this->input->post('slider_title', true),
                    'slider_short_description' => htmlentities($slider_short_description),
                    'slider_link' => $this->input->post('slider_link', true),
                    'slider_youtube_id' => $this->input->post('slider_youtube_id', true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved = $this->mainModel->save();
                $status = $hasSaved['status']; //status saat penyimpanan
                if ($status == 200) {
                    $id = $this->function_lib->insert_id();

                    //handle logo
                    $logo = $this->mainModel->handleUpload($id, 'slider_image_src');
                    if ($logo['status'] != 200) {
                        $status = $logo['status'];
                        $message = $logo['message'];
                        redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=500&msg=' . $msg);
                    } else {
                        $msg = base64_encode('Data berhasil disimpan');
                        if (isset($_GET['redirect'])) {
                            redirect(rawurldecode($_GET['redirect']));
                        } else {
                            redirect(base_url() . 'admin/' . $this->currentModule . '/add?status=200&msg=' . $msg);
                        }
                    }
                }
            }
        }


        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id = 0)
    {
        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Image Slider';
        $description = 'Ubah Slider';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );

        $breadcrumbs_array[] = array(
            'name' => 'Image Slider',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/image_slide/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Ubah',
            'class' => "clip-pencil",
            'link' => base_url() . 'admin/image_slide/add',
            'current' => true, //boolean
        );
        $this->data['slider_category'] = $this->mainModel->get_slider_category();
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);


        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }

        $rowMenu = $this->mainModel->find('slider_id=' .  intval($id));
        $width = $this->function_lib->get_one('slider_category_width', 'site_slider_category', 'slider_category_id=' . $rowMenu['slider_slider_category_id']);
        $height = $this->function_lib->get_one('slider_category_height', 'site_slider_category', 'slider_category_id=' . $rowMenu['slider_slider_category_id']);
        $this->data['size_image_slider_category'] = $width . ' X ' . $height;
        if (empty($rowMenu)) {
            show_error('Request anda tidak valid', 500);
        }

        foreach ($rowMenu as $variable => $value) {
            $this->data[$variable] = $value;
        }

        if ($this->input->post('save')) {
            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation($id);
            $status = $isValidationPassed['status'];
            $message = ($status == 500 and $isValidationPassed['message'] == '') ? validation_errors() : $isValidationPassed['message'];

            if ($status == 200) {
                $title = $this->input->post('slider_title');
                $admin_username = isset($_SESSION['admin']['detail']['admin_username']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                $slider_short_description = $this->input->post('slider_short_description');

                $columns = array(
                    'slider_slider_category_id' => $this->input->post('slider_category_id', true),
                    'slider_title' => $this->input->post('slider_title', true),
                    'slider_short_description' => htmlentities($slider_short_description),
                    'slider_link' => $this->input->post('slider_link', true),
                    'slider_youtube_id' => $this->input->post('slider_youtube_id', true),
                );

                $this->mainModel->setPostData($columns);
                $where = 'slider_id=' .  intval($id);
                $hasSaved = $this->mainModel->save($where);
                $status = $hasSaved['status']; //status saat penyimpanan
                if ($status == 200) {

                    $msg = base64_encode('Data berhasil disimpan');
                    //handle logo
                    $logo = $this->mainModel->handleUpload($id, 'slider_image_src');
                    if ($logo['status'] != 200) {
                        $status = $logo['status'];
                        $message = strip_tags($logo['message']);
                        $msg = base64_encode('Terjadi error saat upload file. ' . $message);
                    }

                    redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=' . $status . '&msg=' . $msg);
                }
            }
        }
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['pathImgArr'] = $this->pathImgArr;


        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }


    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    public function delete_image($id = 0, $output = 'redirect')
    {
        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }
        $where = 'slider_id=' .  intval($id);
        $this->data['rowAdmin'] = $this->mainModel->find($where);

        if (!empty($this->data['rowAdmin'])) {
            $sql = 'UPDATE site_slider SET slider_image_src="-" WHERE ' . $where;
            $this->db->query($sql);
            redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=200&msg=' . base64_encode('Gambar berhasil dihapus'));
        } else {
            $msg = 'Data tidak ditemukan';
            redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=500&msg=' . base64_encode($msg));
        }
    }

    public function act_delete()
    {
        $arr_output = array();
        $arr_output['status'] = 500;
        $arr_output['message'] = 'Tidak ada aksi yang dilakukan';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                    $additional_where = ' '; //default telah ngecek data dihapus
                    if ($this->available_data($id, $additional_where)) {
                        $this->mainModel->setMainTable('site_slider');
                        $this->mainModel->delete('slider_id=' . intval($id));
                        $deleted_count++;
                    }
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['status'] = 200;
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['status'] = 500;
            }
        }

        echo json_encode($arr_output);
    }

    public function delete($id = '')
    {
        $arr_output = array();
        $arr_output['message'] =  'Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 500;
        $additional_where = ' '; //default telah ngecek data dihapus
        if ($this->available_data($id, $additional_where)) {
            $this->mainModel->setMainTable('site_slider');
            $this->mainModel->delete('slider_id=' . intval($id));

            $arr_output['message'] =  ' Konten telah dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

    /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
     */
    protected function available_data($id, $additional_where = '')
    {

        $result = false;
        $scalar = $this->function_lib->get_one(
            'slider_id',
            'site_slider',
            'slider_id=' . intval($id) . ' ' . $additional_where
        );
        if ($scalar) {
            $result = true;
        }

        return $result;
    }

    /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
     * @param string $method
     * @param array $extraVariable
     * **/
    protected function footerScript($method = '', $extraVariable = array())
    {

        //set variable
        if (!empty($extraVariable)) {
            extract($extraVariable);
        }

        ob_start();

?>
        <script type="text/javascript">
            function get_size_slider_category(category_id) {
                var div_alert = $("em.image-size");
                var jqxhr = $.ajax({
                    url: '<?php echo base_url() . $this->currentModule ?>/service_rest/get_size_slider_category/' + category_id,
                    type: 'get',
                    dataType: 'json',
                });
                jqxhr.success(function(response) {
                    div_alert.slideDown('medium');
                    div_alert.html(response);
                    return false;
                });
                jqxhr.error(function() {
                    alert('an error has occurred, please try again.');
                    return false;
                });

                return false;
            }

            function preview_youtube(elem) {
                var youtube_id = $("input[name='slider_youtube_id']").val();

                if (typeof(elem) != 'undefined') {
                    youtube_id = elem.val();
                }

                if ($.trim(youtube_id) != '') {
                    $("#div-preview-youtube").slideDown('medium');
                    $("#preview-youtube").html('loading..');
                    var html_youtube = '<iframe style="width:100%" height="315" src="https://www.youtube.com/embed/' + youtube_id + '" frameborder="0" allowfullscreen></iframe>';

                    $("#preview-youtube").html(html_youtube);
                }

                if ($.trim(youtube_id) == '') {
                    $("#div-preview-youtube").slideUp('medium');
                }

                return false;
            }


            $(function() {
                preview_youtube();
            });
        </script>

        <?php
        switch ($method) {
            case 'index':
        ?>
                <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            {
                                display: 'No.',
                                name: 'no',
                                width: 30,
                                sortable: true,
                                align: 'right'
                            },
                            {
                                display: 'Edit',
                                name: 'edit',
                                width: 50,
                                sortable: true,
                                align: 'center'
                            },
                            {
                                display: 'Delete',
                                name: 'delete',
                                width: 50,
                                sortable: true,
                                align: 'center'
                            },
                            {
                                display: 'Judul',
                                name: 'title',
                                width: 250,
                                sortable: true,
                                align: 'left'
                            },
                            {
                                display: 'Meta Deskripsi',
                                name: 'meta_description',
                                width: 500,
                                sortable: true,
                                align: 'left'
                            },
                            {
                                display: 'Gambar',
                                name: 'photo',
                                width: 150,
                                sortable: true,
                                align: 'center'
                            },

                        ],
                        buttons: [{
                                display: 'Input Data',
                                name: 'add',
                                bclass: 'add',
                                onpress: add
                            },
                            {
                                separator: true
                            },
                            {
                                display: 'Pilih Semua',
                                name: 'selectall',
                                bclass: 'selectall',
                                onpress: check
                            },
                            {
                                separator: true
                            },
                            {
                                display: 'Batalkan Pilihan',
                                name: 'selectnone',
                                bclass: 'selectnone',
                                onpress: check
                            },
                            {
                                separator: true
                            },
                            {
                                display: 'Hapus Item Terpilih',
                                name: 'delete',
                                bclass: 'delete',
                                onpress: act_delete
                            },
                        ],
                        buttons_right: [
                            //                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],

                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false,
                        nowrap: false,
                    });

                    function act_delete(com, grid) {
                        var div_alert = $("div.alert");

                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if ($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if (conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url() . 'admin/' . $this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        div_alert.slideDown('medium');
                                        if (response['status'] != 200) {
                                            div_alert.addClass('alert-danger');
                                            div_alert.removeClass('alert-success');
                                        } else {
                                            div_alert.removeClass('alert-danger');
                                            div_alert.addClass('alert-success');
                                        }
                                        div_alert.html(response['message']);
                                    },
                                    error: function() {
                                        alert('an error has occurred, please try again');
                                        grid_reload();
                                    }
                                });
                            }
                        }
                    }


                    $(document).ready(function() {
                        grid_reload();
                    });


                    function grid_reload() {
                        var link_service = '';
                        $("#gridview").flexOptions({
                            url: '<?php echo base_url() . 'admin/' . $this->currentModule; ?>/get_data' + link_service
                        }).flexReload();
                    }

                    function delete_transaction(id) {
                        var div_alert = $("div.alert");
                        if (confirm('Delete?')) {
                            var jqxhr = $.ajax({
                                url: '<?php echo base_url() . 'admin/' . $this->currentModule ?>/delete/' + id,
                                type: 'get',
                                dataType: 'json',

                            });
                            jqxhr.success(function(response) {
                                div_alert.slideDown('medium');
                                if (response['status'] != 200) {
                                    div_alert.addClass('alert-danger');
                                    div_alert.removeClass('alert-success');
                                } else {
                                    div_alert.removeClass('alert-danger');
                                    div_alert.addClass('alert-success');
                                }
                                div_alert.html(response['message']);
                                grid_reload();
                                return false;

                            });
                            jqxhr.error(function() {
                                alert('an error has occurred, please try again.');
                                grid_reload();
                                return false;
                            });
                        }
                        return false;

                    }

                    function search_by_category($category_id) {
                        $("select#news_category_id").val($category_id);
                        grid_reload();
                        return false;
                    }
                </script>
<?php
                break;
        }

        $footerScript = ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>