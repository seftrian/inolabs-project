<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('image_slide_lib');
        $this->mainModel=new image_slide_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_size_slider_category($slider_category_id) {
        $width = $this->function_lib->get_one('slider_category_width','site_slider_category','slider_category_id='.$slider_category_id);
        $height = $this->function_lib->get_one('slider_category_height','site_slider_category','slider_category_id='.$slider_category_id);
        $slider_size = 'Ukuran gambar yang dianjurkan minimal '.$width.' X '.$height;
        echo json_encode($slider_size);
    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";


        $where=1;
        $params['where'] =$where;

        $params['order_by'] = "
            slider_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $logo_name= (trim($slider_image_src)=='')?'-': pathinfo($slider_image_src,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($slider_image_src,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>140,
            'height'=>95,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'14095/',
            'urlSave'=>$pathImgUrl.'14095/',
            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$slider_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$slider_id.'\');"><i class="clip-remove"></i></a>';
            $slider_category_name=$this->function_lib->get_one('slider_category_name','site_slider_category','slider_category_id='.$slider_slider_category_id);


            $entry = array('id' => $slider_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'title' =>$slider_title.'<br /><em style="color:#C00"">'.$slider_category_name.'</em>',                    
                    'meta_description' => $slider_short_description,
                    'photo' => '<img src="'.$img.'" />',

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
