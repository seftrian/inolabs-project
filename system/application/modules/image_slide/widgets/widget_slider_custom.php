<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author anggoro
 */
class widget_slider_custom extends widget_lib{
    //put your code here
    public function run($module,$class_widget,$category,$limit=1)
    {

        $this->load->model('image_slide/image_slide_lib');
        $lib=new image_slide_lib;

        $data=array();
        $where = 'slider_category_name="'.$category.'"';
        $sql='SELECT * FROM site_slider 
            INNER JOIN site_slider_category ON slider_category_id=slider_slider_category_id
            WHERE '.$where.'
            ORDER BY slider_id DESC
            LIMIT '.$limit.'
            ';
        $exec=$this->db->query($sql);      
        $data['results']=$exec->row_array();
        $data['pathImgArr']=$lib->getImagePath();

        $this->render(get_class(),$data);
    }
}

?>
