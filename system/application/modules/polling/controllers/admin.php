<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('polling_lib',));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new polling_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index($id=0)
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['polling_category_name']=$this->mainModel->get_category_name($id);
        $this->data['seoTitle'] = 'Polling '.$this->data['polling_category_name'];
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Polling '.$this->data['polling_category_name'],
            'class' => "fa fa-question-circle",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $this->data['results']=$this->mainModel->get_polling_by_category_id($id);
        $this->data['category_id']=$id;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
    menampilkan statistik jawaban
    @param int $id
    */
    public function statistik($id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['polling_category_name']=$this->mainModel->get_category_name($id);
        $this->data['seoTitle'] = 'Statistik '.$this->data['polling_category_name'];
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Polling '.$this->data['polling_category_name'],
            'class' => "fa fa-question-circle",
            'link' => base_url().'admin/'.$this->currentModule.'/index/'.$id,
            'current' => false, //boolean
        );
          $breadcrumbs_array[] = array(
            'name' => 'Statistik '.$this->data['polling_category_name'],
            'class' => "fa fa-bar-chart-o",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $this->data['results']=$this->mainModel->get_polling_by_category_id($id);
        $this->data['category_id']=$id;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
      /**
    menampilkan statistik jawaban
    @param int $id
    */
    public function statistik_user($category_id, $question_id=0,$answer_id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['polling_category_name']=$this->mainModel->get_category_name($category_id);
        $this->data['seoTitle'] = 'User '.$this->data['polling_category_name'];
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Polling '.$this->data['polling_category_name'],
            'class' => "fa fa-question-circle",
            'link' => base_url().'admin/'.$this->currentModule.'/index/'.$category_id,
            'current' => false, //boolean
        );
          $breadcrumbs_array[] = array(
            'name' => 'Statistik '.$this->data['polling_category_name'],
            'class' => "fa fa-bar-chart-o",
            'link' => base_url().'admin/'.$this->currentModule.'/statistik/'.$category_id,
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'User ',
            'class' => "fa fa-user",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $additional_where=' AND polling_question_id='.intval($question_id);
        $this->data['results']=$this->mainModel->get_polling_by_category_id($category_id,$additional_where);
        $this->data['category_id']=$category_id;
        $this->data['question_id']=$question_id;
        $this->data['answer_id']=$answer_id;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
   
     /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function user($id=0)
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['polling_category_name']=$this->mainModel->get_category_name($id);
        $this->data['seoTitle'] = 'User '.$this->data['polling_category_name'];
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Polling '.$this->data['polling_category_name'],
            'class' => "fa fa-question-circle",
            'link' => base_url().'admin/'.$this->currentModule.'/index/'.$id,
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'User '.$this->data['polling_category_name'],
            'class' => "fa fa-user",
            'link' =>'#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $this->data['category_id']=$id;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

      /**
     *menampilkan jawaban oleh user
     */
    public function answer_user($category_id=0,$user_id=0)
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['polling_category_name']=$this->mainModel->get_category_name($category_id);
        $this->data['seoTitle'] = 'Jawaban User ';
        $description = 'Rekapan jawaban oleh user';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Polling '.$this->data['polling_category_name'],
            'class' => "fa fa-question-circle",
            'link' => base_url().'admin/'.$this->currentModule.'/index/'.$category_id,
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'User '.$this->data['polling_category_name'],
            'class' => "fa fa-user",
            'link' => base_url().'admin/'.$this->currentModule.'/user/'.$category_id,
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Jawaban User',
            'class' => "clip-lamp",
            'link' =>'#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $this->data['category_id']=$category_id;
        $this->data['user_id']=$user_id;
        $this->data['results']=$this->mainModel->get_polling_by_category_id($category_id);
        $this->data['user_arr']=$this->mainModel->get_user_detail($user_id);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        switch($method)
        {
            case 'index':
            ?>
            <script type="text/javascript">
            var form_polling=$("form#polling");
            function render_question(elem)
            {
                var count_div_question=$("div.polling_question").length;
                var current_number=parseInt(count_div_question);
                var number=(current_number+1);
                var output_html=' <div class="row polling_question">'
                +'<input type="hidden" class="polling_question_order_number" value="'+current_number+'">'
                +'<input type="hidden" name="polling_question_id['+current_number+']" value="0">'
                +' <div class="form-group">'
                +'  <div class="col-sm-1"><label class="col-sm-offset-11" id="label_number"> '+number+'. </label></div>'
                +'  <div class="col-sm-9">'
                +'    <textarea class="form-control" name="polling_question_name['+current_number+']" placeholder="Pertanyaan..."></textarea>'
                +'  </div>'
                +'  <div class="col-sm-1">'
                +'    <input type="text" name="polling_question_order_number['+current_number+']" value="'+number+'" class="form-control input_polling_question_order_number">'
                +'  </div>'
                +'  <button onclick="remove_question($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>'
                +'</div>'
                +'<div class="form-group polling_answer">'
                +'    <div class="col-sm-2">&nbsp;</div>'
                +'    <div class="col-sm-8">'
                +'      <input type="text" class="form-control" name="polling_answer_name['+current_number+'][0]" placeholder="Jawaban..." />'
                +'    </div>'
                +'    <div class="col-sm-1">'
                +'      <input type="hidden" value="1" name="polling_answer_order_number['+current_number+'][0]" class="form-control input_polling_answer_order_number">'
                +'    </div>'
                +'</div>'
                +'<button  onclick="render_answer($(this));return false;" style="float:right;margin-left:50px; margin-bottom: 50px;" class="btn btn-primary btn-xs">Tambah Jawaban</button>'
                +'</div>';
                form_polling.append(output_html);

                //sesuaikan penomoran
                question_order_number();
                return false;
            }

            /**untuk menambahkan jawaban*/
            function render_answer(elem)
            {
                var parent_answer=elem.parent();
                //console.log();
                var current_number=parseInt(parent_answer.find('.polling_answer').length);
                var number=(current_number+1);
                var polling_question_order_number=parent_answer.find(".polling_question_order_number").val();
               
                var output_html='<div class="form-group polling_answer">'
                    +'<div class="col-sm-2">&nbsp;</div>'
                    +'<div class="col-sm-8">'
                    +' <input type="hidden" name="polling_answer_id['+polling_question_order_number+']['+current_number+']" value="0">'
                    +'  <input type="text" class="form-control" name="polling_answer_name['+polling_question_order_number+']['+current_number+']" placeholder="Jawaban..." />'
                    +'</div>'
                    +'<div class="col-sm-1">'
                    +'  <input type="hidden" value="'+number+'" name="polling_answer_order_number['+polling_question_order_number+']['+current_number+']" class="form-control input_polling_answer_order_number">'
                    +'</div>'
                    +'<button onclick="remove_answer($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button> '
                    +'</div>';

                elem.before( output_html );    

                //sesuaikan penomoran
                $.each(parent_answer.find(".polling_answer"),function(index,value){
                    var order_number=(parseInt(index)+1);
                    $(this).find(".input_polling_answer_order_number").val(order_number);
                }); 
            }

            /** hapus pertanyaan */
            function remove_question(elem)
            {
                var parent=elem.parent().parent();

                //hapus yang ada pada container polling_question
                if(confirm('Dengan menghapus pertanyaan ini, maka jawaban-jawabannya juga akan terhapus. Lanjutkan?')){
                    parent.slideUp('medium',function(){
                         parent.remove();   
                         //sesuaikan penomoran
                        question_order_number();
                    });
                }
                return false;
            }

               /** hapus jawaban */
            function remove_answer(elem)
            {
                var parent=elem.parent();

                //hapus yang ada pada container polling_question
                if(confirm('Hapus jawaban?')){
                    parent.slideUp('medium',function(){
                        parent.remove();   
                       
                    });
                }
                return false;
            }


            /**fungsi penomoran pertanyaan*/
            function question_order_number()
            {
                   //sesuaikan penomoran
                $.each($("form > div.polling_question"),function(index,value){
                    var order_number=(parseInt(index)+1);
                    $(this).find("#label_number").html(order_number+'.');
                    $(this).find(".input_polling_question_order_number").val(order_number);
                }); 
            }

            /**
            simpan data 
            */
            function stored_database(elem,category_id)
            {
                 var div_alert=$("div.alert");
                if(confirm('Simpan?'))
                {
                      elem.attr('disabled',true);
                      elem.html('processing...');  
                      var jqxhr=$.ajax({
                            url:'<?php echo base_url().$this->currentModule.'/service_rest/stored_database/'?>'+category_id,
                            dataType:'json',
                            type:'post',
                            data:$('form#polling').serializeArray(),
                        });
                        jqxhr.success(function(response_json){
                            div_alert.show();
                            if(response_json['status'])
                            {
                                div_alert.removeClass('alert-danger');
                                div_alert.addClass('alert-success');
                                setTimeout(window.location.href='<?php echo base_url()?>admin/polling/index/'+category_id,20000)
                            }
                            else
                            {
                                div_alert.removeClass('alert-danger');
                                div_alert.addClass('alert-success');
                            }
                            div_alert.html(response_json['message']);
                            elem.attr('disabled',false);
                            elem.html('Simpan');
                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again!');
                        });
                               
                }
                return false;
            }
            </script>
            <?php
            break;   
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
