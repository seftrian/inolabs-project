<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class f_polling extends front_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('polling_lib',));
        $this->load->helper(array('pagination'));
        
        //inisialisasi model
        $this->mainModel=new polling_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        //$this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index($id=0)
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['polling_category_name']=$this->mainModel->get_category_name($id);
        $this->data['seoTitle'] = 'Polling '.$this->data['polling_category_name'];
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $this->data['results']=$this->mainModel->get_polling_by_category_id($id);
        $this->data['category_id']=$id;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
    cek pengisian form polling
    */
    public function validation_polling($category_id=0)
    {
        $response=$this->mainModel->validation_polling($category_id);
        echo json_encode($response);
    }

     /**
    cek pengisian form polling
    */
    public function save_polling($category_id=0)
    {
        $response=$this->mainModel->save_polling($category_id);
        echo json_encode($response);
    }
    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        ?>
        <?php
        switch($method)
        {
            case 'index':
            ?>
            <script type="text/javascript">
            var div_alert_polling=$("#alert-polling");
            var div_alert_registration=$("#alert-registration");
            function validation_polling(category_id)
            {
                
                    var jqxhr=$.ajax({
                    url:'<?php echo base_url()?>polling/f_polling/validation_polling/'+category_id,
                    data:$("form#polling").serializeArray(),
                    dataType:'json',
                    type:'post'
                    });
                    jqxhr.success(function(response){
                        div_alert_polling.show();
                        div_alert_polling.html(response['message']);

                        if(response['status']!=200)
                        {
                            div_alert_polling.removeClass('alert-success');
                            div_alert_polling.addClass('alert-danger');
                        }
                        else
                        {
                            div_alert_polling.removeClass('alert-danger');
                            div_alert_polling.addClass('alert-success');   
                            $('.nav-tabs a[href=#registration]').tab('show') ;
                            $("input[name='full_name']").focus();
                            
                        }

                    });
                    jqxhr.error(function(){
                        alert('an error has occurred, please try again!');
                    });
                    return false;
                
            }
            function save_polling(category_id)
            {
                if(confirm('Simpan'))
                {
                    var jqxhr=$.ajax({
                    url:'<?php echo base_url()?>polling/f_polling/save_polling/'+category_id,
                    data:$("form#form-registration").serializeArray(),
                    dataType:'json',
                    type:'post'
                    });
                    jqxhr.success(function(response){
                        div_alert_polling.show();
                        div_alert_registration.show();
                        div_alert_polling.html(response['message']);

                        if(response['status']==401)
                        {
                            div_alert_polling.removeClass('alert-success');
                            div_alert_polling.addClass('alert-danger');
                            $('.nav-tabs a[href=#polling]').tab('show') ;
                        }
                        else if(response['status']==500)
                        {
                            div_alert_registration.removeClass('alert-success');
                            div_alert_registration.addClass('alert-danger');
                            div_alert_registration.html(response['message']);
                        }
                        else
                        {
                            div_alert_registration.removeClass('alert-danger');
                            div_alert_registration.addClass('alert-success');
                            div_alert_registration.html(response['message']);

                            setTimeout(div_alert_registration.slideUp('medium',function(){
                                window.location.href='<?php echo base_url().'polling/f_polling/index/'?>'+category_id;
                                return false;
                            }),10000);   
                        }

                    });
                    jqxhr.error(function(){
                        alert('an error has occurred, please try again!');
                    });
                    return false;
                }
                
            }
            </script>
            <?php
            break;   
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
