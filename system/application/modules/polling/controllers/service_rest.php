<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('polling_lib');
        $this->mainModel=new polling_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_user($category_id=0) {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = 'sys_polling_statistik_header';
        $params['select'] = "
            sys_polling_statistik_header.*,
            person_full_name,person_phone, person_id
            
        ";
        $params['join'] = "
            INNER JOIN sys_person ON person_id=spsh_person_id
        ";


        $where=1;
        $where.=' AND spsh_polling_category_id='.intval($category_id);
        $params['where'] =$where;

        $params['order_by'] = "
            spsh_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
            $detail=base_url().'admin/polling/answer_user/'.$category_id.'/'.$person_id;
            $where='additional_info_person_id='.intval($person_id).' ORDER BY additional_info_id DESC';
            $email_address=$this->function_lib->get_one('additional_info_email_address','sys_person_additional_info',$where);
            $entry = array('id' => $spsh_id,
                'cell' => array(
                    'no' =>$no,
                    'full_name' =>'<a href="'.$detail.'">'.$person_full_name.'</a>',
                    'email' =>$email_address,
                    'phone' => $person_phone,
                    'note' => $spsh_note,
                    'timestamp' => convert_datetime($spsh_timestamp,'ina'),
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

    function get_statistik_user($question_id,$answer_id=0) {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = 'sys_polling_statistik_detail';
        $params['select'] = "
            sys_polling_statistik_detail.*,
            spsh_timestamp,person_full_name,person_phone, person_id
            
        ";
        $params['join'] = "
            INNER JOIN sys_polling_statistik_header ON spsh_id=spsd_spsh_id
            INNER JOIN sys_person ON person_id=spsh_person_id
        ";


        $where=1;
        $where.=' AND spsd_polling_question_id='.intval($question_id);
        if($answer_id>0)
        {
            $where.=' AND spsd_polling_answer_id='.intval($answer_id);
        }    
        $params['where'] =$where;
        $params['group_by'] ='person_id';

        $params['order_by'] = "
            spsd_spsh_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
        
            $entry = array('id' => $spsd_spsh_id,
                'cell' => array(
                    'no' =>$no,
                    'full_name' =>$person_full_name,
                    'email' =>'-',
                    'phone' => $person_phone,
                    'timestamp' => convert_datetime($spsh_timestamp,'ina'),
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

    /**
    simpan pertanyaan berdsarkan id category
    @parma int $category_id
    */
    public function stored_database($category_id)
    {
        $response=$this->mainModel->stored_database($category_id);
        echo json_encode($response);
    }
    
}
