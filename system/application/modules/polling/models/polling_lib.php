<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
//require_once dirname(__FILE__).'/../components/core_menu_lib.php';

//class admin_menu extends core_menu_lib{
class polling_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='sys_polling_question';
    protected $dir='assets/images/polling/';
    public function __construct() {

  //      parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));
                              
        if(!session_id())
        {
            session_start();
        }

              
    }

    /**
    simpan pengisian polling
    @param int $category_id
    */
    public function validation_polling($category_id)
    {
      $polling_answer_arr=$this->CI->input->post('spsd_polling_answer_id');
      //hitung jumlah yang telah dijawab
      $count_answer=count($polling_answer_arr);
      $num_of_question=self::count_question($category_id);
      $data=array();
      $data['status']=500;
      $data['message']='Error! mohon lengkapi form isian.';
     
      if($count_answer<$num_of_question)
      {
        $data['status']=500;
        $data['message']='Error! Anda wajib menjawab  '.$num_of_question.' pertanyaan yang tersedia.';
      }
      else
      {

        $_SESSION['polling']=$polling_answer_arr;
        $data['status']=200;
        $data['message']='OK';
      }
      return $data;
    }

     /**
    simpan pengisian polling
    @param int $category_id
    */
    public function save_polling($category_id)
    {
      $polling_answer_arr=isset($_SESSION['polling'])?$_SESSION['polling']:array();

      $data['status']=401;
      $data['message']='Error! mohon lengkapi form isian.';
     
      if(!empty($polling_answer_arr))
      {
        $full_name=$this->CI->input->post('full_name',true);
        $email_address=$this->CI->input->post('email_address',true);
        $phone=$this->CI->input->post('phone',true);
        $spsh_note=$this->CI->input->post('spsh_note',true);

        $validation_arr=$this->formValidation();
        $data['status']=$validation_arr['status'];
        $data['message']=$validation_arr['message'];
        if($data['status']==200)
        {
          //simpan data
          $column=array(
            'person_full_name'=>$full_name,
            'person_phone'=>$phone,
            'person_person_type_id'=>0,
            'person_admin_username'=>'polling',
          );
          $this->CI->db->insert('sys_person',$column);
          $person_id=$this->CI->function_lib->insert_id();

          $column=array(
            'additional_info_person_id'=>$person_id,
            'additional_info_email_address'=>$email_address,
          );
          $this->CI->db->insert('sys_person_additional_info',$column);

          //save header and comment
          $column=array(
            'spsh_polling_category_id'=>$category_id,
            'spsh_person_id'=>$person_id,
            'spsh_note'=>$spsh_note,
          );
          $this->CI->db->insert('sys_polling_statistik_header',$column);
          $spsh_id=$this->CI->function_lib->insert_id();

          //save polling
          foreach($polling_answer_arr AS $question_id=>$anwer_id)
          {
            $column=array(
              'spsd_spsh_id'=>$spsh_id,
              'spsd_polling_question_id'=>$question_id,
              'spsd_polling_answer_id'=>$anwer_id,
            );
            $this->CI->db->insert('sys_polling_statistik_detail',$column);

          }

          //remove session polling
          unset($_SESSION['polling']);

          //send to email
           $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));

           $this->CI->load->library('send_mail');
           $sendMail=new send_mail; 
           $email_to=$email_address;
           $email_subject='Polling '.$this->get_category_name($category_id);
           $email_message='Terimakasih atas partisipasi anda dalam pengisian polling ini.';
           $email_to_name='';
           
            //email for admin
           $emailProperty=array(
               'email_to'=>$email_to,
               'email_subject'=>$email_subject,
               'email_message'=>$email_message,
               'email_from'=>'noreply@'.$web_name,
               'email_from_name'=>$web_name,
               'email_from_organization'=>'',
           );
         
          $sendMail->run($emailProperty);
          $data['status']=200;
          $data['message']='OK';
        }

      }

      return $data;
    }

    /**
    hitung total responden
    @param int $category_id
    */
    public static function count_question($category_id)
    {
      $lib=new polling_lib;
      $lib->mainTable='sys_polling_question';
      $where='polling_question_polling_category_id='.intval($category_id);
      $count=$lib->countAll($where);
      return $count;
    }

     /**
    hitung total responden
    @param int $category_id
    */
    public static function count_responden($category_id)
    {
      $lib=new polling_lib;
      $lib->mainTable='sys_polling_statistik_header';
      $where='spsh_polling_category_id='.intval($category_id);
      $count=$lib->countAll($where);
      return $count;
    }

     /**
    dapatkan jawaban user
    @param int $answer_id
    @param int $user_id
    */
    public function get_user_detail($user_id)
    {
      
      $data=array();
      //full name
      $where='person_id='.intval($user_id);
      $data['full_name']=$this->CI->function_lib->get_one('person_full_name','sys_person',$where);
     
      $where='additional_info_person_id='.intval($user_id);
      $data['email_address']=$this->CI->function_lib->get_one('additional_info_email_address','sys_person_additional_info',$where.' ORDER BY additional_info_id DESC');
     
       $where='person_id='.intval($user_id);
      $data['phone']=$this->CI->function_lib->get_one('person_phone','sys_person',$where);
     

      return $data;  
    }

    /**
    dapatkan jawaban user
    @param int $answer_id
    @param int $user_id
    */
    public static function get_answer_user($answer_id,$user_id)
    {
      $lib=new polling_lib;
      $sql=' SELECT COUNT(spsh_id) AS num FROM sys_polling_statistik_header
            INNER JOIN sys_polling_statistik_detail ON spsd_spsh_id=spsh_id
            WHERE spsh_person_id='.intval($user_id).'
            AND spsd_polling_answer_id='.intval($answer_id);
      $exec=$lib->CI->db->query($sql);
      $row=$exec->row_array();    
      $value=(!empty($row) AND $row['num']>0)?true:false;
      return $value;  
    }

    /**
    link detail statistik question
    @param int $category_id
    @param int $question_id
    */
    public static function link_to_user_by_question($category_id,$question_id)
    {
      return base_url().'admin/polling/statistik_user/'.$category_id.'/'.$question_id;
    }


    /**
    link detail statistik answer
    @param int $category_id
    @param int $question_id
    */
    public static function link_to_user_by_answer($category_id,$question_id,$answer_id)
    {
      return base_url().'admin/polling/statistik_user/'.$category_id.'/'.$question_id.'/'.$answer_id;
    }

    /**
    menghitung statistik penjawab
    - hitung total penjawab pada pertanyaan tersebut
    */
    public static function calculate_statistik($question_id,$answer_id)
    {
      $lib=new polling_lib;
      //total penjawab pada pertanyaan
      $where='spsd_polling_question_id='.intval($question_id);
      $lib->mainTable='sys_polling_statistik_detail';
      $count_question=$lib->countAll($where);

      //total yang jawab pada jawaban tesebut
      $where='spsd_polling_answer_id='.intval($answer_id);
      $lib->mainTable='sys_polling_statistik_detail';
      $count_answer=$lib->countAll($where);

      $data=array();
      $data['count_question']=$count_question;
      $data['count_answer']=$count_answer;
      $data['percentage']=($count_answer>0 AND $count_question>0)?($count_answer/$count_question*100):0;

      if($data['percentage']<=20)
      {
        $data['color_style']=' progress-bar-danger';
      }
      else if($data['percentage']>20 AND $data['percentage']<=50)
      {
        $data['color_style']=' progress-bar-warning';
      }
      else if($data['percentage']>50 AND $data['percentage']<=75)
      {
        $data['color_style']=' progress-bar-info';
      }
      else
      {
        $data['color_style']=' progress-bar-success';
      }

      return $data;
    }

    /**
    dapatkan data jawaban berdasarkan pertanyaan
    */
    public static function get_answer_by_question_id($question_id,$addtional_where='')
    {
      $lib=new polling_lib;
      $where='polling_answer_polling_question_id='.intval($question_id);
      if(trim($addtional_where)!='')
      {
        $where.=$addtional_where;
      }
      $lib->mainTable='sys_polling_answer';
      $results=$lib->findAll($where,'no limit','no offset','polling_answer_order_number ASC');
      return $results;
    }

    /**
    dapatkan nama kategori polling
    @param int $category_id  
    */
    public function get_category_name($category_id)
    {
      $where='polling_category_id='.intval($category_id);
      $name=$this->CI->function_lib->get_one('polling_category_name','sys_polling_category',$where);
      return $name;
    }

    /**
    dapatkan data polling berdasarkan category
    @param int $category_id

    */
    public function get_polling_by_category_id($category_id,$addtional_where='')
    {
      $this->mainTable='sys_polling_question';
      $where='polling_question_polling_category_id='.intval($category_id);
      if(trim($addtional_where)!='')
      {
        $where.=$addtional_where;
      }
      $results=$this->findAll($where,'no limit','no offset','polling_question_order_number ASC');
      return $results;
    }

    /***
    simpan struktur organisasi
    @param int $id -> saat update
    */
    public function stored_database($category_id)
    {
        $data=array();
        $data['status']=500;
        $data['message']='Data belum disimpan.';
        $data['category_id']=$category_id;

       $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
       $polling_question_id_arr=$this->CI->input->post('polling_question_id',true);
       $polling_question_name_arr=$this->CI->input->post('polling_question_name',true);
       $polling_question_order_number_arr=$this->CI->input->post('polling_question_order_number',true);
       $polling_question_is_mandatory_arr=$this->CI->input->post('polling_question_is_mandatory',true);
       $polling_answer_id_arr=$this->CI->input->post('polling_answer_id',true);
       $polling_answer_name_arr=$this->CI->input->post('polling_answer_name',true);
       $polling_answer_order_number_arr=$this->CI->input->post('polling_answer_order_number',true);
       if(!empty($polling_question_name_arr))
       {
        $str_polling_question_id='';
        $no_question=1;
        foreach($polling_question_name_arr AS $index_question=>$value)
        {
          $polling_question_id=(isset($polling_question_id_arr[$index_question]) AND trim($polling_question_id_arr[$index_question])!='')?$polling_question_id_arr[$index_question]:0;
          $polling_question_name=(isset($polling_question_name_arr[$index_question]) AND trim($polling_question_name_arr[$index_question])!='')?$polling_question_name_arr[$index_question]:'-';
          $polling_question_order_number=(isset($polling_question_order_number_arr[$index_question]) AND trim($polling_question_order_number_arr[$index_question])!='')?$polling_question_order_number_arr[$index_question]:1;
          $polling_question_is_mandatory=(isset($polling_question_is_mandatory_arr[$index_question]) AND trim($polling_question_is_mandatory_arr[$index_question])!='')?$polling_question_is_mandatory_arr[$index_question]:'N';
        
          //save pertanyaan
          $column_question=array(
            'polling_question_polling_category_id'=>$category_id,
            'polling_question_name'=>$polling_question_name,
            'polling_question_order_number'=>$polling_question_order_number,
            'polling_question_is_mandatory'=>$polling_question_is_mandatory,
            'polling_question_input_by'=>$admin_username,
          );
          if($polling_question_id>0)
          {
            $this->CI->db->update('sys_polling_question',$column_question,array('polling_question_id'=>$polling_question_id));
          }
          else
          {
            $this->CI->db->insert('sys_polling_question',$column_question);
            $polling_question_id=$this->CI->function_lib->insert_id();
          }


          //simpan answer
          $str_polling_answer_id=0;
          if(!empty($polling_answer_name_arr[$index_question]))
          {
            $str_polling_answer_id='';
            $no_answer=1;
            foreach($polling_answer_name_arr[$index_question] AS $index_answer=>$value_answer)
            {
              $polling_answer_id=(isset($polling_answer_id_arr[$index_question][$index_answer]) AND trim($polling_answer_id_arr[$index_question][$index_answer])!='')?$polling_answer_id_arr[$index_question][$index_answer]:0;
              $polling_answer_name=(isset($polling_answer_name_arr[$index_question][$index_answer]) AND trim($polling_answer_name_arr[$index_question][$index_answer])!='')?$polling_answer_name_arr[$index_question][$index_answer]:'-';
              $polling_answer_order_number=(isset($polling_answer_order_number_arr[$index_question][$index_answer]) AND trim($polling_answer_order_number_arr[$index_question][$index_answer])!='')?$polling_answer_order_number_arr[$index_question][$index_answer]:1;

                //save jawaban
              $column_answer=array(
                'polling_answer_polling_question_id'=>$polling_question_id,
                'polling_answer_name'=>$polling_answer_name,
                'polling_answer_order_number'=>$polling_answer_order_number,

              );
              if($polling_answer_id>0)
              {
                $this->CI->db->update('sys_polling_answer',$column_answer,array('polling_answer_id'=>$polling_answer_id));
              }
              else
              {
                $this->CI->db->insert('sys_polling_answer',$column_answer);
                $polling_answer_id=$this->CI->function_lib->insert_id();
              }

              $str_polling_answer_id.=($no_answer>1)?', '.$polling_answer_id:$polling_answer_id;
              $no_answer++;
            }

            //hapus data yang tidak tersimpan
            $sql='DELETE FROM sys_polling_answer WHERE polling_answer_id NOT IN ('.$str_polling_answer_id.')
            AND polling_answer_polling_question_id='.intval($polling_question_id);

            $this->CI->db->query($sql);
          }

          $str_polling_question_id.=($no_question>1)?', '.$polling_question_id:$polling_question_id;
          $no_question++;
        }

        //hapus data yang tidak tersimpan
        $sql='DELETE FROM sys_polling_question WHERE polling_question_id NOT IN ('.$str_polling_question_id.')
        AND polling_question_polling_category_id='.intval($category_id);
        $this->CI->db->query($sql);

        $data['status']=200;
        $data['message']='OK';
        $data['category_id']=$category_id;

       } 
       return $data;
    }

      /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->dir;
        $pathLocation=FCPATH.$this->dir;

        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
    
    
    /**
     * handle upload
     */
    public function handleUpload($id,$filesName)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
      
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName);
            $status=$results['status'];
            $message=$results['message'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name)
    {
        $config=array();
        $pathImage=$this->dir;
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
             $where=array(
                'struktur_id'=>$id,
            );
            $column=array(
                'struktur_foto'=>$pathImage.$dataImage['file_name'],
            );
            $this->CI->db->update($this->mainTable,$column,$where);
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='category_id ASC')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='--';
          $mainConfig = array(
              
                array(
                        'field'   => 'full_name',
                        'label'   => 'Nama Lengkap',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'email_address',
                        'label'   => 'Email',
                        'rules'   => 'trim|required|valid_email'
                    ),
                array(
                        'field'   => 'phone',
                        'label'   => 'Telepon',
                        'rules'   => 'trim|required'
                ),
              
                
            );
         
          $config=$mainConfig;
          /**
           * jika lokasi admin, maka module controller dan method wajib diisi
           * main config dan additional config akan digabung
           */
        
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
          }
          else
          {
            $status=500;
            $message=validation_errors();
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
