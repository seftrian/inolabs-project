<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author anggoro
 */
class widget_testimony extends widget_lib{
    //put your code here
    public function run($module,$class_widget,$limit=9)
    {        
        $this->load->model('testimonial/testimonial_lib');
        $lib=new testimonial_lib;
        $data=array();
        $where='testimonial_is_published="Y"';
        $sql='SELECT * FROM site_testimonial
              WHERE '.$where.'
              ORDER BY testimonial_id DESC
              LIMIT '.$limit.'
              ';
        $exec=$this->db->query($sql);      
        $data['results']=$exec->result_array();
        $data['pathImgArr']=$lib->getImagePath();
        $themes=$lib->CI->config->item('theme');
        $themes_admin_url=base_url().'themes/front/'.$themes['frontend']['name'].'/inc/';
        $data['themes_inc']=$themes_admin_url;

        $data['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
        $data['base_url'] .= "://".$_SERVER['HTTP_HOST'];
        $data['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
        $this->render(get_class(),$data);
    }
}

?>
