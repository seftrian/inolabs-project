<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('testimonial_lib');
        $this->mainModel=new testimonial_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";


        $where=1;
        $params['where'] =$where;

        $params['order_by'] = "
            testimonial_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $logo_name= (trim($testimonial_avatar_src)=='')?'-': pathinfo($testimonial_avatar_src,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($testimonial_avatar_src,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>140,
            'height'=>95,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'14095/',
            'urlSave'=>$pathImgUrl.'14095/',
            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$testimonial_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$testimonial_id.'\');"><i class="clip-remove"></i></a>';
            $status_active = (isset($testimonial_is_published) && $testimonial_is_published=="Y")?'<span class="icon-ok"></span>':'<span class="clip-minus-circle"></span>';

            $entry = array('id' => $testimonial_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'name' =>$testimonial_full_name,                   
                    'title' =>$testimonial_title,                    
                    'testimony' => $testimonial_value,
                    'photo' => '<img src="'.$img.'" />',
                    'status' => $status_active,

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
