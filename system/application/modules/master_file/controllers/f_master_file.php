<?php

class f_master_file extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('master_file_lib');
        $this->load->helper(array('pagination'));

        $this->mainModel=new master_file_lib;
        $this->pathImgArr=$this->mainModel->getImagePath();
    }

     /**
    download file
    */
    public function download_file($id='')
    {
         if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $rowMenu=$this->mainModel->find('download_id='.  intval($id));
        if(empty($rowMenu))
        {
            show_error('Request anda tidak valid',500);
        }

        foreach($rowMenu AS $variable=>$value)
        {
            $$variable=$value;
        }
        $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
        $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
        $logo_file=$logo_name.'.'.$logo_ext;
        $pathImgArr=$this->pathImgArr;
        $pathImgLoc=$pathImgArr['pathLocation'];

        if(file_exists($pathImgLoc.$logo_file) AND trim($logo_file)!='')
        {
          
            master_file_lib::do_download($pathImgLoc.$logo_file);
        }
        else
        {
            show_error('File not found!',404);
        }

    }

    public function index()
    {

        $where=1;
        $this->mainModel->setMainTable('site_download');

        $limit=10;
        $total_rows=$this->mainModel->countAll($where);
        $paginationArr=create_pagination($this->currentModule.'/'.get_class().'/'.__FUNCTION__.'/', $total_rows, $limit );
        $this->data['offset']=$paginationArr['limit'][1];    
        $this->data['pathImgArr']=$this->mainModel->getImagePath();

        $this->data['results']=$this->mainModel->findAll($where,$limit,$this->data['offset'],$order_by='download_id DESC');
        $this->data['link_pagination']=$paginationArr['links'];
        $title="Download File";

        $desc=$title;
        $key=$title;
        $this->seo($title,$desc,$key);
        template($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }

    /**
     * 
     * @author Tejo Murti 
     * @date 2 September 2013
     * memaksimalkan seo dari content yang ada
     * @param string $title
     * @param string $desc
     * @param string $key
     */
    public function seo($title,$desc,$key)
    {
        $seoFunc=function_lib::make_seo($title, $desc, $key);
        $this->data['seoTitle']=$seoFunc['title'];
        $this->data['seoDesc']=$seoFunc['desc'];
        $this->data['seoKeywords']=$seoFunc['keywords'];
    }
}