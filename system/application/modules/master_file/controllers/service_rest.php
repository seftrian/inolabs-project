<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('master_file_lib');
        $this->mainModel=new master_file_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";

        $download_title=$this->input->get('download_title',true);
        $download_title=str_replace('%20', ' ', $download_title);
        $download_content=$this->input->get('download_content',true);
        $download_content=str_replace('%20', ' ', $download_content);

        $where=1;
      

        $where.=' AND download_title LIKE "%'.$download_title.'%" AND download_content LIKE "%'.$download_content.'%" ';
        $params['where'] =$where;

        $params['order_by'] = "
            download_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $logo_name= (trim($download_file)=='')?'-': pathinfo($download_file,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($download_file,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];

            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$download_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$download_id.'\');"><i class="clip-remove"></i></a>';
            $content=strip_tags(html_entity_decode($download_content));
            $content=(strlen($content)<150)?$content:substr($content,0,150);
            $btn_download='<button class="btn btn-warning btn-xs" onclick="window.location.href=\''. base_url().'admin/master_file/download_file/'.$download_id.'\';return false;"><span class="fa fa-white fa-cloud-download"></span> ('.substr(ucwords($logo_name),0,10).'.'.$logo_ext.')</button>';
                    
            $entry = array('id' => $download_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'title' =>$download_title,
                    'content' =>'<p style="float:right;">'.$btn_download.'</p>'.$content,
                    'meta_description' => $download_meta_description,
                    'meta_tags' => $download_meta_tags,
                    'input_by' => $download_input_by,

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
