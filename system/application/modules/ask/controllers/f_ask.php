<?php

class f_ask extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');        
    }

    public function index(){
        $this->data['facebook'] =$this->function_lib->get_one('social_media_link','site_social_media','social_media_type="facebook"');      
        $this->data['facebook_name'] =$this->function_lib->get_one('social_media_name','site_social_media','social_media_type="facebook"');      
        $this->data['twitter'] =$this->function_lib->get_one('social_media_link','site_social_media','social_media_type="twitter"');      
        $this->data['twitter_name'] =$this->function_lib->get_one('social_media_name','site_social_media','social_media_type="twitter"');      

        template($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }

    /**
     * 
     * @author Tejo Murti 
     * @date 2 September 2013
     * memaksimalkan seo dari content yang ada
     * @param string $title
     * @param string $desc
     * @param string $key
     */
    public function seo($title,$desc,$key,$img='')
    {
        $seoFunc=function_lib::make_seo($title, $desc, $key);
        $this->data['seoTitle']=$seoFunc['title'];
        $this->data['seoDesc']=$seoFunc['desc'];
        $this->data['seoKeywords']=$seoFunc['keywords'];
        $this->data['img_content']=$img;
        $this->data['current_url']=base_url().uri_string();
    }
    
    
}
