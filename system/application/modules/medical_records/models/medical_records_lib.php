<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */

//class admin_privilege extends core_menu_lib{
class medical_records_lib {
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='sys_poliklinik_price';
    public $primaryId='poliklinik_price_id';
    protected $imgPath='assets/images/services_parent/';
    protected $fieldNameImage='services_parent_logo';
    public function __construct() {
        
        //parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));
        $this->CI->load->models(array('item/item_lib','registration/registration_lib','trx_resep/trx_resep_lib'));
        if(!session_id())
        {
            session_start();
        }

     //   $this->getImagePath();
        $this->item_lib=new item_lib;
        $this->trx_resep_lib=new trx_resep_lib;

    }


    /**
    menampilkan semua data pasien
    */
    public function get_all_pasien($params)
    {
        $is_new=$this->CI->input->get('is_new',true);
        $rm_number_prefix=$this->CI->input->get('rm_number_prefix',true);
        $pasien_number=$this->CI->input->get('pasien_number',true);
        $pasien_number_old=$this->CI->input->get('pasien_number_old',true);
        $person_name=$this->CI->input->get('person_name',true);
        $person_address=$this->CI->input->get('person_address',true);
        $person_phone=$this->CI->input->get('person_phone',true);
        $where='
        registration_pasien_number LIKE "%'.$pasien_number.'%"
        AND
        person_full_name LIKE "%'.$person_name.'%"
        AND (
            person_telephone LIKE "%'.$person_phone.'%"
            OR person_phone LIKE "%'.$person_phone.'%"
            )
        AND person_was_deleted="N"
        ';
      
        if($is_new!='')
        {
          switch($is_new)
          {
              case 'new':
                $where.=' AND (registration_pasien_diagnosis_datetime IS NULL OR registration_pasien_action_datetime IS NULL)';
              break;
              case 'old':
                $where.=' AND (registration_pasien_diagnosis_datetime IS NOT NULL OR registration_pasien_action_datetime IS NOT NULL)';
              break;
          }
        }

        if($person_address!='')
        {
            $where.=' AND person_id IN (
                SELECT person_address_person_id 
                FROM sys_person_address
                WHERE person_address_value LIKE "%'.$person_address.'%")';
        }
        if(trim($rm_number_prefix)!='')
        {
            $where.=' AND rm_number_prefix="'.$rm_number_prefix.'"';
        }
        $params['table'] = 'trx_registration_pasien';
        $params['select'] = "
            trx_registration_pasien.*,
            sys_person.person_id, sys_person.person_full_name,
            person_telephone,person_phone, person_was_deleted,
            rm_number_name, rm_number_prefix
        ";
        $params['where']=$where;
        $params['join']='
        INNER JOIN  sys_person ON person_id=registration_pasien_person_id
        INNER JOIN master_rm_number ON rm_number_id =registration_pasien_rm_number_id
        ';
        $params['order_by'] = "
            registration_pasien_id DESC 
        ";
        $params['group_by_detail']='person_id';

        $query = $this->CI->function_lib->get_query_data($params);
        $total = $this->CI->function_lib->get_query_data($params, true);

        return array(
          'query'=>$query,
          'total'=>$total,
        );
    }

    /**
    simpan kategori rm 
    */
    public function save_category_rm()
    {

        $pasien_id=$this->CI->input->post('pasien_id');
        $current_rm_number_prefix=$this->CI->input->post('current_rm_number_prefix');
        $rm_number_prefix=$this->CI->input->post('rm_number_prefix');
        $rm_number_prefix_label=$this->CI->input->post('rm_number_prefix_label');

        $where='rm_number_name="'.$rm_number_prefix_label.'" AND rm_number_prefix="'.$rm_number_prefix.'"';
        $rm_number_id=$this->CI->function_lib->get_one('rm_number_id','master_rm_number',$where);  
        $number_length=$this->CI->function_lib->get_one('rm_number_length','master_rm_number',$where);    
        $pasien_number='';
        if($rm_number_prefix_label=='')
        {
            $status=500;
            $message='Kategori harus diisi';
        }
        else if(!$rm_number_id)
        {
            $status=500;
            $message='Kategori tidak ditemukan';
        }
        else if($current_rm_number_prefix==$rm_number_prefix)
        {
            $status=200;
            $message='Tidak terjadi perubahan data kategori. ';
        }
        else
        {
            //update data
            $new_rm_number=$this->CI->registration_lib->generate_nomor_rekam_medis($rm_number_id,$number_length);
            $column=array(
                'registration_pasien_number'=>$new_rm_number,
                'registration_pasien_rm_number_id'=>$rm_number_id,
            );
            $where=array(
                'registration_pasien_id'=>$pasien_id,
            );

            $this->CI->db->update('trx_registration_pasien',$column,$where);
            $status=200;
            $message='Sukses mengubah kategori, nomor rm saat ini <b>'.$new_rm_number.'</b>.';
            $pasien_number=$new_rm_number;
            
        }

        return array(
            'status'=>$status,
            'message'=>$message,
            'pasien_number'=>$pasien_number,
        );
    }

    /**
    dapatkan detail pasien berdasarkan id pasien
    @param int $pasien_id    
    */
    public function get_detail_pasien($pasien_id)
    {
        $this->setMainTable('trx_registration_pasien');
        $join=array(
            'INNER JOIN sys_person ON person_id=registration_pasien_person_id',
            'LEFT JOIN master_rm_number ON registration_pasien_rm_number_id=rm_number_id'
        );
        $select='person_id, registration_pasien_id, registration_pasien_rm_number_id, 
                registration_pasien_number, rm_number_prefix, rm_number_name, 
                person_full_name';
        $where='registration_pasien_id='.intval($pasien_id);
        $response=$this->findCustom($where,$join,$select);
        return $response;
    }

    /**
    dapatkan data history berdasarkan person_id
    @param int $pasien_id
    */
    public function get_history($pasien_id)
    {
      //
      $sql='SELECT * 
            FROM trx_registration_pasien_history
            WHERE registration_pasien_history_registration_pasien_id='.intval($pasien_id).'
            ORDER BY registration_pasien_history_id DESC';
      $exec=$this->CI->db->query($sql);
      return $exec->result_array();      
    }

    /**
    dapatkan data pemeriksaan dari history
    @param int $history_id
    */
    public function get_pasien_queue($history_id)
    {
      $sql='SELECT *
       FROM  trx_registration_pasien_queue
       WHERE pasien_queue_registration_pasien_history_id='.intval($history_id);
      $exec=$this->CI->db->query($sql);
      return $exec->result_array();
    }

      /**
    dapatkan data pemeriksaan dari history
    @param int $history_id
    */
    public function get_diagnosa($pasien_queue_id)
    {
      $sql='SELECT *
       FROM  trx_pasien_diagnosis
       INNER JOIN trx_pasien_checkup ON pasien_checkup_pasien_queue_id=diagnosis_pasien_queue_id
       WHERE diagnosis_pasien_queue_id='.intval($pasien_queue_id);
       $exec=$this->CI->db->query($sql);
       return $exec->result_array();
    }

    /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->imgPath;
        $pathLocation=FCPATH.$this->imgPath;
        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
  
    public function upload_more_images($id)
    {
        if(!empty($_FILES))
        {
            $no=1;
            $img_detail_id='';
            foreach($_FILES AS $filesName=>$rowArr)
            {
                $results=$this->handleUpload($id,$filesName,$no);
                $post_id=isset($_POST[$filesName.'_id'])?intval($_POST[$filesName.'_id']):0;
                $img_id=($results['id']>0)?$results['id']:$post_id;
                $img_detail_id.=($no>1)?', '.$img_id:$img_id;
                if($results['status']!=200)
                {
                   return array(
                        'status'=>500,
                        'message'=>'Gambar '.$_FILES[$filesName]['name'].' gagal diunggah. ',
                    );
                }
                $no++;
            }
            
        }
        return array(
            'status'=>200,
            'message'=>'success',
        );
    }
    
     /**
     * handle upload
     */
    public function handleUpload($id,$filesName,$no)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
        $id_new=0;
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName,$no);
            $status=$results['status'];
            $message=$results['message'];
            $id_new=$results['id'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$id_new,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name,$no)
    {
        $config=array();
        $pathImage=$this->imgPath;
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        $img_id=0;
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
            if($field_name==$this->fieldNameImage)
            {
                
                if(isset($_POST[$this->fieldNameImage.'_old']) AND trim($_POST[$this->fieldNameImage.'_old'])!='' AND file_exists(FCPATH.$_POST[$this->fieldNameImage.'_old']))
                {
                    unlink(FCPATH.$_POST[$this->fieldNameImage.'_old']);
                }
                
                $where=array(
                $this->primaryId=>$id,
                );
                $column=array(
                    $this->fieldNameImage=>$pathImage.$dataImage['file_name'],
                );
                $this->CI->db->update($this->mainTable,$column,$where);
            }

           
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;

        }
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$img_id,
        );
        
    }

 /**
     * untuk validasi form 
     * @param int $id default 0
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='';
         $config = array(
               array(
                     'field'   => 'psp_parent_id',
                     'label'   => 'Parent',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'psp_name',
                     'label'   => 'Nama',
                     'rules'   => 'trim|required|min_length[2]'
                  ),

            );
         
           $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }

    public function getMainTable()
    {
        return $this->mainTable;
    }

    /**
     * set main table
     */
    public function setMainTable($table)
    {
        $this->mainTable=$table;
    }
    
   
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }

    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @param string $select='*'
     * @param string $having=''
     * @param string $group_by=''
     * @param array $joinArr
     * @return array
     */
    public function findAllCustom($where=1,$limit=10,$offset=0,$orderBy='',$select='*',$having='',$group_by='',$joinArr=array())
    {
        $sql='SELECT '.$select.' FROM '.$this->mainTable.' ';
        if(!empty($joinArr))
        {
            foreach($joinArr AS $value)
            {
                $sql.=$value;
            }
        }

        if(trim($where)!='')
        {
            $sql.=' WHERE '.$where;
        }    

        if(trim($having)!='')
        {
            $sql.=' HAVING '.$having;
        }  
        
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(trim($group_by)!='')
        {
            $sql.=' GROUP BY '.$having;
        }  
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }

      /**
     * dapatkan satu baris data
     * @param string $where
     * @param array $join
     * @param string $select
     * @return array
     */
    public function findCustom($where=1,$join=array(),$select="*")
    {
        $sql='SELECT '.$select.' FROM '.$this->mainTable.'
              ';
        if(!empty($join)) 
        {
            foreach($join AS $value)
            {
              $sql.=' '.$value;
            }
        }     
        $sql.= ' WHERE '.$where;
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * proses penyimpanan data
     * @param int $id default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
