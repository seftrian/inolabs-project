<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    protected $mainModel;
    //put your code here
    public function __construct() {
        parent::__construct();
        //load model menu

        $this->load->model(array('medical_records_lib','poliklinik_billing/poliklinik_billing_lib',
            'trx_pos/trx_pos_lib_utility',));
        $this->load->helper(array('pagination'));
        
        $this->mainModel=new medical_records_lib;
        
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        $this->main_title='Data Pasien';
    }
    /**
    print resep
    */  
    public function print_trx($header_id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
       
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template

        $this->data['show_print_trx']=$this->trx_resep_lib->show_print_trx($header_id);
        $this->load->view_single($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    
    }

    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {

        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        
        $this->data['seoTitle']=$this->main_title;
        $description='Catatan Medis Pasien';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-book",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        //tampilkan jenis layanan 
        $where='kind_service_is_registration="Y" AND kind_service_is_active="Y"';
        $this->mainModel->setMainTable('master_kind_service');
        $this->data['kind_service_arr']=$this->mainModel->findAll($where,'no limit','no offset','kind_service_id DESC');
       
        $this->mainModel->setMainTable('master_rm_number');
        $where=1;
        $this->data['rm_number_arr']=array();
        $this->data['count']=$this->mainModel->countAll($where);
        if($this->data['count']<10)
        {
          $this->data['rm_number_arr']=$this->mainModel->findAll($where,'no limit','no offset','rm_number_prefix ASC');
        }

        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

    /**
    update kategori rm
    */
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function update($id=0)
    {

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        $pasien_id=$this->input->get('pasien_id',true);
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Ubah Kategori Pasien';
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        //data history pasien
        $get_detail_pasien=$this->mainModel->get_detail_pasien($id);
        foreach($get_detail_pasien AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }
        $this->load->view_single($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

     /**
     * untuk menambahkan data
     */
    public function manage_patient() {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        $pasien_id=$this->input->get('pasien_id',true);
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Kelola Pelayanan Poliklinik';
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        //data history pasien
        $this->data['history_arr']=$this->mainModel->get_history($pasien_id);       
        $this->data['mainModel']=$this->mainModel;       
        $this->load->view_single($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
     /**
     * untuk menambahkan data
     */
    public function modal_information() {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Informasi';
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        
        $this->load->view_single($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
      ?>
        <link rel="stylesheet" href="<?php echo base_url()?>assets/js/jquery-ui.css">
        <script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>   
       
          <!-- flexigrid starts here -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
         
        <!-- flexigrid ends here -->
         <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
        <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/datepicker/css/datepicker.css">
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/autoNumeric.js"></script>
        <script type="text/javascript">
         function currency_rupiah()
        {
            var myOptions = {aSep: '.', aDec: ','};
            $('.currency_rupiah').autoNumeric('init', myOptions); 
        }


        $(function(){
          UIModals.init();
        });

        function datetimepicker()
        {
             $('.date-picker').datepicker({
                autoclose: true,
             });
             $('.time-picker').timepicker();
        }

          <!-- script modal-->
            var UIModals = function () {
                //function to initiate bootstrap extended modals
                var initModals = function () {
                    $.fn.modalmanager.defaults.resize = true;
                    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
                        '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                        '<div class="progress progress-striped active">' +
                        '<div class="progress-bar" style="width: 100%;"></div>' +
                        '</div>' +
                        '</div>';

                    
                };
                return {
                    init: function () {
                        initModals();

                    }
                };
            }();

            //fungsi load modal
            function load_modal(method, pasien_id, title, content_information)
            {

                var $modal = $('#ajax-modal');
                var url_get='';
                if(typeof(pasien_id)!='undefined' && pasien_id>0)
                {
                  url_get='?pasien_id='+pasien_id;
                }
                  // create the backdrop and wait for next modal to be triggered
                  $('body').modalmanager('loading');
                  setTimeout(function () {
                      $modal.load('<?php echo base_url('admin/'.$this->currentModule.'/')?>/'+method+url_get, '', function () {
                          $modal.modal();
                         
                          $(".container_modal_information").find('h4.modal-title').html(title);
                          $(".container_modal_information").find("div#content_information").html('<h3>'+content_information+'</h3>');
                          $("#close").focus();
                          datetimepicker();
                          currency_rupiah();
                      });
                  }, 100);
            }

             //fungsi load modal
            function load_modal_information(method, title, content_information)
            {
                var $modal = $('#ajax-modal-information');
                  // create the backdrop and wait for next modal to be triggered
                  $('body').modalmanager('loading');
                  setTimeout(function () {
                      $modal.load('<?php echo base_url('admin/'.$this->currentModule.'/')?>/'+method, '', function () {
                          $modal.modal();
                          $(".container_modal_information").find('#title_information').html(title);
                          $(".container_modal_information").find("#content_information").html('<h3>'+content_information+'</h3>');
                          $("#close").focus();

                      });
                  }, 100);
            }

            function load_modal_update(id)
            {
               var $modal = $('#ajax-modal-information');
                  // create the backdrop and wait for next modal to be triggered
                  $('body').modalmanager('loading');
                  setTimeout(function () {
                      $modal.load('<?php echo base_url('admin/'.$this->currentModule.'/')?>/update/'+id, '', function () {
                          $modal.modal();
                          $(".container_modal_information").find('#title_information').html(title);
                          $(".prefix_label").focus();
                      });
                  }, 100);
            }

             /**
            fungsi untuk mendapatkan kategori nomor RM/ Pasien
            */
            function get_category_rm(modal_active)
            {
                var cacheItem = {};
                var modal_active_value=(typeof(modal_active)!='undefined' && modal_active==1)?1:0;
                $(".rm_number_prefix_label").autocomplete({
                   
                minLength: 1,
                appendTo:(modal_active_value==1)?'#ajax-modal-information':'',
                source: function( request, response ) {
                var term = request.term;
                if ( term in cacheItem ) {
                response( cacheItem[ term ] );
                return;
                }
                $.getJSON( "<?php echo base_url().'registration/service_rest/get_category_rm/';?>"+term, request, function( data, status, xhr ) {
                cacheItem[ term ] = data;
                response( data );
                });
                },
                select: function (event, ui) {
                    return false;
                },
                focus: function (event, ui) {
                $("input[name='rm_number_prefix']").val(ui.item.id);    
                $('.rm_number_prefix_label').val(ui.item.value);

                return false;
                },
                }).on("focus", function () {
                    $(this).autocomplete("search", '');
                });

           }

           function save_category_rm(form_id)
           {
              if(confirm('Lanjutkan?')){
                  var jqxhr=$.ajax({
                      url:'<?php echo base_url().$this->currentModule.'/service_rest/save_category_rm'?>',
                      type:'post',
                      dataType:'json',
                      data:$('form#'+form_id).serializeArray(),
                  });
                  jqxhr.success(function(response){
                      if(response['status']!=200)
                      {
                        alert(response['message']);
                        return false;
                      }
                      else
                      {
                         $("#pasien_number").val(response['pasien_number']);
                         $(".container_modal_information").find("#content_information").html('<p>'+response['message']+'</p>');
                          grid_reload();
                      }
                      return false;
                  });
                 jqxhr.error(function(){
                  alert('an error has occurred, please try again.');
                 });
              }

              return false;

           }
        </script>
        
       <?php
        switch($method)
        {
             case 'index':
            ?>
       
             <script type="text/javascript">

                      
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Detil', name: 'detail', width: 50, sortable: false, align: 'center' },
                            { display: 'Edit', name: 'edit', width: 50, sortable: false, align: 'center' },
                            { display: 'No. RM', name: 'pasien_number', width: 150, sortable: false, align: 'center' },
                         //   { display: 'No. RM Lama', name: 'pasien_number_old', width: 150, sortable: false, align: 'center' },
                          //  { display: 'No. RM Lama', name: 'pasien_number_old', width: 150, sortable: true, align: 'center' },
                            { display: 'Kategori', name: 'pasien_category', width: 150, sortable: false, align: 'left' },
                            { display: 'Nama Pasien', name: 'person_name', width: 260, sortable: false, align: 'left' },
                            { display: 'Telp/Hp', name: 'person_telephone', width: 260, sortable: false, align: 'center' },
                            { display: 'Alamat', name: 'person_address', width: 260, sortable: false, align: 'center' },
                            { display: 'Tgl. Daftar', name: 'person_registration', width: 120, sortable: false, align: 'center' },
                        ],
                        buttons: [
                        ],
                        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],
                     
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false,
                        nowrap:false,
                    });
                    


                    $(document).ready(function() {
                        grid_reload();
                         $( "#from" ).datepicker({
                          defaultDate: "+1w",
                          changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat:'dd-mm-yy',
                          onClose: function( selectedDate ) {
                            $( "#to" ).datepicker( "option", "minDate", selectedDate );
                          }
                        });
                        $( "#to" ).datepicker({
                          defaultDate: "+1w",
                          changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat:'dd-mm-yy',
                          onClose: function( selectedDate ) {
                            $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                          }
                        });
                    });

                    function grid_reload() {
                        var is_new=$("#is_new").val();
                        var rm_number_prefix=$("#rm_number_prefix").val();
                        var pasien_number=$("#pasien_number").val();
                        var pasien_number_old=$("#pasien_number_old").val();
                       // var pasien_number_old=$("#pasien_number_old").val();
                        var person_name=$("#person_name").val();
                        var person_address=$("#person_address").val();
                        var person_phone=$("#person_phone").val();
                        var link_service='?rm_number_prefix='+rm_number_prefix+'&pasien_number='+pasien_number+'&pasien_number_old='+pasien_number_old+'&person_name='+person_name+'&person_address='+person_address
                        +'&person_phone='+person_phone
                        +'&is_new='+is_new
                        ;
                        $("#gridview").flexOptions({url:'<?php echo base_url().$this->currentModule; ?>/service_rest/get_data'+link_service}).flexReload();
                    }


                    
                </script>
            <?php
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
