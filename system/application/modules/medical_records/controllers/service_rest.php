<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('medical_records_lib');
        $this->mainModel=new medical_records_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    public function save_category_rm()
    {
        $response=$this->mainModel->save_category_rm();
        echo json_encode($response);
    }

     /**
    proses penyimpanan resep
    */
    public function load_trx($header_id=0)
    {
        $response=$this->mainModel->load_trx($header_id);
        echo json_encode($response);
    }

     /**
    proses penyimpanan resep
    */
    public function save_resep()
    {
        $response=$this->mainModel->save_resep();
        echo json_encode($response);
    }
    /**
    dapatkan status diagnosis
    @param int pasien_queue_id
    @param int class_pain_id
    */
    public function get_status_diagnosis($pasien_queue_id,$class_pain_id)
    {
        $response=$this->mainModel->get_status_diagnosis($pasien_queue_id,$class_pain_id);
        echo json_encode($response);
    }

    /**
    proses penyimpanan
    */
    public function prepare_trx_to_be_stored()
    {
        $response=$this->mainModel->prepare_trx_to_be_stored();
        echo json_encode($response);
    }
    /**
    ambil data bhp yang ada di session
    */
    public function load_trx_bhp_by_session()
    {
        $response=$this->mainModel->load_trx_bhp_by_session();
        echo json_encode($response);
    }
     /**
    memastikan session telah tersimpan
    */
     /**
    penyimpanan trx_resep 
    */
    public function confirm_session($initsession='')
    {
        if(trim($initsession)!='')
        {
            $_SESSION[$initsession]=$_POST;
        }
    }


    function get_data() {
        $params = isset($_POST) ? $_POST : array();
        
        $query_lib=$this->mainModel->get_all_pasien($params);
        $query = $query_lib['query'];
        $total = $query_lib['total'];

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
            $no++;
            $person_address=function_lib::get_person_address($person_id);
            $rm_number_category=($rm_number_prefix!='')?$rm_number_name.'<br />'.$rm_number_prefix:'';
            $rm_number_category_link=' <span style="float:right;"><a title="Ubah kategori pasien" href="#" onclick="load_modal_update('.$registration_pasien_id.');return false;" class="btn btn-xs btn-info"><i class="fa fa-edit fa fa-white"></i></a></span>';
            $rm_number_category=$rm_number_category;
           // $history_datetime=$this->function_lib->get_one('registration_pasien_history_datetime','trx_registration_pasien_history','registration_pasien_history_id='.intval($registration_pasien_id));         
            $btn_action='<a href="#" title="Histori Medis" class="btn btn-xs btn-green" onclick="load_modal(\'manage_patient\','.$registration_pasien_id.', \'\',\'\')"><i class="clip-file-2 fa fa-white"></i></a>';
            $btn_edit='<a title="Edit Profil" class="btn btn-xs btn-green" href="'.base_url().'admin/people/update/'.$person_id.'"><i class=" fa fa-edit fa fa-white"></i></a>';
            $last_diagnosis=($registration_pasien_diagnosis_datetime!='')?convert_datetime($registration_pasien_diagnosis_datetime):'-';
            $last_action=($registration_pasien_action_datetime!='')?convert_datetime($registration_pasien_action_datetime):'-';
            $telp=($person_telephone!='')?$person_telephone:'-';
            $hp=($person_phone!='')?$person_phone:'-';
            $entry = array('id' => $registration_pasien_id,
                'cell' => array(
                    'no' =>  $no,
                    'detail' =>  $btn_action,
                    'edit' =>  $btn_edit,
                    'pasien_number' => $registration_pasien_number,
                    'pasien_number_old' => $registration_pasien_number_old,
                    'pasien_category'=>$rm_number_category,
                    'person_name' => $person_full_name.'
                    <p class="text-muted">
                    Diagnosis Terakhir: '.$last_diagnosis.'
                    <br />Tindakan Terakhir: '.$last_action.'
                    </p>
                    ',
                    'person_telephone' => $telp.' / '.$hp,
                    'person_address' => $person_address,
                    'person_registration' => convert_datetime($registration_pasien_timestamp,'','','ina'),
                  
                ),
            );
            $json_data['rows'][] = $entry;
        }
        echo json_encode($json_data);
    }

    /**
    ambil data pasien yang akan digunakan pada saat registrasi
    */
     function load_data_pasien() {
        $params = isset($_POST) ? $_POST : array();    
        $registration_pasien_number=$this->input->get('registration_pasien_number', true);
        $registration_pasien_number_old=$this->input->get('registration_pasien_number_old', true);
        $rm_number_category=$this->input->get('rm_number_category', true);
        $person_full_name=$this->input->get('person_full_name', true);
        $telp_hp=$this->input->get('telp_hp', true);
        //$person_address=$this->input->get('person_address', true);

        $where='1 ';
        /* registration_pasien_number */
        if(isset($registration_pasien_number)&&trim($registration_pasien_number)!='')
        {
            $where.='AND registration_pasien_number LIKE "'.$registration_pasien_number.'"';
        }
        if(isset($registration_pasien_number_old)&&trim($registration_pasien_number_old)!='')
        {
            $where.='AND registration_pasien_number_old LIKE "'.$registration_pasien_number_old.'"';
        }
        /* rm_number_name */
        if(isset($rm_number_category)&&trim($rm_number_category)!='')
        {
            $where.='AND rm_number_name LIKE "%'.$rm_number_category.'%"';
        }
        /* person_full_name */
        if(isset($person_full_name)&&trim($person_full_name)!='')
        {
            $where.='AND person_full_name LIKE "%'.$person_full_name.'%"';
        }
        /* telp/hp */
        if(isset($telp_hp)&&trim($telp_hp)!='')
        {
            $where.='AND (person_telephone LIKE "%'.$telp_hp.'%" OR person_phone LIKE "%'.$telp_hp.'%")';
        }

        /* address */
        /*if(isset($person_address)&&trim($person_address)!='')
        {
            $where.='AND registration_pasien_number LIKE "%'.$registration_pasien_number.'%"';
        }*/

        
        $params['table'] = 'trx_registration_pasien';
        $params['select'] = "
            trx_registration_pasien.*,
            sys_person.person_id, sys_person.person_full_name,
            person_telephone,person_phone, person_was_deleted,
            rm_number_name, rm_number_prefix
        ";
        $params['where']=$where;
        $params['join']='
        INNER JOIN  sys_person ON person_id=registration_pasien_person_id
        INNER JOIN master_rm_number ON rm_number_id =registration_pasien_rm_number_id
        ';
        $params['order_by'] = "
            person_id DESC 
        ";
        $params['group_by_detail']='person_id';

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
            $no++;
            $person_address=function_lib::get_person_address($person_id);
            $rm_number_category=($rm_number_prefix!='')?$rm_number_name.'<br />'.$rm_number_prefix:'';
           // $history_datetime=$this->function_lib->get_one('registration_pasien_history_datetime','trx_registration_pasien_history','registration_pasien_history_id='.intval($registration_pasien_id));         
            $btn_load='<a href="#" title="Histori Medis" class="btn btn-xs btn-green" onclick="triger_data_pasien_from_rm_number(\'form_account\','.$registration_pasien_person_id.',\''.$registration_pasien_number.'\')"><i class="clip-checkmark-circle-2"></i></a>';
           
            $telp=($person_telephone!='')?$person_telephone:'-';
            $hp=($person_phone!='')?$person_phone:'-';
            $entry = array('id' => $registration_pasien_id,
                'cell' => array(
                    'no' =>  $no,
                    'load' =>  $btn_load,
                    'pasien_number' => $registration_pasien_number,
                    'pasien_number_old' => ($registration_pasien_number_old!='')?$registration_pasien_number_old:'~',
                    'pasien_category'=>$rm_number_category,
                    'person_name' => $person_full_name,
                    'person_telephone' => $telp.' / '.$hp,
                    'person_address' => $person_address,
                  
                ),
            );
            $json_data['rows'][] = $entry;
        }
        echo json_encode($json_data);
    }
}