<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    protected $mainModel;
    //put your code here
    public function __construct() {
        parent::__construct();
        //load model menu

        $this->load->model(array('people_lib'));
   
        
        $this->mainModel=new people_lib;
        
        parse_str($_SERVER['QUERY_STRING'], $_GET);

        $this->person_title_arr=array('Tn','Ny','Nn','Sdr','An');
        $this->person_type=1; //people
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index_person()
    {

        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        
        
        $this->load->view_single($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {

        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        

        
        $this->data['seoTitle']='Data People';
        $description='Dokter, Pelanggan, Pasien, Karyawan, dll.';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-book",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
          //dapatkan data tenaga medis
        $this->data['medic_arr']=$this->mainModel->get_person_type($is_nakes='Y');
        $this->data['person_type_arr']=$this->mainModel->get_person_type($is_nakes='N');
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function add()
    {

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if($this->input->post('save'))
        {

            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                //jika ada person id, maka otomatis hanya update status
                $person_id=$this->input->post('person_id');
                if(intval($person_id)>0)
                {
                    $this->mainModel->update_person_type_arr($person_id);
                    $this->update($person_id);
                    die();
                }

                $columns=array(
                    'person_person_type_id'=>0,
                    'person_title'=>$this->input->post('person_title',true),
                    'person_full_name'=>$this->input->post('person_full_name',true),
                    'person_gender'=>$this->input->post('person_gender',true),
                    'person_blood_type'=>$this->input->post('person_blood_type',true),
                    'person_birth_date'=>$this->input->post('person_birth_date',true),
                    'person_birth_place'=>$this->input->post('person_birth_place',true),
                    'person_telephone'=>$this->input->post('person_telephone',true),
                    'person_phone'=>$this->input->post('person_phone',true),
                    'person_is_employee'=>($this->input->post('person_is_employee')=='Y')?'Y':'N',
                    
                    'person_admin_username'=>$this->adminUsername,
                    'person_is_active'=>$this->input->post('person_is_active',true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $id=$this->function_lib->insert_id();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    
                    $this->mainModel->update_person_type_arr($id);
                    //statrt audit trail
                    $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                    $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                    $description=$admin_username.' menambahkan people '.$name.' pada tgl '.date('Y-m-d H:i:s');
                    $table=$this->mainModel->getMainTable();
                    $column_primary=$this->mainModel->primaryId;
                    $primary_value=$id;
                    function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                    //end audit trail

                    //save some  detail address
                    $this->mainModel->setMainTable('sys_person_address');
                     $columns=array(
                    'person_address_person_id'=>$id,
                    'person_address_value'=>$this->input->post('person_address_value',true),
                    'person_address_province_id'=>($this->input->post('person_address_province_id',true)!='')?$this->input->post('person_address_province_id',true):0,
                    'person_address_city_id'=>($this->input->post('person_address_city_id',true)!='')?$this->input->post('person_address_city_id',true):0,
                    'person_address_district_id'=>($this->input->post('person_address_district_id',true)!='')?$this->input->post('person_address_district_id',true):0,
                    );
                    $this->mainModel->setPostData($columns);
                    $this->mainModel->save();


                    //save some  detail additional info
                    $this->mainModel->setMainTable('sys_person_additional_info');
                     $columns=array(

                    'additional_info_person_id'=>$id,
                    'additional_info_email_address'=>$this->input->post('additional_info_email_address',true),
                    'additional_info_facebook'=>$this->input->post('additional_info_facebook',true),
                    'additional_info_twitter'=>$this->input->post('additional_info_twitter',true),
                    'additional_info_linkedin'=>$this->input->post('additional_info_linkedin',true),
                    'additional_info_skype'=>$this->input->post('additional_info_skype',true),
                    );
                    $this->mainModel->setPostData($columns);
                    $this->mainModel->save();


                    //save some  detail additional info
                    $this->mainModel->setMainTable('sys_person');
                     //handle logo
                    $logo=$this->mainModel->upload_more_images($id);
                    
                    if($logo['status']!=200)
                    {
                        $status=$logo['status'];
                        $message=$logo['message'];
                        $msg=base64_encode('Terjadi error saat upload file. '.$message);
                        redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.$msg);
                    }
                    else
                    {
                        $msg=base64_encode('Data berhasil disimpan');
                        redirect(base_url().'admin/'.$this->currentModule.'/index?status=200&msg='.$msg);
                    }
                
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
         
        $this->data['seoTitle']='Tambah Data People';
        $description='Dokter, Pelanggan, Pasien, Karyawan, dll.';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Data People',
                                    'class'=>"clip-book",
                                    'link'=>base_url().'admin/'.$this->currentModule.'/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
      
        $person_address_province_id=isset($_POST['person_address_province_id'])?$_POST['person_address_province_id']:null;
        $person_address_city_id=isset($_POST['person_address_city_id'])?$_POST['person_address_city_id']:null;
        $person_address_district_id=isset($_POST['person_address_district_id'])?$_POST['person_address_district_id']:null;

        $this->data['footerScript']=$this->footerScript(__FUNCTION__,array(
            'person_address_province_id'=>$person_address_province_id,
            'person_address_city_id'=>$person_address_city_id,
            'person_address_district_id'=>$person_address_district_id,
             'insurance_arr'=>json_encode(array()),
        ));

        $this->data['person_title_arr']=$this->person_title_arr;


        //dapatkan data tenaga medis
        $this->data['medic_arr']=$this->mainModel->get_person_type($is_nakes='Y');
        $this->data['person_type_arr']=$this->mainModel->get_person_type($is_nakes='N');
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data group via form
     * @param int $id
     */
    public function update($id=0)
    {
        
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if($this->input->post('save'))
        {
            
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($id);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {

                $columns=array(
                    'person_title'=>$this->input->post('person_title',true),
                    'person_full_name'=>$this->input->post('person_full_name',true),
                    'person_gender'=>$this->input->post('person_gender',true),
                    'person_blood_type'=>$this->input->post('person_blood_type',true),
                    'person_birth_date'=>$this->input->post('person_birth_date',true),
                    'person_birth_place'=>$this->input->post('person_birth_place',true),
                    'person_telephone'=>$this->input->post('person_telephone',true),
                    'person_phone'=>$this->input->post('person_phone',true),
                    'person_is_employee'=>($this->input->post('person_is_employee')=='Y')?'Y':'N',
                    'person_admin_username'=>$this->adminUsername,
                    'person_is_active'=>$this->input->post('person_is_active',true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save($where='person_id='.intval($id));
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    
                    $this->mainModel->update_person_type_arr($id);
                    //statrt audit trail
                    $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                    $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                    $description=$admin_username.' mengubah people '.$name.' pada tgl '.date('Y-m-d H:i:s');
                    $table=$this->mainModel->getMainTable();
                    $column_primary=$this->mainModel->primaryId;
                    $primary_value=$id;
                    function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                    //end audit trail

                    $where='person_address_province_id='.intval($this->input->post('person_address_province_id',true)).'
                    AND person_address_city_id='.intval($this->input->post('person_address_city_id')).'
                    AND person_address_district_id='.intval($this->input->post('person_address_district_id')).'
                    AND person_address_value="'.$this->input->post('person_address_value').'"
                    AND person_address_is_active="Y" AND person_address_was_deleted="N"';

                    $is_changed_address=$this->function_lib->get_one('COUNT(person_address_id)','sys_person_address','person_address_person_id='.intval($id).'
                        AND '.$where);

                    //jika datanya tidak tersedia berarti ada perubahan
                    if(!$is_changed_address)
                    {
                        $sql='UPDATE sys_person_address SET person_address_is_active="N"
                        WHERE person_address_person_id='.intval($id);
                        $this->db->query($sql);

                        //save some  detail address
                        $this->mainModel->setMainTable('sys_person_address');
                         $columns=array(
                        'person_address_person_id'=>$id,
                        'person_address_value'=>$this->input->post('person_address_value',true),
                        'person_address_province_id'=>($this->input->post('person_address_province_id',true)!='')?$this->input->post('person_address_province_id',true):0,
                        'person_address_city_id'=>($this->input->post('person_address_city_id',true)!='')?$this->input->post('person_address_city_id',true):0,
                        'person_address_district_id'=>($this->input->post('person_address_district_id',true)!='')?$this->input->post('person_address_district_id',true):0,
                       );
                        $this->mainModel->setPostData($columns);
                        $this->mainModel->save();
                    }
                  

                    $where='additional_info_email_address="'.$this->input->post('additional_info_email_address').'"
                    AND additional_info_facebook="'.$this->input->post('additional_info_facebook').'"
                    AND additional_info_twitter="'.$this->input->post('additional_info_twitter').'"
                    AND additional_info_linkedin="'.$this->input->post('additional_info_linkedin').'"
                    AND additional_info_skype="'.$this->input->post('additional_info_skype').'"
                    AND additional_info_is_active="Y" AND additional_info_was_deleted="N"';

                    $is_changed_ai=$this->function_lib->get_one('COUNT(additional_info_person_id)','sys_person_additional_info','additional_info_person_id='.intval($id).'
                        AND '.$where);
                     //jika datanya tidak tersedia berarti ada perubahan
                    if(!$is_changed_ai)
                    {
                        $sql='UPDATE sys_person_additional_info SET additional_info_is_active="N"
                        WHERE  additional_info_person_id='.intval($id);
                        $this->db->query($sql);

                         //save some  detail additional info
                        $this->mainModel->setMainTable('sys_person_additional_info');
                         $columns=array(

                        'additional_info_person_id'=>$id,
                        'additional_info_email_address'=>$this->input->post('additional_info_email_address',true),
                        'additional_info_facebook'=>$this->input->post('additional_info_facebook',true),
                        'additional_info_twitter'=>$this->input->post('additional_info_twitter',true),
                        'additional_info_linkedin'=>$this->input->post('additional_info_linkedin',true),
                        'additional_info_skype'=>$this->input->post('additional_info_skype',true),
                        );
                        $this->mainModel->setPostData($columns);
                        $this->mainModel->save();
                    }


                    //save some  detail additional info
                    $this->mainModel->setMainTable('sys_person');
                     //handle logo
                    $logo=$this->mainModel->upload_more_images($id);
                    
                    if($logo['status']!=200)
                    {
                        $status=$logo['status'];
                        $message=$logo['message'];
                      
                        $msg=base64_encode('Terjadi error saat upload file. '.$message);
                        redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.$msg);
                    }
                    else
                    {
                        $msg=base64_encode('Data berhasil disimpan');
                        redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=200&msg='.$msg);
                    }
                
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $join=array();
        $dataArr=$this->mainModel->findCustom("person_id=".  intval($id)."  AND person_was_deleted='N'",$join);
        $address_arr=$this->mainModel->get_last_address($id);
        $this->data['person_address_province_id']=isset($address_arr['person_address_province_id'])?$address_arr['person_address_province_id']:0;
        $this->data['person_address_city_id']=isset($address_arr['person_address_city_id'])?$address_arr['person_address_city_id']:0;
        $this->data['person_address_district_id']=isset($address_arr['person_address_district_id'])?$address_arr['person_address_district_id']:0;
        $this->data['person_address_value']=isset($address_arr['person_address_value'])?$address_arr['person_address_value']:0;

        $this->data['additional_info_arr']=$this->mainModel->get_last_additional_info($id);
        
        //cek ketersediaan group admin
        if(empty($dataArr))
        {
            show_404();
        }
        foreach($dataArr AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }
        $photo=$dataArr['person_photo'];
        $getImagePath=$this->mainModel->getImagePath();
        $logo_name= (trim($photo)=='')?'-': pathinfo($photo,PATHINFO_FILENAME);
        $logo_ext=  pathinfo($photo,PATHINFO_EXTENSION);
        $logo_file=$logo_name.'.'.$logo_ext;
        $pathImgUrl=$getImagePath['pathUrl'];
        $pathImgLoc=$getImagePath['pathLocation'];
        $imgProperty=array(
        'width'=>150,
        'height'=>150,
        'imageOriginal'=>$logo_file,
        'directoryOriginal'=>$pathImgLoc,
        'directorySave'=>$pathImgLoc.'150150/',
        'urlSave'=>$pathImgUrl.'150150/',


        );
        $this->data['img']=$this->function_lib->resizeImageMoo($imgProperty);


        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Ubah Data People';
        $description='Dokter, Pelanggan, Pasien, Karyawan, dll.';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Data People',
                                    'class'=>"clip-book",
                                    'link'=>base_url().'admin/'.$this->currentModule.'/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['person_title_arr']=$this->person_title_arr;
        
        //dapatkan data asuransi
        $this->mainModel->setMainTable('sys_insurance');
        $where='insurance_is_active="Y" AND insurance_was_deleted="N"';
        $this->data['insurance_arr']=$this->mainModel->findAll($where,'no limit','no offset','insurance_id DESC'); 
      
           //cek data asuransi
        $where='person_insurance_person_id='.intval($id);
        $this->mainModel->setMainTable('sys_person_insurance');
        $this->data['person_insurance_arr']=$this->mainModel->findAll($where,'no limit','no offset','person_insurance_id ASC');
         
        $this->data['footerScript']=$this->footerScript(__FUNCTION__,array(
            'person_address_province_id'=>$this->data['person_address_province_id'],
            'person_address_city_id'=>$this->data['person_address_city_id'],
            'person_address_district_id'=>$this->data['person_address_district_id'],
            'insurance_arr'=>json_encode($this->data['insurance_arr']),
        )); 


        //dapatkan data tenaga medis
        $this->data['medic_arr']=$this->mainModel->get_person_type($is_nakes='Y');
        $this->data['person_type_arr']=$this->mainModel->get_person_type($is_nakes='N');  
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

    
    public function delete($id='') {
        $arr_output=array();
        $arr_output['message'] =  ' Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 200;
        $additional_where=' '; //default telah ngecek data dihapus
        if($this->available_data($id,$additional_where))
        {
            //statrt audit trail
            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
            $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
            $description=$admin_username.' menghapus people '.$name.' pada tgl '.date('Y-m-d H:i:s');
            $table=$this->mainModel->getMainTable();
            $column_primary=$this->mainModel->primaryId;
            $primary_value=$id;
            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
            //end audit trail                
            //delete detail
           /* $this->mainModel->setMainTable($this->mainModel->getMainTable());
            $column=array(                       
                    'person_was_deleted'=>'Y'
                    );
            $this->mainModel->setPostData($column);
            $this->mainModel->save('person_id='.intval($id));

            $sql='UPDATE sys_person_address SET person_address_was_deleted="Y"
            WHERE person_address_person_id='.intval($id);
            $this->db->query($sql);

            $sql='UPDATE sys_person_additional_info SET additional_info_was_deleted="Y"
                    WHERE  additional_info_person_id='.intval($id);
            $this->db->query($sql);
            */
            $sql='DELETE FROM sys_person_type_detail WHERE person_type_detail_person_id='.intval($id).'
                            AND person_type_detail_person_type_id='.intval($this->person_type);
            $this->db->query($sql);

            $arr_output['message'] =  ' Data berhasil dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

     public function act_publish() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('publish') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' AND person_is_active="N"';
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                            $description=$admin_username.' mengaktifkan people '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary=$this->mainModel->primaryId;
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $column=array(                       
                                    'person_is_active'=>'Y'
                                    );
                            $this->mainModel->setPostData($column);
                            $this->mainModel->save('person_id='.intval($id));

                            $deleted_count++;
                        }
                }
                $arr_output['message'] = $deleted_count . ' data berhasil diaktifkan.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

     public function act_unpublish() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('unpublish') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' AND person_is_active="Y"';
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                            $description=$admin_username.' menonaktifkan people '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary=$this->mainModel->primaryId;
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail

                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $column=array(                       
                                    'person_is_active'=>'N'
                                    );
                            $this->mainModel->setPostData($column);
                            $this->mainModel->save('person_id='.intval($id));

                            $deleted_count++;
                        }
                       
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dinonaktifkan.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

     public function act_delete() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' '; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {
                            //statrt audit trail
                            $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                            $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                            $description=$admin_username.' menghapus people '.$name.' pada tgl '.date('Y-m-d H:i:s');
                            $table=$this->mainModel->getMainTable();
                            $column_primary=$this->mainModel->primaryId;
                            $primary_value=$id;
                            function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                            //end audit trail
                           
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $column=array(                       
                                    'person_was_deleted'=>'Y'
                                    );
                            $this->mainModel->setPostData($column);
                            $this->mainModel->save('person_id='.intval($id));

                             $sql='UPDATE sys_person_address SET person_address_was_deleted="Y"
                             WHERE person_address_person_id='.intval($id);
                             $this->db->query($sql);

                             $sql='UPDATE sys_person_additional_info SET additional_info_was_deleted="Y"
                                    WHERE  additional_info_person_id='.intval($id);
                            $this->db->query($sql);
                           
                             /*
                               $sql='DELETE FROM sys_person_type_detail WHERE person_type_detail_person_id='.intval($id).'
                            AND person_type_detail_person_type_id='.intval($this->person_type);
                            $this->db->query($sql);
                             */

                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

    /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one($this->mainModel->primaryId,$this->mainModel->getMainTable(),
            $this->mainModel->primaryId.'='.intval($id).' AND person_was_deleted="N" '.$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }
    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        $person_address_province_id=isset($person_address_province_id)?$person_address_province_id:'';
        $person_address_city_id=isset($person_address_city_id)?$person_address_city_id:'';
        $person_address_district_id=isset($person_address_district_id)?$person_address_district_id:'';
        ?>
             
        <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

        <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/datepicker/css/datepicker.css">
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
                function get_province_by_country_id(country_id_val,elem,selected_id)
               {
                   // $("select[name='sp_detail_id']").bind('')
                     $.ajax({
                       url:'<?=base_url()?>area/service_rest/get_province_by_country_id',
                       type:'get',
                       dataType:'json',
                       data:{country_id:country_id_val},
                       success:function(response){
                          elem.html('<option value=""> Pilih Provinsi </option>');
                           $.each(response,function(i,rowArr){
                               selected=(selected_id==rowArr['area_id'])?'selected':'';

                               elem.append('<option value="'+rowArr['area_id']+'" '+selected+'>'+rowArr['area_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
               }
            function get_sub_area_by_area_id(area_id_val,elem_name,selected_id)
               {
                   // $("select[name='sp_detail_id']").bind('')
//                   console.log(area_id_val);
//                   console.log(elem_name);
                     $.ajax({
                       url:'<?=base_url()?>area/service_rest/get_sub_area_by_area_id',
                       type:'get',
                       dataType:'json',
                       data:{area_id:area_id_val},
                       success:function(response){
                           switch(elem_name)
                           {
                               case 'person_address_province_id':
                                   elem=$("select[name='person_address_city_id']");
                                   elem_label='Pilih Kota/Kabupaten';
                                   current_id=selected_id;
                               break;    
                                 
                               case 'person_address_city_id':
                                   elem=$("select[name='person_address_district_id']");
                                   elem_label='Pilih Kecamatan';
                                  current_id=selected_id;

                               break;    
                               
                           }
                           elem.html('<option value=""> '+elem_label+' </option>');
                           $.each(response,function(i,rowArr){
                               selected=(current_id==rowArr['area_id'])?'selected':'';
                               elem.append('<option value="'+rowArr['area_id']+'" '+selected+'>'+rowArr['area_name']+'</option>');
                           });
                       },
                       error:function(){
                           alert('an error has occurred, please try again');
                           return false;
                       }
                       });
        
               }

                     //hapus data        
                $("body").delegate("a.delete_shopping_cart",'click', function(e) {
                    //$(this).parent().parent().remove();
                    if(confirm('Hapus?')) {   
                        e.preventDefault();
                        var parent = $(this).parent().parent().parent();
                        parent.slideUp('medium',function() {
                            parent.remove();
                        });
                    }
                    return false;
                }); 


                var number_click=1;
                var insurance_arr=<?php echo $insurance_arr?>;
                var num_data=insurance_arr.length;
                function generate_html()
                {
                    var html_field='<div class="row field_'+number_click+'">'
                        +'<div class="col-md-6">'
                        +'<div class="form-group">'
                        +'<input type="hidden" name="person_insurance_id[]" value="0" /><select name="person_insurance_insurance_id[]" class="form-control">'
                        +'<option value="">Pilih Asuransi</option>'
                        +'</select>'
                        +'</div>'
                        +'</div>'
                        +'<div class="col-md-4">'
                        +'<div class="form-group">'
                        +'<input placeholder="no. asuransi" type="text" name="person_insurance_policy_number[]" class="form-control" />'
                        +'</div>'
                        +'</div>'
                        +' <div class="col-md-2">'
                        +'<div class="form-group">'
                        +'<a href="#" class="delete_shopping_cart btn btn-xs btn-danger">(-)</a>'
                        +'</div>'
                        +'</div>'
                        +'</div>';

                   //tambahkan list insurance_arr
                    $("div.insurance_field_init").after(html_field);
                   $.each(insurance_arr,function(index,rowArr){
                    $("div.field_"+number_click).find('select').append('<option value="'+rowArr['insurance_id']+'">'+rowArr['insurance_product']+'</option>');
                   });

                   number_click++;      

                        
                } 
               function save_person_insurance()
                {
                    if(confirm('Simpan?'))
                    {
                        $("input[type='submit']").val('Processing...');
                        $("input[type='submit']").attr('disabled',true);
                          var jqxhr=$.ajax({
                                url:'<?php echo base_url()?>insurance/service_rest/save_person_insurance',
                                data:$("form#form-insurance").serializeArray(),
                                dataType:'json',
                                type:'post',
                            });
                          jqxhr.success(function(response){
                            console.log(response);
                            if(response['status']==200)
                            {
                                window.location.href='<?php echo base_url()?>admin/<?php echo $this->currentModule?>/update/'+response['person_id']+'?status=200&msg='+response['message'];
                            }
                            else
                            {
                                alert(response['message']);
                                $("input[type='submit']").val('Simpan');
                                $("input[type='submit']").attr('disabled',false);
                            }
                          });
                          jqxhr.error(function(){
                            alert('Sorry, an error has occurred. Please try again.');
                            $("input[type='submit']").val('Simpan');
                            $("input[type='submit']").attr('disabled',false);
                          });
                    }
                    return false;
                  

                }
        </script>
        <?php
        switch($method)
        {
             case 'index':
            ?>
        <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'Actions', name: 'actions', width: 75, sortable: false, align: 'center' },
                    
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Nama Lengkap', name: 'person_full_name', width: 260, sortable: true, align: 'center' },
                            { display: 'Email', name: 'additional_info_email_address', width: 260, sortable: true, align: 'center' },
                            { display: 'Telephone', name: 'person_telephone', width: 260, sortable: true, align: 'center' },
                            { display: 'Phone', name: 'person_phone', width: 260, sortable: true, align: 'center' },
                            { display: 'Alamat', name: 'person_address_value', width: 260, sortable: true, align: 'center' },
                        
                            { display: 'Jenis Kelamin', name: 'person_gender', width: 100, sortable: true, align: 'center' },
                            { display: 'Golongan Darah', name: 'person_blood_type', width: 100, sortable: true, align: 'center' },
                            { display: 'Tempat / Tgl Lahir', name: 'person_birth_date', width: 260, sortable: true, align: 'center' },
                            { display: 'Foto', name: 'person_photo', width: 150, sortable: false, align: 'center' },
                            { display: 'Provinsi', name: 'person_address_province_id', width: 260, sortable: true, align: 'center' },
                            { display: 'Kota', name: 'person_address_city_id', width: 260, sortable: true, align: 'center' },
                            { display: 'Kecamatan', name: 'person_address_district_id', width: 260, sortable: true, align: 'center' },

                            { display: 'Status', name: 'person_is_active', width: 50, sortable: true, align: 'center' },
                            { display: 'Diinput Oleh', name: 'person_admin_username', width: 100, sortable: true, align: 'center' },
                            { display: 'Tanggal', name: 'person_timestamp', width: 130, sortable: false, align: 'center' },
                           
                        ],
                        buttons: [
                            { display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Aktifkan Item Terpilih', name: 'publish', bclass: 'publish', onpress: act_publish },
                            { separator: true },
                            { display: 'NonAktifkan Item Terpilih', name: 'unpublish', bclass: 'unpublish', onpress: act_unpublish },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                        ],
                        buttons_right: [
                        ],
                      
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });
                    
                    function act_delete(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    function act_publish(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_publish',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    function act_unpublish(com, grid) {
                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_unpublish',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        if(response['message'] != '') {
                                            alert(response['message']);
                                        }
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }
                    $(document).ready(function() {
                        grid_reload();
                    });

                   

                    function grid_reload() {
                        var person_full_name=$("#person_full_name").val();
                        var person_email=$("#person_email").val();
                        var person_phone=$("#person_phone").val();
                        var person_address=$("#person_address").val();
                        var person_type=$("#person_type").val();
                        var person_is_employee=$("#person_is_employee").val();

                        var url_service="?person_full_name="+person_full_name+"&person_email="+person_email
                        +"&person_phone="+person_phone+"&person_address="+person_address
                        +"&person_type="+person_type +"&person_is_employee="+person_is_employee;
                        $("#gridview").flexOptions({url:'<?php echo base_url().$this->currentModule; ?>/service_rest/get_data'+url_service}).flexReload();
                    }
                    
                    function delete_transaction(id)
                    {
                        if(confirm('Delete?'))
                        {
                             var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$this->currentModule?>/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            if(response['status']!=200)
                            {
                                alert(response['message']);
                            }
                            grid_reload();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            grid_reload();
                            return false;
                        });
                        }
                        return false;
                       
                    }

                    
                </script>
            <?php
            break;   
            case 'add'
            ?>
               <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
            <link href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
            <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
            <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>

            <script type="text/javascript">
               <!-- script modal-->
        var UIModals = function () {
            //function to initiate bootstrap extended modals
            var initModals = function () {
                $.fn.modalmanager.defaults.resize = true;
                $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
                    '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                    '<div class="progress progress-striped active">' +
                    '<div class="progress-bar" style="width: 100%;"></div>' +
                    '</div>' +
                    '</div>';
                
            };
            return {
                init: function () {
                    initModals();

                }
            };
        }();    

        function load_data_person(){

                var $modal = $('#ajax-modal');
                    // create the backdrop and wait for next modal to be triggered
                    $('body').modalmanager('loading');

                        
                    setTimeout(function () {
                        $modal.load('<?php echo base_url('admin/'.$this->currentModule.'/index_person')?>/', '', function () {
                            $modal.modal();
                        });
                    }, 100);
        }

                var coutry_id_default=1; //indonesia
                var elem_default=$("select[name='person_address_province_id']"); //indonesia
                $(function(){
                    get_province_by_country_id(coutry_id_default,elem_default);
                    get_province_by_country_id(coutry_id_default);
                    //get_sub_area_by_area_id(area_id_val,elem_name);
                    //people province
                    if($.trim('<?=$person_address_province_id?>')!='')
                    {
                        get_province_by_country_id(coutry_id_default,elem_default,'<?=$person_address_province_id?>');
                    }
                   
                    if($.trim('<?=$person_address_city_id?>')!='' || $.trim('<?=$person_address_province_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$person_address_province_id?>','person_address_province_id','<?=$person_address_city_id?>');
                    }
                   
                    if($.trim('<?=$person_address_district_id?>')!='' || $.trim('<?=$person_address_city_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$person_address_city_id?>','person_address_city_id','<?=$person_address_district_id?>');
                    }
                    
                    
                });
                
             $(function(){
             $('.date-picker').datepicker({autoclose:true
                });
           });
        </script>  
            <?php
            break;
            case 'update':
            ?>
            <script type="text/javascript">
                var coutry_id_default=1; //indonesia
                var elem_default=$("select[name='person_address_province_id']"); //indonesia
                $(function(){
                    get_province_by_country_id(coutry_id_default,elem_default);
                    get_province_by_country_id(coutry_id_default);
                    //get_sub_area_by_area_id(area_id_val,elem_name);
                    //people province
                    if($.trim('<?=$person_address_province_id?>')!='')
                    {
                        get_province_by_country_id(coutry_id_default,elem_default,'<?=$person_address_province_id?>');
                    }
                   
                    if($.trim('<?=$person_address_city_id?>')!='' || $.trim('<?=$person_address_province_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$person_address_province_id?>','person_address_province_id','<?=$person_address_city_id?>');
                    }
                   
                    if($.trim('<?=$person_address_district_id?>')!='' || $.trim('<?=$person_address_city_id?>')!='')
                    {
                        get_sub_area_by_area_id('<?=$person_address_city_id?>','person_address_city_id','<?=$person_address_district_id?>');
                    }
                    
                    
                });
           
           $(function(){
             $('.date-picker').datepicker({autoclose:true
                });
           });
        </script>  
            <?php
            break;
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
