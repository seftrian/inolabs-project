<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('people_lib');
        $this->mainModel=new people_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
        $this->person_type=1; //people
    }

    /**
    dapatkan data pasien
    */
    function get_data_pasien($type='pasien') {

        $where='person_type_name="'.$type.'"';
        $person_type_id=$this->function_lib->get_one('person_type_id','sys_person_type',$where);
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";
     
          $where='
        AND person_id IN (SELECT person_type_detail_person_id FROM sys_person_type_detail WHERE 
         person_type_detail_person_type_id='.intval($person_type_id).')
        ';
        $params['where'] = "
            person_was_deleted='N'
        ".$where;
      
        $params['order_by'] = "
            person_full_name ASC
        ";
        $params['group_by'] = "
            person_id 
        ";
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
        $no++;
         
            $status=($person_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',


            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);
            $person_address_arr=$this->mainModel->get_last_address($person_id);          
            $person_address_value=!empty($person_address_arr)?$person_address_arr['person_address_value']:'-';          
            $type='<br />'.$this->mainModel->list_person_type($person_id);
            $entry = array('id' => $person_id,
                'cell' => array(
                    'actions' =>  '<a class="btn btn-xs btn-green" href="#" onclick="load_data_person_by_id('.$person_id.'); return false;"><i class="clip-checkmark-circle-2"></i></a>',
                    'no' =>  $no,
                    'person_full_name' =>$person_title.', '.$person_full_name.$type,
                    'person_phone'=>$person_phone,
                    'person_address_value' =>$person_address_value,
                    'person_gender' =>$person_gender,
                    'person_birth_date' =>$person_birth_place.' / '.date("d-m-Y", strtotime($person_birth_date)),
                    'person_photo' =>'<img src="'.$img.'"></img>',
                    'person_timestamp' => date("d-m-Y H:i:s", strtotime($person_timestamp)),
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

    /**
    simpan data pasien melalui autcomplete trx resep
    */
     public function save_pasien()
    {
        $status=500;
        $message='Tidak ada data yang disimpan.';
        $id=0;
        $label='';
        if($this->input->post('save'))
        {
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                $columns=array(
                    'person_person_type_id'=>0,
                    'person_title'=>$this->input->post('person_title',true),
                    'person_full_name'=>$this->input->post('person_full_name',true),
                  /*  'person_gender'=>$this->input->post('person_gender',true),
                    'person_blood_type'=>$this->input->post('person_blood_type',true),
                    'person_birth_date'=>$this->input->post('person_birth_date',true),
                    'person_birth_place'=>$this->input->post('person_birth_place',true),
                    'person_telephone'=>$this->input->post('person_telephone',true),
                    */
                    'person_phone'=>$this->input->post('person_phone',true),
                    'person_admin_username'=>isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin',
                );

                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $id=$this->function_lib->insert_id();
                $label=$this->input->post('person_title',true).', '.$this->input->post('person_full_name',true);
               // $this->mainModel->update_person_type($id,$this->person_type);
                $this->mainModel->update_person_type_by_label($id,'Pasien');
                //statrt audit trail
                $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                $description=$admin_username.' menambahkan Pasien '.$name.' pada tgl '.date('Y-m-d H:i:s');
                $table=$this->mainModel->getMainTable();
                $column_primary=$this->mainModel->primaryId;
                $primary_value=$id;
                function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                //end audit trail

                    //save some  detail address
                $this->mainModel->setMainTable('sys_person_address');
                 $columns=array(
                'person_address_person_id'=>$id,
                'person_address_value'=>'-',
                'person_address_province_id'=>0,
                'person_address_city_id'=>0,
                'person_address_district_id'=>0,
                );
                $this->mainModel->setPostData($columns);
                $this->mainModel->save();


                //save some  detail additional info
                $this->mainModel->setMainTable('sys_person_additional_info');
                $columns=array(

                'additional_info_person_id'=>$id,
                'additional_info_email_address'=>'-',
                'additional_info_facebook'=>'-',
                'additional_info_twitter'=>'-',
                'additional_info_linkedin'=>'-',
                'additional_info_skype'=>'-',
                );
                $this->mainModel->setPostData($columns);
                $this->mainModel->save();
            }
        }

        echo json_encode(array(
            'status'=>$status,
            'message'=>strip_tags($message),
            'id'=>$id,
            'label'=>$label,
            ));
    }

    function get_data() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();

        $person_full_name=$this->input->get('person_full_name',true);
        $person_email=$this->input->get('person_email',true);
        $person_phone=$this->input->get('person_phone',true);
        $person_address=$this->input->get('person_address',true);
        $person_type=$this->input->get('person_type',true);
        $person_is_employee=$this->input->get('person_is_employee',true);
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
            -- sys_person_address.*,
            -- sys_person_additional_info.*
        ";
        $params['join'] = "
           -- LEFT JOIN sys_person_address ON person_id=person_address_person_id
           -- LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id
        ";
        $params['where'] = "
            person_was_deleted='N'
        ";
      
        if(trim($person_full_name)!='')
        {
            $params['where'].=' AND person_full_name LIKE "%'.$person_full_name.'%"';
        }
        if(trim($person_phone)!='')
        {
            $params['where'].=' AND (person_phone LIKE "%'.$person_phone.'%" OR person_telephone LIKE "%'.$person_phone.'%")';
        }
        if(trim($person_is_employee)!='')
        {
            $params['where'].=' AND person_is_employee ="'.$person_is_employee.'"';
        }
        if(trim($person_email)!='')
        {
            $params['where'].=' AND person_id IN (
                                    SELECT additional_info_person_id 
                                    FROM sys_person_additional_info
                                    WHERE additional_info_email_address LIKE "%'.$person_email.'%" 
                                    AND additional_info_is_active="Y" AND additional_info_was_deleted="N"
                                    )';
        }
        if(trim($person_address)!='')
        {
             $params['where'].=' AND person_id IN (
                                    SELECT person_address_person_id 
                                    FROM sys_person_address
                                    WHERE person_address_value LIKE "%'.$person_address.'%" 
                                    AND person_address_is_active="Y" AND person_address_was_deleted="N"
                                    )';
        }
        if(trim($person_type)!='')
        {
             $params['where'].=' AND person_id IN (
                                    SELECT person_type_detail_person_id 
                                    FROM sys_person_type_detail
                                    WHERE person_type_detail_person_type_id="'.$person_type.'" 
                                    )';
        }
        $params['order_by'] = "
            person_id DESC
        ";
        $params['group_by'] = "
            person_id 
        ";
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $status=($person_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',
            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);


            $address_arr=$this->mainModel->get_last_address($person_id);
            $person_address_province_id=isset($address_arr['person_address_province_id'])?$address_arr['person_address_province_id']:0;
            $person_address_city_id=isset($address_arr['person_address_city_id'])?$address_arr['person_address_city_id']:0;
            $person_address_district_id=isset($address_arr['person_address_district_id'])?$address_arr['person_address_district_id']:0;
            $person_address_value=isset($address_arr['person_address_value'])?$address_arr['person_address_value']:0;

            $additional_info_arr=$this->mainModel->get_last_additional_info($person_id);
            $additional_info_email_address=isset($additional_info_arr['additional_info_email_address'])?$additional_info_arr['additional_info_email_address']:'-';
            $list_person_type=$this->mainModel->list_person_type($person_id);
            $entry = array('id' => $person_id,
                'cell' => array(
                    'actions' =>  '<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$person_id.'" title="Edit"><i class="clip-pencil"></i></a>
                     <a href="#"  class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$person_id.'\');"><i class="clip-remove"></i></a>
                    ',
                    'no' =>  $no,
                    'person_full_name' =>$person_title.', '.$person_full_name.'<br /><span style="font-size:10px;">'.$list_person_type.'</span>',
                    'additional_info_email_address' =>$additional_info_email_address,
                    'person_telephone'=>$person_telephone,
                    'person_phone'=>$person_phone,
                    'person_address_value' =>$person_address_value,
                   
                    'person_gender' =>$person_gender,
                    'person_blood_type' =>$person_blood_type,
                    'person_birth_date' =>$person_birth_place.' / '.date("d-m-Y", strtotime($person_birth_date)),
                    'person_photo' =>'<img src="'.$img.'"></img>',

                    'person_address_province_id' =>$this->function_lib->get_area_name($person_address_province_id),
                    'person_address_city_id' =>$this->function_lib->get_area_name($person_address_city_id),
                    'person_address_district_id' =>$this->function_lib->get_area_name($person_address_district_id),
                   
                    'person_is_active' => $status,
                    'person_timestamp' => date("d-m-Y H:i:s", strtotime($person_timestamp)),
                    'person_admin_username' => $person_admin_username,
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
    
}
