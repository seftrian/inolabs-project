<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
//require_once dirname(__FILE__).'/../components/core_lib.php';

//class admin_privilege extends core_menu_lib{
class admin_privilege{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_administrator_privilege';
    public function __construct() {
        
        //parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));

        if(!session_id())
        {
            session_start();
        }
        
    }
    
    /**
     * set main table
     */
    public function setMainTable($table)
    {
        $this->mainTable=$table;
    }
    
    /**
     * proses penyimpanan privilege
     * @param string $postMenu data yang dipostkan
     * 
     */
    public function processSave($groupId, $postMenu='menu_administrator_id')
    {
        if(!empty($_POST[$postMenu]))
        {
            $dataMenuArr=$_POST[$postMenu];
           

            //sebelum melakukan penyimpanan, hapus dulu data lama dengan id group dan id menu terkait
            $sqlDeleteOldData='DELETE FROM '.$this->mainTable.' WHERE administrator_privilege_administrator_group_id='.  intval($groupId);
            $this->CI->db->query($sqlDeleteOldData);
            foreach($dataMenuArr AS $key=>$value)
            {
                $menuId=$value['id'];
                $methodArr=isset($value['method'])?$value['method']:array();
                
                
                if(!empty($methodArr))
                {
                    //proses penyimpanan hak akses menu
                    foreach($methodArr AS $allowed)
                    {
                        $column=array(
                            'administrator_privilege_administrator_group_id'=>$groupId,
                            'administrator_privilege_administrator_menu_id'=>$menuId,
                            'administrator_privilege_method_allowed'=>$allowed,
                        );
                        $this->CI->db->insert($this->mainTable,$column);
                    }
                }
                
                
                //save menu
                $column=array(
                            'administrator_privilege_administrator_group_id'=>$groupId,
                            'administrator_privilege_administrator_menu_id'=>$menuId,
                            'administrator_privilege_method_allowed'=>'',
                        );
                $this->CI->db->insert($this->mainTable,$column);
                    
                
            }
        }
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
