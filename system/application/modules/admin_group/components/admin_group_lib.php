<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
//echo dirname(__FILE__).'/';
//exit;

//require_once dirname(__FILE__).'/core_lib.php';
//class admin_group_lib extends core_lib{
class admin_group_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_administrator_group';
    public function __construct() {
       
  //      parent::__construct();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));

        if(!session_id())
        {
            session_start();
        }
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * @param int $groupId default 0
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($groupId=0)
    {
         $status=500;
         $message='';
         $config = array(
               array(
                     'field'   => 'admin_group_title',
                     'label'   => 'Nama Group',
                     'rules'   => 'trim|required|min_length[2]'
                  ),
               array(
                     'field'   => 'admin_group_is_active',
                     'label'   => 'Status',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'admin_group_type',
                     'label'   => 'Jenis',
                     'rules'   => 'trim|required'
                  ),  
             
            );
         
         $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
              $query=$this->CI->input->post('admin_group_title');
              $isExist=$this->isExistGroupName($query,$groupId);
            
              if(!$isExist)
              {
                  $status=500;
                  $message='Nama Group '.$query.' sudah pernah diinputkan. Silakan coba dengan yang lain.';
              }
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($groupId=0)
    {
        $status=500;
        $message='Error';
        if(is_numeric($groupId) AND $groupId!=0)
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE admin_group_id='.  intval($groupId);
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * check ketersediaan data group
     * @param stirng $query 
     * @param int $groupId default 0 saat create
     * @return bool true is valid
     */
    public function isExistGroupName($query, $groupId=0)
    {
        $value=TRUE;
        //update
        if(is_numeric($groupId) AND $groupId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_group_title',$this->mainTable,'admin_group_title LIKE TRIM(\''.$query.'\') AND admin_group_id!='.  intval($groupId));
        }
        else//create
        {
            $isFounded=$this->CI->function_lib->get_one('admin_group_title',$this->mainTable,'admin_group_title LIKE TRIM(\''.$query.'\')');
        }
        
        if($isFounded)
        {
            $value=FALSE;
        }
        
        return $value;
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
