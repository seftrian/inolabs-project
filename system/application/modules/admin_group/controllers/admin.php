<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
require_once APPPATH.'modules/admin_group/components/admin_group_lib.php';
class admin extends admin_controller{
    
    protected $adminGroupModel;
    protected $menuModel;
    protected $privilegeModel;
    //put your code here
    public function __construct() {
        parent::__construct();
        //load model menu

        $this->load->model(array('admin_menu/admin_menu','admin_privilege'));
   
        
        $this->menuModel=new admin_menu;
        $this->privilegeModel=new admin_privilege;

        $this->adminGroupModel=new admin_group_lib;
        
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
        //breadcrumbs
        $this->breadcrumbs();
        
        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['title_method']='Data Group Admin';
        $this->data['desc_method']='Daftar Group Administrator';
        
        $this->data['dataGroup']=$this->adminGroupModel->findAll();
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        
        
        //untuk hapus item yang diseleksi
        $this->delete_selected_item();
        
        //aktifkan item yang diseleksi
        $this->publish_selected_item();
        
        //non aktifkan item yang diseleksi
        $this->unpublish_selected_item();
      
        
        $this->data['seoTitle']='Data Grup';
        $description='Daftar grup admin';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-user-3",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function create()
    {
        $message='';
        $status=200;
        if($this->input->post('save'))
        {
            $_POST['admin_group_is_active']=isset($_POST['admin_group_is_active'])?$_POST['admin_group_is_active']:'0';

            //inisialisasi validation
            $isValidationPassed=$this->adminGroupModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                $columns=array(
                    'admin_group_title'=>$this->input->post('admin_group_title'),
                    'admin_group_is_active'=>$this->input->post('admin_group_is_active'),
                    'admin_group_type'=>$this->input->post('admin_group_type'),
                );
                $this->adminGroupModel->setPostData($columns);
                $hasSaved=$this->adminGroupModel->save();
                $menuId=$this->function_lib->insert_id();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    
                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/admin_group/set_privilege/'.$menuId.'?status=200&msg='.$msg);
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Tambah Grup';
        $description='Tambah grup admin';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Data Grup',
                                    'class'=>"clip-user-3",
                                    'link'=>base_url().'admin/admin_group/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data group via form
     * @param int $groupId
     */
    public function update($groupId=0)
    {
        
        if(!is_numeric($groupId) OR $groupId==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $message='';
        $status=200;
        if($this->input->post('save'))
        {
            
            //inisialisasi validation
            $isValidationPassed=$this->adminGroupModel->formValidation($groupId);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                
                $columns=array(
                    'admin_group_title'=>$this->input->post('admin_group_title'),
                    'admin_group_is_active'=>$this->input->post('admin_group_is_active'),
                    'admin_group_type'=>$this->input->post('admin_group_type'),
                );
                
                $this->adminGroupModel->setPostData($columns);
                $hasSaved=$this->adminGroupModel->save($groupId);
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/admin_group/index?status=200&msg='.$msg);
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['rowGroupAdmin']=$this->adminGroupModel->find('admin_group_id='.  intval($groupId));
        
        //cek ketersediaan group admin
        if(empty($this->data['rowGroupAdmin']))
        {
            show_404();
        }
        //load tree menu js
        $this->data['extra_head_content']='<script src="'.base_url().'addons/checkboxtree/jquery.checkboxtree.min.js"></script>';
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Ubah Grup';
        $description='Ubah grup admin';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Data Grup',
                                    'class'=>"clip-user-3",
                                    'link'=>base_url().'admin/admin_group/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk set tree menu
     * @param int $groupId id group
     */
    public function set_privilege($groupId=0)
    {
        //load tree menu js
        $this->data['seoTitle']='Set Privilege';
        $this->data['extra_head_content']='
        <link rel="stylesheet" href="'.base_url().'addons/tree/jquery.treeview.css" />
        <script src="'.base_url().'addons/tree/jquery.treeview.js" type="text/javascript"></script>    
        ';
//        $this->data['extra_head_content'].='<script type="text/javascript">
//                
//                $(function() {
//			$("ul#treeList").treeview({
//				collapsed: true,
//				animated: "medium",
//				control:"#sidetreecontrol",
//				persist: "location"
//			});
//
//                        $(\'#treeList :checkbox\').change(function (){
//    
//                        $(this).siblings(\'ul\').find(\':checkbox\').attr(\'checked\', this.checked);
//                            if (this.checked) {
//                                $(this).parentsUntil(\'#treeList\', \'ul\').siblings(\':checkbox\').attr(\'checked\', true);
//                            } else {
//                                $(this).parentsUntil(\'#treeList\', \'ul\').each(function(){
//                                    var $this = $(this);
//                                    var childSelected = $this.find(\':checkbox:checked\').length;
//                                    if (!childSelected) {
//                                        $this.prev(\':checkbox\').attr(\'checked\', false);
//                                    }
//                                });
//                            }
//                        });
//
//
//		});                 
//                </script>
//                
//                ';
        $this->data['extra_head_content'].='<script type="text/javascript">
                
                $(function() {
			$("ul#treeList").treeview({
				collapsed: true,
				animated: "medium",
				control:"#sidetreecontrol",
				persist: "location"
			});
                        
                        $(\'#treeList :checkbox\').change(function (){
    
                        $(this).siblings(\'ul\').find(\':checkbox\').attr(\'checked\', this.checked);
                            if (this.checked) {
                                //alert(\'ok\');
                                console.log("check_"+this.id);
                                parId=this.id;
                             //   $(this).parentsUntil(\'#treeList\', \'ul\').siblings(\':checkbox\').attr(\'checked\', true);
                                //$(\'#child:checkbox\').find(\'#child_\'+parId).attr(\'checked\',true);
                               // $("input[type=\'checkbox\'].child_"+parId).attr("checked",true);
                             //   $(this).find("input[type=\'checkbox\'].child_"+parId).attr("checked",true);
                                $(\':checkbox.child_\'+parId).attr(\'checked\', true);

                                $(this).parent().siblings().children().find(\':checkbox\').prop(\'checked\', true);
                             //   $(this).parentsUntil(\'#treeList\').find(\':checkbox\').prop(\'checked\',true);
                            } else {
                            
                                parId=this.id;
                                console.log("uncheck_"+parId);
                                 $(".child_"+parId).attr("checked",false);
                                /*$(this).parentsUntil(\'#treeList\', \'ul\').each(function(){
                                    var $this = $(this);
                                    var childSelected = $this.find(\':checkbox:checked\').length;
                                    if (!childSelected) {
                                        $this.prev(\':checkbox\').attr(\'checked\', false);
                                    }
                                });*/
                            }
                        });


		});   
                
                $(document).ready(function(){
                    $(".lbl").css("color","black");

                });
                
                function update_method(url,back)
                {
                    //jQuery.get(url);
                    if(confirm("Update method?"))
                    {
                        jQuery.get(url,function(response){
                        if(response=="true")
                        {
                            window.location=back;
                        }
                        
                        console.log(back);
                        }); 
                    }
                }

                </script>
                
                ';
        
        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if(!is_numeric($groupId) OR $groupId==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        //data group admin
        $this->data['rowGroupAdmin']=$this->adminGroupModel->find('admin_group_id='.  intval($groupId));
        //cek ketersediaan group admin
        if(empty($this->data['rowGroupAdmin']))
        {
            show_404();
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        
        if(isset($_POST['save']))
        {
            //proses penyimpanan
            $this->privilegeModel->processSave($groupId);
            $namaGroup=$this->function_lib->get_one('admin_group_title','site_administrator_group','admin_group_id='.  intval($groupId));
            $status=200;
            $msg=base64_encode('Proses penyimpanan untuk akses Group '.$namaGroup.' berhasil dilakukan');
            $redirect=base_url().'admin/admin_group/index?status='.$status.'&msg='.$msg;
            redirect($redirect);
        }
        
        $initalizeData=$this->menuModel->initalizeRecursiveDynamic('menu_administrator_location="admin"');
        $this->data['buildTree']=$this->menuModel->buildTreePrivilege(0, $initalizeData,$no=0,$groupId);
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $description='Privilege grup admin';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Data Grup',
                                    'class'=>"clip-user-3",
                                    'link'=>base_url().'admin/admin_group/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-tree",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);

    }
    
    /**
     * untuk update data group
     * @param int $groupId
     */
    public function view($groupId=0)
    {
        
        if(!is_numeric($groupId) OR $groupId==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $message='';
        $status=200;
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['rowGroupAdmin']=$this->adminGroupModel->find('admin_group_id='.  intval($groupId));
        if(empty($this->data['rowGroupAdmin']))
        {
            show_404();
        }
        $this->data['seoTitle']='Detil Grup Admin';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Data Grup',
                                    'class'=>"clip-user-3",
                                    'link'=>base_url().'admin/admin_group/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-zoom-in",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * hapus data group
     */
    public function delete($groupId=0,$output='redirect')
    {
        if(!is_numeric($groupId) OR $groupId==0)
        {
            show_error('Request anda tidak valid',500);
        }
        $where='admin_group_id='.  intval($groupId);
        $this->data['rowGroupAdmin']=$this->adminGroupModel->find($where);
        if(!empty($this->data['rowGroupAdmin']))
        {
           $isDelete=$this->adminGroupModel->delete($where);
           switch($output)
           {
               case 'json':
                   echo json_encode($isDelete);
               break;    
               case 'array':
                   return $isDelete;
               break;    
               default:
                   redirect(base_url().'admin/admin_group/index?status='.$isDelete['status'].'&msg='.base64_encode($isDelete['message']));
               break;    
           }
        }
        else
        {
            $msg='Permintaan tidak ditemukan';
            redirect(base_url().'admin/admin_group/index?status=500&msg='.base64_encode($msg));
        }
        
        
    }
    
    /**
     * untuk menghapus item yang diseleksi
     */
    public function delete_selected_item()
    {
        if($this->input->post('delete_selected_item') AND auth_admin_lib::manualProctection('admin','delete_selected_item'))
        {
        
      //  $manualProtection=  auth_admin_lib::manualProctection('admin','delete_selected_item');
        
            $groupAdminArr=isset($_POST['admin_group_id'])?$_POST['admin_group_id']:'';
            if(!empty($groupAdminArr))
            {
                $isDelete=array(
                    'status'=>200,
                    'message'=>'ok',
                );
                foreach($groupAdminArr AS $groupId)
                {
                    $isDelete=$this->delete($groupId, 'array');
                }
                redirect(base_url().'admin/admin_group/index?status='.$isDelete['status'].'&msg='.  base64_encode($isDelete['message']));
            }
        }
    }
    
    /**
     * untuk menghapus item yang diseleksi
     */
    public function publish_selected_item()
    {
        if($this->input->post('publish_selected_item') AND auth_admin_lib::manualProctection('admin','publish_selected_item'))
        {
            $groupAdminArr=isset($_POST['admin_group_id'])?$_POST['admin_group_id']:'';
            if(!empty($groupAdminArr))
            {
                $status=200; //status saat penyimpanan
                $message='';
                foreach($groupAdminArr AS $groupId)
                {
                    $columns=array(
                    'admin_group_is_active'=>1,
                    );

                    $this->adminGroupModel->setPostData($columns);
                    $hasSaved=$this->adminGroupModel->save($groupId);
                    $status=$hasSaved['status']; //status saat penyimpanan
                    $message='Data berhasil diaktifkan';
                }
                redirect(base_url().'admin/admin_group/index?status='.$status.'&msg='.  base64_encode($message));
            }
        }
    }
    
    /**
     * untuk menghapus item yang diseleksi
     */
    public function unpublish_selected_item()
    {
        if($this->input->post('unpublish_selected_item') AND auth_admin_lib::manualProctection('admin','unpublish_selected_item'))
        {
            $groupAdminArr=isset($_POST['admin_group_id'])?$_POST['admin_group_id']:'';
            if(!empty($groupAdminArr))
            {
                $status=200; //status saat penyimpanan
                $message='';
                foreach($groupAdminArr AS $groupId)
                {
                    $columns=array(
                    'admin_group_is_active'=>0,
                    );

                    $this->adminGroupModel->setPostData($columns);
                    $hasSaved=$this->adminGroupModel->save($groupId);
                    $status=$hasSaved['status']; //status saat penyimpanan
                    $message='Data berhasil dinonaktifkan';
                }
                redirect(base_url().'admin/admin_group/index?status='.$status.'&msg='.  base64_encode($message));
            }
        }
    }
    
    protected function breadcrumbs($method='index')
    {
        
        //breadcrumbs
        $breadcrumbs=array();
        
        $breadcrumbs[]=array(
            'label'=>'dashboard',
            'icon'=>'icon-home',
            'link'=>'#',
        );
        switch($method)
        {
            default:
                $breadcrumbs[]=array(
                    'label'=>'Data Group',
                    //'icon'=>'icon-home',
                    'link'=>'#',
                );
            break;    
        }
        $this->system_lib->setBreadcrumbs($breadcrumbs);
        
    }
    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        switch($method)
        {
            case 'set_privilege':
            ?>
            <script type="text/javascript">
             

                   $("input[name='privilege_label_name']").bind('change',function(){
                       input_id=$(this).attr('id');
                       input_name=$(this).attr('name');
                       input_class=$(this).attr('class');
                       new_label=$(this).val();
                       if(jQuery.trim(new_label)!='')
                       {
                           update_menu_label(input_id,new_label);
                       }
                   });
                   
                   
                   
                   function update_menu_label(input_id,new_label)
                   {
                       var jqxhr=jQuery.ajax({
                          url:'<?php echo base_url();?>admin_group/service_rest/save_update_menu_label', 
                          type:'post',
                          dataType:'json',
                          data:{post:1,id:input_id,privilege_menu_label:new_label},
                          beforeSend:function(){
                              console.log('');
                                 $('#response_'+input_class).html('<span style="font-weight:bold;background-color:yellow;color:black;">loading...</span>');
                          }
                       });
                       jqxhr.done(function(responseJson){
                           if(responseJson['status']==200)
                           {
//                               $('input[name='+input_name+']').next().text('sukses');
//                                 console.log($('input[name='+input_name+']').next().attr('id')+' here');
                                $('#response_'+input_class).html('<span style="font-weight:bold;background-color:green;color:white;">'+responseJson['message']+'</span>');
                           }
                       });
                       jqxhr.error(function(){
                           alert('Sorry, there is an error. Please try again later.');
                           return false;
                       });
                   }
            </script>
            <?php
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
