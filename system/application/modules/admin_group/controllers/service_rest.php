<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class service_rest extends front_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('admin_privilege',));
        $this->load->library(array('system_lib',));
        
        //inisialisasi model
        $this->mainModel=new admin_privilege;
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }
    
    
    /**
     * service handle update menu label
     */
    public function save_update_menu_label()
    {
//        $html= '<pre>';
//        print_r(json_encode($_POST));
//        $html.='</pre>';
//        echo $html;
        $status=200;
        $message=  'Success';
        $module='';
        $class='';
        $method='';
        $this->db->trans_start();
        $this->db->trans_strict(true);
        if(isset($_POST['post']) AND isset($_POST['id']))
        {
            $privilege_menu_id=$this->input->post('id',true);
            $privilege_menu_label=$this->input->post('privilege_menu_label',true);
            $explode_id=  explode('#', $privilege_menu_id);
            $module=isset($explode_id[0])?$explode_id[0]:'-';
            $class=isset($explode_id[1])?$explode_id[1]:'-';
            $method=isset($explode_id[1])?$explode_id[2]:'-';
            $is_created=$this->function_lib->get_one('privilege_label_id','site_administrator_privilege_label','
                                                    privilege_label_module="'.$module.'"
                                                        AND privilege_label_class="'.$class.'"
                                                            AND privilege_label_method="'.$method.'"');
//            echo '
//                                                    privilege_label_module="'.$module.'"
//                                                        AND privilege_label_class="'.$class.'"
//                                                            AND privilege_label_method="'.$method.'"';
            if(!$is_created)
            {
                $column=array(
                    'privilege_label_module'=>$module,
                    'privilege_label_class'=>$class,
                    'privilege_label_method'=>$method,
                    'privilege_label_name'=>$privilege_menu_label,
                );
//                print_r($column);
                $this->db->insert('site_administrator_privilege_label',$column);
                $message='Insert success!';
            }
            else
            {
                $where=array('privilege_label_id'=>intval($is_created));
                $where='privilege_label_id='.intval($is_created);
                $column=array(
                    'privilege_label_module'=>$module,
                    'privilege_label_class'=>$class,
                    'privilege_label_method'=>$method,
                    'privilege_label_name'=>$privilege_menu_label,
                );
                $this->mainModel->setMainTable('site_administrator_privilege_label');
                $this->mainModel->setPostData($column);
                $this->mainModel->save($where);
                $message='Update success!';

//                $this->db->update('site_administrator_privilege_labels',$column,$where);

            }
        }
        $this->db->trans_complete();
        
        
        if ($this->db->trans_status() === FALSE)
        {
            // generate an error... or use the log_message() function to log your error
            $status=500;
            $message=  log_message();
        }
        $response=array(
        'status'=>$status,
        'message'=>$message,);
        
        echo json_encode($response);
        
    }
}

?>
