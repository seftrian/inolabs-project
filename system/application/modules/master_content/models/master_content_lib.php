<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of
 *
 * @author tejomurti
 */
//require_once dirname(__FILE__).'/../components/core_menu_lib.php';
//class admin_menu extends core_menu_lib{
class master_content_lib {

    //put your code here

    public $CI;
    protected $postData = array(); //set post data
    protected $mainTable = 'site_news';
    protected $dir = 'assets/images/master_content/';

    public function __construct() {

        //      parent::__construct();
        //   $this->CI->load->database();
        $this->CI = &get_instance();
        $this->CI->load->library(array('function_lib', 'form_validation'));
        if (!session_id()) {
            session_start();
        }

        $this->getImagePath();
    }

    /**
      generate category from news relation
      @param int $news_id
     */
    public function generate_list_category_permalink($news_id) {
        $results = $this->get_list_category($news_id);
        $str = '';
        if (!empty($results)) {
            $str = '';
            $num = 1;
            foreach ($results AS $rowArr) {
                foreach ($rowArr AS $variable => $value) {
                    ${$variable} = $value;
                }
                $str .= ' ' . $category_permalink;
                $num++;
            }
        }

        return $str;
    }

    /**
     * @author : trianggorokasih
     * @email : anggoro.indonesia@gmail.com
     * @date  : 30-01-2016
     *
     * list all category type product except @param
     * @param int $category_permalink
     */
    public function list_all_category_type_product() {
        $sql = "SELECT * FROM site_news_category
        WHERE category_type = 'product'
        ";
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    // get post data

    public function getContactUs() {
        $sql = "SELECT * FROM site_news";
        $ex = $this->CI->db->query($sql);
        $result = $ex->last_row();
        return $result;
    }

    /**
        * @author : yudha
        * list category article except @param $category_id
    */

    public function get_news_title($where) {
        $sql = "SELECT * FROM site_news WHERE news_id = '".$where."' ORDER BY news_id DESC LIMIT 1";
        $x = $this->CI->db->query($sql);
        return $x->last_row();

    }

    /**
     * @author : trianggorokasih
     * @email : anggoro.indonesia@gmail.com
     * @date  : 30-01-2016
     *
     * list category article except @param $category_id
     * @param int $category_id, int $news_id
     */
    public function list_category_article($category_id, $news_id) {
        $sql = "SELECT
               GROUP_CONCAT(category_title SEPARATOR ', ') AS category_title,
               GROUP_CONCAT(category_permalink SEPARATOR ' ') AS category_permalink
            FROM site_news_relation
            INNER JOIN site_news_category ON category_id = news_category_id
                WHERE news_id = '" . $news_id . "' AND news_category_id <> '" . $category_id . "'";
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
     * @author : trianggorokasih
     * @email : anggoro.indonesia@gmail.com
     * @date  : 30-01-2016
     *
     * link detail
     * @param string $news_permalink
     */
    public static function link_archives_produk($news_permalink) {
        return base_url() . 'master_content/detail_produk/' . $news_permalink;
    }

    /**
     * @author : trianggorokasih
     * @email : anggoro.indonesia@gmail.com
     * @date  : 30-01-2016
     *
     * link detail
     * @param string $news_permalink
     */
    public static function link_detic_services($news_permalink) {
        return base_url() . 'master_content/detic_services/' . $news_permalink;
    }

    /**
     * @author : trianggorokasih
     * @email : anggoro.indonesia@gmail.com
     * @date  : 30-01-2016
     *
     * produk article
     * @param int $limit
     */
    public function get_new_produk($limit = 5) {
        $sql = 'SELECT sn.*, GROUP_CONCAT(category_title SEPARATOR ", ") AS category_title
      FROM site_news_relation snr
      INNER JOIN site_news_category snc ON snr.news_category_id=snc.category_id
      INNER JOIN site_news sn ON sn.news_id=snr.news_id
      WHERE
            category_type IN ("product")
            AND category_title <> "project"
      GROUP BY sn.news_id
      ORDER BY news_id DESC
      LIMIT ' . $limit;
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      dapatkan list category
     */
    public function list_tab_content($news_id) {
        $this->setMainTable('site_news_tab');
        $results = $this->findAll('tab_news_id=' . intval($news_id), 'no limit', 'no offset', 'tab_id ASC');
        return $results;
    }

    /**
      simpan tab content
      @param int $news_id
     */
    public function save_tab($news_id) {

        $id_arr = $this->CI->input->post('tab_id');
        $title_arr = $this->CI->input->post('tab_title');
        $content_arr = $this->CI->input->post('tab_content');
        if (!empty($id_arr) OR ! empty($id_arr)) {

            foreach ($id_arr AS $key => $value) {

                $id = (isset($id_arr[$key]) AND $id_arr[$key] > 0) ? $id_arr[$key] : 0;
                $title = (isset($title_arr[$key]) AND trim($title_arr[$key]) != '') ? $title_arr[$key] : '-';
                $content = (isset($content_arr[$key]) AND trim($content_arr[$key]) != '') ? $content_arr[$key] : '-';

                $column = array(
                    'tab_title' => htmlentities($title),
                    'tab_news_id' => $news_id,
                    'tab_content' => htmlentities($content),
                );

                if ($id == 0) {
                    $this->CI->db->insert('site_news_tab', $column);
                    $id = $this->CI->function_lib->insert_id();
                } else {

                    $this->CI->db->update('site_news_tab', $column, array('tab_id' => $id));
                }
            }
        }
    }

    /**
      list kategori dengan tipe yang sama
     */
    public function list_category_same_type($type = 'article') {
        $this->mainTable = 'site_news_category';
        $where = 'category_type="' . $type . '"';
        $find = $this->findAll($where, 'no limit', 'no offset', 'category_title ASC');
        return $find;
    }

    /**
      dapatkan kategori selain kategori yg dsebuutkan pada parameter
      @param string $not_in_permalink
     */
    public static function get_another_category($not_in_permalink) {
        $lib = new master_content_lib;
        //dapatkan kategori lain
        $where = 'news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink NOT IN ("' . $not_in_permalink . '") )';
        $sql = 'SELECT category_id, category_title, category_permalink FROM site_news_relation snr
              INNER JOIN site_news_category snc ON snc.category_id=snr.news_category_id
              WHERE ' . $where;
        $exec = $lib->db->query($sql);
        $row = $exec->row_array();
        //$value=!empty($row)?$row['category_title']:'n/a';
        return $row;
    }

    /**
      Get property limit
     */
    public function get_more_property_limit($id, $limit) {


        //more images
        $sql_img = '
            SELECT * FROM site_news_more_images
            WHERE news_more_images_news_id=' . intval($id) . '
            ORDER BY news_more_images_timestamp DESC
            LIMIT ' . $limit;
        $exec = $this->CI->db->query($sql_img);
        $results_img = $exec->result_array();


        return array(
            'images' => $results_img,
        );
    }

    /**
     * product
     * kriteria
     * images
     */
    public function get_more_property($id) {


        //more images
        $sql_img = '
            SELECT * FROM site_news_more_images
            WHERE news_more_images_news_id=' . intval($id) . '
            ';
        $exec = $this->CI->db->query($sql_img);
        $results_img = $exec->result_array();


        return array(
            'images' => $results_img,
        );
    }

    /**
      dapatkan data berdsarkan category id
      @param int $category_id
     */
    public function get_article_by_category_id_limit($category_id, $limit) {
        $sql = 'SELECT sn.* FROM site_news_relation snr
      INNER JOIN site_news sn ON sn.news_id=snr.news_id
      INNER JOIN site_news_category ON category_id=news_category_id
      WHERE
      news_category_id=' . intval($category_id) . '
      ORDER BY sn.news_update_datetime DESC
      Limit ' . $limit;
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      dapatkan data berdsarkan category id
      @param int $category_id
     */
    public function get_article_by_category_id($category_id) {
        $sql = 'SELECT sn.* FROM site_news_relation snr
      INNER JOIN site_news sn ON sn.news_id=snr.news_id
      INNER JOIN site_news_category ON category_id=news_category_id
      WHERE
      news_category_id=' . intval($category_id) . '
      ORDER BY sn.news_id DESC';
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      hitung jumlah artikel pada sebuah kategori
      @param int $category_id
     */
    public function count_article_in_category($category_id) {
        $where = 'news_category_id=' . intval($category_id);
        $this->setMainTable('site_news_relation');
        $count = $this->countAll($where);
        return $count;
    }

    /**
      tampilkan beberapa artikel popular
      @param int $limit default 5
      @param int $day dalam berapa hari terakhir
     */
    public function get_popular_article($limit = 5, $day = 30) {
        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 30, date('Y')));
        $sql = 'SELECT sn.* FROM site_news_relation snr
      INNER JOIN site_news_category snc ON snr.news_category_id=snc.category_id
      INNER JOIN site_news sn ON sn.news_id=snr.news_id
            WHERE DATE(news_timestamp) BETWEEN "' . $start_date . '" AND "' . $end_date . '"
            AND category_type="article"
            GROUP BY sn.news_id
            ORDER BY news_number_of_read DESC
            LIMIT ' . $limit;
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      tampilkan beberapa artikel terbaru
      @param int $limit default 5
     */
    public function get_new_article($limit = 5) {
        $sql = '
      SELECT sn.* FROM site_news_relation snr
      INNER JOIN site_news_category snc ON snr.news_category_id=snc.category_id
      INNER JOIN site_news sn ON sn.news_id=snr.news_id
      WHERE
            category_type IN ("article","gallery","archives_tag","event")
      GROUP BY sn.news_id
      ORDER BY news_id DESC
      LIMIT ' . $limit;
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      simpan file
     */
    public function save_file($news_id) {
        $this->CI->load->model('master_file/master_file_lib');

        $admin_username = isset($_SESSION['admin']['detail']['admin_username']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';

        $download_id_arr = $this->CI->input->post('download_id');
        $download_title_arr = $this->CI->input->post('download_title');
        $download_file_arr = $this->CI->input->post('download_file');
        $news_title = $this->CI->function_lib->get_one('news_title', 'site_news', 'news_id=' . intval($news_id));
        if (!empty($download_title_arr) OR ! empty($download_file_arr)) {

            foreach ($download_title_arr AS $key => $value) {

                $download_id = (isset($download_id_arr[$key]) AND $download_id_arr[$key] > 0) ? $download_id_arr[$key] : 0;
                $download_title = (isset($download_title_arr[$key]) AND trim($download_title_arr[$key]) != '') ? $download_title_arr[$key] : $news_title . ' ' . ($key + 1);

                $permalink = $this->CI->master_file_lib->generate_permalink($download_title, $download_id);
                $column = array(
                    'download_title' => $download_title,
                    'download_news_id' => $news_id,
                    'download_permalink' => $permalink,
                    'download_input_by' => $admin_username,
                );

                if ($download_id == 0) {
                    $this->CI->db->insert('site_download', $column);
                    $download_id = $this->CI->function_lib->insert_id();
                } else {

                    $this->CI->db->update('site_download', $column, array('download_id' => $download_id));
                }
                $filesName = 'download_file';
                $response = $this->handleFileUpload($download_id, $filesName, $key);
                if ($response['status'] != 200) {
                    return array(
                        'status' => $response['status'],
                        'message' => $response['message'],
                    );
                }
            }
        }
    }

    /**
     * handle upload
     */
    public function handleFileUpload($id, $filesName, $index) {
        $status = 200;
        $message = 'File berhasil disimpan';

        $fileUpload = (isset($_FILES[$filesName . '_' . $index]['name']) AND trim($_FILES[$filesName . '_' . $index]['name']) != '') ? $_FILES[$filesName . '_' . $index]['name'] : array();

        if (!empty($fileUpload) AND ( isset($_FILES[$filesName . '_' . $index]['name']) AND trim($_FILES[$filesName . '_' . $index]['name'] != ''))) {
            //$results=$this->doFileUpload($id,$filesName,$index);
            $results = $this->doFileUpload2($id, $filesName, $index);

            $status = $results['status'];
            $message = $results['message'];
        }

        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    public function doFileUpload2($id, $field_name, $index) {
        $ext = strtolower(pathinfo($_FILES[$field_name . '_' . $index]["name"], PATHINFO_EXTENSION));
        $pathImage = 'assets/images/master_file/';

        $pathUpload = FCPATH . $pathImage;
        $allowed_types = array(1 => 'gif', 'jpg', 'jpeg', 'png', 'pdf', 'doc', 'xls', 'xlsx', 'ppt', 'docx', 'pptx', 'zip');
        if (array_search($ext, $allowed_types, true) < 1) {

            $status = 500;
            $message = "Error: File tidak diizinkan.";
        } else {
            $status = 200;
            $message = "OK";

            $filename = $_FILES[$field_name . '_' . $index]["name"];
            $newFileName = strtotime(date('Y-m-d H:i:s')) . $index . '.' . $ext;

            if (file_exists($pathUpload . '/' . $newFileName) AND $filename != '') {
                unlink($pathUpload . '/' . $newFileName);
            }
            if (move_uploaded_file($_FILES[$field_name . '_' . $index]["tmp_name"], $pathUpload . '/' . $newFileName)) {

                $where = array(
                    'download_id' => $id,
                );
                $column = array(
                    'download_file' => $pathImage . $newFileName,
                );

                $this->setMainTable('site_download');
                $this->CI->db->update($this->mainTable, $column, $where);
                $status = 200;
                $message = 'File berhasil disimpan';
            } else {
                $status = 500;
                $message = 'Gagal upload file';
            }
        }

        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    /**
     * library upload
     * @param string $field_name
     */
    public function doFileUpload($id, $field_name, $index) {

        $config = array();
        $pathImage = 'assets/images/master_file/';
        $config['upload_path'] = FCPATH . '/' . $pathImage;
        $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|doc|xls|xlsx|ppt';
        $config['max_size'] = '3200'; //KB

        $this->CI->load->library('upload', $config);

        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);

        $isComplete = $this->CI->upload->do_upload($field_name . '_' . $index);
        if ($isComplete) {
            $dataImage = $this->CI->upload->data();
            //simpan
            $where = array(
                'download_id' => $id,
            );
            $column = array(
                'download_file' => $pathImage . $dataImage['file_name'],
            );

            $this->setMainTable('site_download');
            $this->CI->db->update($this->mainTable, $column, $where);
            $status = 200;
            $message = 'File berhasil disimpan';
        } else {
            $status = 500;
            $message = $this->CI->upload->display_errors();
            ;
        }
        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    /**
      dapatkan jumlah dibaca
      @param int $news_id
     */
    public static function get_number_of_read($news_id) {
        $lib = new master_content_lib;
        $where = 'news_id=' . intval($news_id);
        $number_of_read = $lib->CI->function_lib->get_one('news_number_of_read', 'site_news', $where);
        return number_format(intval($number_of_read), 0, '', '.');
    }

    /**
      fungsi untuk menghitung jumlah pembaca
      @param int $news_id
      limit time 30 menit
     */
    public function count_number_of_read($news_id) {
        $news_id_original = $news_id;
        //dapatkan ip address
        $client_ip = function_lib::get_client_ip();
        $client_ip = ($client_ip == '::1') ? '127.0.0.1' : $client_ip;
        //cek apakah ip ini membaca berita yg sama selama 30 menit
        $thirty_minute_ago = date('Y-m-d H:i:s', mktime(date('H'), (date('i') - 30), date('s'), date('m'), date('d'), date('Y')));

        $where = "news_counter_news_id=" . intval($news_id) . '
                AND news_counter_ip_address=INET_ATON(\'' . $client_ip . '\')
                AND news_counter_timestamp>"' . $thirty_minute_ago . '"';

        $news_id = $this->CI->function_lib->get_one('news_counter_news_id', 'site_news_counter', $where);

        //jika data tidak tersedia maka insertkan ke database
        if (!$news_id AND $news_id_original > 0) {

            $sql = 'INSERT INTO site_news_counter
          (news_counter_news_id,news_counter_ip_address)
          VALUES (' . intval($news_id_original) . ',INET_ATON("' . $client_ip . '"))';
            $this->CI->db->query($sql);

            //update nilai + 1 ke site news
            $sql = "UPDATE site_news SET news_number_of_read=news_number_of_read+1
          WHERE news_id=" . intval($news_id_original);
            $this->CI->db->query($sql);
        }
    }

    public static function decode_seo_name($query) {
        $value = str_replace('_', ' ', $query);
        return $value;
    }

    /**
      pencarian
      @param string $query
     */
    public function get_news_from_search($query = '', $limit = 10, $offset = 0, $order_by = 'news_id DESC') {
        $filter_value = $this->CI->security->sanitize_filename($query);
        $where = 'news_title LIKE "%' . $filter_value . '%" OR news_content LIKE "%' . $filter_value . '%"';
        $sql = 'SELECT * FROM site_news_relation snr
              INNER JOIN site_news sn ON sn.news_id=snr.news_id
              INNER JOIN site_news_category snc ON snc.category_id=snr.news_category_id
              WHERE ' . $where . '
              GROUP BY sn.news_id
              ORDER BY sn.news_id DESC
              LIMIT ' . $offset . ', ' . $limit . '
              ';
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      dapatkan total pencarian
      @param string $news_permalink
     */
    public function count_news_from_search($query = '') {
        $filter_value = $this->CI->security->sanitize_filename($query);
        $where = 'news_title LIKE "%' . $filter_value . '%" OR news_content LIKE "%' . $filter_value . '%"';
        $sql = 'SELECT COUNT(snr.news_id) AS num FROM site_news_relation snr
              INNER JOIN site_news sn ON sn.news_id=snr.news_id
              WHERE ' . $where
        ;
        $exec = $this->CI->db->query($sql);
        $results = $exec->row_array();
        $value = !empty($results) ? $results['num'] : 0;
        return $value;
    }

    /**
      link detail
      @param string $category_permalink
     */
    public static function link_archives($category_permalink) {
        return base_url() . 'master_content/archives/' . $category_permalink;
    }

    /**
      link detail
      @param string $category_permalink
     */
    public static function link_archives_by_category($category_permalink) {
        $lib = new master_content_lib;
        $category_type = $lib->CI->function_lib->get_one('category_type', 'site_news_category', 'category_permalink="' . $category_permalink . '"');
        switch (strtolower($category_type)) {
            case 'archives_tag':
                $link = base_url() . 'master_content/archives_tag/' . $category_permalink;
                break;
            default:
                $link = base_url() . 'master_content/archives/' . $category_permalink;
                break;
        }

        return $link;
    }

    /**
      link detail
      @param string $category_permalink
      @param string $news_permalink
     */
    public static function link_detail($news_id, $news_permalink) {
        $lib = new master_content_lib;
        $where = 'snc.category_id IN (SELECT snr.news_category_id FROM site_news_relation snr WHERE snr.news_id=' . intval($news_id) . ' AND snr.news_category_id=snc.category_id)';
        $news_category_permalink = $lib->CI->function_lib->get_one('category_permalink', 'site_news_category snc', $where);
        return base_url() . 'content/' . $news_category_permalink . '/' . $news_permalink;
    }

    /**
      link detail
      @param string $category_permalink
     */
    public static function link_detail_by_category($news_id, $news_permalink) {
        $lib = new master_content_lib;
        $where = 'snc.category_id IN (SELECT snr.news_category_id FROM site_news_relation snr WHERE snr.news_id=' . intval($news_id) . ' AND snr.news_category_id=snc.category_id)';
        $news_category_permalink = $lib->CI->function_lib->get_one('category_permalink', 'site_news_category snc', $where);
        $category_type = $lib->CI->function_lib->get_one('category_type', 'site_news_category', 'category_permalink="' . $news_category_permalink . '"');
        switch (strtolower($category_type)) {
            case 'archives_tag':
                $link = base_url() . 'master_content/archives_tag/' . $news_category_permalink . '/single';
                break;
            case 'kode_etik':
                $link = base_url() . 'master_content/detail_produk/' . $news_permalink;
                break;
            case 'gallery':
                $link = base_url() . 'master_content/detail_gallery/' . $news_permalink;
                break;
            case 'event':
                $link = base_url() . 'master_content/detic_services/' . $news_permalink;
                break;
            case 'static':
                $link = base_url() . 'master_content/detail/' . $news_permalink;
                break;
            default:
                $link = self::link_detail($news_id, $news_permalink);
                break;
        }

        return $link;
    }

    /**
      link detail
      @param string $category_permalink
      @param string $news_permalink
     */
    public static function link_detail_gallery($news_permalink) {
        return base_url() . 'master_content/detail_gallery/' . $news_permalink;
    }

    public function get_data_news_category($category_permalink = '', $limit = 10, $offset = 0, $order_by = 'news_id DESC') {
        $where = 'site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "' . $category_permalink . '")';
        $sql = 'SELECT * FROM site_news
      INNER JOIN site_news_relation ON site_news.news_id = site_news_relation.news_id
      WHERE ' . $where . '
      ORDER BY site_news.' . $order_by . '
      LIMIT ' . $offset . ', ' . $limit . '
      ';
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      dapatkan data detail news berdasarkan permalink
      @param string $news_permalink
     */
    public function get_news_from_category_permalink($category_permalink = '', $limit = 10, $offset = 0, $order_by = 'news_id DESC') {
        $where = 'sn.news_is_publish="Y" AND news_category_id=(SELECT category_id FROM site_news_category WHERE category_permalink="' . $category_permalink . '")';
        $sql = 'SELECT * FROM site_news_relation snr
              INNER JOIN site_news sn ON sn.news_id=snr.news_id
              WHERE ' . $where . '
              ORDER BY sn.' . $order_by . '
              LIMIT ' . $offset . ', ' . $limit . '
              ';
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      dapatkan data detail news berdasarkan permalink
      @param string $news_permalink
     */
    public function get_news_from_multi_category($where = '', $limit = 10, $offset = 0, $order_by = 'news_id DESC') {
        $sql = 'SELECT * FROM site_news_relation snr
              INNER JOIN site_news sn ON sn.news_id=snr.news_id
              WHERE ' . $where . '
              ORDER BY ' . $order_by . '
              LIMIT ' . $offset . ', ' . $limit . '
              ';
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      dapatkan data detail news berdasarkan permalink
      @param string $news_permalink
     */
    public function count_news_from_multi_category($where) {
        //    $where='news_category_id=(SELECT category_id FROM site_news_category WHERE category_permalink="'.$category_permalink.'")';
        $sql = 'SELECT COUNT(snr.news_id) AS num FROM site_news_relation snr
              INNER JOIN site_news sn ON sn.news_id=snr.news_id
              WHERE ' . $where
        ;
        $exec = $this->CI->db->query($sql);
        $results = $exec->row_array();
        $value = !empty($results) ? $results['num'] : 0;
        return $value;
    }

    /**
      dapatkan data detail news berdasarkan permalink
      @param string $news_permalink
     */
    public function count_news_from_category_permalink($category_permalink = '') {
        $where = 'sn.news_is_publish="Y" AND news_category_id=(SELECT category_id FROM site_news_category WHERE category_permalink="' . $category_permalink . '")';
        $sql = 'SELECT COUNT(snr.news_id) AS num FROM site_news_relation snr
              INNER JOIN site_news sn ON sn.news_id=snr.news_id
              WHERE ' . $where
        ;
        $exec = $this->CI->db->query($sql);
        $results = $exec->row_array();
        $value = !empty($results) ? $results['num'] : 0;
        return $value;
    }

    /**
      dapatkan data detail news berdasarkan permalink
      @param string $news_permalink
     */
    public function get_news_detail_from_permalink($news_permalink = '') {
        $where = 'news_permalink="' . $this->CI->security->sanitize_filename($news_permalink) . '"';
        $sql = 'SELECT site_news.*
            FROM site_news
            WHERE ' . $where;
        $exec = $this->CI->db->query($sql);
        $row = $exec->row_array();

        return $row;
    }

    public function get_news_id_from_permalink($news_permalink = '') {
        $where = 'news_permalink="' . $this->CI->security->sanitize_filename($news_permalink) . '"';
        $news_id = $this->CI->function_lib->get_one('news_id', 'site_news', $where);

        return $news_id;
    }

    /**
      dapatkan jumlah artikel yang telah ada dikategori tersebut
      @param int $news_category_id
     */
    public static function count_article_on_category($category_id) {
        $lib = new master_content_lib;
        $where = 'news_category_id=' . intval($category_id);
        $sql = 'SELECT COUNT(snr.news_id) AS num FROM site_news_relation snr
            INNER JOIN site_news sn ON sn.news_id=snr.news_id
            WHERE ' . $where;
        $exec = $lib->CI->db->query($sql);
        $row = $exec->row_array();
        $value = !empty($row) ? $row['num'] : 0;
        return $value;
    }

    /**
      fungsi untuk mendapatkan list category
      @param int $news_id
      @param string $additional_where
     */
    public function get_list_category($news_id, $additional_where = '') {
        $where = 'snr.news_id=' . intval($news_id) . ' ' . $additional_where;
        $sql = 'SELECT snc.*
            FROM site_news_category snc
            INNER JOIN site_news_relation snr ON snc.category_id=snr.news_category_id
            WHERE ' . $where;
        $exec = $this->CI->db->query($sql);
        $results = $exec->result_array();
        return $results;
    }

    /**
      generate category from news relation
      @param int $news_id
     */
    public function generate_list_category($news_id) {
        $results = $this->get_list_category($news_id);
        $str = '<a href="' . base_url() . 'admin/master_content/update/' . $news_id . '"><i class="fa fa-pencil"></i> Tambahkan ke kategori?</a>';
        if (!empty($results)) {
            $str = '';
            $num = 1;
            foreach ($results AS $rowArr) {
                foreach ($rowArr AS $variable => $value) {
                    ${$variable} = $value;
                }
                $link_archives = self::link_archives_by_category($category_permalink);
                $link_delete = '<a href="javascript:void(0)" onclick="delete_category(\'' . $news_id . '\',\'' . $category_id . '\');return false;" title="Hapus Kategori">[x]</a>';
                $str .= ($num > 1) ? '<br />' . $link_delete . ' <a href="#" onclick="search_by_category(' . $category_id . ');">' . $category_title . '</a> [<a href="' . $link_archives . '" target="_blank">' . $link_archives . '</a>]' : '' . $link_delete . ' <a href="#" onclick="search_by_category(' . $category_id . ');">' . $category_title . '</a> [<a href="' . $link_archives . '" target="_blank">' . $link_archives . '</a>]';
                $num++;
            }
        }

        return $str;
    }

    /**
      save category and news into relation table
      @param int $news_id
     */
    public function store_category_to_news($news_id = 0) {
        //hapus data lama
        $this->setMainTable('site_news_relation');
        $where = 'news_id=' . intval($news_id);
        $this->delete($where);

        //insertkan data news and category to relation
        $news_category_id_arr = $this->CI->input->post('news_category_id');
        if (!empty($news_category_id_arr)) {
            foreach ($news_category_id_arr AS $news_category_id) {
                $column = array(
                    'news_category_id' => $news_category_id,
                    'news_id' => $news_id,
                );
                $this->CI->db->insert('site_news_relation', $column);
            }
        }
    }

    /**
      dapatkan list category
     */
    public function list_news_category() {
        $this->setMainTable('site_news_category');
        $results = $this->findAll('1', 'no limit', 'no offset', 'category_title ASC');
        return $results;
    }

    /**
      dapatkan list category
     */
    public function list_files($news_id) {
        $this->setMainTable('site_download');
        $results = $this->findAll('download_news_id=' . intval($news_id), 'no limit', 'no offset', 'download_id ASC');
        return $results;
    }

    /**
      generate permalink
      @param string $title
     */
    public function generate_permalink($title, $id = 0) {
        $datetime = date('Y-m-d H:i:s');
        $permalink = $this->CI->function_lib->seo_name($title);
        $where = 'news_permalink="' . $permalink . '"';
        if ($id != 0) {
            $where .= ' AND news_id!=' . intval($id);
        }
        $is_exist = $this->CI->function_lib->get_one('news_id', 'site_news', $where);
        if ($is_exist) {
            $permalink = $this->CI->function_lib->seo_name($title) . strtotime($datetime);
        }
        return $permalink;
    }

    /**
     * dapatkan path image
     */
    public function getImagePath() {
        $path = base_url() . $this->dir;
        $pathLocation = FCPATH . $this->dir;

        if (!file_exists($pathLocation) AND trim($pathLocation) != '') {
            $explode_arr = explode('/', $pathLocation);
            $last_dir = count($explode_arr) - 2;
            //src
            if (!empty($explode_arr)) {
                $src = '';
                $max_src = count($explode_arr) - 2;
                for ($i = 0; $i < count($explode_arr); $i++) {
                    if ($i < $max_src) {

                        $src .= $explode_arr[$i] . '/';
                    }
                }
            }

            $dir = $explode_arr[$last_dir];
            mkdir($src . $dir);
            chmod($src . $dir, 0777);
        }

        return array(
            'pathUrl' => $path,
            'pathLocation' => $pathLocation,
        );
    }

    public function getMainTable() {
        return $this->mainTable;
    }

    /**
     * set main table
     */
    public function setMainTable($table) {
        $this->mainTable = $table;
    }

    public function upload_more_images($news_id) {
        if (!empty($_FILES)) {
            $no = 1;
            $img_detail_id = '';
            foreach ($_FILES AS $filesName => $rowArr) {
                $results = $this->handleUpload($news_id, $filesName, $no);
                $post_id = isset($_POST[$filesName . '_id']) ? intval($_POST[$filesName . '_id']) : 0;
                $img_id = ($results['id'] > 0) ? $results['id'] : $post_id;
                $img_detail_id .= ($no > 1) ? ', ' . $img_id : $img_id;

                if ($results['status'] != 200) {
                    return array(
                        'status' => 500,
                        'message' => 'Gambar ' . $_FILES[$filesName]['name'] . ' gagal diupluoad. ',
                    );
                }
                $no++;
            }

            $sql = 'DELETE FROM site_news_more_images WHERE news_more_images_id NOT IN (' . $img_detail_id . ')
                  AND news_more_images_news_id=' . intval($news_id);
            $this->CI->db->query($sql);
        }
//        else
//        {
//            if(isset($_POST['']))
//            {
//
//            }
//        }
        return array(
            'status' => 200,
            'message' => 'success',
        );
    }

    /**
     * handle upload
     */
    public function handleUpload($id, $filesName, $no) {
        $status = 200;
        $message = 'File berhasil disimpan';

        $fileUpload = (isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name']) != '') ? $_FILES[$filesName] : array();
        $id_new = 0;
        if (!empty($fileUpload) AND ( isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'] != ''))) {
            $results = $this->doUpload($id, $filesName, $no);
            $status = $results['status'];
            $message = $results['message'];
            $id_new = $results['id'];
        }

        return array(
            'status' => $status,
            'message' => $message,
            'id' => $id_new,
        );
    }

    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id, $field_name, $no) {
        $config = array();
        $pathImage = 'assets/images/master_content/';
        $config['upload_path'] = FCPATH . '/' . $pathImage;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '10500'; //KB
        $config['file_name'] = function_lib::seo_name(time());



        $this->CI->load->library('upload', $config);


        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);

        $isComplete = $this->CI->upload->do_upload($field_name);
        $img_id = 0;
        //echo $isComplete.'/'.$field_name;

        $explode_arr = explode('_', $field_name);
        $string_1 = isset($explode_arr[0]) ? $explode_arr[0] : '';
        //echo $string_1;
        //die('sss');


        if ($isComplete) {
            $dataImage = $this->CI->upload->data();
            //simpan
            if ($field_name == 'news_photo') {

                if (isset($_POST['news_photo_old']) AND trim($_POST['news_photo_old']) != '' AND file_exists(FCPATH . $_POST['news_photo_old'])) {
                    unlink(FCPATH . $_POST['news_photo_old']);
                }

                $where = array(
                    'news_id' => $id,
                );
                $column = array(
                    'news_photo' => $pathImage . $dataImage['file_name'],
                );
                $this->CI->db->update('site_news', $column, $where);
            } else {

                if ($string_1 == 'news') {
                    if (!isset($_POST[$field_name . '_id'])) {

                        $column = array(
                            'news_more_images_news_id' => $id,
                            'news_more_images_file' => $pathImage . $dataImage['file_name'],
                            'news_more_images_is_active' => 1,
                        );
                        $this->CI->db->insert('site_news_more_images', $column);
                        $img_id = $this->CI->function_lib->insert_id();
                    } else {
                        $img_id = $_POST[$field_name . '_id'];
                        $old_image = $_POST[$field_name . '_old'];
                        if (file_exists(FCPATH . $old_image) AND trim($old_image) != '') {
                            unlink(FCPATH . $old_image);
                        }
                        $column = array(
                            'news_more_images_news_id' => $id,
                            'news_more_images_file' => $pathImage . $dataImage['file_name'],
                            'news_more_images_is_active' => 1,
                        );
                        $where = array(
                            'news_more_images_id' => $img_id,
                        );
                        $this->CI->db->update('site_news_more_images', $column, $where);
                    }
                }
            }

            $status = 200;
            $message = 'File berhasil disimpan';
        } else {
            $status = 500;
            $message = $this->CI->upload->display_errors();
            ;
        }
        return array(
            'status' => $status,
            'message' => $message,
            'id' => $img_id,
        );
    }

    /*
      /**
     * handle upload
     */
    /* public function handleUpload($id,$filesName)
      {
      $status=200;
      $message='File berhasil disimpan';

      $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();

      if(!empty($fileUpload))
      {
      $results=$this->doUpload($id,$filesName);
      $status=$results['status'];
      $message=$results['message'];
      }

      return array(
      'status'=>$status,
      'message'=>$message,
      );
      }
      /**
     * library upload
     * @param string $field_name
     */
    /* public function doUpload($id,$field_name)
      {
      $config=array();
      $pathImage=$this->dir;
      $config['upload_path'] = FCPATH.'/'.$pathImage;
      $config['allowed_types'] = 'gif|jpg|jpeg|png';
      $config['max_size'] = '10200'; //KB


      $this->CI->load->library('upload', $config);


      // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
      $this->CI->upload->initialize($config);

      $isComplete=$this->CI->upload->do_upload($field_name);
      if($isComplete)
      {
      $dataImage=$this->CI->upload->data();
      //simpan
      $where=array(
      'news_id'=>$id,
      );
      $column=array(
      'news_photo'=>$pathImage.$dataImage['file_name'],
      );
      $this->setMainTable('site_news');
      $this->CI->db->update($this->mainTable,$column,$where);
      $status=200;
      $message='File berhasil disimpan';

      }
      else
      {
      $status=500;
      $message=$this->CI->upload->display_errors();;
      }
      return array(
      'status'=>$status,
      'message'=>$message,
      );

      }
     */

    /**
     * dapatkan satu baris data
     * @param string $where
     *
     * @return array
     */
    public function find($where = 1) {
        $sql = 'SELECT * FROM ' . $this->mainTable . '
              WHERE ' . $where . '
              ';
        $exec = $this->CI->db->query($sql);
        return $exec->row_array();
    }

    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT
     * @return array
     */
    public function findAll($where = 1, $limit = 10, $offset = 0, $orderBy = 'category_id ASC') {
        $sql = 'SELECT * FROM ' . $this->mainTable . '
              WHERE ' . $where . '
              ';
        if (trim($orderBy) != '') {
            $sql .= ' ORDER BY ' . $orderBy;
        }
        if (is_numeric($limit) AND is_numeric($offset)) {
            $sql .= ' LIMIT ' . $offset . ', ' . $limit;
        }
        $exec = $this->CI->db->query($sql);
        return $exec->result_array();
    }

    /*
     * @author yudha
     *
     */

    public function getSiteDownloadOneDes($where, $limit = 1, $orderBy = 'category_id DESC') {
        $sql = 'SELECT * FROM ' . $this->mainTable . '
            WHERE ' . $where . '
                ';
        if (trim($orderBy) != '') {
            $sql .= ' ORDER BY ' . $orderBy;
        }
        if (is_numeric($limit)) {
            $sql .= ' LIMIT ' . $limit;
        }
        $exec = $this->CI->db->query($sql);
        return $exec->row_array();
    }

    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT
     * @return array
     */
    public function countAll($where = 1) {
        $sql = 'SELECT COUNT(*) AS jml FROM ' . $this->mainTable . '
              WHERE ' . $where . '
              ';
        $exec = $this->CI->db->query($sql);
        $row = $exec->row_array();
        return !empty($row) ? $row['jml'] : 0;
    }

    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where) {
        $status = 200;
        $message = '';

        if (trim($where) != '') {
            $sql = 'DELETE FROM ' . $this->mainTable . '
                WHERE ' . $where;

            try {
                $this->CI->db->query($sql);
                $status = 200;
                $message = 'Data berhasil dihapus';
            } catch (Exception $e) {
                $status = 500;
                $message = 'Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }


        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    /**
     * untuk validasi form
     *
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id = 0) {
        $status = 500;
        $message = '';
        $mainConfig = array(
            array(
                'field' => 'news_title',
                'label' => 'Judul',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'news_content',
                'label' => 'Isi',
                'rules' => 'trim|required'
            ),
        );

        $config = $mainConfig;
        /**
         * jika lokasi admin, maka module controller dan method wajib diisi
         * main config dan additional config akan digabung
         */
        $this->CI->form_validation->set_rules($config);
        if ($this->CI->form_validation->run() == TRUE) {

            //proses melewati validasi form
            $status = 200;
            $message = 'Form ready to save';
        }

        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where = '') {
        $status = 500;
        $message = 'Error';
        if (trim($where) != '') {
            $sql = 'UPDATE ' . $this->mainTable . ' SET ';
            if (!empty($this->postData)) {
                $no = 1;
                foreach ($this->postData AS $column => $value) {
                    $separated = ($no >= 1 AND $no < count($this->postData)) ? ',' : '';
                    $sql .= ' ' . $column . '="' . $value . '"' . $separated;
                    $no++;
                }
            }
            $sql .= ' WHERE ' . $where;
            $this->CI->db->query($sql);
            $status = 200;
            $message = 'OK';
        } else {
            $this->CI->db->insert($this->mainTable, $this->postData);
            $status = 200;
            $message = 'OK';
        }
        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    /**
     *
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr) {
        $this->postData = $dataArr;
    }

}

?>
