<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
class admin extends admin_controller {

    public $mainModel; //model utama dari module ini

    //put your code here

    public function __construct() {
        parent::__construct();

        //load model
        $this->load->model(array('master_content_lib'));
        $this->load->helper(array('pagination', 'tinymce'));

        //inisialisasi model
        $this->mainModel = new master_content_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr = $this->mainModel->getImagePath();
    }

    /**
      load form tab
     */
    public function load_form_tab($no = 1) {
        $this->load->helper(array('tinymce'));

        $this->data['index'] = $no;
        $this->data['num_order'] = $no + 1;
        $this->load->view_single($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk update data via form
     * @param int $id
     */
    public function tab_content($id = 0) {
        $status = (isset($_GET['status']) AND trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) AND trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Tab Content';
        $description = 'Menampilkan konten dalam beberapa bagian';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );

        $breadcrumbs_array[] = array(
            'name' => 'Master Konten',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/master_content/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Ubah',
            'class' => "clip-pencil",
            'link' => base_url() . 'admin/master_content/update/' . $id,
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Tab Content',
            'class' => "clip-pencil",
            'link' => base_url() . 'admin/master_content/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);


        if (!is_numeric($id) OR $id == 0) {
            show_error('Request anda tidak valid', 500);
        }

        $rowMenu = $this->mainModel->find('news_id=' . intval($id));
        if (empty($rowMenu)) {
            show_error('Request anda tidak valid', 500);
        }

        foreach ($rowMenu AS $variable => $value) {
            $this->data[$variable] = $value;
        }

        if ($this->input->post('save') OR $this->input->post('save_return')) {
            $this->mainModel->save_tab($id);
            $msg = base64_encode('Data berhasil disimpan.');
            if ($this->input->post('save')) {
                redirect(base_url() . 'admin/' . $this->currentModule . '/tab_content/' . $id . '?status=200&msg=' . $msg);
            } else {
                redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=200&msg=' . $msg);
            }
        }
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['list_tab_content'] = $this->mainModel->list_tab_content($id);

        $this->data['footerScript'] = $this->footerScript(__FUNCTION__, array(
            'count_tab_content' => count($this->data['list_tab_content'])));


        //$this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    function get_data() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            " . $this->mainModel->getMainTable() . ".*
        ";

        $news_category_id = $this->input->get('news_category_id', true);
        $news_title = $this->input->get('news_title', true);
        $news_title = str_replace('%20', ' ', $news_title);
        $news_content = $this->input->get('news_content', true);
        $news_content = str_replace('%20', ' ', $news_content);

        $where = 1;
        if (trim($news_category_id) != '' AND intval($news_category_id) > 0) {
            $where .= ' AND news_id IN (SELECT site_news_relation.news_id FROM site_news_relation WHERE site_news_relation.news_category_id=' . intval($news_category_id) . '
                AND site_news_relation.news_id=news_id)';
        }
        $where .= ' AND news_title LIKE "%' . $news_title . '%" AND news_content LIKE "%' . $news_content . '%" ';
        $params['where'] = $where;

        $params['order_by'] = "
            news_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        $pathImgUrl = $this->pathImgArr['pathUrl'];
        $pathImgLoc = $this->pathImgArr['pathLocation'];
        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {

            foreach ($row AS $variable => $value) {
                ${$variable} = $value;
            }


            $no++;

            $logo_name = (trim($news_photo) == '') ? '-' : pathinfo($news_photo, PATHINFO_FILENAME);
            $logo_ext = pathinfo($news_photo, PATHINFO_EXTENSION);
            $logo_file = $logo_name . '.' . $logo_ext;

            $imgProperty = array(
                'width' => 70,
                'height' => 70,
                'imageOriginal' => $logo_file,
                'directoryOriginal' => $pathImgLoc,
                'directorySave' => $pathImgLoc . '7070/',
                'urlSave' => $pathImgUrl . '7070/',
            );
            $img = $this->function_lib->resizeImageMoo($imgProperty);

            $edit = '<a class="btn btn-xs btn-primary" href="' . base_url() . 'admin/' . $this->currentModule . '/update/' . $news_id . '" title="Edit"><i class="clip-pencil"></i></a>';
            $delete = ' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\'' . $news_id . '\');"><i class="clip-remove"></i></a>';
            //$content=strip_tags(html_entity_decode($news_content));
            //$content=(strlen($content)<150)?$content:substr($content,0,150);
            //kategori
            $generate_list_category = $this->mainModel->generate_list_category($news_id);
            $label_additional = '<p class="text-muted">' . $news_label_1 . '<br /> <br />' . convert_datetime($news_timestamp) . '</p>';
            $link_main = master_content_lib::link_detail_by_category($news_id, $news_permalink);
            $filter_link = str_replace(base_url(), '', $link_main);
            $load_link_from_content = '<a href="javascript:void(0)" title="Pilih link ini" onclick="load_link_from_content(\'' . $filter_link . '\',\'' . $link_main . '\');return false;" class="btn btn-info btn-xs"><span class="clip-checkbox"></span></a>';
            $label_status = ($news_is_publish == 'Y') ? '<label class="label label-info">YES<label>' : '<label class="label label-danger">NO<label>';

            $entry = array('id' => $news_id,
                'cell' => array(
                    'no' => $no . '.',
                    'delete' => ($news_is_deleted == 'Y') ? $delete : 'n/a',
                    'edit' => $edit,
                    'load_link' => $load_link_from_content,
                    'status_publish' => $label_status,
                    'title' => $news_title . $label_additional,
                    'content' => $this->function_lib->limit_words(strip_tags(htmlspecialchars_decode($news_content)), 10) . ' <br /><p style="float:right;display:none;" class="text-muted">' . $generate_list_category . '
                    <!--<br />Link: <a href="' . $link_main . '" target="_blank">' . $link_main . '</a>-->
                    </p>',
                    'meta_description' => $news_meta_description,
                    'meta_tags' => $news_meta_tags,
                    'photo' => '<img src="' . $img . '" />',
                    'input_by' => $news_input_by,
                ),
            );
            $json_data['rows'][] = $entry;
        }
        echo json_encode($json_data);
    }

    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index() {

        $status = (isset($_GET['status']) AND trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) AND trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Konten';
        $description = 'Master Konten';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Konten',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/' . $this->currentModule . '/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        //list category
        $this->data['list_news_category'] = $this->mainModel->list_news_category();
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function load_external_link() {

        $status = (isset($_GET['status']) AND trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) AND trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Konten';
        $description = 'Master Konten';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Konten',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/' . $this->currentModule . '/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        $this->data['list_news_category'] = $this->mainModel->list_news_category();
        $this->load->view_single($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk menambahkan data
     */
    public function add() {

        $status = (isset($_GET['status']) AND trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) AND trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';


        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Master Konten';
        $description = 'Tambah Konten';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );

        $breadcrumbs_array[] = array(
            'name' => 'Master Konten',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/master_content/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url() . 'admin/master_content/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);

        if ($this->input->post('save') OR $this->input->post('save_return')) {
            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation();
            $status = $isValidationPassed['status'];
            $message = ($status == 500 AND $isValidationPassed['message'] == '') ? validation_errors() : $isValidationPassed['message'];

            if ($status == 200) {
                $title = function_lib::clear_special_character($this->input->post('news_title'));
                $admin_username = isset($_SESSION['admin']['detail']['admin_username']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                $content = $this->input->post('news_content');
                $content_dua = $this->input->post('news_content_dua');
                $label_1 = ($this->input->post('news_label_1') != '') ? $this->input->post('news_label_1') : '-';
                $label_2 = ($this->input->post('news_label_2') != '') ? $this->input->post('news_label_2') : '-';
                $label_3 = ($this->input->post('news_label_3') != '') ? $this->input->post('news_label_3') : '-';

                $news_date = ($this->input->post('news_date') != '') ? date('Y-m-d', strtotime($this->input->post('news_date'))) : date('Y-m-d');
                $news_time = ($this->input->post('news_time') != '') ? date('H:i:s', strtotime($this->input->post('news_time'))) : date('H:i:s');

                $columns = array(
                    'news_title' => function_lib::clear_special_character($this->input->post('news_title', true)),
                    'news_permalink' => $this->mainModel->generate_permalink($title),
                    'news_content' => htmlentities($content),
                    'news_content_2' => htmlentities($content_dua),
                    'news_input_by' => $admin_username,
                    'news_label_1' => htmlentities($label_1),
                    'news_label_2' => htmlentities($label_2),
                    'news_label_3' => htmlentities($label_3),
                    'news_external_link' => $this->input->post('news_external_link', true),
                    'news_external_link_label' => $this->input->post('news_external_link_label', true),
                    'news_meta_description' => substr($this->input->post('news_meta_description', true), 0, 190),
                    'news_meta_tags' => substr($this->input->post('news_meta_tags', true), 0, 190),
                    'news_timestamp' => $news_date . ' ' . $news_time,
                    'news_input_datetime' => date('Y-m-d H:i:s'),
                    'news_update_datetime' => date('Y-m-d H:i:s'),
                    'news_is_publish' => $this->input->post('status_publish', true),
                    'news_harga' => intval($this->input->post('news_harga', true)),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved = $this->mainModel->save();
                $status = $hasSaved['status']; //status saat penyimpanan
                $msg = strip_tags(base64_encode($hasSaved['message'])); //status saat penyimpanan
                if ($status == 200) {
                    $id = $this->function_lib->insert_id();
                    //stored category
                    $this->mainModel->store_category_to_news($id);

                    //handle logo
                    //            $logo=$this->mainModel->handleUpload($id,'news_photo');

                    $logo = $this->mainModel->upload_more_images($id);
                    /* if($logo['status']!=200)
                      {
                      $status=$logo['status'];
                      $message=$logo['message'];
                      $msg=strip_tags(base64_encode($message));

                      //   redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.$msg);
                      } */

                    $response_upload = $this->mainModel->save_file($id);
                    /* if($response_upload['status']!=200)
                      {
                      $status=$response_upload['status'];
                      $msg=strip_tags(base64_encode($response_upload['message']));
                      } */

                    if ($this->input->post('save')) {
                        redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=200&msg=' . base64_encode('Data berhasil disimpan.'));
                    } else {
                        redirect(base_url() . 'admin/' . $this->currentModule . '/tab_content/' . $id . '?status=200&msg=' . base64_encode('Data berhasil disimpan.'));
                    }
                }
            }
            //$this->session->set_flashdata('confirmation', '<div class="confirmation alert alert-danger">'.$message.'</div>');
        }

        //list category
        $this->data['list_news_category'] = $this->mainModel->list_news_category();

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id = 0) {
        $status = (isset($_GET['status']) AND trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) AND trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Konten';
        $description = 'Ubah Konten';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );

        $breadcrumbs_array[] = array(
            'name' => 'Master Konten',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/master_content/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Ubah',
            'class' => "clip-pencil",
            'link' => base_url() . 'admin/master_content/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);


        if (!is_numeric($id) OR $id == 0) {
            show_error('Request anda tidak valid', 500);
        }

        $rowMenu = $this->mainModel->find('news_id=' . intval($id));
        if (empty($rowMenu)) {
            show_error('Request anda tidak valid', 500);
        }

        foreach ($rowMenu AS $variable => $value) {
            $this->data[$variable] = $value;
        }

        if ($this->input->post('save') OR $this->input->post('save_return')) {
            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation($id);
            $status = $isValidationPassed['status'];
            $message = ($status == 500 AND $isValidationPassed['message'] == '') ? validation_errors() : $isValidationPassed['message'];

            if ($status == 200) {
                $title = $this->input->post('news_title');
                $admin_username = isset($_SESSION['admin']['detail']['admin_username']) ? $_SESSION['admin']['detail']['admin_username'] : 'admin';
                $content = $this->input->post('news_content');
                $content_dua = $this->input->post('news_content_dua');
                $label_1 = ($this->input->post('news_label_1') != '') ? $this->input->post('news_label_1') : '-';
                $label_2 = ($this->input->post('news_label_2') != '') ? $this->input->post('news_label_2') : '-';
                $label_3 = ($this->input->post('news_label_3') != '') ? $this->input->post('news_label_3') : '-';
                $news_date = ($this->input->post('news_date') != '') ? date('Y-m-d', strtotime($this->input->post('news_date'))) : date('Y-m-d');
                $news_time = ($this->input->post('news_time') != '') ? date('H:i:s', strtotime($this->input->post('news_time'))) : date('H:i:s');

                $columns = array(
                    'news_title' => function_lib::clear_special_character($this->input->post('news_title', true)),
                    // 'news_permalink'=>$this->mainModel->generate_permalink($title),
                    'news_content' => htmlentities($content),
                    'news_content_2' => htmlentities($content_dua),
                    'news_input_by' => $admin_username,
                    'news_label_1' => function_lib::clear_special_character(htmlentities($label_1)),
                    'news_label_2' => htmlentities($label_2),
                    'news_label_3' => htmlentities($label_3),
                    'news_external_link' => $this->input->post('news_external_link', true),
                    'news_external_link_label' => function_lib::clear_special_character($this->input->post('news_external_link_label', true)),
                    'news_meta_description' => function_lib::clear_special_character(substr($this->input->post('news_meta_description', true), 0, 190)),
                    'news_meta_tags' => function_lib::clear_special_character(substr($this->input->post('news_meta_tags', true), 0, 190)),
                    'news_timestamp' => $news_date . ' ' . $news_time,
                    'news_update_datetime' => date('Y-m-d H:i:s'),
                    'news_is_publish' => $this->input->post('status_publish', true),
                    'news_harga' => $this->input->post('news_harga', true),
                );

                $this->mainModel->setPostData($columns);
                $where = 'news_id=' . intval($id);
                $hasSaved = $this->mainModel->save($where);
                $status = $hasSaved['status']; //status saat penyimpanan
                $msg = base64_encode($hasSaved['message']); //status saat penyimpanan
                if ($status == 200) {

                    //store kategori
                    $this->mainModel->store_category_to_news($id);

                    $msg = base64_encode('Data berhasil disimpan');
                    //handle logo
                    //$logo=$this->mainModel->handleUpload($id,'news_photo');
                    $logo = $this->mainModel->upload_more_images($id);
                    /* if($logo['status']!=200)
                      {
                      $status=$logo['status'];
                      $message=strip_tags($logo['message']);
                      $msg=base64_encode('Terjadi error saat upload file. '.$message);
                      } */

                    $response_upload = $this->mainModel->save_file($id);
                    /* if($response_upload['status']!=200 AND $response_upload['status']!='')
                      {
                      $status=$response_upload['status'];
                      $msg=strip_tags(base64_encode($response_upload['message']));


                      } */

                    if ($this->input->post('save')) {
                        redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=' . $status . '&msg=' . $msg);
                    } else if ($this->input->post('save_return')) {
                        redirect(base_url() . 'admin/' . $this->currentModule . '/tab_content/' . $id . '?status=200&msg=' . base64_encode('Data berhasil disimpan.'));
                    }
                }
            }
        }
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['pathImgArr'] = $this->pathImgArr;

        $results = $this->mainModel->get_more_property($id);

        $this->data['images_arr'] = $results['images'];

        $this->data['footerScript'] = $this->footerScript(__FUNCTION__, array(
            'count_img' => count($this->data['images_arr'])
        ));

        //list category
        $this->data['list_news_category'] = $this->mainModel->list_news_category();
        $this->data['list_files'] = $this->mainModel->list_files($id);

        //$this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    public function delete_more_images($id = null, $product_id = null) {
        $file = $this->function_lib->get_one('news_more_images_file', 'site_news_more_images', 'news_more_images_id=' . intval($id));
        $status = 500;
        $msg = base64_encode('Gambar tidak ditemukan.');

        if (file_exists(FCPATH . $file) AND trim($file) != '') {
            unlink(FCPATH . $file);
        }
        $this->db->query('DELETE FROM site_news_more_images WHERE news_more_images_id=' . intval($id));
        $status = 200;
        $msg = base64_encode('Gambar berhasil dihapus.');
        redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $product_id . '?status=' . $status . '&msg=' . $msg);
    }

    /**
     * hapus data
     * @param int $id
     * @param $output
     */
    public function delete_image($id = 0, $output = 'redirect') {
        if (!is_numeric($id) OR $id == 0) {
            show_error('Request anda tidak valid', 500);
        }
        $where = 'news_id=' . intval($id);
        $this->data['rowAdmin'] = $this->mainModel->find($where);

        if (!empty($this->data['rowAdmin'])) {
            $sql = 'UPDATE site_news SET news_photo="-" WHERE ' . $where;
            $this->db->query($sql);
            redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=200&msg=' . base64_encode('Gambar berhasil dihapus'));
        } else {
            $msg = 'Data tidak ditemukan';
            redirect(base_url() . 'admin/' . $this->currentModule . '/update/' . $id . '?status=500&msg=' . base64_encode($msg));
        }
    }

    public function act_delete() {
        $arr_output = array();
        $arr_output['status'] = 500;
        $arr_output['message'] = 'Tidak ada aksi yang dilakukan';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                    $additional_where = ' '; //default telah ngecek data dihapus
                    if ($this->available_data($id, $additional_where)) {
                        $this->mainModel->setMainTable('site_news');
                        $this->mainModel->delete('news_id=' . intval($id));

                        $this->mainModel->setMainTable('site_news_relation');
                        $this->mainModel->delete('news_id=' . intval($id));
                        $deleted_count++;
                    }
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['status'] = 200;
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['status'] = 500;
            }
        }

        echo json_encode($arr_output);
    }

    public function delete($id = '') {
        $arr_output = array();
        $arr_output['message'] = 'Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 500;
        $additional_where = ' '; //default telah ngecek data dihapus
        if ($this->available_data($id, $additional_where)) {
            $this->mainModel->setMainTable('site_news');
            $this->mainModel->delete('news_id=' . intval($id));

            $this->mainModel->setMainTable('site_news_relation');
            $this->mainModel->delete('news_id=' . intval($id));
            $arr_output['message'] = ' Konten telah dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

    public function delete_category($news_id = 0, $category_id = 0) {
        $arr_output = array();
        $arr_output['message'] = 'Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 500;
        $additional_where = ' '; //default telah ngecek data dihapus

        $this->mainModel->setMainTable('site_news_relation');
        $this->mainModel->delete('news_id=' . intval($news_id) . ' AND news_category_id=' . intval($category_id));
        $arr_output['message'] = ' Kategori telah dihapus.';
        $arr_output['status'] = 200;
        echo json_encode($arr_output);
    }

    public function delete_tab($news_id = 0, $tab_id = 0) {

        $arr_output = array();
        $this->mainModel->setMainTable('site_news_tab');
        $this->mainModel->delete('tab_news_id=' . intval($news_id) . ' AND tab_id=' . intval($tab_id));
        $arr_output['message'] = ' Tab telah dihapus.';
        $arr_output['status'] = 200;

        redirect(base_url() . 'admin/' . $this->currentModule . '/tab_content/' . $news_id . '?status=200&msg=' . base64_encode($arr_output['message']));
    }

    /**
      pengecekkan ketersediaan data apakah telah dihapus atau belum
      dan dapat ditambahkan parameter lainnya
     */
    protected function available_data($id, $additional_where = '') {

        $result = false;
        $scalar = $this->function_lib->get_one('news_id', 'site_news', 'news_id=' . intval($id) . ' AND news_is_deleted="Y" ' . $additional_where);
        if ($scalar) {
            $result = true;
        }

        return $result;
    }

    /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
     * @param string $method
     * @param array $extraVariable
     * * */
    protected function footerScript($method = '', $extraVariable = array()) {

        //set variable
        if (!empty($extraVariable)) {
            extract($extraVariable);
        }

        ob_start();
        ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/jquery-ui.css" />
        <script src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>
        <!-- flexigrid starts here -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
        <!-- flexigrid ends here -->
        <link href="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>

        <link rel="stylesheet" href="<?php echo $this->themeUrl; ?>assets/plugins/datepicker/css/datepicker.css">
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>


        <link rel="stylesheet" href="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>


        <script type="text/javascript">
            var UIModals = function() {
                //function to initiate bootstrap extended modals
                var initModals = function() {
                    $.fn.modalmanager.defaults.resize = true;
                    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
                            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                            '<div class="progress progress-striped active">' +
                            '<div class="progress-bar" style="width: 100%;"></div>' +
                            '</div>' +
                            '</div>';

                };
                return {
                    init: function() {
                        initModals();

                    }
                };
            }();

            function add_modal()
            {
                var $modal = $('#ajax-modal');
                // create the backdrop and wait for next modal to be triggered
                $('body').modalmanager('loading');

                setTimeout(function() {
                    $modal.load('<?php echo base_url('admin/' . $this->currentModule . '/load_external_link') ?>', '', function() {
                        $modal.modal();
                    });
                }, 100);
            }</script>
        <script type="text/javascript">
            /*
             ambil link
             */
            function load_link_from_content(link, preview_link)
            {
                var current_label = $("#link_label").val();
                $("#link").val(link);
                if ($.trim(current_label) == '')
                {
                    $("#link_label").val('Selengkapnya');
                }
                $('#ajax-modal').modal('toggle');
                $("#preview_link").html('<a href="' + preview_link + '" target="_blank"><b>Preview</b></a>');
                $("#link").focus();

                return false;
            }


            var current_index = parseInt($("#num_files").val());
            function add_input_file(elem)
            {
                var parent_before = elem.parent().parent();
                var html = '<div class="form-group">'
                        + '<label class="col-sm-2 control-label" for="form-field-1">'
                        + '</label>'
                        + '<div class="col-sm-4">'
                        + '<a href="#" onclick="remove_input_file($(this));return false;" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>'
                        + '      <input type="hidden" value="0" name="download_file_id[' + current_index + ']">'
                        + '      <input style="width:80%;float:right;" class="form-control" type="file" name="download_file_' + current_index + '"/>'
                        + '</div>'
                        + '<div class="col-sm-4">'
                        + '<input class="form-control" placeholder="Label file" type="text" name="download_title[' + current_index + ']"/>'
                        + '</div>'
                        + '</div>';
                current_index++;
                parent_before.before(html);
            }

            function remove_input_file(elem)
            {
                var parent = elem.parent().parent();
                if (confirm('Hapus?'))
                {
                    parent.slideUp('medium', function() {
                        parent.remove();
                    });
                }
                return false;
            }

            function delete_row(elem)
            {
                var id_tr = elem.attr('id');
                if (confirm('Hapus?'))
                {
                    $("tr#tr_" + id_tr).slideUp('medium', function() {
                        $(this).remove();
                    });
                    return false;
                }
                return false;
            }

            var no_tr_more_images =<?php echo isset($count_img) ? ($count_img + 1) : '1' ?>;
            function add_row_table_more_images() {


                var str = '<tr class="tr_more_images_' + no_tr_more_images + ' item_transaction"  id="tr_more_images_' + no_tr_more_images + '">'
                        + '  <td class="center"><a href="#" class="btn btn-sm btn-danger delete" onclick="delete_row($(this));return false;" id="more_images_' + no_tr_more_images + '"><i class="fa fa-trash-o"></i></a></td>'
                        + '  <td class="center td_product_criteria_detail_id">'
                        + '      <input type="file" class="form-control" name="news_more_images_file' + no_tr_more_images + '" />'
                        + '  </td>'

                        + ' </tr>';

                $("table#more_image > tbody").fadeIn('medium', function() {
                    $("table#more_image > tbody").append(str);
                });
                no_tr_more_images++;


                return false;
            }

            function check_same_category(elem)
            {
                var current_id = elem.val();
                var current_class = 'category_id_' + current_id;
                if (elem.prop('checked') == true)
                {
                    $("." + current_class).attr('checked', true);
                } else
                {
                    $("." + current_class).attr('checked', false);
                }
            }


            var no_tab_content =<?php echo isset($count_tab_content) ? ($count_tab_content + 1) : '1' ?>;
            function add_tab_content(elem) {
                var old_label = elem.html();
                elem.attr('disabled', true);
                elem.html('processing...');
                $.get('<?php echo base_url() ?>admin/master_content/load_form_tab/' + no_tab_content, function(response) {
                    $("div.div_tab_content").fadeIn('medium', function() {
                        $("div.div_tab_content").append(response);
                    });
                    elem.html(old_label);
                    elem.attr('disabled', false);
                    $('input[name="tab_title[' + no_tab_content + ']"]').focus();
                });
                no_tab_content++;

                /*
                 var str= '<div class="form-group form_tab_content_'+no_tab_content+'">'
                 +'<label class="col-sm-2 control-label" for="form-field-1">'
                 +'    Judul Tab 1 <span class="symbol required"/>'
                 +'</label>'
                 +'<div class="col-sm-10">'
                 +'    <input type="text" class="form-control" name="tab_title" value="" />'
                 +'</div>'
                 +'</div>'
                 +'<div class="form-group">'
                 +'<label class="col-sm-2 control-label" for="form-field-1">'
                 +'    Isi Tab 1<span class="symbol required"/>'
                 +'</label>'
                 +'<div class="col-sm-10"><?php //echo form_textarea_tinymce('tab_content', '', 'Standard');                          ?></div>'
                 +'</div>';
                 */




                return false;
            }
            function remove_tab_content(index)
            {

                $(".div_form_tab_content_" + index).slideUp('medium', function() {
                    $(".div_form_tab_content_" + index).remove();
                });

                return false;
            }

            $(function() {
                $('.date-picker').datepicker({
                    autoclose: true
                });

                $('.time-picker').timepicker();
            });</script>


        <?php
        switch ($method) {
            case 'index':
                ?>

                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            {display: 'No', name: 'no', width: 50, sortable: false, align: 'right'},
                            {display: 'Edit', name: 'edit', width: 50, sortable: false, align: 'center'},
                            {display: 'Delete', name: 'delete', width: 50, sortable: false, align: 'center'},
                            {display: 'Publish', name: 'status_publish', width: 50, sortable: false, align: 'center'},
                            {display: 'Judul', name: 'title', width: 160, sortable: false, align: 'left'},
                            {display: 'Isi', name: 'content', width: 560, sortable: false, align: 'left'},
                            {display: 'Meta Deskripsi', name: 'meta_description', width: 200, sortable: false, align: 'left'},
                            {display: 'Meta Tags', name: 'meta_tags', width: 200, sortable: false, align: 'left'},
                            {display: 'Gambar', name: 'photo', width: 80, sortable: false, align: 'center'},
                            {display: 'Diinput Oleh', name: 'input_by', width: 80, sortable: false, align: 'center'},
                        ],
                        buttons: [
                            {display: 'Input Data', name: 'add', bclass: 'add', onpress: add},
                            {separator: true},
                            {display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check},
                            {separator: true},
                            {display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check},
                            {separator: true},
                            {display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete},
                        ],
                        buttons_right: [
                            //                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],

                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false,
                        nowrap: false,
                    });

                    function act_delete(com, grid) {
                        var div_alert = $("div.alert");

                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if ($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if (conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url() . 'admin/' . $this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        div_alert.slideDown('medium');
                                        if (response['status'] != 200)
                                        {
                                            div_alert.addClass('alert-danger');
                                            div_alert.removeClass('alert-success');
                                        } else
                                        {
                                            div_alert.removeClass('alert-danger');
                                            div_alert.addClass('alert-success');
                                        }
                                        div_alert.html(response['message']);
                                    },
                                    error: function() {
                                        alert('an error has occurred, please try again');
                                        grid_reload();
                                    }
                                });
                            }
                        }
                    }


                    $(document).ready(function() {
                        grid_reload();
                    });

                    function reset_pencarian()
                    {
                        $("select#news_category_id").val('');
                        $("input#news_title").val('');
                        $("input#news_content").val('');
                        grid_reload();
                    }

                    function grid_reload() {
                        var news_category_id = $("select#news_category_id").val();
                        var news_title = $("input#news_title").val();
                        var news_content = $("input#news_content").val();
                        var link_service = '?news_category_id=' + news_category_id + '&news_title=' + news_title + '&news_content=' + news_content;
                        $("#gridview").flexOptions({url: '<?php echo base_url() . 'admin/' . $this->currentModule; ?>/get_data' + link_service}).flexReload();
                    }

                    function delete_transaction(id)
                    {
                        var div_alert = $("div.alert");
                        if (confirm('Delete?'))
                        {
                            var jqxhr = $.ajax({
                                url: '<?php echo base_url() . 'admin/' . $this->currentModule ?>/delete/' + id,
                                type: 'get',
                                dataType: 'json',

                            });
                            jqxhr.success(function(response) {
                                div_alert.slideDown('medium');
                                if (response['status'] != 200)
                                {
                                    div_alert.addClass('alert-danger');
                                    div_alert.removeClass('alert-success');
                                } else
                                {
                                    div_alert.removeClass('alert-danger');
                                    div_alert.addClass('alert-success');
                                }
                                div_alert.html(response['message']);
                                grid_reload();
                                return false;

                            });
                            jqxhr.error(function() {
                                alert('an error has occurred, please try again.');
                                grid_reload();
                                return false;
                            });
                        }
                        return false;

                    }

                    function delete_category(id, category_id)
                    {
                        var div_alert = $("div.alert");
                        if (confirm('Hapus?'))
                        {
                            var jqxhr = $.ajax({
                                url: '<?php echo base_url() . 'admin/' . $this->currentModule ?>/delete_category/' + id + '/' + category_id,
                                type: 'get',
                                dataType: 'json',

                            });
                            jqxhr.success(function(response) {
                                div_alert.slideDown('medium');
                                if (response['status'] != 200)
                                {
                                    div_alert.addClass('alert-danger');
                                    div_alert.removeClass('alert-success');
                                } else
                                {
                                    div_alert.removeClass('alert-danger');
                                    div_alert.addClass('alert-success');
                                }
                                div_alert.html(response['message']);
                                grid_reload();
                                return false;

                            });
                            jqxhr.error(function() {
                                alert('an error has occurred, please try again.');
                                grid_reload();
                                return false;
                            });
                        }
                        return false;

                    }

                    function search_by_category($category_id)
                    {
                        $("select#news_category_id").val($category_id);
                        grid_reload();
                        return false;
                    }
                </script>
                <?php
                break;
        }

        $footerScript = ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }

}
?>
