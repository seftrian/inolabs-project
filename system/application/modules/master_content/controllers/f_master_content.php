<?php

class f_master_content extends front_controller
{
    protected $mainModel;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model(array('master_content_lib', 'master_content_category/master_content_category_lib', 'master_file/master_file_lib'));
        $this->load->helper(array('pagination'));

        $this->mainModel = new master_content_lib;

        $this->pathImgArr = $this->mainModel->getImagePath();
    }
    /** 
        * bagian menu info
        * @author seftrian yudha pamungkas
    */

    public function contact()
    {
        $this->data['seotitle'] = "Kontak Kami";
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }


    public function info()
    {
        $this->data['seotitle'] = "Info";
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    public function getJsonFileInfo()
    {
        $where ='site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "info")';
        $sql ='SELECT * FROM site_news INNER JOIN site_news_relation ON site_news.news_id = site_news_relation.news_id INNER JOIN site_download ON site_news.news_id = site_download.download_news_id '
                .' WHERE '.$where.' ORDER BY site_news.news_id DESC LIMIT 1';
        $exec = $this->db->query($sql);
        $results = $exec->result_array();
        $data = array(
                    'status' => "200",
                    'label' => "Surat Himbauan - Management Zeelora Global Indonesia",
                    'data' => $results,
            );
        echo json_encode($data);
    }

    public function kode_etik()
    {
        $this->data['seotitle'] = "Kode Etik";
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    public function getJsonFileEtik()
    {
        $where ='site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "kode_etik")';
        $sql ='SELECT * FROM site_news INNER JOIN site_news_relation ON site_news.news_id = site_news_relation.news_id INNER JOIN site_download ON site_news.news_id = site_download.download_news_id '
            .' WHERE '.$where.' ORDER BY site_news.news_id DESC LIMIT 1';
        $exec = $this->db->query($sql);
        $results = $exec->result_array();
        $data = array(
                'status' => "200",
                'label' => "Kode Etik Zeelora",
                'data' => $results,
        );
        echo json_encode($data);
    }

    public function mark_plan()
    {
        $this->data['seotitle'] = 'Marketing Plan';
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    public function markPlan()
    {
        $where ='site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "marketing_plan")';
        $sql = 'SELECT * FROM site_news INNER JOIN site_news_relation ON site_news.news_id = site_news_relation.news_id INNER JOIN site_download ON site_news.news_id = site_download.download_news_id '
             . ' WHERE ' . $where . 'ORDER BY site_news.news_id';
        $results = $this->db->query($sql);
        $results = $results->result_array();
        $data = array(
            'status' => "200",
            'label' => 'Marketing Plan Zeelora',
            'data' => $results,
        );
        echo json_encode($data);
    }


    public function detail($news_permalink = '')
    {
        $this->data['seotitle'] = "Info";
        $this->data['themeUrl'] = $this->themeUrl;
        $news_permalink = $this->security->sanitize_filename($news_permalink);
        $this->data['row_array'] = $this->mainModel->get_news_detail_from_permalink($news_permalink);
        $this->data['pathImgArr'] = $this->mainModel->getImagePath();
        $this->data['news_title'] = $this->data['row_array']['news_title'];
        $this->mainModel->setMainTable('site_download');
        $where = 'download_news_id=' . intval($this->data['row_array']['news_id']);
        $this->data['download_arr'] = $this->mainModel->getSiteDownloadOneDes($where, 1, 'download_id DESC');
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    
    public function jsonListProduk()
    {
        $where ='site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink="product")';
        $sql = 'SELECT * FROM site_news INNER JOIN site_news_relation  ON site_news.news_id = site_news_relation.news_id WHERE '. $where .' ORDER BY site_news_relation.news_category_id DESC LIMIT 4 ';
        $results = $this->db->query($sql);
        $results = $results->result_array();
        $data = array(
            'sts' => '200',
            'alert-success' => "Data produk berhasil di ambil...",
            'data' => $results
       );
        echo json_encode($data);
    }


    public function product()
    {
        $this->data['seotitle'] = "Produk";
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }


    public function detail_product($product_id = 0)
    {
        if (intval($product_id) <= 0) {
            redirect(base_url('product?status=500&msg' . base64_encode('Produk tidak ditemukan')));
        }
        $seotitle = $this->mainModel->get_news_title($product_id);
        $this->data['cur_title'] = "Detail Produk " . $seotitle->news_title;
        $this->data['name_produk'] = "$seotitle->news_title";
        $this->data['produk_id'] = $product_id;
        $this->data['themeUrl'] = $this->themeUrl;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    public function List_data_with_menu($category_permalink = '')
    {
        if (trim($category_permalink) == '') {
            show_error('Halaman tidak ditemukan', 404);
        }
        $title = $category_permalink;
        $desc = $category_permalink;
        $key = $category_permalink;
        $this->seo($title, $desc, $key);
        template($this->themeId, get_class() . '/' . __FUNCTION__);
    }
    
    /****************************************************************** */
    

    public function default_controller($query = '')
    {
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    public function seo($title, $desc, $key, $img = '')
    {
        $seoFunc = function_lib::make_seo($title, $desc, $key);
        $this->data['seoTitle'] = $seoFunc['title'];
        $this->data['seoDesc'] = $seoFunc['desc'];
        $this->data['seoKeywords'] = $seoFunc['keywords'];
        $this->data['img_content'] = $img;
        $this->data['current_url'] = base_url() . uri_string();
    }
}
