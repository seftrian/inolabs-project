<?php

class widget_detail_slider_product extends widget_lib
{
    public function run($module, $class_widget, $limit = 1)
    {   
       
        $this->load->model(array('master_content/master_content_lib','master_content_category/master_content_category_lib'));
        $lib = new master_content_category_lib;
        $data = array();
        $get_url_id_product = $this->uri->segment(3);
        $sql = 'SELECT site_news.news_title, site_news.news_id '
                .'FROM site_news '
                .'INNER JOIN site_news_relation '
                .'ON site_news.news_id = site_news_relation.news_id ' 
                .'WHERE site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "product") AND site_news.news_id= ' . $get_url_id_product  . ' ORDER BY site_news.news_id DESC LIMIT ' . $limit . '';
        $data['site_news'] = $this->db->query($sql)->result_array();
        $sql_1 = 'SELECT download_id, download_news_id, download_file, download_title '
            .'FROM site_download '
            .'WHERE download_news_id = "'.$data['site_news'][0]['news_id'].'" ORDER BY download_id DESC LIMIT 4';
        $data['slider'] = $this->db->query($sql_1)->result_array();
        $this->render(get_class(), $data);
    }
}
