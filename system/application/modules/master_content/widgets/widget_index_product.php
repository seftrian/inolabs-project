<?php

class widget_index_product extends widget_lib {

    /*
            * @author yudha
            * get data image 
    */
    public function run($module, $class_widget, $limit = 4) {
        $this->load->model(array('master_content/master_content_lib', 'master_content_category/master_content_category_lib'));
        $lib = new master_content_category_lib;
        $data = array();

        $where = 'site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "product")';
        $sql = 'SELECT * FROM site_news INNER JOIN site_news_relation ON site_news.news_id = site_news_relation.news_id WHERE ' . $where . ' ORDER BY site_news.news_id DESC LIMIT ' . $limit . '';
        $exec = $this->db->query($sql);
        $data['results'] = $exec->result_array();
        foreach ($data['results'] as $key => $val) {
            $data['imgSub'][$key] = $this->db->query('SELECT * FROM site_news_more_images WHERE news_more_images_news_id = '.$val['news_id'].' ORDER BY news_more_images_id DESC')->row_array();
        }
        $data['pathImgArr'] = $lib->getImagePath();
        $this->render(get_class(), $data);
    }

}
