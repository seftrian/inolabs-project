<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_list_news extends widget_lib{
    //put your code here
    public function run($module,$class_widget,$limit=6,$category_permalink)
    {
        $this->load->model(array('master_content/master_content_lib','master_content_category/master_content_category_lib'));
        $lib=new master_content_category_lib;

        $data=array();

        $total_rows=$lib->all_with_join($category_permalink)->num_rows();
        $paginationArr=create_pagination('list_data/berita/'.$category_permalink.'/', $total_rows, $limit);

        $data['offset']=$paginationArr['limit'][1];
        $data['results']=$this->master_content_lib->get_data_news_category($category_permalink,$limit,$data['offset'],$order_by='news_id ASC');

        $data['link_pagination']=$paginationArr['links'];
        $data['pathImgArr']=$lib->getImagePath();
        $data['category_permalink']=$category_permalink;
        $data['category_name']=ucwords(str_replace('_', ' ', $category_permalink));
        // echo "<pre>";
        // print_r($data);
        $this->render(get_class(),$data);
    }
}

?>
