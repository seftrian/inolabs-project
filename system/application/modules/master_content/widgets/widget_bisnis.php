<?php

class widget_bisnis extends widget_lib {

    public function run($module, $class_widget, $limit = 0) {
        $this->load->model(array('master_content/master_content_lib', 'master_content_category/master_content_category_lib'));
        $lib = new master_content_category_lib;
        $data = array();
        $this->render(get_class(), $data);
    }

}
