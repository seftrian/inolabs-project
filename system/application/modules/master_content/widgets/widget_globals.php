<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_globals extends widget_lib{
    //put your code here
    public function run($module,$class_widget,$category_permalink='',$news_permalink='')
    {
        // echo $category_permalink; //footer
        // echo $news_permalink; //contact us

        $this->load->model(array('master_content/master_content_lib','master_content_category/master_content_category_lib'));
        $lib=new master_content_category_lib;

        $data=array();
        $where = 1;
        if (!empty($category_permalink)) {
            $where.=' AND site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "'.$category_permalink.'")';
        }
        if (!empty($news_permalink)) {
            $where.=' AND news_permalink = "'.$news_permalink.'"';
        }
        $sql='SELECT * FROM site_news INNER JOIN site_news_relation ON site_news.news_id = site_news_relation.news_id WHERE '.$where;
        $exec=$this->db->query($sql);
        $data['rowData']=$exec->row();
        $data['pathImgArr']=$lib->getImagePath();
        $this->render(get_class(),$data);
    }
}

?>
