<?php
class widget_footer_menu extends widget_lib
{
    public function run($module, $class_widget, $limit = 6)
    {
        $data = array();
        $where = 'LOWER(menu_administrator_location) = "user" AND menu_administrator_is_active ="1"';
        $sql = 'SELECT * '
             .'FROM site_administrator_menu WHERE ' . $where . ' ORDER BY menu_administrator_order_by';
        $exec = $this->db->query($sql);
        $data['results'] = $exec->result_array();
        $this->render(get_class(), $data);
    }
}