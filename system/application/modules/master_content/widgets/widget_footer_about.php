<?php

class widget_footer_about extends widget_lib
{
    public function run($module, $class_widget, $limit = 1)
    {
        $this->load->model(array('master_content/master_content_lib', 'master_content_category/master_content_category_lib'));
        $lib = new master_content_category_lib;
        $data = array();
        $where='site_news_relation.news_category_id IN (SELECT category_id FROM site_news_category WHERE category_permalink = "profil")';
        $sql='SELECT * FROM site_news INNER JOIN site_news_relation ON site_news.news_id = site_news_relation.news_id WHERE '.$where.' ORDER BY site_news.news_id DESC LIMIT '.$limit.'';
        $exec=$this->db->query($sql);
        $data['results']=$exec->result_array();
        $data['pathImgArr']=$lib->getImagePath();
        $this->render(get_class(), $data);
    }
}
