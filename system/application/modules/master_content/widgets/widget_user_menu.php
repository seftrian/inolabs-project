<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_user_menu extends widget_lib {

    //put your code here
    public function run($module, $class_widget, $limit = 9) {
        $this->load->model('master_content_category/master_content_category_lib');
        $lib = new master_content_category_lib;

        $data = array();
        $where = 'lower(menu_administrator_location)="user" AND menu_administrator_is_active="1"';
        $sql = 'SELECT * FROM site_administrator_menu
              WHERE ' . $where . '
              ORDER BY menu_administrator_order_by';
        $exec = $this->db->query($sql);
        $data['results'] = $exec->result_array();
        $data['pathImgArr'] = $lib->getImagePath();

        $sql = 'SELECT * FROM site_configuration';
        $exec = $this->db->query($sql);
        $data['results_config'] = $exec->result_array();

        $data['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
        $data['base_url'] .= "://" . $_SERVER['HTTP_HOST'];
        $data['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
        $data['url'] = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $this->render(get_class(), $data);
    }

}

?>
