<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('admin_config_lib');
        $this->mainModel=new admin_config_lib;

    }
    /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data_schedule_dokter($id)
    {

        $result=false;
        $scalar=$this->function_lib->get_one('schedule_id','sys_schedule_dokter','schedule_id='.intval($id));
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }
    public function act_delete_schedule_dokter() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {
                        if($this->available_data_schedule_dokter($id))
                        {    
                            $this->mainModel->setMainTable('sys_schedule_dokter');
                            $this->mainModel->delete('schedule_id='.intval($id));

                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }
    /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data_schedule_patient($id)
    {

        $result=false;
        $scalar=$this->function_lib->get_one('schedule_id','sys_schedule','schedule_id='.intval($id));
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }
    public function act_delete_schedule_patient() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {
                        if($this->available_data_schedule_patient($id))
                        {    
                            $this->mainModel->setMainTable('sys_schedule');
                            $this->mainModel->delete('schedule_id='.intval($id));

                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

    public function get_data_schedule_dokter(){
        $params = isset($_POST) ? $_POST : array();
        $where = 'DATE(schedule_timestamp)="'.date('Y-m-d').'"';

        $params['table'] = 'sys_schedule_dokter';
        $params['where'] = $where;

        $params['order_by'] = "
            schedule_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;

            $schedule_time = date('H:i',strtotime($schedule_datetime_start)).' s/d '.date('H:i',strtotime($schedule_datetime_end));
            $entry = array('id' => $schedule_id,
                'cell' => array(
                    'no' =>  $no.'.',
                    'schedule_kind_service_name' => $schedule_kind_service_name,
                    'schedule_dokter_name' => $schedule_dokter_name,
                    'schedule_time' => $schedule_time,
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    public function get_data_schedule_patient(){
        $params = isset($_POST) ? $_POST : array();
        $where = 'DATE(schedule_datetime)="'.date('Y-m-d').'"';

        $params['table'] = 'sys_schedule';
        $params['where'] = $where;

        $params['order_by'] = "
            schedule_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;

            $entry = array('id' => $schedule_id,
                'cell' => array(
                    'no' =>  $no.'.',
                    'schedule_service_name' => $schedule_service_name,
                    'schedule_queue_value' => $schedule_queue_value,
                    'schedule_dokter_name' => $schedule_dokter_name,
                    'schedule_datetime' => convert_datetime_name($schedule_datetime,'id'),
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

	public function get_default_printer_nota()
	{
		$where='configuration_index="config_printer_name_nota"';
		$printer_name=$this->function_lib->get_one('configuration_value',"site_configuration",$where);
		$value=($printer_name=='')?'inofarma':$printer_name;
		$data=array(
			'label'=>$value,
			'name'=>str_replace(' ', '_', $value)
			);
		echo json_encode($data);
	}
}
