<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
class admin extends admin_controller{
    
    protected $mainModel;
    //put your code here
    public function __construct() {
        parent::__construct();
        //load model menu

        $this->load->model(array('admin_config_lib'));
   
        $this->mainModel=new admin_config_lib;
        
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }
    public function schedule_dokter(){
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';

        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template

        $this->data['seoTitle']='Jadwal Dokter Hari ini';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
      
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);

        $this->data['status']=$status;
        $this->data['message']=$message;

        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    public function schedule_patient(){
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';

        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template

        $this->data['seoTitle']='Data Antrian Hari ini';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
      
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);

        $this->data['status']=$status;
        $this->data['message']=$message;

        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    /**
     * untuk update data group via form
     * @param int $groupId
     */
    public function tagline()
    {
         $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if($this->input->post('save'))
        {
            
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
             $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
            if($status==200)
            {
                
                $columns=array(
                    'config_tagline_value'=>$this->input->post('config_tagline_value',true),
                    'config_tagline_link'=>$this->input->post('config_tagline_link',true),
                );
                
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $status=$hasSaved['status']; //status saat penyimpanan

                if($status==200)
                {
                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/admin_config/tagline?status=200&msg='.$msg);
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['rowConfig']=$this->mainModel->findAll(1,'no limit');
        
        //cek ketersediaan group admin
        if(empty($this->data['rowConfig']))
        {
            show_404();
        }
        
        //extract data
        foreach($this->data['rowConfig'] AS $rowConfig)
        {
            $index=$rowConfig['configuration_index'];
            $value=$rowConfig['configuration_value'];
            $this->data[$index]=isset($_POST[$index])?$_POST[$index]:$value;
        }
         
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template

        $this->data['seoTitle']='Tagline Website';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
      
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
            
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk update data group via form
     * @param int $groupId
     */
    public function update()
    {
        $message='';
        $status=200;
        if($this->input->post('save'))
        {
            
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
             $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
            if($status==200)
            {
                
                $columns=array(
                    'config_lat'=>$this->input->post('config_lat',true),
                    'config_lng'=>$this->input->post('config_lng',true),
                    'config_email'=>$this->input->post('config_email',true),
                    'config_name'=>$this->input->post('config_name',true),
                    'config_address'=>$this->input->post('config_address',true),
                    'config_city_name'=>$this->input->post('config_city_name',true),
                    'config_province_name'=>$this->input->post('config_province_name',true),
                    'config_country_name'=>$this->input->post('config_country_name',true),
                    'config_telephone'=>$this->input->post('config_telephone',true),
                    'config_description'=>$this->input->post('config_description',true),
                    'config_keywords'=>$this->input->post('config_keywords',true),
                    'config_footer'=>$this->input->post('config_footer',true),
                    'config_link_registrasi'=>$this->input->post('config_link_registrasi',true),
                    'config_link_login'=>$this->input->post('config_link_login',true),
                );
                
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $status=$hasSaved['status']; //status saat penyimpanan
                
                //handle logo
                $logo=$this->mainModel->handleUploadConfig('config_logo');
                 //handle favicon
                $favicon=$this->mainModel->handleUploadConfig('config_favicon');
                if($logo['status']!=200)
                {
                    $status=$logo['status'];
                    $message=$logo['message'];
                }
                elseif($favicon['status']!=200)
                {
                    $status=$favicon['status'];
                    $message=$favicon['message'];
                }
                
                if($status==200)
                {
                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/admin_config/view?status=200&msg='.$msg);
                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['rowConfig']=$this->mainModel->findAll(1,'no limit');
        
        //cek ketersediaan group admin
        if(empty($this->data['rowConfig']))
        {
            show_404();
        }
        
        //extract data
        foreach($this->data['rowConfig'] AS $rowConfig)
        {
            $index=$rowConfig['configuration_index'];
            $value=$rowConfig['configuration_value'];
            $this->data[$index]=isset($_POST[$index])?$_POST[$index]:$value;
        }
         
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template

        $this->data['seoTitle']='Ubah Konfigurasi';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Konfigurasi ',
                                    'class'=>"clip-wrench",
                                    'link'=>base_url().'admin/admin_config/view',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
            
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    
    /**
     * untuk update data group
     * @param int $groupId
     */
    public function view()
    {
        //status
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['rowConfig']=$this->mainModel->findAll(1,'no limit');
        if(empty($this->data['rowConfig']))
        {
            show_404();
        }
        //extract data
        foreach($this->data['rowConfig'] AS $rowConfig)
        {
            $index=$rowConfig['configuration_index'];
            $value=$rowConfig['configuration_value'];
            $this->data[$index]=isset($_POST['config_name'])?$_POST['config_name']:$value;
        }
        
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['status']=$status;
        $this->data['message']=$message;
         $this->data['seoTitle']=' Konfigurasi';
        $description='Judul app, deskripsi, logo, etc...';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );

         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-wrench",
                                    'link'=>'#',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }

      /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();

        ?>
        <style>
      .controls {
        margin-top: 40px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #searching-location {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #searching-location:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

    </style>
       <script type="text/javascript"
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN545yt6yNVekoDsxdschqI9XICmhQbSk&libraries=drawing,places">
         </script>
        <script type="text/javascript"
              src="<?php echo base_url()?>assets/js/gis/current_location.js">
             </script>

        <?php
        switch($method)
        {
            case 'update':
            ?>
             
            <?php
            break;
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
    
}

?>
