<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */


require_once dirname(__FILE__).'/../components/core_config_lib.php';
class admin_config_lib extends core_config_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_configuration';
    public function __construct() {
       
       parent::__construct();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));

        if(!session_id())
        {
            session_start();
        }
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation()
    {
         $status=500;
         $message='';
         $config = array(
               array(
                     'field'   => 'config_name',
                     'label'   => 'Nama',
                     'rules'   => 'trim|required|min_length[2]'
                  ),
               array(
                     'field'   => 'config_description',
                     'label'   => 'Deskripsi',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'config_keywords',
                     'label'   => 'Keywords',
                     'rules'   => 'trim|required'
                  ),  
             
            );
         
         $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save()
    {
        $status=500;
        $message='Error';
        
        if(!empty($this->postData))
        {
            foreach($this->postData AS $index=>$value)
            {
                $where=array(
                    'configuration_index'=>$index,
                );
                $column=array(
                    'configuration_value'=>$value,
                );
                $is_exist=$this->CI->function_lib->get_one('configuration_index',$this->mainTable,'configuration_index="'.$index.'"');
                if(!$is_exist)
                {
                    $column=array(
                    'configuration_index'=>$index,
                    'configuration_value'=>$value,
                    );
                    $this->CI->db->insert($this->mainTable,$column);
                }
                else
                {
                    $this->CI->db->update($this->mainTable,$column,$where);
                }
            }
            $status=200;
            $message='OK';
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * check ketersediaan data group
     * @param stirng $query 
     * @param int $groupId default 0 saat create
     * @return bool true is valid
     */
    public function isExistGroupName($query, $groupId=0)
    {
        $value=TRUE;
        //update
        if(is_numeric($groupId) AND $groupId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_group_title',$this->mainTable,'admin_group_title LIKE TRIM(\''.$query.'\') AND admin_group_id!='.  intval($groupId));
        }
        else//create
        {
            $isFounded=$this->CI->function_lib->get_one('admin_group_title',$this->mainTable,'admin_group_title LIKE TRIM(\''.$query.'\')');
        }
        
        if($isFounded)
        {
            $value=FALSE;
        }
        
        return $value;
    }
    
    /**
     * handle upload
     */
    public function handleUploadConfig($filesName)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
      
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($filesName);
            $status=$results['status'];
            $message=$results['message'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($field_name)
    {
        $config=array();
        $pathImage='assets/images/statics/';
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
             $where=array(
                'configuration_index'=>$field_name,
            );
            $column=array(
                'configuration_value'=>$pathImage.$dataImage['file_name'],
            );
            $this->CI->db->update($this->mainTable,$column,$where);
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors('<p>', '</p>');;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    public function setMainTable($table='')
    {
        $this->mainTable=$table;
    }
    
    
}

?>
