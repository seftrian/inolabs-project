<?php
$log_date='2019-05-09';
$add_config=$add_notification=$add_menu=$add_table=$add_column=$add_column_type=$add_value_on_enum_column=array();

$add_menu[$log_date]=array(
      array(
          'name'=>'Chat GM!',
          'query'=>"
			  INSERT INTO `site_administrator_menu` (`menu_administrator_id`, `menu_administrator_par_id`, `menu_administrator_title`, `menu_administrator_description`, `menu_administrator_link`, `menu_administrator_page_id`, `menu_administrator_order_by`, `menu_administrator_location`, `menu_administrator_is_active`, `menu_administrator_module`, `menu_administrator_controller`, `menu_administrator_method`, `menu_administrator_class_css`) VALUES
			(8, 11, 'Group', 'untuk konfigurasi group dan privilege group', 'admin/admin_group/index', 0, 1, 'admin', '1', 'admin_group', 'admin', 'index#create#update#set_privilege#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(9, 11, 'User', 'Untuk memanajemen user/admin', 'admin/admin_user/index', 0, 2, 'admin', '1', 'admin_user', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(10, 11, 'Menu Admin', 'Untuk memanajemen menu', 'admin/admin_menu/index', 0, 4, 'admin', '0', 'admin_menu', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(11, 0, 'Settings', 'settings group, user dan menu', '', 0, 24, 'admin', '1', '-', '-', '-', 'clip-cog-2'),
			(12, 11, 'Config', '', 'admin/admin_config/view', 0, 6, 'admin', '1', 'admin_config', 'admin', 'master_shift#update#view#', ''),
			(13, 11, 'Menu Visitor', 'menu visitor', 'admin/admin_menu/index?location=user', 0, 5, 'admin', '1', 'admin_menu', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(24, 0, 'Dashboard', 'dashboard', 'admin/dashboard/index', 0, 1, 'admin', '1', 'dashboard', 'admin', 'index', 'clip-home-3'),
			(46, 11, 'Privelege Module Label', '', 'admin/admin_group_menu_label/index', 0, 3, 'admin', '0', 'admin_group_menu_label', 'admin', 'index#index_filter#ign_index_filter', ''),
			(53, 0, 'Data Master', '-', '#', 0, 4, 'admin', '1', '-', '-', '-', 'clip-grid-5'),
			(58, 53, 'People', 'Kumpulan data master dokter, apoteker, karyawan, dan lainnya yang berkaitan dengan kependudukan.', 'admin/people/index', 0, 5, 'admin', '0', 'people', 'admin', 'index_person#index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
			(94, 53, 'Unit / Cabang IAI', '', 'admin/store/index', 0, 12, 'admin', '0', 'store', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
			(100, 97, 'Item Belum Expired', '', 'admin/stok_all/arus_stok_sisa', 0, 3, 'admin', '0', 'stok_all', 'admin', 'arus_stok_sisa#export_arus_stok_sisa_data', ''),
			(102, 97, 'Item Kadaluarsa', '', 'admin/stok_all/expired', 0, 5, 'admin', '0', 'stok_all', 'admin', 'expire#export_expired_data', ''),
			(118, 53, 'Kategori', '', 'admin/master_content_category/index', 0, 1, 'admin', '1', 'master_content_category', 'admin', 'index#create#update#delete#delete_image#', ''),
			(119, 53, 'Konten', '', 'admin/master_content/index', 0, 1, 'admin', '1', 'master_content', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', ''),
			(120, 53, 'Youtube', '', 'admin/master_youtube/index', 0, 3, 'admin', '0', 'master_youtube', 'admin', 'index#add#update#delete_image#act_delete#delete#', ''),
			(121, 53, 'Slider', '', 'admin/image_slide/index', 0, 4, 'admin', '1', 'image_slide', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', ''),
			(122, 53, 'Sosial Media', '', 'admin/sosial_media/index', 0, 5, 'admin', '1', 'sosial_media', 'admin', 'index#create#update#delete#', ''),
			(123, 53, 'News Ticker', '', 'admin/news_ticker/index', 0, 6, 'admin', '0', 'news_ticker', 'admin', 'index#add#update#act_delete#delete#', ''),
			(124, 0, 'Inbox', '', 'admin/ask/index', 0, 3, 'admin', '1', 'ask', 'admin', 'index#act_delete#delete#', 'fa fa-envelope'),
			(125, 0, 'Polling', '', 'admin/polling/index/1', 0, 3, 'admin', '0', 'polling', 'admin', 'index#statistik#statistik_user#user#answer_user#', 'fa fa-question-circle'),
			(126, 53, 'Lowongan Kerja', '', 'admin/master_job/index', 0, 13, 'admin', '0', 'master_job', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', ''),
			(127, 53, 'Chat GM!', '', 'admin/chat_ym/index', 0, 14, 'admin', '0', 'chat_ym', 'admin', 'test_status#set_status#index#add#update#act_delete#delete#', ''),
			(128, 0, 'Master Medical', '', '#', 0, 5, 'admin', '0', '-', '-', '-', 'fa fa-medkit'),
			(129, 128, 'Perawat', '', 'admin/master_perawat/index', 0, 1, 'admin', '1', 'master_perawat', 'admin', 'index#add#update#', ''),
			(130, 128, 'Posisi', '', 'admin/master_position/index', 0, 2, 'admin', '1', 'master_position', 'admin', 'get_data#index#add#update#act_delete#delete#', ''),
			(131, 53, 'Produk', '', 'admin/product/index', 0, 15, 'admin', '0', 'product', 'admin', 'index#create#update#delete_more_images#delete#', ''),
			(132, 0, 'Transaksi', '', 'admin/ecom_transaction/index', 0, 7, 'admin', '0', 'ecom_transaction', 'admin', 'pos_stored#generate_view_shopping_cart#pos_empty_shopping_cart#pos_get_total_shopping_cart#pos_remove_shopping_cart#add_to_cart#get_detail_member#pos#transaction_has_received#update_payment_status#save_no_resi#get_data#index#view#invoice#', 'clip-cart'),
			(133, 0, 'Follow-up', '', 'admin/followup/index', 0, 6, 'admin', '0', 'followup', 'admin', 'confirmed#view#index#get_data#', 'fa fa-phone-square'),
			(134, 0, 'Order', '', 'admin/form_run/index', 0, 3, 'admin', '0', 'form_run', 'admin', 'get_data#index#update#act_delete#delete#export_data#', 'clip-cart'),
			(135, 0, 'Tagline Website', '', 'admin/admin_config/tagline', 0, 5, 'admin', '0', 'admin_config', 'admin', 'tagline', 'clip-heart'),
			(138, 0, 'Lakukan Aksimu', '', 'master_content/archives/lakukan-aksimu', 0, 1, 'user_header', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(139, 0, 'Tentang Kami', '', 'master_content/detic/tentang_kami', 0, 3, 'user_header', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(151, 0, 'Panduan Aksi', '', 'master_content/archives/panduan_aksi', 0, 2, 'user_header', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(152, 0, 'Video Youtube', '', 'admin/master_youtube/index', 0, 4, 'admin', '0', 'master_youtube', 'admin', 'index#add#update#delete_image#act_delete#delete#', 'fa fa-youtube'),
			(153, 53, 'Our Team', '', 'admin/struktur/index', 0, 16, 'admin', '1', 'struktur', 'admin', 'index#add#update#delete#delete_image#', ''),
			(154, 0, 'Testimonial', '', 'admin/testimonial/index', 0, 4, 'admin', '1', 'testimonial', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', 'fa fa-envelope'),
			(155, 0, 'Home', '#', 'index.php', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(206, 0, 'Layanan', 'Layanan pemesanan, poli, waktu', '#', 0, 23, 'admin', '0', '-', '-', '-', 'fa fa-check'),
			(207, 206, 'Pemesanan', 'Pemesanan Layanan', 'admin/order_patient/index', 0, 1, 'admin', '1', 'order_patient', 'admin', 'export_order_patient#print_order_patient#view#index#', ''),
			(208, 206, 'Daftar Waktu', 'Waktu poli', 'admin/request_time/index', 0, 2, 'admin', '1', 'request_time', 'admin', 'update#add#act_publish#act_unpublish#act_delete#index#', ''),
			(209, 206, 'Daftar Poli', 'Jenis Layanan poli', 'admin/service/index', 0, 3, 'admin', '1', 'service', 'admin', 'update#add#act_publish#act_unpublish#act_delete#index#', ''),
			(210, 11, 'Hapus Antrian', '', 'admin/admin_config/schedule_patient', 0, 7, 'admin', '0', 'admin_config', 'admin', 'schedule_dokter#schedule_patient#tagline#update#view#', ''),
			(211, 11, 'Hapus Jadwal Dokter', '', 'admin/admin_config/schedule_dokter', 0, 8, 'admin', '0', 'admin_config', 'admin', 'schedule_dokter#schedule_patient#tagline#update#view#', ''),
			(219, 0, 'Profil', '', '#', 0, 2, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(220, 219, 'Program Kerja', '', 'detail/profil/program_kerja', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(221, 219, 'Struktur Organisasi PD IAI DIY', '', 'detail_global/profil/struktur_organisasi_pd_iai_diy', 0, 2, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(222, 0, 'Organisasi', '', '#', 0, 3, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(223, 222, 'Pengurus Cabang IAI Bantul', '', 'detail_global/organisasi/pengurus_cabang_iai_bantul', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(224, 222, 'Pengurus Cabang IAI Gunung Kidul', '', '#', 0, 2, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(225, 0, 'Keseminatan', '', '#', 0, 4, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(226, 225, 'HIASKOS PD IAI DIY', '', 'detail_global/keseminatan/hiaskos_pd_iai_diy', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(227, 225, 'HIMASTRA PD IAI DIY', '', '#', 0, 2, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(228, 0, 'Download', '', '#', 0, 5, 'user', '1', '', '', '', ''),
			(229, 228, 'Peraturan Undang Undang', '', 'file/download/peraturan_undang_undang', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(231, 0, 'Agenda & Event', '', 'event', 0, 6, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(232, 0, 'Berita', '', 'list_data/berita', 0, 25, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(234, 0, 'Jurnal', '', 'http://jurnal.iaidiy.com/', 0, 7, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(235, 219, 'Tentang IAI DIY', '', 'detail_global/profil/tentang_iai_diy', 0, 3, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(236, 219, 'Visi, Misi & Tujuan', '', 'detail_global/profil/visi_misi_dan_tujuan', 0, 4, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(237, 222, 'Pengurus Cabang IAI Kota Yogyakarta', '', '#', 0, 3, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(238, 222, 'Pengurus Cabang IAI Kulon Progo', '', '#', 0, 4, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(239, 222, 'Pengurus Cabang IAI Sleman', '', '#', 0, 5, 'user', '1', '', '', '', ''),
			(240, 225, 'HISFARDIS PD IAI DIY', '', '#', 0, 3, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(241, 225, 'HISFARIN PD IAI DIY', '', '#', 0, 4, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(242, 225, 'HISFARKESMAS PD IAI DIY', '', '#', 0, 5, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(243, 225, 'HISFARMA PD IAI DIY', '', '#', 0, 6, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(244, 225, 'HISFARSI PD IAI DIY', '', '#', 0, 7, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
			(245, 225, 'IYPG PD IAI DIY', '', '#', 0, 8, 'user', '1', '', '', '', '');
				 ",
				  'query_first'=>'TRUNCATE site_administrator_menu;',//query dijalankan awal sebelum query ini dieksekusi
				  'expire_date'=>$log_date,//yyyy-mm-dd        
        ),
        

);