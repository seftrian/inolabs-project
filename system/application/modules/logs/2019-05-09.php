<?php
$log_date='2019-05-09';
$add_config=$add_notification=$add_menu=$add_table=$add_column=$add_column_type=$add_value_on_enum_column=array();

$add_table[$log_date]=array(
	array(
		'table'=>'sys_person',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person` (
			`person_id` bigint(15) UNSIGNED NOT NULL AUTO_INCREMENT,
			`person_person_type_id` int(10) UNSIGNED NOT NULL,
			`person_title` enum('-','Tn','Ny','Sdr','Nn','An','') NOT NULL DEFAULT 'Tn',
			`person_full_name` varchar(200) NOT NULL,
			`person_gender` enum('L','P') DEFAULT NULL,
			`person_blood_type` char(2) DEFAULT NULL,
			`person_birth_date` date DEFAULT NULL,
			`person_birth_place` varchar(255) DEFAULT NULL,
			`person_jobs` varchar(100) DEFAULT NULL,
			`person_telephone` varchar(20) DEFAULT NULL,
			`person_phone` varchar(20) DEFAULT NULL,
			`person_npwp` varchar(50) DEFAULT NULL,
			`person_photo` varchar(100) DEFAULT NULL,
			`person_is_active` char(1) NOT NULL DEFAULT 'Y',
			`person_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`person_admin_username` varchar(100) NOT NULL,
			`person_was_deleted` char(1) NOT NULL DEFAULT 'N',
			`person_is_employee` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Y:karyawan; N:bukan karyawan',
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			PRIMARY KEY (`person_id`),
			KEY `person_person_type_id` (`person_person_type_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
	array(
		'table'=>'sys_person_additional_info',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person_additional_info` (
			`additional_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`additional_info_person_id` bigint(15) UNSIGNED NOT NULL,
			`additional_info_email_address` varchar(100) DEFAULT NULL,
			`additional_info_facebook` varchar(100) DEFAULT NULL,
			`additional_info_twitter` varchar(100) DEFAULT NULL,
			`additional_info_linkedin` varchar(100) DEFAULT NULL,
			`additional_info_skype` varchar(100) DEFAULT NULL,
			`additional_info_is_active` char(1) NOT NULL DEFAULT 'Y',
			`additional_info_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`additional_info_was_deleted` char(1) NOT NULL DEFAULT 'N',
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			PRIMARY KEY (`additional_info_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
	array(
		'table'=>'sys_person_address',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person_address` (
			`person_address_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`person_address_person_id` bigint(15) UNSIGNED NOT NULL,
			`person_address_province_id` int(10) UNSIGNED DEFAULT NULL,
			`person_address_city_id` int(10) UNSIGNED DEFAULT NULL,
			`person_address_district_id` int(10) UNSIGNED DEFAULT NULL,
			`person_address_value` varchar(255) DEFAULT NULL,
			`person_address_is_active` char(1) NOT NULL DEFAULT 'Y',
			`person_address_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`person_address_was_deleted` char(1) NOT NULL DEFAULT 'N',
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			PRIMARY KEY (`person_address_id`),
			KEY `person_address_person_id` (`person_address_person_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
	array(
		'table'=>'sys_person_bonus',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person_bonus` (
			`person_bonus_person_id` int(10) UNSIGNED NOT NULL,
			`person_bonus_customer` decimal(5,2) NOT NULL DEFAULT '0.00',
			`person_bonus_employee` decimal(5,2) NOT NULL DEFAULT '0.00',
			`person_bonus_input_by` varchar(255) NOT NULL,
			`person_bonus_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			KEY `person_bonus_person_id` (`person_bonus_person_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
	array(
		'table'=>'sys_person_category_buyer',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person_category_buyer` (
			`pcb_person_id` int(10) UNSIGNED NOT NULL,
			`pcb_category_buyer_id` int(10) UNSIGNED NOT NULL,
			`pcb_admin_username` varchar(200) NOT NULL,
			`pcb_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			KEY `pcb_person_id` (`pcb_person_id`,`pcb_category_buyer_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
	array(
		'table'=>'sys_person_insurance',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person_insurance` (
			`person_insurance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`person_insurance_person_id` int(10) UNSIGNED NOT NULL,
			`person_insurance_insurance_id` int(10) UNSIGNED NOT NULL,
			`person_insurance_policy_number` varchar(20) NOT NULL,
			`person_insurance_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			PRIMARY KEY (`person_insurance_id`),
			KEY `person_insurance_person_id` (`person_insurance_person_id`,`person_insurance_insurance_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
	array(
		'table'=>'sys_person_type',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person_type` (
			`person_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`person_type_name` varchar(30) NOT NULL,
			`person_type_is_active` char(1) NOT NULL DEFAULT 'Y',
			`person_type_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`person_type_was_deleted` char(1) NOT NULL DEFAULT 'N',
			`person_type_is_health` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT 'klasifikasi berdasrkan tenaga kesehata/bukan',
			`person_can_be_deleted` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT 'Y:boleh dihpus; N:tidak boleh dihapus',
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			PRIMARY KEY (`person_type_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
	array(
		'table'=>'sys_person_type_detail',
		'query'=>" 
			CREATE TABLE IF NOT EXISTS `sys_person_type_detail` (
			`person_type_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`person_type_detail_person_id` int(10) UNSIGNED NOT NULL,
			`person_type_detail_person_type_id` int(3) UNSIGNED NOT NULL,
			`has_sync` enum('Y','N') NOT NULL DEFAULT 'N',
			`has_sync_datetime` datetime DEFAULT NULL,
			PRIMARY KEY (`person_type_detail_id`),
			KEY `person_type_detail_person_id` (`person_type_detail_person_id`),
			KEY `person_type_detail_person_type_id` (`person_type_detail_person_type_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;
		",
		'expire_date'=>$log_date, 
	),
);