<?php
$log_date='2019-05-03';
$add_config=$add_notification=$add_menu=$add_table=$add_column=$add_column_type=$add_value_on_enum_column=array();

$add_value_on_enum_column[$log_date]=array( 
	array(
		'table'=>'site_slider',
		'column'=>'slider_title',
		'select'=>'CHARACTER_MAXIMUM_LENGTH',
		'new_value'=>'VARCHAR(150)',
		'query'=>"
		ALTER TABLE `site_slider` CHANGE `slider_title` `slider_title` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
		",    
		'expire_date'=>$log_date, //kadaluarsa untuk dieksekusi
	),
);