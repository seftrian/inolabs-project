<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('dokter_lib');
        $this->mainModel=new dokter_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
        $this->person_type=1; //dokter
    }

      public function save_dokter()
    {
        $status=500;
        $message='Tidak ada data yang disimpan.';
        $id=0;
        $label='';
        if($this->input->post('save'))
        {
            $isValidationPassed=$this->mainModel->formValidation_external();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                $columns=array(

                    'person_full_name'=>$this->input->post('person_full_name',true),
                    'person_phone'=>$this->input->post('person_phone',true),
                    'person_admin_username'=>isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin',
                );

                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $id=$this->function_lib->insert_id();
                $label=$this->input->post('person_full_name',true);
                $this->mainModel->update_person_type($id,$this->person_type);
                //statrt audit trail
                $admin_username=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $name=$this->function_lib->get_one('person_full_name',$this->mainModel->getMainTable(),$this->mainModel->primaryId.'='.intval($id));
                $description=$admin_username.' menambahkan Tenaga Perujuk '.$name.' pada tgl '.date('Y-m-d H:i:s');
                $table=$this->mainModel->getMainTable();
                $column_primary=$this->mainModel->primaryId;
                $primary_value=$id;
                function_lib::save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value);
                //end audit trail

                    //save some  detail address
                $this->mainModel->setMainTable('sys_person_identity');
                 $columns=array(
                'person_identity_person_id'=>$id,
                'person_identity_identity_id'=>$this->input->post('person_identity_identity_id',true),
                'person_identity_number'=>$this->input->post('person_identity_number',true),
               
                );
                $this->mainModel->setPostData($columns);
                $this->mainModel->save();

                    //save some  detail address
                $this->mainModel->setMainTable('sys_person_address');
                 $columns=array(
                'person_address_person_id'=>$id,
                'person_address_value'=>$this->input->post('person_address_value',true),
                'person_address_province_id'=>$this->input->post('person_address_province_id',true),
                'person_address_city_id'=>$this->input->post('person_address_city_id',true),
                'person_address_district_id'=>$this->input->post('person_address_district_id',true),
                );
                $this->mainModel->setPostData($columns);
                $this->mainModel->save();


                //save some  detail additional info
                $this->mainModel->setMainTable('sys_person_additional_info');
                 $columns=array(

                'additional_info_person_id'=>$id,
                'additional_info_email_address'=>$this->input->post('additional_info_email_address',true),
                'additional_info_facebook'=>$this->input->post('additional_info_facebook',true),
                'additional_info_twitter'=>$this->input->post('additional_info_twitter',true),
                'additional_info_linkedin'=>$this->input->post('additional_info_linkedin',true),
                'additional_info_skype'=>$this->input->post('additional_info_skype',true),
                );
                $this->mainModel->setPostData($columns);
                $this->mainModel->save();
            }
        }

        echo json_encode(array(
            'status'=>$status,
            'message'=>strip_tags($message),
            'id'=>$id,
            'label'=>$label,
            ));
    }
       /**
    mendapatkan data produk melalui nama item
    @param string $item_name
    @return json
    */
    public function get_detail_dokter_by_name($person_name='')
    {
        $this->mainModel->setMainTable('sys_person');
        $where='person_full_name LIKE "%'.$this->security->sanitize_filename($person_name).'%"
        AND person_is_active="Y" AND person_was_deleted ="N"
         AND person_id IN 
                (SELECT person_type_detail_person_id FROM sys_person_type_detail
                    WHERE person_type_detail_person_type_id IN 
                    (   
                        SELECT person_type_id 
                            FROM 
                            sys_person_type 
                        WHERE LOWER(person_type_name)="dokter"
                    )
                )';

        $findAll=$this->mainModel->findAll($where,10,0,'person_full_name ASC');
        $data=array();
        if(!empty($findAll))
        {

            $no=1;
            foreach($findAll AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }
   
                 $data[$no]['id']=$person_id;
                 $data[$no]['label']=$person_full_name.' ('.$person_phone.')';
                 $data[$no]['value']=$person_full_name;
                 $no++;

            }
        }
        echo json_encode($data);
    }

       /**
    mendapatkan data produk melalui nama item
    @param string $item_name
    @return json
    1,3,4,8 
    dokter,apoteker, asisten apoteker, pegawai
    */
    public function get_detail_employee_by_name($person_name='')
    {
        $this->mainModel->setMainTable('sys_person');
        $where='person_full_name LIKE "%'.$this->security->sanitize_filename($person_name).'%"
        AND person_is_active="Y" AND person_was_deleted ="N"
         AND person_id IN (SELECT person_type_detail_person_id FROM sys_person_type_detail
                WHERE person_type_detail_person_type_id IN (1,3,4,8)
                 )
         AND person_id NOT IN (SELECT employee_affair_person_id FROM sys_employee_affair
                 )
            ';
        $findAll=$this->mainModel->findAll($where,10,0,'person_full_name ASC');
        $data=array();
        if(!empty($findAll))
        {

            $no=1;
            foreach($findAll AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }
   
                 $data[$no]['id']=$person_id;
                 $data[$no]['label']=$person_full_name.' ('.$person_phone.')';
                 $data[$no]['value']=$person_full_name;
                 $no++;

            }
        }
        echo json_encode($data);
    }

    function load_data_person_by_id($id)
    {
        $dataResp=array();
        $this->mainModel->setMainTable('sys_person');
        $join=array(
   //     ' LEFT JOIN sys_person_address ON person_id=person_address_person_id',
    //    ' LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id',
     //   ' LEFT JOIN sys_person_identity ON person_id=person_identity_person_id'
        );
      
        $dataArr=$this->mainModel->findCustom("person_id=".  intval($id)."  AND person_was_deleted='N'
          
            ",$join);
// AND additional_info_is_active='Y'
    //        AND additional_info_was_deleted='N'
        $status=200;
        $message='';

        if(!empty($dataArr))
        {
            $status=200;
            $message='Data tersedia';

            $this->mainModel->setMainTable('sys_person_address');
            $where='person_address_person_id='.intval($id).' AND person_address_is_active="Y"';
            $data_address=$this->mainModel->find($where);

            $this->mainModel->setMainTable('sys_person_identity');
            $where='person_identity_person_id='.intval($id).' AND person_identity_is_active="Y"';
            $data_identity=$this->mainModel->find($where);

            $this->mainModel->setMainTable('sys_person_additional_info');
            $where='additional_info_person_id='.intval($id).' AND additional_info_is_active="Y"';
            $data_additional_info=$this->mainModel->find($where);

            $dataResp=array_merge($dataArr,$data_address,$data_identity,$data_additional_info);
        }
     

        echo json_encode(
                array(
                    'status'=>$status,
                    'message'=>$message,
                    'data'=>$dataResp,
                )
            );
    }

    function get_data_dokter() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*,
            sys_person_address.*,
            sys_person_additional_info.*
        ";
        $params['join'] = "
            LEFT JOIN sys_person_address ON person_id=person_address_person_id
            LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id
        ";
          $where='
        AND person_id IN (SELECT person_type_detail_person_id FROM sys_person_type_detail WHERE 
         person_type_detail_person_type_id='.intval($this->person_type).')
        ';
        $params['where'] = "
            person_was_deleted='N'
            AND person_address_is_active='Y'
            AND person_address_was_deleted='N'
            AND additional_info_is_active='Y'
            AND additional_info_was_deleted='N'
        ".$where;
      
        $params['order_by'] = "
            person_id DESC
        ";
        $params['group_by'] = "
            person_id 
        ";
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
        $no++;
         
            $status=($person_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',


            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $type='<br />'.$this->mainModel->list_person_type($person_id);
            $entry = array('id' => $person_id,
                'cell' => array(
                    'actions' =>  '<a class="btn btn-xs btn-green" href="#" onclick="load_data_person_by_id('.$person_id.'); return false;"><i class="clip-checkmark-circle-2"></i></a>',
                    'no' =>  $no,
                    'person_full_name' =>$person_title.', '.$person_full_name.$type,
                    'person_phone'=>$person_phone,
                    'person_address_value' =>$person_address_value,
                    'person_gender' =>$person_gender,
                    'person_birth_date' =>$person_birth_place.' / '.date("d-m-Y", strtotime($person_birth_date)),
                    'person_photo' =>'<img src="'.$img.'"></img>',
                    'person_timestamp' => date("d-m-Y H:i:s", strtotime($person_timestamp)),
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

    function get_data_person() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*,
            sys_person_address.*,
            sys_person_additional_info.*
        ";
        $params['join'] = "
            LEFT JOIN sys_person_address ON person_id=person_address_person_id
            LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id
        ";
          $where='
        AND person_id NOT IN (SELECT person_type_detail_person_id FROM sys_person_type_detail WHERE 
         person_type_detail_person_type_id='.intval($this->person_type).')
        ';
        $params['where'] = "
            person_was_deleted='N'
            AND person_address_is_active='Y'
            AND person_address_was_deleted='N'
            AND additional_info_is_active='Y'
            AND additional_info_was_deleted='N'
        ".$where;
      
        $params['order_by'] = "
            person_full_name ASC
        ";
        $params['group_by'] = "
            person_id 
        ";
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
        $no++;
         
            $status=($person_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',


            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $type='<br />'.$this->mainModel->list_person_type($person_id);
            $entry = array('id' => $person_id,
                'cell' => array(
                    'actions' =>  '<a href="#" onclick="load_data_person_by_id('.$person_id.'); return false;">Pilih</a>',
                    'no' =>  $no,
                    'person_full_name' =>$person_title.', '.$person_full_name.$type,
                    'person_phone'=>$person_phone,
                    'person_address_value' =>$person_address_value,
                    'person_gender' =>$person_gender,
                    'person_birth_date' =>$person_birth_place.' / '.date("d-m-Y", strtotime($person_birth_date)),
                    'person_photo' =>'<img src="'.$img.'"></img>',
                    'person_timestamp' => date("d-m-Y H:i:s", strtotime($person_timestamp)),
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }

    function get_data() {
        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*,
            sys_person_address.*,
            sys_person_additional_info.*
        ";
        $params['join'] = "
            LEFT JOIN sys_person_address ON person_id=person_address_person_id
            LEFT JOIN sys_person_additional_info ON person_id=additional_info_person_id
        ";
        $params['where'] = "
            person_was_deleted='N'
            AND person_address_is_active='Y'
            AND person_address_was_deleted='N'
            AND additional_info_is_active='Y'
            AND additional_info_was_deleted='N'
            AND person_id IN (SELECT person_type_detail_person_id FROM sys_person_type_detail
                WHERE person_type_detail_person_type_id=".intval($this->person_type).")
        ";
      
        $params['order_by'] = "
            person_full_name ASC
        ";
        $params['group_by'] = "
            person_id 
        ";
        
        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $status=($person_is_active=='Y')?'<i class="clip-checkmark"></i>':'<i class="clip-minus-circle"></i>';
            $logo_name= (trim($person_photo)=='')?'-': pathinfo($person_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($person_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',


            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);


            $entry = array('id' => $person_id,
                'cell' => array(
                    'actions' =>  '<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$person_id.'" title="Edit"><i class="clip-pencil"></i></a>
                     <a href="#"  class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$person_id.'\');"><i class="clip-remove"></i></a>
                    ',
                    'no' =>  $no,
                    'person_full_name' =>$person_title.', '.$person_full_name,
                    'additional_info_email_address' =>$additional_info_email_address,
                    'person_telephone'=>$person_telephone,
                    'person_phone'=>$person_phone,
                    'person_address_value' =>$person_address_value,
                   
                    'person_gender' =>$person_gender,
                    'person_blood_type' =>$person_blood_type,
                    'person_birth_date' =>$person_birth_place.' / '.date("d-m-Y", strtotime($person_birth_date)),
                    'person_photo' =>'<img src="'.$img.'"></img>',

                    'person_address_province_id' =>$this->function_lib->get_area_name($person_address_province_id),
                    'person_address_city_id' =>$this->function_lib->get_area_name($person_address_city_id),
                    'person_address_district_id' =>$this->function_lib->get_area_name($person_address_district_id),
                   
                    'person_is_active' => $status,
                    'person_timestamp' => date("d-m-Y H:i:s", strtotime($person_timestamp)),
                    'person_admin_username' => $person_admin_username,
                  
                    

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
    
}
