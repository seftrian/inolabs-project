<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
require_once dirname(__FILE__).'/../components/core_dokter_lib.php';

//class admin_privilege extends core_menu_lib{
class dokter_lib extends core_dokter_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='sys_person';
    public $primaryId='person_id';
    protected $imgPath='assets/images/person/';
    protected $fieldNameImage='person_photo';
    public function __construct() {
        
        //parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));

        if(!session_id())
        {
            session_start();
        }

        $this->getImagePath();

    }

    public function list_person_type($person_id){
        $sql='SELECT person_type_name FROM sys_person_type_detail
        INNER JOIN sys_person_type ON person_type_detail_person_type_id=person_type_id
        WHERE person_type_detail_person_id='.intval($person_id);
        $exec=$this->CI->db->query($sql);
        $results=$exec->result_array();
        $type='-';
        if(!empty($results))
        {
            $type='';
            $no=1;
            foreach($results AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }

                $type.=($no>1)?', '.$person_type_name:$person_type_name;
                $no++;
            }
        }

        return $type;
    }

    public function update_person_type($person_id,$type)
    {
        $is_exist=$this->CI->function_lib->get_one('person_type_detail_id','sys_person_type_detail','person_type_detail_person_id='.intval($person_id).'
        AND person_type_detail_person_type_id='.intval($type));

        if(!$is_exist)
        {
            $column=array(
                'person_type_detail_person_id'=>$person_id,
                'person_type_detail_person_type_id'=>$type,
                );
            $this->CI->db->insert('sys_person_type_detail',$column);
        }

    }

    public function update_person_identity($person_id,$identity_id,$identity_number)
    {
        $is_exist=$this->CI->function_lib->get_one('person_identity_person_id','sys_person_identity','person_identity_identity_id='.intval($identity_id).'
        AND person_identity_number="'.$identity_number.'"');

        if(!$is_exist)
        {
            $this->CI->db->query('UPDATE sys_person_identity SET person_identity_is_active="N" WHERE person_identity_person_id='.intval($person_id));

            $column=array(
                'person_identity_person_id'=>$person_id,
                'person_identity_identity_id'=>$identity_id,
                'person_identity_number'=>$identity_number,
                );
            $this->CI->db->insert('sys_person_identity',$column);
        }

    }
    
      /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->imgPath;
        $pathLocation=FCPATH.$this->imgPath;
        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
  
    public function upload_more_images($id)
    {
        if(!empty($_FILES))
        {
            $no=1;
            $img_detail_id='';
            foreach($_FILES AS $filesName=>$rowArr)
            {
                $results=$this->handleUpload($id,$filesName,$no);
                $post_id=isset($_POST[$filesName.'_id'])?intval($_POST[$filesName.'_id']):0;
                $img_id=($results['id']>0)?$results['id']:$post_id;
                $img_detail_id.=($no>1)?', '.$img_id:$img_id;
                if($results['status']!=200)
                {
                   return array(
                        'status'=>500,
                        'message'=>'Gambar '.$_FILES[$filesName]['name'].' gagal diunggah. ',
                    );
                }
                $no++;
            }
            
        }
        return array(
            'status'=>200,
            'message'=>'success',
        );
    }
    
     /**
     * handle upload
     */
    public function handleUpload($id,$filesName,$no)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
        $id_new=0;
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName,$no);
            $status=$results['status'];
            $message=$results['message'];
            $id_new=$results['id'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$id_new,
        );
    }
    
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name,$no)
    {
        $config=array();
        $pathImage=$this->imgPath;
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        $img_id=0;
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
           
            //simpan
            if($field_name==$this->fieldNameImage)
            {
                
                if(isset($_POST[$this->fieldNameImage.'_old']) AND trim($_POST[$this->fieldNameImage.'_old'])!='' AND file_exists(FCPATH.$_POST[$this->fieldNameImage.'_old']))
                {
                    unlink(FCPATH.$_POST[$this->fieldNameImage.'_old']);
                }
                $where=array(
                $this->primaryId=>$id,
                );
                $column=array(
                    $this->fieldNameImage=>$pathImage.$dataImage['file_name'],
                );

                $this->CI->db->update($this->mainTable,$column,$where);
            }

           
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;

        }

        return array(
            'status'=>$status,
            'message'=>$message,
            'id'=>$img_id,
        );
        
    }

    /**
     * untuk validasi form 
     * @param int $id default 0
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation_external($id=0)
    {
         $status=500;
         $message='';
         $config = array(
               array(
                     'field'   => 'person_full_name',
                     'label'   => 'Nama Lengkap',
                     'rules'   => 'trim|required|min_length[2]'
                  ),
                array(
                     'field'   => 'person_phone',
                     'label'   => 'No. Telp. / No. HP.',
                     'rules'   => 'trim|required'
                  ),
                 /*array(
                     'field'   => 'person_identity_identity_id',
                     'label'   => 'Identitas',
                     'rules'   => 'trim|required'
                  ),*/
                  /*array(
                     'field'   => 'person_identity_number',
                     'label'   => 'No. Identitas',
                     'rules'   => 'trim|required'
                  ),*/

            );
         
           $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }

 /**
     * untuk validasi form 
     * @param int $id default 0
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='';
         $config = array(
               array(
                     'field'   => 'person_full_name',
                     'label'   => 'Nama Lengkap',
                     'rules'   => 'trim|required|min_length[2]'
                  ),
               array(
                     'field'   => 'person_address_province_id',
                     'label'   => 'Provinsi',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'person_address_city_id',
                     'label'   => 'Kota',
                     'rules'   => 'trim|required'
                  ),
                array(
                     'field'   => 'person_address_district_id',
                     'label'   => 'Kecamatan',
                     'rules'   => 'trim|required'
                  ),

            );
         
           $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }

    public function getMainTable()
    {
        return $this->mainTable;
    }

    /**
     * set main table
     */
    public function setMainTable($table)
    {
        $this->mainTable=$table;
    }
    
   
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }

     /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function findCustom($where=1,$join=array())
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              ';
        if(!empty($join)) 
        {
            foreach($join AS $value)
            {
              $sql.=$value;
            }
        }     
        $sql.= ' WHERE '.$where;
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * proses penyimpanan data
     * @param int $id default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
