<?php 
/**
* @author Ari Armanda
*/
class admin extends admin_controller
{
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('request_time_lib'));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new request_time_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);

        $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    /**
     * untuk menambahkan data
     */
    public function update($id=0)
    {

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
            	$columns = array(
            		'request_time_label' => $this->input->post('request_time_label',true),
            		'request_time_is_show' => $this->input->post('request_time_is_show',true),
            	);

            	$this->mainModel->setMainTable($this->mainModel->getMainTable());
            	$this->mainModel->setPostData($columns);
            	$hasSaved = $this->mainModel->save('request_time_id='.intval($id));
            	$status = $hasSaved['status'];
            	$message = 'Data berhasil disimpan.';
            }
        }

        $dataArr = $this->mainModel->find('request_time_id='.intval($id));
        if(!empty($dataArr)){
        	foreach ($dataArr as $key => $value) {
        		$this->data[$key]=$value;
        	}
        } else{
        	show_404();
        }

        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Edit Waktu';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Waktu',
                                    'class'=>"clip-book",
                                    'link'=>base_url().'admin/'.$this->currentModule.'/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
      
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
    /**
     * untuk menambahkan data
     */
    public function add()
    {

        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
            	$columns = array(
            		'request_time_label' => $this->input->post('request_time_label',true),
            		'request_time_is_show' => $this->input->post('request_time_is_show',true),
            	);

            	$this->mainModel->setMainTable($this->mainModel->getMainTable());
            	$this->mainModel->setPostData($columns);
            	$hasSaved = $this->mainModel->save();
            	$status = $hasSaved['status'];
            	$message = 'Data berhasil disimpan.';
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
        $this->data['themeId']=$this->themeId; //dapatkan themeId
        $this->data['themeUrl']=$this->themeUrl; //dapatkan themeId
        $this->data['templateName']=$this->templateName; //nama template
        $this->data['seoTitle']='Tambah Waktu';
        $description='';
        $breadcrumbs_array=array();
        $breadcrumbs_array[]= array(
                                    'name'=>'Home',
                                    'class'=>"clip-home-3",
                                    'link'=>base_url().'admin/dashboard',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>'Waktu',
                                    'class'=>"clip-book",
                                    'link'=>base_url().'admin/'.$this->currentModule.'/index',
                                    'current'=>false, //boolean
                                );
         $breadcrumbs_array[]= array(
                                    'name'=>$this->data['seoTitle'],
                                    'class'=>"clip-pencil",
                                    'link'=>'',
                                    'current'=>true, //boolean
                                );
        $this->data['breadcrumbs']=$this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
      
        template($this->themeId,  get_class().'/'.__FUNCTION__, $this->data);
    }
     public function act_publish() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('publish') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' AND request_time_is_show="N"';
                        if($this->available_data($id,$additional_where))
                        {
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $column=array(                       
                                    'request_time_is_show'=>'Y'
                                    );
                            $this->mainModel->setPostData($column);
                            $this->mainModel->save('request_time_id='.intval($id));

                            $deleted_count++;
                        }
                }
                $arr_output['message'] = $deleted_count . ' data berhasil diaktifkan.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

     public function act_unpublish() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('unpublish') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' AND request_time_is_show="Y"';
                        if($this->available_data($id,$additional_where))
                        {
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $column=array(                       
                                    'request_time_is_show'=>'N'
                                    );
                            $this->mainModel->setPostData($column);
                            $this->mainModel->save('request_time_id='.intval($id));

                            $deleted_count++;
                        }
                       
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dinonaktifkan.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }

     public function act_delete() {
        $arr_output = array();
        $arr_output['message'] = '';
        $arr_output['message_class'] = '';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' '; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {    
                            $this->mainModel->setMainTable($this->mainModel->getMainTable());
                            $this->mainModel->delete('request_time_id='.intval($id));

                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['message_class'] = 'response_confirmation alert alert-success';
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['message_class'] = 'response_error alert alert-danger';
            }
        }
        
        echo json_encode($arr_output);
    }
    /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one($this->mainModel->primaryId,$this->mainModel->getMainTable(),
            $this->mainModel->primaryId.'='.intval($id).' '.$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }
    /**
    * tampilkan data grid order_patient
    */
    public function index(){
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Waktu';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Waktu',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );

        $this->data['currentModule'] = $this->currentModule;

        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
}
?>