<?php 
/**
* @author Ari Armanda
*/
class service_rest extends front_controller
{
    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('request_time_lib');
        $this->mainModel=new request_time_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }
    public function get_data(){
        $results = $this->mainModel->get_data_request_time();
        $query = $results['query'];
        $total = $results['total'];

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;

            $edit = '<a href="'.base_url().'admin/'.$this->currentModule.'/update/'.$request_time_id.'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            $status = (trim($request_time_is_show)=="Y")?'<i class="fa fa-eye"></i>':'<i class="fa fa-eye-slash"></i>';
            $entry = array('id' => $request_time_id,
                'cell' => array(
                    'no' =>  $no.'.',
                    'edit' => $edit,
                    'label' => $request_time_label,
                    'status' => $status,
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
}
?>