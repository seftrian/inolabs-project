<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
class admin extends admin_controller {

    public $mainModel; //model utama dari module ini
    //
    //put your code here

    public function __construct() {
        parent::__construct();

        //inisialisasi model
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        $this->load->helper('date');
    }

    /**
     * menampilkan daftar data history transfer
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index() {
        $status = (isset($_GET['status']) AND trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) AND trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Dashboard';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        $this->data['session'] = auth_admin_lib::staticGetAdminSession();
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    

}

?>