<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
class service_rest extends front_controller {

    public $mainModel; //model utama dari module ini
    //
    //put your code here

    public function __construct() {
        parent::__construct();

        //inisialisasi model
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        $this->load->model(array('notification_lib','auth/auth_admin_lib'));
        $this->mainModel=new notification_lib;
    }

    public function fetch_new_notification()
    {
        $data_user=auth_admin_lib::staticGetAdminSession();
        $admin_username=isset($data_user['detail']['admin_username'])?$data_user['detail']['admin_username']:'-';
        $where='notification_list_admin_username="'.$admin_username.'" AND notification_list_is_read="N"';
        
        $data=$this->mainModel->findAll($where,'no limit','no offset','notification_list_id DESC');

        if(!empty($data))
        {
            $sql='UPDATE notification_list SET notification_list_is_read="Y"
            WHERE '.$where;
            $this->db->query($sql);
        }
        $count_data=count($data);
        if(empty($data))
        {
            $where='notification_list_admin_username="'.$admin_username.'"
            AND DATE(notification_timestamp)="'.date('Y-m-d').'"';
            $data=$this->mainModel->findAll($where,'no limit','no offset','notification_list_id DESC');
            
        }
        $data_resp=array();
        if(!empty($data))
        {
            $index=0;

            foreach($data AS $rowArr)
            {
                $data_resp[$index]['notification_list_title']=$rowArr['notification_list_title'];
                $data_resp[$index]['notification_list_text']=$rowArr['notification_list_text'];
                $data_resp[$index]['notification_timestamp']=function_lib::time_elapsed_string($rowArr['notification_timestamp']);
                $index++;
            }
        }


        $response=array(
            'count_notification'=>intval($count_data),
            'data'=>$data_resp,
            );
        echo json_encode($response);
    }
}

?>