<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


//class admin_privilege extends core_menu_lib{
class notification_lib {

    //put your code here

    public $CI;
    protected $postData = array(); //set post data
    protected $mainTable = 'notification_list';
    public $primaryId = 'notification_list_id';

    public function __construct() {

        //parent::__construct();
        //   $this->CI->load->database();
        $this->CI = &get_instance();
        $this->CI->load->library(array('function_lib', 'form_validation'));

        if (!session_id()) {
            session_start();
        }
    }

    /**
     * untuk validasi form 
     * @param int $id default 0
     * nilai group id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id = 0) {
        $status = 500;
        $message = '';
        $config = array(
            array(
                'field' => 'edc_machine_name',
                'label' => 'Nama EDC',
                'rules' => 'trim|required|min_length[2]'
            ),
        );

        $this->CI->form_validation->set_rules($config);
        if ($this->CI->form_validation->run() == TRUE) {
            //proses melewati validasi form
            $status = 200;
            $message = 'Form ready to save';
        }

        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    public function getMainTable() {
        return $this->mainTable;
    }

    /**
     * set main table
     */
    public function setMainTable($table) {
        $this->mainTable = $table;
    }

    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where = 1) {
        $sql = 'SELECT * FROM ' . $this->mainTable . '
              WHERE ' . $where . ' 
              ';
        $exec = $this->CI->db->query($sql);
        return $exec->row_array();
    }

    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where = 1, $limit = 10, $offset = 0, $orderBy = '') {
        $sql = 'SELECT * FROM ' . $this->mainTable . '
              WHERE ' . $where . ' 
              ';
        if (trim($orderBy) != '') {
            $sql.=' ORDER BY ' . $orderBy;
        }
        if (is_numeric($limit) AND is_numeric($offset)) {
            $sql.=' LIMIT ' . $offset . ', ' . $limit;
        }
        $exec = $this->CI->db->query($sql);
        return $exec->result_array();
    }

    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where = 1) {
        $sql = 'SELECT COUNT(*) AS jml FROM ' . $this->mainTable . '
              WHERE ' . $where . ' 
              ';
        $exec = $this->CI->db->query($sql);
        $row = $exec->row_array();
        return !empty($row) ? $row['jml'] : 0;
    }

    /**
     * proses penyimpanan data
     * @param int $id default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where = '') {
        $status = 500;
        $message = 'Error';
        if (trim($where) != '') {
            $sql = 'UPDATE ' . $this->mainTable . ' SET ';
            if (!empty($this->postData)) {
                $no = 1;
                foreach ($this->postData AS $column => $value) {
                    $separated = ($no >= 1 AND $no < count($this->postData)) ? ',' : '';
                    $sql.=' ' . $column . '="' . $value . '"' . $separated;
                    $no++;
                }
            }
            $sql.=' WHERE ' . $where;
            $this->CI->db->query($sql);
            $status = 200;
            $message = 'OK';
        } else {
            $this->CI->db->insert($this->mainTable, $this->postData);
            $status = 200;
            $message = 'OK';
        }
        return array(
            'status' => $status,
            'message' => $message,
        );
    }

    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr) {
        $this->postData = $dataArr;
    }

}

?>