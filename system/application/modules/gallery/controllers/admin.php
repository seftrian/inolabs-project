<?php

class admin extends admin_controller
{

    public $mainModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('master_artikel'));
        // load tymce
        $this->load->helper(array('tinymce'));
        $this->mainModel = new master_artikel;
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }

    public function index()
    {

        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Gallery';
        $description = 'Master Artikel';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false,
        );
        $breadcrumbs_array[] = array(
            'name' => 'Artikel',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/' . $this->currentModule . '/index',
            'current' => true,
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        // list artikel menunggu tabel di buat terlebih dahulu
        // $this->data['list_news_category'] = $this->mainModel->list_news_category();
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    // add create
    public function create()
    {

        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Artikel';
        $description = 'Tambah Artikel';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Kategori Konten',
            'class' => "clip-grid-2",
            'link' => base_url() . 'admin/artikel/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url() . 'admin/artikel/create',
            'current' => true,
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);
        $this->data['status'] = $status;
        $this->data['message'] = $message;
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }

    protected function footerScript($method = '', $extraVariable = array())
    {
        if (!empty($extraVariable)) {
            extract($extraVariable);
        }

        ob_start();
        switch ($method) {
            default:
?>
            
            <?php
                break;
        }
        $footerScript = ob_get_contents();
        ob_end_clean();
        return $footerScript;
    }
}
