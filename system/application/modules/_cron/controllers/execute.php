<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */
class execute extends front_controller {

    protected $api_server; //model utama dari module ini
    protected $required; //model utama dari module ini
    protected $arr; //model utama dari module ini
    protected $domain; //model utama dari module ini

    //put your code here

    public function __construct() {
        parent::__construct();
        // echo base_url();exit;
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        $this->api_server = $this->function_lib->get_one('configuration_value', 'site_configuration', 'configuration_index="config_api"');
        $this->domain = $_SERVER['SERVER_NAME'];
    }

    public function get_event_detail($id = 0) {
        $this->arr = array(
            'domain' => $this->domain,
            'id' => $this->input->post('id'),
                // 'id'=>$id,
        );
        $arr = http_build_query($this->arr);
        // fungi ini dari mana
        $this->api_server .= 'api/get_event_detail?' . $arr;

        $response = $this->execution();
        echo json_encode($response);
    }

    public function get_event_list() {
        $this->arr = array(
            'domain' => $this->domain,
            'limit' => $this->input->post('limit'),
            'offset' => $this->input->post('offset'),
        );
        $arr = http_build_query($this->arr);
        $this->api_server .= 'api/get_event_list?' . $arr;

        $response = $this->execution();
        echo json_encode($response);
    }

    protected function execution($post_data = array()) {
        //uri invite
        $uri = $this->api_server;
        // echo $uri;
        // Setup cURL
        $ch = curl_init($uri);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_POST => 1,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
            ),
            CURLOPT_POSTFIELDS => json_encode($post_data)
                // CURLOPT_POSTFIELDS => $post_data
        ));

        // echo '<pre>';
        // Send the request
        $response = curl_exec($ch);
        if (!curl_exec($ch)) {
            $response = array(
                'status' => curl_errno($ch),
                'message' => curl_error($ch),
            );
        }
        $response_arr = json_decode($response);
        return $response_arr;
    }

}

?>
