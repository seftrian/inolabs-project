<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author anggoro
 */
class widget_social_media extends widget_lib{
    //put your code here
    public function run()
    {

        $this->load->model('sosial_media/sosial_media_lib');
        $lib=new sosial_media_lib;

        $data=array();
        $where = 'social_media_is_published="Y"';
        $sql='SELECT * FROM site_social_media 
            WHERE '.$where.'
            ORDER BY social_media_type ASC';
        $exec=$this->db->query($sql);      
        $data['results']=$exec->result_array();
        $data['pathImgArr']=$lib->getImagePath();

        $this->render(get_class(),$data);
    }
}

?>
