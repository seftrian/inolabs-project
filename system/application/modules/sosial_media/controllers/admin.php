<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('sosial_media_lib'));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new sosial_media_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Social Media';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Social Media',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/sosial_media/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        

        $limit=10;
        $where='1';
        $total_rows=$this->mainModel->countAll($where);
        $paginationArr=create_pagination('admin/'.$this->currentModule.'/'.__FUNCTION__, $total_rows, $limit );
        $this->data['offset']=$paginationArr['limit'][1];
        $this->data['findAll']=$this->mainModel->findAll($where,$limit,$this->data['offset'], 'social_media_id DESC');
        $this->data['mainModel']=$this->mainModel;

        $this->data['link_pagination']=$paginationArr['links'];
        $this->data['pathImgArr']=$this->pathImgArr;
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function create()
    {
   
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Social Media';
        $description = 'Tambah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Social Media',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/sosial_media/index',
            'current' => false, //boolean
        );
           $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/sosial_media/create',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
        
            if($status==200)
            {
                $title=$this->input->post('social_media_name');
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $columns=array(
                    'social_media_name'=>$this->input->post('social_media_name',true),
                    'social_media_link'=>$this->input->post('social_media_link',true),
                    'social_media_icon_src'=>$this->input->post('social_media_icon_src',true),
                    'social_media_is_published'=>$this->input->post('social_media_is_published',true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $id=$this->function_lib->insert_id();
                    //handle logo
                    /*
                    $logo=$this->mainModel->handleUpload($id,'category_photo');
                    if($logo['status']!=200)
                    {
                        $status=$logo['status'];
                        $message=$logo['message'];
                        redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.$msg);
                    }
                    else
                    {
                        */
                        $msg=base64_encode('Data berhasil disimpan');
                        if(isset($_GET['redirect']))
                        {
                            redirect(rawurldecode($_GET['redirect']));
                        }
                        else
                        {
                            redirect(base_url().'admin/'.$this->currentModule.'/create?status=200&msg='.$msg);
                        }
                        
                    /*
                    }
                    */
                }


            }
            //$this->session->set_flashdata('confirmation', '<div class="confirmation alert alert-danger">'.$message.'</div>');
        }
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Social Media';
        $description = 'Ubah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Social Media',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/sosial_media/index',
            'current' => false, //boolean
        );
           $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/sosial_media/create',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $rowMenu=$this->mainModel->find('social_media_id='.  intval($id));
        if(empty($rowMenu))
        {
            show_error('Request anda tidak valid',500);
        }

        foreach($rowMenu AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }

        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($id);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
                $title=$this->input->post('social_media_name');
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $columns=array(
                    'social_media_name'=>$this->input->post('social_media_name',true),
                    'social_media_link'=>$this->input->post('social_media_link',true),
                    'social_media_icon_src'=>$this->input->post('social_media_icon_src',true),
                    'social_media_is_published'=>$this->input->post('social_media_is_published',true),
                );
                $this->mainModel->setPostData($columns);
                $where='social_media_id='.  intval($id);
                $hasSaved=$this->mainModel->save($where);
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $msg=base64_encode('Data berhasil disimpan');
                    //handle logo
                    /*
                    $logo=$this->mainModel->handleUpload($id,'category_photo');
                    if($logo['status']!=200)
                    {
                        $status=$logo['status'];
                        $message=strip_tags($logo['message']);
                        $msg=base64_encode('Terjadi error saat upload file. '.$message);
                    }
                    */
                
                    redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status='.$status.'&msg='.$msg);

                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
      
        $this->data['pathImgArr']=$this->pathImgArr;
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);

    }
    
    
    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    public function delete($id=0,$output='redirect')
    {
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        $where='social_media_id='.  intval($id);
        $this->data['rowAdmin']=$this->mainModel->find($where);

        if(!empty($this->data['rowAdmin']))
        {
            
           $isDelete=$this->mainModel->delete($where);
           redirect(base_url().'admin/'.$this->currentModule.'/index?status='.$isDelete['status'].'&msg='.base64_encode($isDelete['message']));
           
        }
        else
        {
            $msg='Data tidak ditemukan';
            redirect(base_url().'admin/'.$this->currentModule.'/index?status=500&msg='.base64_encode($msg));
        }
        
    }

    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    /*
    public function delete_image($id=0,$output='redirect')
    {
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        $where='social_media_id='.  intval($id);
        $this->data['rowAdmin']=$this->mainModel->find($where);

        if(!empty($this->data['rowAdmin']))
        {
           $sql='UPDATE site_slider_category SET slider_category_photo="-" WHERE '.$where; 
           $this->db->query($sql);
           redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=200&msg='.base64_encode('Gambar berhasil dihapus'));
        }
        else
        {
            $msg='Data tidak ditemukan';
            redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.base64_encode($msg));
        }
        
    }
    */

    
    
    
     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {
        
        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();
        switch($method)
        {
            default:
            ?>
            
            <?php
            break;   
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
