<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class auth extends front_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {

        parent::__construct();
                  
        //load model
        $this->load->model(array('auth_admin_lib','actv/actv_lib'));;
        $this->load->library(array('system_lib'));
        //inisialisasi model
        $this->mainModel=new auth_admin_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);

    }


     public function captcha_admin($block = 'login') {
        $this->load->library('captcha');
        
        $config = array(
            'background_image' => _dir_captcha . 'captcha-widget-2.png',
            'image_width' => 124,
            'image_height' => 34,
            'multi_text_color' => array(
                new Securimage_Color("#215f82"),
                new Securimage_Color("#822136"),
                new Securimage_Color("#218238"),
                new Securimage_Color("#a61682"),
                new Securimage_Color("#333333"),
            ),
        );
        $this->captcha->generate_image($config);
    }

     /**
     * setup aplikasi
     */
    public function setup()
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        $adminSession=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_id']:'';
        if(trim($adminSession)!='' AND is_numeric($adminSession))
        {
            if(isset($_GET['continue']) AND trim($_GET['continue'])!='')
            {
                redirect(rawurldecode($_GET['continue']));
            }
            else
            {
                redirect(base_url().'admin/dashboard/');
            }
        }
        if(isset($_POST['doLogin']))
        {
            $isValid=$this->mainModel->formValidation();
            $status=$isValid['status'];
            $message=($status==500 AND $isValid['message']=='')?validation_errors():$isValid['message'];

            if($status==200)
            {
                if(isset($_GET['continue']) AND trim($_GET['continue'])!='')
                {
                    redirect(rawurldecode($_GET['continue']));
                }
                else
                {
                    redirect(base_url().'admin/dashboard/');
                }
            }
            
        }
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $theme=$this->config->item('theme');

        $currentPath='themes/admin/'.$theme['backend']['name'].'/';
        $this->data['themeUrl']= base_url().$currentPath.'inc/';
        $this->data['themeId']= 'admin';
        $this->data['templateName']= $theme['backend']['name'];

        $this->load->view(get_class().'/'.$theme['backend']['name'].'/'.__FUNCTION__,$this->data);
    }
    
    /**
     * login ke admin
     */
    public function connect_to_admin()
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        $adminSession=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_id']:'';
        if(trim($adminSession)!='' AND is_numeric($adminSession))
        {
            if(isset($_GET['continue']) AND trim($_GET['continue'])!='')
            {
                redirect(rawurldecode($_GET['continue']));
            }
            else
            {
                redirect(base_url().'admin/dashboard/');
            }
        }
        if(isset($_POST['doLogin']))
        {
            $isValid=$this->mainModel->formValidation();
            $status=$isValid['status'];
//            $message=$isValid['message'];
            $message=($status==500 AND $isValid['message']=='')?validation_errors():$isValid['message'];

            if($status==200)
            {
                if(isset($_GET['continue']) AND trim($_GET['continue'])!='')
                {
                    redirect(rawurldecode($_GET['continue']));
                }
                else
                {
                    redirect(base_url().'admin/dashboard/');
                }
            }
            
        }
        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $theme=$this->config->item('theme');
        /*$check_has_expired=$this->actv_lib->check_has_expired();
        if($check_has_expired['status']==500)
        {
            redirect(base_url().'actv/index?status='.$check_has_expired['status'].'&msg='.base64_encode($check_has_expired['message']));
        }
        */
        $currentPath='themes/admin/'.$theme['backend']['name'].'/';
        $this->data['themeUrl']= base_url().$currentPath.'inc/';
        $this->data['themeId']= 'admin';
        $this->data['templateName']= $theme['backend']['name'];
        $this->data['list_master_shift']=$this->mainModel->list_master_shift();
        $this->load->view(get_class().'/'.$theme['backend']['name'].'/'.__FUNCTION__,$this->data);
    }
    
    
    
    /**
     * logout 
     * @param string $user admin|member
     */
    public function logout($user='admin')
    {
        
        switch(strtolower($user))
        {
            case 'admin':
                $this->mainModel->destroyAdminSession();
            break;    
        
        }
        
        //jika ada request redirect
        if(isset($_GET['redirect']))
        {
            redirect(rawurldecode($_GET['redirect']));
        }
        //redirect ke depan
        redirect(base_url());
    }
}

?>
