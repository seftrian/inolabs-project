<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */


class auth_member_lib{
    //put your code here
    
    public $CI;
    public function __construct() {
        
        $this->CI=&get_instance();
        $this->CI->load->library(array('system_lib','function_lib','form_validation','session'));
        $this->CI->load->model(array('mlm/member_lib'));

        if(!session_id())
        {
            session_start();
        }
        
        $this->memberLib=new member_lib;
    }
    
    /**
     * pembuatan kode reset dan expire date
     * @param int $networkId
     * @return string $message
     */
    public function createResetCode($networkId)
    {
        $uniqId=  uniqid(rand());
        $uniqVal=substr($uniqId,0,3);
        $ed=date('Y-m-d H:i:s',mktime(date('H')+1,date('i'),date('s'),date('m'),date('d'),date('Y'))); //1 jam dari sekarang
        $column=array(
            'reset_password_network_id'=>$networkId,
            'reset_password_code'=>$uniqVal,
            'reset_password_expire_date'=>$ed,
        );
        
        $this->CI->db->insert('sys_reset_password',$column);
        $memberName=$this->CI->function_lib->get_name_from_id($networkId);
        $memberId=$this->CI->function_lib->get_mid($networkId);
        $message='Yth. Sdr/Sdri '.$memberName.' ('.$memberId.'),';
        $genCode=$networkId.'#'.$uniqVal;
        $link=base_url().'auth/reset/3?res='.  base64_encode($genCode);
        $message.='<br /><br /> Anda atau seseorang telah melakukan reset password. Untuk melanjutkannya, silakan klik <a href="'.$link.'">di sini</a> atau <a href="'.$link.'">'.$link.'</a>.';
        $message.='<br /><br /> Jika anda merasa tidak melakukannya, mohon diabaikan saja.';
        $message.='<br /><br /> ';
        $message.='<br /><br /> <i>Pesan ini terkirim secara otomatis oleh sistem, kami mohon jangan dibalas karena anda tidak akan mendapat respon apapun.</i> ';
        $message.='<br /><br />';
        $message.='<br /><br /> Terimakasih.';
        return $message;
    }
    
    /**
     * pengecekkan kebenaran code
     * @param int $networkId
     * @param string $code
     * @return true valid
     */
    public function checkResetCode($networkId,$code)
    {
        $isValid=$this->CI->function_lib->get_one('COUNT(reset_password_id)','sys_reset_password','reset_password_network_id='.  intval($networkId).' AND reset_password_code="'.$code.'" AND reset_password_expire_date>="'.date('Y-m-d H:i:s').'"');
    
        if($isValid)
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * reset password
     * @param int $networkId
     * @param $_POST password_new
     * @param $_POST password_new_re
     */
    public function doChangePass($networkId)
    {
        
         $status=500;
         $message=$redirect='';
         
            $mainConfig = array(
                array(
                        'field'   => 'password_new',
                        'label'   => 'Password Baru',
                        'rules'   => 'trim|required|min_length[5]'
                    ),
                array(
                        'field'   => 'password_new_re',
                        'label'   => 'Ulangi Password Baru',
                        'rules'   => 'trim|required|matches[password_new]'
                    ),

            );
         
          $config=$mainConfig;
            
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
                  $passNew=$this->CI->input->post('password_new',true); //filter injeksi
              
                  //proses penyimpanan password baru
                  $column=array(
                      'password_value'=>md5($passNew)
                  );
                  $where='password_network_id='.  intval($networkId);
                  $this->memberLib->setMainTable('sys_password');
                  $this->memberLib->setPostData($column);
                  $this->memberLib->save($where);
                  
                  
                  //lakukan login
                  //set session admin
                  $username=$this->CI->function_lib->get_mid($networkId);
                  
                  $this->memberLib->setMainTable('sys_member');
                  $this->setMemberSession($username);
                  //reset session fail
                  $_SESSION['fail_login_member']=0;
                  
                  //library send to email
                  $this->CI->load->library('send_mail');
                
                  //buat reset code
                  $memberEmail=$this->CI->function_lib->get_one('member_email','sys_member','member_network_id="'.$networkId.'"');
                  $projName=$this->CI->config->item('project_name');
                  $emailFx=$this->CI->config->item('email');
                  
                  $sendEmail=new send_mail;
                  $emailProperty=array(
                      'email_to'=>$memberEmail,
                      'email_subject'=>$projName.' - Reset Password Sukses',
                      'email_message'=>'Perubahan password berhasil dilakukan.',
                      'email_from'=>$emailFx,
                  );
                  $sendEmail->run($emailProperty);
                  
                  //update reset password agar tak digunakan lagi
                  //proses penyimpanan password baru
                  $columnRes=array(
                      'reset_password_expire_date'=>date('Y-m-d H:i:s'),
                  );
                  $whereRes='reset_password_network_id='.  intval($networkId);
                  $this->memberLib->setMainTable('sys_reset_password');
                  $this->memberLib->setPostData($columnRes);
                  $this->memberLib->save($whereRes);
                  
                //proses melewati validasi form
                $status=200;
                $message='Perubahan password berhasil dilakukan.';
              
             
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
          
    }
    
    /**
     * untuk validasi form 
     * 
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *          'html'=>'string',
     *      );
     */
    public function formValidation()
    {
         $maxShowCaptcha=2;
         $status=500;
         $message='';
         $html=(isset($_SESSION['fail_login_member']) AND $_SESSION['fail_login_member']>$maxShowCaptcha)?recaptcha_get_html($this->CI->system_lib->pubkeyRecaptcha).'<p>Mohon inputkan kode captcha di atas.</p>':'';
         
            $mainConfig = array(
                array(
                        'field'   => 'username',
                        'label'   => 'Member ID',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'password',
                        'label'   => 'Password',
                        'rules'   => 'trim|required'
                    ),

            );
         
          $config=$mainConfig;
            
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              
              //proses melewati validasi form
              $status=200;
              $message='Form ready to use';
              
              $username=$this->CI->input->post('username',true); //filter injeksi
              $password=$this->CI->input->post('password',true); //filter injeksi
              
              $found=$this->checkAuth($username, $password);
              
              //jika captcha telah ditampilkan
              if(trim($html)!='' AND isset($_POST['recaptcha_response_field']))
              {
                   //verifikasi captcha dan bypass validasi
                $isValidCaptcha=$this->CI->system_lib->isValidCaptcha();
                $status=$isValidCaptcha['status'];
                $message=$isValidCaptcha['message'];
                if($status!=200)
                {
                    return array(
                        'status'=>$status,
                        'message'=>$message,
                        'html'=>$html, 
                    );
                }
              }
              
              
                
              
              //jika data ditemukan
              if(intval($found)>0)
              {
                 //set session admin
                 $this->setMemberSession($username);
                
                 $status=200;
                 $message='Hi Member, Welcome!';  
                 
                 //reset session fail
                 $_SESSION['fail_login_member']=0;
              }
              else
              {
                //count session fail
                $_SESSION['fail_login_member']=isset($_SESSION['fail_login_member'])?$_SESSION['fail_login_member']+1:1;  
                $status=500;
                $message='Member ID atau Password tidak ditemukan';   
                if(isset($_SESSION['fail_login_member']) AND $_SESSION['fail_login_member']>$maxShowCaptcha)
                {
                    $html=recaptcha_get_html($this->CI->system_lib->pubkeyRecaptcha).'<p>Mohon inputkan kode captcha di atas.</p>';
                    $message='Member ID atau Password tidak ditemukan. Silakan klik reset password atau ulangi proses verifikasi login dengan menyertakan kode captcha.';   
                }
              }
              
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
              'html'=>$html,
          );
         
    }
    
    /**
     * @param string $code $_GET['res']
     * @return array id dan code
     * **/
    public function decodeResetCode($code)
    {
        $decodeB64=  base64_decode($code);
        $expTag=explode('#',$decodeB64);
        if(!empty($expTag) AND count($expTag)==2)
        {
            $networkId=$expTag[0];
            $codeVal=$expTag[1];
        }
        else
        {
            return array();
        }
        
        return array (
            'id'=>$networkId,
            'code'=>$codeVal,
        );
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidationReset()
    {
         $status=500;
         $message=$redirect='';
         
            $mainConfig = array(
                array(
                        'field'   => 'username',
                        'label'   => 'Member ID',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'recaptcha_response_field',
                        'label'   => 'Kode Captcha',
                        'rules'   => 'trim|required'
                    ),

            );
         
          $config=$mainConfig;
            
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              $username=$this->CI->input->post('username',true); //filter injeksi
              $pass=$this->CI->function_lib->get_one('COUNT(network_mid)','sys_network','network_mid="'.$username.'"');
              
               $isValidCaptcha=$this->CI->system_lib->isValidCaptcha();
               $status=$isValidCaptcha['status'];
              
              //jika tidak ditemukan mid tersebut
              if(!$pass)
              {
                  
                //proses melewati validasi form
                $status=500;
                $message='Member ID tidak ditemukan.';
              //  die('ok');

              }
              elseif($status!=200)
              {
                 $message=$isValidCaptcha['message'];
              }
              else
              {
                  //library send to email
                  $this->CI->load->library('send_mail');
                
                  //buat reset code
                  $networkId=$this->CI->function_lib->get_id($username);
                  $memberEmail=$this->CI->function_lib->get_one('member_email','sys_member','member_network_id="'.$networkId.'"');
                  $messageEmail=$this->createResetCode($networkId);
                  $projName=$this->CI->config->item('project_name');
                  $emailFx=$this->CI->config->item('email');
                  
                  $sendEmail=new send_mail;
                  $emailProperty=array(
                      'email_to'=>$memberEmail,
                      'email_subject'=>$projName.' - Reset Password',
                      'email_message'=>$messageEmail,
                      'email_from'=>$emailFx,
                  );
                  $sendEmail->run($emailProperty);
                  
                //proses melewati validasi form
                $status=200;
                $message='Kami telah mengirimkan pesan ke email anda. Mohon segera diverifikasi.';
                $redirect=base_url().'auth/reset/2?status='.$status.'&msg='.  base64_encode($message);
              }
             
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
              'redirect'=>$redirect,
          );
         
    }
    
    /**
     * pengecekkan member id dan password
     * @param string $username member id
     * @param string $password
     * @param bool true valid otherwise false
     */
    public function checkAuth($username,$password)
    {
        $where='(network_mid="'.$username.'" OR member_email="'.$username.'")  AND password_value="'.md5($password).'"';
        $isFounded=$this->CI->function_lib->get_one('COUNT(network_id)','sys_network LEFT JOIN sys_password ON password_network_id=network_id 
            LEFT JOIN sys_member ON member_network_id=network_id',$where);
        if($isFounded)
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * set session
     * @param string $username MID
     */
    public function setMemberSession($username)
    {
        
        $where='network_mid="'.$username.'" OR member_email="'.$username.'"';
        
        $join=array(
            'LEFT JOIN sys_network ON network_id=member_network_id'
        );
        
        $results=$this->memberLib->findCustom($where,$join);
        $data=array();
        if(!empty($results))
        {

            //proses set session
            foreach($results AS $key=>$row)
            {
                $data['__member__'][$key]=$row;
            }
            
            //update last login
            $where=array(
                'member_network_id'=>$results['network_id'],
            );
            $column=array(
                'member_last_login'=>date('Y-m-d H:i:s'),
            );
            $this->CI->db->update($this->memberLib->mainTable,$column,$where);

            $this->CI->session->set_userdata($data);

        }
        
        

    }
    
    
    /**
     * destroy session admin
     * digunakan ketika akan logout
     */
    public function destroyMemberSession()
    {
        $getAdminSession=$this->getMemberSession();
        if(!empty($getAdminSession))
        {
            unset($_SESSION['__member__']);
        }
    }
    
    
    /**
     * dapatkan session admin
     */
    public function getMemberSession()
    {
        $dataSession=array();
        if(isset($_SESSION['__member__']) AND !empty($_SESSION['__member__']))
        {
            $dataSession=$_SESSION['__member__'];
        }
        
        return $dataSession;
    }
   
    /**
     * dapatkan session admin
     */
    public static function staticGetMemberSession()
    {
        $dataSession=array();
        if(isset($_SESSION['__member__']) AND !empty($_SESSION['__member__']))
        {
            $dataSession=$_SESSION['__member__'];
        }
        
        return $dataSession;
    }
    
}

?>
