<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */

require_once dirname(__FILE__).'/../components/core_auth_lib.php';
class auth_admin_lib extends core_auth_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_administrator';
    public function __construct() {
        //turunan dari class core
        parent::__construct();
        
        
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation','session'));

        /*
        if(!session_id()) {
            session_start();
        }
        */
        
    }

    /**
    list shift
    */
    public function list_master_shift()
    {
        $sql='SELECT * FROM master_shift WHERE shift_is_active="Y"
              ORDER BY shift_id ASC  ';
        $exec=$this->CI->db->query($sql);
        $results=$exec->result_array();
        return $results;      
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
    
    /**
     * dapatkan semua data dengan fitur join
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @param string $select 
     * @param array $join
     *          eq. $join=array(
     *              'LEFT JOIN site_administrator_group ON administrator_group_id=admin_group_id',
     *                 );
     * @return array
     */
    public function findAllCustom($where=1,$limit=10,$offset=0,$orderBy='',$select='*',$join=array())
    {
        $sql='SELECT '.$select.' FROM '.$this->mainTable.' ';
        
        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery;
            }
        }
        
        $sql.=' WHERE '.$where;
        
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @param array $join
     *          eq. $join=array(
     *              'LEFT JOIN site_administrator_group ON administrator_group_id=admin_group_id',
     *                 );
     * @return array
     */
    public function findCustom($where=1,$join=array())
    {
        $sql='SELECT * FROM '.$this->mainTable.' ';
        if(!empty($join))
        {
            foreach($join AS $joinQuery)
            {
                $sql.=$joinQuery;
            }
        }
        $sql.=' WHERE '.$where;
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
    validasi captcha
    */
    protected function validation_captcha_admin($str) {
        $this->CI->load->library('captcha');
        if($this->CI->captcha->verify($str)) {
            $status=200;
            $message='Captcha OK';
        } else {
            $status=500;
            $message='Kode unik yang diinputkan salah.';
        }

        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }

    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation()
    {
         $status=500;
         $message='';
         
         
            $mainConfig = array(
                array(
                        'field'   => 'username',
                        'label'   => 'Username',
                        'rules'   => 'trim|required|min_length[2]'
                ),
                array(
                        'field'   => 'password',
                        'label'   => 'Password',
                        'rules'   => 'trim|required|min_length[2]'
                ),
                array(
                        'field'   => 'shift_user',
                        'label'   => 'Pilih Shift',
                        'rules'   => 'trim|required'
                ),
                /*array(
                        'field'   => 'captcha',
                        'label'   => 'Kode Unik',
                        'rules'   => 'trim|required|min_length[2]'
                ),*/

            );
         
          $config=$mainConfig;
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {

                //proses pengecekan captcha jika kesalahan login lebih dar 2x
                if(isset($_SESSION['fail_login_admin']) AND $_SESSION['fail_login_admin']>2)
                {
                  //cek captcha
                  $captcha_str=$this->CI->input->post('captcha');  
                  $validation_captcha=$this->validation_captcha_admin($captcha_str);
                  $status=$validation_captcha['status'];
                  $message=$validation_captcha['message'];
                }
                else
                {
                    $status=200;
                    $message='Ignore captcha';
                }
              if($status==200)
              {
                  //proses melewati validasi form
                  $status=200;
                  $message='Form ready to use';
                  $username=$this->CI->input->post('username',true); //filter injeksi
                  $password=$this->CI->input->post('password',true); //filter injeksi
                  $where='admin_username="'.$username.'" AND admin_password="'.md5($password).'" AND admin_is_active="1"';
                  $found=$this->countAll($where);
                  
                  //jika data ditemukan
                  if(intval($found)>0)
                  {
                     //set session admin
                     $this->setAdminSession($where);
                    
                     $status=200;
                     $message='Hi Admin, Welcome!';  
                     
                     //reset session fail
                     $_SESSION['fail_login_admin']=0;
                  }
                  else
                  {
                    //count session fail
                    $_SESSION['fail_login_admin']=isset($_SESSION['fail_login_admin'])?$_SESSION['fail_login_admin']+1:1;  
                    $status=500;
                    $message='Username atau Password tidak ditemukan';   
                    if(isset($_SESSION['fail_login_admin']) AND $_SESSION['fail_login_admin']>3)
                    {
                        $message='Username atau Password tidak ditemukan. Silakan kontak '.$this->CI->config->item('email').' untuk mendapatkan username dan password anda.';   

                    }
                  }
              }  
 
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * set session
     * @param string $where parameter
     */
    public function setAdminSession($where)
    {
        $select='admin_username,admin_last_login,admin_group_id,';
        $select.='admin_group_title,admin_group_type';
        $join=array(
            'INNER JOIN site_administrator_group sag ON sag.admin_group_id='.$this->mainTable.'.admin_group_id'
        );
        
        $results=$this->findCustom($where,$join);
        $data=array();
        if(!empty($results))
        {
            //proses set session
            foreach($results AS $key=>$row)
            {
                if($key!='admin_password')
                {
                   // $_SESSION['admin'][$key]=$row;
                    $data['admin']['detail'][$key]=$row;
                }
            }
            
            $data['admin']['detail']['shift']=$this->CI->input->post('shift_user');

            //update last login
            $where=array(
                'admin_id'=>$results['admin_id'],
            );
            $column=array(
                'admin_last_login'=>date('Y-m-d H:i:s'),
            );
            $this->CI->db->update($this->mainTable,$column,$where);
            $this->setPrivilegeSession($data);
            
        }
        
        

    }
    
    
    /**
     * set session
     * @param array $adminArr parameter
     */
    protected function setPrivilegeSession($adminArr)
    {   
        $groupId=$adminArr['admin']['detail']['admin_group_id'];
        $this->mainTable='site_administrator_privilege';
        $where='administrator_privilege_administrator_group_id='.  intval($groupId);
        $resultsPrivilege=$this->findAll($where,'no limit','no offset');
        $buildArray=array();
        
        $mergeSession=array();
        $mergeSession['admin']=$adminArr['admin'];
        if(!empty($resultsPrivilege))
        {
            foreach($resultsPrivilege AS $row)
            {
                //filter yang hanya ada methodnya
                if(trim($row['administrator_privilege_method_allowed'])!='')
                {
                    $menuId=$row['administrator_privilege_administrator_menu_id'];
                    $module=$this->CI->function_lib->get_one('menu_administrator_module','site_administrator_menu','menu_administrator_id='.  intval($menuId));
                    $controller=$this->CI->function_lib->get_one('menu_administrator_controller','site_administrator_menu','menu_administrator_id='.  intval($menuId));
                     $buildArray['admin']['privilege'][]=array(
                          'menu_id'=>$row['administrator_privilege_administrator_menu_id'],
                          'module'=>$module,
                          'controller'=>$controller,
                          'method'=>$row['administrator_privilege_method_allowed'],
                );
                }
               
            }
            //gabungkan data admin dan privilege
            $mergeSession['admin']=  array_merge($adminArr['admin'],$buildArray['admin']);
            

        }
        
//        $this->CI->session->set_userdata($mergeSession);
        $_SESSION['admin'] = $mergeSession['admin'];
        //return $buildArray;
    }
    
    /**
     * destroy session admin
     * digunakan ketika akan logout
     */
    public function destroyAdminSession()
    {
        $getAdminSession=$this->getAdminSession();
        if(!empty($getAdminSession))
        {
            unset($_SESSION['admin']);
        }
    }
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * dapatkan session admin
     */
    public function getAdminSession()
    {
        $dataSession=array();
        if(isset($_SESSION['admin']) AND !empty($_SESSION['admin']))
        {
            $dataSession=$_SESSION['admin'];
        }
        
        return $dataSession;
    }
   
    /**
     * dapatkan session admin
     */
    public static function staticGetAdminSession()
    {
        $dataSession=array();
        if(isset($_SESSION['admin']) AND !empty($_SESSION['admin']))
        {
            $dataSession=$_SESSION['admin'];
        }
        
        return $dataSession;
    }
    
    /**
     * check ketersediaan data username
     * @param stirng $query 
     * @param int $userId default 0 saat create
     * @return bool true is valid
     */
    public function isExistUserName($query, $userId=0)
    {
        $value=TRUE;
        //update
        if(is_numeric($userId) AND $userId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_username',$this->mainTable,'admin_username LIKE TRIM(\''.$query.'\') AND admin_id!='.  intval($userId));
        }
        else//create
        {
            $isFounded=$this->CI->function_lib->get_one('admin_username',$this->mainTable,'admin_username LIKE TRIM(\''.$query.'\')');
        }
        
        if($isFounded)
        {
            $value=FALSE;
        }
        
        return $value;
    }
    
    /**
     * check password saat ini
     * @param stirng $query 
     * @param int $userId default 0 saat create
     * @return bool true is valid
     */
    public function isCurrentPassword($query, $userId=0)
    {
        $value=false;
        //update
        if(is_numeric($userId) AND $userId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_password',$this->mainTable,'admin_password="'.md5($query).'" AND admin_id='.  intval($userId));
        }
        
        if($isFounded)
        {
            $value=true;
        }
        
        return $value;
    }
    
    /**
     * untuk manual proteksi ketika action ada di method lain
     * @param string $class inputkan nama class secara manual
     * @param string $method inputkan nama method secara manual
     */
    public static function manualProctection($class='',$method='')
    {
        
        $authLib=new auth_admin_lib;
        
        //dapatkan session admin
        $sessionArr=$authLib->getAdminSession();
        $groupType=$sessionArr['detail']['admin_group_type'];
        $allowed=($groupType=='superuser')?true:false; //inisialisasi perizinan akses
        
        if(!empty($sessionArr))
        {
            $currentModule=$authLib->CI->router->fetch_module();
            $currentClass=(trim($class)!='')?$class:$authLib->CI->router->fetch_class();
            $currentMethod=(trim($method)!='')?$method:$authLib->CI->router->fetch_method();

            
            $privilegeAccess=isset($sessionArr['privilege'])?$sessionArr['privilege']:array();
            if($groupType!='superuser')
            {
                if(!empty($privilegeAccess))
                {
                    foreach($privilegeAccess AS $access)
                    {
                        $myModule=$access['module'];
                        $myController=$access['controller'];
                        $myMethod=$access['method'];
                        if($myModule==$currentModule AND $myController==$currentClass AND $myMethod==$currentMethod)
                        {
                            $allowed=true;
                            return $allowed;
                        }
                    }
                }
            }
            
        }
        
        
        if($allowed===false)
        {

            show_error('Maaf, anda tidak diizinkan untuk mengakses halaman ini. 
                <br /><a href="javascript:history.back(-1)">Kembali</a>', 403);
        }
        else
        {
            return $allowed;
        }
    }
    
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
