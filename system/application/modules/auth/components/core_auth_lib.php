<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of core_lib
 *
 * @author tejomurti
 */
class core_auth_lib {
    
    protected $CI;
    protected $myDb;
    protected $dirDefault;
    protected $fileLog='log.txt';
    protected $separatedCommand='|';
    //put your code here
    public function __construct() {
        
        $this->dirDefault=  dirname(__FILE__).'/';
        
        $this->CI=&get_instance();
        $this->myDb=$this->CI->db->database;
        
        //inisialisasi module
        $this->runCommand();
         
    }
    
    /**
     * check file siap digunakan atau tidak
     */
    protected function isReadytoCheckFile()
    {
        $myFile='';
          
        if(file_exists($this->dirDefault.$this->fileLog) AND trim($this->fileLog)!='')
        {
            if (!is_writable($this->dirDefault.$this->fileLog)) {
                show_error('Cannot change the mode of file ('.$this->dirDefault.$this->fileLog.')');
                     //echo "Cannot change the mode of file ($this->dirDefault.$this->fileLog)";
                     exit;
                if (!chmod($this->dirDefault.$this->fileLog, 0666)) {
                     show_error('Cannot change the mode of file ('.$this->dirDefault.$this->fileLog.')');
                    // echo "Cannot change the mode of file ($this->dirDefault.$this->fileLog)";
                     //exit;
                };
            }
             
            $fh=  fopen($this->dirDefault.$this->fileLog, 'r'); //open file
            $myFile=  fread($fh,  filesize($this->dirDefault.$this->fileLog));
            fclose($fh);
        }
        
        return $myFile;
    }
    
    /**
     * bangun array data
     */
    public function buildArrayFromLog()
    {
        $separated=$this->getSeparatedCommand();
        $command= $this->isReadytoCheckFile();
        $explode=array();
        if(trim($command)!='')
        {
            $explode=  explode($separated, $command);
        }
        $commandArr=array();
        if(!empty($explode))
        {
            $no=1;
            $index=0;
            foreach($explode AS $key=>$val)
            {
                switch($no)
                {
                    case 1:
                    $commandArr[$index][]=array(
                        'method'=>$val,
                    );

                    break;    
                    case 2:
                    $commandArr[$index][]=array(
                        'status'=>$val,
                    );

                    break;    
                    case 3:
                    $commandArr[$index][]=array(
                        'date'=>$val,
                    );

                    break;    
                }
                if($no==3)
                {
                    $no=0; //balikan ke 1
                    $index++;
                }
                $no++;
            }
        }
        
        
        $newArr=array();
        if(!empty($commandArr))
        {
            foreach($commandArr AS $key=>$dataArr)
            {
                $newArr[$key]=array(
                    'method'=>$dataArr[0]['method'],
                    'status'=>$dataArr[1]['status'],
                    'date'=>$dataArr[2]['date'],
                );
            }
        }
        return $newArr;
    }
    
  
    
    /**
     * dapatkan log file
     */
    public function getFileLog()
    {
        return $this->dirDefault.$this->fileLog;
    }
    
    public function getSeparatedCommand()
    {
        return $this->separatedCommand;
    }
    
    /**
     * menjalankan perintah yang telah disiapkan di log
     */
    protected function runCommand()
    {
        $commandArr=$this->buildArrayFromLog();
        if(!empty($commandArr))
        {
          //  chmod($regLib->getFileLog(),0777);

            $no=1;
            $words='';
            foreach($commandArr AS $row)
            {
                $status=$row['status'];
                $method=$row['method'];
                if($no==1)
                {
                      $words=$row['method'].'|1|'.$row['date'];
                }
                else
                {
                      $words.='|'.$row['method'].'|1|'.$row['date'];
                }
                //execute
                if($status==0)
                {
                    if(method_exists($this, $method))
                    {
                        $this->$method();//jalankan method
                    }
                }
                $no++;
            }
            
            if(trim($words)!='')
            {
                $fh=fopen($this->getFileLog(), 'w+');
                fwrite($fh,$words);
                fclose($fh);
            }
        }
    }
    
     /**
     * pengecekkan table baru
     */
    protected function runExistMyTable()
    {
        $this->myTable=$this->tableList();
        if(!empty($this->myTable))
        {
            foreach($this->myTable AS $table=>$query)
            {
                $isExecute=$this->isExistMyTable($table);
                if($isExecute)
                {
                    $this->CI->db->query($query);
                    if(count($this->myTable)>1)
                    {
                        sleep(0.5);

                    }
                }
            }
        }
    }
    
    /**
     * query pengecekkan
     */
    protected function isExistMyTable($table)
    {
        $result=false;
        $sql='SELECT COUNT(*) AS num
                FROM information_schema.tables 
                WHERE table_schema = "'.$this->myDb.'"
                AND table_name = "'.$table.'"';
        
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        if(!empty($row) AND $row['num']<1)
        {
           $result=true; 
        }
        
        return $result;
    }


    /**
     * daftar table baru
     */
    protected function tableList()
    {
        $myTable=array();
        $myTable['site_administrator']="
                   
                    CREATE TABLE IF NOT EXISTS `site_administrator` (
                    `admin_id` int(15) NOT NULL AUTO_INCREMENT,
                    `admin_group_id` int(15) NOT NULL DEFAULT '0',
                    `admin_username` varchar(255) NOT NULL DEFAULT 'admin',
                    `admin_password` varchar(255) NOT NULL DEFAULT 'admin',
                    `admin_last_login` datetime DEFAULT '0000-00-00 00:00:00',
                    `admin_is_active` enum('0','1') NOT NULL DEFAULT '1',
                    PRIMARY KEY (`admin_id`),
                    KEY `admin_group_id` (`admin_group_id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

                ";
        $myTable['site_administrator_group']="
                   
                CREATE TABLE IF NOT EXISTS `site_administrator_group` (
                `admin_group_id` int(15) NOT NULL AUTO_INCREMENT,
                `admin_group_title` varchar(255) NOT NULL,
                `admin_group_is_active` enum('0','1') NOT NULL DEFAULT '1',
                `admin_group_type` enum('superuser','administrator') NOT NULL DEFAULT 'administrator',
                PRIMARY KEY (`admin_group_id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

                ";
        $myTable['site_administrator_menu']="
                    CREATE TABLE IF NOT EXISTS `site_administrator_menu` (
                    `menu_administrator_id` int(15) NOT NULL AUTO_INCREMENT,
                    `menu_administrator_par_id` int(15) NOT NULL DEFAULT '0',
                    `menu_administrator_title` varchar(255) DEFAULT NULL,
                    `menu_administrator_description` text,
                    `menu_administrator_link` varchar(255) DEFAULT NULL,
                    `menu_administrator_page_id` int(15) NOT NULL DEFAULT '0',
                    `menu_administrator_order_by` int(15) NOT NULL DEFAULT '0',
                    `menu_administrator_location` enum('user','member','stockist','admin') NOT NULL,
                    `menu_administrator_is_active` enum('0','1') NOT NULL DEFAULT '0',
                    `menu_administrator_module` varchar(50) DEFAULT NULL COMMENT '[admin]',
                    `menu_administrator_controller` varchar(50) DEFAULT NULL COMMENT '[admin]',
                    `menu_administrator_method` varchar(100) DEFAULT NULL COMMENT '[admin]tiap method dipisahkan dengan #',
                    PRIMARY KEY (`menu_administrator_id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;
                ";
        $myTable['site_administrator_privilege']="
                CREATE TABLE IF NOT EXISTS `site_administrator_privilege` (
                `administrator_privilege_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `administrator_privilege_administrator_group_id` int(15) NOT NULL,
                `administrator_privilege_administrator_menu_id` int(10) unsigned NOT NULL,
                `administrator_privilege_method_allowed` varchar(100) NOT NULL,
                PRIMARY KEY (`administrator_privilege_id`),
                KEY `administrator_privilege_administrator_group_id` (`administrator_privilege_administrator_group_id`,`administrator_privilege_administrator_menu_id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

                ";
        return $myTable;
        
    }
    
    /**
     * menjalankan query relasi atau penambahan kolom
     * yang dijalankan adalah status 0
     */
    public function runAdditionalQuery()
    {
        $queryArr=$this->listQuery();
        

        if(!empty($queryArr))
        {
            foreach($queryArr AS $result)
            {
                $query=$result['query'];
                $status=$result['status'];
                if($status==0) //jalankan query dengan status 0
                {
                    $this->CI->db->query($query);
                }
            }
        }
    }
    
    /**
     * list query yang akan dijalankan
     * pastikan status di set 1 kembali jika telah dijalankan.
     * karena sistem buka tutup baru dari log.txt, belum ke core_lib
     * eq. $query[]=array(
            'query'=>'ALTER TABLE `sys_member`
                      ADD CONSTRAINT `sys_member_ibfk_1` FOREIGN KEY (`member_serial_number`) REFERENCES `sys_serial` (`serial_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                ',
            'status'=>0,
        );
     */
    protected function listQuery()
    {
        $query=array();
        
        $query[]=array(
            'query'=>'
                ALTER TABLE `site_administrator`
                ADD CONSTRAINT `site_administrator_ibfk_1` FOREIGN KEY (`admin_group_id`) REFERENCES `site_administrator_group` (`admin_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ',
            'status'=>0,
        );
        $query[]=array(
            'query'=>'
               ALTER TABLE `site_administrator_privilege`
                ADD CONSTRAINT `site_administrator_privilege_ibfk_1` FOREIGN KEY (`administrator_privilege_administrator_group_id`) REFERENCES `site_administrator_group` (`admin_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ',
            'status'=>0,
        );
        $query[]=array(
            'query'=>'
            INSERT INTO `site_administrator_group` (`admin_group_id`, `admin_group_title`, `admin_group_is_active`, `admin_group_type`) VALUES
            (9, \'Superuser\', \'1\', \'superuser\');              
                ',
            'status'=>0,
        );
        $query[]=array(
            'query'=>'
                INSERT INTO `site_administrator` (`admin_id`, `admin_group_id`, `admin_username`, `admin_password`, `admin_last_login`, `admin_is_active`) VALUES
                (4, 9, \'admin\', \'9638b0b7a1c9044a02fcfda4e9bc14e9\', \'2013-05-31 09:16:50\', \'1\');
                ',
            'status'=>0,
        );
        $query[]=array(
            'query'=>'
            
            INSERT INTO `site_administrator_menu` (`menu_administrator_id`, `menu_administrator_par_id`, `menu_administrator_title`, `menu_administrator_description`, `menu_administrator_link`, `menu_administrator_page_id`, `menu_administrator_order_by`, `menu_administrator_location`, `menu_administrator_is_active`, `menu_administrator_module`, `menu_administrator_controller`, `menu_administrator_method`) VALUES
            (8, 11, \'Group\', \'untuk konfigurasi group dan privilege group\', \'admin/admin_group/index\', 0, 1, \'admin\', \'1\', \'admin_group\', \'admin\', \'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item\'),
            (9, 11, \'User\', \'Untuk memanajemen user/admin\', \'admin/admin_user/index\', 0, 2, \'admin\', \'1\', \'admin_user\', \'admin\', \'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item\'),
            (10, 11, \'Menu\', \'Untuk memanajemen menu\', \'admin/admin_menu/index\', 0, 3, \'admin\', \'1\', \'admin_menu\', \'admin\', \'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item\'),
            (11, 0, \'Settings\', \'settings group, user dan menu\', \'\', 0, 1, \'admin\', \'1\', \'-\', \'-\', \'-\');
                ',
            'status'=>0,
        );
        //ALTER TABLE `site_administrator_group` ADD `admin_group_type` ENUM( 'superuser', 'administrator' ) NOT NULL DEFAULT 'administrator'
        return $query;
    }
    
      
}

?>
