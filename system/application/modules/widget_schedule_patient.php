<?php 
/**
* @author Ari Armanda
*/
class widget_schedule_patient extends widget_lib
{
	public function run($module,$class_widget)
	{
		$data['results'] = $this->get_data_schedule();
		$this->render(get_class(),$data);
	}
	function get_data_schedule(){
		$CI = & get_instance(); $dataArr = array();
		$sql = 'SELECT schedule_service_id,schedule_service_name FROM sys_schedule WHERE DATE(schedule_datetime) = "'.date('Y-m-d').'" GROUP BY schedule_service_id';
		$exec = $CI->db->query($sql);
		$results = $exec->result_array();

		foreach ($results as $rowArr) {
			foreach ($rowArr as $key => $value) {
				${$key}=$value;
			}
			$dataArr[] = array(
				'schedule_service_name' => $schedule_service_name,
				'schedule_queue_value' => $CI->function_lib->get_one('schedule_queue_value','sys_schedule','schedule_service_id='.$schedule_service_id.' ORDER BY schedule_datetime DESC'),
				'schedule_dokter_name' => $CI->function_lib->get_one('schedule_dokter_name','sys_schedule','schedule_service_id='.$schedule_service_id.' ORDER BY schedule_datetime DESC'),
			);
		}

		return $dataArr;
	}
}
?>