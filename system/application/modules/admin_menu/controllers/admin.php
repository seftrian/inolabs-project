<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller
{

    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct()
    {
        parent::__construct();

        //load model
        $this->load->model(array('admin_menu'));
        $this->load->helper(array('pagination'));

        //inisialisasi model
        $this->mainModel = new admin_menu;
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }

    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';

        $this->data['title_method'] = 'Data Menu';
        $this->data['desc_method'] = 'Daftar Menu';

        //lokasi  menu
        $location = (isset($_GET['location']) and trim($_GET['location']) != '') ? $this->input->get('location', true) : 'admin';

        $where = 'menu_administrator_par_id=0 AND menu_administrator_location="' . $location . '"';

        $this->data['dataMenu'] = $this->mainModel->findAll($where, 'no limit', 'no offset');

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template


        //untuk hapus item yang diseleksi
        $this->delete_selected_item();

        //aktifkan item yang diseleksi
        $this->publish_selected_item();

        //non aktifkan item yang diseleksi
        $this->unpublish_selected_item();


        $this->data['location'] = $location;

        $this->data['seoTitle'] = 'Data Menu ' . ucfirst($location);
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => $this->data['seoTitle'],
            'class' => "clip-folder",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);

        template($this->themeId,  get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk menambahkan data
     */
    public function create()
    {

        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';
        $location = isset($_GET['location']) ? $this->input->get('location', true) : 'admin';
        $this->data['location'] = $location;
        if ($this->input->post('save')) {
            $_POST['menu_administrator_is_active'] = isset($_POST['menu_administrator_is_active']) ? $_POST['menu_administrator_is_active'] : '0';
            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation();
            $status = $isValidationPassed['status'];
            $message = ($status == 500 and $isValidationPassed['message'] == '') ? validation_errors() : $isValidationPassed['message'];

            if ($status == 200) {

                $columns = array(
                    'menu_administrator_par_id' => $this->input->post('menu_administrator_par_id', true),
                    'menu_administrator_title' => $this->input->post('menu_administrator_title', true),
                    'menu_administrator_description' => $this->input->post('menu_administrator_description', true),
                    'menu_administrator_link' => $this->input->post('menu_administrator_link', true),
                    'menu_administrator_page_id' => 0,
                    'menu_administrator_order_by' => $this->input->post('menu_administrator_order_by', true),
                    'menu_administrator_location' => $this->input->post('menu_administrator_location', true),
                    'menu_administrator_is_active' => $this->input->post('menu_administrator_is_active', true),
                    'menu_administrator_module' => $this->input->post('menu_administrator_module', true),
                    'menu_administrator_controller' => $this->input->post('menu_administrator_controller', true),
                    'menu_administrator_method' => $this->input->post('menu_administrator_method', true),
                    'menu_administrator_class_css' => $this->input->post('menu_administrator_class_css', true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved = $this->mainModel->save();
                $status = $hasSaved['status']; //status saat penyimpanan
                if ($status == 200) {

                    $msg = base64_encode('Data berhasil disimpan');
                    redirect(base_url() . 'admin/' . $this->currentModule . '/index?location=' . $location . '&status=200&msg=' . $msg);
                }
            }
        }

        $this->data['status'] = $status;
        $this->data['message'] = $message;

        //lokasi penempatan menu
        $this->data['locationArr'] = array('admin' => 'Admin', 'user' => 'Depan Kiri', 'member' => 'Voffice', 'stockist' => 'Stockist', 'user_footer' => 'Depan Footer', 'user_header' => 'Depan Header');


        $whereAdd = 'menu_administrator_location="' . $location . '"';
        //data recursive
        $initDataMenu = $this->mainModel->initalizeRecursive(0, $whereAdd);
        $this->data['recursiveDataMenu'] = $this->mainModel->buildMenuDropDown(0, $initDataMenu);

        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Tambah Menu ' . ucfirst($location);
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Data Menu ' . ucfirst($location),
            'class' => "clip-folder",
            'link' => base_url() . 'admin/admin_menu/index?location=' . $location,
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => $this->data['seoTitle'],
            'class' => "clip-pencil",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);

        template($this->themeId,  get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id = 0)
    {

        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }


        $status = (isset($_GET['status']) and trim($_GET['status']) != '') ? $_GET['status'] : 200;
        $message = (isset($_GET['msg']) and trim($_GET['msg']) != '') ? base64_decode($_GET['msg']) : '';
        $location = isset($_GET['location']) ? $this->input->get('location', true) : 'admin';

        if ($this->input->post('save')) {
            $_POST['menu_administrator_is_active'] = isset($_POST['menu_administrator_is_active']) ? $_POST['menu_administrator_is_active'] : '0';
            //inisialisasi validation
            $isValidationPassed = $this->mainModel->formValidation($id);
            $status = $isValidationPassed['status'];
            $message = ($status == 500 and $isValidationPassed['message'] == '') ? validation_errors() : $isValidationPassed['message'];

            if ($status == 200) {
                $columns = array(
                    'menu_administrator_par_id' => $this->input->post('menu_administrator_par_id'),
                    'menu_administrator_title' => $this->input->post('menu_administrator_title'),
                    'menu_administrator_description' => $this->input->post('menu_administrator_description'),
                    'menu_administrator_link' => $this->input->post('menu_administrator_link'),
                    'menu_administrator_page_id' => 0,
                    'menu_administrator_order_by' => $this->input->post('menu_administrator_order_by'),
                    'menu_administrator_location' => $this->input->post('menu_administrator_location'),
                    'menu_administrator_is_active' => $this->input->post('menu_administrator_is_active'),
                    'menu_administrator_module' => $this->input->post('menu_administrator_module'),
                    'menu_administrator_controller' => $this->input->post('menu_administrator_controller'),
                    'menu_administrator_method' => $this->input->post('menu_administrator_method'),
                    'menu_administrator_class_css' => $this->input->post('menu_administrator_class_css', true),
                );
                $this->mainModel->setPostData($columns);
                $where = 'menu_administrator_id=' .  intval($id);
                $hasSaved = $this->mainModel->save($where);
                $status = $hasSaved['status']; //status saat penyimpanan
                if ($status == 200) {
                    $msg = base64_encode('Data berhasil disimpan');
                    if (isset($_GET['redirect'])) {
                        redirect(rawurldecode($_GET['redirect']));
                    } else {
                        redirect(base_url() . 'admin/' . $this->currentModule . '/index?location=' . $location . '&status=200&msg=' . $msg);
                    }
                }
            }
        }
        $this->data['status'] = $status;
        $this->data['message'] = $message;
        $this->data['rowMenu'] = $this->mainModel->find('menu_administrator_id=' .  intval($id));
        if (empty($this->data['rowMenu'])) {
            show_error('Request anda tidak valid', 500);
        }


        //data group admin
        $this->data['locationArr'] = array('admin' => 'Admin', 'user' => 'Depan Kiri', 'member' => 'Voffice', 'stockist' => 'Stockist', 'user_footer' => 'Depan Footer', 'user_header' => 'Depan Header');


        $whereAdd = 'menu_administrator_location="' . $location . '"';

        //data recursive
        $initDataMenu = $this->mainModel->initalizeRecursive($id, $whereAdd);
        $this->data['recursiveDataMenu'] = $this->mainModel->buildMenuDropDown(0, $initDataMenu, '', $id);
        $this->data['location'] = $location;
        $this->data['footerScript'] = $this->footerScript(__FUNCTION__);
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        template($this->themeId,  get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * untuk update data group
     * @param int $groupId
     */
    public function view($id = 0)
    {

        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }

        $message = '';
        $status = 200;
        $this->data['status'] = $status;
        $this->data['message'] = $message;
        $this->data['rowMenu'] = $this->mainModel->find('menu_administrator_id=' .  intval($id));
        if (empty($this->data['rowMenu'])) {
            show_404();
        }

        $this->data['seoTitle'] = 'Detil Menu';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url() . 'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Data Menu ' . ucfirst($this->data['rowMenu']['menu_administrator_location']),
            'class' => "clip-folder",
            'link' => base_url() . 'admin/admin_menu/index?location=' . $this->data['rowMenu']['menu_administrator_location'],
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => $this->data['seoTitle'],
            'class' => "clip-zoom-in",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array, $this->data['seoTitle'], $description, $this->templateName, $this->themeId);

        template($this->themeId,  get_class() . '/' . __FUNCTION__, $this->data);
    }

    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    public function delete($id = 0, $output = 'redirect')
    {
        if (!is_numeric($id) or $id == 0) {
            show_error('Request anda tidak valid', 500);
        }
        $where = 'menu_administrator_id=' .  intval($id);
        $this->data['rowAdmin'] = $this->mainModel->find($where);
        $_GET['location'] = isset($_GET['location']) ? $_GET['location'] : 'admin';

        if (!empty($this->data['rowAdmin'])) {

            //pengecekkan submenu 
            //jika ada maka set submenu dengan par id 0
            $hasSubmenu = $this->mainModel->hasSubmenu($id);
            if ($hasSubmenu) {
                $whereSubmenu = 'menu_administrator_par_id=' .  intval($id);
                $columns = array(
                    'menu_administrator_par_id' => 0,

                );
                $this->mainModel->setPostData($columns);
                $this->mainModel->save($whereSubmenu);
            }

            $isDelete = $this->mainModel->delete($where);
            switch ($output) {
                case 'json':
                    echo json_encode($isDelete);
                    break;
                case 'array':
                    return $isDelete;
                    break;
                default:

                    $status = $isDelete['status'];
                    $message = $isDelete['message'];
                    $_GET['redirect'] =  rawurlencode(base_url() . 'admin/' . $this->currentModule . '/index?location=' . $_GET['location'] . '&status=' . $status . '&msg=' .  base64_encode($message));

                    if (isset($_GET['redirect']) and trim($_GET['redirect']) != '') {

                        redirect(rawurldecode($_GET['redirect']));
                    } else {
                        redirect(base_url() . 'admin/' . $this->currentModule . '/index?status=' . $isDelete['status'] . '&msg=' . base64_encode($isDelete['message']));
                    }
                    break;
            }
        } else {
            $msg = 'Permintaan tidak ditemukan';
            $_GET['redirect'] =  rawurlencode(base_url() . 'admin/' . $this->currentModule . '/index?location=' . $_GET['location'] . '&status=500&msg=' .  base64_encode($msg));

            if (isset($_GET['redirect']) and trim($_GET['redirect']) != '') {

                redirect(rawurldecode($_GET['redirect']));
            } else {
                redirect(base_url() . 'admin/' . $this->currentModule . '/index?status=500&msg=' . base64_encode($msg));
            }
        }
    }

    /**
     * untuk menghapus item yang diseleksi
     */
    public function delete_selected_item()
    {
        if ($this->input->post('delete_selected_item') and auth_admin_lib::manualProctection('admin', 'delete_selected_item')) {
            $adminArr = isset($_POST['id']) ? $_POST['id'] : '';
            if (!empty($adminArr)) {
                $isDelete = array(
                    'status' => 200,
                    'message' => 'Data berhasil di hapus',
                );
                foreach ($adminArr as $userId) {
                    $isDelete = $this->delete($userId, 'array');
                }


                $status = $isDelete['status'];
                $message = $isDelete['message'];
                $_GET['redirect'] =  rawurlencode(base_url() . 'admin/' . $this->currentModule . '/index?location=' . $_GET['location'] . '&status=' . $status . '&msg=' .  base64_encode($message));


                if (isset($_GET['redirect']) and trim($_GET['redirect']) != '') {

                    redirect(rawurldecode($_GET['redirect']));
                } else {
                    redirect(base_url() . 'admin/' . $this->currentModule . '/index?status=' . $isDelete['status'] . '&msg=' .  base64_encode($isDelete['message']));
                }
            }
        }
    }

    /**
     * untuk menghapus item yang diseleksi
     */
    public function publish_selected_item()
    {
        if ($this->input->post('publish_selected_item') and auth_admin_lib::manualProctection('admin', 'publish_selected_item')) {
            $groupAdminArr = isset($_POST['id']) ? $_POST['id'] : '';
            if (!empty($groupAdminArr)) {
                $status = 200; //status saat penyimpanan
                $message = '';
                foreach ($groupAdminArr as $userId) {
                    $columns = array(
                        'menu_administrator_is_active' => 1,
                    );

                    $this->mainModel->setPostData($columns);
                    $where = 'menu_administrator_id=' .  intval($userId);
                    $hasSaved = $this->mainModel->save($where);
                    $status = $hasSaved['status']; //status saat penyimpanan
                    $message = 'Data berhasil diaktifkan';
                }

                $_GET['redirect'] =  rawurlencode(base_url() . 'admin/' . $this->currentModule . '/index?location=' . $_GET['location'] . '&status=' . $status . '&msg=' .  base64_encode($message));

                if (isset($_GET['redirect']) and trim($_GET['redirect']) != '') {

                    redirect(rawurldecode($_GET['redirect']));
                } else {
                    redirect(base_url() . 'admin/' . $this->currentModule . '/index?status=' . $status . '&msg=' .  base64_encode($message));
                }
            }
        }
    }

    /**
     * untuk menghapus item yang diseleksi
     */
    public function unpublish_selected_item()
    {
        if ($this->input->post('unpublish_selected_item') and auth_admin_lib::manualProctection('admin', 'unpublish_selected_item')) {
            $groupAdminArr = isset($_POST['id']) ? $_POST['id'] : '';
            if (!empty($groupAdminArr)) {
                $status = 200; //status saat penyimpanan
                $message = '';
                foreach ($groupAdminArr as $userId) {
                    $columns = array(
                        'menu_administrator_is_active' => 0,
                    );

                    $this->mainModel->setPostData($columns);
                    $where = 'menu_administrator_id=' .  intval($userId);
                    $hasSaved = $this->mainModel->save($where);
                    $status = $hasSaved['status']; //status saat penyimpanan
                    $message = 'Data berhasil dinonaktifkan';
                }

                $_GET['redirect'] =  rawurlencode(base_url() . 'admin/' . $this->currentModule . '/index?location=' . $_GET['location'] . '&status=' . $status . '&msg=' .  base64_encode($message));

                if (isset($_GET['redirect']) and trim($_GET['redirect']) != '') {

                    redirect(rawurldecode($_GET['redirect']));
                } else {
                    redirect(base_url() . 'admin/' . $this->currentModule . '/index?status=' . $status . '&msg=' .  base64_encode($message));
                }
            }
        }
    }

    /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
     * @param string $method
     * @param array $extraVariable
     * **/
    protected function footerScript($method = '', $extraVariable = array())
    {

        //set variable
        if (!empty($extraVariable)) {
            extract($extraVariable);
        }

        ob_start();
?>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/jquery-ui.css" />
        <script src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>
        <!-- flexigrid starts here -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
        <!-- flexigrid ends here -->
        <link href="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="<?php echo $this->themeUrl; ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
        <script type="text/javascript">
            var UIModals = function() {
                //function to initiate bootstrap extended modals
                var initModals = function() {
                    $.fn.modalmanager.defaults.resize = true;
                    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
                        '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                        '<div class="progress progress-striped active">' +
                        '<div class="progress-bar" style="width: 100%;"></div>' +
                        '</div>' +
                        '</div>';

                };
                return {
                    init: function() {
                        initModals();

                    }
                };
            }();

            function add_modal() {
                var $modal = $('#ajax-modal');
                // create the backdrop and wait for next modal to be triggered
                $('body').modalmanager('loading');

                setTimeout(function() {
                    $modal.load('<?php echo base_url('admin/master_content/load_external_link') ?>', '', function() {
                        $modal.modal();
                    });
                }, 100);
            }
            /*
            ambil link    
            */
            function load_link_from_content(link, preview_link) {
                var current_label = $("#link_label").val();
                $("#link").val(link);
                if ($.trim(current_label) == '') {
                    $("#link_label").val('Selengkapnya');
                }
                $('#ajax-modal').modal('toggle');
                $("#preview_link").html('<a href="' + preview_link + '" target="_blank"><b>Preview</b></a>');
                $("#link").focus();
                return false;
            }
        </script>
        <?php
        switch ($method) {
            default:
        ?>
                <script type="text/javascript">
                    module = $('input[name="menu_administrator_module"]');
                    controller = $('input[name="menu_administrator_controller"]');
                    method = $('textarea[name="menu_administrator_method"]');
                    module, controller.keyup(function(e) {
                        e.preventDefault();
                        $.get('<?php echo base_url() . 'admin_menu/service_rest/get_controller_public_method' ?>?module=' + module.val() + '&controller=' + controller.val()).
                        done(function(output) {
                            method.val(output);
                        })
                    });
                </script>
<?php
                break;
        }

        $footerScript = ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>