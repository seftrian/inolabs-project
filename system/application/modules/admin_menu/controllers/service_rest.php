<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class service_rest extends front_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('admin_menu',));
        $this->load->library(array('system_lib',));
        
        //inisialisasi model
        $this->mainModel=new admin_menu;
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }
    
    /**
     * update public method menu
     */
    public function update_menu_public_method($menuId,$update='false')
    {
        $rowMenu=$this->mainModel->find('menu_administrator_id='.intval($menuId));
        $strMethod=false;
        if(!empty($rowMenu))
        {
            $module = $rowMenu['menu_administrator_module'];
            $controller = $rowMenu['menu_administrator_controller'];
            if(trim($module)!='' AND trim($controller)!='')
            {
                $publicMethodArr=$this->system_lib->getPublicMethodOnControllerReleaseRequire($module,$controller);
                if(!empty($publicMethodArr))
                {
                    foreach($publicMethodArr AS $key=>$row)
                    {
                        $strMethod.=($key>0)?'#'.$row:$row;
                    }
                    if($update=='true')
                    {
                        $sql='UPDATE site_administrator_menu SET menu_administrator_method="'.$strMethod.'"
                            WHERE menu_administrator_id='.intval($menuId);
                        $this->db->query($sql);
                        $strMethod='true';
                    }
                }
                
            }
        }
        echo 'true';
        
    }
    
    /**
     * recursive tree privilege service
     * @param int $parentId 
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     * 
     */
    public function buildTreePrivilegeService($parentId, $menuData,$no=0,$groupId=0) {
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $module = $menuData['items'][$itemId]['menu_administrator_module'];
                $controller = $menuData['items'][$itemId]['menu_administrator_controller'];
//                $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
//                $isChecked=$isAllowedAccess?'checked':'';
//                echo $name.'<br />';

//                $hasSubmenu=$this->hasSubmenu($id);
                //proses update method
                
                if(trim($module)!='' AND trim($controller)!='')
                {
                    echo $module.'/'.$controller.'<br />';
                        $publicMethodArr=$this->system_lib->getPublicMethodOnControllerReleaseRequire($module,$controller);
                        
                    // echo '<pre>';
                    // print_r($publicMethodArr);
                    //break;
                    sleep(0.1);

                }
                
                $no++;
                // find childitems recursively 
                $html .= $this->buildTreePrivilegeService($itemId, $menuData,$no,$groupId);
            }

        }

        return $html;
    }
    
    /**
     * dapatkan nomor urut menu
     * @param int $parId 
     * @param int $id id menu tsb, <br />
     * sehingga pada saat update tidak perlu memberikan saran untuk penomoran
     */
    public function my_number($parId=0,$currentParId=0,$id=0,$location='admin')
    {
        $myNumber=1;
        if(is_numeric($parId))
        {
            $myNumber=  admin_menu::getMyNumber($parId,$currentParId,$id,$location);
        }
        
        echo json_encode($myNumber);
    }
    
    /**
     * set ke atas atau ke bawah
     * @param int $menuId
     * @param int $menuParId
     * @param string $pos
     */
    public function setPosition($menuId,$menuParId,$pos)
    {
        
       admin_menu:: setPosition($menuId,$menuParId,$pos);
       if(isset($_GET['redirect']) AND trim($_GET['redirect'])!='')
       {
           
           redirect(rawurldecode($_GET['redirect']));
       }
    }
    
    /**
     * fungsi untuk mendapatkan public method
     */
    public function get_controller_public_method()
    {
        $module=isset($_GET['module'])?$_GET['module']:'';
        $controller=isset($_GET['controller'])?$_GET['controller']:'';
        $methods=$this->system_lib->getPublicMethodOnController($module,$controller);

        $html='';
        if(!empty($methods))
        {
     
            foreach($methods AS $name)
            {
                $html.=$name.'#';
            }
        }
                
        echo $html;
    }
}

?>
