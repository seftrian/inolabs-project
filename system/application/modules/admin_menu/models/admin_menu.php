<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of
 *
 * @author tejomurti
 */
//require_once dirname(__FILE__).'/../components/core_menu_lib.php';

//class admin_menu extends core_menu_lib{
class admin_menu{
    //put your code here

    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_administrator_menu';
    public function __construct() {

  //      parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));
        $this->CI->load->model(array('auth/auth_admin_lib'));

        if(!session_id())
        {
            session_start();
        }

        $this->websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();


    }
         public function buildTreeMenu_customsks($parentId, $menuData,$no=0,$groupId=0,$currentId=0,$additionalClass='') {
        $html='';
        $initClass=($no==0)?'sf-js-enabled':$additionalClass;
        if (isset($menuData['parents'][$parentId])) {
            // $html = '<ul>';
            $html = '';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
                $isChecked=$isAllowedAccess?'checked':''; //echo '<pre>'; print_r($name); exit;
                $isCurrent=($currentId==$id)?'current':(trim($this->CI->uri->segment(1))=="" AND trim($name)=="Home")?'current':'';
                $html.='<li class="'.$additionalClass.'">';
                
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $link= base_url().$menuData['items'][$itemId]['menu_administrator_link'];
                    $html.='<a class="" href="'.$link.'">
                                	'.$name.'
                                </a>';
                }
                else
                {
					$additionalClass=='medium-list';
                    $html.='<a href="#">
                                	<!-- Icon Container -->
                                	
                                	'.$name.'
                                    <i class="icon-angle-down"></i>
                                </a>';
					$html .= '<ul class="'.$initClass.'">';
                }
                
                $no++;
                // find childitems recursively 
                $html .= $this->buildTreeMenu_customsks($itemId, $menuData,$no,$groupId,$additionalClass);
                $html.='</li>';
                //$html .= '</tr>'; 
            }
            $html.='</ul>';

        }

        return $html; 
    }
	/**
    mencari parent current id dan parent id
    @param string $url
    */
    public function get_current_id_and_parent($url)
    {
        $where='menu_administrator_link="'.$url.'" ORDER BY menu_administrator_id DESC';
        $current_id=$this->CI->function_lib->get_one('menu_administrator_id','site_administrator_menu',$where);
        $data=array();
        $data[1]=$current_id;
        if($current_id>0)
        {
            do
            {
                $where='menu_administrator_id='.$current_id;
                $par_id=$this->CI->function_lib->get_one('menu_administrator_par_id','site_administrator_menu',$where);
                if($par_id>0)
                {
                    $data[]=$par_id;
                }
                $current_id=$par_id;
            }
            while($par_id!=0);
        }

        return $data;

    }

    /**
     * dapatkan satu baris data
     * @param string $where
     *
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.'
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='menu_administrator_order_by ASC')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.'
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }

    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.'
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }

    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';

        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;

            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }


        return array(
            'status'=>$status,
            'message'=>$message,
        );

    }

    /**
     * untuk validasi form
     *
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='';


            $mainConfig = array(
                array(
                        'field'   => 'menu_administrator_par_id',
                        'label'   => 'Parent',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'menu_administrator_title',
                        'label'   => 'Menu',
                        'rules'   => 'trim|required|min_length[2]'
                    ),
                array(
                        'field'   => 'menu_administrator_location',
                        'label'   => 'Lokasi',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'menu_administrator_order_by',
                        'label'   => 'Nomor Urut',
                        'rules'   => 'trim|required|numeric'
                    ),
                array(
                        'field'   => 'menu_administrator_is_active',
                        'label'   => 'Status',
                        'rules'   => 'trim|required'
                    ),

            );

          $additionalConfig = array(
                array(
                        'field'   => 'menu_administrator_module',
                        'label'   => 'Modul',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'menu_administrator_controller',
                        'label'   => 'Controller',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'menu_administrator_method',
                        'label'   => 'Method',
                        'rules'   => 'trim|required'
                    ),
            );

          $config=$mainConfig;
          /**
           * jika lokasi admin, maka module controller dan method wajib diisi
           * main config dan additional config akan digabung
           */
          if(isset($_POST['menu_administrator_location']) AND $_POST['menu_administrator_location']=='admin')
          {
              $config=  array_merge($mainConfig,$additionalConfig);
          }
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {

              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';

          }

          return array(
              'status'=>$status,
              'message'=>$message,
          );

    }

    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }

    /**
     * check ketersediaan data username
     * @param stirng $query
     * @param int $userId default 0 saat create
     * @return bool true is valid
     */
    public function isExistUserName($query, $userId=0)
    {
        $value=TRUE;
        //update
        if(is_numeric($userId) AND $userId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_username',$this->mainTable,'admin_username LIKE TRIM(\''.$query.'\') AND admin_id!='.  intval($userId));
        }
        else//create
        {
            $isFounded=$this->CI->function_lib->get_one('admin_username',$this->mainTable,'admin_username LIKE TRIM(\''.$query.'\')');
        }

        if($isFounded)
        {
            $value=FALSE;
        }

        return $value;
    }

    /**
     * check password saat ini
     * @param stirng $query
     * @param int $userId default 0 saat create
     * @return bool true is valid
     */
    public function isCurrentPassword($query, $userId=0)
    {
        $value=false;
        //update
        if(is_numeric($userId) AND $userId!=0)
        {
             $isFounded=$this->CI->function_lib->get_one('admin_password',$this->mainTable,'admin_password="'.md5($query).'" AND admin_id='.  intval($userId));
        }

        if($isFounded)
        {
            $value=true;
        }

        return $value;
    }

    /**
     * format yang diizinkan untuk username dengan menghilangkan spasi
     * @param string $str
     */
    public function username_format($str)
    {

        if (! preg_match("/^([-a-z-0-9_])+$/i", $str))
        {
                $this->form_validation->set_message('username_format', 'The %s field can not be the word "test"');
                return FALSE;
        }
        else
        {
            $this->form_validation->set_message('username_format', 'The %s field can not be the word "test"');
                return FALSE;
                //return TRUE;
        }
      //  return ( ! preg_match("/^([-a-z-0-9_])+$/i", $str)) ? FALSE : TRUE;
    }

    /**
     * pengambilan data secara recursive
     * @param int $id default 0. ketika update set sesuai menu id, sehingga tidak memunculkan dirinya sendiri.
     * @param string $whereAdd
     */
     public function initalizeRecursive($id = 0,$whereAdd='') {
        $where = 1;
        if ($id != 0 AND is_numeric($id)) {
            $where = 'menu_administrator_id!=' . intval($id);
        }

        if(trim($whereAdd)!='')
        {
            $where.=' AND '.$whereAdd;
        }
        $list_data = $this->findAll($where,'no limit','no offset');
        $data_build = array(
            'items' => array(),
            'parents' => array(),
        );

        foreach ($list_data AS $val) {
            $data_build['items'][$val['menu_administrator_id']] = $val;
            $data_build['parents'][$val['menu_administrator_par_id']][] = $val['menu_administrator_id'];
        }

        return $data_build;
    }

    /**
     * sama dengan method initalizeRecursive(),
     * tetapi yg ini lebih flexible u/ set parameter where
     *
     * @param string $where
     */
     public function initalizeRecursiveDynamic($where=1) {
         $list_data = $this->findAll($where,'no limit','no offset');
        $data_build = array(
            'items' => array(),
            'parents' => array(),
        );

        foreach ($list_data AS $val) {
            $data_build['items'][$val['menu_administrator_id']] = $val;
            $data_build['parents'][$val['menu_administrator_par_id']][] = $val['menu_administrator_id'];
        }

        return $data_build;
    }
     /**
     * recursive dengan drop down
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param string $nbsp pemisah karakter
     *
     */
     public function buildMenu($parentId, $menuData, $nbsp = '') {
        $html = '';
        $nbsp.=($parentId == 0) ? ' ' : ' ';
        if (isset($menuData['parents'][$parentId])) {

            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $desc = $menuData['items'][$itemId]['menu_administrator_description'];
                $loc = $menuData['items'][$itemId]['menu_administrator_location'];
                $parName=self::getParentName($parId);
                $myNum=$menuData['items'][$itemId]['menu_administrator_order_by'];

                $menuIsActive=$menuData['items'][$itemId]['menu_administrator_is_active'];

                $msg=  base64_encode('Posisi berhasil diatur');
                $redirect=  rawurlencode(base_url().'admin/admin_menu/index?location='.$loc.'&status=200&msg='.$msg).'#'.$id;
                $upDownMenu= self::upDownMenu($parId, $myNum,$id,$redirect,$loc);

//                    $nbsp.=!empty($menuData)?'&nbsp;&nbsp;':'';
//                    if($parId==0)
//                    {
//                        $nbsp='';
//                    }
                $html .= '<tr id="'.$id.'">
                              <td style="text-align:center;" class="checkbox-column"><label><input style="float:left;" type="checkbox" name="id[]" value="'.$id.'" /><span class="lbl"></span></label></td>
                              <td><a href="'.base_url().'admin/admin_menu/view/'.$parId.'">'.$parName.'</a></td>
                              <td style="text-align:center;">'.$upDownMenu.'</td>
                              <td>&nbsp;&nbsp;' . $nbsp . '<a href="'.base_url().'admin/admin_menu/view/'.$id.'">'.$name .'</a></td>
                              <td>'.$desc.'</td>
                              <td width="10%"  style="text-align:center;"><span class="'.(($menuIsActive==1)?'icon-ok':'clip-minus-circle').'"></span></span></td>
                                <td class="da-icon-column text-center">
                                  <a href="'.base_url().'admin/admin_menu/view/'.$id.'" class="btn btn-xs btn-default" title="Detail"><i class="clip-zoom-in"></i></a>
                                  <a href="'.base_url().'admin/admin_menu/update/'.$id.'?location='.$loc.'" class="btn btn-xs btn-default" title="Edit"><i class="clip-pencil"></i></a>
                                  <a href="'.base_url().'admin/admin_menu/delete/'.$id.'?location='.$loc.'" onclick="return confirm(\'Hapus?\');" class="btn btn-xs btn-default" title="Hapus"><i class="clip-remove"></i></a>
                              </td>
                              </tr>
                              '
                ;

                // find childitems recursively
                $html .= $this->buildMenu($itemId, $menuData, $nbsp);

                //$html .= '</tr>';
            }
        }

        return $html;
    }

    /**
     * recursive dengan drop down
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param string $nbsp pemisah karakter
     * @param int $parentDataId $id primary data untuk selected ketika update/post
     *
     */
    public function buildMenuDropDown($parentId, $menuData, $nbsp = '', $menuId = 0) {
        $html = '';
        $nbsp.=($parentId == 0) ? '=' : '==';
        if (isset($menuData['parents'][$parentId])) {

            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $parentMenuId=$this->CI->function_lib->get_one('menu_administrator_par_id',$this->mainTable,'menu_administrator_id='.  intval($menuId));

//                    $nbsp.=!empty($menuData)?'==':'';
//                    if($parId==0)
//                    {
//                        $nbsp='==';
//                    }
                $html .= '<option value="' . $id . '" ' . (($id == $parentMenuId) ? 'selected' : '') . '>' . $nbsp . ' ' . $name . '</option>';

                // find childitems recursively
                $html .= $this->buildMenuDropDown($itemId, $menuData, $nbsp, $menuId);

                //$html .= '</tr>';
            }
        }

        return $html;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     *
     */
    public function buildTreePrivilege($parentId, $menuData,$no=0,$groupId=0) {
        $id=($no==0)?'class="tree" id="treeList"':'';
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul '.$id.'>';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $module = $menuData['items'][$itemId]['menu_administrator_module'];
                $controller = $menuData['items'][$itemId]['menu_administrator_controller'];
                $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
                $isChecked=$isAllowedAccess?'checked':'';
                $redirect=  rawurlencode(base_url().'admin/admin_group/set_privilege/'.$groupId.'#'.$id);
                $linkUpdateService=base_url().'admin_menu/service_rest/update_menu_public_method/'.$id.'/true';
                $linkUpdateCallback=base_url().'auth/connect_to_admin?continue='.  rawurlencode(base_url().'admin/admin_group/set_privilege/'.$groupId.'#update_method_'.$id);
                $linkUpdate=(strlen($module)>3 AND strlen($controller)>3)?' <a href="'.$linkUpdateService.'" title="Refresh Method" onclick="update_method(\''.$linkUpdateService.'\',\''.$linkUpdateCallback.'\');return false;"><i class="icon-exchange"></i></a> ':'';
                $editLabel='<a id="update_method_'.$id.'" title="Edit" href="'.base_url().'admin/admin_menu/update/'.$id.'?redirect='.$redirect.'"><i class="icon-pencil"></i></a> ';
                $html.='<li>';
                //$html .= '<input type="checkbox" name="menu_administrator_id['.$id.'][id]" value="' . $id . '" id="'.$id.'" '.$isChecked.'> <label for="'.$id.'">'.$name.'</label> '.$editLabel;
                $html .= '<label><input type="checkbox" name="menu_administrator_id['.$id.'][id]" value="' . $id . '" id="'.$id.'" '.$isChecked.'> <span class="lbl">'.$name.'</span> '.$editLabel.$linkUpdate.'</label>';


                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $html.=$this->getMethod($id,$groupId);
                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreePrivilege($itemId, $menuData,$no,$groupId);
                $html.='</li>';
                //$html .= '</tr>';
            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * proses pengecekkan untuk pembuatan label menu
     * <ul>
     * <li>jika menu belum mempunyai label, otomatis akan tampil input tipe text</li>
     * <li>Jika menu telah mempunyai label, akan muncul link untuk mengarahkan ke detail menu.
     * Pada link tsb akan ada fitur hidden atau change label</li>
     * </ul>
     * @date 2 Februari 2014
     * @param string $module
     * @param string $class
     * $param string $method
     */
    public function build_menu_label($module, $class, $method,$menuName='')
    {
        $html='';
        //hanya berlaku untuk menu method
        $is_created=$this->CI->function_lib->get_one('COUNT(privilege_label_id)','site_administrator_privilege_label','privilege_label_module="'.$module.'"
            AND privilege_label_class="'.$class.'" AND privilege_label_method="'.$method.'"');
        if(!$is_created)
        {
            $html='<input type="text" class="'.$module.$class.$method.'" id="'.$module.'#'.$class.'#'.$method.'" name="privilege_label_name" value="" style="float:right;"/ placeholder="Set label for '.$menuName.'..." title="Set label for '.$menuName.'">
                <span id="response_'.$module.$class.$method.'"></span>';
        }

        return $html;
    }

   /**
    * Label Menu
    *  * @param string $module
     * @param string $class
     * $param string $method
    */
    public function get_menu_label($module, $class, $method,$default_name='')
    {
        $privilege_label_name=$this->CI->function_lib->get_one('privilege_label_name','site_administrator_privilege_label','privilege_label_module="'.$module.'"
             AND privilege_label_class="'.$class.'" AND privilege_label_method="'.$method.'"');
        $response=$default_name;
        if(trim($privilege_label_name)!='')
        {
            $response=$privilege_label_name;
        }
        return $response;
    }

   /**
    * cek hidden Menu Method
    *  * @param string $module
     * @param string $class
     * $param string $method
    */
    public function is_hidden_menu_label($module, $class, $method)
    {
        $privilege_label_is_hidden=$this->CI->function_lib->get_one('privilege_label_is_hidden','site_administrator_privilege_label','privilege_label_module="'.$module.'"
             AND privilege_label_class="'.$class.'" AND privilege_label_method="'.$method.'"');
        $response=false;
        if($privilege_label_is_hidden==1)
        {
            $response=true;
        }
        return $response;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     *
     */
    public function buildTreeMenu($parentId, $menuData,$no=0,$groupId=0,$currentId=0) {
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            //$html = '<ul>';
            $html = '';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
                $isChecked=$isAllowedAccess?'checked':''; //echo '<pre>'; print_r($name); exit;
                $isCurrent=($currentId==$id)?'current':(trim($this->CI->uri->segment(1))=="" AND trim($name)=="Home")?'current':'';
                $html.='<li class="'.$isCurrent.'">';

                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $link= base_url().$menuData['items'][$itemId]['menu_administrator_link'];
                    $html.='<a href="'.$link.'"><div>'.$name.'</div></a>';
                }
                else
                {
                    $html.='<a href="#"><div>'.$name.'</div></a>';
                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenu($itemId, $menuData,$no,$groupId);
                $html.='</li>';
                //$html .= '</tr>';
            }
            //$html.='</ul>';

        }

        return $html;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     *
     */
    public function buildTreeMenu_greenart($parentId, $menuData,$no=0,$groupId=0) {
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul>';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
                $isChecked=$isAllowedAccess?'checked':'';
                $html.='<li class="sub-menu">';
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $news_external_link = $menuData['items'][$itemId]['menu_administrator_link'];
                    $isValidUrl=$this->CI->function_lib->isValidUrl($news_external_link);
                    $link= $isValidUrl?base_url().'?continue_front='.  rawurlencode($news_external_link): base_url().$news_external_link;

                    $html.='<a class="sf-with-ul" href="'.$link.'">
                                    '.$name.'
                                </a>';
                }
                else
                {
                    $html.='<a href="#"><div>'.$name.'</div></a>


                                </a>';
                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenu_greenart($itemId, $menuData,$no,$groupId);
                $html.='</li>';
                //$html .= '</tr>';
            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * template Plain
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     * @param int $currentParent parent menu yang sedang aktif berdasarkan childnya
     * @param int $currentId id menu yang sedang aktif
     */
    public function buildTreeMenu_plain($parentId, $menuData,$no=0,$groupId=0,$currentParent=0,$currentId=0) {
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            $classUl=($no==0)?'nav':'';
            $html = '<ul class="'.$classUl.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
               // $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
              //  $isChecked=$isAllowedAccess?'checked':'';
                $classPar=($currentParent==$id)?'active':'';
                $classCurrId=($currentId==$id )?'active':'';
                $hasSubmenu=$this->hasSubmenu($id);
//echo $currentParent;
                $dropDownClass=!$hasSubmenu?'':'dropdown';
//$currentParent==$id OR
                $html.='<li class="'.$dropDownClass.' '.$classPar.' '.$classCurrId.'">';
                $isValidUrl=$this->CI->function_lib->isValidUrl($menuData['items'][$itemId]['menu_administrator_link']);
                $link=$isValidUrl?base_url().'?continue_front='.  rawurlencode($menuData['items'][$itemId]['menu_administrator_link']): base_url().$menuData['items'][$itemId]['menu_administrator_link'];

                //cek submenu
                if(!$hasSubmenu)
                {
                    //$link= base_url().$menuData['items'][$itemId]['menu_administrator_link'];

                    $html.='<a href="'.$link.'">
							<i></i>
							<span class="menu-text"> '.$name.' </span>
						</a>

                    ';
                }
                else
                {
                    $html.='<a href="'.$link.'">'.$name.' <i class="icon-caret-down"></i>
						</a>  ';
                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenu_plain($itemId, $menuData,$no,$groupId,$currentParent,$currentId);
                $html.='</li>';

            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * template ace
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     * @param int $currentParent parent menu yang sedang aktif berdasarkan childnya
     * @param int $currentId id menu yang sedang aktif
     */
    public function buildTreeMenuAce($parentId, $menuData,$no=0,$groupId=0,$currentParent=0,$currentId=0) {
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            $classUl=($no==0)?'nav nav-list':'submenu';
            $html = '<ul class="'.$classUl.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
               // $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
              //  $isChecked=$isAllowedAccess?'checked':'';
                $classPar=($currentParent==$id)?'active open':'';
                $classCurrId=($currentId==$id )?'active':'';
//$currentParent==$id OR
                $html.='<li class="'.$classPar.' '.$classCurrId.'">';
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $link= base_url().$menuData['items'][$itemId]['menu_administrator_link'];
                    $html.='<a href="'.$link.'">
							<i></i>
							<span class="menu-text"> '.$name.' </span>
						</a>

                    ';
                }
                else
                {
                    $html.='<a href="#" class="dropdown-toggle">
							<i></i>
							<span class="menu-text">'.$name.'</span>

							<b class="arrow icon-angle-down"></b>
						</a>
                            ';
                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenuAce($itemId, $menuData,$no,$groupId,$currentParent,$currentId);
                $html.='</li>';

            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * template ace
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     * @param int $currentParent parent menu yang sedang aktif berdasarkan childnya
     * @param int $currentId id menu yang sedang aktif
     * <ul class="main-navigation-menu">
						<li class="active open">
							<a href="index.html"><i class="clip-home-3"></i>
								<span class="title"> Dashboard </span><span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-screen"></i>
								<span class="title"> Layouts </span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="layouts_horizontal_menu1.html">
										<span class="title"> Horizontal Menu </span>
										<span class="badge badge-new">new</span>
									</a>
								</li>
								<li>
									<a href="layouts_sidebar_closed.html">
										<span class="title"> Sidebar Closed </span>
									</a>
								</li>
								<li>
									<a href="layouts_boxed_layout.html">
										<span class="title"> Boxed Layout </span>
									</a>
								</li>
								<li>
									<a href="layouts_footer_fixed.html">
										<span class="title"> Footer Fixed </span>
									</a>
								</li>
								<li>
									<a href="../clip-one-rtl/index.html">
										<span class="title"> RTL Version </span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="../../frontend/clip-one/index.html" target="_blank"><i class="clip-cursor"></i>
								<span class="title"> Frontend Theme </span><span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-cog-2"></i>
								<span class="title"> UI Lab </span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="ui_elements.html">
										<span class="title"> Elements </span>
									</a>
								</li>
								<li>
									<a href="ui_buttons.html">
										<span class="title"> Buttons &amp; icons </span>
									</a>
								</li>
								<li>
									<a href="ui_animations.html">
										<span class="title"> CSS3 Animation </span>
									</a>
								</li>
								<li>
									<a href="ui_modals.html">
										<span class="title"> Extended Modals </span>
									</a>
								</li>
								<li>
									<a href="ui_tabs_accordions.html">
										<span class="title"> Tabs &amp; Accordions </span>
									</a>
								</li>
								<li>
									<a href="ui_sliders.html">
										<span class="title"> Sliders </span>
									</a>
								</li>
								<li>
									<a href="ui_treeview.html">
										<span class="title"> Treeview </span>
									</a>
								</li>
								<li>
									<a href="ui_nestable.html">
										<span class="title"> Nestable List </span>
									</a>
								</li>
								<li>
									<a href="ui_typography.html">
										<span class="title"> Typography </span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-grid-6"></i>
								<span class="title"> Tables </span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="table_static.html">
										<span class="title">Static Tables</span>
									</a>
								</li>
								<li>
									<a href="table_responsive.html">
										<span class="title">Responsive Tables</span>
									</a>
								</li>
								<li>
									<a href="table_data.html">
										<span class="title">Data Tables</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-pencil"></i>
								<span class="title"> Forms </span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="form_elements.html">
										<span class="title">Form Elements</span>
									</a>
								</li>
								<li>
									<a href="form_wizard.html">
										<span class="title">Form Wizard</span>
									</a>
								</li>
								<li>
									<a href="form_validation.html">
										<span class="title">Form Validation</span>
									</a>
								</li>
								<li>
									<a href="form_inline.html">
										<span class="title">Inline Editor</span>
									</a>
								</li>
								<li>
									<a href="form_x_editable.html">
										<span class="title">Form X-editable</span>
									</a>
								</li>
								<li>
									<a href="form_image_cropping.html">
										<span class="title">Image Cropping</span>
									</a>
								</li>
								<li>
									<a href="form_multiple_upload.html">
										<span class="title">Multiple File Upload</span>
									</a>
								</li>
								<li>
									<a href="form_dropzone.html">
										<span class="title">Dropzone File Upload</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-user-2"></i>
								<span class="title">Login</span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="login_example1.html">
										<span class="title">Login Form Example 1</span>
									</a>
								</li>
								<li>
									<a href="login_example2.html">
										<span class="title">Login Form Example 2</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-file"></i>
								<span class="title">Pages</span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="pages_user_profile.html">
										<span class="title">User Profile</span>
									</a>
								</li>
								<li>
									<a href="pages_invoice.html">
										<span class="title">Invoice</span>
										<span class="badge badge-new">new</span>
									</a>
								</li>
								<li>
									<a href="pages_gallery.html">
										<span class="title">Gallery</span>
									</a>
								</li>
								<li>
									<a href="pages_timeline.html">
										<span class="title">Timeline</span>
									</a>
								</li>
								<li>
									<a href="pages_calendar.html">
										<span class="title">Calendar</span>
									</a>
								</li>
								<li>
									<a href="pages_messages.html">
										<span class="title">Messages</span>
									</a>
								</li>
								<li>
									<a href="pages_blank_page.html">
										<span class="title">Blank Page</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-attachment-2"></i>
								<span class="title">Utility</span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="utility_faq.html">
										<span class="title">Faq</span>
									</a>
								</li>
								<li>
									<a href="utility_search_result.html">
										<span class="title">Search Results </span>
										<span class="badge badge-new">new</span>
									</a>
								</li>
								<li>
									<a href="utility_lock_screen.html">
										<span class="title">Lock Screen</span>
									</a>
								</li>
								<li>
									<a href="utility_404_example1.html">
										<span class="title">Error 404 Example 1</span>
									</a>
								</li>
								<li>
									<a href="utility_404_example2.html">
										<span class="title">Error 404 Example 2</span>
									</a>
								</li>
								<li>
									<a href="utility_404_example3.html">
										<span class="title">Error 404 Example 3</span>
									</a>
								</li>
								<li>
									<a href="utility_500_example1.html">
										<span class="title">Error 500 Example 1</span>
									</a>
								</li>
								<li>
									<a href="utility_500_example2.html">
										<span class="title">Error 500 Example 2</span>
									</a>
								</li>
								<li>
									<a href="utility_pricing_table.html">
										<span class="title">Pricing Table</span>
									</a>
								</li>
								<li>
									<a href="utility_coming_soon.html">
										<span class="title">Cooming Soon</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:;" class="active">
								<i class="clip-folder"></i>
								<span class="title"> 3 Level Menu </span>
								<i class="icon-arrow"></i>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="javascript:;">
										Item 1 <i class="icon-arrow"></i>
									</a>
									<ul class="sub-menu">
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 2
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 3
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="javascript:;">
										Item 1 <i class="icon-arrow"></i>
									</a>
									<ul class="sub-menu">
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">
										Item 3
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:;">
								<i class="clip-folder-open"></i>
								<span class="title"> 4 Level Menu </span><i class="icon-arrow"></i>
								<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="javascript:;">
										Item 1 <i class="icon-arrow"></i>
									</a>
									<ul class="sub-menu">
										<li>
											<a href="javascript:;">
												Sample Link 1 <i class="icon-arrow"></i>
											</a>
											<ul class="sub-menu">
												<li>
													<a href="#"><i class="fa fa-times"></i>
														Sample Link 1</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-pencil"></i>
														Sample Link 1</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-edit"></i>
														Sample Link 1</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 2
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 3
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="javascript:;">
										Item 2 <i class="icon-arrow"></i>
									</a>
									<ul class="sub-menu">
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
										<li>
											<a href="#">
												Sample Link 1
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">
										Item 3
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="maps.html"><i class="clip-location"></i>
								<span class="title">Maps</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="charts.html"><i class="clip-bars"></i>
								<span class="title">Charts</span>
								<span class="selected"></span>
							</a>
						</li>
					</ul>
     */
    public function buildTreeMenuApp($parentId, $menuData,$no=0,$groupId=0,$currentParent=0,$currentId=0,$cur_id_par=array()) {
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            $classUl=($no==0)?'main-navigation-menu':'sub-menu';
            $html = '<ul class="'.$classUl.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $class_css = $menuData['items'][$itemId]['menu_administrator_class_css'];
                $class_css_active=(isset($class_css) AND $class_css!='')?$class_css:'';
               // $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
              //  $isChecked=$isAllowedAccess?'checked':'';
                $classPar=($currentParent==$id)?'active open':'';
                $classCurrId=($currentId==$id )?'active open':'';
                $menu_has_related=array_search($id, $cur_id_par);
                $class_active_open=($menu_has_related>0)?'active open':'';
               // $html.='<li class="'.$classPar.' '.$classCurrId.'">'.$menu_has_related;
                $html.='<li class="'.$class_active_open.'">';
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $link= base_url().$menuData['items'][$itemId]['menu_administrator_link'];
                    $html.=' <a href="'.$link.'">
                    <i class="'.$class_css_active.'"></i>
                                    <span class="title">'.$name.'</span>
                                    <span class="selected"></span>

                                    </a>

                    ';
                }
                else
                {
                    $html.='
                                    <a href="javascript:;">
                                    <i class="'.$class_css_active.'"></i>
                                    <span class="title">'.$name.'</span><i class="icon-arrow"></i>
                                    <span class="badge badge-new">'.(($hasSubmenu>0)?$hasSubmenu:'').'</span>
                                    <span class="selected"></span>
                                    </a>

                            ';
                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenuApp($itemId, $menuData,$no,$groupId,$currentParent,$currentId,$cur_id_par);
                $html.='</li>';

            }
            $html.='</ul>';

        }

        return $html;
    }
    /**
     * template ace
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param int $groupId untuk pengecekkan menu ke group
     * @param int $currentParent parent menu yang sedang aktif berdasarkan childnya
     * @param int $currentId id menu yang sedang aktif
     */
    public function buildTreeMenu_ace($parentId, $menuData,$no=0,$groupId=0,$currentParent=0,$currentId=0) {
        $html='';
        if (isset($menuData['parents'][$parentId])) {
            $classUl=($no==0)?'nav nav-list':'submenu';
            $html = '<ul class="'.$classUl.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
               // $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $id);
              //  $isChecked=$isAllowedAccess?'checked':'';
                $classPar=($currentParent==$id)?'active open':'';
                $classCurrId=($currentId==$id )?'active':'';
//$currentParent==$id OR
                $html.='<li class="'.$classPar.' '.$classCurrId.'">';
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                if(!$hasSubmenu)
                {
                    $link= base_url().$menuData['items'][$itemId]['menu_administrator_link'];
                    $html.='<a href="'.$link.'">
							<i></i>
							<span class="menu-text"> '.$name.' </span>
						</a>

                    ';
                }
                else
                {
                    $html.='<a href="#" class="dropdown-toggle">
							<i></i>
							<span class="menu-text">'.$name.'</span>

							<b class="arrow icon-angle-down"></b>
						</a>
                            ';
                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenuAce($itemId, $menuData,$no,$groupId,$currentParent,$currentId);
                $html.='</li>';

            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param string $additionalClass
     * @param int $selectParId
     *
     */
    public function buildTreeMenuMember($parentId, $menuData,$no=0,$additionalClass='',$selectParId='') {
        $html='';
        $initClass=($no==0)?'nav ':$additionalClass;
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul class="'.$initClass.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                $selected=($selectParId==$id)?' active ':'';
                if(!$hasSubmenu)
                {
                    $html.='<li class="'.$selected.'">';
                    $isValidUrl=$this->CI->function_lib->isValidUrl($menuData['items'][$itemId]['menu_administrator_link']);
                    $link=$isValidUrl?base_url().'?continue_front='.  rawurlencode($menuData['items'][$itemId]['menu_administrator_link']): base_url().$menuData['items'][$itemId]['menu_administrator_link'];


                    $html.='<a href="'.$link.'">
                                	'.$name.'
                                </a>';
                }
                else
                {
                    $additionalClass='dropdown-menu';
                    $html.='<li class="dropdown '.$selected.'">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">'.$name.' <b class="caret"></b></a>';

                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenuMember($itemId, $menuData,$no,$additionalClass,$selectParId);
                $html.='</li>';
                //$html .= '</tr>';
            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param string $additionalClass
     * @param int $selectParId
     *  <ul class="nav-icon-list">
              <li><a class="planner-btn"><span>LAKUKAN AKSIMU</span></a></li>
              <li><a class="planner-btn"><span>APA ITU FCTC?</span></a></li>
              <li><a class="planner-btn"><span>GABUNG</span></a></li>
            </ul>
     */
    public function buildTreeMenuUserHeader($parentId, $menuData,$no=0,$additionalClass='',$selectParId='') {
        $uri_1=$this->CI->uri->segment(1);
        $uri_2=$this->CI->uri->segment(2);

        $html='';
        $initClass=($no==0)?'nav-icon-list ':$additionalClass;
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul class="'.$initClass.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                //cek submenu
                $hasSubmenu=$this->hasSubmenu_user($id);
                $selected=($selectParId==$id)?' active ':'';
                if(!$hasSubmenu)
                {
                    $html.='<li class="planner-btn '.$selected.'">';
                    $isValidUrl=$this->CI->function_lib->isValidUrl($menuData['items'][$itemId]['menu_administrator_link']);
                    $link=$isValidUrl?base_url().'?continue_front='.  rawurlencode($menuData['items'][$itemId]['menu_administrator_link']): base_url().$menuData['items'][$itemId]['menu_administrator_link'];


                    $html.='<a href="'.$link.'">
                                    '.$name.'
                                </a>';
                }
                else
                {
                    $additionalClass='dropdown-menu';
                    $html.='<li class="dropdown '.$selected.'">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">'.$name.' <b class="caret"></b></a>';

                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenuUserHeader($itemId, $menuData,$no,$additionalClass,$selectParId);
                $html.='</li>';
                //$html .= '</tr>';
            }
            $last_data=$no;
            //echo $last_data.'/'.count($menuData['parents'][0]);
            //exit;

            if($last_data==count($menuData['parents'][0]))
            {
                $html.='<li><a class="planner-btn" title="Gabung">Gabung</a></li>';
                if($uri_1=='master_content' AND $uri_2!='detail' )
                {
                    $html.='<li><a title="Lihat Pilihan Lainnya" href="" class="switch open-sidebar" gumby-trigger="#hidden-sidebar"><span style="display:none;"></span><i class="fa fa-exchange"></i></a></li>';
                }
            }
   //         if($uri_1=='' OR $uri_1=='index.php'  AND $last_data==count($menuData['parents'][0]))
//            {
  //          }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param string $additionalClass
     * @param int $selectParId
     *  <ul class="nav-icon-list">
              <li><a class="planner-btn"><span>LAKUKAN AKSIMU</span></a></li>
              <li><a class="planner-btn"><span>APA ITU FCTC?</span></a></li>
              <li><a class="planner-btn"><span>GABUNG</span></a></li>
            </ul>
     */
    public function buildTreeMenuUserFooter($parentId, $menuData,$no=0,$additionalClass='',$selectParId='') {
        $html='';

        if (isset($menuData['parents'][$parentId])) {
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                //cek submenu
                $hasSubmenu=$this->hasSubmenu_user($id);
                $selected=($selectParId==$id)?' active ':'';
                $sparator = ($no!=0)?'/':'';
                if(!$hasSubmenu)
                {
                    $isValidUrl=$this->CI->function_lib->isValidUrl($menuData['items'][$itemId]['menu_administrator_link']);
                    $link=$isValidUrl?base_url().'?continue_front='.  rawurlencode($menuData['items'][$itemId]['menu_administrator_link']): base_url().$menuData['items'][$itemId]['menu_administrator_link'];

                    $html.=$sparator.'<a href="'.$link.'">'.$name.'</a>';
                }

                $no++;
            }

        }

        return $html;
    }

    public function buildTreeMenuUserLeft($parentId, $menuData,$no=0,$additionalClass='',$selectParId='',$additionalId='') {
        $html='';
        $initClass=($no==0)?'site-nav ':$additionalClass;
        $initId=($no==0)?' ':$additionalId;
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul class="'.$initClass.'" id="'.$initId.'">';
            if($no==0)
            {
                $html.='<li><a href="'.base_url().'">'.$this->websiteConfig['config_name'].'</a></li>';
            }

            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $description = $menuData['items'][$itemId]['menu_administrator_description'];
                //cek submenu
                $hasSubmenu=$this->hasSubmenu_user($id);
                $selected=($selectParId==$id)?' active ':'';
                if(!$hasSubmenu)
                {
                    $html.='<li class="'.$selected.'">';
                    $isValidUrl=$this->CI->function_lib->isValidUrl($menuData['items'][$itemId]['menu_administrator_link']);
                    $link=$isValidUrl?base_url().'?continue_front='.  rawurlencode($menuData['items'][$itemId]['menu_administrator_link']): base_url().$menuData['items'][$itemId]['menu_administrator_link'];


                    $html.='<a href="'.$link.'">
                                    '.$name.'<span>'.$description.'</span>
                                </a>';
                }
                else
                {
                    $additionalClass='drawer';
                    $additionalId='left'.$id;
                    $html.='<li class=" '.$selected.'">
                        <a  href="" class="toggle" gumby-trigger="#'.$additionalId.'">'.$name.'<span>'.$description.'</span></a>';

                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenuUserLeft($itemId, $menuData,$no,$additionalClass,$selectParId,$additionalId);
                $html.='</li>';
                //$html .= '</tr>';
            }

            $last_data=$no;
            //echo $last_data.'/'.count($menuData['parents'][0]);
            //exit;
            if($last_data==count($menuData['parents'][0]))
            {
                $list_category_content=$this->list_category_content();
                //$html.='<li><a class="planner-btn" title="Gabung">Gabung</a></li>';

                if(!empty($list_category_content))
                {
                    $html.='<li><a href="" class="toggle" gumby-trigger="#home-publikasi">PUBLIKASI</a>
                    <ul id="home-publikasi" class="drawer">';
                    foreach($list_category_content AS $rowArr)
                    {
                        foreach($rowArr AS $variable=>$value)
                        {
                            ${$variable}=$value;
                        }
                        $link_archives=master_content_lib::link_archives($category_permalink);

                        $html.='<li><a href="'.$link_archives.'">'.$category_title.'</a></li>';
                    }
                    $html.='</ul></li>';
                }



            }
            $html.='</ul>';

        }

        return $html;
    }

    public function list_category_content($type='article')
    {
        $sql='SELECT category_title, category_permalink FROM  site_news_category
              WHERE
              category_type="'.$type.'"
              ORDER BY category_title ASC
              ';
        $exec=$this->CI->db->query($sql);
        $results=$exec->result_array();
        return $results;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param string $additionalClass
     * @param int $selectParId
     *
     */
    public function buildTreeMenu_andia($parentId, $menuData,$no=0,$additionalClass='',$selectParId='') {
        $html='';
        $initClass=($no==0)?'nav pull-right ':$additionalClass;
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul class="'.$initClass.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                $selected=($selectParId==$id)?' active ':'';
                if(!$hasSubmenu)
                {
                    $html.='<li class="'.$selected.'">';
                    $isValidUrl=$this->CI->function_lib->isValidUrl($menuData['items'][$itemId]['menu_administrator_link']);
                    $link=$isValidUrl?base_url().'?continue_front='.  rawurlencode($menuData['items'][$itemId]['menu_administrator_link']): base_url().$menuData['items'][$itemId]['menu_administrator_link'];


                    $html.='<a href="'.$link.'"><i class="icon-tasks "></i><br />
                                	'.$name.'
                                </a>';
                }
                else
                {
                    $additionalClass='dropdown-menu';
                    $html.='<li class="dropdown '.$selected.'">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">'.$name.' <b class="caret"></b></a>';

                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenu_andia($itemId, $menuData,$no,$additionalClass,$selectParId);
                $html.='</li>';
                //$html .= '</tr>';
            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * recursive tree privilege
     * @param int $parentId
     * @param array $menuData data array yang digenerate dari method initalizeRecursive
     * @param int $no nomor urut
     * @param string $additionalClass
     * @param int $selectId id menu yang sedang diakses
     *
     */
    public function buildTreeMenuMemberDetail($parentId, $menuData,$no=0,$additionalClass='',$selectId='') {
        $html='';
        $initClass=($no==0)?'nav nav-tabs nav-stacked ':$additionalClass;
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul class="'.$initClass.'">';
            foreach ($menuData['parents'][$parentId] as $itemId) {
                $id = $menuData['items'][$itemId]['menu_administrator_id'];
                $parId = $menuData['items'][$itemId]['menu_administrator_par_id'];
                $name = $menuData['items'][$itemId]['menu_administrator_title'];
                $link= base_url().$menuData['items'][$itemId]['menu_administrator_link'];

                //cek submenu
                $hasSubmenu=$this->hasSubmenu($id);
                $selected=($selectId==$id)?' active ':'';
                $linkSelected=($selectId==$id)?"#":$link;
                if(!$hasSubmenu)
                {
                    $html.='<li class="'.$selected.'"> ';
                    $html.='<a href="'.$linkSelected.'"><i class="icon-chevron-right"></i>
                                	'.$name.'
                                </a>';
                }
                else
                {
                    $html.='<li class="'.$selected.'">';
                    $additionalClass='';
                    $html.='<a href="#"><i class="icon-chevron-right"></i>
                                	'.$name.'
                                </a>';

                }

                $no++;
                // find childitems recursively
                $html .= $this->buildTreeMenuMemberDetail($itemId, $menuData,$no,$additionalClass,$selectId);
                $html.='</li>';
                //$html .= '</tr>';
            }
            $html.='</ul>';

        }

        return $html;
    }

    /**
     * cek apakah group tersebut sudah pernah diset untuk akses ke menu tersebut
     * @param int $groupId
     * @param int $menuId
     * @param string $method default string empty
     * @return int 1 => OK 0=> not
     */
    public function isAllowedThisGroupToMenu($groupId,$menuId,$method='')
    {
       if(trim($method)!='')
       {
            $scalar=$this->CI->function_lib->get_one('COUNT(*)','site_administrator_privilege','administrator_privilege_administrator_group_id='.  intval($groupId).' AND administrator_privilege_administrator_menu_id='.  intval($menuId).' AND administrator_privilege_method_allowed="'.$method.'"');
       }
       else
       {
          $scalar=$this->CI->function_lib->get_one('COUNT(*)','site_administrator_privilege','administrator_privilege_administrator_group_id='.  intval($groupId).' AND administrator_privilege_administrator_menu_id='.  intval($menuId));
       }
       return intval($scalar);
    }

    /**
     * check has submenu
     * @param int $menuId
     * @return bool true has submenu
     */
    public function hasSubmenu($menuId)
    {
        $session=  auth_admin_lib::staticGetAdminSession();
        $groupId=isset($session['detail']['admin_group_id'])?$session['detail']['admin_group_id']:'';
        $groupType=isset($session['detail']['admin_group_type'])?$session['detail']['admin_group_type']:'';

        $value=false;
        $where='';
        if($groupType!='superuser')
        {
            $where=' AND menu_administrator_id IN (SELECT administrator_privilege_administrator_menu_id FROM site_administrator_privilege
            WHERE administrator_privilege_administrator_group_id='.  intval($groupId).')   ';
        }
        $scalar=$this->CI->function_lib->get_one('COUNT(*)','site_administrator_menu','menu_administrator_par_id='.  intval($menuId).' AND menu_administrator_is_active="1"
            '.$where);
        if(intval($scalar)>0)
        {
            $value=$scalar;
        }
        return $value;
    }


    /**
     * check has submenu
     * @param int $menuId
     * @return bool true has submenu
     */
    public function hasSubmenu_user($menuId)
    {

        $value=false;
        $where='';

        $scalar=$this->CI->function_lib->get_one('COUNT(*)','site_administrator_menu','menu_administrator_par_id='.  intval($menuId).' AND menu_administrator_is_active="1"
            '.$where);
        if(intval($scalar)>0)
        {
            $value=$scalar;
        }
        return $value;
    }

    /**
     * dapatkan method
     * @param int $menuId
     * @param int $groupId
     */
    public function getMethod($menuId,$groupId)
    {
        $html='';
        $method=$this->CI->function_lib->get_one('menu_administrator_method',$this->mainTable,'menu_administrator_id='.  intval($menuId));
        $menuName=$this->CI->function_lib->get_one('menu_administrator_title',$this->mainTable,'menu_administrator_id='.  intval($menuId));
        if($method)
        {
            $row_menu=$this->find('menu_administrator_id='.intval($menuId));
            $module=$row_menu['menu_administrator_module'];
            $class=$row_menu['menu_administrator_controller'];
            //extract jadi array dari #
            $explode=  explode('#', $method);
            if(!empty($explode))
            {
                $html.='<ul style="margin-left:20px;">';
                foreach($explode AS $val)
                {
                    $isAllowedAccess=$this->isAllowedThisGroupToMenu($groupId, $menuId,$val);
                    $isChecked=$isAllowedAccess?'checked':'';
                    if(trim($val)!='' AND $val!='-')
                    {
                        $menu_label=$menuName.'.'.$val;
                        $buil_menu_label=$this->build_menu_label($module, $class, $val,$menu_label);
                        $get_menu_label=$this->get_menu_label($module, $class, $val,$menu_label);
                        //$html.='<li><input type="checkbox" name="menu_administrator_id['.$menuId.'][method][]" value="'.$val.'" id="'.$menuId.'_'.$val.'" '.$isChecked.'> <label for="'.$menuId.'_'.$val.'"><i>'.$menuName.'.'.$val.'</i></label></li>';
                        $is_hidden_menu_label=$this->is_hidden_menu_label($module, $class, $val);
                        if(!$is_hidden_menu_label)
                        {                        $html.='<li><label><input type="checkbox" name="menu_administrator_id['.$menuId.'][method][]" value="'.$val.'"  class="child_'.$menuId.'" '.$isChecked.'> <span class="lbl"><i>'.$get_menu_label.'</i></span></label>'.$buil_menu_label.'</li>';
}


                    }
                }
                $html.='</ul>';
            }
        }
        return $html;
    }


    /**
     * fungsi static dapatkan nama parent
     * @param int $parId
     */
    public static function getParentName($parId)
    {
        $am=new admin_menu;
        $scalar=$am->CI->function_lib->get_one('menu_administrator_title','site_administrator_menu','menu_administrator_id='.  intval($parId));
        return ($parId==0)?'Menu Utama':$scalar;

    }

    /**
     * mendapatkan nomor urut
     * @param int $parId default 0
     * @param int $currentParId saat update
     * @param int $id saat update
     * @param string $location
     */
     public static function getMyNumber($parId=0,$currentParId=0,$id=0,$location='admin')
     {
         $am=new admin_menu;
         $defaultNumber=1;
         $lastNumber=$am->CI->function_lib->get_one('menu_administrator_order_by','site_administrator_menu','menu_administrator_location="'.$location.'" AND menu_administrator_par_id='.  intval($parId).' ORDER BY menu_administrator_order_by DESC');
         $myNumber=$defaultNumber+intval($lastNumber);

         //jika parid dan current par id sama, set ke penomoran semula
         if($parId==$currentParId AND $id!=0)
         {
             $myNumber=$am->CI->function_lib->get_one('menu_administrator_order_by','site_administrator_menu','menu_administrator_location="'.$location.'" AND menu_administrator_id='.  intval($id));
         }

         return $myNumber;

     }

     /**
      * untuk mengatur posisi lewat grid
      * @param int $parId default 0
      * @param int $myNum nomor menu itu sendiri
      * @param int $myId untuk link position
      * @param string $redirect
      * @param string $location
      *
      */
     public static function upDownMenu($parId,$myNum,$myId,$redirect='',$location='')
     {
          $html='';
          $am=new admin_menu;
          $scalarUp=$am->CI->function_lib->get_one('menu_administrator_order_by','site_administrator_menu','menu_administrator_par_id='.  intval($parId).' AND menu_administrator_location="'.$location.'" AND menu_administrator_order_by<'.  intval($myNum));
          $scalarDown=$am->CI->function_lib->get_one('menu_administrator_order_by','site_administrator_menu','menu_administrator_par_id='.  intval($parId).' AND menu_administrator_location="'.$location.'" AND menu_administrator_order_by>'.  intval($myNum));

          if(trim($scalarUp)!='')
          {
             //$html.='<a href="'.base_url().'admin_menu/service_rest/setPosition/'.$myId.'/'.$parId.'/up/?redirect='.$redirect.'"><img src="'.base_url().'themes/admin/dendelion/inc/assets/images/icons/led/src/order-up-active.png" /></a>';
             $html.='<a title="Up" href="'.base_url().'admin_menu/service_rest/setPosition/'.$myId.'/'.$parId.'/up/?redirect='.$redirect.'"><i class="clip-arrow-up-3"></i></a> ';
          }

          if(trim($scalarDown)!='')
          {
              //$html.='<a href="'.base_url().'admin_menu/service_rest/setPosition/'.$myId.'/'.$parId.'/down/?redirect='.$redirect.'"><img src="'.base_url().'themes/admin/dendelion/inc/assets/images/icons/led/src/order-down-active.png" /></a>';
              $html.='<a title="Down" href="'.base_url().'admin_menu/service_rest/setPosition/'.$myId.'/'.$parId.'/down/?redirect='.$redirect.'"><i class="clip-arrow-down-3"></i></a>';
          }

          return $html;
     }

    /**
     * set ke atas atau ke bawah
     * @param int $menuId
     * @param int $menuParId
     * @param string $pos
     */
    public static function setPosition($menuId,$menuParId,$pos)
    {
        $am=new admin_menu;
        $currentPosition=$am->CI->function_lib->get_one('menu_administrator_order_by','site_administrator_menu','menu_administrator_id='.  intval($menuId));
        $locationMine=$am->CI->function_lib->get_one('menu_administrator_location','site_administrator_menu','menu_administrator_id='.  intval($menuId));
        switch(strtolower($pos))
        {
            //ketika up, set nilai di atasnya +1 dan nilai dirinya -1
            case 'up':
                $upId=$am->CI->function_lib->get_one('menu_administrator_id','site_administrator_menu','menu_administrator_par_id='.  intval($menuParId).' AND menu_administrator_order_by<'.intval($currentPosition).' AND menu_administrator_location="'.$locationMine.'" ORDER BY menu_administrator_order_by DESC');
            //    $upNum=$am->CI->function_lib->get_one('menu_administrator_order_by','site_administrator_menu','menu_administrator_par_id='.  intval($menuParId).' ORDER BY menu_administrator_order_by ASC');
                if($upId)
                {
                    $sqlUp='UPDATE  site_administrator_menu SET menu_administrator_order_by=menu_administrator_order_by+1
                            WHERE menu_administrator_id='.  intval($upId);
                    $am->CI->db->query($sqlUp);
                    $sqlMine='UPDATE  site_administrator_menu SET menu_administrator_order_by=menu_administrator_order_by-1
                            WHERE menu_administrator_id='.  intval($menuId);
                    $am->CI->db->query($sqlMine);
                }
            break;
            //ketika down, set nilai di bawahnya -1 dan nilai dirinya +1
            case 'down':
                $downId=$am->CI->function_lib->get_one('menu_administrator_id','site_administrator_menu','menu_administrator_par_id='.  intval($menuParId).' AND menu_administrator_order_by>'.intval($currentPosition).' AND menu_administrator_location="'.$locationMine.'" ORDER BY menu_administrator_order_by ASC');
                if($downId)
                {
                    $sqlDown='UPDATE  site_administrator_menu SET menu_administrator_order_by=menu_administrator_order_by-1
                            WHERE menu_administrator_id='.  intval($downId);
                    $am->CI->db->query($sqlDown);
                    $sqlMine='UPDATE  site_administrator_menu SET menu_administrator_order_by=menu_administrator_order_by+1
                            WHERE menu_administrator_id='.  intval($menuId);
                    $am->CI->db->query($sqlMine);
                }
            break;
        }
    }


    /**
     *
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }


}
