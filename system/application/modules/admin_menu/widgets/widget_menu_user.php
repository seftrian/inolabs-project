<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_menu_user extends widget_lib{
    //put your code here
   public function run()
    {
        $ci=&get_instance();
        $ci->load->model('admin_menu/admin_menu');
        
        $adminMenu=new admin_menu;
        $data=array();

        $where='menu_administrator_location="user" AND menu_administrator_is_active="1"';
     
        $pathInfo=isset($_SERVER['REQUEST_URI'])?substr($_SERVER['REQUEST_URI'],10):'';
        $findParIdMenu=$this->function_lib->get_one('menu_administrator_par_id','site_administrator_menu','menu_administrator_link LIKE \'%'.$pathInfo.'%\'');
     
        $initData=$adminMenu->initalizeRecursiveDynamic($where);
        
        $themeArr=$ci->load->config->item('theme');
        $themeName=strtolower($themeArr['frontend']['name']);
        
        $functionMenu='buildTreeMenu_'.$themeName;
        
        //$pathInfo=isset($_SERVER['REQUEST_URI'])?substr($_SERVER['REQUEST_URI'],10):'';
        //$pathInfoExp=explode('/',$pathInfo);
       // $pathInfo=(isset($pathInfoExp) AND !empty($pathInfoExp))?$pathInfoExp[0]:'';
        // echo $pathInfo;

        //$pathScriptName = preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
        //$pathInfo = str_replace($pathScriptName, '', $_SERVER['REQUEST_URI']);
        $pathInfo=isset($_SERVER['REQUEST_URI'])?substr($_SERVER['REQUEST_URI'],1):'';

        $findParIdMenu=$this->function_lib->get_one('menu_administrator_par_id','site_administrator_menu','menu_administrator_link LIKE \'%'.$pathInfo.'%\'');
        $findIdMenu=$this->function_lib->get_one('menu_administrator_id','site_administrator_menu','menu_administrator_link LIKE \'%'.$pathInfo.'%\'');
        
 
		$data['treeMenu']=$adminMenu->buildTreeMenu_customsks(0, $initData,'','',$findIdMenu);
        
        $this->render('widget_menu_user',$data);
    }
}

?>
