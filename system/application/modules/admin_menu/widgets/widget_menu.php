<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_menu extends widget_lib
{
    //put your code here
    public function run()
    {
        $ci = &get_instance();
        $ci->load->model('admin_menu/admin_menu');
        $adminMenu = new admin_menu;
        $data = array();
        $session =  auth_admin_lib::staticGetAdminSession();
        $groupId = $session['detail']['admin_group_id'];
        $groupType = $session['detail']['admin_group_type'];

        $where = 'menu_administrator_location="admin" AND menu_administrator_is_active="1"';

        if ($groupType != 'superuser') {
            $where .= ' AND menu_administrator_id IN (SELECT administrator_privilege_administrator_menu_id FROM site_administrator_privilege
            WHERE administrator_privilege_administrator_group_id=' .  intval($groupId) . ')   ';
        }
        $pathInfo = isset($_SERVER['PATH_INFO']) ? substr($_SERVER['PATH_INFO'], 1) : '';
        $pathInfoExp = explode('/', $pathInfo);

        $pathInfo = $pathInfoExp[0] . '/' . $pathInfoExp[1] . '/' . (isset($pathInfoExp[2]) ? $pathInfoExp[2] : 'index');
        $cur_id_and_par = $adminMenu->get_current_id_and_parent($pathInfo);

        //echo $pathInfo;
        // exit;
        $findParIdMenu = $this->function_lib->get_one('menu_administrator_par_id', 'site_administrator_menu', 'menu_administrator_link LIKE \'%' . $pathInfo . '%\'');
        $findIdMenu = $this->function_lib->get_one('menu_administrator_id', 'site_administrator_menu', 'menu_administrator_link LIKE \'%' . $pathInfo . '%\'');

        $initData = $adminMenu->initalizeRecursiveDynamic($where);
        $data['treeMenu'] = $adminMenu->buildTreeMenuApp(0, $initData, 0, 0, $findParIdMenu, $findIdMenu, $cur_id_and_par);
        $this->render('widget_menu', $data);
    }
}
