<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_menu_user_header extends widget_lib{
    //put your code here
    public function run()
    {
        $ci=&get_instance();
        $ci->load->model('admin_menu/admin_menu');
        
        $adminMenu=new admin_menu;
        $data=array();
        
        
        $where='menu_administrator_location="user_header" AND menu_administrator_is_active="1"';
     
        $pathInfo=isset($_SERVER['PATH_INFO'])?substr($_SERVER['PATH_INFO'],1):'';
        $findParIdMenu=$this->function_lib->get_one('menu_administrator_par_id','site_administrator_menu','menu_administrator_link LIKE \'%'.$pathInfo.'%\'');
     
        $initData=$adminMenu->initalizeRecursiveDynamic($where);
        
        $themeArr=$ci->load->config->item('theme');
        $themeName=strtolower($themeArr['frontend']['name']);
        
        $functionMenu='buildTreeMenu_'.$themeName;
      
        $pathInfo=isset($_SERVER['PATH_INFO'])?substr($_SERVER['PATH_INFO'],1):'';
        //$pathInfoExp=explode('/',$pathInfo);
       // $pathInfo=(isset($pathInfoExp) AND !empty($pathInfoExp))?$pathInfoExp[0]:'';
       // echo $pathInfo;
        $findParIdMenu=$this->function_lib->get_one('menu_administrator_par_id','site_administrator_menu','menu_administrator_link LIKE \'%'.$pathInfo.'%\'');
        $findIdMenu=$this->function_lib->get_one('menu_administrator_id','site_administrator_menu','menu_administrator_link LIKE \'%'.$pathInfo.'%\'');
       
        $data['treeMenu']=$adminMenu->buildTreeMenuUserHeader(0, $initData,'','',$findParIdMenu);
        
        
        $this->render('widget_menu_user_footer',$data);
    }
}

?>
