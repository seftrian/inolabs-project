<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_group_menu extends widget_lib{
    //put your code here
    public function run()
    {
        $ci=&get_instance();
        $ci->load->model('admin_menu/admin_menu');
        $adminMenu=new admin_menu;
        $data=array();
        $session=  auth_admin_lib::staticGetAdminSession();
        $groupId=$session['detail']['admin_group_id'];
        $groupType=$session['detail']['admin_group_type'];
        $where='menu_administrator_location="admin" AND menu_administrator_is_active="1"';
        
        if($groupType!='superuser')
        {
            $where.=' AND menu_administrator_id IN (SELECT administrator_privilege_administrator_menu_id FROM site_administrator_privilege
            WHERE administrator_privilege_administrator_group_id='.  intval($groupId).')   ';
        }
        $pathInfo=isset($_SERVER['PATH_INFO'])?substr($_SERVER['PATH_INFO'],1):'';
        $pathInfoExp=explode('/',$pathInfo);
        
        $pathInfo=$pathInfoExp[0].'/'.$pathInfoExp[1].'/'.(isset($pathInfoExp[2])?$pathInfoExp[2]:'index');
        $parent_id=($this->input->get('parent_id')!='')?$this->input->get('parent_id'):0;
        $sql='SELECT * FROM site_administrator_menu WHERE '.$where.' AND menu_administrator_par_id='.intval($parent_id).'
            ORDER BY menu_administrator_order_by ASC';
        $exec=$this->db->query($sql);
        $where='menu_administrator_id='.intval($parent_id);
        $menu_title=$this->function_lib->get_one('menu_administrator_title','site_administrator_menu',$where);
        $data['menu_arr']=$exec->result_array();
        $data['menu_lib']=$adminMenu;
        $data['parent_id']=$parent_id;
        $data['menu_title']=($parent_id==0)?' Menu Utama':$menu_title;
        $this->render('widget_group_menu',$data);
    }
}

?>
