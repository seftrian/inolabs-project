<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('chat_ym_lib','admin_config/admin_config_lib'));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new chat_ym_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


     //   $this->pathImgArr=$this->mainModel->getImagePath();
        
    }

    public function test_status()
    {
        $icon=chat_ym_lib::set_status_by_icon('joe_engressia',$width='100',$height='');
        echo $icon;
        exit;
    }


     /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function set_status()
    {
        
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Chat Yahoo! Messenger';
        $description = 'Set icon online dan offline';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Chat Yahoo Messenger',
            'class' => "clip-yahoo",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
       $breadcrumbs_array[] = array(
            'name' => 'Set Status',
            'class' => "clip-question",
            'link' => base_url().'admin/'.$this->currentModule.'/set_status',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        $this->mainModel->setMainTable('site_configuration');
        $this->data['rowConfig']=$this->mainModel->findAll(1,'no limit','','');
        
        //cek ketersediaan group admin
        if(empty($this->data['rowConfig']))
        {
            show_404();
        }
        
        //extract data
        foreach($this->data['rowConfig'] AS $rowConfig)
        {
            $index=$rowConfig['configuration_index'];
            $value=$rowConfig['configuration_value'];
            $this->data[$index]=isset($_POST[$index])?$_POST[$index]:$value;
        }

        if($this->input->post('save'))
        {
            
            //handle logo
            $ym_online=$this->admin_config_lib->handleUploadConfig('config_ym_online');
             //handle favicon
            $ym_offline=$this->admin_config_lib->handleUploadConfig('config_ym_offline');
            if($ym_online['status']!=200)
            {
                $status=$ym_online['status'];
                $message=$ym_online['message'];
            }
            elseif($ym_offline['status']!=200)
            {
                $status=$ym_offline['status'];
                $message=$ym_offline['message'];
            }
            
            if($status==200)
            {
                $msg=base64_encode('Data berhasil disimpan');
                redirect(base_url().'admin/chat_ym/set_status?status=200&msg='.$msg);
            }
        }
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
        
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Chat Yahoo! Messenger';
        $description = 'Daftar Akun Live Chat';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Chat Yahoo Messenger',
            'class' => " clip-yahoo",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function add()
    {
   
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Chat Yahoo! Messenger';
        $description = 'Tambah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Chat Yahoo! Messenger',
            'class' => " clip-yahoo",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
        
            if($status==200)
            {
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $columns=array(
                    'chat_ym_full_name'=>$this->input->post('chat_ym_full_name',true),
                    'chat_ym_email'=>$this->input->post('chat_ym_email',true),
                    'chat_ym_mobilephone'=>$this->input->post('chat_ym_mobilephone',true),
                    'chat_ym_note'=>$this->input->post('chat_ym_note',true),
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $id=$this->function_lib->insert_id();
                   
                    $msg=base64_encode('Data berhasil disimpan');
                    if(isset($_GET['redirect']))
                    {
                        redirect(rawurldecode($_GET['redirect']));
                    }
                    else
                    {
                        redirect(base_url().'admin/'.$this->currentModule.'/add?status=200&msg='.$msg);
                    }
                    
                }


            }
        }

        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Ubah';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Chat Yahoo! Messenger',
            'class' => " clip-yahoo",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );

       $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/update',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $rowMenu=$this->mainModel->find('chat_ym_id='.  intval($id));
        if(empty($rowMenu))
        {
            show_error('Request anda tidak valid',500);
        }

        foreach($rowMenu AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }

        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($id);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
               $title=$this->input->post('news_title');
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $columns=array(
                    'chat_ym_full_name'=>$this->input->post('chat_ym_full_name',true),
                    'chat_ym_email'=>$this->input->post('chat_ym_email',true),
                    'chat_ym_mobilephone'=>$this->input->post('chat_ym_mobilephone',true),
                    'chat_ym_note'=>$this->input->post('chat_ym_note',true),
                );
                $this->mainModel->setPostData($columns);
                $where='chat_ym_id='.  intval($id);
                $hasSaved=$this->mainModel->save($where);
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {

                    $msg=base64_encode('Data berhasil disimpan');
                    redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status='.$status.'&msg='.$msg);

                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
      
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);

    }

     public function act_delete() {
        $arr_output = array();
        $arr_output['status'] = 500;
        $arr_output['message'] = 'Tidak ada aksi yang dilakukan';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' '; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {
                            $this->mainModel->setMainTable('site_chat_ym');
                            $this->mainModel->delete('chat_ym_id='.intval($id));
                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['status'] = 200;
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['status'] = 500;
            }
        }
        
        echo json_encode($arr_output);
    }

     public function delete($id='') {
        $arr_output=array();
        $arr_output['message'] =  'Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 500;
        $additional_where=' '; //default telah ngecek data dihapus
        if($this->available_data($id,$additional_where))
        {
            $this->mainModel->setMainTable('site_chat_ym');
            $this->mainModel->delete('chat_ym_id='.intval($id));
            $arr_output['message'] =  ' Konten telah dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

     /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one('chat_ym_id','site_chat_ym',
            'chat_ym_id='.intval($id).' '.$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }

     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();

        ?>
       
        <?php
        switch($method)
        {
             case 'index':
            ?>
        <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Edit', name: 'edit', width: 50, sortable: true, align: 'center' },
                            { display: 'Delete', name: 'delete', width: 50, sortable: true, align: 'center' },
                            { display: 'Nama Lengkap', name: 'full_name', width: 160, sortable: true, align: 'center' },
                            { display: 'Email/YM ID', name: 'email', width: 160, sortable: true, align: 'center' },
                            { display: 'No. Hp.', name: 'mobilephone', width: 160, sortable: true, align: 'center' },
                            { display: 'Keterangan', name: 'note', width: 160, sortable: true, align: 'center' },
                    
                        ],
                        buttons: [
                            { display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                        ],
                        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],
                      
                        sortname: "id",
                        sortorder: "desc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });
                    
                    function act_delete(com, grid) {
                        var div_alert=$("div.alert");

                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        div_alert.slideDown('medium');
                                        if(response['status']!=200)
                                        {
                                            div_alert.addClass('alert-danger');
                                            div_alert.removeClass('alert-success');
                                        }
                                        else
                                        {
                                            div_alert.removeClass('alert-danger');
                                            div_alert.addClass('alert-success');
                                        }
                                        div_alert.html(response['message']);
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    $(document).ready(function() {
                        grid_reload();
                    });

                  

                    function grid_reload() {
                        var chat_ym_full_name=$("input#chat_ym_full_name").val();
                        var chat_ym_email=$("input#chat_ym_email").val();
                        var chat_ym_mobilephone=$("input#chat_ym_mobilephone").val();
                        var chat_ym_note=$("input#chat_ym_note").val();
                        var link_service='?chat_ym_full_name='+chat_ym_full_name+'&chat_ym_email='+chat_ym_email+'&chat_ym_mobilephone='+chat_ym_mobilephone+'&chat_ym_note='+chat_ym_note;
                        $("#gridview").flexOptions({url:'<?php echo base_url().$this->currentModule; ?>/service_rest/get_data'+link_service}).flexReload();
                    }
                    
                    function delete_transaction(id)
                    { 
                        var div_alert=$("div.alert");
                        if(confirm('Delete?'))
                        {
                            var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$this->currentModule?>/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            div_alert.slideDown('medium');
                            if(response['status']!=200)
                            {
                                div_alert.addClass('alert-danger');
                                div_alert.removeClass('alert-success');
                            }
                            else
                            {
                                div_alert.removeClass('alert-danger');
                                div_alert.addClass('alert-success');
                            }
                            div_alert.html(response['message']);
                            grid_reload();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            grid_reload();
                            return false;
                        });
                        }
                        return false;
                       
                    }

                    
                </script>
            <?php
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
