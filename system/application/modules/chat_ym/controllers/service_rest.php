<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('chat_ym_lib');
        $this->mainModel=new chat_ym_lib;

    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";

        $chat_ym_full_name=$this->input->get('chat_ym_full_name',true);
        $chat_ym_full_name=str_replace('%20', ' ', $chat_ym_full_name);
        $chat_ym_email=$this->input->get('chat_ym_email',true);
        $chat_ym_email=str_replace('%20', ' ', $chat_ym_email);
        $chat_ym_mobilephone=$this->input->get('chat_ym_mobilephone',true);
        $chat_ym_mobilephone=str_replace('%20', ' ', $chat_ym_mobilephone);
        $chat_ym_note=$this->input->get('chat_ym_note',true);
        $chat_ym_note=str_replace('%20', ' ', $chat_ym_note);

        $where=' chat_ym_full_name LIKE "%'.$chat_ym_full_name.'%" AND chat_ym_email LIKE "%'.$chat_ym_email.'%" ';
        $where.=' AND chat_ym_mobilephone LIKE "%'.$chat_ym_mobilephone.'%" AND chat_ym_note LIKE "%'.$chat_ym_note.'%" ';
        $params['where'] =$where;

        $params['order_by'] = "
            chat_ym_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;

            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$chat_ym_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" class="btn btn-xs btn-bricky" title="Delete" onclick="delete_transaction(\''.$chat_ym_id.'\');"><i class="clip-remove"></i></a>';
            $username_email=$this->mainModel-> get_email_username($chat_ym_email);
           
            $preview=chat_ym_lib::set_status_by_icon($username_email,$width='30',$height='');
            $entry = array('id' => $chat_ym_id,
                'cell' => array(
                    'no' =>  $no,
                    'delete' =>  $delete,
                    'edit' =>  $edit,
                    'full_name' =>$chat_ym_full_name,
                    'email' =>$chat_ym_email.'<p style="float:left;">'.$preview.'</p>',
                    'mobilephone' =>$chat_ym_mobilephone,
                    'note' =>$chat_ym_note,
                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
