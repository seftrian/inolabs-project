<?php

class service_rest extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('master_job_lib');
        $this->mainModel=new master_job_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    function get_data() {

        $params = isset($_POST) ? $_POST : array();
        $params['table'] = $this->mainModel->getMainTable();
        $params['select'] = "
            ".$this->mainModel->getMainTable().".*
        ";

        $job_title=$this->input->get('job_title',true);
        $job_title=str_replace('%20', ' ', $job_title);
        $job_content=$this->input->get('job_content',true);
        $job_content=str_replace('%20', ' ', $job_content);

        $where=1;
        

        $where.=' AND job_title LIKE "%'.$job_title.'%" AND job_content LIKE "%'.$job_content.'%" ';
        $params['where'] =$where;

        $params['order_by'] = "
            job_id DESC
        ";

        $query = $this->function_lib->get_query_data($params);
        $total = $this->function_lib->get_query_data($params, true);

        header("Content-type: application/json");
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $json_data = array('page' => $page, 'total' => $total, 'rows' => array());
        $prev_trx = '';
        $no = 0 + ($_POST['rp'] * ($page - 1));
        foreach ($query->result() as $row) {
            
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }
          $no++;
         
            $logo_name= (trim($job_photo)=='')?'-': pathinfo($job_photo,PATHINFO_FILENAME);
            $logo_ext=  pathinfo($job_photo,PATHINFO_EXTENSION);
            $logo_file=$logo_name.'.'.$logo_ext;
            $pathImgUrl=$this->pathImgArr['pathUrl'];
            $pathImgLoc=$this->pathImgArr['pathLocation'];
            $imgProperty=array(
            'width'=>70,
            'height'=>70,
            'imageOriginal'=>$logo_file,
            'directoryOriginal'=>$pathImgLoc,
            'directorySave'=>$pathImgLoc.'7070/',
            'urlSave'=>$pathImgUrl.'7070/',
            );
            $img=$this->function_lib->resizeImageMoo($imgProperty);

            $edit='<a class="btn btn-xs btn-primary" href="'.base_url().'admin/'.$this->currentModule.'/update/'.$job_id.'" title="Edit"><i class="clip-pencil"></i></a>';
            $delete=' <a href="#" title="Delete" onclick="delete_transaction(\''.$job_id.'\');"><i class="clip-remove"></i> Hapus</a>';
            $content=strip_tags(html_entity_decode($job_content));
            $content=(strlen($content)<150)?$content:substr($content,0,150);
               //Pilihan Opsi
            $opsi ='<div class="dropdown" style="position:absolute;padding:0px;">
                        <div class="btn-group" style="margin-top:-10px;">
                          <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Pilih Opsi
                          </button>
                          <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="text-align:left">
                            <li><a href="'.base_url().'admin/master_job/aplicant/index/'.$job_id.'"><i class="fa fa-user"></i> Pelamar</a></li>
                            <li>'.$delete.'</li>
                            </ul>
                        </div>
            </div>';
            $count_applicant=$this->function_lib->get_one('COUNT(*)','site_job_applicant','job_applicant_job_id='.intval($job_id));
            $div_count_applicant='<span style="margin-top:10px;float:right;" class="text-danger"><a href="'.base_url().'admin/master_job/aplicant/index/'.$job_id.'"><i class="fa fa-user"></i> '.$count_applicant.'</a></span>';
            $entry = array('id' => $job_id,
                'cell' => array(
                    'no' =>  $no,
                    'edit' =>  $edit,
                    'options' =>  $opsi,
                    'title' =>$job_title.''.$div_count_applicant,
                    'content' =>$content,
                    'meta_description' => $job_meta_description,
                    'meta_tags' => $job_meta_tags,
                    'photo' => '<img src="'.$img.'" />',
                    'input_by' => $job_input_by,

                ),
            );
            $json_data['rows'][] = $entry;
            

        }

        echo json_encode($json_data);
    }
    
}
