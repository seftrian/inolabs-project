<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin_aplicant extends admin_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('master_job_lib'));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new master_job_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


      //  $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index($job_id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Lowongan Kerja';
        $description = 'Informasi Lowongan dan Pelamar';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
        $breadcrumbs_array[] = array(
            'name' => 'Lowongan Kerja',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/master_job/index',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Pelamar',
            'class' => "fa fa-envelope-o",
            'link' => '#',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        $this->data['job_id']=$job_id;
        $where='job_id='.intval($job_id);
        $this->mainModel->setMainTable('site_job');
        $this->data['job_arr']=$this->mainModel->find($where);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    

     public function delete($id='') {
        $arr_output=array();
        $arr_output['message'] =  'Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 500;
        $additional_where=' '; //default telah ngecek data dihapus
        if($this->available_data($id,$additional_where))
        {
            $this->mainModel->setMainTable('site_job_applicant');
            $this->mainModel->delete('job_applicant_id="'.$id.'"');

            $this->mainModel->setMainTable('site_job_applicant_reply');
            $this->mainModel->delete('job_applicant_reply_job_applicant_id="'.$id.'"');
            $arr_output['message'] =  ' Konten telah dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

     /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one('job_applicant_id','site_job_applicant',
            'job_applicant_id="'.$id.'" '.$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }

     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();

        ?>
       
        <?php
        switch($method)
        {
             case 'index':
            ?>
        <link href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
            <script type="text/javascript">
            
            $(function(){
                get_list_all_sender();
                //default message
                read_message();

                //modals
                UIModals.init();

            });

            function refresh_inbox()
            {
                get_list_all_sender(0);
                //default message
                read_message();
            }

                  <!-- script modal-->
            var UIModals = function () {
                //function to initiate bootstrap extended modals
                var initModals = function () {
                    $.fn.modalmanager.defaults.resize = true;
                    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
                        '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                        '<div class="progress progress-striped active">' +
                        '<div class="progress-bar" style="width: 100%;"></div>' +
                        '</div>' +
                        '</div>';
                };
                return {
                    init: function () {
                        initModals();

                    }
                };
            }();

            /*
            balas pesan
            */
            function reply_message(from,to,subject,message,ask_id)
            {
                $("input#from").val(from);
                $("input#to").val(to);
                $("input#subject").val(subject);
                $("textarea#message").val(message);
                $("input[name='ask_id']").val(0);
                if(typeof(ask_id)!='undefined')
                {
                    $("input[name='ask_id']").val(ask_id);
                }
            }

            function send_message()
            {
                var div_alert=$("div.alert");
                if(confirm('Kirim'))
                {
                    var jqxhr=$.ajax({
                    url:'<?php echo base_url().$this->currentModule?>/service_rest_aplicant/send_message',
                    data:$("form#form-compose-message").serializeArray(),
                    dataType:'json',
                    type:'post'
                    });
                    jqxhr.success(function(response){
                        div_alert.slideDown('medium');
                        if(response['status']!=200)
                        {
                            div_alert.addClass('alert-danger');
                            div_alert.removeClass('alert-success');
                        }
                        else
                        {
                            div_alert.removeClass('alert-danger');
                            div_alert.addClass('alert-success');
                            $('#compose_message').modal('toggle');
                        }
                        div_alert.html(response['message']);
                         setInterval(function(){
                        div_alert.slideUp('medium');
                        },10000);
                        return false;
                    });
                    jqxhr.error(function(){
                        alert('an error has occurred, please try again.');
                        return false;
                    });
                }
                return false;
                
            }

            var container_sender_loading=$("span#info-loading");
            var container_sender=$("ul.messages-list");
            var container_message=$("div.messages-content");

            function delete_transaction(id)
            { 
                        var div_alert=$("div.alert");
                        if(confirm('Delete?'))
                        {
                            var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$this->currentModule?>/aplicant/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            div_alert.slideDown('medium');
                            if(response['status']!=200)
                            {
                                div_alert.addClass('alert-danger');
                                div_alert.removeClass('alert-success');
                            }
                            else
                            {
                                div_alert.removeClass('alert-danger');
                                div_alert.addClass('alert-success');
                            }
                            div_alert.html(response['message']);
                             get_list_all_sender();
                            //default message
                            read_message();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            get_list_all_sender();
                            //default message
                            read_message();
                            return false;
                        });
                        }
                        return false;
                       
            }
            /*
            dapatkan list semua pesan
            */
            function get_list_all_sender(offset)
            {
                var job_id=$("#job_id").val();
                var limit=$("#limit").val();
                var query_message=$('#query_message').val();
                if(typeof(offset)=='undefined')
                {
                    var offset=$("#offset").val();
                }

                if(typeof(query_message)=='undefined')
                {
                    query_message='';
                }
                else
                {
                    container_message.html('');
                }

                container_sender_loading.show();
                container_sender_loading.html('loading...');
                $.getJSON('<?php echo base_url().$this->currentModule?>/service_rest_aplicant/get_list_all_sender/'+job_id+'/'+limit+'/'+offset+'?query_message='+query_message,function(json_response){
                    if(json_response['status']==200)
                    {
                        if(offset==0)
                        {
                            container_sender.html(json_response['message']);
                        }
                        else
                        {
                            container_sender.append(json_response['message']);
                        }

                        container_sender_loading.hide();
                    }

                });
            }

            /*
            load more message
            */
            function load_more_message(offset)
            {
                $("#offset").val(offset);
                get_list_all_sender(offset);
            }


            function read_message(elem,ask_id)
            {
                var job_id=$("#job_id").val();
                container_message.html('loading...');
                if(typeof(elem)!='undefined')
                {
                    elem.removeClass('active');
                }
                if(typeof(ask_id)=='undefined')
                {
                    ask_id=0;
                }
                $.getJSON('<?php echo base_url().$this->currentModule?>/service_rest_aplicant/read_message/'+job_id+'/'+ask_id,function(json_response){
                    if(json_response['status']==200)
                    {
                        container_message.html(json_response['message']);
                        container_message.after(json_response['paging']);
                    }
                });
            }
            </script>
            <?php
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
