<?php

class service_rest_aplicant extends front_controller {

    protected $mainModel;
    public $email_from;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('master_job_lib');
        $this->mainModel=new master_job_lib;

       // $this->pathImgArr=$this->mainModel->getImagePath();
        $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->config->slash_item('base_url'));
        $this->email_from='noreply@'.$web_name;
    }

     public function send_message_visitor()
    {
        $websiteConfig = $this->system_lib->getWebsiteConfiguration();

        $response=$this->mainModel->formValidation_send_message();
        $status=$response['status'];
        $message=$response['message'];
        $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';

        if($status==200)
        {
               $this->load->library('send_mail');
               $sendMail=new send_mail; 
               $email_from=$this->input->post('from',true);
               $email_to=$this->input->post('to',true);
               $email_subject=$this->input->post('subject',true);
               $email_message=$this->input->post('message',true);
               $email_to_name='';
               $ask_id=$this->input->post('ask_id');
               $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->config->slash_item('base_url'));

                //email for admin
               $emailProperty=array(
                   'email_to'=>$email_to,
                   'email_subject'=>$email_subject,
                   'email_message'=>$email_message,
                   'email_from'=>'noreply@'.$web_name,
                   'email_from_name'=>$web_name,
                   'email_from_organization'=>'',
               );
             
               $sendMail->run($emailProperty);
               $status=200;
               $message='Pesan terkirim';

               $column=array(
                'ask_par_id'=>($ask_id>0)?$ask_id:null,
                'ask_email'=>$email_from,
                'ask_full_name'=>$email_from,
                'ask_subject'=>$email_subject,
                'ask_message'=>$email_message,
               );
               $this->db->insert('site_ask',$column);

               $this->function_lib->insert_notification($email_from.' ('.$email_subject.')',$email_message);
        }

        $response= array(
            'status'=>$status,
            'message'=>$message,
        );
        echo json_encode($response);
    }

    public function send_message()
    {
        $response=$this->mainModel->formValidation_send_message();
        $status=$response['status'];
        $message=$response['message'];
        $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';

        if($status==200)
        {
               $this->load->library('send_mail');
               $sendMail=new send_mail; 
               $email_to=$this->input->post('to',true);
               $email_subject=$this->input->post('subject',true);
               $email_message=$this->input->post('message',true);
               $email_to_name='';
               $ask_id=$this->input->post('ask_id');
               $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->config->slash_item('base_url'));

                //email for admin
               $emailProperty=array(
                   'email_to'=>$email_to,
                   'email_subject'=>$email_subject,
                   'email_message'=>$email_message,
                   'email_from'=>'noreply@'.$web_name,
                   'email_from_name'=>$web_name,
                   'email_from_organization'=>'',
               );
             
               $sendMail->run($emailProperty);
               $status=200;
               $message='Pesan terkirim';

               $column=array(
                'ask_par_id'=>($ask_id>0)?$ask_id:null,
                'ask_email'=>'noreply@'.$web_name,
                'ask_full_name'=>$admin_username,
                'ask_subject'=>$email_subject,
                'ask_message'=>$email_message,
                'ask_status'=>'replied',
                'ask_input_by'=>$admin_username,
                'ask_update_datetime'=>date('Y-m-d H:i:s'),
               );
               $this->db->insert('site_ask',$column);
        }

        $response= array(
            'status'=>$status,
            'message'=>$message,
        );
        echo json_encode($response);
    }

    /**
    dapatkan data
    */

    public function get_list_all_sender($job_id=0,$limit=1,$offset=0)
    {

        $this->mainModel->setMainTable('site_job_applicant');
        $query_message=$this->input->get('query_message',true);
        $additional_where='';
        if(trim($query_message)!='')
        {
            $additional_where='AND (
                            job_applicant_job_title LIKE "%'.$query_message.'%" OR job_applicant_experience_year LIKE "%'.$query_message.'%" 
                            OR job_applicant_last_company LIKE "%'.$query_message.'%" OR job_applicant_last_job_functions LIKE "%'.$query_message.'%"
                            OR job_content LIKE "%'.$query_message.'%" OR job_applicant_last_job_functions LIKE "%'.$query_message.'%"
                            )';
        }
        $select='*';
        $where='job_applicant_job_id='.intval($job_id).' '.$additional_where;
        $join='
        INNER JOIN site_job ON job_applicant_job_id=job_id
        INNER JOIN sys_person ON person_id=job_applicant_person_id
        ';
        $count=$this->mainModel->countAllCustom($where,$select,$join);
        $status=200;
        $message='';

        

        $results=array();
        $content_message='';
        $div_paging='';
        $current_load=$limit;
        $simulate_next_page=$offset+$limit;
        $next_page=($simulate_next_page<$count)?(ceil($offset+$limit)):(ceil($offset));
        $show_paging=($simulate_next_page<$count)?1:0;
        if($offset>0)
        {
            $current_load=$offset+$limit;
        }
        if($count)
        {
            $select='site_job_applicant.*, person_full_name';
            $results=$this->mainModel->findAllCustom($where,$limit,$offset,'job_applicant_id DESC',$select,$join);

            ob_start();
            foreach($results AS $rowArr)
            {
                foreach($rowArr AS $variable=>$value)
                {
                    ${$variable}=$value;
                }
                $user_email=function_lib::get_person_email_addres($job_applicant_person_id);
                $subject=(strlen($job_applicant_job_title)>25)?substr($job_applicant_job_title,0,25).'[...]':$job_applicant_job_title;
                $ask_message_value=(strlen($job_applicant_application_letter)>80)?substr($job_applicant_application_letter,0,80).'[...]':$job_applicant_application_letter;
                $time_elapsed=function_lib::time_elapsed_string($job_applicant_timestamp);
                $is_unread=($job_applicant_has_read=='N')?'active':'';
                $ask_email_value=(strlen($person_full_name)>15)?substr($person_full_name,0,15).'...':$person_full_name;
                ?>
                  <li class="messages-item <?php echo $is_unread;?>" >
                            <span onclick="read_message($(this),<?php echo $job_applicant_id?>);return false;"><strong><?php echo $ask_email_value?></strong></span>
                            <div class="messages-item-time">
                                <span class="text"><?php echo $time_elapsed?></span>
                                <div class="messages-item-actions">
                                    <a data-toggle="modal" title="Reply" href="#compose_message" onclick="reply_message('<?php echo $this->email_from?>','<?php echo $user_email?>','Re:<?php echo $job_applicant_job_title?>','',<?php echo $job_applicant_id?>);return false;" ><i class="fa fa-mail-reply"></i></a>
                                    <a data-toggle="dropdown" href="#"  onclick="delete_transaction('<?php echo $job_applicant_id?>');return false;">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </div>
                            </div>
                            <span onclick="read_message($(this),<?php echo $job_applicant_id?>);return false;"><?php echo $subject?></span>
                            <span onclick="read_message($(this),<?php echo $job_applicant_id?>);return false;" class="messages-item-preview"><?php echo $ask_message_value?></span>
                    </li>
                <?php
            }

            $message.=ob_get_contents();
            ob_end_clean();

            $status=200;
        }

        if($show_paging==1)
        {
            $div_paging='<span style="text-align:center;"><a class="btn btn-xs btn-info" onclick="load_more_message('.$next_page.');$(\'li.paging\').hide();return false;">load more</a></span>';
            $message.='<li class="messages-item paging" style="text-align:center;">'.$div_paging.'</li>';

        }
        $response= array(
            'status'=>$status,
            'message'=>$message,
            'results'=>$results,
            'current_load'=>$current_load,
            'next_page'=>$next_page,
            'count'=>$count,
            'show_paging'=>$show_paging,
        );
        echo json_encode($response);
    }

    /**
    baca pesan
    @param int $ask_id
    */
    public function read_message($default_job_id, $id=0)
    {
        if($id==0)
        {
            $where='job_applicant_has_read IN ("Y")
            AND job_applicant_job_id='.intval($default_job_id).'
            ORDER BY job_applicant_read_datetime DESC';
        }
        else
        {
            $where='job_applicant_id='.intval($id);
        }
        $this->mainModel->setMainTable('site_job_applicant');
        $row=$this->mainModel->find($where);
        $status=200;
        $message='';
        if(!empty($row))
        {
            //update pesan telah dibaca
            $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
            if($id!=0)
            {
                $where.=' AND job_applicant_has_read="N"';    
                $column=array(
                    'job_applicant_read_datetime'=>date('Y-m-d H:i:s'),
                    'job_applicant_has_read'=>'Y',
                    'job_applicant_read_by'=>$admin_username,
                );
                $this->mainModel->setPostData($column);
                $this->mainModel->save($where);

                $where='job_applicant_id='.intval($id);    
                $column=array(
                    'job_applicant_read_datetime'=>date('Y-m-d H:i:s'),
                );
                $this->mainModel->setPostData($column);
                $this->mainModel->save($where);
            }
           


            ob_start();
            foreach($row AS $variable=>$value)
            {
                ${$variable}=$value;
            }

            $person_full_name=function_lib::get_person_full_name($job_applicant_person_id);
            $person_address=function_lib::get_person_addres($job_applicant_person_id);
            $email_address=function_lib::get_person_email_addres($job_applicant_person_id);
            $person_phone=function_lib::get_person_phone($job_applicant_person_id);
            $html_content='<center><table style="width:70%;margin:10px;display:none;" class="table table-hover" id="table_ask_id_'.$job_applicant_id.'">';
            $html_content.='<tr>';
            $html_content.='<th style="width:25%;">Nama Lengkap</th>';
            $html_content.='<td>: '.$person_full_name.'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Tempat Tinggal</th>';
            $html_content.='<td>: '.(($person_address!='')?$person_address:'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Pendidikan Tertinggi</th>';
            $html_content.='<td>: '.(($job_applicant_experience_year!='')?$job_applicant_experience_year.'th':'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Email</th>';
            $html_content.='<td>: '.(($email_address!='')?$email_address:'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>No. Kontak</th>';
            $html_content.='<td>: '.(($person_phone!='')?$person_phone:'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Jumlah tahun pengalaman</th>';
            $html_content.='<td>: '.(($job_applicant_experience_year!='')?$job_applicant_experience_year.'th':'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Jabatan Pekerjaan Terakhir</th>';
            $html_content.='<td>: '.(($job_applicant_last_position!='')?$job_applicant_last_position:'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Fungsi Pekerjaan Terakhir</th>';
            $html_content.='<td>: '.(($job_applicant_last_job_functions!='')?$job_applicant_last_job_functions:'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Perusahaan Terakhir</th>';
            $html_content.='<td>: '.(($job_applicant_last_company!='')?$job_applicant_last_company:'n/a').'</td>';
            $html_content.='</tr>';
            $html_content.='<tr>';
            $html_content.='<th>Periode Pekerjaan</th>';
            $html_content.='<td>: '.(($job_applicant_month_start!='' AND $job_applicant_year_start!='')?convert_month($job_applicant_month_start).' '.$job_applicant_year_start.' s/d '.convert_month($job_applicant_month_end).' '.$job_applicant_year_end:'n/a').'</td>';
            $html_content.='</tr>';

            $html_content.='</table></center>';
            $email_sender=$email_address;


            //cek balasan
            $this->mainModel->setMainTable('site_job_applicant_reply');
            $where_reply='job_applicant_reply_job_applicant_id='.intval($job_applicant_id);
            $findAll=$this->mainModel->findAll($where_reply,'no limit','no offset','job_applicant_reply_id ASC');
            
            ?>
            <div class="message-header">
                <div class="message-time">
                    <?php echo convert_datetime_name($job_applicant_timestamp)?>
                </div>
                <div class="message-from">
                    <?php echo $person_full_name.' <'.$email_sender.'>'?>
                </div>
              
                <div class="message-subject">
                  <?php echo $job_applicant_job_title?>
                </div>
                <div class="message-actions">
                    <a title="Move to trash" href="#" onclick="delete_transaction('<?php echo $job_applicant_id?>');return false;"><i class="fa fa-trash-o"></i></a>
                    <a title="Reply" onclick="reply_message('<?php echo $this->email_from?>','<?php echo $email_sender?>','Re:<?php echo $job_applicant_job_title?>','',<?php echo $job_applicant_id?>);return false;" href="#compose_message" data-toggle="modal"><i class="fa fa-reply"></i></a>
                </div>
            </div>
            <?php
            if(!empty($findAll))
            {
                ?>
                <div class="message-content">
                <p class="text-muted" style="display:none;" id="ask_id_<?php echo $job_applicant_id?>">
                <?php echo $job_applicant_application_letter?>   
                <?php echo $html_content?> 
                </p>
                <a href="#" onclick="$('#ask_id_<?php echo $job_applicant_id?>').toggle();$('#table_ask_id_<?php echo $job_applicant_id?>').toggle();return false;">-------</a>
                </div>
                <?php
                $no=1;
                $num_reply=count($findAll);
                foreach($findAll AS $rowArr)
                {
                    $ask_reply_id=$rowArr['job_applicant_reply_id'];
                    $ask_reply_message=$rowArr['job_applicant_reply_message'];
                    $ask_reply_input_by=$rowArr['job_applicant_reply_by'];
                    $ask_reply_timestamp=$rowArr['job_applicant_reply_timestamp'];
                    if($no==$num_reply)
                    {
                        ?>
                        <div class="message-content">
                            <strong><?php echo $ask_reply_input_by?></strong> / <?php echo convert_datetime_name($ask_reply_timestamp)?><br />
                            <p>
                            <?php echo $ask_reply_message?>    
                            </p>
                        </div>
                        <?php
                    }
                    else
                    {
                    ?>
                    <div class="message-content">
                        <strong><?php echo $ask_reply_input_by?></strong> / <?php echo convert_datetime_name($ask_reply_timestamp)?><br />
                        <p class="text-muted"  style="display:none;" id="ask_id_<?php echo $ask_reply_id?>">
                        <?php echo $ask_reply_message?>    
                      
                        </p>
                        <a href="#" onclick="$('#ask_id_<?php echo $ask_reply_id?>').toggle();$('#table_ask_id_<?php echo $job_applicant_id?>').toggle();return false;">-------</a>
                    </div>
                    <?php
                    }
                    $no++;
                }

            }
            else
            {
                ?>
                <div class="message-content">
                <p>
                <?php echo $job_applicant_application_letter?>  
                <?php echo '<br />'.$html_content?>
                </p>
                </div>
                <?php
            }
            ?>
           


            <?php
            $message=ob_get_contents();
            ob_end_clean();
            $status=200;

        }
        else
        {
            $status=200;
            $message='<div class="message-content">
                <p style="text-align:center;">
                Belum ada pesan yang dibaca.    
                </p>
                </div>';
        }
            $response= array(
            'status'=>$status,
            'message'=>$message,
            );
        echo json_encode($response);
    }
    
}
