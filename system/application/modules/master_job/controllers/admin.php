<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_group
 *
 * @author tejomurti
 */

class admin extends admin_controller{
    
    public $mainModel; //model utama dari module ini

    //put your code here
    public function __construct() {
        parent::__construct();
        
        //load model
        $this->load->model(array('master_job_lib'));
        $this->load->helper(array('pagination','tinymce'));
        
        //inisialisasi model
        $this->mainModel=new master_job_lib;
        parse_str($_SERVER['QUERY_STRING'], $_GET);


        $this->pathImgArr=$this->mainModel->getImagePath();
        
    }
    
    /**
     * menampilkan daftar data group menu
     * untuk menampilkan pesan dynamic lwt $_GET['msg'] && $_GET['status']
     */
    public function index()
    {
       
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['status'] = $status;
        $this->data['message'] = $message;

        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Lowongan Kerja';
        $description = '';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
         $breadcrumbs_array[] = array(
            'name' => 'Lowongan Kerja',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
    
    /**
     * untuk menambahkan data
     */
    public function add()
    {
   
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template

        $this->data['seoTitle'] = 'Lowongan Kerja';
        $description = 'Tambah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
      
        $breadcrumbs_array[] = array(
            'name' => 'Lowongan Kerja',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
           $breadcrumbs_array[] = array(
            'name' => 'Tambah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation();
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];
        
            if($status==200)
            {
                $title=$this->input->post('job_title');
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $content=$this->input->post('job_content',true);
                
                $columns=array(
                    'job_title'=>$this->input->post('job_title',true),
                    'job_permalink'=>$this->mainModel->generate_permalink($title),
                    'job_content'=>htmlentities($content),
                    'job_input_by'=>$admin_username,
                    'job_meta_description'=>$this->input->post('job_meta_description',true),
                    'job_meta_tags'=>$this->input->post('job_meta_tags',true),
                    'job_show_time'=>$this->input->post('job_show_time',true),
                    'job_start_date'=>($this->input->post('job_start_date',true) AND strtotime($this->input->post('job_start_date',true))>0)?date('Y-m-d',strtotime($this->input->post('job_start_date',true))):'0000-00-00',
                    'job_end_date'=>($this->input->post('job_end_date',true) AND strtotime($this->input->post('job_end_date',true))>0)?date('Y-m-d',strtotime($this->input->post('job_end_date',true))):'0000-00-00',
             
                );
                $this->mainModel->setPostData($columns);
                $hasSaved=$this->mainModel->save();
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {
                    $id=$this->function_lib->insert_id();

                    //handle logo
                    $logo=$this->mainModel->handleUpload($id,'job_photo');
                    $response_upload=$this->mainModel->save_file($id);
                     $status=($logo['status']!=200)?500:(
                            ($response_upload['status']!=200)?500:200
                            );
                    $message=($logo['status']!=200)?$logo['message']:(
                        ($response_upload['status']!=200)?$response_upload['message']:'OK'
                        );
                    if($status!=200 AND $message!='')
                    {
                        $msg=base64_encode($message);
                        redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status='.$status.'&msg='.$msg);

                    }
                    else
                    {
                        $msg=base64_encode('Data berhasil disimpan');
                        if(isset($_GET['redirect']))
                        {
                            redirect(rawurldecode($_GET['redirect']));
                        }
                        else
                        {
                            redirect(base_url().'admin/'.$this->currentModule.'/add?status=200&msg='.$msg);
                        }
                    }
                    
                }


            }
            //$this->session->set_flashdata('confirmation', '<div class="confirmation alert alert-danger">'.$message.'</div>');
        }

        
        $this->data['status']=$status;
        $this->data['message']=$message;
        
        $this->data['footerScript']=$this->footerScript(__FUNCTION__);

        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);
    }
   
    /**
     * untuk update data via form
     * @param int $id
     */
    public function update($id=0)
    {
        $status=(isset($_GET['status']) AND trim($_GET['status'])!='')?$_GET['status']:200;
        $message=(isset($_GET['msg']) AND trim($_GET['msg'])!='')?base64_decode($_GET['msg']):'';
        
        $this->data['themeId'] = $this->themeId; //dapatkan themeId
        $this->data['themeUrl'] = $this->themeUrl; //dapatkan themeId
        $this->data['templateName'] = $this->templateName; //nama template
        $this->data['seoTitle'] = 'Lowongan Kerja';
        $description = 'Ubah';
        $breadcrumbs_array = array();
        $breadcrumbs_array[] = array(
            'name' => 'Home',
            'class' => "clip-home-3",
            'link' => base_url().'admin/dashboard',
            'current' => false, //boolean
        );
       
        $breadcrumbs_array[] = array(
            'name' => 'Lowongan Kerja',
            'class' => "clip-grid-2",
            'link' => base_url().'admin/'.$this->currentModule.'/index',
            'current' => false, //boolean
        );
           $breadcrumbs_array[] = array(
            'name' => 'Ubah',
            'class' => "clip-pencil",
            'link' => base_url().'admin/'.$this->currentModule.'/add',
            'current' => true, //boolean
        );
        $this->data['breadcrumbs'] = $this->function_lib->show_breadcrumbs($breadcrumbs_array,$this->data['seoTitle'],$description,$this->templateName,$this->themeId);
        
        
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        
        $rowMenu=$this->mainModel->find('job_id='.  intval($id));
        if(empty($rowMenu))
        {
            show_error('Request anda tidak valid',500);
        }

        foreach($rowMenu AS $variable=>$value)
        {
            $this->data[$variable]=$value;
        }

        if($this->input->post('save'))
        {
            //inisialisasi validation
            $isValidationPassed=$this->mainModel->formValidation($id);
            $status=$isValidationPassed['status'];
            $message=($status==500 AND $isValidationPassed['message']=='')?validation_errors():$isValidationPassed['message'];

            if($status==200)
            {
               $title=$this->input->post('job_title');
                $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';
                $content=$this->input->post('job_content',true);
                $columns=array(
                    'job_title'=>$this->input->post('job_title',true),
                    'job_permalink'=>$this->mainModel->generate_permalink($title,$id),
                    'job_content'=>htmlentities($content),
                    'job_input_by'=>$admin_username,
                    'job_meta_description'=>$this->input->post('job_meta_description',true),
                    'job_meta_tags'=>$this->input->post('job_meta_tags',true),
                    'job_show_time'=>$this->input->post('job_show_time',true),
                    'job_start_date'=>($this->input->post('job_start_date',true) AND strtotime($this->input->post('job_start_date',true))>0)?date('Y-m-d',strtotime($this->input->post('job_start_date',true))):'0000-00-00',
                    'job_end_date'=>($this->input->post('job_end_date',true) AND strtotime($this->input->post('job_end_date',true))>0)?date('Y-m-d',strtotime($this->input->post('job_end_date',true))):'0000-00-00',
                );

                $this->mainModel->setPostData($columns);
                $where='job_id='.  intval($id);
                $hasSaved=$this->mainModel->save($where);
                $status=$hasSaved['status']; //status saat penyimpanan
                if($status==200)
                {

                    $msg=base64_encode('Data berhasil disimpan');
                    //handle logo
                    $logo=$this->mainModel->handleUpload($id,'job_photo');
                    if($logo['status']!=200)
                    {
                        $status=$logo['status'];
                        $message=strip_tags($logo['message']);
                        $msg=base64_encode('Terjadi error saat upload file. '.$message);
                    }
                    
                    $response_upload=$this->mainModel->save_file($id);
                    if($response_upload['status']!=200)
                    {
                        $status=$response_upload['status'];
                        $msg=strip_tags(base64_encode($response_upload['message']));
                    }
                    redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status='.$status.'&msg='.$msg);

                }
            }
        }
        $this->data['status']=$status;
        $this->data['message']=$message;
      
        $this->data['pathImgArr']=$this->pathImgArr;
        
        $this->data['list_files']=$this->mainModel->list_files($id);

        $this->data['footerScript']=$this->footerScript(__FUNCTION__);
        template($this->themeId, get_class() . '/' . __FUNCTION__, $this->data);

    }
    

    /**
     * hapus data 
     * @param int $id
     * @param $output
     */
    public function delete_image($id=0,$output='redirect')
    {
        if(!is_numeric($id) OR $id==0)
        {
            show_error('Request anda tidak valid',500);
        }
        $where='job_id='.  intval($id);
        $this->data['rowAdmin']=$this->mainModel->find($where);

        if(!empty($this->data['rowAdmin']))
        {
           $sql='UPDATE site_job SET job_photo="-" WHERE '.$where; 
           $this->db->query($sql);
           redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=200&msg='.base64_encode('Gambar berhasil dihapus'));
        }
        else
        {
            $msg='Data tidak ditemukan';
            redirect(base_url().'admin/'.$this->currentModule.'/update/'.$id.'?status=500&msg='.base64_encode($msg));
        }
        
    }

     public function act_delete() {
        $arr_output = array();
        $arr_output['status'] = 500;
        $arr_output['message'] = 'Tidak ada aksi yang dilakukan';

        //delete
        if ($this->input->post('delete') != FALSE) {
            $arr_item = json_decode($_POST['item']);
            if (is_array($arr_item)) {
                $deleted_count = 0;
                $undeleted_count = 0;
                foreach ($arr_item as $id) {

                        $additional_where=' '; //default telah ngecek data dihapus
                        if($this->available_data($id,$additional_where))
                        {
                            $this->mainModel->setMainTable('site_job');
                            $this->mainModel->delete('job_id='.intval($id));
                            $deleted_count++;
                        }   
                }
                $arr_output['message'] = $deleted_count . ' data berhasil dihapus.';
                $arr_output['status'] = 200;
            } else {
                $arr_output['message'] = 'Anda belum memilih data.';
                $arr_output['status'] = 500;
            }
        }
        
        echo json_encode($arr_output);
    }

     public function delete($id='') {
        $arr_output=array();
        $arr_output['message'] =  'Tidak ada aksi yang dilakukan.';
        $arr_output['status'] = 500;
        $additional_where=' '; //default telah ngecek data dihapus
        if($this->available_data($id,$additional_where))
        {
            $this->mainModel->setMainTable('site_job');
            $this->mainModel->delete('job_id='.intval($id));
            $arr_output['message'] =  ' Konten telah dihapus.';
            $arr_output['status'] = 200;
        }
        echo json_encode($arr_output);
    }

     /**
    pengecekkan ketersediaan data apakah telah dihapus atau belum
    dan dapat ditambahkan parameter lainnya
    */
    protected function available_data($id,$additional_where='')
    {

        $result=false;
        $scalar=$this->function_lib->get_one('job_id','site_job',
            'job_id='.intval($id).' 
            AND (SELECT COUNT(*) FROM site_job_applicant
                WHERE job_applicant_job_id='.intval($id).') < 1 '.$additional_where);
        if($scalar)
        {
            $result=true;
        }

        return $result;
    }

     /**
     * jquery script
     * Script ditempatkan pada controller agar saat ganti template, fokus hanya pada tampilan.
      * @param string $method
      * @param array $extraVariable
     * **/
    protected function footerScript($method='',$extraVariable=array())
    {

        //set variable
        if(!empty($extraVariable))
        {
                extract($extraVariable);
        }
        
        ob_start();

        ?>
        <link rel="stylesheet" href="<?php echo $this->themeUrl;?>assets/plugins/datepicker/css/datepicker.css">
        <script src="<?php echo $this->themeUrl;?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
       
        <script type="text/javascript">
         $(function(){
             $('.date-picker').datepicker({autoclose:true
                });
        });    

        var current_index=parseInt($("#num_files").val());
        function add_input_file(elem)
        {
            var parent_before=elem.parent().parent();
            var html='<div class="form-group">'
                    +'<label class="col-sm-2 control-label" for="form-field-1">'
                    +'</label>'
                    +'<div class="col-sm-4">'
                    +'<a href="#" onclick="remove_input_file($(this));return false;" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>'
                    +'      <input type="hidden" value="0" name="download_file_id['+current_index+']">'
                    +'      <input style="width:80%;float:right;" class="form-control" type="file" name="download_file_'+current_index+'"/>'
                    +'</div>'
                    +'<div class="col-sm-4">'
                    +'<input class="form-control" placeholder="Label file" type="text" name="download_title['+current_index+']"/>'
                    +'</div>'
                    +'</div>';
            current_index++;        
            parent_before.before(html);        
        }

        function remove_input_file(elem)
        {
            var parent=elem.parent().parent();
            if(confirm('Hapus?'))
            {
                parent.slideUp('medium',function(){
                    parent.remove();
                });
            }
            return false;
        }

        function show_time(elem)
        {
            var current_value=elem.val();
            var div_show_time=$(".div_show_time");
            if(current_value=='Y')
            {
                div_show_time.slideDown('medium');
            }
            else
            {
                div_show_time.slideUp('medium');
            }
        }
        </script>
        <?php
        switch($method)
        {
             case 'index':
            ?>
        <!-- flexigrid starts here -->
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/flexigrid.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexigrid/js/json2.js"></script>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/css/flexigrid.css" />
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/flexigrid/button/style.css" />
                <!-- flexigrid ends here -->
                <script>
                    $("#gridview").flexigrid({
                        dataType: 'json',
                        colModel: [
                            //{ display: '', name: 'delete', width: 60, sortable: false, align: 'center' },
                            { display: 'No', name: 'no', width: 30, sortable: true, align: 'right' },
                            { display: 'Edit', name: 'edit', width: 50, sortable: true, align: 'center' },
                            { display: 'Opsi', name: 'options', width: 120, sortable: true, align: 'center' },
                            { display: 'Judul', name: 'title', width: 160, sortable: true, align: 'center' },
                            { display: 'Isi', name: 'content', width: 360, sortable: true, align: 'center' },
                            { display: 'Meta Deskripsi', name: 'meta_description', width: 200, sortable: true, align: 'center' },
                            { display: 'Meta Tags', name: 'meta_tags', width: 200, sortable: true, align: 'center' },
                            { display: 'Gambar', name: 'photo', width: 80, sortable: true, align: 'center' },
                            { display: 'Diinput Oleh', name: 'input_by', width: 80, sortable: true, align: 'center' },
                    
                        ],
                        buttons: [
                            { display: 'Input Data', name: 'add', bclass: 'add', onpress: add },
                            { separator: true },
                            { display: 'Pilih Semua', name: 'selectall', bclass: 'selectall', onpress: check },
                            { separator: true },
                            { display: 'Batalkan Pilihan', name: 'selectnone', bclass: 'selectnone', onpress: check },
                            { separator: true },
                            { display: 'Hapus Item Terpilih', name: 'delete', bclass: 'delete', onpress: act_delete },
                        ],
                        buttons_right: [
//                            { display: 'Export Excel', name: 'excel', bclass: 'excel', onpress: export_excel },
                        ],
                      
                        sortname: "id",
                        sortorder: "asc",
                        usepager: true,
                        title: ' ',
                        useRp: true,
                        rp: 50,
                        showTableToggleBtn: false,
                        showToggleBtn: true,
                        width: 'auto',
                        height: '300',
                        resizable: false,
                        singleSelect: false
                    });
                    
                    function act_delete(com, grid) {
                        var div_alert=$("div.alert");

                        var grid_id = $(grid).attr('id');
                        grid_id = grid_id.substring(grid_id.lastIndexOf('grid_') + 5);

                        if($('.trSelected', grid).length > 0) {
                            var title = '';
                            if (com == 'delete') {
                                title = 'Hapus';
                            }

                            var conf = confirm(title + ' ' + $('.trSelected', grid).length + ' data?');
                            if(conf == true) {
                                var arr_id = [];
                                var i = 0;
                                $('.trSelected', grid).each(function() {
                                    var id = $(this).attr('data-id');
                                    arr_id.push(id);
                                    i++;
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url().'admin/'.$this->currentModule; ?>/act_delete',
                                    data: com + '=true&item=' + JSON.stringify(arr_id),
                                    dataType: 'json',
                                    success: function(response) {
                                        grid_reload();
                                        div_alert.slideDown('medium');
                                        if(response['status']!=200)
                                        {
                                            div_alert.addClass('alert-danger');
                                            div_alert.removeClass('alert-success');
                                        }
                                        else
                                        {
                                            div_alert.removeClass('alert-danger');
                                            div_alert.addClass('alert-success');
                                        }
                                        div_alert.html(response['message']);
                                    },
                                    error:function(){
                                        alert('an error has occurred, please try again');
                                        grid_reload();    
                                    }
                                });
                            }
                        }
                    }


                    $(document).ready(function() {
                        grid_reload();
                    });

                    function reset_pencarian()
                    {
                        $("input#job_title").val('');
                       $("input#job_content").val('');
                       grid_reload() ;
                    }

                    function grid_reload() {
                        var job_title=$("input#job_title").val();
                        var job_content=$("input#job_content").val();
                        var link_service='?&job_title='+job_title+'&job_content='+job_content;
                        $("#gridview").flexOptions({url:'<?php echo base_url().$this->currentModule; ?>/service_rest/get_data'+link_service}).flexReload();
                    }
                    
                    function delete_transaction(id)
                    { 
                        var div_alert=$("div.alert");
                        if(confirm('Delete?'))
                        {
                            var jqxhr=$.ajax({
                            url:'<?php echo base_url().'admin/'.$this->currentModule?>/delete/'+id,
                            type:'get',
                            dataType:'json',
                            
                        });
                        jqxhr.success(function(response){
                            div_alert.slideDown('medium');
                            if(response['status']!=200)
                            {
                                div_alert.addClass('alert-danger');
                                div_alert.removeClass('alert-success');
                            }
                            else
                            {
                                div_alert.removeClass('alert-danger');
                                div_alert.addClass('alert-success');
                            }
                            div_alert.html(response['message']);
                            grid_reload();
                            return false;

                        });
                        jqxhr.error(function(){
                            alert('an error has occurred, please try again.');
                            grid_reload();
                            return false;
                        });
                        }
                        return false;
                       
                    }

                    function search_by_category($category_id)
                    {
                        $("select#job_category_id").val($category_id);
                        grid_reload();
                        return false;
                    }
                    
                </script>
            <?php
            break;   
        
        }
        
        $footerScript=ob_get_contents();
        ob_end_clean();

        return $footerScript;
    }
}

?>
