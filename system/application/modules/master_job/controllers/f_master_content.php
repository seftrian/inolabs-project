<?php

class f_master_content extends front_controller {

    protected $mainModel;
    public function __construct() {
        parent::__construct();
        $this->load->library('function_lib');
        $this->load->model('master_content_lib');
        $this->load->helper(array('pagination'));

        $this->mainModel=new master_content_lib;

        $this->pathImgArr=$this->mainModel->getImagePath();
    }

    public function search($query='')
    {
        if($this->input->post('query'))
        {
            $filter_query=function_lib::seo_name($this->input->post('query'));
            redirect(base_url().'master_content/'.__FUNCTION__.'/'.$filter_query);
        }

        $decode_query=master_content_lib::decode_seo_name($query);
        $limit=10;
        $total_rows=$this->mainModel->count_news_from_search($decode_query);
        $paginationArr=create_pagination($this->currentModule.'/'.__FUNCTION__.'/'.$query, $total_rows, $limit );
        $this->data['offset']=$paginationArr['limit'][1];    
        $this->data['results']=$this->mainModel->get_news_from_search($decode_query,$limit,$this->data['offset'],$order_by='news_id DESC');
        $this->data['pathImgArr']=$this->mainModel->getImagePath();
        $title='Pencarian';
        $this->data['link_pagination']=$paginationArr['links'];
        $desc=$title;
        $key=$title;
        $this->seo($title,$desc,$key);
        $this->data['category_title']=$decode_query;
        $this->data['limit']=$limit;
        $this->data['total']=$total_rows;
        $this->data['offset']=$this->data['offset'];
        template($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }

    public function archives($category_permalink='')
    {
        if(trim($category_permalink)=='')
        {
            show_error('Halaman tidak ditemukan',404);
        }
        $category_permalink=$this->security->sanitize_filename($category_permalink);
        $limit=15;
        $total_rows=$this->mainModel->count_news_from_category_permalink($category_permalink);
        $paginationArr=create_pagination($this->currentModule.'/'.__FUNCTION__.'/'.$category_permalink, $total_rows, $limit );
        $this->data['offset']=$paginationArr['limit'][1];    
        $this->data['results']=$this->mainModel->get_news_from_category_permalink($category_permalink,$limit,$this->data['offset'],$order_by='news_id DESC');
        $this->data['pathImgArr']=$this->mainModel->getImagePath();
        $title=$this->function_lib->get_one('category_title','site_news_category','category_permalink="'.$category_permalink.'"');
        $this->data['link_pagination']=$paginationArr['links'];
        
        $desc=$title;
        $key=$title;
        $this->seo($title,$desc,$key);
        $this->data['category_title']=$title;
        template($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }

    /**
    detail news
    @param string $news_permalink    
    */
    public function detail($news_permalink='')
    {
        if(trim($news_permalink)=='')
        {
            show_error('Halaman tidak ditemukan',404);
        }
        $news_permalink=$this->security->sanitize_filename($news_permalink);
        $this->data['row_array']=$this->mainModel-> get_news_detail_from_permalink($news_permalink);
        $this->data['pathImgArr']=$this->mainModel->getImagePath();
        $title=isset($this->data['row_array']['news_title'])?$this->data['row_array']['news_title']:'';
        $desc=isset($this->data['row_array']['news_meta_description'])?$this->data['row_array']['news_meta_description']:'';
        $key=isset($this->data['row_array']['news_meta_tags'])?$this->data['row_array']['news_meta_tags']:'';
        $this->seo($title,$desc,$key);

        $news_id=isset($this->data['row_array']['news_id'])?$this->data['row_array']['news_id']:0;
        //counter
        $this->mainModel->count_number_of_read($news_id);
        //kategori permalink
        $uri_2=$this->uri->segment(2);
        $where='category_permalink="'.$uri_2.'"';
        $category_title=$this->function_lib->get_one('category_title','site_news_category',$where);
        $this->data['category_title']=($category_title!='')?$category_title:'n/a';
        $this->data['category_permalink']=$uri_2;

        //files
        $this->mainModel->setMainTable('site_download');
        $where='download_news_id='.intval($news_id);
        $this->data['download_arr']=$this->mainModel->findAll($where,'no limit','no offset','download_id ASC');

        template($this->themeId,  get_class().'/'.__FUNCTION__,$this->data);
    }

    /**
     * 
     * @author Tejo Murti 
     * @date 2 September 2013
     * memaksimalkan seo dari content yang ada
     * @param string $title
     * @param string $desc
     * @param string $key
     */
    public function seo($title,$desc,$key)
    {
        $seoFunc=function_lib::make_seo($title, $desc, $key);
        $this->data['seoTitle']=$seoFunc['title'];
        $this->data['seoDesc']=$seoFunc['desc'];
        $this->data['seoKeywords']=$seoFunc['keywords'];
    }
    
    
}
