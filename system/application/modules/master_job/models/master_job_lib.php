<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author tejomurti
 */
//require_once dirname(__FILE__).'/../components/core_menu_lib.php';

//class admin_menu extends core_menu_lib{
class master_job_lib{
    //put your code here
    
    public $CI;
    protected $postData=array(); //set post data
    protected $mainTable='site_job';
    protected $dir='assets/images/master_job/';
    public function __construct() {

  //      parent::__construct();
     //   $this->CI->load->database();
        $this->CI=&get_instance();
        $this->CI->load->library(array('function_lib','form_validation'));
        if(!session_id())
        {
            session_start();
        }

        $this->getImagePath();
              
    }

    /**
    simpan file
    */
    public function save_file($job_id)
    {
      $this->CI->load->model('master_file/master_file_lib');                      

      $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'admin';

      $download_id_arr=$this->CI->input->post('download_id');
      $download_title_arr=$this->CI->input->post('download_title');
      $download_file_arr=$this->CI->input->post('download_file');
      $job_title=$this->CI->function_lib->get_one('job_title','site_job','job_id='.intval($job_id));
      if(!empty($download_title_arr) OR !empty($download_file_arr))
      {

        foreach($download_title_arr AS $key=>$value)
        {

          $download_id=(isset($download_id_arr[$key]) AND $download_id_arr[$key]>0)?$download_id_arr[$key]:0;
          $download_title=(isset($download_title_arr[$key]) AND trim($download_title_arr[$key])!='')?$download_title_arr[$key]:$job_title.' '.($key+1);

          $permalink=$this->CI->master_file_lib->generate_permalink($download_title,$download_id);
          $column=array(
            'download_title'=>$download_title,
            'download_foreign_id'=>$job_id,
            'download_permalink'=>$permalink,
            'download_input_by'=>$admin_username,
          );
          
          if($download_id==0)
          {
            $this->CI->db->insert('site_download',$column);
            $download_id=$this->CI->function_lib->insert_id();
          }
          else
          {

            $this->CI->db->update('site_download',$column,array('download_id'=>$download_id));
          }
          $filesName='download_file';
          $response=$this->handleFileUpload($download_id,$filesName,$key);
          if($response['status']!=200)
          {
            return array(
              'status'=>$response['status'],
              'message'=>$response['message'],
            );
          }
        }
      }
    }

     /**
     * handle upload
     */
    public function handleFileUpload($id,$filesName,$index)
    {
        $status=200;
        $message='File berhasil disimpan';

        $fileUpload=(isset($_FILES[$filesName.'_'.$index]['name']) AND trim($_FILES[$filesName.'_'.$index]['name'])!='')?$_FILES[$filesName.'_'.$index]['name']:array();
      
        if(!empty($fileUpload))
        {

            //$results=$this->doFileUpload($id,$filesName,$index);
            $results=$this->doFileUpload2($id,$filesName,$index);
           
            $status=$results['status'];
            $message=$results['message'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }

    public function doFileUpload2($id,$field_name,$index)
    {
        $ext=strtolower(pathinfo($_FILES[$field_name.'_'.$index]["name"], PATHINFO_EXTENSION));
        $pathImage='assets/images/master_file/';

        $pathUpload=FCPATH.$pathImage;
        $allowed_types=array(1=>'gif','jpg','jpeg','png','pdf','doc','xls','xlsx','ppt');

        if(array_search($ext,$allowed_types,true)<1)
        {

            $status=500;
            $message="Error: File tidak diizinkan.";
        }
        else
        {
            $status=200;
            $message="OK";
           
            $filename=$_FILES[$field_name.'_'.$index]["name"];
            $newFileName=strtotime(date('Y-m-d H:i:s')).'.'.$ext;

            if(file_exists($pathUpload.'/'.$newFileName) AND $filename!='')
            {
                unlink($pathUpload.'/'.$newFileName);
            }
            if(move_uploaded_file($_FILES[$field_name.'_'.$index]["tmp_name"],$pathUpload.'/'.$newFileName) )
            {
                
                $where=array(
                'download_id'=>$id,
                );
                $column=array(
                    'download_file'=>$pathImage.$newFileName,
                );
                
                $this->setMainTable('site_download');
                $this->CI->db->update($this->mainTable,$column,$where);
                $status=200;
                $message='File berhasil disimpan';
            }
            else
            {
                $status=500;
                $message='Gagal upload file';
            }
        }

        return array(
          'status'=>$status,
          'message'=>$message,
        );
    }

    /**
     * library upload
     * @param string $field_name
     */
    public function doFileUpload($id,$field_name,$index)
    {
       
        $config=array();
        $pathImage='assets/images/master_file/';
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|doc|xls|xlsx|ppt';
        $config['max_size'] = '3200'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name.'_'.$index);
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
             $where=array(
                'download_id'=>$id,
            );
            $column=array(
                'download_file'=>$pathImage.$dataImage['file_name'],
            );
            
            $this->setMainTable('site_download');
            $this->CI->db->update($this->mainTable,$column,$where);
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }

    /**
    dapatkan jumlah dibaca
    @param int $job_id
    */
    public static function get_number_of_read($job_id)
    {
      $lib=new master_content_lib;
      $where='job_id='.intval($job_id);
      $number_of_read=$lib->CI->function_lib->get_one('job_number_of_read','site_job',$where);
      return number_format(intval($number_of_read),0,'','.');
    }

    /**
    fungsi untuk menghitung jumlah pembaca
    @param int $job_id
    limit time 30 menit
    */
    public function count_number_of_read($job_id)
    {
        $job_id_original=$job_id;
        //dapatkan ip address
        $client_ip=function_lib::get_client_ip();
        $client_ip=($client_ip=='::1')?'127.0.0.1':$client_ip;
        //cek apakah ip ini membaca berita yg sama selama 30 menit
        $thirty_minute_ago=date('Y-m-d H:i:s',mktime(date('H'),(date('i')-30),date('s'),date('m'),date('d'),date('Y')));
      
        $where="job_counter_job_id=".intval($job_id).'
                AND job_counter_ip_address=INET_ATON(\''.$client_ip.'\')
                AND job_counter_timestamp>"'.$thirty_minute_ago.'"';
               
        $job_id=$this->CI->function_lib->get_one('job_counter_job_id','site_job_counter',$where);
     
        //jika data tidak tersedia maka insertkan ke database
        if(!$job_id AND $job_id_original>0)
        {
           
          $sql='INSERT INTO site_job_counter 
          (job_counter_job_id,job_counter_ip_address)
          VALUES ('.intval($job_id_original).',INET_ATON("'.$client_ip.'"))';
          $this->CI->db->query($sql);

          //update nilai + 1 ke site job
          $sql="UPDATE site_job SET job_number_of_read=job_number_of_read+1
          WHERE job_id=".intval($job_id_original);
          $this->CI->db->query($sql);
        }        


    }

    public static function decode_seo_name($query)
    {
      $value=str_replace('_', ' ', $query);
      return $value;
    }

    /**
    pencarian
    @param string $query
    */
    public function get_job_from_search($query='',$limit=10,$offset=0,$order_by='job_id DESC')
    {
        $filter_value=$this->CI->security->sanitize_filename($query);
        $where='job_title LIKE "%'.$filter_value.'%" OR job_content LIKE "%'.$filter_value.'%"';
        $sql='SELECT * FROM site_job_relation snr
              INNER JOIN site_job sn ON sn.job_id=snr.job_id
              WHERE '.$where.'
              ORDER BY sn.job_id DESC
              LIMIT '.$offset.', '.$limit.'
              ';
        $exec=$this->CI->db->query($sql);      
        $results=$exec->result_array();
        return $results;      
    }


     /**
    dapatkan total pencarian
    @param string $job_permalink
    */
    public function count_job_from_search($query='')
    {
        $filter_value=$this->CI->security->sanitize_filename($query);
        $where='job_title LIKE "%'.$filter_value.'%" OR job_content LIKE "%'.$filter_value.'%"';
        $sql='SELECT COUNT(snr.job_id) AS num FROM site_job_relation snr
              INNER JOIN site_job sn ON sn.job_id=snr.job_id
              WHERE '.$where
              ;
        $exec=$this->CI->db->query($sql);      
        $results=$exec->row_array();
        $value=!empty($results)?$results['num']:0;
        return $value;      
    }

     /**
    link detail
    @param string $category_permalink
    @param string $job_permalink
    */
    public static function link_archives($category_permalink)
    {
      return base_url().'master_content/archives/'.$category_permalink;
    }


    /**
    link detail
    @param string $category_permalink
    @param string $job_permalink
    */
    public static function link_detail($job_id, $job_permalink)
    {
      return base_url().'master_job/f_master_job/detail/'.$job_permalink;
    }

    /**
    dapatkan data detail job berdasarkan permalink
    @param string $job_permalink
    */
    public function get_job_from_category_permalink($category_permalink='',$limit=10,$offset=0,$order_by='job_id DESC')
    {
        $where='job_category_id=(SELECT category_id FROM site_job_category WHERE category_permalink="'.$category_permalink.'")';
        $sql='SELECT * FROM site_job_relation snr
              INNER JOIN site_job sn ON sn.job_id=snr.job_id
              WHERE '.$where.'
              ORDER BY sn.job_id DESC
              LIMIT '.$offset.', '.$limit.'
              ';
        $exec=$this->CI->db->query($sql);      
        $results=$exec->result_array();
        return $results;      
    }

     /**
    dapatkan data detail job berdasarkan permalink
    @param string $job_permalink
    */
    public function count_job_from_category_permalink($category_permalink='')
    {
        $where='job_category_id=(SELECT category_id FROM site_job_category WHERE category_permalink="'.$category_permalink.'")';
        $sql='SELECT COUNT(snr.job_id) AS num FROM site_job_relation snr
              INNER JOIN site_job sn ON sn.job_id=snr.job_id
              WHERE '.$where
              ;
        $exec=$this->CI->db->query($sql);      
        $results=$exec->row_array();
        $value=!empty($results)?$results['num']:0;
        return $value;      
    }

    /**
    dapatkan data detail job berdasarkan permalink
    @param string $job_permalink
    */
    public function get_job_detail_from_permalink($job_permalink='')
    {
      $where='job_permalink="'.$this->CI->security->sanitize_filename($job_permalink).'"';
      $sql='SELECT site_job.*
            FROM site_job
            WHERE '.$where;
      $exec=$this->CI->db->query($sql);
      $row=$exec->row_array();

      return $row;      
    }

    public function get_job_id_from_permalink($job_permalink='')
    {
      $where='job_permalink="'.$this->CI->security->sanitize_filename($job_permalink).'"';
      $job_id=$this->CI->function_lib->get_one('job_id','site_job',$where);

      return $job_id;   
    }

    /**
    dapatkan jumlah artikel yang telah ada dikategori tersebut
    @param int $job_category_id
    */
    public static function count_article_on_category($category_id)
    {
      $lib=new master_content_lib;
      $where='job_category_id='.intval($category_id);
      $sql='SELECT COUNT(snr.job_id) AS num FROM site_job_relation snr
            INNER JOIN site_job sn ON sn.job_id=snr.job_id
            WHERE '.$where;
      $exec=$lib->CI->db->query($sql);
      $row=$exec->row_array();      
      $value=!empty($row)?$row['num']:0;
      return $value;
    }


    /**
    fungsi untuk mendapatkan list category
    */
    public function get_list_category($job_id)
    {
      $where='snr.job_id='.intval($job_id);
      $sql='SELECT snc.*
            FROM site_job_category snc
            INNER JOIN site_job_relation snr ON snc.category_id=snr.job_category_id
            WHERE '.$where;
      $exec=$this->CI->db->query($sql);
      $results=$exec->result_array();
      return $results;      
    }

    /**
    generate category from job relation 
    @param int $job_id
    */
    public function generate_list_category($job_id)
    {
      $results=$this->get_list_category($job_id);
      $str='<a href="'.base_url().'admin/master_content/update/'.$job_id.'"><i class="fa fa-pencil"></i> Tambahkan ke kategori?</a>';
      if(!empty($results))
      {
        $str='';
        $num=1;
        foreach($results AS $rowArr)
        {
          foreach($rowArr AS $variable=>$value)
          {
            ${$variable}=$value;
          }

          $str.=($num>1)?', <a href="#" onclick="search_by_category('.$category_id.');">'.$category_title.'</a>':'<a href="#" onclick="search_by_category('.$category_id.');">'.$category_title.'</a>';
          $num++;
        }
      }

      return $str;
    }

    /**
    save category and job into relation table
    @param int $job_id
    */
    public function store_category_to_job($job_id=0)
    {
      //hapus data lama
      $this->setMainTable('site_job_relation');
      $where='job_id='.intval($job_id);      
      $this->delete($where);

      //insertkan data job and category to relation
      $job_category_id_arr=$this->CI->input->post('job_category_id');
      if(!empty($job_category_id_arr))
      {
        foreach($job_category_id_arr AS $job_category_id)
        {
          $column=array(
            'job_category_id'=>$job_category_id,
            'job_id'=>$job_id,
          );
          $this->CI->db->insert('site_job_relation',$column);
        }
      }
    }


    /**
    dapatkan list category
    */
    public function list_job_category()
    {
      $this->setMainTable('site_job_category');
      $results=$this->findAll('1','no limit','no offset','category_id ASC');
      return $results;
    }

     /**
    dapatkan list category
    */
    public function list_files($job_id)
    {
      $this->setMainTable('site_download');
      $results=$this->findAll('download_foreign_id='.intval($job_id),'no limit','no offset','download_id ASC');
      return $results;
    }

    /**
    generate permalink
    @param string $title
    */
    public function generate_permalink($title,$id=0)
    {
        $datetime=date('Y-m-d H:i:s');
        $permalink=$this->CI->function_lib->seo_name($title);
        $where='job_permalink="'.$permalink.'"';
        if($id!=0)
        {
            $where.=' AND job_id!='.intval($id);
        }
        $is_exist=$this->CI->function_lib->get_one('job_id','site_job',$where);
        if($is_exist)
        {
            $permalink=$this->CI->function_lib->seo_name($title).strtotime($datetime);
        }
        return $permalink;
    }
    
      /**
     * dapatkan path image
     */
    public function getImagePath()
    {
        $path=base_url().$this->dir;
        $pathLocation=FCPATH.$this->dir;

        if(!file_exists($pathLocation) AND trim($pathLocation)!='')
        {
            $explode_arr=explode('/', $pathLocation);
            $last_dir=count($explode_arr)-2;
            //src    
            if(!empty($explode_arr))
            {
                $src='';
                $max_src=count($explode_arr)-2;
                for($i=0;$i<count($explode_arr);$i++)
                {
                    if($i<$max_src)
                    {

                        $src.=$explode_arr[$i].'/';
                    }
                }
            }

            $dir=$explode_arr[$last_dir];
            mkdir($src.$dir);
            chmod($src.$dir,0777);
        }

        return array(
            'pathUrl'=>$path,
            'pathLocation'=>$pathLocation,
        );
    }
    
    
    public function getMainTable()
    {
        return $this->mainTable;
    }

    /**
     * set main table
     */
    public function setMainTable($table)
    {
        $this->mainTable=$table;
    }
    
    /**
     * handle upload
     */
    public function handleUpload($id,$filesName)
    {
        $status=200;
        $message='File berhasil disimpan';
            
        $fileUpload=(isset($_FILES[$filesName]['name']) AND trim($_FILES[$filesName]['name'])!='')?$_FILES[$filesName]:array();
      
        if(!empty($fileUpload))
        {
            $results=$this->doUpload($id,$filesName);
            $status=$results['status'];
            $message=$results['message'];
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    /**
     * library upload
     * @param string $field_name
     */
    public function doUpload($id,$field_name)
    {
        $config=array();
        $pathImage=$this->dir;
        $config['upload_path'] = FCPATH.'/'.$pathImage;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '500'; //KB
        
        
        $this->CI->load->library('upload', $config);
        
        
        // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
        $this->CI->upload->initialize($config);
        
        $isComplete=$this->CI->upload->do_upload($field_name);
        if($isComplete)
        {
            $dataImage=$this->CI->upload->data();
            //simpan
             $where=array(
                'job_id'=>$id,
            );
            $column=array(
                'job_photo'=>$pathImage.$dataImage['file_name'],
            );
            $this->setMainTable('site_job');
            $this->CI->db->update($this->mainTable,$column,$where);
            $status=200;
            $message='File berhasil disimpan';
            
        }
        else
        {
            $status=500;
            $message=$this->CI->upload->display_errors();;
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * dapatkan satu baris data
     * @param string $where
     * 
     * @return array
     */
    public function find($where=1)
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        return $exec->row_array();
    }
    /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='category_id ASC')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }

     /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @param string $orderBy + SORT 
     * @param string $select * 
     * @param string $join  
     * @param string $group_by 
     * @param string $having 

     * @return array
     */
    public function findAllCustom($where=1,$limit=10,$offset=0,$orderBy='category_id ASC',$select='*',$join='',$group_by='',$having='')
    {

        $sql='SELECT '.$select.' FROM '.$this->mainTable;
        if(trim($join)!='')
        {
          $sql.=$join;
        }   
        if(trim($where)!='')
        {
          $sql.='WHERE '.$where;
        }
        
        if(trim($having)!='')
        {
          $sql.='HAVING '.$having;
        }      
        if(trim($group_by)!='')
        {
          $sql.='GROUP BY '.$group_by;
        }      
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }
  
    /**
     * dapatkan total data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function countAll($where=1)
    {
        $sql='SELECT COUNT(*) AS jml FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }

    /**
     * dapatkan total data
     * @param string $where
      * @param string $select * 
     * @param string $join  
     * @param string $group_by 
     * @param string $having 
     * @return array
     */
    public function countAllCustom($where=1,$select='*',$join='',$group_by='',$having='')
    {
        $sql='SELECT COUNT('.$select.') AS jml FROM '.$this->mainTable.'
              
              ';
        if(trim($join)!='')
        {
          $sql.=$join;
        }   
        if(trim($where)!='')
        {
          $sql.='WHERE '.$where;
        }

        if(trim($having)!='')
        {
          $sql.='HAVING '.$having;
        }      
        if(trim($group_by)!='')
        {
          $sql.='GROUP BY '.$group_by;
        } 
        $exec=$this->CI->db->query($sql);
        $row=$exec->row_array();
        return !empty($row)?$row['jml']:0;
    }
    
    /**
     * hapus data pada 1 table
     * @param string $where
     * @return array
     */
    public function delete($where)
    {
        $status=200;
        $message='';
                
        if(trim($where)!='')
        {
            $sql='DELETE FROM '.$this->mainTable.'
                WHERE '.$where;
            
            try
            {
                $this->CI->db->query($sql);
                $status=200;
                $message='Data berhasil dihapus';
            }
            catch(Exception $e)
            {
                $status=500;
                $message='Terjadi kesalahan, data tidak dapat dihapus.';
            }
        }
        
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
        
    }
    
    /**
     * untuk validasi form 
     * 
     * @param int $id default 0
     * nilai id ada jika aksi update
     * * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function formValidation($id=0)
    {
         $status=500;
         $message='';
            $mainConfig = array(
                array(
                        'field'   => 'job_title',
                        'label'   => 'Judul',
                        'rules'   => 'trim|required'
                    ),
                array(
                        'field'   => 'job_content',
                        'label'   => 'Isi',
                        'rules'   => 'trim|required'
                    ),
                
            );
         
          $config=$mainConfig;
          /**
           * jika lokasi admin, maka module controller dan method wajib diisi
           * main config dan additional config akan digabung
           */
        
          
          $this->CI->form_validation->set_rules($config);
          if ($this->CI->form_validation->run() == TRUE)
          {
              
              //proses melewati validasi form
              $status=200;
              $message='Form ready to save';
              
          }
          
          return array(
              'status'=>$status,
              'message'=>$message,
          );
         
    }
    
    /**
     * proses penyimpanan data
     * @param int $groupId default=0 untuk create
     * @return array(
     *          'status'=>200{OK}|else false
     *          'message'=>'pesan',
     *      );
     */
    public function save($where='')
    {
        $status=500;
        $message='Error';
        if(trim($where)!='')
        {
            $sql='UPDATE '.$this->mainTable.' SET ';
            if(!empty($this->postData))
            {
                $no=1;
                foreach($this->postData AS $column=>$value)
                {
                    $separated=($no>=1 AND $no<count($this->postData))?',':'';
                    $sql.=' '.$column.'="'.$value.'"'.$separated;
                    $no++;
                }
            }
            $sql.=' WHERE '.$where;
            $this->CI->db->query($sql);
            $status=200;
            $message='OK';
        }
        else
        {
            $this->CI->db->insert($this->mainTable,$this->postData);
            $status=200;
            $message='OK';
        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
     
    /**
     * 
     * set post data
     * @parram $dataArr array(
     *          'member'=>array() array table member
     *          'password'=>array() array table password
     * );
     */
    public function setPostData($dataArr)
    {
        $this->postData=$dataArr;
    }
    
    
}

?>
