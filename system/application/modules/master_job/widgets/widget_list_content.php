<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list_menu
 *
 * @author tejomurti
 */
class widget_list_content extends widget_lib{
    //put your code here
    public function run($module,$class_widget,$category_permalink,$limit=2)
    {

        $this->load->model('master_content/master_content_lib');
        $lib=new master_content_lib;

        $data=array();
        $where='news_category_id=(SELECT category_id FROM site_news_category WHERE category_permalink="'.$category_permalink.'")';
        $sql='SELECT * FROM site_news_relation snr
              INNER JOIN site_news sn ON sn.news_id=snr.news_id
              WHERE '.$where.'
              ORDER BY sn.news_id DESC
              LIMIT '.$limit.'
              ';
        $exec=$this->db->query($sql);      
        $data['results']=$exec->result_array();
        $data['pathImgArr']=$lib->getImagePath();
        $data['category_permalink']=$category_permalink;

        $this->render(get_class(),$data);
    }
}

?>
