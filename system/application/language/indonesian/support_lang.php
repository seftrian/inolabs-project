<?php
/* support_lang */
$lang['support.msgInfo1'] = "Data belum dipilih";
$lang['support.msgInfo2'] = "Nama Support";
$lang['support.msgInfo3'] = "ID Support";
$lang['support.msgInfo4'] = "Data support berhasil ditambah";
$lang['support.msgInfo5'] = "Data gagal ditambahkan";
$lang['support.msgInfo6'] = "Data support berhasil diubah";
$lang['support.msgInfo7'] = "Data gagal diubah";
$lang['support.msgInfo8'] = "Daftar Online Support";
$lang['support.msgInfo9'] = "Tambah";
$lang['support.msgInfo10'] = "Publish";
$lang['support.msgInfo11'] = "UnPublish";
$lang['support.msgInfo12'] = "Delete";
$lang['support.msgInfo13'] = "Nomor Kontak";
$lang['support.msgInfo14'] = "Ubah";
$lang['support.msgInfo15'] = "Ubah Data Online Support";
$lang['support.msgInfo16'] = "Nama";
$lang['support.msgInfo17'] = "Simpan";
$lang['support.msgInfo18'] = "Tambah Data Online Support";

/* End of file support_lang.php */
/* Location: ./system/language/indonesian/support_lang.php */