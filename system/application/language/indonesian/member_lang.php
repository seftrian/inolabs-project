<?php
/* member_lang */
$lang['member.msginfo1'] = "Nama Lengkap";
$lang['member.msginfo2'] = "Alamat";
$lang['member.msginfo3'] = "Tempat Lahir";
$lang['member.msginfo4'] = "Tanggal Lahir";
$lang['member.msginfo5'] = "Kolom Jenis Kelamin";
$lang['member.msginfo6'] = "Identitas";
$lang['member.msginfo7'] = "data berhasil di ubah";
$lang['member.msginfo8'] = "data gagal diubah";
$lang['member.msginfo9'] = "Ubah Password Member";
$lang['member.msginfo10'] = "DATA KEANGGOTAAN";
$lang['member.msginfo11'] = "ID Member";
$lang['member.msginfo13'] = "Nomor Serial";
$lang['member.msginfo14'] = "UBAH PASSWORD";
$lang['member.msginfo15'] = "Password Baru";
$lang['member.msginfo16'] = "Simpan";
$lang['member.msginfo17'] = "Daftar Member";
$lang['member.msginfo18'] = "Pencarian";
$lang['member.msginfo19'] = "Kata Kunci";
$lang['member.msginfo21'] = "Kota";
$lang['member.msginfo22'] = "Provinsi";
$lang['member.msginfo23'] = "Nomor Handphone";
$lang['member.msginfo24'] = "Blocked Member";
$lang['member.msginfo25'] = "Cari";
$lang['member.msginfo26'] = "Sponsor";
$lang['member.msginfo27'] = "Upline";
$lang['member.msginfo28'] = "Serial";
$lang['member.msginfo29'] = "PIN";
$lang['member.msginfo30'] = "Block";
$lang['member.msginfo31'] = "Aktif";
$lang['member.msginfo32'] = "Ubah Password";
$lang['member.msginfo33'] = "Ubah Data";
$lang['member.msginfo34'] = "Login";
$lang['member.msginfo35'] = "Ubah Data Member";
//$lang['member.msginfo35'] = "DATA KEANGGOTAAN";
$lang['member.msginfo36'] = "E-mail";
$lang['member.msginfo37'] = "Kode Pos";
$lang['member.msginfo38'] = "Jenis Kelamin";
$lang['member.msginfo39'] = "No. Identitas";
$lang['member.msginfo40'] = "KTP";
$lang['member.msginfo41'] = "SIM";
$lang['member.msginfo42'] = "Passport";
$lang['member.msginfo43'] = "No. NPWP";
$lang['member.msginfo44'] = "No. Telepon";
$lang['member.msginfo45'] = "No. Handphone";
$lang['member.msginfo46'] = "DATA PEWARIS";
$lang['member.msginfo47'] = "Hubungan";
$lang['member.msginfo48'] = "DATA BANK";
$lang['member.msginfo49'] = "Nama Bank";
$lang['member.msginfo50'] = "Kota Bank";
$lang['member.msginfo51'] = "Cabang";
$lang['member.msginfo52'] = "Nama Nasabah";
$lang['member.msginfo53'] = "Nomor Rekening";
$lang['member.msginfo54'] = "Laki-laki";
$lang['member.msginfo55'] = "Perempuan";
$lang['member.msginfo56'] = "Nama Pewaris";
$lang['member.msginfo57'] = "Hubungan Waris";
$lang['member.msginfo58'] = "No. Telepon Pewaris";

/* End of file member_lang.php */
/* Location: ./system/language/indonesian/member_lang.php */