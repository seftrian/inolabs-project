<?php
/* serial_lang */
$lang['serial.msgInfo1'] = " data berhasil diubah, ";
$lang['serial.msgInfo2'] = " data gagal diubah.";
$lang['serial.msgInfo3'] = "data belum dipilih";
$lang['serial.msgInfo4'] = "Serial Kartu berhasil di Generate. Silakan";
$lang['serial.msgInfo5'] = "Download File Excel</a> hasil Generate Serial Kartu.";
$lang['serial.msgInfo6'] = "member";
$lang['serial.msgInfo7'] = "tidak ada";
$lang['serial.msgInfo8'] = "Kartu";
$lang['serial.msgInfo9'] = "tidak ada dalam database kami";
$lang['serial.msgInfo10'] = "sudah dipakai <br />Kartu dipakai Oleh";
$lang['serial.msgInfo11'] = "Belum diaktifkan <br />Kartu dibeli Oleh";
$lang['serial.msgInfo12'] = "aktif tetapi belum digunakan";
$lang['serial.msgInfo13'] = "Cek Kartu";
$lang['serial.msgInfo14'] = "Cek Kartu Aktivasi";
$lang['serial.msgInfo15'] = "Nomor Serial";
$lang['serial.msgInfo16'] = "Pencarian";
$lang['serial.msgInfo17'] = "pilih salah satu mode pencarian";
$lang['serial.msgInfo18'] = "Serial";
$lang['serial.msgInfo19'] = "Tanggal";
$lang['serial.msgInfo20'] = "Kartu Dibuat";
$lang['serial.msgInfo21'] = "Kartu Terbeli";
$lang['serial.msgInfo22'] = "Kartu Aktif";
$lang['serial.msgInfo23'] = "Kartu Digunakan";
$lang['serial.msgInfo24'] = "Kata kunci";
$lang['serial.msgInfo25'] = "ID Member";
$lang['serial.msgInfo26'] = "Cari";
$lang['serial.msgInfo27'] = "Aktifkan";
$lang['serial.msgInfo28'] = "Batalkan Kartu Aktif";
$lang['serial.msgInfo29'] = "Batalkan Pembelian Kartu";
$lang['serial.msgInfo30'] = "Aktif";
$lang['serial.msgInfo31'] = "Digunakan";
$lang['serial.msgInfo32'] = "Tanggal Pembuatan";
$lang['serial.msgInfo33'] = "Tanggal Diaktifkan";
$lang['serial.msgInfo34'] = "Pengguna";
$lang['serial.msgInfo35'] = "Tanggal Digunakan";
$lang['serial.msgInfo36'] = "Pembeli";
$lang['serial.msgInfo37'] = "Tanggal Pembelian";
$lang['serial.msgInfo38'] = "DATA KOSONG";
$lang['serial.msgInfo39'] = "Generate Serial Kartu";
$lang['serial.msgInfo40'] = "Jumlah Serial Kartu";
$lang['serial.msgInfo41'] = "Pembelian Kartu";
$lang['serial.msgInfo42'] = "s/d";
$lang['serial.msgInfo43'] = "ID Member Pembeli";
$lang['serial.msgInfo44'] = "Tampilkan";
$lang['serial.msgInfo45'] = "Proses";
$lang['serial.msgInfo46'] = "Detail Pembeli";
$lang['serial.msgInfo47'] = "Nama";

/* End of file serial_lang.php */
/* Location: ./system/language/indonesian/serial_lang.php */