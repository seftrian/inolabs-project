<?php
/* bank_lang */
$lang['bank.msgInfo1'] = "Data telah diubah";
$lang['bank.msgInfo2'] = "Data Gagal Diubah";
$lang['bank.msgInfo3'] = "Data belum dipilih";
$lang['bank.msgInfo4'] = "Data berhasil ditambah";
$lang['bank.msgInfo5'] = "Data gagal ditambah";
$lang['bank.msgInfo6'] = "Nama Bank";
$lang['bank.msgInfo7'] = "Nama Bank sudah ada";
$lang['bank.msgInfo8'] = "Daftar Bank";
$lang['bank.msgInfo9'] = "Tambah";
$lang['bank.msgInfo10'] = "Publish";
$lang['bank.msgInfo11'] = "UnPublish";
$lang['bank.msgInfo12'] = "Ubah";
$lang['bank.msgInfo13'] = "Ubah Data Bank";
$lang['bank.msgInfo14'] = "Nama";
$lang['bank.msgInfo15'] = "Simpan";
$lang['bank.msgInfo16'] = "Tambah Data Bank";

/* End of file bank_lang.php */
/* Location: ./system/language/indonesian/bank_lang.php */