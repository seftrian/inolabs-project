<?php
/* menu_lang */
$lang['menu.msgInfo1'] = "Data belum dipilih";
$lang['menu.msgInfo2'] = "Data berhasil ditambah";
$lang['menu.msgInfo3'] = "Data gagal ditambah";
$lang['menu.msgInfo4'] = "Tambah";
$lang['menu.msgInfo5'] = "Publish";
$lang['menu.msgInfo6'] = "UnPublish";
$lang['menu.msgInfo7'] = "Judul";
$lang['menu.msgInfo8'] = "Ubah";
$lang['menu.msgInfo9'] = "Edit Menu";
$lang['menu.msgInfo10'] = "Judul";
$lang['menu.msgInfo11'] = "Deskripsi";
$lang['menu.msgInfo12'] = "Edit";
$lang['menu.msgInfo13'] = "Tambah Menu";
$lang['menu.msgInfo14'] = "Link";
$lang['menu.msgInfo15'] = "Simpan";

/* End of file menu_lang.php */
/* Location: ./system/language/indonesian/menu_lang.php */