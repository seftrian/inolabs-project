<?php
/* reward_lang */
$lang['reward.msgInfo1'] = "Data belum dipilih";
$lang['reward.msgInfo2'] = "Syarat Kiri";
$lang['reward.msgInfo3'] = "Syarat Kanan";
$lang['reward.msgInfo4'] = "Bonus Reward";
$lang['reward.msgInfo5'] = "Data berhasil ditambahkan";
$lang['reward.msgInfo6'] = "Data gagal ditambahkan";
$lang['reward.msgInfo7'] = "Data berhasil diganti";
$lang['reward.msgInfo8'] = "Data gagal diganti";
$lang['reward.msgInfo9'] = "Jalankan Tombol Dibawah ini untuk merekap daftar member yang mendapatkan reward";
$lang['reward.msgInfo10'] = "Daftar Member yang Mendapatkan Bonus Reward";
$lang['reward.msgInfo11'] = "Penyaringan";
$lang['reward.msgInfo12'] = "(pilih salah satu mode penyaringan)";
$lang['reward.msgInfo13'] = "Tampilkan";
$lang['reward.msgInfo14'] = "Semua Bonus Reward";
$lang['reward.msgInfo15'] = "Belum Diproses";
$lang['reward.msgInfo16'] = "Telah Diproses";
$lang['reward.msgInfo17'] = "Kata Kunci";
$lang['reward.msgInfo18'] = "ID Member";
$lang['reward.msgInfo19'] = "Saring";
$lang['reward.msgInfo20'] = "Proses";
$lang['reward.msgInfo21'] = "Nama";
$lang['reward.msgInfo22'] = "Tanggal Pengambilan";
$lang['reward.msgInfo23'] = "Tanggal Proses";
$lang['reward.msgInfo24'] = "Diproses";
$lang['reward.msgInfo25'] = "Daftar Bonus Reward";
$lang['reward.msgInfo26'] = "Tambah";
$lang['reward.msgInfo27'] = "Aktifkan";
$lang['reward.msgInfo28'] = "Non Aktifkan";
$lang['reward.msgInfo29'] = "Kiri";
$lang['reward.msgInfo30'] = "Kanan";
$lang['reward.msgInfo31'] = "Aktif";
$lang['reward.msgInfo32'] = "Ubah";
$lang['reward.msgInfo33'] = "Ubah Bonus Reward";
$lang['reward.msgInfo34'] = "Simpan";
$lang['reward.msgInfo35'] = "Tambah Bonus Reward";

/* End of file reward_lang.php */
/* Location: ./system/language/indonesian/reward_lang.php */