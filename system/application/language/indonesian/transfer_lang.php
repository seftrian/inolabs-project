<?php
/* transfer_lang */
$lang['transfer.msgInfo1'] = "Potongan Pajak lebih dari 100%";
$lang['transfer.msgInfo2'] = "Potongan Index lebih dari 100%";
$lang['transfer.msgInfo3'] = "Pembayaran Komisi Harian";
$lang['transfer.msgInfo4'] = "Nilai Index";
$lang['transfer.msgInfo5'] = "Komisi-Harian-";
$lang['transfer.msgInfo6'] = "No.";
$lang['transfer.msgInfo7'] = "ID Member";
$lang['transfer.msgInfo8'] = "Nama Member";
$lang['transfer.msgInfo9'] = "Nama Bank";
$lang['transfer.msgInfo10'] = "Nama Nasabah";
$lang['transfer.msgInfo11'] = "Nomor Rekening";
$lang['transfer.msgInfo12'] = "Nomor Handphone";
$lang['transfer.msgInfo13'] = "Total Komisi";
$lang['transfer.msgInfo14'] = "Total Ditransfer";
$lang['transfer.msgInfo15'] = "TOTAL";
$lang['transfer.msgInfo16'] = "Komisi Member berhasil di rekap. Silakan ";
$lang['transfer.msgInfo17'] = "Download File Excel";
$lang['transfer.msgInfo18'] = " untuk mengetahui hasil rekap komisi.";
$lang['transfer.msgInfo19'] = "data belum dipilih";
$lang['transfer.msgInfo20'] = "Daftar Transfer";
$lang['transfer.msgInfo21'] = "Daftar Member yang ditransfer";
$lang['transfer.msgInfo22'] = "MID";
$lang['transfer.msgInfo23'] = "Nama";
$lang['transfer.msgInfo24'] = "Potongan Index + Administrasi";
$lang['transfer.msgInfo25'] = "Total Transfer";
$lang['transfer.msgInfo26'] = "Transfer";
$lang['transfer.msgInfo27'] = "Penyaringan ";
$lang['transfer.msgInfo28'] = "(pilih salah satu mode penyaringan)";
$lang['transfer.msgInfo29'] = "Kata Kunci";
$lang['transfer.msgInfo30'] = "Saring";
$lang['transfer.msgInfo31'] = "Konfigurasi MLM";
$lang['transfer.msgInfo32'] = "Nama Potongan";
$lang['transfer.msgInfo33'] = "Setting";
$lang['transfer.msgInfo34'] = "Ubah";
$lang['transfer.msgInfo35'] = "Proses";
$lang['transfer.msgInfo36'] = "Indeks Komisi";
$lang['transfer.msgInfo37'] = "Tanggal";
$lang['transfer.msgInfo38'] = "Potongan";
$lang['transfer.msgInfo39'] = "Pembayaran Komisi Bulanan";
$lang['transfer.msgInfo40'] = "Komisi-Bulanan-";
/* End of file transfer_lang.php */
/* Location: ./system/language/indonesian/transfer_lang.php */