<?php
/* theme_lang */
$lang['theme.msgInfo1'] = "Selamat Datang";
$lang['theme.msgInfo2'] = "Menu Utama";
$lang['theme.msgInfo3'] = "Tanda Terima";
$lang['theme.msgInfo4'] = "Profil";
$lang['theme.msgInfo5'] = "Ubah Password";
$lang['theme.msgInfo6'] = "Testimonial";
$lang['theme.msgInfo7'] = "JARINGAN";
$lang['theme.msgInfo8'] = "Geneology Jaringan";
$lang['theme.msgInfo9'] = "Sponsorisasi";
$lang['theme.msgInfo10'] = "Laporan Jaringan";
$lang['theme.msgInfo11'] = "Transaksi Pulsa";
$lang['theme.msgInfo12'] = "Pesan";
$lang['theme.msgInfo13'] = "Kirim Pesan";
$lang['theme.msgInfo14'] = "KOMISI";
$lang['theme.msgInfo15'] = "Total Bonus";
$lang['theme.msgInfo16'] = "Statement Komisi";
$lang['theme.msgInfo17'] = "Penghargaan";
$lang['theme.msgInfo18'] = "Sejarah Penghargaan";
$lang['theme.msgInfo19'] = "Buat Admin";
$lang['theme.msgInfo20'] = "Rencana B";
$lang['theme.msgInfo21'] = "Histori Bonus";

/* End of file theme_lang.php */
/* Location: ./system/language/indonesian/theme_lang.php */