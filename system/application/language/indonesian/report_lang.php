<?php
/* report_lang */
$lang['report.msgInfo1'] = "Lihat data";
$lang['report.msgInfo2'] = "s.d.";
$lang['report.msgInfo3'] = "Tampilkan";
$lang['report.msgInfo4'] = "Biaya Aktivasi";
$lang['report.msgInfo5'] = "Ubah";
$lang['report.msgInfo6'] = "Report Pendapatan periode";
$lang['report.msgInfo7'] = "Report Perkembangan";
$lang['report.msgInfo8'] = "Total Aktivasi";
$lang['report.msgInfo9'] = "Total Pendapatan";
$lang['report.msgInfo10'] = "Payout Bonus";
$lang['report.msgInfo11'] = "Bonus ";
$lang['report.msgInfo12'] = "Daftar Member Yang dapat bonus Periode ";
$lang['report.msgInfo13'] = "No";
$lang['report.msgInfo14'] = "Tanggal";
$lang['report.msgInfo15'] = "Nama";
$lang['report.msgInfo16'] = "Pilih Bulan dan Tahun";
$lang['report.msgInfo17'] = "Bulan";
$lang['report.msgInfo18'] = "Januari";
$lang['report.msgInfo19'] = "Februari";
$lang['report.msgInfo20'] = "Maret";
$lang['report.msgInfo21'] = "April";
$lang['report.msgInfo22'] = "Mei";
$lang['report.msgInfo23'] = "Juni";
$lang['report.msgInfo24'] = "Juli";
$lang['report.msgInfo25'] = "Agustus";
$lang['report.msgInfo26'] = "September";
$lang['report.msgInfo27'] = "Oktober";
$lang['report.msgInfo28'] = "November";
$lang['report.msgInfo29'] = "Desember";
$lang['report.msgInfo30'] = "Tahun";
$lang['report.msgInfo31'] = "kirim";
$lang['report.msgInfo32'] = "Jumlah Aktivasi";
$lang['report.msgInfo33'] = "Jumlah";
$lang['report.msgInfo34'] = "Daftar Perubahan Setting Bonus";
$lang['report.msgInfo35'] = "Tanggal";
$lang['report.msgInfo36'] = "Nama Bonus";
$lang['report.msgInfo37'] = "Nominal Bonus";
$lang['report.msgInfo38'] = "Daftar Perubahan Registrasi";
$lang['report.msgInfo39'] = "Nama Registrasi";
$lang['report.msgInfo40'] = "Deskripsi";

//custom
$lang['report.msgInfo41'] = "Cari";
/* End of file report_lang.php */
/* Location: ./system/language/indonesian/report_lang.php */