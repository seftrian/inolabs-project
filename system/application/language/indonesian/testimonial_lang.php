<?php
/* testimonial_lang */
$lang['testimonial.msgInfo1'] = "Data belum dipilih";
$lang['testimonial.msgInfo2'] = "Data berhasil diganti";
$lang['testimonial.msgInfo3'] = "Data gagal diganti";
$lang['testimonial.msgInfo4'] = "Ubah Isi Testimoni";
$lang['testimonial.msgInfo5'] = "ID Member";
$lang['testimonial.msgInfo6'] = "Isi Testimoni";
$lang['testimonial.msgInfo7'] = "Simpan";
$lang['testimonial.msgInfo8'] = "Daftar Testimonial";
$lang['testimonial.msgInfo9'] = "Tanggal";
$lang['testimonial.msgInfo10'] = "Publish";
$lang['testimonial.msgInfo11'] = "Ubah";

/* End of file testimonial_lang.php */
/* Location: ./system/language/indonesian/testimonial_lang.php */