<?php
/* info_lang */
$lang['info.msgInfo1'] = "berita";
$lang['info.msgInfo2'] = "ERROR!data belum dipilih";
$lang['info.msgInfo3'] = "Judul Berita";
$lang['info.msgInfo4'] = "isi berita";
$lang['info.msgInfo5'] = "data berhasil ditambah";
$lang['info.msgInfo6'] = "Data gagal ditambah";
$lang['info.msgInfo7'] = "Judul Berita";
$lang['info.msgInfo8'] = "data berhasil diganti";
$lang['info.msgInfo9'] = "data gagal diganti";
$lang['info.msgInfo10'] = "Daftar";
$lang['info.msgInfo11'] = "Tambah";
$lang['info.msgInfo12'] = "Judul";
$lang['info.msgInfo13'] = "Ubah";
$lang['info.msgInfo14'] = "Sumber";
$lang['info.msgInfo15'] = "File Gambar";
$lang['info.msgInfo16'] = "(ukuran terbaik 350px x 350px)";
$lang['info.msgInfo17'] = "Isi";
$lang['info.msgInfo18'] = "Simpan";

/* End of file info_lang.php */
/* Location: ./system/language/indonesian/info_lang.php */