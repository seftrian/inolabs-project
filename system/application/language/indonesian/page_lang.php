<?php
/* page_lang */
$lang['page.msgInfo1'] = "Judul";
$lang['page.msgInfo2'] = "data berhasil ditambahkan";
$lang['page.msgInfo3'] = "data berhasil diubah";
$lang['page.msgInfo4'] = "data gagal diubah";
$lang['page.msgInfo5'] = "halaman tidak ditemukan";
$lang['page.msgInfo6'] = "Tambah";
$lang['page.msgInfo7'] = "Ubah";
$lang['page.msgInfo8'] = "Isi Halaman";
$lang['page.msgInfo9'] = "Simpan";
$lang['page.msgInfo10'] = "Tambah halaman";

/* End of file page_lang.php */
/* Location: ./system/language/indonesian/page_lang.php */