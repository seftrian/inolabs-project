<?php
/* config_lang */
$lang['config.msgInfo1'] = "harus aktif";
$lang['config.msgInfo2'] = "Data";
$lang['config.msgInfo3'] = "berhasil diubah";
$lang['config.msgInfo4'] = "gagal diubah";
$lang['config.msgInfo5'] = "Nilai";
$lang['config.msgInfo6'] = "harus berupa angka";
$lang['config.msgInfo7'] = "Konfigurasi Bonus";
$lang['config.msgInfo8'] = "Ubah";
$lang['config.msgInfo9'] = "Konfigurasi MLM";
$lang['config.msgInfo10'] = "Jenis Bonus";
$lang['config.msgInfo11'] = "Aktif";
$lang['config.msgInfo12'] = "Setting";
$lang['config.msgInfo13'] = "Hapus";
$lang['config.msgInfo14'] = "Konfigurasi Website";
$lang['config.msgInfo15'] = "Title Website";
$lang['config.msgInfo16'] = "Kata Kunci";
$lang['config.msgInfo17'] = "(gunakan koma(,) sebagai pemisah kata kunci)";
$lang['config.msgInfo18'] = "Banner Website";
$lang['config.msgInfo19'] = "(ukuran terbaik 1012px x 200px)";
$lang['config.msgInfo20'] = "Logo Website";
$lang['config.msgInfo21'] = "(ukuran terbaik 200px x 200px)";
$lang['config.msgInfo22'] = "Kirim";
$lang['config.msgInfo23'] = "Pilih Tema";
$lang['config.msgInfo24'] = "Tema dipakai";
$lang['config.msgInfo25'] = "Klik link nama di bawah gambar tema untuk mengaktifkan.";
$lang['config.msgInfo26'] = "Daftar Tema";
$lang['config.msgInfo27'] = "UBAH";
$lang['config.msgInfo28'] = "Detail Bonus";
$lang['config.msgInfo29'] = "Ubah";

/* End of file config_lang.php */
/* Location: ./system/language/indonesian/config_lang.php */