<?php
/* system_lang */
$lang['system.msgInfo1'] = "Username dan password belum anda masukkan.";
$lang['system.msgInfo2'] = "Silakan masukkan Username dan password anda.";
$lang['system.msgInfo3'] = "Username atau password yang anda masukkan salah.";
$lang['system.msgInfo4'] = "Silakan ulangi proses login.";
$lang['system.msgInfo5'] = "Status akun anda tidak aktif.";
$lang['system.msgInfo6'] = "Silakan hubungi Administrator Pusat anda.";
$lang['system.msgInfo7'] = "Proses gagal dilakukan, terdapat kesalahan pada sistem.";
$lang['system.msgInfo8'] = "Data belum dipilih";
$lang['system.msgInfo9'] = "Nama Grup harus anda isi.";
$lang['system.msgInfo10'] = "Grup admin berhasil ditambahkan";
$lang['system.msgInfo11'] = "Grup admin berhasil diubah";
$lang['system.msgInfo12'] = "Ulangi";
$lang['system.msgInfo13'] = "Admin berhasil ditambahkan";
$lang['system.msgInfo14'] = "Username Kosong";
$lang['system.msgInfo15'] = "Admin berhasil diubah";
$lang['system.msgInfo16'] = "Admin gagal diubah";
$lang['system.msgInfo17'] = "Password admin berhasil diubah";
$lang['system.msgInfo18'] = "Password admin gagal diubah";
$lang['system.msgInfo19'] = "Selamat datang di Halaman Administrator";
$lang['system.msgInfo20'] = "Login terakhir anda:";
$lang['system.msgInfo21'] = "Daftar User Administrator";
$lang['system.msgInfo22'] = "Tambah";
$lang['system.msgInfo23'] = "Non Aktifkan";
$lang['system.msgInfo24'] = "Aktifkan";
$lang['system.msgInfo25'] = "Hapus";
$lang['system.msgInfo26'] = "Nama";
$lang['system.msgInfo27'] = "Grup";
$lang['system.msgInfo28'] = "Login Terakhir";
$lang['system.msgInfo29'] = "Aktif";
$lang['system.msgInfo30'] = "Ubah Password";
$lang['system.msgInfo31'] = "Ubah";
$lang['system.msgInfo32'] = "Ubah User Administrator";
$lang['system.msgInfo33'] = "Simpan";
$lang['system.msgInfo34'] = "Tambah User Administrator";
$lang['system.msgInfo35'] = "(password minimal 5 karakter)";
$lang['system.msgInfo36'] = "Ulangi Password";
$lang['system.msgInfo37'] = "Ubah Password User Administrator";
$lang['system.msgInfo38'] = "DATA USER ADMINISTRATOR";
$lang['system.msgInfo39'] = "UBAH PASSWORD";
$lang['system.msgInfo40'] = "Password Baru";
$lang['system.msgInfo41'] = "Daftar Grup Administrator";
$lang['system.msgInfo42'] = "Ubah Grup Administrator";
$lang['system.msgInfo43'] = "Hak Akses Menu";
$lang['system.msgInfo44'] = "CENTANG SEMUA";
$lang['system.msgInfo45'] = "Tambah Grup Administrator";

/* End of file system_lang.php */
/* Location: ./system/language/indonesian/system_lang.php */