<?php
/* widget_lang */
$lang['widget.msgInfo1'] = "Data berhasil dihapus";
$lang['widget.msgInfo2'] = "Data gagal dihapus";
$lang['widget.msgInfo3'] = "Widget berhasil ditambah";
$lang['widget.msgInfo4'] = "Data gagal ditambah";
$lang['widget.msgInfo5'] = "Title widget sudah ada yang bernama";
$lang['widget.msgInfo6'] = "Data berhasil diubah";
$lang['widget.msgInfo7'] = "Sidebar Kiri";
$lang['widget.msgInfo8'] = "Widget Yang tidak aktif";
$lang['widget.msgInfo9'] = "Sidebar Kanan";
$lang['widget.msgInfo10'] = "Widget baru";
$lang['widget.msgInfo11'] = "Buat Widget";
$lang['widget.msgInfo12'] = "Title Widget";
$lang['widget.msgInfo13'] = "Deskripsi Widget";
$lang['widget.msgInfo14'] = "Content Widget";
$lang['widget.msgInfo15'] = "Kirim";
$lang['widget.msgInfo16'] = "Data tidak ada";

/* End of file widget_lang.php */
/* Location: ./widget/language/indonesian/widget_lang.php */