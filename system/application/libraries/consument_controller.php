<?php
/**
* 
* @author Tejo Murti 
* @date 2 September 2013
 * class untuk handle front
 * <ul>
 * <li>integrasi ke template helper</li>
 * </ul>
 */
class consument_controller extends MX_Controller{
    
    //set property untuk template admin
    protected $themeId='front';
    protected $data=array();
    public $themeUrl;
    public $templateName;
    public $currentModule;
    public $seoTitle,$seoDesc,$seoKeywords;
    public function __construct() {
        
        parent::__construct();
        
        //load library umum sistem
        $this->load->library(array('system_lib','session'));
        
        $theme=$this->config->item('theme');//themes yang dipakai apa
        $currentPath='themes/'.$this->themeId.'/'.$theme['frontend']['name'].'/';
        $this->themeUrl= base_url().$currentPath.'inc/';
        $this->templateName=$theme['frontend']['name'];
        $this->currentModule=$this->router->fetch_module();

        if(isset($_GET['continue_front']))
        {
            $this->load->library('function_lib');
            $url=  rawurldecode($_GET['continue_front']);
            $isValidUrl=$this->function_lib->isValidUrl($url);
            if($isValidUrl)
            {
                redirect($url);
            }
        }

        $response_session=$this->check_session();
        $status=$response_session['status'];
        if($status!=200)
        {
            redirect(base_url().'vo_consument/login');
            exit;
        }
    }

    /**
    cek session, jika tidak ada lempar ke halaman login
    */
    public function check_session()
    {
        $status=500;
        $message='Access forbidden';
        $session_arr=$this->session->userdata('consument');
      
        if(!empty($session_arr))
        {
            $person_id=(isset($session_arr['detail']['person_id']) AND intval($session_arr['detail']['person_id'])>0)?$session_arr['detail']['person_id']:0;
            if($person_id>0)
            {
                $status=200;
                $message='OK';
            }

        }
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
}
?>