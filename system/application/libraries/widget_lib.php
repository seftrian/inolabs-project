<?php

class widget_lib {

    public $module_path;
    public $module_path_view;

    /**
     * @param string $location {admin,front,vo}
     * @param string $modules lokasi module
     * @param string $file {filename}
     */
    function run($location, $modules, $file) {
        $ci = &get_instance();

        // ini fungsi dari mana setup
        $theme = $ci->config->item('theme');
        /*
          Array (
          [backend] => Array
          ( [name] => app [url] => http://localhost/iaidiyweb/themes/admin/app/ )
          [frontend] => Array ( [name] => newiaidiy [url] => http://localhost/iaidiyweb/themes/newiaidiy/views/layouts )
         */
        $args = func_get_args();

        $module = '';

        /* is module in filename? */
        if (($pos = strrpos($file, '/')) !== FALSE) {
            $module = substr($file, 0, $pos);
            $file = substr($file, $pos + 1);
        }
        // list($path, $file) = Modules::find($file, $module, 'widgets/');

        $path = APPPATH . 'modules/' . $modules . '/widgets/';
        switch ($location) {
            case 'admin':
                $themeName = $theme['backend']['name'];
                break;
            case 'front':
                $themeName = $theme['frontend']['name'];
                break;
            case 'vo':

                $themeName = $theme['member']['name'];
                break;
        }

        $pathView = FCPATH . 'themes/' . $location . '/' . $themeName . '/widgets/';

        if (!file_exists($path . $file . EXT)) {
            show_error('file widget ' . $file . ' not found', 500);
        }

        Modules::load_file($file, $path);
        $file = ucfirst($file);
        $widget = new $file();

        $widget->module_path = $path;

        $widget->module_path_view = $pathView;
        return call_user_func_array(array($widget, 'run'), array_slice($args, 1));
    }

    // tidak tahu
    function render($view, $data = array()) {
        extract($data);
        include $this->module_path_view . $view . EXT;
    }

    function load($object) {
        $this->$object = load_class(ucfirst($object));
    }

    function __get($var) {
        global $CI;
        return $CI->$var;
    }

}
