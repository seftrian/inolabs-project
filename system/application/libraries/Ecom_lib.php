<?php

/*
 * Function Libraries
 */

// -----------------------------------------------------------------------------
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ecom_lib {

    var $CI;

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
        $this->CI->load->library('function_lib');
    }

      /**
   kirim email no resi
   @param array $data
   extract $data
   - $customer_email
   - $customer_name
   - $transaction_code
   - $textarea_message
   */ 
   public function send_email_no_resi($data)
   {
       extract($data); 
        $this->CI->load->library(array('send_mail','system_lib'));
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();
       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       ob_start();

       ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tbody>
                <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
                <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                    <tbody>
                        <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                            <div style="width:80%;margin:auto"><div>Hei <?php echo $email_to_name?>! Berikut informasi no resi pengiriman untuk nomor pesanan <b><?php echo $transaction_code?></b>.
                             <br />
                            <br /> 
                            <?php echo $textarea_message?></div>
                            </div>

                            <div>
                            <br><br>
                                <!--FOOTER-->
                            <br><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">
                            <div><br>Jika Anda memerlukan bantuan atau informasi lebih lanjut, silakan menghubungi kami pada hari kerja.</div>
                            <br>

                            <div style="color:#923700"><br><b>Terima kasih telah berkunjung dan berbelanja di  <?php echo $websiteConfig['config_name']?>.</b><br></div>
                            <br>Salam dan selamat berkarya,<br><br><b><span style="color:#F41400"> <?php echo $websiteConfig['config_name']?></span><span style="color:#0FFF00">.</span></b>
                            </div></div></font></div></td>
                        </tr>
                    </tbody></table></td>
                </tr>
            </tbody>
        </table>

       <?php
       $message=ob_get_contents();
   
       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
       $emailProperty=array(
           'email_to'=>$customer_email,
           'email_subject'=>'Informasi Pengiriman! Nomor Pesanan '.$transaction_code,
           'email_message'=>$message,
           'email_from'=>'noreply@'.$web_name,
           'email_from_name'=>$web_name,
           'email_from_organization'=>$web_name,
       );
     
       $sendMail->run($emailProperty);
   }

       /**
   kirim email penolakan atas konfirmasi pembayaran
   @param array $data
   extract $data
   - $customer_email
   - $customer_name
   - $transaction_code
   */ 
   public function send_email_payment_reject($data)
   {
       extract($data); 
       $this->CI->load->library(array('send_mail','system_lib'));
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();

       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       ob_start();

       ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tbody>
                <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
                <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                    <tbody>
                        <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                            <div style="width:80%;margin:auto"><div>Hei <?php echo $email_to_name?>! Maaf, pembayaran anda untuk 
                            nomor pesanan <b><?php echo $transaction_code?></b> kami tolak.
                             <br />
                            Jika anda merasa telah terjadi kekeliruan terhadap aksi ini, silakan menghubungi kami pada hari kerja.<br /> 
                            Status pesanan dapat Anda pantau melalui fitur <a href="<?php echo base_url()?>" target="_blank">Status Order</a> yang dapat diakses dari halaman depan situs.</div>
                            </div>

                            <div>
                            <br><br>
                                <!--FOOTER-->
                            <br><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">
                            <div><br>Jika Anda memerlukan bantuan atau informasi lebih lanjut, silakan menghubungi kami pada hari kerja.</div>
                            <br>

                            <div style="color:#923700"><br><b>Terima kasih telah berkunjung dan berbelanja di <?php echo $websiteConfig['config_name']?>.</b><br></div>
                            <br>Salam dan selamat berkarya,<br><br><b><span style="color:#F41400"> <?php echo $websiteConfig['config_name']?></span><span style="color:#0FFF00">.</span></b>
                            </div></div></font></div></td>
                        </tr>
                    </tbody></table></td>
                </tr>
            </tbody>
        </table>

       <?php
       $message=ob_get_contents();

       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
       $emailProperty=array(
           'email_to'=>$customer_email,
           'email_subject'=>'Pembayaran Ditolak! Nomor Pesanan '.$transaction_code,
           'email_message'=>$message,
           'email_from'=>'noreply@'.$web_name,
           'email_from_name'=>$web_name,
           'email_from_organization'=>$web_name,
       );
     
       $sendMail->run($emailProperty);
   }

       /**
   kirim email terimakasih telah konfirmasi pembayaran ke user
   @param array $data
   extract $data
   - $customer_email
   - $customer_name
   - $transaction_code
   */ 
   public function send_email_payment_approve($data)
   {
       extract($data); 
       $this->CI->load->library(array('send_mail','system_lib'));
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();

       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       ob_start();

       ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tbody>
                <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
                <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                    <tbody>
                        <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                            <div style="width:80%;margin:auto"><div>Hei <?php echo $email_to_name?>! Pembayaran anda untuk 
                            nomor pesanan <b><?php echo $transaction_code?></b> telah kami terima.
                             <br />
                            Status pesanan dapat Anda pantau melalui fitur <a href="<?php echo base_url()?>" target="_blank">Status Order</a> yang dapat diakses dari halaman depan situs.</div>
                            </div>

                            <div>
                            <br><br>
                                <!--FOOTER-->
                            <br><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">
                            <div><br>Jika Anda memerlukan bantuan atau informasi lebih lanjut, silakan menghubungi kami pada hari kerja.</div>
                            <br>

                            <div style="color:#923700"><br><b>Terima kasih telah berkunjung dan berbelanja di  <?php echo $websiteConfig['config_name']?>.</b><br></div>
                            <br>Salam dan selamat berkarya,<br><br><b><span style="color:#F41400"> <?php echo $websiteConfig['config_name']?></span><span style="color:#0FFF00">.</span></b>
                            </div></div></font></div></td>
                        </tr>
                    </tbody></table></td>
                </tr>
            </tbody>
        </table>

       <?php
       $message=ob_get_contents();
       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
       $emailProperty=array(
           'email_to'=>$customer_email,
           'email_subject'=>'Pembayaran Diterima! Nomor Pesanan '.$transaction_code,
           'email_message'=>$message,
           'email_from'=>'noreply@'.$web_name,
           'email_from_name'=>$web_name,
           'email_from_organization'=>$web_name,
       );
    
       $sendMail->run($emailProperty);
   }

      /**
   kirim email terimakasih telah konfirmasi pembayaran ke user
   @param array $data
   extract $data
   - $customer_email
   - $customer_name
   - $transaction_code
   */ 
   public function send_email_thanks_payment_confirm($data)
   {
       extract($data); 
       $this->CI->load->library(array('send_mail','system_lib'));
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();

       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       ob_start();

       ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tbody>
                <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
                <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                    <tbody>
                        <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                            <div style="width:80%;margin:auto"><div>Hei <?php echo $email_to_name?>! Terimakasih telah melakukan konfirmasi pembayaran untuk 
                            nomor pesanan <b><?php echo $transaction_code?></b>.
                             <br />
                            Kami akan segera melakukan pengecekan atas konfirmasi pembayaran tersebut.<br /> 
                            Status pesanan dapat Anda pantau melalui fitur <a href="<?php echo base_url()?>" target="_blank">Status Order</a> yang dapat diakses dari halaman depan situs.</div>
                            </div>

                            <div>
                            <br><br>
                                <!--FOOTER-->
                            <br><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">
                            <div><br>Jika Anda memerlukan bantuan atau informasi lebih lanjut, silakan menghubungi kami pada hari kerja.</div>
                            <br>

                            <div style="color:#923700"><br><b>Terima kasih telah berkunjung dan berbelanja di  <?php echo $websiteConfig['config_name']?>.</b><br></div>
                            <br>Salam dan selamat berkarya,<br><br><b><span style="color:#F41400"> <?php echo $websiteConfig['config_name']?></span><span style="color:#0FFF00">.</span></b>
                            </div></div></font></div></td>
                        </tr>
                    </tbody></table></td>
                </tr>
            </tbody>
        </table>

       <?php
       $message=ob_get_contents();
       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
       $emailProperty=array(
           'email_to'=>$customer_email,
           'email_subject'=>'Konfirmasi Pembayaran Nomor Pesanan '.$transaction_code,
           'email_message'=>$message,
           'email_from'=>'noreply@'.$web_name,
           'email_from_name'=>$web_name,
           'email_from_organization'=>$web_name,
       );

       $sendMail->run($emailProperty);
   }

       /**
   kirim email ke admin krn telah ada konfirmasi pembayaran
   @param array $data
   extract $data
   - $customer_email
   - $customer_name
   - $transaction_code
   */ 
   public function send_email_admin_payment_confirm($data)
   {
       extract($data); 
       $this->CI->load->library(array('send_mail','system_lib'));
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();

       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       ob_start();

       ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tbody>
                <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
                <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                    <tbody>
                        <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                            <div style="width:80%;margin:auto"><div>Hei <?php echo $email_to_name?>! Kami menerima konfirmasi pembayaran atas 
                            nomor pesanan <b><?php echo $transaction_code?></b>.
                             <br />
                            Mohon segera melakukan tindakan atas konfirmasi pembayaran tersebut.
                            </div>
                            </div>

                            </div></font></div></td>
                        </tr>
                    </tbody></table></td>
                </tr>
            </tbody>
        </table>

       <?php
       $message=ob_get_contents();
       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
       $emailProperty=array(
           'email_to'=>$customer_email,
           'email_subject'=>'Konfirmasi Pembayaran Nomor Pesanan '.$transaction_code,
           'email_message'=>$message,
           'email_from'=>'noreply@'.$web_name,
           'email_from_name'=>$web_name,
           'email_from_organization'=>$web_name,
       );
       $sendMail->run($emailProperty);
   }
     /**
   kirim email order
   @param array $data
   extract $data
   - table ecom_transaction
   - table ecom_transaction_detail
   - $get_invoice_item dari 2 table di atas
   - $customer_email
   - $customer_name
   */ 
   public function send_email_order($data)
   {
       extract($data); 
       $this->CI->load->library(array('send_mail','system_lib'));
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();

       $this->CI->load->model('product_catalog/product_catalog_lib');
       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       ob_start();

       ?>
       <table width="100%" cellpadding="0" cellspacing="0" border="0" class="message">
        <tbody>
            <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
            <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                <tbody>
                <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                <div style="width:80%;margin:auto"><div>Kami menerima pemesanan atas nama <?php echo $customer_name?> dengan email <b><a href="mailto:<?php echo $customer_email?>" target="_blank"><?php echo $customer_email?></a></b> 
                untuk pembelian sejumlah produk sesuai dengan data di bawah ini. 
                Silakan melakukan pembayaran via transfer ke rekening (pilih salah satu): 
                <br />
                <?php echo $this-> get_bank_information();?>

                Mohon di-transfer dengan <u>angka pas</u> sejumlah <b>Rp <?php echo ecom_lib::currency_rupiah($transaction_total_due)?></b> dan cantumkan <u>Nomor Pesanan</u> 
                Anda saat transfer agar transaksi ini dapat diproses dengan cepat.</div><div>
                <br>Setelah melakukan pembayaran, konfirmasi dapat dilakukan dengan cara mengisi <a href="<?php echo base_url()?>ecom_transaction/payment_confirmation?transaction_code=<?php echo $transaction_code?>" target="_blank">form konfirmasi ini</a>.
                Status pesanan dapat Anda pantau melalui fitur <a href="<?php echo base_url()?>" target="_blank">Status Order</a> yang dapat diakses dari halaman depan situs  <?php echo $websiteConfig['config_name']?>.</div>
                <div><br>Jika Anda memerlukan bantuan atau informasi lebih lanjut, silakan menghubungi kami pada hari kerja.</div>
                <br><br>

                <span style="color:#008000"><b>Data pesanan produk yang kami terima adalah sebagai berikut:</b></span><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">

                <table style="width:100%;font-family:Tahoma;font-size:10pt" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="width:25%"><b>Tanggal Transaksi</b></td><td>: <?php echo convert_datetime($transaction_datetime)?></td><td colspan="2">&nbsp;</td></tr>
                <tr><td><b>Dipesan oleh</b></td><td>: <?php echo $customer_name?></td><td><b>Email</b></td><td>: <a href="mailto:<?php echo $customer_email?>" target="_blank"><?php echo $customer_email?></a></td></tr></tbody></table>

                <div><br><b>Nomor Pesanan</b>: <?php echo $transaction_code;?>
                    <table cellspacing="0" cellpadding="4" border="0" style="width:100%;font-family:Tahoma;font-size:8pt"><tbody><tr style="font-family:Arial;background-color:#eeeeee"><td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:51%">NAMA PRODUK</td><td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:22%">HARGA</td><td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:5%">QTY</td><td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:22%;text-align:right">TOTAL</td></tr></tbody></table>

                    <!--produk-->
                    <table cellspacing="0" cellpadding="4" border="0" style="width:100%;font-family:Tahoma;font-size:8pt"><tbody>
                  <?php
                  $total=0;
                  if(!empty($get_invoice_item))
                  {
                    $imgPathArr=$this->CI->product_catalog_lib->getImagePath();

                    $pathImgUrl=$imgPathArr['pathUrl'];
                    $pathImgLoc=$imgPathArr['pathLocation'];
                    foreach($get_invoice_item AS $rowArr)
                        {
                          foreach($rowArr AS $variable=>$value)
                          {
                            ${$variable}=$value;
                          }
                          $product_image=$this->CI->function_lib->get_one('ecom_product','product_image',array('product_id'=>$transaction_detail_product_id));
                          $logo_name= (trim($product_image)=='')?'-': pathinfo($product_image,PATHINFO_FILENAME);
                          $logo_ext=  pathinfo($product_image,PATHINFO_EXTENSION);
                          $logo_file=$logo_name.'.'.$logo_ext;
                          $imgProperty=array(
                          'width'=>30,
                          'height'=>58,
                          'imageOriginal'=>$logo_file,
                          'directoryOriginal'=>$pathImgLoc,
                          'directorySave'=>$pathImgLoc.'3058/',
                          'urlSave'=>$pathImgUrl.'3058/',
                          );
                          $img=$this->resizeImageMoo($imgProperty);
                          $default_img=base_url().'assets/images/product/default.png';

                          $link_detail=ecom_lib::link_detail_product($transaction_detail_product_id,$transaction_detail_product_title);
                          $subtotal=$transaction_detail_unit_nett_price*$transaction_detail_quantity;
                          ?>
                           <tr style="font-family:Arial">
                                <td style="border-bottom:#cccccc 1px solid;width:51%">
                                <a href="<?php echo $link_detail?>" target="_blank"><b><?php echo $transaction_detail_product_title?></b></a> 
                                <br><img class="lazy-load" src="<?php echo $img?>" alt="<?php echo $transaction_detail_product_title?>" /><div style="color:Red;font-weight:bold"></div>
                                <div>
                                </div></td><td style="border-bottom:#cccccc 1px solid;width:5%">Rp.
                                </td>
                                <td style="border-bottom:#cccccc 1px solid;width:17%;text-align:right"><?php echo ecom_lib::currency_rupiah($transaction_detail_unit_nett_price)?></td>
                                <td style="border-bottom:#cccccc 1px solid;width:5%"><?php echo $transaction_detail_quantity?></td>
                                <td style="border-bottom:#cccccc 1px solid;width:5%">Rp.</td>
                                <td style="border-bottom:#cccccc 1px solid;width:17%;text-align:right"><?php echo ecom_lib::currency_rupiah($subtotal)?></td>
                            </tr>
                     
                          <?php
                          $total+=$subtotal;
                        }
                    }
                    ?>
                 
                    </tbody>
                    </table>
                    <!--end produk-->

                    <table cellspacing="0" cellpadding="4" border="0" style="width:100%;font-family:Tahoma;font-size:8pt"><tbody><tr style="font-family:Arial;background-color:#eeeeee"><td style="border-bottom:#cccccc 1px solid;width:78%;text-align:right"> TOTAL</td>
                    <td style="border-bottom:#cccccc 1px solid;width:5%">Rp.</td><td style="border-bottom:#cccccc 1px solid;width:17%;text-align:right"><b><?php echo ecom_lib::currency_rupiah($total)?></b></td></tr></tbody></table>
                    </div>

                    <!--FOOTER-->
    <br><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">
    <div style="color:#923700"><br><b>Terima kasih telah berkunjung dan berbelanja di  <?php echo $websiteConfig['config_name']?>.</b><br></div>
    <br>Salam dan selamat berkarya,<br><br><b><span style="color:#F41400"> <?php echo $websiteConfig['config_name']?></span><span style="color:#0FFF00">.</span></b>
    </div></div></font></div></td></tr></tbody></table></td></tr></tbody></table>
       <?php
       $message=ob_get_contents();
       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
       $emailProperty=array(
           'email_to'=>$customer_email,
           'email_subject'=>'Ifone - Nomor Pesanan '.$transaction_code,
           'email_message'=>$message,
           'email_from'=>'noreply@'.$web_name,
           'email_from_name'=>$web_name,
           'email_from_organization'=>$web_name,
       );
       $sendMail->run($emailProperty);
   }

   /**
   kirim email reset password
   @param array $data
   extract $data
   - $customer_email
   - $customer_name
   - $link_confirm_password
   */ 
   public function send_email_forget_password($data)
   {
       extract($data); 
       $this->CI->load->library(array('send_mail','system_lib'));
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();

       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       ob_start();

       ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tbody>
                <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
                <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                    <tbody>
                        <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                            <div style="width:80%;margin:auto"><div>Hei <?php echo $email_to_name?>! Anda atau seseorang telah melakukan permintaan reset password.</div><div>
                            <br>Untuk melanjutkannya, silakan klik <a href="<?php echo $link_confirm_password?>" target="_blank">link ini</a>.</div>
                            <br><br>

                                <!--FOOTER-->
                            <br><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">
                            <div><br>Jika Anda memerlukan bantuan atau informasi lebih lanjut, silakan menghubungi <?php echo $this->get_information_phone()?> pada hari kerja.</div>
                            <br>

                            <div style="color:#923700"><br><b>Terima kasih telah berkunjung dan berbelanja di IFONE.</b><br></div>
                            <br>Salam dan selamat berkarya,<br><br><b><span style="color:#F41400"> <?php echo $websiteConfig['config_name']?></span><span style="color:#0FFF00">.</span></b>
                            </div></div></font></div></td>
                        </tr>
                    </tbody></table></td>
                </tr>
            </tbody>
        </table>

       <?php
       $message=ob_get_contents();
       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
       $emailProperty=array(
           'email_to'=>$customer_email,
           'email_subject'=>'Konfirmasi Reset Password',
           'email_message'=>$message,
           'email_from'=>'noreply@'.$web_name,
           'email_from_name'=>$web_name,
           'email_from_organization'=>$web_name,
       );
       $sendMail->run($emailProperty);
   }

   public function get_bank_information()
  {
    $sql='SELECT * FROM `site_bank_account`
    INNER JOIN ref_bank ON bank_id=bank_account_bank_id
    WHERE bank_account_is_active="1"
    ';
    $exec=$this->CI->db->query($sql);
    $results=$exec->result_array();
    $result='<ul>';
    if(!empty($results))
    {
        $no=1;
        foreach($results AS $row)
        {
            $result.='<li>'.$row['bank_name'].' atas nama '.$row['bank_account_name'].' # '.$row['bank_account_no'].'</li>';
            $no++;
        }
    }
    $result.='</ul>';

    return $result;
  }  

  public function get_information_phone()
  {
    $sql='SELECT support_phone FROM site_support WHERE support_is_active="1"
    ';
    $exec=$this->CI->db->query($sql);
    $results=$exec->result_array();
    $telephone='';
    if(!empty($results))
    {
        $no=1;
        foreach($results AS $row)
        {
            $telephone.=($no>1)?'/ '.$row['support_phone']:$row['support_phone'];
            $no++;
        }
    }
    return $telephone;
  } 

  /**
  dapatkan kategori secara detail
  @param int $category_id  
  */  
  public function get_category_from_category_id($category_id)
  {
    $data=array();
    do
    {
        $data[]=$category_id;   
        $where=array(
            'category_id'=>$category_id
        );
        $parent_id=$this->CI->function_lib->get_one('ecom_category','category_par_id',$where);
        
        $category_id=$parent_id;
    }
    while($parent_id>0);
    return $data;
  }

  /**
    dapatkan kategori produk dari product_id
    @param int $product_id
  */
  public function get_product_category_id($product_id)
  {
    $sql='SELECT product_category_category_id 
                FROM ecom_product_category
          WHERE product_category_product_id='.intval($product_id).'
          ORDER BY product_category_category_id DESC';
    $exec=$this->CI->db->query($sql);
    $row=$exec->row_array();
    $category_id=!empty($row)?$row['product_category_category_id']:0;
    return $category_id;      

  } 

  /**
  static method get product price
  */
  public static function get_product_price_by_region($product_id)
  {

    $lib=new Ecom_lib;
    $lib->CI->load->model('ecom_transaction/order_transaction');
    $order_transaction=new order_transaction;
    return $order_transaction->get_price_by_region($product_id);
  }

    public static function javascript($function)
    {

        ob_start();
        ?>
        <script type="text/javascript">


        <?php
        switch(strtolower($function)){
             case 'check_order_status':
            ?>
            function check_order_status(elem_button)
            {
                var button_old_text=elem_button.html();
                elem_button.text('processing...');
                elem_button.attr('disabled',true);
                var jqxhr=$.ajax({
                    url:'<?php echo base_url()?>ecom_transaction/check_order_status',
                    type:'post',
                    dataType:'json',
                    data:$("form.form_check_order_status").serializeArray(),
                });
                jqxhr.success(function(response){
                   
                     $("li#dropdown-check_order_status").addClass('open');
                    $("#msg-check_order_status").show();    
                    if(response['status']==200)
                    {
                        $("#msg-check_order_status").removeClass('alert alert-error');
                        $("#msg-check_order_status").addClass('alert alert-success');
                        $("#msg-check_order_status").html(response['message']);
                        window.location.href='<?php echo base_url()?>ecom_transaction/finish';
                    }
                    else
                    {
                        $("#msg-check_order_status").removeClass('alert alert-success');
                        $("#msg-check_order_status").addClass('alert alert-error');
                        $("#msg-check_order_status").html(response['message']);
                        setTimeout(function(){
                            $("#msg-check_order_status").slideUp('medium');    
                        },5000);
                    }
                    elem_button.html(button_old_text);
                    elem_button.attr('disabled',false);
                   
                    return false;
                });
                jqxhr.error(function(){
                    alert('sorry, an error has occurred, please try again.');
                    elem_button.html(button_old_text);
                    elem_button.attr('disabled',false);
                    $("#msg-check_order_status").slideUp('medium');

                });
                
                return false;
            }
            <?php
            break;
            case 'destination_order':
            ?>
            function select_destination_order(elem)
            {

                var select_value=elem.val();
                if(select_value=='different')
                {
                      $("#recipient_name").val('');
                      $("#recipient_phone").val('');
                      $("#recipient_province_id").val('0');
                      $('select#customer_shipping_recipient_city_id').html('<option value=""></option>');
                      $("#recipient_address").val('');
                      $("#recipient_zipcode").val('');
                }
                else
                {
                      var province_id=$("#customer_province_id").val();
                      $("#recipient_name").val($('#customer_name').val());
                      $("#recipient_phone").val($('#customer_phone').val());
                      $("#recipient_province_id").val(province_id);
                      get_city(province_id,$('select#customer_shipping_recipient_city_id'),$('#customer_city_id').val());
                      //$("#recipient_city_id").val(response['customer_city_id']);
                      $("#recipient_address").val($('#customer_address').val());
                      $("#recipient_zipcode").val($('#customer_zipcode').val());
                }
                return false;
            }
            <?php
            break;
            case 'get_total_item_shopping_cart':
            ?>
            function get_total_item_shopping_cart()
            {

                $.get('<?php echo base_url()?>ecom_transaction/get_total_item_shopping_cart',function(response){
                    $("span#count_items").html(response);
                });
              
            }
            <?php
            break;
            case 'get_total_shopping_cart':
            ?>
            function get_total_shopping_cart()
            {
                var jqxhr=$.ajax({
                    url:'<?php echo base_url()?>ecom_transaction/get_total_shopping_cart',
                    dataType:'json',
                    type:'get'
                });
                jqxhr.success(function(response){
                   $("span.get_total_shopping_cart").html(response);
                });
            }
            <?php
            break;
            case 'get_city':
            ?>
            function get_city(province_id,elem_city,city_id)
            {
                var jqxhr=$.ajax({
                    url:'<?php echo base_url()?>ecom_transaction/get_city/'+province_id,
                    dataType:'json',
                    type:'get'
                });
                jqxhr.success(function(response){
                    elem_city.html('');
                    var selected='';
                    $.each(response,function(index,rowArr){
                        selected=(typeof(city_id)!='undefined' && city_id==rowArr['city_id'])?'selected':'';
                        elem_city.append('<option value="'+rowArr['city_id']+'" '+selected+'>'+rowArr['city_name']+'</option>');
                    });
                });
            }
            <?php
            break;
            case 'login_ecommerce':
            ?>
            function login_ecommerce(elem_button,form_id,redirect_url)
            {
                var button_old_text=elem_button.html();
                elem_button.text('processing...');
                elem_button.attr('disabled',true);
                var jqxhr=$.ajax({
                    url:'<?php echo base_url()?>voffice/login/login_ecommerce',
                    type:'post',
                    dataType:'json',
                    data:$("form#"+form_id).serializeArray()
                });
                jqxhr.success(function(response){
                    if(response['status']==200)
                    {
                        $("#msg-error").slideDown('medium');
                        var link_redirect_url=(redirect_url=='form_transaction')?'<?php echo base_url().'ecom_transaction/form_transaction'?>':'<?php echo base_url()?>vo_ecommerce/index';
                        
                        elem_button.html('OK');
                        window.location.href=link_redirect_url;
                    }
                    else
                    {
                        $("#msg-error").html(response['message']);
                        $("#msg-error").slideDown('medium');
                    }
                    elem_button.html(button_old_text);
                    elem_button.attr('disabled',false);
                    reload_captcha();
                    return false;
                });
                jqxhr.error(function(){
                    alert('sorry, an error has occurred, please try again.');
                    elem_button.html(button_old_text);
                    elem_button.attr('disabled',false);
                    $("#msg-success").slideUp('medium');

                });
            }
            function reload_captcha()
            {
              // $("#captcha_reload").click(function(){
                    var url = '<?php echo site_url('voffice/login/captcha/login'); ?>';
                    $("#captcha_image").attr('src', url + '?' + Math.random());
              //});
               return false;
            }
            <?php
            break;
            case 'add_to_cart_product':
            ?>
            function add_to_cart_product(elem_button,product_id,redirect)
            {
               call_ajax_cart_product(elem_button,product_id);
            }

            function call_ajax_cart_product(elem_button,product_id)
            {
                $.fancybox({
                    'width': '60%',
                    'min-height': '1400px',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'type': 'iframe',
                    'href': '<?php echo base_url()?>ecom_transaction/f_ecom_transaction/generate_html_shopping_cart_product/'+product_id
                });
                $("#msg-success").slideDown('medium');
                $("#msg-success").html('&nbsp;<i class="icon-check"></i> data berhasil ditambahkan <a style="color:red;" href="#" onclick="$(\'#msg-success\').hide();return false;">(x)</a>&nbsp;');
            
               
            }

            
            <?php
            break;
            case 'add_to_cart_nakes':
            ?>
            function add_to_cart_nakes(elem_button,person_id,redirect)
            {
                if(elem_button!=false)
                {
                    var button_old_text=elem_button.html();
                    elem_button.text('processing...');
                    elem_button.attr('disabled',true);
                }
               
                var jqxhr=$.ajax({
                    url:'<?php echo base_url()?>ecom_transaction/service_rest/add_to_cart_nakes/'+person_id,
                    type:'get',
                    dataType:'json'
                });
                jqxhr.success(function(response){
                     
                    if(elem_button!=false)
                    {
                      elem_button.html(button_old_text);
                      elem_button.attr('disabled',false);
                    }

                    if(response['status']==200)
                    {
                        $("#msg-success").slideDown('medium');
                        if(typeof(redirect)!='undefined' && redirect==true)
                        {
                            call_ajax_cart_nakes();
                            //window.location.href='<?php echo base_url()?>ecom_transaction/cart';
                            return false;
                        }
                    }
                    else
                    {
                        alert(response['message']);
                        $("#msg-success").slideUp('medium');

                    }
                    
                    return false;
                });
                jqxhr.error(function(){
                    alert('sorry, an error has occurred, please try again.');
                    
                    if(elem_button!=false)
                    {
                    elem_button.html(button_old_text);
                    elem_button.attr('disabled',false);
                    }
                    $("#msg-success").slideUp('medium');

                });
            }

            function call_ajax_cart_nakes()
            {
                $.fancybox({
                    'width': '60%',
                    'min-height': '1400px',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'type': 'iframe',
                    'href': '<?php echo base_url()?>ecom_transaction/f_ecom_transaction/generate_html_shopping_cart_nakes'
                });
                $("#msg-success").slideDown('medium');
                $("#msg-success").html('&nbsp;<i class="icon-check"></i> data berhasil ditambahkan <a style="color:red;" href="#" onclick="$(\'#msg-success\').hide();return false;">(x)</a>&nbsp;');
            }

            <?php
            break;
            case 'switch_to_nakes_utama':
            ?>
            function switch_to_nakes_utama(current_order_by)
            {
                    var jqxhr=$.ajax({
                        url:'<?php echo base_url()?>ecom_transaction/f_ecom_transaction/switch_to_nakes_utama/'+current_order_by,
                        type:'get',
                        dataType:'json'
                    });
                    jqxhr.success(function(response){

                        if(response['status']==200)
                        {
                           window.location.href='<?php echo base_url().'ecom_transaction/f_ecom_transaction/generate_html_shopping_cart_nakes'?>';
                           return false;
                        }
                        else
                        {
                            alert(response['message']);
                            $("#msg-success").slideUp('medium');
                        }
                        return false;
                    });
                    jqxhr.error(function(){
                        alert('sorry, an error has occurred, please try again.');
                        $("#msg-success").slideUp('medium');
                    });  
                
                
            }
            <?php
            break;
             case 'switch_nakes_utama_to_alternatif':
            ?>
            function switch_nakes_utama_to_alternatif()
            {
                    var jqxhr=$.ajax({
                        url:'<?php echo base_url()?>ecom_transaction/f_ecom_transaction/switch_nakes_utama_to_alternatif',
                        type:'get',
                        dataType:'json'
                    });
                    jqxhr.success(function(response){

                        if(response['status']==200)
                        {
                           window.location.href='<?php echo base_url().'ecom_transaction/f_ecom_transaction/generate_html_shopping_cart_nakes'?>';
                           return false;
                        }
                        else
                        {
                            alert(response['message']);
                            $("#msg-success").slideUp('medium');
                        }
                        return false;
                    });
                    jqxhr.error(function(){
                        alert('sorry, an error has occurred, please try again.');
                        $("#msg-success").slideUp('medium');
                    });  
                
                
            }
            <?php
            break;
            case 'remove_shopping_cart_nakes':
            ?>
            function remove_shopping_cart_nakes(current_order_by)
            {
                if(confirm('Hapus?'))
                {
                    var jqxhr=$.ajax({
                        url:'<?php echo base_url()?>ecom_transaction/f_ecom_transaction/remove_shopping_cart_nakes/'+current_order_by,
                        type:'get',
                        dataType:'json'
                    });
                    jqxhr.success(function(response){

                        if(response['status']==200)
                        {
                           window.location.href='<?php echo base_url().'ecom_transaction/f_ecom_transaction/generate_html_shopping_cart_nakes'?>';
                           return false;
                        }
                        else
                        {
                            alert(response['message']);
                            $("#msg-success").slideUp('medium');
                        }
                        return false;
                    });
                    jqxhr.error(function(){
                        alert('sorry, an error has occurred, please try again.');
                        $("#msg-success").slideUp('medium');
                    });  
                }
                   
                return false;
                
            }
            <?php
            break;

           
        }   
        ?>
        </script>    
        <?php

        
        $script=ob_get_contents();
        ob_end_clean();
        return $script;
    }

     /**
    menampilkan link detail product
    */
    public static function link_detail_category($category_id,$category_title,$base_url=true)
    {
        $link='product_catalog/category/'.$category_id.'/'.ecom_lib::seo_name($category_title);

        if($base_url==true)
        {
            $link=base_url().$link;
        }
      
      return $link;
    }

    /**
    menampilkan link detail product
    */
    public static function link_detail_product($product_id,$product_title)
    {
        return base_url().'product_catalog/content/'.$product_id.'/'.ecom_lib::seo_name($product_title);
    }

    /**
    format rupiah
    @param int $value
    */
    public static function currency_rupiah($value)
    {
        return number_format($value,0,',','.');
    }

     
    /**
     * @author Tejo Murti
     * Fx: CodeIgniter
     * dari library image moo
     * untuk resize image original
     * @param array $imgProperty=array(
                                'width'=>210,
                                'height'=>135,
                                'imageOriginal'=>$row['news_image'], nama file
                                'directoryOriginal'=>$pathImgLoc, lokasi penyimpanan file utama
                                'directorySave'=>$pathImgLoc.'210135/', lokasi penyimpanan file setelah diresize
                                'urlSave'=>$pathImgUrl.'210135/', lokasi file yang akan ditampilkan
                            );
     */
     public function resizeImageMoo($property=array(
                                                        'width'=>77,
                                                        'height'=>100,
                                                        'imageOriginal'=>'',
                                                        'directoryOriginal'=>'',
                                                        'directorySave'=>'',
                                                        'urlSave'=>'',
                                                    ),$pad=true,$crop=false)
    {
         $this->CI->load->library('Image_moo');
         $width=$property['width'];
         $height=$property['height'];
         $directorySave=$property['directorySave'];
         $imgName=$width.$height.'_'.$property['imageOriginal'];
         if(trim($directorySave)=='')
         {
             $directorySave=FCPATH.'assets/images/';
         }
         
        //proses pembuatan directory
        if(!file_exists($directorySave) AND $directorySave!='')
        {
            
            mkdir($directorySave);
            chmod($directorySave, 0777);
        }
        
        
        $imgSource=$property['imageOriginal'];
        
        $folder_un=FCPATH.'assets/images/product/unavailable.jpg';
        $imgMoo=new Image_moo;
        $imgMoo->set_jpeg_quality=100;
        $imageSrc=base_url().'assets/images/product/'.$width.$height.'/unavailable.png';
        //jika file yang diinginkan belum ketemu, maka akan ada pengecekkan
        if(!file_exists($directorySave.$imgName) OR trim($imgSource)=='')
        {
            
            if(file_exists($property['directoryOriginal'].$imgSource) AND trim($imgSource)!='')
            {
                if($crop)
                {
                    
                $thumb=$imgMoo->load($property['directoryOriginal'].$imgSource)
                        ->set_background_colour('#fff')
                        ->resize_crop($width, $height)
                        ->save($directorySave.$imgName,true);
                }
                else
                {
                    
                $thumb=$imgMoo->load($property['directoryOriginal'].$imgSource)
                        ->set_background_colour('#fff')
                        ->resize($width, $height,$pad)
                        ->save($directorySave.$imgName,true);
                }
                
                
                $imageSrc=$property['urlSave'].$imgName;
               
            }
            else
            {
                $thumb=$imgMoo->load($folder_un)
                        ->set_background_colour('#fff')
                        ->resize($width, $height,$pad)
                        ->save($directorySave.$width.$height.'unavailable.png',true);
               
                $imageSrc=$property['urlSave'].$width.$height.'unavailable.png';

            }
        }
        else
        {
            if(file_exists($directorySave.$imgName) AND $imgName!='')
            {
                $imageSrc=$property['urlSave'].$imgName;
            }
        }
        return $imageSrc;
    }  

    /**
    mendapatkan informasi customer
    email, nama, telepon
    */
    public function get_customer_mnp($customer_id,$sep=',')
    {
        $sql='SELECT CONCAT_WS("'.$sep.' ",customer_email,customer_name,customer_phone) AS mnp
            FROM ecom_customer WHERE customer_id='.intval($customer_id);
        $exec=$this->CI->db->query($sql);    
        $row=$exec->row_array();
        return !empty($row)?$row['mnp']:'-';
    }

    /**
    @param string $email
    @param string $password
    */
    public function verify_login($email,$password,$captcha='',$captcha_active=false){

        if(trim($email)=='')
        {
            $status=500;
            $message='Email diperlukan untuk login';
        }
        else if(trim($password)=='')
        {

            $status=500;
            $message='Password diperlukan untuk login';
        }
        else if(trim($captcha)=='' AND $captcha_active==true)
        {
            $status=500;
            $message='Kode captcha diperlukan';
        }
        else
        {

            if($captcha_active==true)
            {
                $this->CI->load->library('captcha');
                if(!$this->CI->captcha->verify($captcha)) {
                    return array(
                            'status'=>500,
                            'message'=>'Kode captcha salah',
                            );
                }
            }

            $datetime=date('Y-m-d');
            $where=array('customer_email'=>$this->CI->security->sanitize_filename($email),
                'customer_password'=>$this->CI->security->sanitize_filename(md5($password)));
            $customer_id=$this->CI->function_lib->get_one('ecom_customer','customer_id',$where);
            if($customer_id)
            {

                $query_customer = $this->CI->function_lib->get_detail_data("ecom_customer", "customer_id", (int)$customer_id);
                $row_customer = $query_customer->row();
                $arr_session_customer = array(
                    'customer_id' => $row_customer->customer_id,
                    'customer_email' => $row_customer->customer_email,
                    'customer_name' => $row_customer->customer_name,
                    'customer_logged_in' => TRUE,
                );
                $this->CI->session->set_userdata($arr_session_customer);

                //cek id 
                $where=array('network_customer_customer_id'=>$customer_id);
                $network_id=$this->CI->function_lib->get_one('sys_network_customer','network_customer_network_id',$where);
                if($network_id)
                {
                    $sql='SELECT * FROM sys_network 
                            INNER JOIN sys_member ON member_network_id=network_id
                            INNER JOIN sys_member_detail ON member_detail_network_id=network_id
                          WHERE network_id='.intval($network_id);
                    $exec=$this->CI->db->query($sql);
                    $row=$exec->row();      
                    $array_items = array(
                        'network_id' => $row->network_id,
                        'network_code' => $row->network_code,
                        'member_name' => $row->member_name,
                        'member_nickname' => $row->member_nickname,
                        'member_last_login' => $row->member_last_login,
                        'member_detail_image' => $row->member_detail_image,
                        'parent_group_network_id' => $network_id,
                        'arr_member_group' => array(),
                        'member_logged_in' => TRUE,
                    );
                    $this->CI->session->set_userdata($array_items);
                    $data = array();
                    $data['member_access_log_network_id'] = $row->network_id;
                    $data['member_access_log_session_id'] = $this->CI->session->userdata('session_id');
                    $data['member_access_log_ip_address'] = $this->CI->session->userdata('ip_address');
                    $data['member_access_log_login_datetime'] = $datetime;
                    $this->CI->db->insert('sys_member_access_log', $data);

                    $data = array();
                    $data['member_last_login'] = $datetime;
                    $this->CI->db->where('member_network_id', $row->network_id);
                    $this->CI->db->update('sys_member', $data);


                    //load model referal
                    $this->CI->load->model('voffice/referal_model');
                    $username = $this->CI->function_lib->get_one("sys_member_account", "member_account_username", array("member_account_network_id" => $row->network_id));
                    $referal_arr=$this->CI->referal_model->check_referal($username);
                    $_SESSION['referal']=$referal_arr;
                }

                $status=200;
                $message='OK';
            }
            else
            {
                $status=500;
                $message='Email atau password tidak ditemukan.';
            }

        }
        return array(
            'status'=>$status,
            'message'=>$message,
            );
    }

    /**
    dapatkan nama produk dari produk id
    @param int $product_id
    **/
    public function get_product_name($product_id)
    {
        $value=$this->CI->function_lib->get_one('ecom_product','product_title',array('product_id'=>intval($product_id)));
        return ($value!='')?$value:'-';
    }
    
     /**
     * untuk seo name
     * @param string $s
     */
    public static function seo_name($s)
    {
    $c = array (' ');
    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
    
    $s = strtolower(str_replace($c, '_', $s)); // Ganti spasi dengan tanda _ dan ubah hurufnya menjadi kecil semua
    return $s;
    }
}
