<?php
/**
 * 
 * class untuk handle admin
 * <ul>
 * <li>Authentifikasi</li>
 * <li>Hak Akses</li>
 * <li>integrasi ke template helper</li>
 * </li>
 */
class admin_controller extends MX_Controller{
    
    //set property untuk template admin
    
    protected $themeId='admin';
    protected $data=array();
    public  $themeUrl,$templateName,$currentModule,$currentClass,$currentMethod,$arr_flashdata;
    protected $authLib;
    
    public function __construct() {
        parent::__construct();
        
        //load model auth
        $this->load->model(array('auth/auth_admin_lib','actv/actv_lib'));

        //load library umum sistem
        $this->load->library('system_lib');
        
        $theme = $this->config->item('theme');//themes yang dipakai apa
        $currentPath = 'themes/'.$this->themeId.'/'.$theme['backend']['name'].'/';
        $this->themeUrl = base_url().$currentPath.'inc/';
        $this->templateName = $theme['backend']['name'];
        $this->currentModule = $this->router->fetch_module(); //module yg sedang aktif
        $this->currentClass = $this->router->fetch_class(); //class/controller yang sedang aktif
        $this->currentMethod = $this->router->fetch_method(); //method yang sedang aktif
        $this->adminUsername=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_username']:'admin';

        $this->arr_flashdata = $this->session->all_flashdata();
        
       /* $check_has_expired=$this->actv_lib->check_has_expired(100);
        if($check_has_expired['status']!=200)
        {
            redirect(base_url().'actv/index?status='.$check_has_expired['status'].'&msg='.base64_encode($check_has_expired['message']));
            exit;
        }
        */

        //untuk melakukan proteksi
        $this->accessRule();

        //pengecekkan kolom
        $column_arr=$this->add_column();
        $this->check_columns_is_exist($column_arr);
    }
    
    /**
     * filter hak akses
     */
    protected function accessRule()
    {
        $this->authLib=new auth_admin_lib;
        
        //dapatkan session admin
        $sessionArr=$this->authLib->getAdminSession();
       
        if(!empty($sessionArr))
        {
            $groupType=isset($sessionArr['detail']['admin_group_type'])?$sessionArr['detail']['admin_group_type']:array();
            if(empty($groupType))
            {
                 $referer=  rawurlencode("http://".$_SERVER['HTTP_HOST']. preg_replace('@/+$@','',$_SERVER['REQUEST_URI']).'/');
                 $origin=isset($_SERVER['HTTP_REFERER'])?rawurlencode($_SERVER['HTTP_REFERER']):$referer;
                 redirect(base_url().'auth/connect_to_admin?continue='.$origin);
                 exit;   
            }
            $allowed=($groupType=='superuser')?true:false; //inisialisasi perizinan akses

            $privilegeAccess=isset($sessionArr['privilege'])?$sessionArr['privilege']:array();
            if($groupType!='superuser')
            {
                if(!empty($privilegeAccess))
                {
                    foreach($privilegeAccess AS $access)
                    {
                        $myModule=$access['module'];
                        $myController=$access['controller'];
                        $myMethod=$access['method'];
                        if($myModule==$this->currentModule AND $myController==$this->currentClass AND $myMethod==$this->currentMethod)
                        {
                            $allowed=true;
                        }
                    }
                }
            }
            
            //pengecekkan hak akses
            if($allowed===false)
            {
                show_error('Maaf, anda tidak diizinkan untuk mengakses halaman ini. 
                    <br /><a href="javascript:history.back(-1)">Kembali</a>', 403);
            }
            
        }
        else
        {
            $referer=  rawurlencode("http://".$_SERVER['HTTP_HOST']. preg_replace('@/+$@','',$_SERVER['REQUEST_URI']).'/');
            $origin=isset($_SERVER['HTTP_REFERER'])?rawurlencode($_SERVER['HTTP_REFERER']):$referer;
            redirect(base_url().'auth/connect_to_admin?continue='.$origin);
            exit;    
        }
        
    }

     public function add_column()
        {
            $column=array(
                //;
                array(
                    'table'=>'sys_insurance',
                    'column'=>'insurance_pagu',
                    'query'=>"ALTER TABLE `sys_insurance` ADD `insurance_pagu` DECIMAL(15,2) NULL AFTER `insurance_product`;",    
                    'expire_date'=>'2014-09-30', //kadaluarsa untuk dieksekusi
                ),

            );

            return $column;
        }

        public function check_columns_is_exist($column_arr)
        {
            $ci=&get_instance();
            $today=date('Y-m-d');
            if(!empty($column_arr))
            {
                foreach($column_arr AS $rowArr)
                {
                    foreach($rowArr AS $variable=>$value)
                    {
                        ${$variable}=$value;
                    }

                    if(isset($table) AND isset($column) AND isset($query) AND strtotime($today)<=strtotime($expire_date))
                    {
                        //execute
                        //check is exist
                        $check_is_exist='SELECT * 
                                FROM information_schema.COLUMNS 
                                WHERE 
                                    TABLE_SCHEMA ="'.$ci->db->database.'" 
                                AND TABLE_NAME = "'.$table.'"
                                AND COLUMN_NAME = "'.$column.'"';
                        $execute=$ci->db->query($check_is_exist);
                        $row=$execute->row_array();
                        if(empty($row))
                        {
                            $ci->db->query($query); 
                        }
                    }
                }
            }
        }
    
}
?>