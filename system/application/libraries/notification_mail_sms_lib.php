<?php
/***
	library untuk kirim notifikasi
	- notifikasi email
	- notifikasi sms reguler
*/
	// -----------------------------------------------------------------------------
if (!defined('BASEPATH')) exit('No direct script access allowed');

class notification_mail_sms_lib {

    var $CI;
    protected $mainTable='ecom_medical_header';

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
        $this->CI->load->library(array('function_lib','send_mail','system_lib','api_zenziva'));
    }

    /**
   kirim email konfirmasi setelah followup
   @param array $data
   extract $data
   - transaction_id
   - transaction_datetime
   - transaction_type
   - $customer_email
   - $customer_name
   - $transaction_code
   - $transaction_datetime
   - $invoice_item //detail item transaksi
   - $termin_arr //detail item transaksi
   $person_phone
   */ 
   public function followup_confirmation($data)
   {
       extract($data); 
       $websiteConfig = $this->CI->system_lib->getWebsiteConfiguration();
       $sendMail=new send_mail; 
       $email_to=$customer_email;
       $email_to_name=$customer_name;

       $this->setMainTable('ecom_transaction_detail');
       $where='transaction_detail_transaction_id='.intval($transaction_id);
       $invoice_item=$this->findAll($where,'no limit','no offset','transaction_detail_id ASC');
       //termin arr
       $this->setMainTable('ecom_transaction_termin');
       $where='termin_transaction_id='.intval($transaction_id);
       $termin_arr=$this->findAll($where,'no limit','no offset','termin_id ASC');

       ob_start();

       ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tbody>
                <tr><td colspan="2"><font size="-1" class="recipient"></font></td></tr>
                <tr><td colspan="2"><table width="100%" cellpadding="12" cellspacing="0" border="0">
                    <tbody>
                        <tr><td><div style="overflow: hidden;"><font size="-1"><div>
                            <div style="width:80%;margin:auto"><div>Dear <?php echo $email_to_name?>,
                            <br />Transaksi anda telah kami konfirmasi dengan rincian sebagai berikut:</b>.
                             <br />
                            <br /> 

			                <span style="color:#008000"><b>Data pesanan yang kami terima adalah sebagai berikut:</b></span><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">

			                <table style="width:100%;font-family:Tahoma;font-size:10pt" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="width:25%"><b>Tanggal Transaksi</b></td><td>: <?php echo convert_datetime($transaction_datetime)?></td><td colspan="2">&nbsp;</td></tr>
			                <tr><td><b>Dipesan oleh</b></td><td>: <?php echo $customer_name?></td><td><b>Email</b></td><td>: <a href="mailto:<?php echo $customer_email?>" target="_blank"><?php echo $customer_email?></a></td></tr></tbody></table>

			                <div><br><b>Nomor Pesanan</b>: <?php echo $transaction_code;?>
			                    <table cellspacing="0" cellpadding="4" border="0" style="width:100%;font-family:Tahoma;font-size:8pt"><tbody>
			                    <tr style="font-family:Arial;background-color:#eeeeee">
			                    <td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:51%">NAMA LAYANAN</td>
			                    <td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:22%">HARGA</td>
			                    <td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:5%">QTY</td>
			                    <td style="border-top:#cccccc 1px solid;border-bottom:#cccccc 1px solid;width:22%;text-align:right">TOTAL</td>
			                    </tr></tbody></table>

			                    <!--produk-->
			                    <table cellspacing="0" cellpadding="4" border="0" style="width:100%;font-family:Tahoma;font-size:8pt"><tbody>
			                  <?php
			                  $total=0;

			                    if(!empty($invoice_item))
                          {
                              $no=1;
                              foreach($invoice_item AS $rowArr)
                              {
                                  foreach($rowArr AS $variable=>$value)
                                  {
                                      ${$variable}=$value;
                                  }
                                  
                                  $label_product=($transaction_type=='service')?$transaction_detail_position_title:$transaction_detail_product_title;
                                  $product_overview=($transaction_type=='service')?'':$this->CI->function_lib->get_one('product_overview','sys_product','product_id='.intval($transaction_detail_product_id));
                                  ?>
                                  <tr>
                                  <td > <?php echo $label_product?> <p class="text-warning"><?php echo $product_overview?></p></td>
                                  <td class="hidden-480" style="text-align: right;"> <?php echo function_lib::currency_rupiah($transaction_detail_unit_nett_price)?> </td>
                                  <td class="hidden-480" style="text-align: right;"> <?php echo ($transaction_detail_quantity*1)?>  <?php echo $transaction_detail_unit?></td>
                                  <td  style="text-align: right;"> <?php echo function_lib::currency_rupiah($transaction_detail_subtotal_price)?> </td>
                              		</tr>
                                  <?php
                                  $total+=$transaction_detail_subtotal_price;
                                  $no++;
                              }
                          }

			                    ?>


			                 
			                    </tbody>
			                    </table>
			                    <!--end produk-->

			                    <table cellspacing="0" cellpadding="4" border="0" style="width:100%;font-family:Tahoma;font-size:8pt"><tbody><tr style="font-family:Arial;background-color:#eeeeee"><td style="border-bottom:#cccccc 1px solid;width:78%;text-align:right"> TOTAL</td>
			                    <td style="border-bottom:#cccccc 1px solid;width:5%">Rp.</td><td style="border-bottom:#cccccc 1px solid;width:17%;text-align:right"><b><?php echo ecom_lib::currency_rupiah($total)?></b></td></tr></tbody></table>
			                    </div>

			                    <?php
			                    if(!empty($termin_arr))
			                    {
			                    	?>
			                    	<p>Termin Pembayaran</p>
			                    	<ul>
			                    	<?php
			                    	foreach($termin_arr AS $rowArr)
			                    	{
			                    		foreach($rowArr AS $variable=>$value)
			                    		{
			                    			${$variable}=$value;
			                    		}
			                    		?>
			                    		<li>
                                  <strong><?php echo $termin_label?></strong>
                                  <p class="text-warning">
                                      Rp <?php echo function_lib::currency_rupiah($termin_nominal)?> (<?php echo ($termin_status=='paid')?'<b>'.$termin_status.'</b>':$termin_status?>)<br />
                                  Batas Waktu: <?php echo convert_date($termin_start_date,'','','ina')?> s/d <?php echo convert_date($termin_end_date,'','','ina')?>
                                  </p>
                              </li>
			                    		<?php
			                    	}
			                    	?>
			                    	</ul>
			                    	<?php
			                    }
			                    ?>
                            </div>
                            </div>

                            <div>
                            <br><br>
                                <!--FOOTER-->
                            <br><hr align="left" width="75%" color="#eeeeee" noshade="" size="1">
                            <div><br>Jika Anda memerlukan bantuan atau informasi lebih lanjut, silakan menghubungi kami pada hari kerja.</div>
                            <br>

                            <div style="color:#923700"><br><b>Terima kasih telah berkunjung dan berbelanja di  <?php echo $websiteConfig['config_name']?>.</b><br></div>
                            <br>Salam dan selamat berkarya,<br><br><b><span style="color:#F41400"> <?php echo $websiteConfig['config_name']?></span><span style="color:#0FFF00">.</span></b>
                            </div></div></font></div></td>
                        </tr>
                    </tbody></table></td>
                </tr>
            </tbody>
        </table>

       <?php
       $message=ob_get_contents();
       ob_end_clean(); 
       $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
       //email for admin
	       $emailProperty=array(
	           'email_to'=>$customer_email,
	           'email_subject'=>'Konfirmasi Permintaan '.$transaction_code,
	           'email_message'=>$message,
	           'email_from'=>'noreply@'.$web_name,
	           'email_from_name'=>$web_name,
	           'email_from_organization'=>$web_name,
	       );
	     
	       $sendMail->run($emailProperty);

       	//kirim sms ke pengguna jasa bahwa data telah dikonfirmasi
        $sms='Kode transaksi pesanan anda '.$transaction_code.' dg biaya Rp '.ecom_lib::currency_rupiah($total).', silakan cek di email atau virtual office anda untuk lebih detil. '.$websiteConfig['config_name'].'.';
        $data=array();
        $data['person_phone']=function_lib::replace_phone_no_to_zero($person_phone);
        $data['message']=strip_tags($sms);
        $response=array();
        $response=$this->CI->api_zenziva->execute('send_sms_reguler',$data);
        $column=array(
                      'sms_mobilephone'=>$data['person_phone'],
                      'sms_text'=>$data['message'],
                      'sms_type'=>'reguler',
                  );
        $this->CI->db->insert('sys_sms',$column);
        $sms_id=$this->CI->function_lib->insert_id();
        if(isset($response['status']) AND $response['status']==200)
        {
          $column=array(
              'sms_status'=>'sent',
          );
         
        }
        else
        {
          $current_try=$this->CI->function_lib->get_one('sms_try_to_send','sys_sms','sms_id='.intval($sms_id));
          $save_try_send=intval($current_try)+1;
          $column=array(
              'sms_status'=>'failed',
              'sms_try_to_send'=>$save_try_send,
          );

        }
        $this->CI->db->update('sys_sms',$column,array('sms_id'=>$sms_id));
   }

   public function setMainTable($table)
    {
        $this->mainTable=$table;
    }

      /**
     * dapatkan semua data
     * @param string $where
     * @param int $limit
     * @param int $offset
     * @param string $orderBy + SORT 
     * @return array
     */
    public function findAll($where=1,$limit=10,$offset=0,$orderBy='transaction_id ASC')
    {
        $sql='SELECT * FROM '.$this->mainTable.'
              WHERE '.$where.' 
              ';
        if(trim($orderBy)!='')
        {
            $sql.=' ORDER BY '.$orderBy;
        }
        if(is_numeric($limit) AND is_numeric($offset))
        {
            $sql.=' LIMIT '.$offset.', '.$limit;
        }
        $exec=$this->CI->db->query($sql);
        return $exec->result_array();
    }

}

?>