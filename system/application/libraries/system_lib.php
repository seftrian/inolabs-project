<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of System_lib
 * class untuk handle sistem secara umum
 * @author tejomurti
 */
require_once APPPATH . 'libraries/recaptcha/recaptchalib.php';

class system_lib
{

    public $breadcrumbs = array();
    public $CI;
    public $pubkeyRecaptcha = "6LfvBeUSAAAAAJJZtYkpzLFXuBroDPP6GOywo4Kh";
    public $privatekeyRecaptcha = "6LfvBeUSAAAAALTdZug4rOMHC439mAfxNYNoMVcZ";
    public function system_lib()
    {
        $this->CI = &get_instance();
    }

    /**
     * fungsi untuk mendapatkan public method pada controller
     * @param string $module
     * @param string $className  class name dan nama file harus sama
     */
    public function getPublicMethodOnController($module, $className)
    {
        $methodArr = array();
        if (file_exists(APPPATH . 'modules/' . $module . '/controllers/' . $className . '.php')) {

            require_once APPPATH . 'modules/' . $module . '/controllers/' . $className . '.php';
            $reflectClass = new ReflectionClass($className);
            $publicArr = $reflectClass->getMethods(ReflectionMethod::IS_PUBLIC);
            if (!empty($publicArr)) {
                foreach ($publicArr as $method) {
                    if ($method->class == $reflectClass->getName() and $method->name != '__construct') {
                        $methodArr[] = $method->name;
                    }
                }
            }
        }
        return $methodArr;
    }

    /**
     * fungsi untuk mendapatkan public method pada controller
     * @param string $module
     * @param string $className  class name dan nama file harus sama
     */
    public function getPublicMethodOnControllerReleaseRequire($module, $className)
    {
        $methodArr = array();
        if (file_exists(APPPATH . 'modules/' . $module . '/controllers/' . $className . '.php')) {
            require_once APPPATH . 'modules/' . $module . '/controllers/' . $className . '.php';
            $reflectClass = new ReflectionClass($className);
            $publicArr = $reflectClass->getMethods(ReflectionMethod::IS_PUBLIC);
            if (!empty($publicArr)) {
                foreach ($publicArr as $method) {
                    if ($method->class == $reflectClass->getName() and $method->name != '__construct') {
                        $methodArr[] = $method->name;
                    }
                }
            }
        }
        return $methodArr;
    }


    /**
     * variable report by option {today,yesterday,this_week,last_week,last_month,3months_ago,}
     * @param string $option
     * @return array array(
     *      'startDate'=>$startDate, //date
     *      'endDate'=>$endDate, //date
     * );
     */
    public static function getReportDate($option)
    {
        $today = date('Y-m-d');
        $day = substr($today, 8, 2);
        $month = substr($today, 5, 2);
        $year = substr($today, 0, 4);
        switch ($option) {
            default:
                $startDate = $today;
                $endDate = $today;
                $label = 'Hari Ini';
                break;
            case 'yesterday':
                $startDate = date('Y-m-d', mktime(0, 0, 0, $month, $day - 1, $year));
                $endDate = date('Y-m-d', mktime(0, 0, 0, $month, $day - 1, $year));
                $label = 'Kemarin';
                break;
            case 'this_week':
                $sundayThisWeek = date('Y-m-d', strtotime('sunday this week'));
                $startDate = date('Y-m-d', strtotime('monday this week'));
                $endDate = (strtotime($sundayThisWeek) > strtotime($today)) ? $today : $sundayThisWeek; //hari ini atau hari minggu kemarin
                $label = 'Minggu Ini';
                break;
            case 'last_week':
                $startDate = date('Y-m-d', strtotime('monday last week')); //tgl hari senin kemarin
                $endDate = date('Y-m-d', strtotime('sunday last week')); //tgl hari minggu kemarin
                $label = 'Minggu Kemarin';
                break;
            case 'last_month':
                $startDate = date('Y-m-d', mktime(0, 0, 0, $month - 1, 1, $year)); //tgl awal bulan kemarin
                $endDate = date('Y-m-d', mktime(0, 0, 0, $month, 1 - 1, $year)); //tgl akhir bulan kemarin
                $label = 'Bulan Kemarin';
                break;
            case '3months_ago':
                $startDate = date('Y-m-d', mktime(0, 0, 0, $month - 3, 1, $year));
                $endDate = date('Y-m-d', mktime(0, 0, 0, $month, 1 - 1, $year));
                $label = '3 Bulan Kemarin';
                break;
        }

        return array(
            'startDate' => $startDate,
            'endDate' => $endDate,
            'label' => $label,
        );
    }

    /**
     * cek apakah respons captcha telah benar
     */
    public function isValidCaptcha()
    {
        $resp = recaptcha_check_answer(
            $this->privatekeyRecaptcha,
            $_SERVER["REMOTE_ADDR"],
            $_POST["recaptcha_challenge_field"],
            $_POST["recaptcha_response_field"]
        );

        if (!$resp->is_valid) {
            $status = 500;
            $message = "The reCAPTCHA wasn't entered correctly. Go back and try it again." .
                "(reCAPTCHA said: " . $resp->error . ")";
        } else {
            // Your code here to handle a successful verification
            $status = 200;
            $message = "OK";
        }

        return array(
            'status' => $status,
            'message' => $message,
        );
    }



    /**
     * @date July 1st 2013
     * modifikasi format json dari jquery
     * @param string $data nama post
     */
    public function jsonModification($data = 'postData')
    {
        $postData = array();
        if (isset($_POST[$data])) {
            $jmlPost = count($_POST[$data]);
            for ($i = 0; $i < $jmlPost; $i++) {
                $postData[$i] = $_POST[$data][$i]['value'];
            }
        }

        return $postData;
    }

    /**
     * set breadcrumbs
     * @param array $breadcrumbs
     */
    public function setBreadcrumbs($breadcrumbs = array())
    {
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * untuk menampilkan breadcrumbs atau navigasi di admin
     * @param array $breadcrumbs 
     *      eq. 
     *          $breadcrumbs=array();
     *          $breadcrumbs[]=array(
     *              'label'=>'dashboard',
     *              'link'=>'',
     *              'icon'=>'',
     *          );
     */
    public function showAdminBreadcrumbs()
    {

        $breadcrumbs = $this->breadcrumbs;
        $html = ' <div id="da-breadcrumb">
                        <ul>';
        if (!empty($breadcrumbs)) {
            foreach ($breadcrumbs as $value) {
                $label = isset($value['label']) ? $value['label'] : '';
                $link = (isset($value['link']) and trim($value['link']) != '') ? $value['link'] : '#';
                $icon = (isset($value['icon']) and trim($value['icon']) != '') ? 'class="' . $value['icon'] . '"' : '';
                $html .= '<li><i ' . $icon . '></i><a href="' . $link . '">' . $label . '</a></li>';
            }
        }
        $html .= '</ul></div>';
        return $html;
    }

    /**
     * dapatkan data konfigurasi website
     * @return array $results=array(
     *      'title'=>'',
     *      'description'=>'',
     *      'keywords'=>'',
     *      'favicon'=>'',
     *      'logo'=>'',
     * );
     * **/
    public function getWebsiteConfiguration()
    {
        $this->CI->load->model('admin_config/admin_config_lib');
        $configLib = new admin_config_lib;
        $dataConfig = $configLib->findAll(1, 'no limit');
        $results = array();
        if (!empty($dataConfig)) {
            $totalData = count($dataConfig);
            for ($i = 0; $i < $totalData; $i++) {
                $results[$dataConfig[$i]['configuration_index']] = $dataConfig[$i]['configuration_value'];
            }
        }
        return $results;
    }



    /**
     * pengecekkan folder untuk upload file
     * @param string $path
     */
    public function preparePath($path = '')
    {
        if (trim($path) != '') {
            $lastDirectory = '/';
            $pathBefore = '';
            $explodeDir =  explode('/', $path);

            if (!empty($explodeDir)) {
                for ($i = 0; $i < count($explodeDir); $i++) {
                    if (trim($explodeDir[$i]) != '') {
                        $lastDirectory .= $explodeDir[$i] . '/';

                        if (!file_exists($lastDirectory) and trim($explodeDir[$i]) != '') {
                            //build directory sebelumnya
                            for ($index = 0; $index < $i; $index++) {
                                $pathBefore .= $explodeDir[$index] . '/';
                            }

                            if (!is_writable($pathBefore)) {
                                show_error('Maaf, Direktori ' . $explodeDir[$i] . ' tidak dapat dibuat.', 403);
                            }

                            mkdir($lastDirectory);
                            chmod($lastDirectory, 0777);

                            //reset path
                            $pathBefore = '';
                        }
                    }
                }
            }
        }
    }
}
