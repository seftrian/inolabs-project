<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sendMail
 *
 * @author tejomurti
 */
class sendMail {
     //put your code here
    /**
     * @param array $emailProperty=array(
     *              'email_to'=>'tujuan',
     *              'email_subject'=>'subject',
     *              'email_message'=>'pesan',
     *              'email_from'=>'pengirim',
     *              'email_from_name'=>'pengirim',
     *              'email_organization'=>'pengirim',
     * );
     * @param array $attachment=array(
     *              'dir'=>'nama direktori', hanya nama direktori dan akan ditempatkan ke root
     *              'file'=>'nama file', hanya nama file. contoh tiket.pdf
     *              'data'=>'string file', data string atau kalimat yang akan dimasukkan ke file
     *              
     * );
     */
    public function run($emailProperty=array(),$attachment=array())
    {
        $send=array(
            'status'=>500,
            'message'=>'Gagal mengirimkan email',
        );
        if(!empty($attachment))
        {
            $dir=$attachment['dir']; //nama directory
            $file=$attachment['file']; //string nama file
            $data=$attachment['data']; //string data
            
          //  $pathDir=$this->attachmentDir($dir);
            
            $attachment=$this->attachmentFile($dir,$file,$data);
           
        }
        
        if(!empty($emailProperty))
        {
            $email_to=$emailProperty['email_to'];
            $email_subject=$emailProperty['email_subject'];
            $email_message=$emailProperty['email_message'];
            $email_from=$emailProperty['email_from'];
            $email_from_name=$emailProperty['email_from_name'];
            $email_from_organization=$emailProperty['email_from_organization'];
            
            $send=$this->emailReady($email_to, $email_from, $email_subject, $email_message, $attachment,$email_from_name,$email_from_organization);
        }
        
        return $send;
    }
    
    
    /**
     * persiapkan fungsi pengiriman email
     * @param string $email_to
     * @param string $email_from
     * @param string $email_subject
     * @param string $email_message
     * @param array $attachment=array(
                                'pathFile'=>$pathFile,
                                'ext'=>  pathinfo($pathFile,PATHINFO_EXTENSION),
                                'filename'=>  $file,
                            );
     */
    protected function emailReady($email_to, $email_from,$email_subject, $email_message,$attachment=array(),$email_from_name='',$email_from_organization='')
    {
     
        //$email_from = "dev@demo.com"; // Who the email is from  
        //$email_subject = "Your attached file"; // The Subject of the email  
       // $email_message = "Thanks for visiting mysite.com!  Here is your free file.<br>";
       // $email_message .= "Thanks for visiting.<br>"; // Message that the email has in it  
       // $email_to = 'brandontjblog@gmail.com'; // Who the email is to  
        //$headers = "From: ".$email_from;  
        

       // $headers = "Reply-To: ".$email_from_name." <".$email_from.">\r\n"; 
       // $headers .= "Return-Path: ".$email_from_name." <".$email_from.">\r\n"; 
        $headers = "From: ".$email_from_name." <".$email_from.">"; 
      //  $headers .= "Organization: ".$email_from_organization."\r\n"; 
        
        //jika menggunakan attachment                
        if(!empty($attachment))
        {
            $fileatt = $attachment['pathFile']; // Path to the file                  
            $fileatt_type = "application/".$attachment['ext']; // File Type  
            $fileatt_name = $attachment['filename']; // Filename that will be used for the file as the attachment  
            $file = fopen($fileatt,'rb');  
            $data = fread($file,filesize($fileatt));  
            fclose($file);  
            $semi_rand = md5(time());
            
            $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";  
             $headers .= "\nMIME-Version: 1.0\n" .  
                    "Content-Type: multipart/mixed;\n" .  
                    " boundary=\"{$mime_boundary}\"";  
            $email_message .= "This is a multi-part message in MIME format.\n\n" .  
                        "--{$mime_boundary}\n" .  
                        "Content-Type:text/html; charset=\"iso-8859-1\"\n" .  
                    "Content-Transfer-Encoding: 7bit\n\n" .  
            $email_message .= "\n\n";  
            $data = chunk_split(base64_encode($data));  
            $email_message .= "--{$mime_boundary}\n" .  
                        "Content-Type: {$fileatt_type};\n" .  
                        " name=\"{$fileatt_name}\"\n" .  
                        //"Content-Disposition: attachment;\n" .  
                        //" filename=\"{$fileatt_name}\"\n" .  
                        "Content-Transfer-Encoding: base64\n\n" .  
                        $data .= "\n\n" .  
                        "--{$mime_boundary}--\n";  
        }
        else
        {
            $headers .= "\nMIME-Version: 1.0\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        }
             
        ob_start();
        $messageHtml=$email_message;
        ob_get_clean();
                        
        $ok = @mail($email_to, $email_subject, $messageHtml, $headers);  
        
        if($ok)
        {
            $status=200;
            $message='Email terkirim';
        }
        else
        {
             $status=500;
            $message='Email gagal terkirim';
        }
        
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }
    
    /**
     * persiapkan directory penyimpanan attachment sementara
     * @param stirng $dir
     */
    protected function attachmentDir($dir='file')
    {
        $path=$_SERVER['DOCUMENT_ROOT'].preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'.$dir;
        if(!file_exists($path) AND trim($dir)!='')
        {
            mkdir($path,0777);
            chmod($path, 0777);
        }
        
        return $path;
    }
    
    /**
     * persiapkan file
     * @param string $dir
     * @param string $file
     * @param string $data
     */
    protected function attachmentFile($dir='file',$file='tiket.pdf',$data='')
    {
        $dirInit=$this->attachmentDir($dir);
        $pathFile=$dirInit.'/'.$file;
        //hapus file lama dan siapkan file baru
//        echo $pathFile;
//        exit;
        if(file_exists($pathFile) AND trim($file)!='')
        {
           unlink($pathFile);

        }
        $handle = fopen($pathFile, 'w') or die('Cannot open file:  '.$pathFile); //implicitly creates file
        fwrite($handle, $data);
        fclose($handle);
        $dataArr=array(
            'pathFile'=>$pathFile,
            'ext'=>  pathinfo($pathFile,PATHINFO_EXTENSION),
            'filename'=>  $file,
        );
        return $dataArr;
    }
    
    
}

?>
