<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * -------------------------------------------------------------------
 * Kumpulan script2 yang sering digunakan
 * -------------------------------------------------------------------
 */
class api_zenziva
{
    var $CI = null;

    protected $__subdomain,$__user_key,$__pass_key,$__number,$__masking;
    function api_zenziva()
    {
        $this->CI = & get_instance();
        $this->CI->load->database();

        $this->__subdomain='https://reguler.zenziva.net'; //subdomain dari website
        $this->__user_key='a75r84-'; //api dari menu setting website
        $this->__pass_key='d3m00-'; //api dari menu setting website
        $this->__number='085274397773'; //no telp. jika layanan sms center
        $this->__masking='n/a'; //masking

        $this->CI->load->library(array('icurl'));
    }

    /**
    dapatkan nomor telepon
    */
    public static function get_number()
    {
        return $__number;
    }

    /**
    eksekusi data
    */
    public function execute($method='',$data=array())
    {
        $response=array();
        if(!method_exists($this, $method))
        {
            $status=500;
            $message='Gagal disimpan ke server, request '.$method.' tidak ditemukan.';
            $response=array('status'=>$status,'message'=>$message);

        }
        /*else if(empty($data)){
            $status=500;
            $message='Gagal disimpan ke server, data kosong.';
            $response=array('status'=>$status,'message'=>$message);
        }*/
        else
        {
           
            $response=$this->$method($data);
            $status=500;
            $message=$response['status'];
            $id=null;
            $data=$response['data'];
            if(intval($response['status'])==1)
            {
                $status=200;
                $message='OK';
                $id=isset($response['id'])?$response['id']:null;
                $data=isset($response['response'])?$response['response']:array();
            }

            $response=array('status'=>$status,'message'=>$message,'id'=>$id,'data'=>$data);
        }
        return $response;
    }

    /**
    mendapatkan kredit sms
    */
    public function get_sms_credit($data=array())
    {
        extract($data);

        $nohp=isset($person_phone)?$this->replace_space($person_phone):'-';
        $tipe=isset($tipe)?$this->replace_space($tipe):'reguler';
        $message=isset($message)?$this->replace_space($message):'';

        $url_service=$this->__subdomain.'/api/credit.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key;
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);  
        $response_output=array();
        $response_api=$response['response'];
        //echo '<pre>';
       //print_r($response);
        //echo $response_api->credit->value;
        $text=isset($response_api->credit->text)?$response_api->credit->text:'';
        $credit=isset($response_api->credit->value)?intval($response_api->credit->value):0;
       // echo number_format($credit);
        $activedate=isset($response_api->credit->activedate)?trim($response_api->credit->activedate):0;
        //echo $credit;
        $response_output['status']=$this->is_success($text);
        $response_output['data']=$credit;
        $response_output['response']['credit']=number_format($credit);
        $response_output['response']['active_date']=$activedate;
       // $response_api['data']['active_date']=isset($response_api->credit->activedate)?$response_api->credit->activedate:array();
        return $response_output;             
    }

    /**
    pengecekan kredit sms
    */
    public  function get_sms_credit_reguler($data=array())
    {
        extract($data);

        $nohp=isset($person_phone)?$this->replace_space($person_phone):'-';
        $tipe=isset($tipe)?$this->replace_space($tipe):'reguler';
        $message=isset($message)?$this->replace_space($message):'';

        $url_service=$this->__subdomain.'/apps/smsapibalance.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key;
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);  
        $response_output=array();
        $response_api=$response['response'];
       
        $text='Success';
        $credit=isset($response_api->message->value)?intval($response_api->message->value):0;
     
       // echo number_format($credit);
        $activedate=isset($response_api->message->text)?trim($response_api->message->text):0;
        //echo $credit;
        $response_output['status']=$this->is_success($text);
        $response_output['data']=$credit;
        $response_output['response']['credit']=number_format($credit);
        $response_output['response']['active_date']=$activedate;
       // $response_api['data']['active_date']=isset($response_api->credit->activedate)?$response_api->credit->activedate:array();
        return $response_output;
    }

      /**
    mengirimkan sms ke 1 penerima
    */
    public function send_sms_reguler($data=array())
    {
        extract($data);
        $nohp=isset($person_phone)?$this->replace_space($person_phone):'-';
        $message=isset($message)?$this->replace_space($message):'';

        $url_service=$this->__subdomain.'/apps/smsapi.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key.'&nohp='.$nohp.'&pesan='.$message;
        $uriRequest=$url_service;

        //cek saldo sebelum kirim
        $response_credit=$this->get_sms_credit_reguler();
        if(isset($response_credit['response']['credit']) AND $response_credit['response']['credit']>1)
        { 
            $response=$this->generate_response($uriRequest,$data,__FUNCTION__);  
        }
        else
        {
              $response['status']=500;
              $response['data']=array();
              $response['message']='Kredit sms anda tidak mencukupi.';
        }
        return $response;             
    }

      /**
    mengirimkan sms ke 1 penerima
    */
    public function send_sms_one($data=array())
    {
        extract($data);

        $nohp=isset($person_phone)?$this->replace_space($person_phone):'-';
        $tipe=isset($tipe)?$this->replace_space($tipe):'reguler';
        $message=isset($message)?$this->replace_space($message):'';

        $url_service=$this->__subdomain.'/api/sendsms.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key.'&nohp='.$nohp.'&tipe='.$tipe.'&pesan='.$message;
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);  
       // print_r($response);
        return $response;             
    }

    /**
    dapatkan data semua pesan
    @param array $data
    */
    public function get_all_message($data=array())
    {
        extract($data);
        $from=isset($from)?$from:date('Y-m-d',mktime(0,0,0,date('m'),date('d')-1,date('Y')));
        $to=isset($to)?$to:date('Y-m-d');
        $status=isset($status)?$status:'all';
        $url_service=$this->__subdomain.'/api/inboxgetbydate.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key.'&from='.$from.'&to='.$to.'&status='.$status;
                    
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);     
        return $response;  
    }

    /**
    dapatkan pesan yang belum dibaca
    @param array $data
    */
    public function get_unread_message($data=array())
    {

        $url_service=$this->__subdomain.'/api/readsms.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key;
                    
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);     
        return $response;  
    }

    /**
    dapatkan subdomian
    */
    public function get_subdomain()
    {
        return $this->__subdomain;
    }

    /***
    dapatkan user key
    */
    public function get_user_key()
    {
        return $this->__user_key;
    }

    /**
    dapatkan pass key
    */
    public function get_pass_key()
    {
        return $this->__pass_key;
    }

    /**
    menambahkan phonebook
    */
    public function add_phonebook($data=array())
    {
        extract($data);

        $nama=isset($person_full_name)?$this->replace_space($person_full_name):'-';
        
        $alamat=isset($person_address_value)?$this->replace_space($person_address_value):'-';
        $nohp=isset($person_phone)?$this->replace_space($person_phone):'-';

        $grup=isset($store_name)?$this->replace_space($store_name):'-';
        $kota=isset($person_address_city_name)?$this->replace_space($person_address_city_name):'-';
        $agama=isset($agama)?$this->replace_space($agama):'-';
        $jenis_kelamin=isset($person_gender)?$this->replace_space($person_gender):'-';
        $tgl_lahir=isset($person_birth_date)?$this->replace_space($person_birth_date):'-';
        $pekerjaan=isset($pekerjaan)?$this->replace_space($pekerjaan):'-';

        $url_service=$this->__subdomain.'/api/pbadd.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key.'&nama='.$nama.'&alamat='.$alamat.'&nohp='.
                     $nohp.'&grup='.$grup.'&kota='.str_replace(' ', '', $kota).'&agama='.
                     $agama.'&jenis_kelamin='.$jenis_kelamin.'&tgl_lahir='.$tgl_lahir.'&pekerjaan='.
                     $pekerjaan;
                    
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);     
        return $response;             
    }

     /**
    menambahkan group
    */
    public function add_group($data=array())
    {
        extract($data);

        $grup=isset($store_name)?$this->replace_space($store_name):'-';
        $url_service=$this->__subdomain.'/api/groupadd.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key.'&grup='.$grup;
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);  

        $data=$response['data'];
        $response_api=array();
        $response_api['status']=$response['status'];  
        $id=isset($data->idGrup)?$data->idGrup:null;
        $response_api['id']=$id;
        return $response_api;             
    }

      /**
    menambahkan group
    */
    public function update_group($data=array())
    {
        extract($data);

        $id=isset($id)?$this->replace_space($id):'-';
        $grup=isset($store_name)?$this->replace_space($store_name):'-';
        $url_service=$this->__subdomain.'/api/groupedit.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key.'&id='.$id.'&grup='.$grup;
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);  
     
        $data=$response['data'];
        $response_api=array();
        $response_api['status']=$response['status'];  
        $id=isset($data->idGrup)?$data->idGrup:null;
        $response_api['id']=$id;
        return $response_api;             
    }

        /**
    delete group
    */
    public function delete_group($data=array())
    {
        extract($data);

        $id=isset($id)?$this->replace_space($id):'-';
        $url_service=$this->__subdomain.'/api/groupdelete.php?userkey='.$this->__user_key.'&passkey='.
                     $this->__pass_key.'&id='.$id;
        $uriRequest=$url_service;
        $response=$this->generate_response($uriRequest,$data,__FUNCTION__);  
     
        $data=$response['data'];
        $response_api=array();
        $response_api['status']=$response['status'];  
        $id=isset($data->idGrup)?$data->idGrup:null;
        $response_api['id']=$id;
        return $response_api;             
    }

     /**
     * untuk generate response setelah terjadi request ke api server
     * @param string $uriRequest
     * @param array $parameter
     * @param string $method
     */
    protected function generate_response($uriRequest,$paramater,$method)
    {
        $this->CI->icurl->setUrl($uriRequest);
      //  echo $uriRequest.'<br />';
        $postArr=array(
          //  'secret_code'=>$this->icurl->secret_code,
           // 'data_arr'=>$paramater,
         //   'method'=>$method,
        );
        $this->CI->icurl->setRequestBody($postArr);
        $this->CI->icurl->setVerb('GET');
        $this->CI->icurl->execute();
        $response=  $this->CI->icurl->getResponseBody();
        return $this->convert_response_to_array($response);
    }

    /**
     * @param mixed $response
     * @param string $output
     */
    public function convert_response_to_array($response,$from='xml')
    {
        $data=array();
        $status=false;
        switch(strtolower($from))
        {
            case 'xml':
                $response_server = new SimpleXMLElement($response);
                $data=isset($response_server->message)?$response_server->message:array();
                $status=isset($response_server->message->text)?$response_server->message->text:false;
                $status=$this->is_success($status);
            break;
        }    
        
        $response= array(
            'status'=>$status,
            'data'=>$data,
            'response'=>$response_server,
            );
        return $response;
    }

    protected function is_success($response)
    {
        $pattern='/Success/';
        $is_match=preg_match($pattern, $response);
     
        return $is_match;
    }

    protected function replace_space($words)
    {
        return str_replace(' ', '%20', trim($words));
    }

}
?>
