<?php
/**
* 
* @author Tejo Murti 
* @date 21 Maret 2014
 * class untuk handle api
 * 
 */
class api_controller extends MX_Controller{
    
    //set property untuk template admin
    public $api_status,$api_message,$api_data,$api_allowed_post,$api_method;
    public $api_output='json';
    protected $allowedPost=array(); //config post
    protected $secretCode; //config secret code
    public function __construct() {
        
        parent::__construct();
        $api_config=$this->config->item('api');//themes yang dipakai apa
        $this->secretCode='freedom';
        if(isset($_GET['continue_front']))
        {
            $this->load->library('function_lib');
            $url=  rawurldecode($_GET['continue_front']);
            $isValidUrl=$this->function_lib->isValidUrl($url);
            if($isValidUrl)
            {
                redirect($url);
            }
        }
        
        $this->allowedPost=array(
            'secret_code','data_arr','method',
        );
        //validasi request from client
        $this->validation();
    }
    
    protected function validation()
    {
         //pengecekkan validasi dari ketentuan service
        $isValidTerm=$this->is_valid();
      
        $this->api_status=$isValidTerm['status'];
        $this->api_message=$isValidTerm['message'];
        $this->api_data=$isValidTerm['data'];
        $this->api_method=$isValidTerm['method'];
        if($this->api_status!=200)
        {
            $this->convert_output($isValidTerm);
            exit;
        }
        
    }
    
    /**
     * set output
     * @param string $output
     */
    public function set_output($output)
    {
        $this->api_output=$output;
    }
    
    /**
     * convert data
     * @param array $data
     * @param string $output json
     */
    
    protected function convert_output($data)
    {
        switch(strtolower($this->api_output))
        {
            case 'json':
                echo json_encode($data);
            break;    
        }
    }
    
    /**
     * set post data
     * @param array $allowed_post
     */
    public function set_alloweed_post($allowed_post)
    {
        $this->api_allowed_post=$allowed_post;
    }
    
    /**
     * validasi post data
     * hanya mengizinkan $_POST['secrect_code'] & $_POST['data_arr']
     * <ul>
     * <li>Pengecekkan data yang dilempar ke service, apakah telah sesuai dg $this->allowedPost</li>
     * <li>Pengecekkan secret code</li>
     * <li>return json</li>
     * </ul>
     */
    protected function is_valid()
    {
        $passed=false;
        $postArr=isset($_POST)?$_POST:array();
//        $postArr=array(
//            'secret_code'=>'@joeengressia',
//            'data_arr'=>array(
//                'network_id'=>1,
//            ),
//        );
        $data=array();
        if(!empty($postArr))
        {
            
                for($i=0;$i<count($this->allowedPost);$i++)
                {
                    //$isFounded=array_search($this->allowedPost[$i],$postArr);
                    $isFounded=  array_key_exists($this->allowedPost[$i],$postArr);
                    
                    if($isFounded)
                    {
                        $passed=true;
                        if(isset($postArr[$this->allowedPost[$i]]) AND !empty($postArr[$this->allowedPost[$i]]) AND is_array($postArr[$this->allowedPost[$i]]))
                        {
                            
                            foreach($postArr[$this->allowedPost[$i]] AS $key=>$value)
                            {
                                $data[$key]=$value;
                            }
                        }
                    }
                    else
                    {
                        $passed=false;
                        $data=array();
                         $buildResponse=array(
                        'status'=>500,
                        'message'=>'Validasi gagal. Data yang anda request salah, silakan kontak tejo.murti@inolabs.net.',
                        'data'=>$data,
                        );
                         return $buildResponse;

                    }
                }
                
                //jika melewati validasi, maka bangun data 
                if($passed==true)
                {
                    //pengecekkan secret code
                    if(isset($postArr['secret_code']) AND $this->secretCode==$postArr['secret_code'])
                    { 
                        $buildResponse=array(
                        'status'=>200,
                        'message'=>'Validasi Ok.',
                        'data'=>$data,
                        );
                        
                    }
                    else
                    {
                        $buildResponse=array(
                        'status'=>500,
                        'message'=>'Validasi gagal. Secret Code anda salah, silakan kontak '.$this->config->item('email'),
                        'data'=>$data,
                        );
                    }
                    
                   
                }
                else
                {
                    $buildResponse=array(
                        'status'=>500,
                        'message'=>'Validasi gagal. Secret code atau data yang anda request salah, silakan kontak '.$this->config->item('email'),
                        'data'=>$data,
                    );
                }
                
            
            
            
        }
        else
        {
            $buildResponse=array(
                        'status'=>500,
                        'message'=>'Validasi gagal. Anda tidak diizinkan mengakses fitur ini.',
                        'data'=>$data,
                    );
        }
        
        return array_merge($buildResponse,array('method'=>$_POST['method']));
    }
    
    /**
     * fungsi pengecekkan validasi post data dari method yang akan direquest
     * @param array $postArr
     * @param array $allowedPost
     */
    public function is_valid_method($postArr,$allowedPost)
    {
        if(!empty($postArr))
        {
                $string='';
                if(!empty($allowedPost))
                {
                    $no=1;
                    foreach($allowedPost AS $index) 
                    {   
                        $string.=($no>1)?', '.$index:$index;
                        $no++;
                    }
                }
            
                for($i=0;$i<count($allowedPost);$i++)
                {
                    //$isFounded=array_search($this->allowedPost[$i],$postArr);
                    $isFounded=  array_key_exists($allowedPost[$i],$postArr);
                   
                    
                    if($isFounded)
                    {
                        $passed=true;
                    }
                    else
                    {
                        $passed=false;
                           $buildResponse=array(
                            'status'=>500,
                            'message'=>'Validasi gagal. Data yang anda request salah. Dibutuhkan '.$string.' pada request ini. Silakan kontak '.$this->config->item('email'),
                            'data'=>array(),
                            );
                         return $buildResponse;

                    }
                }
                
                //jika melewati validasi, maka bangun data 
                if(isset($passed) AND $passed==true)
                {
                    $buildResponse=array(
                    'status'=>200,
                    'message'=>'Validasi method sukses. ',
                    );
                }
                else
                {
                    $buildResponse=array(
                        'status'=>500,
                        'message'=>'Validasi method gagal. Parameter salah, silakan kontak '.$this->config->item('email'),
                    );
                }
                
            
        }
        else
        {
            $buildResponse=array(
                        'status'=>500,
                        'message'=>'Validasi method gagal. Anda tidak diizinkan mengakses fitur ini.',
                    );
        }
        
        return $buildResponse;

    }
}
?>