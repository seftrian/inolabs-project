<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * -------------------------------------------------------------------
 * Kumpulan script2 yang sering digunakan
 * -------------------------------------------------------------------
 */
class function_lib
{
    var $CI = null;

    function function_lib()
    {
        $this->CI = & get_instance();
        $this->CI->load->database();
    }

    public function wordLimit($string, $word_limit=400){
        if (strlen($string)>$word_limit) {
            $word = mb_substr($string, 0, $word_limit-3).'...';
        }else{
            $word = $string;
        }
        return $word;
    }
    
    public function limit_words($string, $word_limit){
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }
    
    public static function clear_special_character($sting=''){
        return preg_replace('/[^A-Za-z0-9\. -]/', '', $sting);
    }

    public static function get_current_domain()
    {
      $url=base_url();  
      $pieces = parse_url($url);
      $domain = isset($pieces['host']) ? $pieces['host'] : '';
      if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
      }
      return false;
    }

    public static function btn_share_this()
    {
        ob_start();
        
        ?>
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "e601fb94-e914-489d-bdd0-61a98639a3e5", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
        <span class='st_sharethis_large' displayText='ShareThis'></span>
        <span class='st_facebook_large' displayText='Facebook'></span>
        <span class='st_twitter_large' displayText='Tweet'></span>
        <span class='st_linkedin_large' displayText='LinkedIn'></span>
        <span class='st_pinterest_large' displayText='Pinterest'></span>
        <span class='st_email_large' displayText='Email'></span>
        <?php
        $footerScript=ob_get_contents();
        ob_end_clean();
        
        return $footerScript;       
    }

    
    public static function remove_html_tag($string)
    {
        return function_lib::UnfoldLines(preg_replace("/<.*?>/", "", $string));
    }

         /**
    replace +62 jadi 0
    */
    public static function replace_phone_no_to_zero($number)
    {
        return preg_replace('/([+62]){3}/', '0', $number);;
    }

     /**
    generate uniq code and send to mail and sms
    @param string $email_to tujuan email
    @param string $mobilephone tujuan kirim sms
    @param string $ref referensi pengiriman
    @param int $limit maksimal kode unik
    @param boolean $is_sms true kirim sms | false tanpa sms
    */
    public function create_uniq_code($email_to,$mobilephone,$ref='',$limit=5,$is_sms=true)
    {
            $expired_date=date('Y-m-d H:i:s',mktime(date('H'),date('i'),date('s'),date('m'),(date('d')+1),date('Y')));
            $value=md5(uniqid(rand(), true));
            $uniqid=substr($value,0,$limit);
        
           $this->CI->load->library(array('send_mail','api_zenziva'));
           $sendMail=new send_mail; 
           $email_message='Kode verifikasi anda: <b>'.$uniqid.'</b>.';
           $email_to_name='';
           $web_name=preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $this->CI->config->slash_item('base_url'));
           $email_subject=$ref;

            //email for admin
           $emailProperty=array(
               'email_to'=>$email_to,
               'email_subject'=>$email_subject,
               'email_message'=>$email_message,
               'email_from'=>'noreply@'.$web_name,
               'email_from_name'=>$web_name,
               'email_from_organization'=>'',
           );
         
           $sendMail->run($emailProperty);
        
           $phone=($mobilephone!='')?$mobilephone:'-';
           //generate to table
           $column=array(
            'verification_code_email'=>$email_to,
            'verification_code_mobilephone'=>$phone,
            'verification_code_value'=>$uniqid,
            'verification_code_ref'=>($ref!='')?$ref:'-',
            'verification_code_expired_datetime'=>$expired_date,
            );
            $this->CI->db->insert('sys_verification_code',$column);

            if($is_sms==true)
            {
                 //kirim notifikasi sms
                  $sms=$email_message.' '.$web_name.'.';
                  $data=array();
                  $data['person_phone']=function_lib::replace_phone_no_to_zero($phone);
                  $data['message']=strip_tags($sms);
                  $response=array();
                  $response=$this->CI->api_zenziva->execute('send_sms_reguler',$data);
                  $column=array(
                                'sms_mobilephone'=>$data['person_phone'],
                                'sms_text'=>$data['message'],
                                'sms_type'=>'reguler',
                            );
                  $this->CI->db->insert('sys_sms',$column);
                  $sms_id=$this->insert_id();
                  if(isset($response['status']) AND $response['status']==200)
                  {
                    $column=array(
                        'sms_status'=>'sent',
                    );
                   
                  }
                  else
                  {
                    $current_try=$this->get_one('sms_try_to_send','sys_sms','sms_id='.intval($sms_id));
                    $save_try_send=intval($current_try)+1;
                    $column=array(
                        'sms_status'=>'failed',
                        'sms_try_to_send'=>$save_try_send,
                    );

                  }
                  $this->CI->db->update('sys_sms',$column,array('sms_id'=>$sms_id));
            }
         
              

    }

    /**
    cek kode verifikasi
    @param string $code
    @param string $email_to
    @param string $ref
    */
    public function check_verification_code($email_to,$code,$ref)
    {
        $current_time=date('Y-m-d H:i:s');
        $where='verification_code_email="'.$email_to.'" AND verification_code_value="'.$code.'"
                AND verification_code_expired_datetime>="'.$current_time.'"
                AND verification_code_ref="'.$ref.'"';

        $is_exist=$this->get_one('verification_code_id','sys_verification_code',$where);
        
        $status=500;
        $message='ERROR';
        if($is_exist)
        {
            $status=200;
            $message='OK';

            //update kode telah digunakan
            $sql='UPDATE sys_verification_code SET verification_code_expired_datetime="'.$current_time.'"
            WHERE '.$where;
            $this->CI->db->query($sql);
        }                
        return array(
            'status'=>$status,
            'message'=>$message,
        );
    }

     public static function get_person_full_name($person_id)
    {
        $lib=new function_lib;
        $where='person_id='.intval($person_id);
        $value=$lib->get_one('person_full_name','sys_person',$where);
        return (trim($value)!='')?$value:'-';
    }

    public static function get_person_email_addres($person_id)
    {
        $lib=new function_lib;
        $where='additional_info_person_id='.intval($person_id);
        $value=$lib->get_one('additional_info_email_address','sys_person_additional_info',$where.' ORDER BY additional_info_id DESC');
        return (trim($value)!='')?$value:'-';
    }

     public static function get_person_addres($person_id)
    {
        $lib=new function_lib;
        $where='person_address_person_id='.intval($person_id);
        $value=$lib->get_one('person_address_value','sys_person_address',$where.' ORDER BY person_address_id DESC');
        return (trim($value)!='')?$value:'-';
    }

     public static function get_person_phone($person_id)
    {
        $lib=new function_lib;
        $where='person_id='.intval($person_id);
        $value=$lib->get_one('person_phone','sys_person',$where);
        return (trim($value)!='')?$value:'-';
    }

       // Function to get the client IP address
    public static function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /**
    fungsi terbilang
    */
    public static function terbilang($a) {
    $ambil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($a < 12)
        return " " . $ambil[$a];
    elseif ($a < 20)
        return function_lib::terbilang($a - 10) . "belas";
    elseif ($a < 100)
        return function_lib::terbilang($a / 10) . " puluh " . function_lib::terbilang($a % 10);
    elseif ($a < 200)
        return " seratus" . function_lib::terbilang($a - 100);
    elseif ($a < 1000)
        return function_lib::terbilang($a / 100) . " ratus " . function_lib::terbilang($a % 100);
    elseif ($a < 2000)
        return " seribu" . function_lib::terbilang($a - 1000);
    elseif ($a < 1000000)
        return function_lib::terbilang($a / 1000) . " ribu " . function_lib::terbilang($a % 1000);
    elseif ($a < 1000000000)
        return function_lib::terbilang($a / 1000000) . " juta " . function_lib::terbilang($a % 1000000);
    }

     /**
    dapatkan shift yang sedang aktif
    */
    public static function get_current_shift()
    {
        $value=isset($_SESSION['admin']['detail']['shift'])?$_SESSION['admin']['detail']['shift']:0;
        return $value;
    }


     /**
    dapatkan shift yang sedang aktif
    */
    public static function get_current_shift_name()
    {
        $lib=new function_lib;
        $shift_id=function_lib::get_current_shift();
        $where='shift_id='.intval($shift_id);
        $shift_title=$lib->get_one('shift_title','master_shift',$where);
        return $shift_title;
    }
    /**
    dapatkan username yang sedang aktif
    */
    public static function get_current_admin_username()
    {
        $admin_username=isset($_SESSION['admin']['detail']['admin_username'])?$_SESSION['admin']['detail']['admin_username']:'';
        
        return $admin_username;
    }


    /**
    label type log tranasction
    */
    public static function label_log_transaction($value)
    {
        $label=$value;
        switch($value)
        {
            case 11:
            $label='Penerimaan Barang';
            break;
            case 12:
            $label='Penerimaan Distribusi';
            break;
            case 21:
            $label='Penjualan Non Resep';
            break;
            case 22:
            $label='Penjualan Resep';
            break;
            case 23:
            $label='Pengeluaran Distribusi Unit';
            break;
            case 24:
            $label='Retur Supplier';
            break;
            case 31:
            $label='Stock Adjustment';
            break;
            case 41:
            $label='Stock Opname';
            break;

        }
        return $label;
    }

    /**
    dapatkan nama unit/cabang
    */
    public static function get_store_name($store_id)
    {
        $lib=new function_lib;
        $value=$lib->get_one('store_name','sys_store','store_id='.intval($store_id));
        return $value;
    }

    /**
    dapatkan cabang user yang sedang login
    */
    public static function get_current_store_name()
    {
        $admin_id=isset($_SESSION['admin']['detail'])?$_SESSION['admin']['detail']['admin_id']:0;
        $store_id=function_lib::get_store_id($admin_id);

        $store_name=function_lib::get_store_name($store_id);
        return $store_name;

    }

    /**
    konversi numeric id expired date into date
    @param int $ed_id
    */
    public static function get_stock_balance_id_by_ed_batch($ed,$batch)
    {
        $lib=new function_lib;

        $value=$lib->get_one('item_stock_balance_id','trx_item_stock_balance','item_stock_balance_item_no_batch="'.$batch.'" AND item_stock_balance_item_expired_date="'.$ed.'"');
        
        return $value;
    }

    /**
    konversi numeric id expired date into date
    @param int $ed_id
    */
    public static function convertion_expired_date_balance($ed_id)
    {
        $lib=new function_lib;
        if(is_numeric($ed_id) AND trim($ed_id)!='')
        {
            $value=$lib->get_one('item_stock_balance_item_expired_date','trx_item_stock_balance','item_stock_balance_id='.intval($ed_id));
        }
        else 
        {
            $value=$ed_id;
        }

        return $value;
    }

    /**
    pengecekan minimum stok
    @param int $item_id
    @param date $ed
    @param string $batch
    */
    public static function check_minimum_stock($item_id,$ed,$batch)
    {
        $lib=new function_lib;
        $today=date('Y-m-d');
        $sql='SELECT item_stock_balance_item_nama,item_stock_balance_stock,item_stock_balance_item_expired_date,item_stock_balance_item_satuan_name FROM trx_item_stock_balance
              WHERE item_stock_balance_item_id='.intval($item_id).'
              AND item_stock_balance_item_no_batch="'.$batch.'"
              AND item_stock_balance_item_expired_date="'.$ed.'"
              AND item_stock_balance_item_expired_date>="'.$today.'"
              AND item_stock_balance_stock<=(SELECT item_stock_minimal FROM sys_item WHERE item_id="'.intval($item_id).'")';
        $exec=$lib->CI->db->query($sql); 
        $row=$exec->row_array();
        if(!empty($row))
        {
            $item_name=$row['item_stock_balance_item_nama'];
            $item_stock=$row['item_stock_balance_stock']*1;
            $item_satuan=$row['item_stock_balance_item_satuan_name'];
            $name='PERINGATAN STOK: '.$item_name.'.';
            $text='Stok telah melewati minimum stok untuk ED '.date('d/m/Y',strtotime($ed)).' no. batch '.$batch.'.
            Sisa stok saat ini sejumlah '.$item_stock.' '.$item_satuan.'.';
            function_lib::insert_notification($name,$text);
        }     
    }

    /**
    class untuk handle notifikasi
    @param string $name
    @param string $text
    */
    public static function insert_notification($name,$text)
    {
        $lib=new function_lib;
        //dapatkan data superuser
        $sql_user='SELECT admin_username FROM site_administrator sa
        WHERE sa.admin_group_id IN (
                    SELECT sag.admin_group_id FROM site_administrator_group sag WHERE 
                    admin_group_type="superuser")
                            GROUP BY admin_username';
        $exec=$lib->CI->db->query($sql_user);    
        $results=$exec->result_array();
        if(!empty($results))
        {
            foreach($results AS $rowArr)
            {
                    $admin_username=$rowArr['admin_username'];
                    $name_str=(strlen($name)>100)?substr($name,0,80).'...':$name;
                    $text_str=(strlen($text)>255)?substr($text,0,200).'...':$text;
                    $column=array(
                        'notification_list_admin_username'=>$admin_username,
                        'notification_list_title'=>$name_str,
                        'notification_list_text'=>$text_str,
                        );
                    //save
                    $lib->CI->db->insert('notification_list',$column);   
            }
        }       
    }

    public function update_item_balance($item_id,$satuan_name,$expired_date)
    {
        $sql='SELECT * FROM trx_item_stock_balance WHERE item_stock_balance_item_id='.intval($item_id).'
              AND item_stock_balance_item_satuan_name="'.strtoupper($satuan_name).'"
              AND DATE(item_stock_balance_item_expired_date)="'.$expired_date.'"';
        $exec=$CI->db->query($sql);
        $row=$exec->row_array();
        if(!empty($row))
        {
            //update
        }      
        else
        {
            //insert    
            $column=array(
                'item_stock_balance_store_id'=>$store_id,
                'item_stock_balance_item_id'=>$item_id,
                'item_stock_balance_item_barcode'=>$item_barcode,
                'item_stock_balance_item_nama'=>$item_nama,
                'item_stock_balance_item_satuan_name'=>strtoupper($satuan_name),
                'item_stock_balance_item_expired_date'=>$expired_date,
                'item_stock_balance_stock'=>$stock,
                'item_stock_balance_price'=>$price,
                'item_stock_balance_last_item_stock_log_input_datetime'=>$log_input_datetime,
                'item_stock_balance_first_item_stock_log_id'=>$log_id,
                );
            $CI->db->insert('trx_item_stock_balance',$column);
        }
    }

    /**
    dapatkan store id berdasarkan iduser
    @param int $administrator_id
    */
    public static function get_store_id($administrator_id)
    {
        $fl=new function_lib;
        $admin_id=$fl->get_one('administrator_store_store_id','site_administrator_store','administrator_store_administrator_id='.intval($administrator_id));
    
        return intval($admin_id);
    }   

    /**
    dapatkan nama obat, kekuatan dan satuan kekuatan
    eq. bodrex 10mg
    @param int $item_id
    */
    public static function get_item_name_kekuatan_satuan($item_id)
    {
        $ci=&get_instance();
        $sql='SELECT CONCAT_WS(" ",item_nama,item_kekuatan,satuan_name) item_name_kekuatan_satuan
        FROM sys_item
        INNER JOIN sys_satuan ON item_satuan_id=satuan_id
        WHERE item_id='.intval($item_id);
        $exec=$ci->db->query($sql);
        $row=$exec->row_array();
        $value=!empty($row)?$row['item_name_kekuatan_satuan']:'';
        return $value;
    }

     /**
    dapatkan nama obat, kekuatan dan satuan kekuatan
    eq. bodrex 10mg
    @param int $item_id
    */
    public static function get_satuan_name($satuan_id)
    {
        $ci=&get_instance();
        $sql='SELECT satuan_name
        FROM sys_satuan
        WHERE satuan_id='.intval($satuan_id);
        $exec=$ci->db->query($sql);
        $row=$exec->row_array();
        $value=!empty($row)?$row['satuan_name']:'-';
        return $value;
    }

      /**
    dapatkan nama obat, kekuatan dan satuan kekuatan
    eq. bodrex 10mg
    @param int $item_id
    */
    public static function get_satuan_id($satuan_name)
    {
        $ci=&get_instance();
        $sql='SELECT satuan_id
        FROM sys_satuan
        WHERE UPPER(satuan_name)="'.strtoupper($satuan_name).'"';
        $exec=$ci->db->query($sql);
        $row=$exec->row_array();
        $value=!empty($row)?$row['satuan_id']:NULL;
        return $value;
    }

      /**
    dapatkan nama obat, kekuatan dan satuan kekuatan
    eq. bodrex 10mg
    @param int $item_id
    */
    public static function get_sediaan_name($id)
    {
        $ci=&get_instance();
        $sql='SELECT sediaan_name
        FROM sys_sediaan
        WHERE sediaan_id='.intval($id);
        $exec=$ci->db->query($sql);
        $row=$exec->row_array();
        $value=!empty($row)?$row['sediaan_name']:'-';
        return $value;
    }

      /**
    dapatkan nama obat, kekuatan dan satuan kekuatan
    eq. bodrex 10mg
    @param int $item_id
    */
    public static function get_rute_pemberian_name($id)
    {
        $ci=&get_instance();
        $sql='SELECT rute_pemberian_name
        FROM sys_rute_pemberian
        WHERE rute_pemberian_id='.intval($id);
        $exec=$ci->db->query($sql);
        $row=$exec->row_array();
        $value=!empty($row)?$row['rute_pemberian_name']:'-';
        return $value;
    }

       /**
    dapatkan nama obat, kekuatan dan satuan kekuatan
    eq. bodrex 10mg
    @param int $item_id
    */
    public static function get_golongan_obat_name($id)
    {
        $ci=&get_instance();
        $sql='SELECT golongan_obat_name
        FROM sys_golongan_obat
        WHERE golongan_obat_id='.intval($id);
        $exec=$ci->db->query($sql);
        $row=$exec->row_array();
        $value=!empty($row)?$row['golongan_obat_name']:'-';
        return $value;
    }

       /**
    dapatkan nama obat, kekuatan dan satuan kekuatan
    eq. bodrex 10mg
    @param int $item_id
    */
    public static function get_golongan_margin_name($id)
    {
        $ci=&get_instance();
        $sql='SELECT golongan_margin_name
        FROM sys_golongan_margin
        WHERE golongan_margin_id='.intval($id);
        $exec=$ci->db->query($sql);
        $row=$exec->row_array();
        $value=!empty($row)?$row['golongan_margin_name']:'-';
        return $value;
    }

    /**
    hilangkan fungsi format rupiah
    menghilangkan titik untuk pemisah ribuan
    dan mengganti koma dengan titik untuk desimal
    */
    public static function remove_currency_rupiah($value)
    {
        $result=str_replace(array('.',','), array('','.'), $value);
        return $result;
    }

     /**
    hilangkan fungsi format rupiah
    menghilangkan titik untuk pemisah ribuan
    dan mengganti koma dengan titik untuk desimal
    */
    public static function currency_rupiah($value)
    {
        $result=number_format($value,2,',','.');
        return $result;
    }

    /**
    @param string $description
    @param string $admin_username
    @param string $table
    @param string $column_primary
    @param string $primary_value
    */
    public static function save_audit_trail($description,$admin_username,$table,$column_primary,$primary_value)
    {
        $ci=&get_instance();
        
        $sql="INSERT INTO audit_trail ( `audit_trail_username`, `audit_trail_full_name`, `audit_trail_activity`, `audit_trail_table`, `audit_trail_column`, `audit_trail_primary_value`, `audit_trail_timestamp`)
        VALUES ('".$ci->security->sanitize_filename($admin_username)."', 
             '".$ci->security->sanitize_filename($admin_username)."',
             '".$ci->security->sanitize_filename($description)."',
             '".$ci->security->sanitize_filename($table)."', 
             '".$ci->security->sanitize_filename($column_primary)."', 
             '".$ci->security->sanitize_filename($primary_value)."',
              CURRENT_TIMESTAMP)
        ";
        $exec=$ci->db->query($sql);
    }
    
   public static function time_elapsed_string($ptime)
    {
        $etime = time() - strtotime($ptime);

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'year',
                     30 * 24 * 60 * 60  =>  'month',
                          24 * 60 * 60  =>  'day',
                               60 * 60  =>  'hour',
                                    60  =>  'minute',
                                     1  =>  'second'
                    );
        $a_plural = array( 'year'   => 'years',
                           'month'  => 'months',
                           'day'    => 'days',
                           'hour'   => 'hours',
                           'minute' => 'minutes',
                           'second' => 'seconds'
                    );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

    public static function get_admin_username_by_id($id)
    {
        $fl=new function_lib;
        return $fl->get_one('admin_username','site_administrator','admin_id='.intval($id));
    }
    
    
    /**
     * @author Tejo Murti
     * Fx: CodeIgniter
     * dari library image moo
     * untuk resize image original
     * @param array $imgProperty=array(
                                'width'=>210,
                                'height'=>135,
                                'imageOriginal'=>$row['news_image'], nama file
                                'directoryOriginal'=>$pathImgLoc, lokasi penyimpanan file utama
                                'directorySave'=>$pathImgLoc.'210135/', lokasi penyimpanan file setelah diresize
                                'urlSave'=>$pathImgUrl.'210135/', lokasi file yang akan ditampilkan
                            );
     */
     public function resizeImageMoo($property=array(
                                                        'width'=>77,
                                                        'height'=>100,
                                                        'imageOriginal'=>'',
                                                        'directoryOriginal'=>'',
                                                        'directorySave'=>'',
                                                        'urlSave'=>'',
                                                    ),$pad=true,$crop=false)
    {
         $this->CI->load->library('Image_moo');
         $width=$property['width'];
         $height=$property['height'];
         $directorySave=$property['directorySave'];
         $imgName=$width.$height.'_'.$property['imageOriginal'];
         if(trim($directorySave)=='')
         {
             $directorySave=FCPATH.'assets/images/';
         }
         
        //proses pembuatan directory
        if(!file_exists($directorySave) AND $directorySave!='')
        {
            
            mkdir($directorySave);
            chmod($directorySave, 0777);
        }
        
        
        $imgSource=$property['imageOriginal'];
        
        $folder_un=FCPATH.'assets/images/unavailable.png';
        $imgMoo=new Image_moo;
        $imgMoo->set_jpeg_quality=100;
        $imageSrc=base_url().'assets/images/'.$width.$height.'/unavailable.png';
        //jika file yang diinginkan belum ketemu, maka akan ada pengecekkan
        if(!file_exists($directorySave.$imgName) OR trim($imgSource)=='')
        {
            
            if(file_exists($property['directoryOriginal'].$imgSource) AND trim($imgSource)!='')
            {
                if($crop)
                {
                    
                $thumb=$imgMoo->load($property['directoryOriginal'].$imgSource)
                        ->set_background_colour('#fff')
                        ->resize_crop($width, $height)
                        ->save($directorySave.$imgName,true);
                }
                else
                {
                    
                $thumb=$imgMoo->load($property['directoryOriginal'].$imgSource)
                        ->set_background_colour('#fff')
                        ->resize($width, $height,$pad)
                        ->save($directorySave.$imgName,true);
                }
                
                
                $imageSrc=$property['urlSave'].$imgName;
               
            }
            else
            {
                $thumb=$imgMoo->load($folder_un)
                        ->set_background_colour('#fff')
                        ->resize($width, $height,$pad)
                        ->save($directorySave.$width.$height.'unavailable.png',true);
               
                $imageSrc=$property['urlSave'].$width.$height.'unavailable.png';

            }
        }
        else
        {
            if(file_exists($directorySave.$imgName) AND $imgName!='')
            {
                $imageSrc=$property['urlSave'].$imgName;
            }
        }
        return $imageSrc;
    }  
    
    /**
     * menghilangkan tag php
     * @param string $content
     */
    static function UnfoldLines($content) {
        $data = '';
        $content = explode("\n", $content);
        for ($i = 0; $i < count($content); $i++) {
            $line = rtrim($content[$i]);
            while (isset($content[$i + 1]) && strlen($content[$i + 1]) > 0 && ($content[$i + 1]{0} == ' ' || $content[$i + 1]{0} == "\t" )) {
                $line .= rtrim(substr($content[++$i], 1));
            }
            $data .= $line;
        }
        return $data;
    }

    /**
     * untuk seo name
     * @param string $s
     */
    public static function seo_name($s)
    {
    $c = array (' ');
    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
    
    $s = strtolower(str_replace($c, '_', $s)); // Ganti spasi dengan tanda _ dan ubah hurufnya menjadi kecil semua
    return $s;
    }
    
    /**
     * @author Tejo Murti 
     * @date 2 September 2013
     * @param string $title title
     * @param string $desc deskripsi website
     * @param string $keywords keywords
     */
    public static function make_seo($title,$desc,$keywords)
    {
        return array(
            'title'=>function_lib::UnfoldLines($title),
            'desc'=>  function_lib::build_description($desc),
            'keywords'=>  function_lib::build_keywords($keywords),
        );
    }
    
     /**
      * 
     * @author Tejo Murti 
     * @date 2 September 2013
     * make description an article
      * @param string $content
      * @param int $max_length
     */
    public static function build_description($content,$max_length=150)
    {
        
        $pure_content=  strip_tags($content);
        $results=(strlen($pure_content)>$max_length)?substr($pure_content, 0,$max_length):$pure_content;
        return function_lib::UnfoldLines($results);
    }
    
    /**
     * @author Tejo Murti 
     * @date 2 September 2013
     * make keywords an article
     * @param string $content
     */
    public static function build_keywords($content)
    {
            $results='';
        
        if(trim($content)!='')
        {
            $keywords=  explode(' ', strip_tags($content));
            $jml_data=count($keywords);
            
            if($jml_data)
            {
                $no=1;
                foreach($keywords AS $row)
                {
                    if($no<100)
                    {
                        $results.=($no>1)?', '.$row:$row;
                    }
                    
                    $no++;
                }
            }
        }
        
        return function_lib::UnfoldLines($results);
    }
    

    function get_one($field='',$table='',$where='')
    {
        $result = $this->CI->db->query("SELECT ".$field." FROM ".$table." WHERE ".$where." LIMIT 1");
        if($result->num_rows() > 0)
        {
            $row = $result->row_array();
            return $row[$field];
        }
        else return "";
    }

    function get_row($table='',$where='')
    {
        $result = $this->CI->db->query("SELECT * FROM ".$table." WHERE ".$where." LIMIT 1");
        if($result->num_rows() > 0)
        {
            return $result->row_array();
        }
        else return "";
    }

    function get_one_by($field='',$table='',$where='', $orderby='')
    {
        $result = $this->CI->db->query("SELECT ".$field." FROM ".$table." WHERE ".$where." ORDER BY ".$orderby." LIMIT 1");
        if($result->num_rows() > 0)
        {
            $row = $result->row_array();
            return $row[$field];
        }
        else return "";
    }
    
    function insert_id()
    {
        $query = $this->CI->db->query('SELECT LAST_INSERT_ID()');
        $row = $query->row_array();
        return $row['LAST_INSERT_ID()'];
    }

    function get_country_name($area_id)
    {
        $result = $this->get_one("country_name","sys_country","country_id = '{$area_id}'");
        return $result;
    }
    function get_area_name($area_id)
    {
        $result = $this->get_one("area_name","ref_area","area_id = '{$area_id}'");
        return $result;
    }

    /**
     * menampilkan pesan sesuai css
     $breadcrumbs_array=array(
        array(
            'name'=>'home',
            'class'="clip-home-3",
            'link'=>'#',
            'current'=>false, //boolean
        ),
     );
     */
    public function show_breadcrumbs($breadcrumbs_array,$title,$description,$template,$themeId)
    {
        ob_start();
        $result='';
        switch($themeId)
        {
            case 'admin':
                switch($template)
                {
                    case 'app':
                    ?>
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <?php
                                if(!empty($breadcrumbs_array))
                                {

                                    foreach($breadcrumbs_array AS $rowArr)
                                    {
                                        foreach($rowArr AS $variable=>$value)
                                        {
                                            ${$variable}=$value;
                                        }

                                        ?>
                                         <li class="<?php echo (isset($current) AND $current==true)?'active':''?>">
                                            <i class="<?php echo isset($class)?$class:''?>"></i>
                                            <?php echo (isset($current) AND $current==true)? '':'<a href="'.(isset($link)?$link:'#').'">'?>
                                                <?php echo isset($name)?$name:''?>
                                            <?php echo  (isset($current) AND $current==true)?'':'</a>';?>
                                        </li>
                                        <?php    
                                    }
                                }
                                ?>
								<!--
                                <li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>
								-->
                            </ol>
                            <div class="page-header">
                                <h1><?php  echo isset($title)?$title:''?> <small><?php echo isset($description)?$description:''?> </small></h1>
                            </div>
                    <?php
                    break;    
                }
            break;
          
        }

        
        $footerScript=ob_get_contents();
        ob_end_clean();
        
        return $footerScript;
    }
    
    /**
     * menampilkan pesan sesuai css
     */
    public function show_message($msg,$template,$themeId,$status)
    {
        

        $result='';
        switch($themeId)
        {
            case 'admin':
                switch($template)
                {
                    case 'dendelion':
                        $cssClass=($status==200)?'success':'error';
                        $result='<div class="da-message '.$cssClass.'">
                                           '.$msg.'
                                        </div>';
                    break;    
                    case 'ace':
                        $cssClass=($status==200)?'success':'alert-error';
                        $result='
                            
                                <div class="alert alert-block '.$cssClass.'">
                                        <button type="button" class="close" data-dismiss="alert">
                                                <i class="icon-remove"></i>
                                        </button>
                                        '.$msg.'
                                </div>';
                    break;    
                    case 'app':
                        $cssClass=($status==200)?'alert-success':'alert-danger';
                        $result='
                            <div class=" alert '.$cssClass.'">
                                 '.$msg.'
                            </div>
                            ';
                    break;    
                }
            break;
        
        //default using bootstrap alert
            default:
                $cssClass=($status==200)?'alert-success':'alert-danger';
                $result='<div class="alert '.$cssClass.'">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                 '.$msg.'
                </div>';
            break;    
        }
        return $result;
    }
    
    /**
     * format rupiah
     */
    public static function currencyFormat($value,$decimal=0,$country='id')
    {
        $amount=$value;
        switch($country)
        {
            default:
                $amount=number_format($value,$decimal,',','.');
            break;     
        }
        
        return $amount;
    }
    
    public function isValidUrl($url)
    {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
    }
    
    function get_query_condition($params, $count = false) {
        $arr_condition = array();
        
        $arr_condition['parent_select'] = "*";
        if($count) {
            $arr_condition['parent_select'] = "COUNT(*) AS row_count";
        }
        
        $arr_condition['table'] = "";
        if(isset($params['table'])) {
            $arr_condition['table'] = $params['table'];
        }
        
        $arr_condition['select'] = "*";
        if(isset($params['select'])) {
            $arr_condition['select'] = $params['select'];
        }
        
        $arr_condition['join'] = "";
        if(isset($params['join'])) {
            $arr_condition['join'] = $params['join'];
        }
        
        $arr_condition['where_detail'] = " WHERE 1 ";
        if(isset ($params['where_detail'])) {
            $arr_condition['where_detail'] .= "AND " . $params['where_detail'];
        }
        
        $arr_condition['group_by_detail'] = "";
        if(isset ($params['group_by_detail'])) {
            $arr_condition['group_by_detail'] = "GROUP BY " . $params['group_by_detail'];
        }
        
        $arr_condition['where'] = " WHERE 1 ";
        if(isset($params['query']) && $params['query'] != false && $params['query'] != '') {
            $arr_condition['where'] .= "AND " . $params['qtype'] . " LIKE '%" . mysql_real_escape_string($params['query']) . "%' ";
        } elseif(isset($params['optionused']) && $params['optionused'] == 'true') {
            $arr_condition['where'] .= "AND " . $params['qtype'] . " = '" . $params['option'] . "' ";
        } elseif((isset($params['date_start']) && $params['date_start'] != false) && (isset($params['date_end'])) && $params['date_end'] != false) {
            $arr_condition['where'] .= "AND DATE(" . $params['qtype'] . ") BETWEEN '" . mysql_real_escape_string($params['date_start']) . "' AND '" . mysql_real_escape_string($params['date_end']) . "' ";
        } elseif((isset($params['num_start']) && $params['num_start'] != false) && (isset($params['num_end'])) && $params['num_end'] != false) {
            $arr_condition['where'] .= "AND " . $params['qtype'] . " BETWEEN '" . mysql_real_escape_string($params['num_start']) . "' AND '" . mysql_real_escape_string($params['num_end']) . "' ";
        }
        
        if(isset($params['where'])) {
            $arr_condition['where'] .= "AND " . $params['where'];
        }
        
        $arr_condition['group_by'] = "";
        if(isset ($params['group_by'])) {
            $arr_condition['group_by'] = "GROUP BY " . $params['group_by'];
        }
        
        $arr_condition['sort'] = "";
        if(isset($params['order_by']) && $count == false) {
            $arr_condition['sort'] = "ORDER BY " . $params['order_by'];
        } elseif(isset($params['sortname']) && isset($params['sortorder']) && $count == false) {
            $arr_condition['sort'] = "ORDER BY " . $params['sortname'] . " " . $params['sortorder'];
        }
        
        $arr_condition['limit'] = "";
        if(isset($params['rp']) && $count == false) {
            $offset = (($params['page'] - 1) * $params['rp']);
            $arr_condition['limit'] = "LIMIT $offset, " . $params['rp'];
        }
        
        return $arr_condition;
    }
    
    function get_query_data($params, $count = false) {
        extract($this->get_query_condition($params, $count));
        $sql = "
            SELECT $parent_select  
            FROM 
            (
                SELECT $select 
                FROM $table 
                $join 
                $where_detail 
                $group_by_detail
            ) result 
            $where 
            $group_by 
            $sort
            $limit
        ";

        $query = $this->CI->db->query($sql);
        
        if($count) {
            $row = $query->row();
            return isset($row->row_count)?$row->row_count:0;
        } else {
            return $query;
        }
    }
    
    function export_excel_standard($data = false) {
        if($data) {
            $this->CI->load->library('Excel_lib');
            extract($data);
            $filename = url_title($title) . '-' . date("YmdHis");
            $arr_column_name = json_decode($column['name']);
            $arr_column_show = json_decode($column['show']);
            $arr_column_align = json_decode($column['align']);
            $arr_column_title = json_decode($column['title']);

            $first_column = $cell_column = 'A';
            $first_row = $cell_row = 1;
            $excel = new Excel_lib();
            $excel->getProperties()->setTitle($title)->setSubject($title);
            $excel->getActiveSheet()->setTitle(substr($title, 0, 31));
            $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $excel->getDefaultStyle()->getFont()->setName('Arial');
            $excel->getDefaultStyle()->getFont()->setSize(9);

            //title
            $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
            $excel->setActiveSheetIndex(0)->setCellValue($cell_column . $cell_row, strtoupper($title));
            $cell_row++;
            $excel->setActiveSheetIndex(0)->setCellValue($cell_column . $cell_row, 'Tanggal Export : ' . convert_datetime(date("Y-m-d H:i:s"), 'id'));
            $cell_row++;
            $cell_row++;

            if(is_array($arr_column_title)) {
                $cell_column = $first_column;
                foreach($arr_column_title as $id => $value) {
                    if($arr_column_show[$id] == true) {
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
                        $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(9);
                        $excel->getActiveSheet()->getColumnDimension($cell_column)->setWidth(1.3 * strlen($value) + 0.584);

                        $excel->setActiveSheetIndex(0)->setCellValue($cell_column . $cell_row, strtoupper($value));
                        $cell_column++;
                    }
                }
                $cell_row++;
            }

            if($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $cell_column = $first_column;
                    foreach($arr_column_name as $id => $value) {
                        if($arr_column_show[$id] == true) {
                            if(!isset($row->$value)) {
                                $data = '';
                            } else {
                                $data = $row->$value;
                            }
                            $excel->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal($arr_column_align[$id]);
                            //$excel->setActiveSheetIndex(0)->setCellValue($cell_column . $cell_row, $data);
                            $excel->setActiveSheetIndex(0)->setCellValueExplicit($cell_column . $cell_row, $data, PHPExcel_Cell_DataType::TYPE_STRING);
                            $cell_column++;
                        }
                    }
                    $cell_row++;
                }
            }

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
            header('Cache-Control: max-age=0');

            $write = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            $write->save('php://output');
            exit;
        }
    }

    function get_detail_data($table_name, $fieldname, $value_id) {
        $this->CI->db->select('*');
        $this->CI->db->from($table_name);
        $this->CI->db->where($fieldname, $value_id);
        return $this->CI->db->get();
    }
    
    function last_id(){
        $query = $this->CI->db->query('SELECT LAST_INSERT_ID() AS last_insert_id');
        $row = $query->row();
        return $row->last_insert_id;
    }
    
    function insert_data($table_name, $data) {
        $this->CI->db->insert($table_name, $data);
        return $this->CI->db->insert_id();
    }
    
    function update_data($table_name, $fieldname, $value_id, $data) {
        $this->CI->db->where($fieldname, $value_id);
        $this->CI->db->update($table_name, $data);
    }

    function delete_data($table_name, $fieldname, $value_id) {
        $this->CI->db->where($fieldname, $value_id);
        $this->CI->db->delete($table_name);
    }
    
    function set_number_format($number, $is_int = true) {
        if((is_numeric($number) && floor($number) != $number) || $is_int == false) {
            return number_format($number, 2, '.', ',');
        } else {
            return number_format($number, 0, '.', ',');
        }
    }
    
    function clear_thousand_separator($number) {
        return str_replace(',', '', $number);
    }
    
    
    
}
?>
