<?php

/**
 *
 * @author Tejo Murti
 * @date 2 September 2013
 * class untuk handle front
 * <ul>
 * <li>integrasi ke template helper</li>
 * </ul>
 */
class front_controller extends MX_Controller {

    //set property untuk template admin
    protected $themeId = 'front';
    protected $data = array();
    public $themeUrl;
    public $templateName;
    public $currentModule;
    public $seoTitle, $seoDesc, $seoKeywords;

    public function __construct() {

        parent::__construct();

        //load library umum sistem
        $this->load->library('system_lib');

        $theme = $this->config->item('theme'); //themes yang dipakai apa
        $currentPath = 'themes/' . $this->themeId . '/' . $theme['frontend']['name'] . '/';
        $this->themeUrl = base_url() . $currentPath . 'inc/';
        $this->templateName = $theme['frontend']['name'];
        $this->currentModule = $this->router->fetch_module();

        if (isset($_GET['continue_front'])) {
            $this->load->library('function_lib');
            $url = rawurldecode($_GET['continue_front']);
            $isValidUrl = $this->function_lib->isValidUrl($url);
            if ($isValidUrl) {
                redirect($url);
            }
        }
    }

}

?>