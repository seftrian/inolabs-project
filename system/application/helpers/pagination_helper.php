<?php
if (!defined('BASEPATH')) exit('No direct script access allowed.');

function create_pagination($uri, $total_rows, $limit = NULL, $uri_segment = 4)
{
	$ci = &get_instance();
	$ci->load->library('pagination');

	$current_page = $ci->uri->segment($uri_segment, 0);
	$config['base_url'] = base_url() . '/' . $uri . '/';

	$config['total_rows'] = $total_rows; // count all records
	$config['per_page'] = $limit === NULL ? $ci->settings->item('records_per_page') : $limit;
	$config['uri_segment'] = $uri_segment;
	$config['page_query_string'] = FALSE;

	$config['num_links'] = 4;
	$config['full_tag_open'] = "<ul class='pagination pagination-sm justify-content-center'>";

	$config['cur_tag_open'] = '<li class="page-item"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';

	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';

	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';

	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';

	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';

	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	$config['full_tag_close'] = '</ul>';

	$ci->pagination->initialize($config); // initialize pagination

	return array(
		'current_page' 	=> $current_page,
		'per_page' 		=> $config['per_page'],
		'limit'			=> array($config['per_page'], $current_page),
		'links' 		=> $ci->pagination->create_links()
	);
}

function create_pagination_query_string($uri, $total_rows, $limit = NULL, $uri_segment = 4, $current_string = '')
{
	$ci = &get_instance();
	$ci->load->library('pagination');

	$current_page = (isset($_GET['per_page']) and trim($_GET['per_page']) != '') ? $_GET['per_page'] : 0;
	$current_string = (trim($current_string) != '') ? '?query=' . $current_string : '?query=';
	$config['base_url'] = base_url() . '/' . $uri . $current_string;

	$config['total_rows'] = $total_rows; // count all records
	$config['per_page'] = $limit === NULL ? $ci->settings->item('records_per_page') : $limit;
	$config['uri_segment'] = $uri_segment;
	$config['page_query_string'] = true;
	//$config['suffix'] = '?query='.$current_string;

	$config['num_links'] = 4;
	$config['full_tag_open'] = '<ul class="pagination">';

	$config['cur_tag_open'] = '<li class="current"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';

	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';

	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';

	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';

	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';

	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	$config['full_tag_close'] = '</ul>';


	$ci->pagination->initialize($config); // initialize pagination

	return array(
		'current_page' 	=> $current_page,
		'per_page' 		=> $config['per_page'],
		'limit'			=> array($config['per_page'], $current_page),
		'links' 		=> $ci->pagination->create_links()
	);
}
