
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Helper untuk direct printing 
 */

//Untuk print laporan reception retur unit
function print_reception_retur_unit()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
		
		$.get("<?php echo base_url()?>admin/reception_retur_unit/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print laporan reception unit
function print_reception_unit()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
		
		$.get("<?php echo base_url()?>admin/reception_unit/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print laporan distribusi unit
function print_distribusi_unit()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
		
		$.get("<?php echo base_url()?>admin/distribusi_unit/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print laporan retur supplier
function print_retur_supplier()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
		
		$.get("<?php echo base_url()?>admin/retur_supplier/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print laporan penerimaan
function print_penerimaan()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
		
		$.get("<?php echo base_url()?>admin/penerimaan/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print laporan pemesanan
function print_pemesanan()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
		
		$.get("<?php echo base_url()?>admin/pemesanan/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print nota piutang penjualan
function print_billing_transaction()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_type, id_header, id_detail) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_type, id_header, id_detail);
		return false; }
		
		$.get("<?php echo base_url()?>admin/billing_transaction/print_trx/"+id_type+"/"+id_header+"/"+id_detail,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print nota inkaso
function print_inkaso()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header, id_detail) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header, id_detail);
		return false; }
		
		$.get("<?php echo base_url()?>admin/inkaso/print_trx/"+id_header+"/"+id_detail,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

//Untuk print nota resep
function print_resep()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
		
		$.get("<?php echo base_url()?>admin/trx_resep/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}
	

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}



	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

function print_non_resep()
{
	error_reporting(0);
	ob_start();
	?>
	
	<script type="text/javascript">
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function do_printing_transaction(id_header) {
		//jika belum tersedia, maka tampilkan popup print
		if (notReady()) { 
		cetak_struk(id_header);
		return false; }
	
		$.get("<?php echo base_url()?>admin/trx_non_resep/print_trx/"+id_header,function(html_response){
			//console.log(html_response);
			qz.appendHTML('<html>'+html_response+'</html>');
			qz.printHTML();
		});
        
	}

	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter() {
		// Get printer name from input box
		$.getJSON('<?php echo base_url()?>admin_config/service_rest/get_default_printer_nota',function(printer_arr){
		
			var printer_name=printer_arr['name']; //replace space with underline
			if (isLoaded()) {
				// Searches for locally installed printer with specified name
				qz.findPrinter(printer_name);
				
				// Automatically gets called when "qz.findPrinter()" is finished.
				window['qzDoneFinding'] = function() {
					var printer = qz.getPrinter();
					
					// Alert the printer name to user
					/*
					alert(printer !== null ? 'Printer found: "' + printer + 
						'" after searching for "' + p.value + '"' : 'Printer "' + 
						p.value + '" not found.');
					*/
					// Remove reference to this function
					window['qzDoneFinding'] = null;
				};
			}
		});

	}
	
	

	//auto load printer
	$(function(){
		findPrinter();
	});
	</script>
		<?php
		$footerScript=initialize_qzprint();
    $footerScript.=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}

/** inisialisasi direct printing */
function initialize_qzprint()
{
	error_reporting(0);
	ob_start();
		?>
		<script type="text/javascript" src="<?php echo base_url()?>addons/qz-print/qz-print/dist/js/deployJava.js"></script>
	<script type="text/javascript">	
	/**
	* Optionally used to deploy multiple versions of the applet for mixed
	* environments.  Oracle uses document.write(), which puts the applet at the
	* top of the page, bumping all HTML content down.
	*/
	deployQZ();
	
	/**
	* Deploys different versions of the applet depending on Java version.
	* Useful for removing warning dialogs for Java 6.  This function is optional
	* however, if used, should replace the <applet> method.  Needed to address 
	* MANIFEST.MF TrustedLibrary=true discrepency between JRE6 and JRE7.
	*/
	function deployQZ() {
		var attributes = {id: "qz", code:'qz.PrintApplet.class', 
			archive:'<?php echo base_url()?>addons/qz-print/qz-print/dist/qz-print.jar', width:1, height:1};
		var parameters = {jnlp_href: '<?php echo base_url()?>addons/qz-print/qz-print/dist/qz-print_jnlp.jnlp', 
			cache_option:'plugin', disable_logging:'false', 
			initial_focus:'false'};
		if (deployJava.versionCheck("1.7+") == true) {}
		else if (deployJava.versionCheck("1.6+") == true) {
			delete parameters['jnlp_href'];
		}
		deployJava.runApplet(attributes, parameters, '1.5');
	}
	
	/**
	* Automatically gets called when applet has loaded.
	*/
	function qzReady() {
		// Setup our global qz object
		window["qz"] = document.getElementById('qz');
		var title = document.getElementById("title");
		if (qz) {
			try {
				title.innerHTML = title.innerHTML + " " + qz.getVersion();
				document.getElementById("content").style.background = "#F0F0F0";
			} catch(err) { // LiveConnect error, display a detailed meesage
				document.getElementById("content").style.background = "#F5A9A9";
				alert("ERROR:  \nThe applet did not load correctly.  Communication to the " + 
					"applet has failed, likely caused by Java Security Settings.  \n\n" + 
					"CAUSE:  \nJava 7 update 25 and higher block LiveConnect calls " + 
					"once Oracle has marked that version as outdated, which " + 
					"is likely the cause.  \n\nSOLUTION:  \n  1. Update Java to the latest " + 
					"Java version \n          (or)\n  2. Lower the security " + 
					"settings from the Java Control Panel.");
		  }
	  }
	}
	
	/**
	* Returns whether or not the applet is not ready to print.
	* Displays an alert if not ready.
	*/
	function notReady() {
		// If applet is not loaded, display an error
		if (!isLoaded()) {
			return true;
		}
		// If a printer hasn't been selected, display a message.
		else if (!qz.getPrinter()) {
			alert('Sedang mencari printer yang aktif, mohon ulangi kembali.');
			return true;
		}
		return false;
	}
	
	/**
	* Returns is the applet is not loaded properly
	*/
	function isLoaded() {
		if (!qz) {
			alert('Error:\n\n\tPrint plugin is NOT loaded!');
			return false;
		} else {
			try {
				if (!qz.isActive()) {
					alert('Error:\n\n\tPrint plugin is loaded but NOT active!');
					return false;
				}
			} catch (err) {
				alert('Error:\n\n\tPrint plugin is NOT loaded properly!');
				return false;
			}
		}
		return true;
	}
	
	/**
	* Automatically gets called when "qz.print()" is finished.
	*/
	function qzDonePrinting(alert_message) {
		// Alert error, if any
		if (qz.getException()) {
			alert('Error printing:\n\n\t' + qz.getException().getLocalizedMessage());
			qz.clearException();
			return; 
		}
		
		if(typeof(alert_message)!='undefined' && alert_message==true)
		{
			// Alert success message
			alert('Successfully sent print data to "' + qz.getPrinter() + '" queue.');
		}
	}
	
	/***************************************************************************
	* Prototype function for finding the "default printer" on the system
	* Usage:
	*    qz.findPrinter();
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function useDefaultPrinter() {
		if (isLoaded()) {
			// Searches for default printer
			qz.findPrinter();
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				// Alert the printer name to user
				var printer = qz.getPrinter();
				alert(printer !== null ? 'Default printer found: "' + printer + '"':
					'Default printer ' + 'not found');
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}
	
	/***************************************************************************
	* Prototype function for printing raw commands directly to the filesystem
	* Usage:
	*    qz.append("\n\nHello world!\n\n");
	*    qz.printToFile("C:\\Users\\Jdoe\\Desktop\\test.txt");
	***************************************************************************/
	function printToFile() {
		if (isLoaded()) {
			// Any printer is ok since we are writing to the filesystem instead
			qz.findPrinter();
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				// Send characters/raw commands to qz using "append"
				// Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
				qz.append("A590,1600,2,3,1,1,N,\"QZ Print Plugin " + qz.getVersion() + " sample.html\"\n");
				qz.append("A590,1570,2,3,1,1,N,\"Testing qz.printToFile() function\"\n");
				qz.append("P1\n");
				
				// Send characters/raw commands to file
				// i.e.  qz.printToFile("\\\\server\\printer");
				//       qz.printToFile("/home/user/test.txt");
				qz.printToFile("C:\\qz-print_test-print.txt");
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}
	
	/***************************************************************************
	* Prototype function for printing raw commands directly to a hostname or IP
	* Usage:
	*    qz.append("\n\nHello world!\n\n");
	*    qz.printToHost("192.168.1.254", 9100);
	***************************************************************************/
	function printToHost() {
		if (isLoaded()) {
			// Any printer is ok since we are writing to a host address instead
			qz.findPrinter();
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				// Send characters/raw commands to qz using "append"
				// Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
				qz.append("A590,1600,2,3,1,1,N,\"QZ Print Plugin " + qz.getVersion() + " sample.html\"\n");
				qz.append("A590,1570,2,3,1,1,N,\"Testing qz.printToHost() function\"\n");
				qz.append("P1\n");
				
				// qz.printToHost(String hostName, int portNumber);
				// qz.printToHost("192.168.254.254");   // Defaults to 9100
				qz.printToHost("192.168.1.254", 9100);
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}
	
	
	
	/***************************************************************************
	* Prototype function for listing all printers attached to the system
	* Usage:
	*    qz.findPrinter('\\{dummy_text\\}');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinters()); };
	***************************************************************************/
	function findPrinters() {
		if (isLoaded()) {
			// Searches for a locally installed printer with a bogus name
			qz.findPrinter('\\{bogus_printer\\}');
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				// Get the CSV listing of attached printers
				var printers = qz.getPrinters().split(',');
				for (i in printers) {
					alert(printers[i] ? printers[i] : 'Unknown');      
				}
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}

	
	
	/***************************************************************************
	* Prototype function for controlling print spooling between pages
	* Usage:
	*    qz.setEndOfDocument('P1,1\r\n');
	*    qz.setDocumentsPerSpool('5');
	*    qz.appendFile('/path/to/file.txt');
	*    window['qzDoneAppending'] = function() { qz.print(); };
	***************************************************************************/     
	function printPages() {
		if (notReady()) { return; }
		
		// Mark the end of a label, in this case  P1 plus a newline character
		// qz-print knows to look for this and treat this as the end of a "page"
		// for better control of larger spooled jobs (i.e. 50+ labels)
		qz.setEndOfDocument('P1,1\r\n');
		
		// The amount of labels to spool to the printer at a time. When
		// qz-print counts this many `EndOfDocument`'s, a new print job will 
		// automatically be spooled to the printer and counting will start
		// over.
		qz.setDocumentsPerSpool("2");
		
		qz.appendFile(getPath() + "misc/epl_multiples.txt");
		
		// Automatically gets called when "qz.appendFile()" is finished.
		window['qzDoneAppending'] = function() {
			// Tell the applet to print.
			qz.print();
			
			// Remove reference to this function
			window['qzDoneAppending'] = null;
		};
	}
	

	/***************************************************************************
	* Prototype function for logging a PostScript printer's capabilites to the
	* java console to expose potentially  new applet features/enhancements. 
	* Warning, this has been known to trigger some PC firewalls
	* when it scans ports for certain printer capabilities.
	* Usage: (identical to appendImage(), but uses html2canvas for png rendering)
	*    qz.setLogPostScriptFeatures(true);
	*    qz.appendHTML("<h1>Hello world!</h1>");
	*    qz.printPS();
	***************************************************************************/ 
	function logFeatures() {
		if (isLoaded()) {
			var logging = qz.getLogPostScriptFeatures();
			qz.setLogPostScriptFeatures(!logging);
			alert('Logging of PostScript printer capabilities to console set to "' + !logging + '"');
		}
	}
	
	/***************************************************************************
	* Prototype function to force Unix to use the terminal/command line for
	* printing rather than the Java-to-CUPS interface.  This will write the 
	* raw bytes to a temporary file, then execute a shell command. 
	* (i.e. lpr -o raw temp_file).  This was created specifically for OSX but 
	* may work on several Linux versions as well.
	*    qz.useAlternatePrinting(true);
	*    qz.append('\n\nHello World!\n\n');
	*    qz.print();
	***************************************************************************/ 
	function useAlternatePrinting() {
		if (isLoaded()) {
			var alternate = qz.isAlternatePrinting();
			qz.useAlternatePrinting(!alternate);
			alert('Alternate CUPS printing set to "' + !alternate + '"');
		}
	}
	
	
	/***************************************************************************
	****************************************************************************
	* *                          HELPER FUNCTIONS                             **
	****************************************************************************
	***************************************************************************/
	
	
	/***************************************************************************
	* Gets the current url's path, such as http://site.com/example/dist/
	***************************************************************************/
	function getPath() {
		var path = window.location.href;
		return path.substring(0, path.lastIndexOf("/")) + "/";
	}
	
	/**
	* Fixes some html formatting for printing. Only use on text, not on tags!
	* Very important!
	*   1.  HTML ignores white spaces, this fixes that
	*   2.  The right quotation mark breaks PostScript print formatting
	*   3.  The hyphen/dash autoflows and breaks formatting  
	*/
	function fixHTML(html) {
		return html.replace(/ /g, "&nbsp;").replace(/’/g, "'").replace(/-/g,"&#8209;"); 
	}
	
	/**
	* Equivelant of VisualBasic CHR() function
	*/
	function chr(i) {
		return String.fromCharCode(i);
	}
	
		</script>
		<?php

    $footerScript=ob_get_contents();
    ob_end_clean();

    return $footerScript;
}
?>
