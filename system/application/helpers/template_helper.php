<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Helper Buat template
 * @param string $jenis admin|voffice|front
 * @param string $view nama file
 * @param array  $data
 */

function template($jenis, $view, $data = array()) {
//    echo $jenis;

    $ci = &get_instance(); //instance objek CI
    $module = $ci->router->fetch_module(); //berada di module apa(ngambil alamat module)
    $theme = $ci->config->item('theme'); //themes yang dipakai apa
    $template = 'template';


    if ($jenis == "admin") {
        $currentTheme = $theme['backend']['name'];
    } elseif ($jenis == "front") {
        $currentTheme = $theme['frontend']['name'];
    } elseif ($jenis == "vo") {
        $currentTheme = $theme['member']['name'];
    }

    $currentPath = 'themes/' . $jenis . '/' . $currentTheme . '/';

    $data['themes_inc'] = base_url() . $currentPath . 'inc/';
    $data['view'] = 'modules/' . $module . "/" . $view; //view yang akan di includekan
    $ci->load->custom_view($currentPath, $template, $data); //at last masukkan semua datanya
}

?>
