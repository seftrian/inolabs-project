<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package   CodeIgniter
 * @author    ExpressionEngine Dev Team
 * @copyright Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license   http://codeigniter.com/user_guide/license.html
 * @link    http://codeigniter.com
 * @since   Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package   CodeIgniter
 * @subpackage  Libraries
 * @category  Libraries
 * @author    ExpressionEngine Dev Team
 * @link    http://codeigniter.com/user_guide/general/controllers.html
 */


class CI_Controller {

  private static $instance;
  protected $log_file='assets/log.txt'; //catatan terakhir diperbaharui
  protected $log_execute_file='system/application/modules/logs'; //daftar file yang siap dieksekusi
  protected $log_current_date;

  /**
   * Constructor
   */
  public function __construct()
  {

    self::$instance =& $this;
                      
    // Assign all the class objects that were instantiated by the
    // bootstrap file (CodeIgniter.php) to local class variables
    // so that CI can run as one big super object.
    foreach (is_loaded() as $var => $class)
    {
      $this->$var =& load_class($class);
    }
    $this->load =& load_class('Loader', 'core');

    $this->load->initialize();
    
    log_message('debug', "Controller Class Initialized");
    $ci=&get_instance();
    $ci->load->library('function_lib');

    //cek apakah tidak ada file log.txt
    /*
    if(!file_exists(FCPATH.$this->log_file) AND trim($this->log_file)!='')
    {
      $myfile = fopen(FCPATH.$this->log_file, "w") or die("Unable to open file!");
      $txt = "/0000-00-00/";
      fwrite($myfile, $txt);
      fclose($myfile);
      //die('not found');
    }
    */

    //cek prefix last update
    $is_exist_config=$ci->function_lib->get_one('configuration_index','site_configuration','configuration_index="engine_last_updated"');
    if(!$is_exist_config)
    {
      //buatkan config jika belum tersedia
      $column=array(
        'configuration_index'=>'engine_last_updated',
        'configuration_value'=>'0000-00-00',
        'configuration_note'=>'Tanggal terakhir diupdate',
      );
      $ci->db->insert('site_configuration',$column);
    }


    //proses update aplikasi
    //if(file_exists(FCPATH.$this->log_file) AND trim($this->log_file)!='')
    //{

     // $myfile = fopen(FCPATH.$this->log_file, "r") or die("Unable to open file!");
     // $log_text=fread($myfile,filesize(FCPATH.$this->log_file));
    //  fclose($myfile);

      //cari prefix date
  //    preg_match_all("/\d{4}+-\d{2}+-+\d{2}/", $log_text, $matches_words);
 //     if(isset($matches_words[0]))
//      {

      //  $data=$matches_words[0];
      //  $current_date=$data[0];
        $current_date=$ci->function_lib->get_one('configuration_value','site_configuration','configuration_index="engine_last_updated"');
        //dapatkan list file
        if(file_exists(FCPATH.$this->log_execute_file) AND trim($this->log_execute_file)!='')
        {
            $log_execute_file_arr=array_diff(scandir(FCPATH.$this->log_execute_file), array('..', '.'));
            if(!empty($log_execute_file_arr))
            {
              foreach($log_execute_file_arr AS $index=>$filename)
              {
                $file_date=str_replace('.php', '', $filename);
                if(strtotime($file_date)>0 AND strtotime($file_date)>=strtotime($current_date))
                {
                  //die('ss');
                  require_once FCPATH.$this->log_execute_file.'/'.$filename;
               
                  if(isset($add_table[$file_date]))
                  {
                    $this->check_table_is_exist($add_table[$file_date],$current_date);
                  }
                  
                  if(isset($add_column[$file_date]))
                  {
                     //kolom
                    $this->check_columns_is_exist($add_column[$file_date],$current_date);
                  }

                  //hapus kolom
                  if(isset($delete_column[$file_date]))
                  {
                    //kolom
                    $this->check_delete_column_is_exist($delete_column[$file_date],$current_date);
                  }

                  if(isset($add_menu[$file_date]))
                  {
                       //$menu_arr=$this->add_menu();
                      $this->check_menu_is_exist($add_menu[$file_date],$current_date);
                  }

                   if(isset($add_notification[$file_date]))
                   {
                      //notifikasi
                      $this->check_notification_is_exist($add_notification[$file_date],$current_date);
                   }
                  
                  if(isset($add_config[$file_date]))
                  {
                    //config
                    $this->check_config_is_exist($add_config[$file_date],$current_date);
                  }

                  if(isset($add_value_on_enum_column[$file_date]))
                  {
                      //add value for column that the type data is enum
                      $this->check_value_on_enum_column_is_exist($add_value_on_enum_column[$file_date],$current_date);
    
                  }

                   if(isset($add_column_type[$file_date]))
                  {
                     //kolom
                    $this->check_columns_type_is_exist($add_column_type[$file_date],$current_date);
                  }

                  /*
                  if (!is_writable(FCPATH.$this->log_file)) {
                      show_error('Cannot change the mode of file ('.FCPATH.$this->log_file.')');
                           //echo "Cannot change the mode of file ($this->dirDefault.$this->fileLog)";
                           exit;
                      if (!chmod(FCPATH.$this->log_file, 0666)) {
                           show_error('Cannot change the mode of file ('.FCPATH.$this->log_file.')');
                          // echo "Cannot change the mode of file ($this->dirDefault.$this->fileLog)";
                           //exit;
                      };
                  }

                  //open file append and add new line date
                  $fh = fopen(FCPATH.$this->log_file, 'r+');
                 // $stringData = '/'.$file_date.'/'."\n".$log_text;
                  $stringData = '/'.$file_date.'/'."\n";
                  fwrite($fh, $stringData);
                  */
                  //update last update
                  $column=array(
                    'configuration_value'=>$file_date,
                  );
                  $where=array(
                    'configuration_index'=>'engine_last_updated',
                  );
                  $ci->db->update('site_configuration',$column,$where);

                }
              }
            }
        }
      
        //echo $log_text;
        //die('found');
     // }
    //}


      //add table
   /* $table_arr=$this->add_table();
    $this->check_table_is_exist($table_arr);*/
    
    //add kolom
    /*$column_arr=$this->add_column();
    $this->check_columns_is_exist($column_arr);
  */

    //add kolom type
    /*$column_arr=$this->add_column_type();
    $this->check_columns_type_is_exist($column_arr);
    */
    
    //add menu
    /*$menu_arr=$this->add_menu();
    $this->check_menu_is_exist($menu_arr);                
    */
    //add notification
    /*$notif_arr=$this->add_notification();
    $this->check_notification_is_exist($notif_arr);
    */

    //add config
   /* $config_arr=$this->add_config();
    $this->check_config_is_exist($config_arr);*/

    //add value for column that the type data is enum
    //$enum_arr=$this->add_value_on_enum_column();
    //$this->check_value_on_enum_column_is_exist($enum_arr);
  
  }

  public static function &get_instance()
  {
    return self::$instance;
  }

    /**
   * daftar config baru
   */
  protected function add_config()
  {
      $my_menu=array(
          array(
          'index'=>'print_trx_non_resep',
          'value'=>'N',
          'note'=>"Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.",
          'expire_date'=>'2015-03-27',//yyyy-mm-dd        
          ),
          array(
          'index'=>'print_trx_pos',
          'value'=>'N',
          'note'=>"Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),
          array(
          'index'=>'change_item_price',
          'value'=>'N',
          'note'=>"Y:dapat merubah harga jual;N:harga jual tidak dapat diubah. Digunakan saat transaksi penjualan.",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),
          array(
          'index'=>'config_sale_ppn',
          'value'=>'0',
          'note'=>"Nilai default PPN di transaksi penjualan.",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),
          array(
          'index'=>'config_sale_tuslah',
          'value'=>'0',
          'note'=>"Nilai default Tuslah di transaksi penjualan.",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),
          array(
          'index'=>'config_sale_embalage',
          'value'=>'0',
          'note'=>"Nilai default Embalage di transaksi penjualan.",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),

      );
      return $my_menu;
  }

  /**
  pengecekan config
  @param array $column_arr
  */
  protected function check_config_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
      //$today=date('Y-m-d');
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {
              foreach($rowArr AS $variable=>$value_variable)
              {
                  ${$variable}=$value_variable;
              }

              if(isset($index) AND trim($index)!='' AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist='
                    SELECT COUNT(*) AS num
                    FROM site_configuration 
                    WHERE configuration_index ="'.$index.'"
                  ';
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();
               
                  if($row['num']==0 AND trim($value)!='')
                  {
                      $column=array(
                        'configuration_index'=>$index,
                        'configuration_value'=>$value,
                        'configuration_note'=>$note,
                      );    
                      $ci->db->insert('site_configuration',$column);   
                  }
              }
          }
      }
  }

  /**
   * daftar notifikasi baru
   */
  protected function add_notification()
  {
      $my_menu=array(
          /*array(
          'name'=>'Retur Supplier',
          'text'=>"New Fitur! Inventory -> Retur Supplier",
          'expire_date'=>'2015-02-27',//yyyy-mm-dd        
          ),
          */
        
          array(
          'name'=>'POS',
          'text'=>"New Fitur! POS -> POS",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),
          array(
          'name'=>'Laporan Transaksi',
          'text'=>"New Fitur! Lap. Penjualan -> Transaksi",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),
          array(
          'name'=>'Stok Tertahan',
          'text'=>"New Fitur! Inv. Lokal -> Stok Tertahan",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),
          array(
          'name'=>'Item Kit',
          'text'=>"New Fitur! Utility -> Item Kit",
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
          ),

        );

      return $my_menu;
      
  }

  /**
  pengecekan menu
  @param array $column_arr
  */
  protected function check_notification_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
     // $today=date('Y-m-d');
      //echo '<pre>';
      //print_r($column_arr);
      //exit;
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {
              foreach($rowArr AS $variable=>$value)
              {
                  ${$variable}=$value;
              }

              if(isset($name) AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist='
                    SELECT COUNT(*) AS num
                    FROM notification_list 
                    WHERE notification_list_title LIKE "%'.$name.'%"
                  ';
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();
               
                  if($row['num']==0 AND trim($text)!='')
                  {
                      //dapatkan data superuser
                      $sql_user='SELECT admin_username FROM site_administrator sa
                                WHERE sa.admin_group_id IN (
                                      SELECT sag.admin_group_id FROM site_administrator_group sag WHERE 
                                      admin_group_type="superuser")
                                GROUP BY admin_username';
                      $exec=$ci->db->query($sql_user);  
                      $results=$exec->result_array();
                      if(!empty($results))
                      {
                        foreach($results AS $rowArr)
                        {
                            $admin_username=$rowArr['admin_username'];
                            $column=array(
                              'notification_list_admin_username'=>$admin_username,
                              'notification_list_title'=>$name,
                              'notification_list_text'=>$text,
                              );
                            //save
                            $ci->db->insert('notification_list',$column); 
                        }
                      }       
                  }
              }
          }
      }
  }

  /**
   * daftar menu baru
   */
  protected function add_menu()
  {
      $my_menu=array(
      array(
          'name'=>'Audit Trail',
          'query'=>"
          INSERT INTO `site_administrator_menu` (`menu_administrator_id`, `menu_administrator_par_id`, `menu_administrator_title`, `menu_administrator_description`, `menu_administrator_link`, `menu_administrator_page_id`, `menu_administrator_order_by`, `menu_administrator_location`, `menu_administrator_is_active`, `menu_administrator_module`, `menu_administrator_controller`, `menu_administrator_method`, `menu_administrator_class_css`) VALUES
          (8, 11, 'Group', 'untuk konfigurasi group dan privilege group', 'admin/admin_group/index', 0, 1, 'admin', '1', 'admin_group', 'admin', 'index#create#update#set_privilege#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
          (9, 11, 'User', 'Untuk memanajemen user/admin', 'admin/admin_user/index', 0, 2, 'admin', '1', 'admin_user', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
          (10, 11, 'Menu Admin', 'Untuk memanajemen menu', 'admin/admin_menu/index', 0, 4, 'admin', '0', 'admin_menu', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
          (11, 0, 'Settings', 'settings group, user dan menu', '', 0, 23, 'admin', '1', '-', '-', '-', 'clip-cog-2'),
          (12, 11, 'Config', '', 'admin/admin_config/view', 0, 6, 'admin', '1', 'admin_config', 'admin', 'update#view', ''),
          (13, 11, 'Menu Visitor', 'menu visitor', 'admin/admin_menu/index?location=user', 0, 5, 'admin', '0', 'admin_menu', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
          (24, 0, 'Dashboard', 'dashboard', 'admin/dashboard/index', 0, 1, 'admin', '1', 'dashboard', 'admin', 'index', 'clip-home-3'),
          (46, 11, 'Privelege Module Label', '', 'admin/admin_group_menu_label/index', 0, 3, 'admin', '0', 'admin_group_menu_label', 'admin', 'index#index_filter#ign_index_filter', ''),
          (48, 0, 'Tracer', '', 'tracer/tracer_auth/index', 0, 2, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
          (52, 48, 'sdf', 's', 'sf', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', NULL),
          (53, 0, 'Data Master', '-', '#', 0, 4, 'admin', '1', '-', '-', '-', 'clip-grid-5'),
          (54, 53, 'Asuransi', '', 'admin/insurance/index', 0, 1, 'admin', '1', 'insurance', 'admin', 'index#add#update#', ''),
          (55, 53, 'Jasa Apoteker', '', 'admin/jasa_apoteker/index', 0, 2, 'admin', '1', 'jasa_apoteker', 'admin', 'index#add#update#', ''),
          (56, 53, 'Pabrik', '-', 'admin/pabrik/index', 0, 3, 'admin', '1', 'pabrik', 'admin', 'index#add#update#', ''),
          (57, 53, 'Supplier', '', 'admin/supplier/index', 0, 4, 'admin', '1', 'supplier', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (58, 53, 'People', 'Kumpulan data master dokter, apoteker, karyawan, dan lainnya yang berkaitan dengan kependudukan.', 'admin/people/index', 0, 5, 'admin', '1', 'people', 'admin', 'index_person#index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (59, 53, 'Pasien', '', 'admin/pasien/index', 0, 6, 'admin', '0', 'pasien', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (60, 53, 'Apoteker', '', 'admin/apoteker/index', 0, 7, 'admin', '0', 'apoteker', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (61, 53, 'Asisten Apoteker', '', 'admin/asisten_apoteker/index', 0, 8, 'admin', '0', 'asisten_apoteker', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (62, 53, 'Pelanggan', '', 'admin/pelanggan/index', 0, 9, 'admin', '0', 'pelanggan', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (63, 53, 'Data Barang', '', 'admin/item/index', 0, 10, 'admin', '1', 'item', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (64, 0, 'Penjualan Barang', '', 'admin/trx_pos/index', 0, 2, 'admin', '1', 'trx_pos', 'admin', 'index#index_detail#option_insurance#add#add_payment#add_card_payment#payment_returns#add_dokter#load_dokter#add_pasien#load_pasien#load_item#load_item_resep#print_trx#print_kwitansi#print_etiket#load_suspend_trx#load_all_data_trx#report#report_piutang#report_item_sale#delete_transaction#export_data#', 'clip-cart'),
          (65, 64, 'Non Resep', '-', 'admin/trx_non_resep/index', 0, 1, 'admin', '0', 'trx_non_resep', 'admin', 'index#add#add_payment#add_card_payment#payment_returns#add_person#load_person#load_item#print_trx#load_suspend_trx#load_all_data_trx', ''),
          (67, 0, 'Inventory Lokal', '', '', 0, 5, 'admin', '1', '-', '-', '-', 'clip-database'),
          (68, 67, 'Pemesanan', '', 'admin/pemesanan/index', 0, 2, 'admin', '1', 'pemesanan', 'admin', 'index#add#add_supplier#load_supplier#load_item#modal_information#print_trx#load_suspend_trx#load_all_data_trx', ''),
          (69, 67, 'Penerimaan', '', 'admin/penerimaan/index', 0, 6, 'admin', '1', 'penerimaan', 'admin', 'index#add#load_item#add_supplier#load_supplier#modal_information#load_all_data_trx#load_all_data_po_trx', ''),
          (71, 110, 'Pemesanan', '', 'admin/pemesanan/report', 0, 1, 'admin', '1', 'pemesanan', 'admin', 'report#export_data#', ''),
          (73, 110, 'Penerimaan', '', 'admin/penerimaan/report', 0, 3, 'admin', '1', 'penerimaan', 'admin', 'report#export_data#', ''),
          (75, 116, 'Non Resep', '', 'admin/trx_non_resep/report', 0, 1, 'admin', '0', 'trx_non_resep', 'admin', 'report#export_data#', ''),
          (76, 0, 'Import Data', '', '', 0, 18, 'admin', '1', '-', '-', '-', ' clip-upload'),
          (77, 76, 'Barang', '', 'admin/import_item/index', 0, 1, 'admin', '1', 'import_item', 'admin', 'index#preview_import#saving_data#', ''),
          (82, 64, 'Resep', '', 'admin/trx_resep/index', 0, 3, 'admin', '0', 'trx_resep', 'admin', 'index#add#add_payment#add_card_payment#payment_returns#add_dokter#load_dokter#add_pasien#load_pasien#load_item#print_trx#load_suspend_trx#load_all_data_trx', ''),
          (83, 67, 'Stock Opname', '', 'admin/stock_opname/index', 0, 10, 'admin', '1', 'stock_opname', 'admin', 'index#add#load_item#modal_information#load_all_data_trx', ''),
          (85, 117, 'Stock Opname', '', 'admin/stock_opname/report', 0, 4, 'admin', '1', 'stock_opname', 'admin', '#report#export_data#', ''),
          (86, 76, 'Stock Opname', '', 'admin/import_stock_opname/index', 0, 2, 'admin', '1', 'import_stock_opname', 'admin', 'index#preview_import#saving_data#', ''),
          (87, 0, 'Pembayaran', '', 'admin/report/payment', 0, 3, 'admin', '1', 'report', 'admin', 'payment#payment_cash#payment_card#export_payment_data#export_payment_cash_data#export_payment_card_data#', ' clip-file'),
          (88, 110, 'Pembayaran', '', 'admin/report/payment', 0, 8, 'admin', '1', 'report', 'admin', 'payment#export_payment_data', ''),
          (89, 110, 'Pembayaran Tunai', '', 'admin/report/payment_cash', 0, 9, 'admin', '0', 'report', 'admin', 'payment_cash#export_payment_cash_data', ''),
          (90, 110, 'Pembayaran Kartu', '', 'admin/report/payment_card', 0, 10, 'admin', '0', 'report', 'admin', 'payment_card#export_payment_card_data#', ''),
          (92, 116, 'Resep', '', 'admin/trx_resep/report', 0, 2, 'admin', '0', 'trx_resep', 'admin', '#report#export_data#', ''),
          (93, 53, 'Mesin EDC', '', 'admin/edc_machine/index', 0, 11, 'admin', '1', 'edc_machine', 'admin', 'index#add#update#', ''),
          (94, 53, 'Unit / Cabang', '', 'admin/store/index', 0, 12, 'admin', '1', 'store', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (95, 119, 'Distribusi Unit/Cabang', '', 'admin/distribusi_unit/index', 0, 1, 'admin', '1', 'distribusi_unit', 'admin', 'index#add#load_item#modal_information#print_trx#load_suspend_trx#load_store#load_all_data_trx', ''),
          (96, 119, 'Penerimaan Unit/Cabang', '', 'admin/reception_unit/index', 0, 2, 'admin', '1', 'reception_unit', 'admin', 'index#add#load_item#add_supplier#load_supplier#modal_information#load_all_data_trx#load_all_data_po_trx', ''),
          (98, 117, 'Summary Stok', '', 'admin/stok_all/index', 0, 1, 'admin', '1', 'stok_all', 'admin', 'index#export_data', ''),
          (99, 117, 'Keluar Masuk', '', 'admin/stok_all/detail', 0, 2, 'admin', '1', 'stok_all', 'admin', 'detail#export_detail_data', ''),
          (100, 97, 'Item Belum Expired', '', 'admin/stok_all/arus_stok_sisa', 0, 3, 'admin', '0', 'stok_all', 'admin', 'arus_stok_sisa#export_arus_stok_sisa_data', ''),
          (101, 117, 'By ED & Batch', '', 'admin/stok_all/arus_stok_history', 0, 3, 'admin', '1', 'stok_all', 'admin', 'arus_stok_history#export_arus_stok_history_data#', ''),
          (102, 97, 'Item Kadaluarsa', '', 'admin/stok_all/expired', 0, 5, 'admin', '0', 'stok_all', 'admin', 'expire#export_expired_data', ''),
          (103, 119, 'Retur Unit/Cabang', '', 'admin/reception_retur_unit/index', 0, 3, 'admin', '1', 'reception_retur_unit', 'admin', 'index#add#load_item#add_supplier#load_supplier#modal_information#load_all_data_trx#load_all_data_po_trx', ''),
          (104, 87, 'Inkaso', '', 'admin/inkaso/index', 0, 4, 'admin', '1', 'inkaso', 'admin', 'index#pay#history', ''),
          (105, 87, 'Piutang Penjualan', '', 'admin/billing_transaction/index', 0, 5, 'admin', '1', 'billing_transaction', 'admin', 'index#manage_bill#option_insurance#add_card_payment#modal_information#', ''),
          (106, 116, 'Laba / Rugi', '', 'admin/report_all/income', 0, 3, 'admin', '0', 'report_all', 'admin', 'income#export_income_data#', ''),
          (108, 118, 'Non Resep', 'laporan piutang penjualan non resep', 'admin/piutang/non_resep', 0, 1, 'admin', '0', 'piutang', 'admin', 'non_resep#export_piutang_data_non_resep', ''),
          (109, 118, 'Resep', '', 'admin/piutang/resep', 0, 2, 'admin', '0', 'piutang', 'admin', 'resep#export_piutang_data_resep#', ''),
          (110, 0, 'Lap. Umum', 'Untuk mengelola laporan transaksi', '#', 0, 7, 'admin', '1', '-', '-', '-', ' clip-file-excel'),
          (111, 110, 'Resep Dokter', '', 'admin/resep_dokter/', 0, 11, 'admin', '1', 'resep_dokter', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
          (112, 110, 'Probabilitas', '', 'admin/probabilitas/', 0, 12, 'admin', '1', 'probabilitas', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
          (113, 67, 'Defecta', '', 'admin/defecta/index', 0, 1, 'admin', '1', 'defecta', 'admin', 'index#act_add_to_plan#', ''),
          (114, 67, 'Retur Supplier', '', 'admin/retur_supplier/index', 0, 11, 'admin', '1', 'retur_supplier', 'admin', 'index#add#load_item#add_supplier#load_supplier#modal_information#load_all_data_trx#load_all_data_po_trx#report#delete_transaction#', ''),
          (115, 11, 'Printer', '', 'admin/master_printer/index', 0, 7, 'admin', '1', 'master_printer', 'admin', 'index#', ''),
          (116, 0, 'Lap. Penjualan', '', '#', 0, 8, 'admin', '1', '-', '-', '-', 'clip-file-excel'),
          (117, 0, 'Lap. Stok', 'Laporan summary stok, keluar masuk stok, stok opname.', '#', 0, 9, 'admin', '1', '-', '-', '-', 'clip-file-excel'),
          (118, 0, 'Lap. Piutang', 'Laporan piutang penjualan', '#', 0, 10, 'admin', '0', '-', '-', '-', 'clip-file-excel'),
          (119, 0, 'Inventory Unit', 'Inventory cabang/unit', '#', 0, 6, 'admin', '1', '-', '-', '-', 'fa fa-cloud'),
          (120, 53, 'Data Barang v2', 'data barang dengan formula dari apotek 9eighteen', 'admin/item/9eighteen/index', 0, 13, 'admin', '1', 'item', 'admin_9eighteen', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
          (121, 67, 'Pemesanan v2', '', 'admin/pemesanan/9eighteen/index', 0, 5, 'admin', '1', 'pemesanan', 'admin_9eighteen', 'index#add#add_supplier#load_supplier#load_item#modal_information#print_trx#load_suspend_trx#load_plan_trx#load_all_data_trx#delete_transaction', ''),
          (122, 67, 'Penerimaan v2', 'menggunakan harga jual pabrik untuk menghitung HNA', 'admin/penerimaan/9eighteen/index', 0, 9, 'admin', '1', 'penerimaan', 'admin_9eighteen', 'index#add#load_item#add_supplier#load_supplier#modal_information#load_all_data_trx#load_all_data_po_trx#delete_transaction#', ''),
          (123, 110, 'Pemesanan v2', '', 'admin/pemesanan/9eighteen/report', 0, 2, 'admin', '1', 'pemesanan', 'admin_9eighteen', 'report#export_data', ''),
          (124, 110, 'Penerimaan v2', '', 'admin/penerimaan/9eighteen/report', 0, 4, 'admin', '1', 'penerimaan', 'admin_9eighteen', 'report#export_data#', ''),
          (125, 0, 'Akuntansi', '', '', 0, 12, 'admin', '0', '-', '-', '-', 'fa fa-money'),
          (126, 125, 'Akun', '', 'admin/coa/index', 0, 1, 'admin', '1', 'coa', 'admin', 'index#add#update#', ''),
          (127, 125, 'Template Jurnal', '', 'admin/journal_auto/index', 0, 2, 'admin', '1', 'journal_auto', 'admin', 'index#create#', ''),
          (128, 125, 'Jurnal Umum', '', 'admin/journal/index', 0, 3, 'admin', '1', 'journal', 'admin', 'index#add#', ''),
          (129, 125, 'Set Template Jurnal', '', 'admin/journal_template/index', 0, 4, 'admin', '1', 'journal_template', 'admin', 'index#add#update#', ''),
          (130, 0, 'Utility', '', '#', 0, 12, 'admin', '1', '-', '-', '-', 'clip-paperplane'),
          (131, 130, 'Pembelian Pelanggan', '', 'admin/reminder_customer/index', 0, 1, 'admin', '1', 'reminder_customer', 'admin', 'index#show_order#', ''),
          (132, 67, 'Pemusnahan Obat', '', 'admin/destruction_drugs/index', 0, 12, 'admin', '1', 'destruction_drugs', 'admin', 'index#add#load_item#modal_information#print_trx#delete_transaction#', ''),
          (133, 130, 'Item Kit', 'penggabungan item', 'admin/item_kit/index', 0, 2, 'admin', '1', 'item_kit', 'admin', 'index#add#load_item#print_trx#add_supplier#load_supplier#modal_information#load_all_data_trx#load_all_data_po_trx#report#delete_transaction#export_data#', ''),
          (134, 130, 'Penerimaan Item Kit', '', 'admin/penerimaan_kit/index', 0, 3, 'admin', '1', 'penerimaan_kit', 'admin', 'index#add#load_item#print_trx#modal_information#load_all_data_trx#report#delete_transaction#export_data#', ''),
          (135, 130, 'Opname Kit', '', 'admin/stock_opname_kit/index', 0, 4, 'admin', '1', 'stock_opname_kit', 'admin', 'index#add#load_item#print_trx#modal_information#load_all_data_trx#report#delete_transaction#export_data#', ''),
          (136, 64, 'POS', '', 'admin/trx_pos/index', 0, 4, 'admin', '0', 'trx_pos', 'admin', 'index#option_insurance#add#add_payment#add_card_payment#payment_returns#add_dokter#load_dokter#add_pasien#load_pasien#load_item#load_item_resep#print_trx#print_kwitansi#print_etiket#load_suspend_trx#load_all_data_trx#report#delete_transaction#export_data#', ''),
          (137, 116, 'Transaksi', '', 'admin/trx_pos/report', 0, 4, 'admin', '1', 'trx_pos', 'admin', 'report', ''),
          (138, 116, 'Piutang', '', 'admin/trx_pos/report_piutang', 0, 5, 'admin', '1', 'trx_pos', 'admin', 'report_piutang', ''),
          (139, 67, 'Stok Tertahan', '', 'admin/stok_all/pending_stock', 0, 13, 'admin', '1', 'stok_all', 'admin', 'pending_stock', ''),
          (140, 116, 'Barang Terjual', '', 'admin/trx_pos/report_item_sale', 0, 6, 'admin', '1', 'trx_pos', 'admin', 'report_item_sale', ''),
          (141, 0, 'Audit Trail', 'Mencatat semua kegiatan yang dilakukan tiap user', '#', 0, 12, 'admin', '1', '-', '-', '-', 'fa fa-clock-o'),
          (142, 141, 'Penjualan', '', 'admin/at_pos/index', 0, 1, 'admin', '1', 'at_pos', 'admin', 'index#history#', '');
          ",
          'query_first'=>'TRUNCATE site_administrator_menu;',//query dijalankan awal sebelum query ini dieksekusi
          'expire_date'=>'2015-12-31',//yyyy-mm-dd        
        ),
        

        );

      return $my_menu;
      
  }

  /**
  pengecekan menu
  @param array $column_arr
  */
  protected function check_menu_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
     // $today=date('Y-m-d');
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {
              foreach($rowArr AS $variable=>$value)
              {
                  ${$variable}=$value;
              }

              if(isset($name) AND isset($query) AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist='
                    SELECT COUNT(*) AS num
                    FROM site_administrator_menu 
                    WHERE menu_administrator_title LIKE "'.$name.'"
                  ';
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();

                  if($row['num']==0 AND trim($query)!='')
                  {
                      if(isset($rowArr['query_first']) AND trim($rowArr['query_first'])!='')
                      {
                         $ci->db->query($rowArr['query_first']); 
                      }
                      $ci->db->query($query); 
                  }
              }
          }
      }
  }

  /**
   * daftar table baru
   */
  protected function add_table()
  {
      $myTable=array(
        
        /*  array(
            'table'=>'accounting_coa',
            'query'=>"
            CREATE TABLE IF NOT EXISTS `accounting_coa` (
              `coa_id` int(9) NOT NULL AUTO_INCREMENT,
              `coa_par_id` int(9) NOT NULL,
              `coa_code` varchar(20) NOT NULL,
              `coa_name` varchar(255) NOT NULL,
              `coa_type` varchar(30) NOT NULL,
              `coa_dbcr` enum('debit','credit') NOT NULL DEFAULT 'debit',
              `coa_level` int(11) unsigned NOT NULL DEFAULT '0',
              `coa_report` enum('Y','N') NOT NULL DEFAULT 'Y',
              `coa_is_neraca` enum('Y','N') NOT NULL DEFAULT 'N',
              `coa_is_laba_rugi` enum('Y','N') NOT NULL DEFAULT 'N',
              PRIMARY KEY (`coa_id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;",
            'expire_date'=>'2015-12-31',//yyyy-mm-dd    
          ),*/ 

        );

      return $myTable;
      
  }

   /**
  pengecekan kolom
  @param array $column_arr
  */
  protected function check_table_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
     // $today=date('Y-m-d');
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {

              foreach($rowArr AS $variable=>$value)
              {
                  ${$variable}=$value;
              }

              if(isset($table) AND isset($query) AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist='
                  SELECT COUNT(*) AS num
                    FROM information_schema.tables 
                    WHERE TABLE_SCHEMA = "'.$ci->db->database.'"
                    AND TABLE_NAME = "'.$table.'"
                     ';
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();
 
                  if($row['num']==0)
                  {
                      $ci->db->query($query); 

                      //eksekusi data
                      if(isset($rowArr['query_data']) AND trim($rowArr['query_data'])!='')
                      {
                          if(isset($rowArr['query_first']) AND trim($rowArr['query_first'])!='')
                          {
                            $ci->db->query($rowArr['query_first']); 
                          }
                          $ci->db->query($rowArr['query_data']); 
                      }

                  }


              }
          }
      }
  }

  /**
  query penghapusan kolom
  */
  protected function delete_column()
  {
      $column=array(
        /*
          array(
              'table'=>'sys_retur_supplier_detail',
              'column'=>'return_supplier_detail_dp',
              'query'=>"ALTER TABLE `sys_retur_supplier_detail` ADD `return_supplier_detail_dp` DECIMAL(4,2) NULL , ADD `return_supplier_detail_dr` DECIMAL(15,2) NULL , ADD `return_supplier_detail_old_hna` DECIMAL(15,2) NULL , ADD `return_supplier_detail_factory_price` DECIMAL(15,2) NULL , ADD `return_supplier_detail_hna` DECIMAL(15,2) NULL , ADD `return_supplier_detail_subtotal` DECIMAL(15,2) NULL ;",    
              'expire_date'=>'2015-09-30', //kadaluarsa untuk dieksekusi
              'query_first'=>'QUERY SQL', //query di eksekusi sebelum query inti
              'query_end'=>'QUERY SQL', //query di eksekusi setelah query inti
          ),
          */          
       );
      return $column;
  }

  /**
  pengecekan kolom
  @param array $column_arr
  */
  protected function check_delete_column_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
     // $today=date('Y-m-d');
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {
              foreach($rowArr AS $variable=>$value)
              {
                  ${$variable}=$value;
              }

              if(isset($table) AND isset($column) AND isset($query) AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist='SELECT * 
                          FROM information_schema.COLUMNS 
                          WHERE 
                          TABLE_SCHEMA ="'.$ci->db->database.'" 
                          AND TABLE_NAME = "'.$table.'"
                          AND COLUMN_NAME = "'.$column.'"';
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();
                  
                  if(!empty($row))
                  {
                      if(isset($rowArr['query_first']) AND trim($rowArr['query_first'])!='')
                      {
                         $ci->db->query($rowArr['query_first']); 
                      }
                     
                      $ci->db->query($query); 

                      if(isset($rowArr['query_end']) AND trim($rowArr['query_end'])!='')
                      {
                         $ci->db->query($rowArr['query_end']); 
                      }
                  }
              }
          }
      }
  }

  /**
  query penambahan kolom
  */
  protected function add_column()
  {
      $column=array(
        /*
          array(
              'table'=>'sys_retur_supplier_detail',
              'column'=>'return_supplier_detail_dp',
              'query'=>"ALTER TABLE `sys_retur_supplier_detail` ADD `return_supplier_detail_dp` DECIMAL(4,2) NULL , ADD `return_supplier_detail_dr` DECIMAL(15,2) NULL , ADD `return_supplier_detail_old_hna` DECIMAL(15,2) NULL , ADD `return_supplier_detail_factory_price` DECIMAL(15,2) NULL , ADD `return_supplier_detail_hna` DECIMAL(15,2) NULL , ADD `return_supplier_detail_subtotal` DECIMAL(15,2) NULL ;",    
              'expire_date'=>'2015-09-30', //kadaluarsa untuk dieksekusi
          ),
          */          
       );
      return $column;
  }

  /**
  pengecekan kolom
  @param array $column_arr
  */
  protected function check_columns_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
     // $today=date('Y-m-d');
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {
              foreach($rowArr AS $variable=>$value)
              {
                  ${$variable}=$value;
              }

              if(isset($table) AND isset($column) AND isset($query) AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist='SELECT * 
                          FROM information_schema.COLUMNS 
                          WHERE 
                              TABLE_SCHEMA ="'.$ci->db->database.'" 
                          AND TABLE_NAME = "'.$table.'"
                          AND COLUMN_NAME = "'.$column.'"';
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();
                  if(empty($row))
                  {
                      if(isset($rowArr['query_first']) AND trim($rowArr['query_first'])!='')
                      {
                         $ci->db->query($rowArr['query_first']); 
                      }

                      $ci->db->query($query); 

                      if(isset($rowArr['query_end']) AND trim($rowArr['query_end'])!='')
                      {
                         $ci->db->query($rowArr['query_end']); 
                      }
                  }
              }
          }
      }
  }

  /**
  query penambahan kolom
  */
  protected function add_column_type()
  {
      $column=array(
           array(
              'table'=>'trx_item_stock_log',
              'column'=>'item_stock_log_item_nama',
              'select'=>'CHARACTER_MAXIMUM_LENGTH',
              'new_value'=>100,
              'query'=>"ALTER TABLE `trx_item_stock_log` CHANGE `item_stock_log_item_nama` `item_stock_log_item_nama` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;",    
              'expire_date'=>'2015-12-31', //kadaluarsa untuk dieksekusi
          ),

     //
       );
      return $column;
  }

  /**
  pengecekan kolom
  @param array $column_arr
  */
  protected function check_columns_type_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
    //  $today=date('Y-m-d');
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {
              foreach($rowArr AS $variable=>$value)
              {
                  ${$variable}=$value;
              }

              if(isset($table) AND isset($column) AND isset($query) AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist="
                    SELECT ".$select." AS old_value from information_schema.columns where table_schema = '".$ci->db->database."' 
                    AND 
                    table_name = '".$table."' AND 
                    COLUMN_NAME = '".$column."'
                  ";
                
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();

                  if(!empty($row) AND $row['old_value']!=$new_value)
                  {
                      $ci->db->query($query); 
                  }
              }
          }
      }
  }

   /**
  tambahkan value pada tipe enum
  */
  protected function add_value_on_enum_column()
  {
      $column=array(
        
          array(
              'table'=>'trx_item_stock_log',
              'column'=>'item_stock_log_type',
              'select'=>'CHARACTER_MAXIMUM_LENGTH',
              'new_value'=>'27',
              'query'=>"ALTER TABLE `trx_item_stock_log` CHANGE `item_stock_log_type` `item_stock_log_type` ENUM('11','12','13','21','22','23','24','25','26','27','31','41') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '11:penerimaan barang; 12:penerimaan distribusi unit; 13:penerimaan retur unit; 21:penjualan non resep; 22:penjualan resep; 23:pengeluaran distribusi unit; 24:return supplier; 25:return unit; 26:pemusnahan; 27:item kit; 31:stock adjusment; 41:stock opname;';  ",    
              'expire_date'=>'2015-12-31', //kadaluarsa untuk dieksekusi
          ),

          array(
              'table'=>'trx_item_stock_balance',
              'column'=>'item_stock_balance_item_barcode',
              'select'=>'CHARACTER_MAXIMUM_LENGTH',
              'new_value'=>'300',
              'query'=>"ALTER TABLE `trx_item_stock_balance` CHANGE `item_stock_balance_item_barcode` `item_stock_balance_item_barcode` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `item_stock_balance_item_no_batch` `item_stock_balance_item_no_batch` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `item_stock_balance_item_nama` `item_stock_balance_item_nama` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `item_stock_balance_item_satuan_name` `item_stock_balance_item_satuan_name` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL; ",    
              'expire_date'=>'2015-12-31', //kadaluarsa untuk dieksekusi
          ),

       

          //
     //
       );
      return $column;
  }

  /**
  pengecekan kolom
  @param array $column_arr
  */
  protected function check_value_on_enum_column_is_exist($column_arr,$today)
  {
      $ci=&get_instance();
      //$today=date('Y-m-d');
      if(!empty($column_arr))
      {
          foreach($column_arr AS $rowArr)
          {
              foreach($rowArr AS $variable=>$value)
              {
                  ${$variable}=$value;
              }

              if(isset($table) AND isset($column) AND isset($query) AND strtotime($today)<=strtotime($expire_date))
              {
                  //execute
                  //check is exist
                  $check_is_exist=" SHOW COLUMNS FROM ".$table." LIKE '".$column."'";
                  $execute=$ci->db->query($check_is_exist);
                  $row=$execute->row_array();
                  if(!empty($row))
                  {
                    $type_value=isset($row['Type'])?$row['Type']:'';
                    //Get the values
                    $values_arr = explode("','", preg_replace("/(enum)\('(.+?)'\)/","\\2", $type_value) );
                    $array_key=array_search($new_value,$values_arr);
                    $key_exist=(trim($array_key)!='' AND isset($values_arr[$array_key]))?$values_arr[$array_key]:'';
                    
                    if($key_exist=='')
                    {
                      $ci->db->query($query); 
                    }
                  }
              }
          }
      }
  }
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */