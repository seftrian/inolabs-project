-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2018 at 10:42 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inolabs_sks`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_token`
--

CREATE TABLE `api_token` (
  `token_id` int(10) UNSIGNED NOT NULL,
  `token_owner_id` int(10) UNSIGNED NOT NULL,
  `token_value` varchar(50) NOT NULL,
  `token_expire_datetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_customer`
--

CREATE TABLE `ecom_customer` (
  `customer_id` int(10) UNSIGNED NOT NULL,
  `customer_person_id` int(10) UNSIGNED NOT NULL,
  `customer_password` varchar(255) NOT NULL,
  `customer_email_offer` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data member e-commerce';

-- --------------------------------------------------------

--
-- Table structure for table `ecom_medical_detail`
--

CREATE TABLE `ecom_medical_detail` (
  `medical_detail_id` int(10) UNSIGNED NOT NULL,
  `medical_detail_medical_header_id` int(10) UNSIGNED NOT NULL,
  `medical_detail_medical_master_person_id` int(10) UNSIGNED NOT NULL,
  `medical_detail_order_by` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_medical_header`
--

CREATE TABLE `ecom_medical_header` (
  `medical_header_id` int(10) UNSIGNED NOT NULL,
  `medical_header_person_id` int(10) UNSIGNED NOT NULL COMMENT 'data pemesan',
  `medical_header_position_id` int(10) UNSIGNED NOT NULL,
  `medical_header_position_title` varchar(200) NOT NULL,
  `medical_header_note_pasien` text NOT NULL,
  `medical_header_qty` int(5) UNSIGNED DEFAULT NULL,
  `medical_header_unit` enum('hour','day','month') DEFAULT NULL,
  `medical_header_status` enum('unread','read','confirmed','rejected') NOT NULL DEFAULT 'unread',
  `medical_header_update_by` varchar(100) DEFAULT NULL,
  `medical_header_last_update_datetime` datetime DEFAULT NULL,
  `medical_header_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_payment`
--

CREATE TABLE `ecom_payment` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `payment_transaction_id` int(10) UNSIGNED NOT NULL,
  `payment_termin_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_from_bank` varchar(255) NOT NULL,
  `payment_to_bank` varchar(255) NOT NULL,
  `payment_value` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `payment_date` date NOT NULL,
  `payment_note` varchar(255) DEFAULT NULL,
  `payment_status` enum('pending','approve','reject') NOT NULL DEFAULT 'pending',
  `payment_input_datetime` datetime NOT NULL,
  `payment_response_administrator_username` varchar(50) DEFAULT NULL,
  `payment_response_note` varchar(255) DEFAULT NULL,
  `payment_response_datetime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data konfirmasi pembayaran transaksi';

-- --------------------------------------------------------

--
-- Table structure for table `ecom_transaction`
--

CREATE TABLE `ecom_transaction` (
  `transaction_id` int(10) UNSIGNED NOT NULL,
  `transaction_medical_header_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'hanya untuk transaksi perawat',
  `transaction_type` enum('service','product') NOT NULL DEFAULT 'service',
  `transaction_code` varchar(20) NOT NULL,
  `transaction_person_id` int(10) UNSIGNED NOT NULL COMMENT 'id pengguna jasa',
  `transaction_total_price` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `transaction_total_discount` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `transaction_total_cash_back` decimal(15,2) DEFAULT NULL,
  `transaction_total_due` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `transaction_note` varchar(255) DEFAULT NULL,
  `transaction_recipient_name` varchar(50) NOT NULL,
  `transaction_recipient_address` varchar(255) NOT NULL,
  `transaction_recipient_phone` varchar(50) NOT NULL,
  `transaction_status` enum('pending','confirmed','paid','sent','received','canceled') NOT NULL DEFAULT 'pending',
  `transaction_datetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data transaksi e-commerce';

-- --------------------------------------------------------

--
-- Table structure for table `ecom_transaction_detail`
--

CREATE TABLE `ecom_transaction_detail` (
  `transaction_detail_id` int(10) UNSIGNED NOT NULL,
  `transaction_detail_transaction_id` int(10) UNSIGNED NOT NULL,
  `transaction_detail_product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'untuk produk',
  `transaction_detail_product_code` varchar(20) DEFAULT NULL,
  `transaction_detail_product_title` varchar(100) DEFAULT NULL COMMENT 'untuk produk',
  `transaction_detail_position_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'untuk posisi perawat, transaksi ini saat followup',
  `transaction_detail_position_title` varchar(100) DEFAULT NULL COMMENT 'untuk posisi perawat, transaksi ini saat followup',
  `transaction_detail_cash_back` decimal(15,2) NOT NULL DEFAULT '0.00',
  `transaction_detail_unit_price` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `transaction_detail_unit_discount` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `transaction_detail_unit_nett_price` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `transaction_detail_quantity` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `transaction_detail_unit` enum('pcs','hour','day','month') NOT NULL DEFAULT 'pcs',
  `transaction_detail_subtotal_price` decimal(15,2) UNSIGNED NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data detail produk transaksi e-commerce';

-- --------------------------------------------------------

--
-- Table structure for table `ecom_transaction_perawat`
--

CREATE TABLE `ecom_transaction_perawat` (
  `transaction_perawat_id` int(10) UNSIGNED NOT NULL,
  `transaction_perawat_transaction_id` int(10) UNSIGNED NOT NULL,
  `transaction_perawat_person_id` int(10) UNSIGNED NOT NULL,
  `transaction_perawat_datetime` datetime NOT NULL,
  `transaction_perawat_input_by` varchar(100) NOT NULL,
  `transaction_perawat_status` enum('approved','canceled') NOT NULL DEFAULT 'approved',
  `transaction_perawat_note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_transaction_status`
--

CREATE TABLE `ecom_transaction_status` (
  `transaction_status_id` int(10) UNSIGNED NOT NULL,
  `transaction_status_transaction_id` int(10) UNSIGNED NOT NULL,
  `transaction_status_value` enum('pending','confirmed','paid','sent','received','canceled') NOT NULL,
  `transaction_status_note` varchar(255) DEFAULT NULL,
  `transaction_status_administrator_username` varchar(50) DEFAULT NULL,
  `transaction_status_datetime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data log status transaksi e-commerce';

-- --------------------------------------------------------

--
-- Table structure for table `ecom_transaction_termin`
--

CREATE TABLE `ecom_transaction_termin` (
  `termin_id` int(10) UNSIGNED NOT NULL,
  `termin_transaction_id` int(10) UNSIGNED NOT NULL,
  `termin_label` varchar(100) NOT NULL,
  `termin_start_date` date NOT NULL,
  `termin_end_date` date NOT NULL,
  `termin_nominal` decimal(15,2) NOT NULL,
  `termin_status` enum('unpaid','paid','rejected') NOT NULL DEFAULT 'unpaid',
  `termin_input_by` varchar(100) NOT NULL,
  `termin_update_by` varchar(100) DEFAULT NULL,
  `termin_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form_run`
--

CREATE TABLE `form_run` (
  `run_id` int(10) UNSIGNED NOT NULL,
  `run_category` varchar(100) NOT NULL,
  `run_length` varchar(50) NOT NULL,
  `run_price` varchar(50) NOT NULL,
  `run_date` date NOT NULL,
  `run_time` varchar(100) NOT NULL,
  `run_full_name` varchar(100) NOT NULL,
  `run_gender` enum('M','F') NOT NULL,
  `run_date_of_birth` date NOT NULL,
  `run_nationality` varchar(50) NOT NULL,
  `run_email_address` varchar(50) NOT NULL,
  `run_mobile_number_1` varchar(20) NOT NULL,
  `run_mobile_number_2` varchar(20) DEFAULT NULL,
  `run_home_address` varchar(255) NOT NULL,
  `run_current_address` varchar(255) DEFAULT NULL,
  `run_emergency_contact_name` varchar(50) NOT NULL,
  `run_emergency_contact_phone` varchar(20) NOT NULL,
  `run_blood_type` varchar(5) DEFAULT NULL,
  `run_allergies_1` enum('Y','N') NOT NULL,
  `run_allergies_2` varchar(100) DEFAULT NULL,
  `run_hospitalized_1` enum('Y','N') NOT NULL DEFAULT 'N',
  `run_hospitalized_2` varchar(100) DEFAULT NULL,
  `run_serious_illnes_1` enum('Y','N') NOT NULL DEFAULT 'N',
  `run_serious_illnes_2` varchar(100) DEFAULT NULL,
  `run_another_informations` varchar(255) DEFAULT NULL,
  `run_intensity` varchar(50) DEFAULT NULL,
  `run_furthest` varchar(50) DEFAULT NULL,
  `run_pace` varchar(50) DEFAULT NULL,
  `run_tshirt_size` varchar(10) DEFAULT NULL,
  `run_read_by` varchar(100) DEFAULT NULL,
  `run_read_datetime` datetime DEFAULT NULL,
  `run_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_activation`
--

CREATE TABLE `master_activation` (
  `activation_id` int(9) UNSIGNED NOT NULL,
  `activation_pin` char(4) NOT NULL,
  `activation_num_days` int(4) NOT NULL DEFAULT '30' COMMENT 'jumlah hari',
  `activation_person_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'buyer',
  `activation_company_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `activation_expired_date` date DEFAULT NULL,
  `activation_has_used` enum('Y','N') NOT NULL DEFAULT 'N',
  `activation_used_date` date DEFAULT NULL,
  `activation_ip_address` int(10) UNSIGNED DEFAULT NULL,
  `activation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activation_created_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_position`
--

CREATE TABLE `master_position` (
  `position_id` int(10) UNSIGNED NOT NULL,
  `position_title` varchar(100) NOT NULL,
  `position_permalink` varchar(100) NOT NULL,
  `position_description` text,
  `position_input_by` varchar(100) NOT NULL,
  `position_is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `position_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_printer`
--

CREATE TABLE `master_printer` (
  `printer_id` tinyint(3) UNSIGNED NOT NULL,
  `printer_name` varchar(50) NOT NULL,
  `printer_paper_size` enum('58mm','76mm','210mm') NOT NULL DEFAULT '58mm',
  `printer_for` enum('nota') NOT NULL DEFAULT 'nota',
  `printer_note` varchar(400) DEFAULT NULL,
  `printer_is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `printer_is_used` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Y:digunakan;N:tidak digunakan',
  `printer_admin_username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_printer`
--

INSERT INTO `master_printer` (`printer_id`, `printer_name`, `printer_paper_size`, `printer_for`, `printer_note`, `printer_is_active`, `printer_is_used`, `printer_admin_username`) VALUES
(1, 'INOFARMA_SZ58', '58mm', 'nota', 'Printer dengan ukuran kertas 58mm.', 'Y', 'N', 'admin'),
(2, 'INOFARMA_SZ76', '76mm', 'nota', 'Printer dengan ukuran kertas 76mm.', 'Y', 'N', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `master_shift`
--

CREATE TABLE `master_shift` (
  `shift_id` int(2) UNSIGNED NOT NULL,
  `shift_title` varchar(50) NOT NULL,
  `shift_start_time` time DEFAULT NULL,
  `shift_end_time` time DEFAULT NULL,
  `shift_is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `shift_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_list`
--

CREATE TABLE `notification_list` (
  `notification_list_id` int(10) UNSIGNED NOT NULL,
  `notification_list_admin_username` varchar(50) NOT NULL,
  `notification_list_title` varchar(100) NOT NULL,
  `notification_list_text` varchar(255) NOT NULL,
  `notification_list_is_read` enum('N','Y') NOT NULL DEFAULT 'N',
  `notification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_list`
--

INSERT INTO `notification_list` (`notification_list_id`, `notification_list_admin_username`, `notification_list_title`, `notification_list_text`, `notification_list_is_read`, `notification_timestamp`) VALUES
(1, 'admin', 'zaharaulia@gmail.com (tanya harga - )', 'ssdsghcfahgc', 'Y', '2016-12-15 03:59:10'),
(2, 'admin', 'anggoro.indonesia@gmail.com (Testing Kontak - )', 'Percobaan Bro', 'Y', '2018-01-12 16:53:55'),
(3, 'admin', 'anggoro.indonesia@gmail.com (Testing Web - )', 'Percobaan', 'Y', '2018-01-13 12:22:24'),
(4, 'admin', 'setiyo10@gmail.com ( - keluhan)', 'terlalu bagus web e', 'N', '2018-04-02 06:31:58'),
(5, 'admin', 'setiyo10@gmail.com ( - saran)', 'Terlalu bagus bos', 'N', '2018-04-02 06:47:02'),
(6, 'admin', 'setiyo10@gmail.com ( - saran)', 'sdfsdfsdf', 'N', '2018-04-02 08:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `ref_area`
--

CREATE TABLE `ref_area` (
  `area_id` int(15) UNSIGNED NOT NULL,
  `area_country_id` int(10) UNSIGNED NOT NULL,
  `area_par_id` int(15) NOT NULL DEFAULT '0',
  `area_name` varchar(255) NOT NULL,
  `area_latitude` varchar(25) NOT NULL,
  `area_longitude` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ref_bank`
--

CREATE TABLE `ref_bank` (
  `bank_id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `bank_logo` varchar(255) NOT NULL,
  `bank_is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data referensi bank';

-- --------------------------------------------------------

--
-- Table structure for table `ref_country`
--

CREATE TABLE `ref_country` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(200) NOT NULL,
  `country_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_administrator`
--

CREATE TABLE `site_administrator` (
  `admin_id` int(15) NOT NULL,
  `admin_group_id` int(15) NOT NULL DEFAULT '0',
  `admin_username` varchar(255) NOT NULL DEFAULT 'admin',
  `admin_password` varchar(255) NOT NULL DEFAULT 'admin',
  `admin_last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_is_active` enum('0','1') NOT NULL DEFAULT '1',
  `admin_config_printer` enum('popup','direct') NOT NULL DEFAULT 'popup' COMMENT 'popup -> tampilkan popup; direct: plugin java',
  `admin_person_id` int(11) DEFAULT NULL,
  `admin_person_full_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_administrator`
--

INSERT INTO `site_administrator` (`admin_id`, `admin_group_id`, `admin_username`, `admin_password`, `admin_last_login`, `admin_is_active`, `admin_config_printer`, `admin_person_id`, `admin_person_full_name`) VALUES
(7, 14, 'admin', '2aefc34200a294a3cc7db81b43a81873', '2018-04-07 01:19:01', '1', 'popup', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_administrator_group`
--

CREATE TABLE `site_administrator_group` (
  `admin_group_id` int(15) NOT NULL,
  `admin_group_title` varchar(255) NOT NULL,
  `admin_group_is_active` enum('0','1') NOT NULL DEFAULT '1',
  `admin_group_type` enum('superuser','administrator') NOT NULL DEFAULT 'administrator'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_administrator_group`
--

INSERT INTO `site_administrator_group` (`admin_group_id`, `admin_group_title`, `admin_group_is_active`, `admin_group_type`) VALUES
(14, 'superuser', '1', 'superuser'),
(17, 'Admin Report', '1', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `site_administrator_menu`
--

CREATE TABLE `site_administrator_menu` (
  `menu_administrator_id` int(15) NOT NULL,
  `menu_administrator_par_id` int(15) NOT NULL DEFAULT '0',
  `menu_administrator_title` varchar(255) DEFAULT NULL,
  `menu_administrator_description` text,
  `menu_administrator_link` varchar(255) DEFAULT NULL,
  `menu_administrator_page_id` int(15) NOT NULL DEFAULT '0',
  `menu_administrator_order_by` int(15) NOT NULL DEFAULT '0',
  `menu_administrator_location` enum('user','member','stockist','admin','user_footer','user_header') NOT NULL,
  `menu_administrator_is_active` enum('0','1') NOT NULL DEFAULT '0',
  `menu_administrator_module` varchar(50) DEFAULT NULL COMMENT '[admin]',
  `menu_administrator_controller` varchar(50) DEFAULT NULL COMMENT '[admin]',
  `menu_administrator_method` varchar(500) DEFAULT NULL COMMENT '[admin]tiap method dipisahkan dengan #',
  `menu_administrator_class_css` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_administrator_menu`
--

INSERT INTO `site_administrator_menu` (`menu_administrator_id`, `menu_administrator_par_id`, `menu_administrator_title`, `menu_administrator_description`, `menu_administrator_link`, `menu_administrator_page_id`, `menu_administrator_order_by`, `menu_administrator_location`, `menu_administrator_is_active`, `menu_administrator_module`, `menu_administrator_controller`, `menu_administrator_method`, `menu_administrator_class_css`) VALUES
(8, 11, 'Group', 'untuk konfigurasi group dan privilege group', 'admin/admin_group/index', 0, 1, 'admin', '1', 'admin_group', 'admin', 'index#create#update#set_privilege#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(9, 11, 'User', 'Untuk memanajemen user/admin', 'admin/admin_user/index', 0, 2, 'admin', '1', 'admin_user', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(10, 11, 'Menu Admin', 'Untuk memanajemen menu', 'admin/admin_menu/index', 0, 4, 'admin', '0', 'admin_menu', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(11, 0, 'Settings', 'settings group, user dan menu', '', 0, 24, 'admin', '1', '-', '-', '-', 'clip-cog-2'),
(12, 11, 'Config', '', 'admin/admin_config/view', 0, 6, 'admin', '1', 'admin_config', 'admin', 'master_shift#update#view#', ''),
(13, 11, 'Menu Visitor', 'menu visitor', 'admin/admin_menu/index?location=user', 0, 5, 'admin', '1', 'admin_menu', 'admin', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(24, 0, 'Dashboard', 'dashboard', 'admin/dashboard/index', 0, 1, 'admin', '1', 'dashboard', 'admin', 'index', 'clip-home-3'),
(46, 11, 'Privelege Module Label', '', 'admin/admin_group_menu_label/index', 0, 3, 'admin', '0', 'admin_group_menu_label', 'admin', 'index#index_filter#ign_index_filter', ''),
(53, 0, 'Data Master', '-', '#', 0, 4, 'admin', '1', '-', '-', '-', 'clip-grid-5'),
(58, 53, 'People', 'Kumpulan data master dokter, apoteker, karyawan, dan lainnya yang berkaitan dengan kependudukan.', 'admin/people/index', 0, 5, 'admin', '0', 'people', 'admin', 'index_person#index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
(94, 53, 'Unit / Cabang', '', 'admin/store/index', 0, 12, 'admin', '0', 'store', 'admin', 'index#add#update#delete#act_publish#act_unpublish#act_delete#', ''),
(100, 97, 'Item Belum Expired', '', 'admin/stok_all/arus_stok_sisa', 0, 3, 'admin', '0', 'stok_all', 'admin', 'arus_stok_sisa#export_arus_stok_sisa_data', ''),
(102, 97, 'Item Kadaluarsa', '', 'admin/stok_all/expired', 0, 5, 'admin', '0', 'stok_all', 'admin', 'expire#export_expired_data', ''),
(118, 53, 'Kategori', '', 'admin/master_content_category/index', 0, 1, 'admin', '1', 'master_content_category', 'admin', 'index#create#update#delete#delete_image#', ''),
(119, 53, 'Konten', '', 'admin/master_content/index', 0, 1, 'admin', '1', 'master_content', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', ''),
(120, 53, 'Youtube', '', 'admin/master_youtube/index', 0, 3, 'admin', '0', 'master_youtube', 'admin', 'index#add#update#delete_image#act_delete#delete#', ''),
(121, 53, 'Slider', '', 'admin/image_slide/index', 0, 4, 'admin', '1', 'image_slide', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', ''),
(122, 53, 'Sosial Media', '', 'admin/sosial_media/index', 0, 5, 'admin', '1', 'sosial_media', 'admin', 'index#create#update#delete#', ''),
(123, 53, 'News Ticker', '', 'admin/news_ticker/index', 0, 6, 'admin', '0', 'news_ticker', 'admin', 'index#add#update#act_delete#delete#', ''),
(124, 0, 'Inbox', '', 'admin/ask/index', 0, 3, 'admin', '1', 'ask', 'admin', 'index#act_delete#delete#', 'fa fa-envelope'),
(125, 0, 'Polling', '', 'admin/polling/index/1', 0, 3, 'admin', '0', 'polling', 'admin', 'index#statistik#statistik_user#user#answer_user#', 'fa fa-question-circle'),
(126, 53, 'Lowongan Kerja', '', 'admin/master_job/index', 0, 13, 'admin', '0', 'master_job', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', ''),
(127, 53, 'Chat YM!', '', 'admin/chat_ym/index', 0, 14, 'admin', '0', 'chat_ym', 'admin', 'test_status#set_status#index#add#update#act_delete#delete#', ''),
(128, 0, 'Master Medical', '', '#', 0, 5, 'admin', '0', '-', '-', '-', 'fa fa-medkit'),
(129, 128, 'Perawat', '', 'admin/master_perawat/index', 0, 1, 'admin', '1', 'master_perawat', 'admin', 'index#add#update#', ''),
(130, 128, 'Posisi', '', 'admin/master_position/index', 0, 2, 'admin', '1', 'master_position', 'admin', 'get_data#index#add#update#act_delete#delete#', ''),
(131, 53, 'Produk', '', 'admin/product/index', 0, 15, 'admin', '0', 'product', 'admin', 'index#create#update#delete_more_images#delete#', ''),
(132, 0, 'Transaksi', '', 'admin/ecom_transaction/index', 0, 7, 'admin', '0', 'ecom_transaction', 'admin', 'pos_stored#generate_view_shopping_cart#pos_empty_shopping_cart#pos_get_total_shopping_cart#pos_remove_shopping_cart#add_to_cart#get_detail_member#pos#transaction_has_received#update_payment_status#save_no_resi#get_data#index#view#invoice#', 'clip-cart'),
(133, 0, 'Follow-up', '', 'admin/followup/index', 0, 6, 'admin', '0', 'followup', 'admin', 'confirmed#view#index#get_data#', 'fa fa-phone-square'),
(134, 0, 'Order', '', 'admin/form_run/index', 0, 3, 'admin', '0', 'form_run', 'admin', 'get_data#index#update#act_delete#delete#export_data#', 'clip-cart'),
(135, 0, 'Tagline Website', '', 'admin/admin_config/tagline', 0, 5, 'admin', '0', 'admin_config', 'admin', 'tagline', 'clip-heart'),
(138, 0, 'Lakukan Aksimu', '', 'master_content/archives/lakukan-aksimu', 0, 1, 'user_header', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(139, 0, 'Tentang Kami', '', 'master_content/detic/tentang_kami', 0, 3, 'user_header', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(151, 0, 'Panduan Aksi', '', 'master_content/archives/panduan_aksi', 0, 2, 'user_header', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(152, 0, 'Video Youtube', '', 'admin/master_youtube/index', 0, 4, 'admin', '0', 'master_youtube', 'admin', 'index#add#update#delete_image#act_delete#delete#', 'fa fa-youtube'),
(153, 53, 'Our Team', '', 'admin/struktur/index', 0, 16, 'admin', '1', 'struktur', 'admin', 'index#add#update#delete#delete_image#', ''),
(154, 0, 'Testimonial', '', 'admin/testimonial/index', 0, 4, 'admin', '1', 'testimonial', 'admin', 'get_data#index#add#update#delete_image#act_delete#delete#', 'fa fa-envelope'),
(155, 0, 'Home', '#', 'index.php', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(181, 0, 'Kontak', '', 'ask/f_ask/', 0, 8, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(201, 0, 'Perwakilan', 'Menu Utama Perwakilan', 'master_content/archives/perwakilan?v=static', 0, 4, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(202, 0, 'Paket', 'Menu utama paket', '#', 0, 3, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(203, 0, 'Berita', 'Menu Utama Berita', 'master_content/archives/berita?v=static', 0, 5, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(204, 0, 'Tentang Kami', 'Menu Utama About', 'master_content/archives/about?v=static', 0, 2, 'user', '1', '', '', '', ''),
(205, 0, 'Info', 'Menu Utama Info', 'master_content/archives/news/', 0, 9, 'user', '0', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(206, 0, 'Layanan', 'Layanan pemesanan, poli, waktu', '#', 0, 23, 'admin', '0', '-', '-', '-', 'fa fa-check'),
(207, 206, 'Pemesanan', 'Pemesanan Layanan', 'admin/order_patient/index', 0, 1, 'admin', '1', 'order_patient', 'admin', 'export_order_patient#print_order_patient#view#index#', ''),
(208, 206, 'Daftar Waktu', 'Waktu poli', 'admin/request_time/index', 0, 2, 'admin', '1', 'request_time', 'admin', 'update#add#act_publish#act_unpublish#act_delete#index#', ''),
(209, 206, 'Daftar Poli', 'Jenis Layanan poli', 'admin/service/index', 0, 3, 'admin', '1', 'service', 'admin', 'update#add#act_publish#act_unpublish#act_delete#index#', ''),
(210, 11, 'Hapus Antrian', '', 'admin/admin_config/schedule_patient', 0, 7, 'admin', '0', 'admin_config', 'admin', 'schedule_dokter#schedule_patient#tagline#update#view#', ''),
(211, 11, 'Hapus Jadwal Dokter', '', 'admin/admin_config/schedule_dokter', 0, 8, 'admin', '0', 'admin_config', 'admin', 'schedule_dokter#schedule_patient#tagline#update#view#', ''),
(212, 0, 'Itenerary', 'Menu Utama Itenerary', 'master_content/archives/itenerary?v=static', 0, 8, 'user', '0', NULL, NULL, NULL, NULL),
(213, 0, 'Gallery', 'Menu Utama Gallery', 'master_content/archives/gallery?v=gallery', 0, 10, 'user', '0', NULL, NULL, NULL, NULL),
(214, 0, 'Download File', 'Menu Utama Download', 'master_content/archives/download?v=widget', 0, 6, 'user', '1', '', '', '', ''),
(215, 202, 'PAKET UMRAH REGULER', 'PAKET UMRAH REGULER', '', 0, 1, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(216, 202, 'PAKET UMRAH PROMO', 'PAKET UMRAH PROMO', '', 0, 2, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(217, 202, 'PAKET UMRAH PLUS', 'PAKET UMRAH PLUS', '', 0, 3, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', ''),
(218, 202, 'PAKET UMRAH RAMADHAN', 'PAKET UMRAH RAMADHAN', '', 0, 4, 'user', '1', '', '', 'index#create#update#view#delete#delete_selected_item#publish_selected_item#unpublish_selected_item', '');

-- --------------------------------------------------------

--
-- Table structure for table `site_administrator_privilege`
--

CREATE TABLE `site_administrator_privilege` (
  `administrator_privilege_id` int(10) UNSIGNED NOT NULL,
  `administrator_privilege_administrator_group_id` int(15) NOT NULL,
  `administrator_privilege_administrator_menu_id` int(10) UNSIGNED NOT NULL,
  `administrator_privilege_method_allowed` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_administrator_privilege`
--

INSERT INTO `site_administrator_privilege` (`administrator_privilege_id`, `administrator_privilege_administrator_group_id`, `administrator_privilege_administrator_menu_id`, `administrator_privilege_method_allowed`) VALUES
(730, 21, 24, 'index'),
(731, 21, 24, 'service_rest_by_request'),
(732, 21, 24, ''),
(733, 21, 70, ''),
(734, 21, 71, 'index'),
(735, 21, 71, 'export_data'),
(736, 21, 71, ''),
(737, 21, 79, 'index'),
(738, 21, 79, 'export_data'),
(739, 21, 79, ''),
(740, 21, 80, 'index'),
(741, 21, 80, 'export_data'),
(742, 21, 80, ''),
(743, 21, 81, 'index'),
(744, 21, 81, 'export_data'),
(745, 21, 81, ''),
(746, 21, 82, 'index'),
(747, 21, 82, 'export_data'),
(748, 21, 82, ''),
(749, 21, 83, 'index'),
(750, 21, 83, 'export_data'),
(751, 21, 83, ''),
(752, 21, 84, 'index'),
(753, 21, 84, 'export_data'),
(754, 21, 84, ''),
(755, 21, 85, 'index'),
(756, 21, 85, 'export_data'),
(757, 21, 85, ''),
(758, 21, 86, 'index'),
(759, 21, 86, 'export_data'),
(760, 21, 86, ''),
(761, 21, 88, 'index'),
(762, 21, 88, 'export_data'),
(763, 21, 88, ''),
(764, 21, 89, 'index'),
(765, 21, 89, 'export_data'),
(766, 21, 89, ''),
(767, 21, 11, ''),
(768, 21, 9, 'index'),
(769, 21, 9, 'update'),
(770, 21, 9, 'view'),
(771, 21, 9, ''),
(803, 23, 24, 'index'),
(804, 23, 24, 'service_rest_by_request'),
(805, 23, 24, ''),
(806, 23, 56, ''),
(807, 23, 57, 'index'),
(808, 23, 57, 'add'),
(809, 23, 57, 'edit'),
(810, 23, 57, 'kode_check'),
(811, 23, 57, 'export_data'),
(812, 23, 57, ''),
(813, 23, 58, 'index'),
(814, 23, 58, 'add'),
(815, 23, 58, 'edit'),
(816, 23, 58, 'kode_check'),
(817, 23, 58, 'nama_check'),
(818, 23, 58, 'export_data'),
(819, 23, 58, ''),
(820, 23, 59, 'index'),
(821, 23, 59, 'add'),
(822, 23, 59, 'edit'),
(823, 23, 59, 'kode_check'),
(824, 23, 59, 'nama_check'),
(825, 23, 59, 'export_data'),
(826, 23, 59, ''),
(827, 23, 69, 'index'),
(828, 23, 69, 'add'),
(829, 23, 69, 'edit'),
(830, 23, 69, 'nama_check'),
(831, 23, 69, 'export_data'),
(832, 23, 69, ''),
(833, 23, 65, 'index'),
(834, 23, 65, 'add'),
(835, 23, 65, 'edit'),
(836, 23, 65, 'kode_check'),
(837, 23, 65, 'export_data'),
(838, 23, 65, ''),
(839, 23, 11, ''),
(840, 23, 9, 'index'),
(841, 23, 9, 'update'),
(842, 23, 9, 'view'),
(843, 23, 9, ''),
(1048, 22, 12, 'view'),
(1049, 22, 12, ''),
(1169, 22, 8, 'index'),
(1170, 22, 8, ''),
(1763, 22, 24, 'index'),
(1764, 22, 24, 'service_rest_by_request'),
(1765, 22, 24, ''),
(1766, 22, 56, ''),
(1767, 22, 57, 'index'),
(1768, 22, 57, ''),
(1769, 22, 58, 'index'),
(1770, 22, 58, ''),
(1771, 22, 59, 'index'),
(1772, 22, 59, 'export_data'),
(1773, 22, 59, ''),
(1774, 22, 69, 'index'),
(1775, 22, 69, 'export_data'),
(1776, 22, 69, ''),
(1777, 22, 65, 'index'),
(1778, 22, 65, 'export_data'),
(1779, 22, 65, ''),
(1780, 22, 60, ''),
(1781, 22, 61, 'index'),
(1782, 22, 61, ''),
(1783, 22, 62, 'index'),
(1784, 22, 62, ''),
(1785, 22, 63, 'index'),
(1786, 22, 63, ''),
(1787, 22, 64, 'index'),
(1788, 22, 64, ''),
(1789, 22, 67, 'index'),
(1790, 22, 67, ''),
(1791, 22, 68, 'index'),
(1792, 22, 68, ''),
(1793, 22, 70, ''),
(1794, 22, 71, 'index'),
(1795, 22, 71, 'export_data'),
(1796, 22, 71, ''),
(1797, 22, 79, 'index'),
(1798, 22, 79, 'export_data'),
(1799, 22, 79, ''),
(1800, 22, 80, 'index'),
(1801, 22, 80, 'export_data'),
(1802, 22, 80, ''),
(1803, 22, 81, 'index'),
(1804, 22, 81, 'export_data'),
(1805, 22, 81, ''),
(1806, 22, 82, 'index'),
(1807, 22, 82, 'export_data'),
(1808, 22, 82, ''),
(1809, 22, 83, 'index'),
(1810, 22, 83, 'export_data'),
(1811, 22, 83, ''),
(1812, 22, 84, 'index'),
(1813, 22, 84, 'export_data'),
(1814, 22, 84, ''),
(1815, 22, 85, 'index'),
(1816, 22, 85, 'export_data'),
(1817, 22, 85, ''),
(1818, 22, 86, 'index'),
(1819, 22, 86, 'export_data'),
(1820, 22, 86, ''),
(1821, 22, 88, 'index'),
(1822, 22, 88, 'export_data'),
(1823, 22, 88, ''),
(1824, 22, 89, 'index'),
(1825, 22, 89, 'export_data'),
(1826, 22, 89, ''),
(1827, 22, 11, ''),
(1828, 22, 9, 'index'),
(1829, 22, 9, ''),
(1923, 16, 24, 'index'),
(1924, 16, 24, 'service_rest_by_request'),
(1925, 16, 24, ''),
(1926, 16, 11, ''),
(1927, 16, 8, 'index'),
(1928, 16, 8, 'create'),
(1929, 16, 8, 'update'),
(1930, 16, 8, 'set_privilege'),
(1931, 16, 8, 'view'),
(1932, 16, 8, 'delete'),
(1933, 16, 8, 'delete_selected_item'),
(1934, 16, 8, 'publish_selected_item'),
(1935, 16, 8, 'unpublish_selected_item'),
(1936, 16, 8, ''),
(1937, 16, 46, 'index'),
(1938, 16, 46, 'index_filter'),
(1939, 16, 46, ''),
(1940, 16, 9, 'index'),
(1941, 16, 9, 'create'),
(1942, 16, 9, 'update'),
(1943, 16, 9, 'view'),
(1944, 16, 9, 'delete'),
(1945, 16, 9, 'delete_selected_item'),
(1946, 16, 9, 'publish_selected_item'),
(1947, 16, 9, 'unpublish_selected_item'),
(1948, 16, 9, ''),
(1949, 16, 10, 'index'),
(1950, 16, 10, 'create'),
(1951, 16, 10, 'update'),
(1952, 16, 10, 'view'),
(1953, 16, 10, 'delete'),
(1954, 16, 10, 'delete_selected_item'),
(1955, 16, 10, 'publish_selected_item'),
(1956, 16, 10, 'unpublish_selected_item'),
(1957, 16, 10, ''),
(1958, 16, 13, 'index'),
(1959, 16, 13, 'create'),
(1960, 16, 13, 'update'),
(1961, 16, 13, 'view'),
(1962, 16, 13, 'delete'),
(1963, 16, 13, 'delete_selected_item'),
(1964, 16, 13, 'publish_selected_item'),
(1965, 16, 13, 'unpublish_selected_item'),
(1966, 16, 13, ''),
(1967, 16, 12, 'update'),
(1968, 16, 12, 'view'),
(1969, 16, 12, ''),
(2559, 19, 24, 'index'),
(2560, 19, 24, ''),
(2561, 19, 64, ''),
(2562, 19, 65, ''),
(2563, 19, 74, 'index'),
(2564, 19, 74, 'add'),
(2565, 19, 74, 'add_payment'),
(2566, 19, 74, 'add_card_payment'),
(2567, 19, 74, 'payment_returns'),
(2568, 19, 74, 'add_person'),
(2569, 19, 74, 'load_person'),
(2570, 19, 74, 'load_item'),
(2571, 19, 74, 'print_trx'),
(2572, 19, 74, 'load_suspend_trx'),
(2573, 19, 74, 'load_all_data_trx'),
(2574, 19, 74, ''),
(2575, 19, 75, 'report'),
(2576, 19, 75, 'export_data'),
(2577, 19, 75, ''),
(2578, 19, 82, ''),
(2579, 19, 91, 'index'),
(2580, 19, 91, 'add'),
(2581, 19, 91, 'add_payment'),
(2582, 19, 91, 'add_card_payment'),
(2583, 19, 91, 'payment_returns'),
(2584, 19, 91, 'add_dokter'),
(2585, 19, 91, 'load_dokter'),
(2586, 19, 91, 'add_pasien'),
(2587, 19, 91, 'load_pasien'),
(2588, 19, 91, 'load_item'),
(2589, 19, 91, 'print_trx'),
(2590, 19, 91, 'load_suspend_trx'),
(2591, 19, 91, 'load_all_data_trx'),
(2592, 19, 91, ''),
(2593, 19, 92, 'report'),
(2594, 19, 92, 'export_data'),
(2595, 19, 92, ''),
(2596, 19, 87, ''),
(2597, 19, 88, 'payment'),
(2598, 19, 88, 'export_payment_data'),
(2599, 19, 88, ''),
(2600, 19, 89, 'payment_cash'),
(2601, 19, 89, 'export_payment_cash_data'),
(2602, 19, 89, ''),
(2603, 19, 90, 'payment_card'),
(2604, 19, 90, 'export_payment_card_data'),
(2605, 19, 90, ''),
(2606, 19, 11, ''),
(2607, 19, 9, 'index'),
(2608, 19, 9, 'update'),
(2609, 19, 9, 'view'),
(2610, 19, 9, ''),
(2611, 19, 12, 'view'),
(2612, 19, 12, ''),
(2613, 18, 24, 'index'),
(2614, 18, 24, ''),
(2615, 18, 53, ''),
(2616, 18, 54, 'index'),
(2617, 18, 54, 'add'),
(2618, 18, 54, 'update'),
(2619, 18, 54, ''),
(2620, 18, 55, 'index'),
(2621, 18, 55, 'add'),
(2622, 18, 55, 'update'),
(2623, 18, 55, ''),
(2624, 18, 56, 'index'),
(2625, 18, 56, 'add'),
(2626, 18, 56, 'update'),
(2627, 18, 56, ''),
(2628, 18, 57, 'index'),
(2629, 18, 57, 'add'),
(2630, 18, 57, 'update'),
(2631, 18, 57, 'delete'),
(2632, 18, 57, 'act_publish'),
(2633, 18, 57, 'act_unpublish'),
(2634, 18, 57, 'act_delete'),
(2635, 18, 57, ''),
(2636, 18, 58, 'index'),
(2637, 18, 58, 'add'),
(2638, 18, 58, 'update'),
(2639, 18, 58, 'delete'),
(2640, 18, 58, 'act_publish'),
(2641, 18, 58, 'act_unpublish'),
(2642, 18, 58, 'act_delete'),
(2643, 18, 58, ''),
(2644, 18, 59, 'index'),
(2645, 18, 59, 'add'),
(2646, 18, 59, 'update'),
(2647, 18, 59, 'delete'),
(2648, 18, 59, 'act_publish'),
(2649, 18, 59, 'act_unpublish'),
(2650, 18, 59, 'act_delete'),
(2651, 18, 59, ''),
(2652, 18, 60, 'index'),
(2653, 18, 60, 'add'),
(2654, 18, 60, 'update'),
(2655, 18, 60, 'delete'),
(2656, 18, 60, 'act_publish'),
(2657, 18, 60, 'act_unpublish'),
(2658, 18, 60, 'act_delete'),
(2659, 18, 60, ''),
(2660, 18, 61, 'index'),
(2661, 18, 61, 'add'),
(2662, 18, 61, 'update'),
(2663, 18, 61, 'delete'),
(2664, 18, 61, 'act_publish'),
(2665, 18, 61, 'act_unpublish'),
(2666, 18, 61, 'act_delete'),
(2667, 18, 61, ''),
(2668, 18, 62, 'index'),
(2669, 18, 62, 'add'),
(2670, 18, 62, 'update'),
(2671, 18, 62, 'delete'),
(2672, 18, 62, 'act_publish'),
(2673, 18, 62, 'act_unpublish'),
(2674, 18, 62, 'act_delete'),
(2675, 18, 62, ''),
(2676, 18, 63, 'index'),
(2677, 18, 63, 'add'),
(2678, 18, 63, 'update'),
(2679, 18, 63, 'delete'),
(2680, 18, 63, 'act_publish'),
(2681, 18, 63, 'act_unpublish'),
(2682, 18, 63, 'act_delete'),
(2683, 18, 63, ''),
(2684, 18, 93, 'index'),
(2685, 18, 93, 'add'),
(2686, 18, 93, 'update'),
(2687, 18, 93, ''),
(2688, 18, 11, ''),
(2689, 18, 9, 'index'),
(2690, 18, 9, 'update'),
(2691, 18, 9, 'view'),
(2692, 18, 9, ''),
(2693, 18, 12, 'view'),
(2694, 18, 12, ''),
(2963, 20, 24, 'index'),
(2964, 20, 24, ''),
(2965, 20, 53, ''),
(2966, 20, 56, 'index'),
(2967, 20, 56, 'add'),
(2968, 20, 56, 'update'),
(2969, 20, 56, ''),
(2970, 20, 57, 'index'),
(2971, 20, 57, 'add'),
(2972, 20, 57, 'update'),
(2973, 20, 57, 'delete'),
(2974, 20, 57, 'act_publish'),
(2975, 20, 57, 'act_unpublish'),
(2976, 20, 57, 'act_delete'),
(2977, 20, 57, ''),
(2978, 20, 63, 'index'),
(2979, 20, 63, 'add'),
(2980, 20, 63, 'update'),
(2981, 20, 63, 'delete'),
(2982, 20, 63, 'act_publish'),
(2983, 20, 63, 'act_unpublish'),
(2984, 20, 63, 'act_delete'),
(2985, 20, 63, ''),
(2986, 20, 67, ''),
(2987, 20, 68, ''),
(2988, 20, 70, 'index'),
(2989, 20, 70, 'add'),
(2990, 20, 70, 'add_supplier'),
(2991, 20, 70, 'load_supplier'),
(2992, 20, 70, 'load_item'),
(2993, 20, 70, 'modal_information'),
(2994, 20, 70, 'print_trx'),
(2995, 20, 70, 'load_suspend_trx'),
(2996, 20, 70, 'load_all_data_trx'),
(2997, 20, 70, ''),
(2998, 20, 71, 'report'),
(2999, 20, 71, 'export_data'),
(3000, 20, 71, ''),
(3001, 20, 69, ''),
(3002, 20, 72, 'index'),
(3003, 20, 72, 'add'),
(3004, 20, 72, 'load_item'),
(3005, 20, 72, 'add_supplier'),
(3006, 20, 72, 'load_supplier'),
(3007, 20, 72, 'modal_information'),
(3008, 20, 72, 'load_all_data_trx'),
(3009, 20, 72, 'load_all_data_po_trx'),
(3010, 20, 72, ''),
(3011, 20, 73, 'report'),
(3012, 20, 73, 'export_data'),
(3013, 20, 73, ''),
(3014, 20, 78, ''),
(3015, 20, 79, 'index'),
(3016, 20, 79, 'export_data'),
(3017, 20, 79, ''),
(3018, 20, 80, 'detail'),
(3019, 20, 80, 'export_detail_data'),
(3020, 20, 80, ''),
(3021, 20, 81, 'expired'),
(3022, 20, 81, 'export_expired_data'),
(3023, 20, 81, ''),
(3024, 20, 83, ''),
(3025, 20, 84, 'index'),
(3026, 20, 84, 'add'),
(3027, 20, 84, 'load_item'),
(3028, 20, 84, 'modal_information'),
(3029, 20, 84, 'load_all_data_trx'),
(3030, 20, 84, ''),
(3031, 20, 85, 'report'),
(3032, 20, 85, 'export_data'),
(3033, 20, 85, ''),
(3034, 20, 95, 'index'),
(3035, 20, 95, 'add'),
(3036, 20, 95, 'load_item'),
(3037, 20, 95, 'modal_information'),
(3038, 20, 95, 'print_trx'),
(3039, 20, 95, 'load_suspend_trx'),
(3040, 20, 95, 'load_store'),
(3041, 20, 95, 'load_all_data_trx'),
(3042, 20, 95, 'report'),
(3043, 20, 95, 'export_data'),
(3044, 20, 95, ''),
(3045, 20, 96, 'index'),
(3046, 20, 96, 'add'),
(3047, 20, 96, 'load_item'),
(3048, 20, 96, 'add_supplier'),
(3049, 20, 96, 'load_supplier'),
(3050, 20, 96, 'modal_information'),
(3051, 20, 96, 'load_all_data_trx'),
(3052, 20, 96, 'load_all_data_po_trx'),
(3053, 20, 96, 'report'),
(3054, 20, 96, 'export_data'),
(3055, 20, 96, ''),
(3056, 20, 11, ''),
(3057, 20, 9, 'index'),
(3058, 20, 9, 'update'),
(3059, 20, 9, 'view'),
(3060, 20, 9, ''),
(3061, 20, 12, 'view'),
(3062, 20, 12, ''),
(3864, 17, 24, 'index'),
(3865, 17, 24, ''),
(3866, 17, 64, ''),
(3867, 17, 65, 'print_kwitansi'),
(3868, 17, 65, 'index'),
(3869, 17, 65, 'add'),
(3870, 17, 65, 'option_insurance'),
(3871, 17, 65, 'add_payment'),
(3872, 17, 65, 'add_card_payment'),
(3873, 17, 65, 'payment_returns'),
(3874, 17, 65, 'add_person'),
(3875, 17, 65, 'load_person'),
(3876, 17, 65, 'load_item'),
(3877, 17, 65, 'print_trx'),
(3878, 17, 65, 'load_suspend_trx'),
(3879, 17, 65, 'load_all_data_trx'),
(3880, 17, 65, 'report'),
(3881, 17, 65, 'delete_transaction'),
(3882, 17, 65, 'export_data'),
(3883, 17, 65, ''),
(3884, 17, 82, 'index'),
(3885, 17, 82, 'option_insurance'),
(3886, 17, 82, 'add'),
(3887, 17, 82, 'add_payment'),
(3888, 17, 82, 'add_card_payment'),
(3889, 17, 82, 'payment_returns'),
(3890, 17, 82, 'add_dokter'),
(3891, 17, 82, 'load_dokter'),
(3892, 17, 82, 'add_pasien'),
(3893, 17, 82, 'load_pasien'),
(3894, 17, 82, 'load_item'),
(3895, 17, 82, 'print_trx'),
(3896, 17, 82, 'print_kwitansi'),
(3897, 17, 82, 'print_etiket'),
(3898, 17, 82, 'load_suspend_trx'),
(3899, 17, 82, 'load_all_data_trx'),
(3900, 17, 82, 'report'),
(3901, 17, 82, 'delete_transaction'),
(3902, 17, 82, 'export_data'),
(3903, 17, 82, ''),
(3904, 17, 53, ''),
(3905, 17, 55, 'index'),
(3906, 17, 55, 'add'),
(3907, 17, 55, 'update'),
(3908, 17, 55, ''),
(3909, 17, 56, 'index'),
(3910, 17, 56, 'add'),
(3911, 17, 56, 'update'),
(3912, 17, 56, ''),
(3913, 17, 57, 'index'),
(3914, 17, 57, 'add'),
(3915, 17, 57, 'update'),
(3916, 17, 57, 'delete'),
(3917, 17, 57, 'act_publish'),
(3918, 17, 57, 'act_unpublish'),
(3919, 17, 57, 'act_delete'),
(3920, 17, 57, ''),
(3921, 17, 58, 'index_person'),
(3922, 17, 58, 'index'),
(3923, 17, 58, 'add'),
(3924, 17, 58, 'update'),
(3925, 17, 58, 'delete'),
(3926, 17, 58, 'act_publish'),
(3927, 17, 58, 'act_unpublish'),
(3928, 17, 58, 'act_delete'),
(3929, 17, 58, ''),
(3930, 17, 59, 'index'),
(3931, 17, 59, 'add'),
(3932, 17, 59, 'update'),
(3933, 17, 59, 'delete'),
(3934, 17, 59, 'act_publish'),
(3935, 17, 59, 'act_unpublish'),
(3936, 17, 59, 'act_delete'),
(3937, 17, 59, ''),
(3938, 17, 60, 'index'),
(3939, 17, 60, 'add'),
(3940, 17, 60, 'update'),
(3941, 17, 60, 'delete'),
(3942, 17, 60, 'act_publish'),
(3943, 17, 60, 'act_unpublish'),
(3944, 17, 60, 'act_delete'),
(3945, 17, 60, ''),
(3946, 17, 61, 'index'),
(3947, 17, 61, 'add'),
(3948, 17, 61, 'update'),
(3949, 17, 61, 'delete'),
(3950, 17, 61, 'act_publish'),
(3951, 17, 61, 'act_unpublish'),
(3952, 17, 61, 'act_delete'),
(3953, 17, 61, ''),
(3954, 17, 62, 'index'),
(3955, 17, 62, 'add'),
(3956, 17, 62, 'update'),
(3957, 17, 62, 'delete'),
(3958, 17, 62, 'act_publish'),
(3959, 17, 62, 'act_unpublish'),
(3960, 17, 62, 'act_delete'),
(3961, 17, 62, ''),
(3962, 17, 63, 'index'),
(3963, 17, 63, 'add'),
(3964, 17, 63, 'update'),
(3965, 17, 63, 'delete'),
(3966, 17, 63, 'act_publish'),
(3967, 17, 63, 'act_unpublish'),
(3968, 17, 63, 'act_delete'),
(3969, 17, 63, ''),
(3970, 17, 93, 'index'),
(3971, 17, 93, 'add'),
(3972, 17, 93, 'update'),
(3973, 17, 93, ''),
(3974, 17, 94, 'index'),
(3975, 17, 94, 'add'),
(3976, 17, 94, 'update'),
(3977, 17, 94, 'delete'),
(3978, 17, 94, 'act_publish'),
(3979, 17, 94, 'act_unpublish'),
(3980, 17, 94, 'act_delete'),
(3981, 17, 94, ''),
(3982, 17, 67, ''),
(3983, 17, 113, 'index'),
(3984, 17, 113, 'act_add_to_plan'),
(3985, 17, 113, ''),
(3986, 17, 68, 'index'),
(3987, 17, 68, 'add'),
(3988, 17, 68, 'add_supplier'),
(3989, 17, 68, 'load_supplier'),
(3990, 17, 68, 'load_item'),
(3991, 17, 68, 'modal_information'),
(3992, 17, 68, 'print_trx'),
(3993, 17, 68, 'load_suspend_trx'),
(3994, 17, 68, 'load_all_data_trx'),
(3995, 17, 68, ''),
(3996, 17, 69, 'index'),
(3997, 17, 69, 'add'),
(3998, 17, 69, 'load_item'),
(3999, 17, 69, 'add_supplier'),
(4000, 17, 69, 'load_supplier'),
(4001, 17, 69, 'modal_information'),
(4002, 17, 69, 'load_all_data_trx'),
(4003, 17, 69, 'load_all_data_po_trx'),
(4004, 17, 69, ''),
(4005, 17, 83, 'index'),
(4006, 17, 83, 'add'),
(4007, 17, 83, 'load_item'),
(4008, 17, 83, 'modal_information'),
(4009, 17, 83, 'load_all_data_trx'),
(4010, 17, 83, ''),
(4011, 17, 114, 'index'),
(4012, 17, 114, 'add'),
(4013, 17, 114, 'load_item'),
(4014, 17, 114, 'add_supplier'),
(4015, 17, 114, 'load_supplier'),
(4016, 17, 114, 'modal_information'),
(4017, 17, 114, 'load_all_data_trx'),
(4018, 17, 114, 'load_all_data_po_trx'),
(4019, 17, 114, 'report'),
(4020, 17, 114, 'delete_transaction'),
(4021, 17, 114, ''),
(4022, 17, 110, ''),
(4023, 17, 71, 'report'),
(4024, 17, 71, 'export_data'),
(4025, 17, 71, ''),
(4026, 17, 73, 'report'),
(4027, 17, 73, 'export_data'),
(4028, 17, 73, ''),
(4029, 17, 88, 'payment'),
(4030, 17, 88, 'export_payment_data'),
(4031, 17, 88, ''),
(4032, 17, 89, 'payment_cash'),
(4033, 17, 89, 'export_payment_cash_data'),
(4034, 17, 89, ''),
(4035, 17, 90, 'payment_card'),
(4036, 17, 90, 'export_payment_card_data'),
(4037, 17, 90, ''),
(4038, 17, 111, 'index'),
(4039, 17, 111, 'create'),
(4040, 17, 111, 'update'),
(4041, 17, 111, 'view'),
(4042, 17, 111, 'delete'),
(4043, 17, 111, 'delete_selected_item'),
(4044, 17, 111, 'publish_selected_item'),
(4045, 17, 111, 'unpublish_selected_item'),
(4046, 17, 111, ''),
(4047, 17, 112, 'index'),
(4048, 17, 112, 'create'),
(4049, 17, 112, 'update'),
(4050, 17, 112, 'view'),
(4051, 17, 112, 'delete'),
(4052, 17, 112, 'delete_selected_item'),
(4053, 17, 112, 'publish_selected_item'),
(4054, 17, 112, 'unpublish_selected_item'),
(4055, 17, 112, ''),
(4056, 17, 117, ''),
(4057, 17, 98, 'index'),
(4058, 17, 98, 'export_data'),
(4059, 17, 98, ''),
(4060, 17, 101, 'arus_stok_history'),
(4061, 17, 101, 'export_arus_stok_history_data'),
(4062, 17, 101, ''),
(4063, 17, 11, ''),
(4064, 17, 9, 'index'),
(4065, 17, 9, 'update'),
(4066, 17, 9, 'view'),
(4067, 17, 9, ''),
(4068, 17, 12, 'update'),
(4069, 17, 12, 'view'),
(4070, 17, 12, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_administrator_privilege_label`
--

CREATE TABLE `site_administrator_privilege_label` (
  `privilege_label_id` int(10) UNSIGNED NOT NULL,
  `privilege_label_module` varchar(200) NOT NULL,
  `privilege_label_class` varchar(200) NOT NULL,
  `privilege_label_method` varchar(200) NOT NULL,
  `privilege_label_name` varchar(30) NOT NULL,
  `privilege_label_desc` text,
  `privilege_label_is_hidden` int(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_administrator_privilege_label`
--

INSERT INTO `site_administrator_privilege_label` (`privilege_label_id`, `privilege_label_module`, `privilege_label_class`, `privilege_label_method`, `privilege_label_name`, `privilege_label_desc`, `privilege_label_is_hidden`) VALUES
(2, 'dashboard', 'admin', 'service_rest_by_request', 'Service Index', '', 0),
(3, 'product_catalog', 'admin', 'product', 'Data Product', '', 0),
(4, 'product_catalog', 'admin', 'category', 'Data Category', '', 0),
(5, 'page', 'admin', 'index', 'Data Static Content', '', 0),
(6, 'page', 'admin', 'set_permalink', 'Ign Permalink', '', 0),
(7, 'page', 'admin', 'field_page_content', 'Ign Field Content', '', 0),
(8, 'page', 'admin', '_callback_webpage_url', 'Ign Callback Url', '', 0),
(9, 'image_slide', 'admin', 'index', 'Data Image Slider', '', 0),
(10, 'support', 'admin', 'index', 'Data Live Chat', '', 0),
(11, 'portfolio', 'admin', 'category', 'Data Category Potfolio', '', 0),
(12, 'portfolio', 'admin', 'set_permalink_category', 'Ign Permalink', '', 0),
(13, 'portfolio', 'admin', 'portfolio', 'Data Portfolio', '', 0),
(14, 'portfolio', 'admin', 'set_permalink_portfolio', 'Ign Permalink', '', 0),
(15, 'portfolio', 'admin', '_callback_category_url', 'Ign. Callback category url', '', 0),
(16, 'portfolio', 'admin', '_callback_portfolio_url', 'Ign Callback portfolio Url', '', 0),
(17, 'testimonial_admin', 'admin', 'index', 'Data Testimonial', '', 0),
(18, 'widget_content', 'admin', 'index', 'Data Widget Static', '', 0),
(19, 'admin_group', 'admin', 'index', 'Data Group', '', 0),
(20, 'admin_group', 'admin', 'create', 'Tambah Group', '', 0),
(21, 'admin_group', 'admin', 'update', 'Update Group', '', 0),
(22, 'admin_group', 'admin', 'set_privilege', 'Set Privilege Group', '', 0),
(23, 'admin_group', 'admin', 'view', 'Detail Group', '', 0),
(24, 'admin_group', 'admin', 'delete', 'Delete Group', '', 0),
(25, 'admin_group', 'admin', 'delete_selected_item', 'Delete Selected Item', '', 0),
(26, 'admin_group', 'admin', 'publish_selected_item', 'Publish Group', '', 0),
(27, 'admin_group', 'admin', 'unpublish_selected_item', 'Unpublish Group', '', 0),
(28, 'admin_user', 'admin', 'index', 'Data User', '', 0),
(29, 'admin_user', 'admin', 'create', 'Tambah User', '', 0),
(30, 'admin_user', 'admin', 'update', 'Update User', '', 0),
(31, 'admin_user', 'admin', 'view', 'Detail User', '', 0),
(32, 'admin_user', 'admin', 'delete', 'Delete User', '', 0),
(33, 'admin_user', 'admin', 'delete_selected_item', 'Delete Selected User', '', 0),
(34, 'admin_user', 'admin', 'publish_selected_item', 'Publish User', '', 0),
(35, 'admin_user', 'admin', 'unpublish_selected_item', 'Unpublish User', '', 0),
(38, 'admin_menu', 'admin', 'index', 'Data Menu Admin', '', 0),
(39, 'admin_menu', 'admin', 'create', 'Create Menu Admin', '', 0),
(40, 'admin_menu', 'admin', 'update', 'Update Menu Admin', '', 0),
(41, 'admin_menu', 'admin', 'view', 'Detail Menu Admin', '', 0),
(42, 'admin_menu', 'admin', 'delete', 'Delete Menu Admin', '', 0),
(43, 'admin_menu', 'admin', 'delete_selected_item', 'Delete Selected', '', 0),
(44, 'admin_menu', 'admin', 'publish_selected_item', 'Publish Selected', '', 0),
(45, 'admin_menu', 'admin', 'unpublish_selected_item', 'Unpublish Selected', '', 0),
(46, 'dashboard', 'admin', 'index', 'Dashboard', '', 0),
(47, 'admin_group_menu_label', 'admin', 'index', 'List Module', '', 0),
(48, 'admin_group_menu_label', 'admin', 'index_filter', 'List Method by Module', '', 0),
(49, 'admin_group_menu_label', 'admin', 'ign_index_filter', 'Ign Set Link', '', 1),
(50, 'admin_config', 'admin', 'update', 'Update', '', 0),
(51, 'admin_config', 'admin', 'view', 'Detail', '', 0),
(52, 'gis_kml', 'admin', 'index', 'List Data', '', 0),
(53, 'gis_kml', 'admin', 'create', 'Create Data', '', 0),
(54, 'gis_kml', 'admin', 'update', 'Update Data', '', 0),
(55, 'gis_kml', 'admin', 'view', 'View Data', '', 0),
(56, 'gis_kml', 'admin', 'delete', 'Delete Once', '', 0),
(57, 'gis_kml', 'admin', 'delete_selected_item', 'Delete Selected', '', 0),
(58, 'tracer', 'admin_tracer_answer', 'detail', 'Detail Kuesioner', '', 0),
(59, 'tracer', 'admin_tracer_answer', 'list_answer', 'List Kuesioner', '', 0),
(60, 'tracer', 'admin_tracer_answer_indicator', 'detail', 'Detail Kuesioner', '', 0),
(61, 'tracer', 'admin_tracer_statistic', 'tracer', 'Statistik Tracer', '', 0),
(62, 'tracer', 'admin_tracer_statistic', 'kuesioner', 'Statistik Kuesioner', '', 0),
(63, 'document', 'admin', 'index', 'List Data', '', 0),
(64, 'document', 'admin', 'get_data', 'ignore get data', '', 1),
(65, 'document', 'admin', '__get', 'ignore get', '', 1),
(66, 'document', 'admin', 'export_data', 'Export Excel', '', 0),
(67, 'document', 'admin', 'act_index', 'ignore_action', '', 1),
(68, 'document', 'admin', 'add', 'Tambah', '', 0),
(69, 'document', 'admin', 'edit', 'Edit', '', 0),
(70, 'document', 'admin', 'act_edit', 'ignore_action', '', 1),
(71, 'document', 'admin', 'kode_check', 'Dependency Tambah & Edit', '', 0),
(72, 'supplier', 'admin', 'index', 'List Data', '', 0),
(73, 'supplier', 'admin', 'act_index', 'ignore_action', '', 1),
(74, 'supplier', 'admin', 'add', 'Tambah', '', 0),
(75, 'supplier', 'admin', 'edit', 'Edit', '', 0),
(76, 'supplier', 'admin', 'act_edit', 'ignore_action', '', 1),
(77, 'supplier', 'admin', 'kode_check', 'Dependency Tambah & Edit', '', 0),
(78, 'supplier', 'admin', 'nama_check', 'Dependency Tambah & Edit', '', 0),
(79, 'supplier', 'admin', 'export_data', 'Export Data', '', 0),
(80, 'customer', 'admin', 'index', 'List Data', '', 0),
(81, 'customer', 'admin', 'act_index', 'ignore_action', '', 1),
(82, 'customer', 'admin', 'add', 'Tambah', '', 0),
(83, 'customer', 'admin', 'edit', 'Edit', '', 0),
(84, 'customer', 'admin', 'act_edit', 'ignore_action', '', 1),
(85, 'customer', 'admin', 'kode_check', 'Dependency Tambah & Edit', '', 0),
(86, 'customer', 'admin', 'nama_check', 'Dependency Tambah & Edit', '', 0),
(87, 'customer', 'admin', 'export_data', 'Export Data', '', 0),
(88, 'satuan', 'admin', 'index', 'List Data', '', 0),
(89, 'satuan', 'admin', 'add', 'Tambah', '', 0),
(90, 'satuan', 'admin', 'act_index', 'ignore_action', '', 1),
(91, 'satuan', 'admin', 'edit', 'Edit', '', 0),
(92, 'satuan', 'admin', 'act_edit', 'Ignore Ac', '', 1),
(93, 'satuan', 'admin', 'nama_check', 'Dependency Tambah & Edit', '', 0),
(94, 'satuan', 'admin', 'export_data', 'Export Data', '', 0),
(95, 'master_stock', 'admin', 'index', 'List Data', '', 0),
(96, 'master_stock', 'admin', 'act_index', 'ignore_action', '', 1),
(97, 'master_stock', 'admin', 'add', 'Tambah', '', 0),
(98, 'master_stock', 'admin', 'edit', 'Edit', '', 0),
(99, 'master_stock', 'admin', 'act_edit', 'ignore_action', '', 1),
(100, 'master_stock', 'admin', 'kode_check', 'Dependency Tambah ', '', 0),
(101, 'master_stock', 'admin', 'export_data', 'Export Data', '', 0),
(102, 'pemasukan_eksternal', 'admin', 'index', 'List Data', '', 0),
(103, 'pemasukan_eksternal', 'admin', 'add', 'Tambah', '', 0),
(104, 'pemasukan_eksternal', 'admin', 'edit', 'Edit', '', 0),
(105, 'pengeluaran_internal', 'admin', 'index', 'List Data', '', 0),
(106, 'pengeluaran_internal', 'admin', 'add', 'Tambah ', '', 0),
(107, 'pengeluaran_internal', 'admin', 'edit', 'Edit', '', 0),
(108, 'pemasukan_internal', 'admin', 'index', 'List Data', '', 0),
(109, 'pemasukan_internal', 'admin', 'add', 'Tambah', '', 0),
(110, 'pemasukan_internal', 'admin', 'edit', 'Edit', '', 0),
(111, 'pengeluaran_eksternal', 'admin', 'index', 'List Data', '', 0),
(112, 'pengeluaran_eksternal', 'admin', 'add', 'Tambah', '', 0),
(113, 'pengeluaran_eksternal', 'admin', 'edit', 'Edit', '', 0),
(114, 'stock_opname', 'admin', 'index', 'List Da', '', 0),
(115, 'stock_opname', 'admin', 'add', 'Tambah', '', 0),
(116, 'stock_opname', 'admin', 'edit', 'Edit', '', 0),
(117, 'stock_adjustment', 'admin', 'index', 'List Data', '', 0),
(118, 'stock_adjustment', 'admin', 'add', 'Tambah ', '', 0),
(119, 'stock_adjustment', 'admin', 'edit', 'Edi', '', 0),
(120, 'import_transaksi', 'admin', 'index', 'Import', '', 0),
(121, 'import_transaksi', 'admin', 'preview_import', 'Dependency Import', '', 0),
(122, 'import_transaksi', 'admin', 'saving_data', 'Dependency Import', '', 0),
(123, 'approval_pemasukan_eksternal', 'admin', 'index', 'Approval Pemasukan Eksternal', '', 0),
(124, 'approval_pengeluaran_internal', 'admin', 'index', 'Approval Pengeluaran Internal', '', 0),
(125, 'approval_pemasukan_internal', 'admin', 'index', 'Approval Pemasukan Internal', '', 0),
(126, 'approval_pengeluaran_eksternal', 'admin', 'index', 'Approval Pengeluaran Eksternal', '', 0),
(127, 'approval_stock_opname', 'admin', 'index', 'Approval Stok Opname', '', 0),
(128, 'approval_stock_adjustment', 'admin', 'index', 'Approval Stok Adjustment', '', 0),
(129, 'closing', 'admin', 'index', 'List Data', '', 0),
(130, 'closing', 'admin', 'add', 'Trx Closing', '', 0),
(131, 'closing', 'admin', 'export_data', 'Export Data', '', 0),
(132, 'report_pemasukan_eksternal', 'admin', 'index', 'Preview Report', '', 0),
(133, 'report_pemasukan_eksternal', 'admin', 'export_data', 'Export Data', '', 0),
(134, 'report_pengeluaran_internal', 'admin', 'index', 'Preview Report', '', 0),
(135, 'report_pengeluaran_internal', 'admin', 'export_data', 'Export Data', '', 0),
(136, 'report_pemasukan_internal', 'admin', 'index', 'Preview Report', '', 0),
(137, 'report_pemasukan_internal', 'admin', 'export_data', 'Export Data', '', 0),
(138, 'report_pengeluaran_eksternal', 'admin', 'index', 'Preview Report', '', 0),
(139, 'report_pengeluaran_eksternal', 'admin', 'export_data', 'Export Data', '', 0),
(140, 'report_wip', 'admin', 'index', 'Preview Report', '', 0),
(141, 'report_wip', 'admin', 'export_data', 'Export Data', '', 0),
(142, 'report_mutasi_bahan_penolong', 'admin', 'index', 'Preview Report', '', 0),
(143, 'report_mutasi_bahan_penolong', 'admin', 'export_data', 'Export Data', '', 0),
(144, 'report_mutasi_bahan_sisa', 'admin', 'index', 'Preview', '', 0),
(145, 'report_mutasi_bahan_sisa', 'admin', 'export_data', 'Export Data', '', 0),
(146, 'report_mutasi_barang_jadi', 'admin', 'index', 'Preview Report', '', 0),
(147, 'report_mutasi_barang_jadi', 'admin', 'export_data', 'Export Data', '', 0),
(148, 'report_mutasi_mesin', 'admin', 'index', 'Preview Report', '', 0),
(149, 'report_mutasi_mesin', 'admin', 'export_data', 'Export Data', '', 0),
(150, 'report_mutasi_bahan_baku', 'admin', 'index', 'Preview Report', '', 0),
(151, 'report_mutasi_bahan_baku', 'admin', 'export_data', 'Export', '', 0),
(152, 'report_mutasi_bbbp', 'admin', 'export_data', 'Export Data', '', 0),
(153, 'report_mutasi_bbbp', 'admin', 'index', 'Preview Report', '', 0),
(154, 'closing', 'admin', 'unclosing', 'Trx Unclosing', '', 0),
(155, 'closing', 'admin', 'act_unclosing', 'Dependency Unclosing', '', 0),
(156, 'closing', 'admin', 'period_check', 'Dependency Unclosin', '', 0),
(157, 'unapproval_pemasukan_eksternal', 'admin', 'index', 'Unapproval Pemasukan Eksternal', '', 0),
(158, 'unapproval_pengeluaran_internal', 'admin', 'index', 'Unapproval Pengeluaran Interna', '', 0),
(159, 'unapproval_pemasukan_internal', 'admin', 'index', 'Unapproval Pemasukan Internal ', '', 0),
(160, 'unapproval_pengeluaran_eksternal', 'admin', 'index', 'Unapproval Pengeluaran Ekstern', '', 0),
(161, 'unapproval_stock_opname', 'admin', 'index', 'Unapproval Stok Opname ', '', 0),
(162, 'unapproval_stock_adjustment', 'admin', 'index', 'Unapproval Stok Adjustment ', '', 0),
(163, 'insurance', 'admin', 'index', 'Daftar Data', NULL, 0),
(164, 'insurance', 'admin', 'add', 'Tambah Data', NULL, 0),
(165, 'insurance', 'admin', 'update', 'Edit Data', NULL, 0),
(166, 'insurance', 'admin', 'view', 'Detil', NULL, 0),
(167, 'jasa_apoteker', 'admin', 'index', 'Daftar Data', NULL, 0),
(168, 'jasa_apoteker', 'admin', 'add', 'Tambah Data', NULL, 0),
(169, 'jasa_apoteker', 'admin', 'update', 'Edit Data', NULL, 0),
(170, 'pabrik', 'admin', 'index', 'Daftar Data', NULL, 0),
(171, 'pabrik', 'admin', 'add', 'Tambah Data', NULL, 0),
(172, 'pabrik', 'admin', 'update', 'Edit Data', NULL, 0),
(173, 'supplier', 'admin', 'update', 'Edit', NULL, 0),
(174, 'supplier', 'admin', 'delete', 'Hapus per Data', NULL, 0),
(175, 'supplier', 'admin', 'act_publish', 'Set Tampilkan', NULL, 0),
(176, 'supplier', 'admin', 'act_unpublish', 'Set Draft', NULL, 0),
(177, 'supplier', 'admin', 'act_delete', 'Hapus Data Terseleksi', NULL, 0),
(178, 'dokter', 'admin', 'index', 'Daftar Data', NULL, 0),
(179, 'dokter', 'admin', 'add', 'Tambah Data', NULL, 0),
(180, 'dokter', 'admin', 'update', 'Edit Data', NULL, 0),
(181, 'dokter', 'admin', 'delete', 'Hapus per Data', NULL, 0),
(182, 'dokter', 'admin', 'act_publish', 'Set Tampilkan', NULL, 0),
(183, 'dokter', 'admin', 'act_unpublish', 'Set Draft', NULL, 0),
(184, 'dokter', 'admin', 'act_delete', 'Hapus Data Terseleksi', NULL, 0),
(185, 'pasien', 'admin', 'index', 'Daftar Data', NULL, 0),
(186, 'pasien', 'admin', 'add', 'Tambah Data', NULL, 0),
(187, 'pasien', 'admin', 'update', 'Edit Data', NULL, 0),
(188, 'pasien', 'admin', 'delete', 'Hapus per Data', NULL, 0),
(189, 'pasien', 'admin', 'act_publish', 'Set Tampilkan', NULL, 0),
(190, 'pasien', 'admin', 'act_unpublish', 'Set Draft', NULL, 0),
(191, 'pasien', 'admin', 'act_delete', 'Hapus Data Terseleksi', NULL, 0),
(192, 'apoteker', 'admin', 'index', 'Daftar Data', NULL, 0),
(193, 'apoteker', 'admin', 'add', 'Tambah Data', NULL, 0),
(194, 'apoteker', 'admin', 'update', 'Edit Data', NULL, 0),
(195, 'apoteker', 'admin', 'delete', 'Hapus per Data', NULL, 0),
(196, 'apoteker', 'admin', 'act_publish', 'Set Tampilkan', NULL, 0),
(197, 'apoteker', 'admin', 'act_unpublish', 'Set Draft', NULL, 0),
(198, 'apoteker', 'admin', 'act_delete', 'Hapus Data Terseleksi', NULL, 0),
(199, 'asisten_apoteker', 'admin', 'index', 'Daftar Data', NULL, 0),
(200, 'asisten_apoteker', 'admin', 'add', 'Tambah Data', NULL, 0),
(201, 'asisten_apoteker', 'admin', 'update', 'Edit Data', NULL, 0),
(202, 'asisten_apoteker', 'admin', 'delete', 'Hapus per Data', NULL, 0),
(203, 'asisten_apoteker', 'admin', 'act_publish', 'Set Tampilkan', NULL, 0),
(204, 'asisten_apoteker', 'admin', 'act_unpublish', 'Set Draft', NULL, 0),
(205, 'asisten_apoteker', 'admin', 'act_delete', 'Hapus Data Terseleksi', NULL, 0),
(206, 'pelanggan', 'admin', 'index', 'Daftar Data', NULL, 0),
(207, 'pelanggan', 'admin', 'add', 'Tambah Data', NULL, 0),
(208, 'pelanggan', 'admin', 'update', 'Edit Data', NULL, 0),
(209, 'pelanggan', 'admin', 'delete', 'Hapus per Data', NULL, 0),
(210, 'pelanggan', 'admin', 'act_publish', 'Set Tampilkan', NULL, 0),
(211, 'pelanggan', 'admin', 'act_unpublish', 'Set Draft', NULL, 0),
(212, 'pelanggan', 'admin', 'act_delete', 'Hapus Data Terseleksi', NULL, 0),
(213, 'item', 'admin', 'index', 'Daftar Data', NULL, 0),
(214, 'item', 'admin', 'add', 'Tambah Data', NULL, 0),
(215, 'item', 'admin', 'update', 'Edit Data', NULL, 0),
(216, 'item', 'admin', 'delete', 'Hapus per Data', NULL, 0),
(217, 'item', 'admin', 'act_publish', 'Set Tampilkan', NULL, 0),
(218, 'item', 'admin', 'act_unpublish', 'Set Draft', NULL, 0),
(219, 'item', 'admin', 'act_delete', 'Hapus Data Terseleksi', NULL, 0),
(220, 'edc_machine', 'admin', 'index', 'Daftar Data', NULL, 0),
(221, 'edc_machine', 'admin', 'add', 'Tambah Data', NULL, 0),
(222, 'edc_machine', 'admin', 'update', 'Edit Data', NULL, 0),
(223, 'pemesanan', 'admin', 'index', 'Daftar Data', NULL, 0),
(224, 'pemesanan', 'admin', 'add', 'Dependency Trx', NULL, 0),
(225, 'pemesanan', 'admin', 'add_supplier', 'Tambah Supplier', NULL, 0),
(226, 'pemesanan', 'admin', 'load_supplier', 'Load Supplier', NULL, 0),
(227, 'pemesanan', 'admin', 'load_item', 'Load Item', NULL, 0),
(228, 'pemesanan', 'admin', 'modal_information', 'Dependency Trx', NULL, 0),
(229, 'pemesanan', 'admin', 'print_trx', 'Dependency Trx', NULL, 0),
(230, 'pemesanan', 'admin', 'load_suspend_trx', 'Load Trx Tertunda', NULL, 0),
(231, 'pemesanan', 'admin', 'load_all_data_trx', 'Load Trx', NULL, 0),
(232, 'pemesanan', 'admin', 'export_data', 'Download Excel', NULL, 0),
(233, 'pemesanan', 'admin', 'report', 'Preview ', NULL, 0),
(234, 'penerimaan', 'admin', 'index', 'Daftar Data', NULL, 0),
(235, 'penerimaan', 'admin', 'add', 'Dependency Trx', NULL, 0),
(236, 'penerimaan', 'admin', 'load_item', 'Dependency Trx', NULL, 0),
(237, 'penerimaan', 'admin', 'add_supplier', 'Tambah Supplier', NULL, 0),
(238, 'penerimaan', 'admin', 'load_supplier', 'Load Supplier', NULL, 0),
(239, 'penerimaan', 'admin', 'modal_information', 'Dependency Trx', NULL, 0),
(240, 'penerimaan', 'admin', 'load_all_data_trx', 'Load Trx', NULL, 0),
(241, 'penerimaan', 'admin', 'load_all_data_po_trx', 'Load Data PO', NULL, 0),
(242, 'penerimaan', 'admin', 'report', 'Preview', NULL, 0),
(243, 'penerimaan', 'admin', 'export_data', 'Download Excel', NULL, 0),
(244, 'stok', 'admin', 'index', 'Preview', NULL, 0),
(245, 'stok', 'admin', 'export_data', 'Download Excel', NULL, 0),
(246, 'stok', 'admin', 'detail', 'Preview', NULL, 0),
(247, 'stok', 'admin', 'export_detail_data', 'Download Excel', NULL, 0),
(248, 'stok', 'admin', 'expired', 'Preview ', NULL, 0),
(249, 'stok', 'admin', 'export_expired_data', 'Download Excel', NULL, 0),
(250, 'stock_opname', 'admin', 'load_item', 'Load Item', NULL, 0),
(251, 'stock_opname', 'admin', 'modal_information', 'Dependency Trx', NULL, 0),
(252, 'stock_opname', 'admin', 'load_all_data_trx', 'Load Trx', NULL, 0),
(253, 'stock_opname', 'admin', 'report', 'Preview', NULL, 0),
(254, 'stock_opname', 'admin', 'export_data', 'Download Excel', NULL, 0),
(255, 'trx_resep', 'admin', 'index', 'Daftar Data', NULL, 0),
(256, 'trx_non_resep', 'admin', 'index', 'Daftar Data', NULL, 0),
(257, 'trx_non_resep', 'admin', 'add', 'Dependency Trx', NULL, 0),
(258, 'trx_non_resep', 'admin', 'add_payment', 'Dependency Trx', NULL, 0),
(259, 'trx_non_resep', 'admin', 'add_card_payment', 'Dependency Trx', NULL, 0),
(260, 'trx_non_resep', 'admin', 'payment_returns', 'Dependency Trx', NULL, 0),
(261, 'trx_non_resep', 'admin', 'add_person', 'Tambah Pelanggan', NULL, 0),
(262, 'trx_non_resep', 'admin', 'load_person', 'Load Pelanggan', NULL, 0),
(263, 'trx_non_resep', 'admin', 'load_item', 'Load Item', NULL, 0),
(264, 'trx_non_resep', 'admin', 'print_trx', 'Dependency Trx', NULL, 0),
(265, 'trx_non_resep', 'admin', 'load_suspend_trx', 'Load Suspend', NULL, 0),
(266, 'trx_non_resep', 'admin', 'load_all_data_trx', 'Load Trx', NULL, 0),
(267, 'trx_non_resep', 'admin', 'report', 'Preview', NULL, 0),
(268, 'trx_non_resep', 'admin', 'export_data', 'Download Excel', NULL, 0),
(269, 'trx_resep', 'admin', 'add', 'Dependency Trx', NULL, 0),
(270, 'trx_resep', 'admin', 'add_payment', 'Dependency Trx', NULL, 0),
(271, 'trx_resep', 'admin', 'add_card_payment', 'Dependency Trx', NULL, 0),
(272, 'trx_resep', 'admin', 'payment_returns', 'Dependency Trx', NULL, 0),
(273, 'trx_resep', 'admin', 'add_dokter', 'Tambah Dokter', NULL, 0),
(274, 'trx_resep', 'admin', 'load_dokter', 'Load Dokter', NULL, 0),
(275, 'trx_resep', 'admin', 'add_pasien', 'Tambah Pasien', NULL, 0),
(276, 'trx_resep', 'admin', 'load_pasien', 'Load Pasien', NULL, 0),
(277, 'trx_resep', 'admin', 'load_item', 'Load Item', NULL, 0),
(278, 'trx_resep', 'admin', 'print_trx', 'Dependency Trx', NULL, 0),
(279, 'trx_resep', 'admin', 'load_suspend_trx', 'Load Suspend Trx', NULL, 0),
(280, 'trx_resep', 'admin', 'load_all_data_trx', 'Load Trx', NULL, 0),
(281, 'trx_resep', 'admin', 'report', 'Preview', NULL, 0),
(282, 'trx_resep', 'admin', 'export_data', 'Download Excel', NULL, 0),
(283, 'import_item', 'admin', 'index', 'Master Item', NULL, 0),
(284, 'report', 'admin', 'payment', 'Preview', NULL, 0),
(285, 'report', 'admin', 'export_payment_data', 'Download Excel', NULL, 0),
(286, 'report', 'admin', 'payment_cash', 'Preview', NULL, 0),
(287, 'report', 'admin', 'export_payment_cash_data', 'Download Excel', NULL, 0),
(288, 'report', 'admin', 'payment_card', 'Preview', NULL, 0),
(289, 'report', 'admin', 'export_payment_card_data', 'Download Excel', NULL, 0),
(290, 'import_item', 'admin', 'preview_import', 'Dependency', NULL, 0),
(291, 'import_item', 'admin', 'saving_data', 'Dependency', NULL, 0),
(292, 'distribusi_unit', 'admin', 'index', 'Daftar Data', NULL, 0),
(293, 'distribusi_unit', 'admin', 'add', 'Dependency Trx', NULL, 0),
(294, 'distribusi_unit', 'admin', 'load_item', 'Load Item', NULL, 0),
(295, 'distribusi_unit', 'admin', 'modal_information', 'Dependency Trx', NULL, 0),
(296, 'distribusi_unit', 'admin', 'print_trx', 'Dependency Trx', NULL, 0),
(297, 'distribusi_unit', 'admin', 'load_suspend_trx', 'Load Suspend Trx', NULL, 0),
(298, 'distribusi_unit', 'admin', 'load_store', 'Load Unit/Cabang', NULL, 0),
(299, 'distribusi_unit', 'admin', 'load_all_data_trx', 'Load Trx', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_administrator_store`
--

CREATE TABLE `site_administrator_store` (
  `administrator_store_id` int(10) UNSIGNED NOT NULL,
  `administrator_store_administrator_id` int(10) UNSIGNED NOT NULL,
  `administrator_store_store_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_administrator_store`
--

INSERT INTO `site_administrator_store` (`administrator_store_id`, `administrator_store_administrator_id`, `administrator_store_store_id`) VALUES
(1, 7, 1),
(2, 10, 4),
(3, 11, 4),
(4, 12, 1),
(5, 13, 4),
(6, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_ask`
--

CREATE TABLE `site_ask` (
  `ask_id` int(10) UNSIGNED NOT NULL,
  `ask_par_id` int(10) UNSIGNED DEFAULT NULL,
  `ask_email` varchar(100) NOT NULL,
  `ask_full_name` varchar(100) NOT NULL,
  `ask_mobile_phone` varchar(30) DEFAULT NULL,
  `ask_subject` varchar(100) DEFAULT NULL,
  `ask_message` text NOT NULL,
  `ask_status` enum('unread','read','replied') NOT NULL DEFAULT 'unread',
  `ask_input_by` varchar(100) DEFAULT NULL,
  `ask_update_datetime` datetime DEFAULT NULL,
  `ask_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_ask`
--

INSERT INTO `site_ask` (`ask_id`, `ask_par_id`, `ask_email`, `ask_full_name`, `ask_mobile_phone`, `ask_subject`, `ask_message`, `ask_status`, `ask_input_by`, `ask_update_datetime`, `ask_timestamp`) VALUES
(1, NULL, 'zaharaulia@gmail.com', 'zahara', '085261141870', 'tanya harga -  - zahara', '\r\n               <p class=\"text-muted\" style=\"font-size:12px;\">Full Name: zahara<br />\r\n                  Email :  zaharaulia@gmail.com<br />\r\n                  Phone :  085261141870\r\n               </p>\r\n              <hr />\r\n               \r\n               ssdsghcfahgc', 'read', 'admin', '2016-12-24 01:37:28', '2016-12-15 03:59:10'),
(2, 1, 'noreply@kliniksyifa.id', 'admin', NULL, 'Re:tanya harga -  - zahara', '250000 bu', 'replied', 'admin', '2016-12-15 11:02:27', '2016-12-15 04:02:27'),
(3, NULL, 'anggoro.indonesia@gmail.com', 'Anggoro', '', 'Testing Kontak -  - Anggoro', '\r\n               <p class=\"text-muted\" style=\"font-size:12px;\">Full Name: Anggoro<br />\r\n                  Email :  anggoro.indonesia@gmail.com<br />\r\n                  Phone :  \r\n               </p>\r\n              <hr />\r\n               \r\n               Percobaan Bro', 'read', 'admin', '2018-01-12 23:54:04', '2018-01-12 16:53:55'),
(4, NULL, 'anggoro.indonesia@gmail.com', 'Tri Anggoro Kasih', '', 'Testing Web -  - Tri Anggoro Kasih', '\r\n               <p class=\"text-muted\" style=\"font-size:12px;\">Full Name: Tri Anggoro Kasih<br />\r\n                  Email :  anggoro.indonesia@gmail.com<br />\r\n                  Phone :  \r\n               </p>\r\n              <hr />\r\n               \r\n               Percobaan', 'read', 'admin', '2018-01-13 19:22:47', '2018-01-13 12:22:24'),
(5, NULL, 'setiyo10@gmail.com', 'setiyo', '0', ' - keluhan - setiyo', '\r\n               <p class=\"text-muted\" style=\"font-size:12px;\">Full Name: setiyo<br />\r\n                  Email :  setiyo10@gmail.com<br />\r\n                  Phone :  \r\n               </p>\r\n              <hr />\r\n               \r\n               terlalu bagus web e', 'unread', NULL, NULL, '2018-04-02 06:31:58'),
(6, NULL, 'setiyo10@gmail.com', 'setiyo', '0', ' - saran - setiyo', '\r\n               <p class=\"text-muted\" style=\"font-size:12px;\">Full Name: setiyo<br />\r\n                  Email :  setiyo10@gmail.com<br />\r\n                  Phone :  \r\n               </p>\r\n              <hr />\r\n               \r\n               Terlalu bagus bos', 'unread', NULL, NULL, '2018-04-02 06:47:02'),
(7, NULL, 'setiyo10@gmail.com', 'setiyo', '0', ' - saran - setiyo', '\r\n               <p class=\"text-muted\" style=\"font-size:12px;\">Full Name: setiyo<br />\r\n                  Email :  setiyo10@gmail.com<br />\r\n                  Phone :  \r\n               </p>\r\n              <hr />\r\n               \r\n               sdfsdfsdf', 'unread', NULL, NULL, '2018-04-02 08:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `site_bank_account`
--

CREATE TABLE `site_bank_account` (
  `bank_account_id` int(10) UNSIGNED NOT NULL,
  `bank_account_bank_id` int(10) UNSIGNED NOT NULL,
  `bank_account_name` varchar(50) NOT NULL,
  `bank_account_no` varchar(50) NOT NULL,
  `bank_account_is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data rekening bank perusahaan';

-- --------------------------------------------------------

--
-- Table structure for table `site_chat_ym`
--

CREATE TABLE `site_chat_ym` (
  `chat_ym_id` int(2) UNSIGNED NOT NULL,
  `chat_ym_full_name` varchar(100) NOT NULL,
  `chat_ym_email` varchar(100) NOT NULL,
  `chat_ym_mobilephone` varchar(20) DEFAULT NULL,
  `chat_ym_note` varchar(50) DEFAULT NULL,
  `chat_ym_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_chat_ym`
--

INSERT INTO `site_chat_ym` (`chat_ym_id`, `chat_ym_full_name`, `chat_ym_email`, `chat_ym_mobilephone`, `chat_ym_note`, `chat_ym_timestamp`) VALUES
(1, 'Tejo Murti', 'joe_engressia@yahoo.com', '03493434993', 'Admin', '2015-05-11 10:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `site_configuration`
--

CREATE TABLE `site_configuration` (
  `configuration_index` varchar(100) NOT NULL,
  `configuration_value` varchar(500) DEFAULT NULL,
  `configuration_note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_configuration`
--

INSERT INTO `site_configuration` (`configuration_index`, `configuration_value`, `configuration_note`) VALUES
('config_address', 'Jl. RA. Abu samah', NULL),
('config_city_name', 'Palembang', NULL),
('config_country_name', 'Indonesia', NULL),
('config_description', 'Klinik Syifa Medan. Poliklinik', NULL),
('config_email', 'sks-indonesia@gmail.com', NULL),
('config_favicon', 'assets/images/statics/favicon7.png', NULL),
('config_fax', '-', NULL),
('config_footer', 'Copyrights &copy; 2018. All Rights Reserved', NULL),
('config_keywords', 'Poliklinik, Klinik, Syifa, Syifa Medan, Medan', NULL),
('config_lat', '3.5755694126282735', NULL),
('config_link_login', 'http://jamaah.barayacsi.com/login', NULL),
('config_link_registrasi', 'http://jamaah.barayacsi.com/reg', NULL),
('config_lng', '98.67363604484126', NULL),
('config_logo', 'assets/images/statics/logo-sks.png', NULL),
('config_name', 'SKS Indonesia', NULL),
('config_phone', '0711-5710107', 'no telephone'),
('config_printername', 'Canon MP280 series', NULL),
('config_printout_footer', 'SEMOGA LEKAS SEMBUH', NULL),
('config_province_name', 'Sumatera Utara', NULL),
('config_reply_order', 'We are already scheduling your run.\r\n                                                    Please complete your payment, maximum\r\n                                                    2 x 24 hours.\r\n                                                    Check your email to further info payment.\r\n                                                    Thanks for running with us!\r\n                                                    Finish Strong and Enjoy JOGJA', NULL),
('config_tagline_link', 'http://localhost/enusoft/client/fctc/master_content/detail_event/tentang_kami', NULL),
('config_tagline_value', 'Tahukah kamu bahwa Indonesia merupakan satu-satunya Negara di Asia yang belum Aksesi FCTC (Framework Convention on Tobacco Control)?', NULL),
('config_telephone', '+687756789234', NULL),
('config_ym_offline', 'assets/images/statics/ym-off.jpg', NULL),
('config_ym_online', 'assets/images/statics/ym.jpg', NULL),
('engine_last_updated', '2016-11-25', 'Tanggal terakhir diupdate'),
('print_billing_transaction', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_distribusi_unit', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_inkaso', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_pemesanan', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_penerimaan', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_reception_retur_unit', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_reception_unit', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_retur_supplier', 'N', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_trx_non_resep', 'Y', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.'),
('print_trx_resep', 'Y', 'Y:setelah transaksi disimpan langsung cetak;N:transaksi tidak langsung cetak.');

-- --------------------------------------------------------

--
-- Table structure for table `site_download`
--

CREATE TABLE `site_download` (
  `download_id` int(10) UNSIGNED NOT NULL,
  `download_news_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'optional jika ada file dalam content',
  `download_foreign_id` int(10) UNSIGNED DEFAULT NULL,
  `download_title` varchar(200) NOT NULL,
  `download_permalink` varchar(200) NOT NULL,
  `download_content` text,
  `download_meta_tags` varchar(200) DEFAULT NULL,
  `download_meta_description` varchar(200) DEFAULT NULL,
  `download_file` varchar(100) DEFAULT NULL,
  `download_input_by` varchar(100) NOT NULL,
  `download_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_download`
--

INSERT INTO `site_download` (`download_id`, `download_news_id`, `download_foreign_id`, `download_title`, `download_permalink`, `download_content`, `download_meta_tags`, `download_meta_description`, `download_file`, `download_input_by`, `download_timestamp`) VALUES
(1, 213, NULL, 'Poli kecantikan 1', 'poli_kecantikan_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:42:38'),
(2, 214, NULL, 'Poli penyakit dalam 1', 'poli_penyakit_dalam_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:43:31'),
(3, 215, NULL, 'Poli kandungan 1', 'poli_kandungan_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:49:09'),
(4, 216, NULL, 'Poli anak 1', 'poli_anak_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:49:55'),
(5, 217, NULL, 'Poli cardio (otot) 1', 'poli_cardio_otot_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:51:14'),
(6, 218, NULL, 'Poli Gigi 1', 'poli_gigi_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:53:32'),
(7, 219, NULL, 'Poli Neuro (syaraf) 1', 'poli_neuro_syaraf_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:54:26'),
(8, 220, NULL, 'Poli THT 1', 'poli_tht_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-15 08:54:59'),
(9, 221, NULL, 'Coba 1', 'coba_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 04:13:55'),
(10, 222, NULL, 'This is a Standard post with a Youtube Video 1', 'this_is_a_standard_post_with_a_youtube_video_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 04:19:19'),
(11, 223, NULL, 'This is a Standard post with a Preview Image 1', 'this_is_a_standard_post_with_a_preview_image_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 04:20:50'),
(12, 224, NULL, 'This is a Standard post with a Preview Image 1', 'this_is_a_standard_post_with_a_preview_image_11479270117', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 04:21:35'),
(13, 225, NULL, 'This is a Standard post with a Preview Image 1', 'this_is_a_standard_post_with_a_preview_image_11479270177', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 04:22:57'),
(14, 226, NULL, 'Layanan Poli 1', 'layanan_poli_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:15:07'),
(15, 227, NULL, 'Poli Anak 1', 'poli_anak_11480563101', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:19:29'),
(16, 228, NULL, 'Poli Cardio 1', 'poli_cardio_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:20:20'),
(17, 229, NULL, 'Poli Neuro 1', 'poli_neuro_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:21:35'),
(18, 230, NULL, 'Poli THT 1', 'poli_tht_11482402185', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:23:49'),
(19, 231, NULL, 'Poli Kandungan 1', 'poli_kandungan_11482550981', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:25:03'),
(20, 232, NULL, 'Poli Penyakit Dalam 1', 'poli_penyakit_dalam_11482550391', NULL, NULL, NULL, 'assets/images/master_file/1482550391.jpg', 'admin', '2016-11-16 07:25:45'),
(21, 233, NULL, 'Poli Kecantikan 1', 'poli_kecantikan_11482805853', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:26:22'),
(22, 234, NULL, 'Poli Gigi 1', 'poli_gigi_11491468120', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:27:01'),
(23, 235, NULL, 'Apotek 1', 'apotek_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:46:39'),
(24, 236, NULL, 'Laboratorium 1', 'laboratorium_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 07:47:34'),
(25, 237, NULL, 'Jadwal Dokter 1', 'jadwal_dokter_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 08:04:06'),
(26, 238, NULL, 'Visi dan Misi 1', 'visi_dan_misi_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 08:13:16'),
(27, 239, NULL, 'Tentang Klinik 1', 'tentang_klinik_1', NULL, NULL, NULL, NULL, 'admin', '2016-11-16 08:13:53'),
(28, 240, NULL, 'Values 1', 'values_1', NULL, NULL, NULL, NULL, 'admin', '2016-12-15 04:36:01'),
(29, 241, NULL, 'CPayment 1', 'cpayment_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-09 11:54:57'),
(30, 242, NULL, '3 Keraguan Jamaah Terhadap Travel Haji & Umrah 1', '3_keraguan_jamaah_terhadap_travel_haji__umrah_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-09 12:22:35'),
(31, 243, NULL, 'Jaminan Rezeki Bagi Orang yang Menunaikan Ibadah Haji & Umroh 1', 'jaminan_rezeki_bagi_orang_yang_menunaikan_ibadah_haji__umroh_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-09 12:45:26'),
(32, 244, NULL, 'Berdoa Ditempat & Pada Waktu yang Mustajab 1', 'berdoa_ditempat__pada_waktu_yang_mustajab_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-09 12:51:39'),
(33, 245, NULL, 'Anda Ingin Pergi Umrah ?, Pastikan !! 1', 'anda_ingin_pergi_umrah__pastikan__1', NULL, NULL, NULL, NULL, 'admin', '2018-01-09 12:55:17'),
(34, 246, NULL, 'Sudah ada Niat kah Anda untuk menjadi Tamu Allah 1', 'sudah_ada_niat_kah_anda_untuk_menjadi_tamu_allah_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-09 12:56:03'),
(36, 248, NULL, 'Visi dan Misi 1', 'visi_dan_misi_11522954288', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 02:06:07'),
(37, 249, NULL, 'Legalitas Perusahaan 1', 'legalitas_perusahaan_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 02:31:47'),
(38, 250, NULL, 'Struktur Citra Sagara Inten 1', 'struktur_citra_sagara_inten_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 02:34:20'),
(39, 251, NULL, 'MARKETING PLAN 1', 'marketing_plan_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 03:12:21'),
(40, 252, NULL, 'KEUNTUNGAN 1', 'keuntungan_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 03:57:27'),
(41, 253, NULL, 'UPGRADE PAKET PREMIUM 1', 'upgrade_paket_premium_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 04:11:49'),
(42, 254, NULL, 'Mekanisme Pembayaran Ujroh 1', 'mekanisme_pembayaran_ujroh_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 04:16:09'),
(43, 255, NULL, 'PT. CITRA SAGARA INTEN 1', 'pt_citra_sagara_inten_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 07:34:29'),
(44, 256, NULL, 'CATATAN 1', 'catatan_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 07:40:30'),
(45, 257, NULL, 'PAKET TUNAI 1', 'paket_tunai_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 07:44:36'),
(46, 258, NULL, 'Harga Paket Tunai 1', 'harga_paket_tunai_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 07:46:28'),
(47, 259, NULL, 'Embarkasi 1', 'embarkasi_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 07:51:07'),
(48, 260, NULL, 'Persyaratan Umroh 1', 'persyaratan_umroh_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 07:51:54'),
(49, 261, NULL, 'Penduduk Indonesia Kurang Lebih 250 Juta Jiwa Lebih dari 200 jiwa beraga islam 1', 'penduduk_indonesia_kurang_lebih_250_juta_jiwa_lebih_dari_200_jiwa_beraga_islam_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 07:52:47'),
(50, 262, NULL, 'CPayment 1', 'cpayment_11515756254', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 11:20:35'),
(51, 263, NULL, 'Kemudahan Transaksi CPayment 1', 'kemudahan_transaksi_cpayment_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 11:21:45'),
(52, 264, NULL, 'Itenerary 1', 'itenerary_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-12 11:25:58'),
(53, 265, NULL, 'Data Jamaah 1', 'data_jamaah_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-13 14:39:01'),
(54, 266, NULL, 'Manivest Keberangkatan 1', 'manivest_keberangkatan_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-13 14:51:52'),
(55, 267, NULL, 'brosur sks', 'brosur_sks', NULL, NULL, NULL, 'assets/images/master_file/1522808031.jpg', 'admin', '2018-01-13 15:12:16'),
(56, 268, NULL, 'Sertifikaat', 'sertifikaat', NULL, NULL, NULL, 'assets/images/master_file/1515914790.png', 'admin', '2018-01-14 07:06:24'),
(57, 268, NULL, 'List Data', 'list_data', NULL, NULL, NULL, 'assets/images/master_file/1515914790.xlsx', 'admin', '2018-01-14 07:26:30'),
(58, 269, NULL, 'Cah Bumen 1', 'cah_bumen_1', NULL, NULL, NULL, NULL, 'admin', '2018-01-14 07:42:48'),
(59, 270, NULL, 'Data Peserta 2017', 'data_peserta_2017', NULL, NULL, NULL, 'assets/images/master_file/1515917107.xlsx', 'admin', '2018-01-14 08:05:07'),
(60, 271, NULL, 'EKO BY LION 9 HARI SURABAYA-JEDDAH 1', 'eko_by_lion_9_hari_surabayajeddah_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 02:35:09'),
(61, 272, NULL, 'EKO BY LION 9 HARI SURABAYA-MADINAH 1', 'eko_by_lion_9_hari_surabayamadinah_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 02:38:04'),
(62, 273, NULL, 'EKO BY LION 9 HARI SURABAYA-JEDDAH 1', 'eko_by_lion_9_hari_surabayajeddah_11522636929', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 02:39:04'),
(63, 274, NULL, 'EKO BY LION 9 HARI SURABAYA-MADINAH 1', 'eko_by_lion_9_hari_surabayamadinah_11522637074', NULL, NULL, NULL, 'assets/images/master_file/1522636834.jpg', 'admin', '2018-04-02 02:40:34'),
(64, 275, NULL, 'REGULER 7 MALAM 40-45 PAX(4 MEKKAH,3 MADINAH) 1', 'reguler_7_malam_4045_pax4_mekkah3_madinah_1', NULL, NULL, NULL, 'assets/images/master_file/1522637357.jpg', 'admin', '2018-04-02 02:45:39'),
(65, 276, NULL, 'REGULER 7 MALAM 35-39 PAX(4 MEKKAH,3 MADINAH) 1', 'reguler_7_malam_3539_pax4_mekkah3_madinah_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 02:46:49'),
(66, 277, NULL, 'REGULER 7 MALAM 30-34 PAX(4 MEKKAH,3 MADINAH) 1', 'reguler_7_malam_3034_pax4_mekkah3_madinah_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 02:47:32'),
(67, 278, NULL, 'REGULER 7 MALAM 25-29 PAX(4 MEKKAH,3 MADINAH) 1', 'reguler_7_malam_2529_pax4_mekkah3_madinah_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 02:48:14'),
(68, 279, NULL, 'Global Plugin 1', 'global_plugin_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 03:08:55'),
(69, 280, NULL, 'menu footer 1', 'menu_footer_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-02 07:29:06'),
(70, 267, NULL, 'brosur sks2', 'brosur_sks2', NULL, NULL, NULL, 'assets/images/master_file/1522808031.jpg', 'admin', '2018-04-04 02:13:51'),
(71, 281, NULL, 'masjidil haram', 'masjidil_haram', NULL, NULL, NULL, 'assets/images/master_file/1522823849.jpg', 'admin', '2018-04-04 02:16:12'),
(72, 281, NULL, 'umrah masjidil haram', 'umrah_masjidil_haram', NULL, NULL, NULL, 'assets/images/master_file/1522823850.jpeg', 'admin', '2018-04-04 02:16:12'),
(73, 282, NULL, 'Photo 1', 'photo_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 02:18:37'),
(74, 283, NULL, 'Brosur 1', 'brosur_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 03:17:55'),
(75, 284, NULL, 'Pendukung 1', 'pendukung_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 08:09:32'),
(76, 285, NULL, 'Maskapai 1', 'maskapai_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 08:59:08'),
(77, 286, NULL, 'Paket Reguler 1 1', 'paket_reguler_1_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 09:17:55'),
(78, 287, NULL, 'Paket Reguler 2 1', 'paket_reguler_2_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 09:18:47'),
(79, 288, NULL, 'Paket Reguler 3 1', 'paket_reguler_3_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 09:20:35'),
(80, 289, NULL, 'Information 1', 'information_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 12:22:49'),
(81, 290, NULL, 'Panduan 1', 'panduan_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-04 12:23:56'),
(82, 247, NULL, 'Profile 1', 'profile_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-05 02:11:10'),
(83, 291, NULL, 'Manajemen 1', 'manajemen_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-05 03:07:28'),
(84, 292, NULL, 'Rekening PT. SINAR KABAH SEMESTA 1', 'rekening_pt_sinar_kabah_semesta_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-05 19:39:20'),
(85, 293, NULL, '91 Calon Umrah Asal Sumatera Barat Telantar di Malaysia 1', '91_calon_umrah_asal_sumatera_barat_telantar_di_malaysia_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-05 20:44:59'),
(86, 294, NULL, 'Jinem 1', 'jinem_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-06 07:25:17'),
(87, 295, NULL, 'Eksekutif 1', 'eksekutif_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-07 02:40:23'),
(88, 296, NULL, 'VIP 1', 'vip_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-07 02:45:40'),
(89, 297, NULL, 'VVIP 1', 'vvip_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-07 02:48:38'),
(90, 298, NULL, 'UMROH SESUAI SUNNAH DAN AMANAH 1', 'umroh_sesuai_sunnah_dan_amanah_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-07 04:47:46'),
(91, 299, NULL, 'Hadiah Umrah bagi Marbut Terbaik di Garut 1', 'hadiah_umrah_bagi_marbut_terbaik_di_garut_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-07 04:49:12'),
(92, 300, NULL, 'Pulang Umrah, Penumpang Citilink Meninggal Dunia di Pesawat 1', 'pulang_umrah_penumpang_citilink_meninggal_dunia_di_pesawat_1', NULL, NULL, NULL, NULL, 'admin', '2018-04-07 05:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `site_job`
--

CREATE TABLE `site_job` (
  `job_id` int(10) UNSIGNED NOT NULL,
  `job_title` varchar(200) NOT NULL,
  `job_permalink` varchar(200) NOT NULL,
  `job_content` text,
  `job_meta_tags` varchar(200) DEFAULT NULL,
  `job_meta_description` varchar(200) DEFAULT NULL,
  `job_photo` varchar(100) DEFAULT NULL,
  `job_input_by` varchar(100) NOT NULL,
  `job_number_of_read` int(10) UNSIGNED DEFAULT '0',
  `job_show_time` enum('Y','N') NOT NULL DEFAULT 'N',
  `job_start_date` date DEFAULT NULL COMMENT 'waktu tayang',
  `job_end_date` date DEFAULT NULL COMMENT 'waktu tayang',
  `job_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_job_applicant`
--

CREATE TABLE `site_job_applicant` (
  `job_applicant_id` int(10) UNSIGNED NOT NULL,
  `job_applicant_person_id` int(10) UNSIGNED NOT NULL,
  `job_applicant_job_id` int(10) UNSIGNED NOT NULL,
  `job_applicant_job_title` varchar(100) NOT NULL,
  `job_applicant_application_letter` text,
  `job_applicant_experience_year` char(4) DEFAULT NULL,
  `job_applicant_last_position` varchar(255) DEFAULT NULL,
  `job_applicant_last_job_functions` varchar(255) DEFAULT NULL,
  `job_applicant_last_company` varchar(100) DEFAULT NULL,
  `job_applicant_month_start` tinyint(2) UNSIGNED ZEROFILL DEFAULT NULL,
  `job_applicant_year_start` year(4) DEFAULT NULL,
  `job_applicant_month_end` tinyint(2) UNSIGNED ZEROFILL DEFAULT NULL,
  `job_applicant_year_end` year(4) DEFAULT NULL,
  `job_applicant_has_read` enum('N','Y') DEFAULT NULL,
  `job_applicant_read_by` varchar(100) DEFAULT NULL,
  `job_applicant_read_datetime` datetime DEFAULT NULL,
  `job_applicant_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_job_applicant_reply`
--

CREATE TABLE `site_job_applicant_reply` (
  `job_applicant_reply_id` int(10) UNSIGNED NOT NULL,
  `job_applicant_reply_job_applicant_id` int(10) UNSIGNED NOT NULL,
  `job_applicant_reply_message` varchar(300) NOT NULL,
  `job_applicant_reply_by` varchar(100) NOT NULL,
  `job_applicant_reply_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_job_counter`
--

CREATE TABLE `site_job_counter` (
  `job_counter_id` int(11) NOT NULL,
  `job_counter_job_id` int(10) UNSIGNED NOT NULL,
  `job_counter_ip_address` int(10) UNSIGNED NOT NULL,
  `job_counter_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_news`
--

CREATE TABLE `site_news` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `news_title` varchar(200) NOT NULL,
  `news_permalink` varchar(200) NOT NULL,
  `news_content` text,
  `news_meta_tags` varchar(200) DEFAULT NULL,
  `news_meta_description` varchar(200) DEFAULT NULL,
  `news_photo` varchar(255) DEFAULT NULL,
  `news_input_by` varchar(100) NOT NULL,
  `news_number_of_read` int(10) UNSIGNED DEFAULT '0',
  `news_label_1` varchar(255) DEFAULT NULL,
  `news_label_2` varchar(100) DEFAULT NULL,
  `news_label_3` varchar(100) DEFAULT NULL,
  `news_external_link` varchar(255) DEFAULT NULL,
  `news_external_link_label` varchar(100) DEFAULT NULL,
  `news_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `news_is_deleted` enum('Y','N') NOT NULL DEFAULT 'Y',
  `news_input_datetime` datetime DEFAULT NULL,
  `news_update_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_news`
--

INSERT INTO `site_news` (`news_id`, `news_title`, `news_permalink`, `news_content`, `news_meta_tags`, `news_meta_description`, `news_photo`, `news_input_by`, `news_number_of_read`, `news_label_1`, `news_label_2`, `news_label_3`, `news_external_link`, `news_external_link_label`, `news_timestamp`, `news_is_deleted`, `news_input_datetime`, `news_update_datetime`) VALUES
(241, 'CPayment', 'home_cpayment', '&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-weight-400 font-size-20 text-center&quot;&gt;&lt;strong&gt;CPayment&lt;/strong&gt; adalah merk dagang dari &lt;strong&gt;PT. Citra Sagara Inten&lt;/strong&gt;&lt;br /&gt; Sebuah perusahaan yang bergerak dalam bidang Technology Mikro Payment dan Jasa Marketing Biro Perjalanan Umroh, CSI memberikan fasilitas kemudahan dan keuntungan kepada seluruh jama\'ah mengubah pengeluaran menjadi keuntungan dengan fasilitas layanan :&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Pembayaran Tagihan Rekening Listrik PLN&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Penjualan Token Prabayar PLN (Pulsa Listrik)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Pembayaran rekening Telephone dan Speedy (Telkom)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Penjualan Voucher Pulsa Elektronik (Semua Operator)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Pembayaran Rekening PDAM&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Leasing&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Transfer dan Ticketing (Tiket Kereta Api / Pesawat)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Iuran BPJS&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;dll&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-09 11:45:00', 'Y', '2018-01-09 18:54:56', '2018-01-09 18:59:45'),
(242, '3 Keraguan Jamaah Terhadap Travel Haji & Umrah', 'home_keraguan_jamaah_haji', '&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-purple &quot;&gt;3 Keraguan Jamaah Terhadap Travel Haji &amp;amp; Umrah&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-black font-size-20&quot;&gt;Legalitas Jelas atau Tidak ?&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-black font-size-20&quot;&gt;Fasilitas Terjamin atau Tidak ?&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-black  font-size-20&quot;&gt;Harga Terjangkau atau tidak ?&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', 'assets/images/master_content/1515500555.jpg', 'admin', 0, '-', '-', '-', '', '', '2018-01-09 12:15:00', 'Y', '2018-01-09 19:22:35', '2018-01-09 19:22:35'),
(243, 'Jaminan Rezeki Bagi Orang yang Menunaikan Ibadah Haji & Umroh', 'home_jaminan_rezeki', '&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0 text-center&quot;&gt;Orang yang sudah menjalankan Haji &amp;amp; Umroh dihilangkan Kefakirannya (dicukupi hidupnya) biaya Haji &amp;amp; Umroh diganti berlipat Oleh Allah SWT&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0 text-center&quot;&gt;Orang yang Haji tidak akan melarat sama sekali biaya Haji &amp;amp; Umroh seperti biaya dalam Sabilillah dilipat gandakan tujuh ratus kali lipat (HR.Ahmad wa Thobrini)&lt;/p&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0 text-center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-09 12:30:00', 'Y', '2018-01-09 19:45:26', '2018-01-09 19:45:26'),
(244, 'Berdoa Ditempat & Pada Waktu yang Mustajab', 'home_berdoa_ditempat', '&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-white&quot;&gt;Berdoa Ditempat &amp;amp; Pada Waktu yang Mustajab&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-white font-weight-300 font-size-20&quot;&gt;Raudah atau taman Surga&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-white font-weight-300 font-size-20&quot;&gt;Almultazam&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-white font-weight-300 font-size-20&quot;&gt;Dibelakang maqam Ibrahim&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-white font-weight-300 font-size-20&quot;&gt;Hijir Ismail atau Talang Emas&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-white font-weight-300 font-size-20&quot;&gt;Rukun Yamani &amp;amp; Hajar Aswad&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom color-white font-weight-300 font-size-20&quot;&gt;Sa\'i dibukit Safa &amp;amp; Marwa&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', 'assets/images/master_content/1515502299.jpg', 'admin', 0, '-', '-', '-', '', '', '2018-01-09 12:45:00', 'Y', '2018-01-09 19:51:39', '2018-01-09 19:51:39'),
(245, 'Anda Ingin Pergi Umrah ?, Pastikan !!', 'home_ingin_pergi_umrah', '&lt;div class=&quot;col_half&quot;&gt;&lt;img src=&quot;&amp;lt;?php echo $themes_inc; ?&amp;gt;images/5-pasti-umroh.png&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;h3 class=&quot;m-t-3&quot;&gt;Anda Ingin Pergi Umrah ?, Pastikan !!&lt;/h3&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-weight-300 font-size-20&quot;&gt;&lt;strong&gt;Pastikan&lt;/strong&gt; Travelnya Berizi Umrah&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-weight-300 font-size-20&quot;&gt;&lt;strong&gt;Pastikan&lt;/strong&gt; Jadwal &amp;amp; Penerbangannya&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-weight-300 font-size-20 &quot;&gt;&lt;strong&gt;Pastikan&lt;/strong&gt; Harga &amp;amp; Paket Layanannya&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-weight-300 font-size-20&quot;&gt;&lt;strong&gt;Pastikan&lt;/strong&gt; Hotelnya&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-weight-300 font-size-20&quot;&gt;&lt;strong&gt;Pastikan&lt;/strong&gt; Visanya&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-09 12:45:00', 'Y', '2018-01-09 19:55:17', '2018-01-09 19:55:17'),
(246, 'Sudah ada Niat kah Anda untuk menjadi Tamu Allah', 'home_niat_menjadi_tamu_allah', '&lt;div class=&quot;col_full center m-b-0&quot;&gt;\r\n&lt;h3 class=&quot;m-t-1 text-center&quot;&gt;Sudah ada Niat kah Anda untuk menjadi Tamu Allah menuju Baitullah dengan beribadah Umrah ?&lt;br /&gt; SEGERA..!!!&lt;br /&gt; Daftarkan Diri Anda &amp;amp; Keluarga Sekarang&lt;/h3&gt;\r\n&lt;a class=&quot;button button-circle button-purple&quot; href=&quot;#&quot;&gt;Daftar&lt;/a&gt;&lt;hr class=&quot;hr-dashed-1&quot; /&gt;\r\n&lt;h3 class=&quot;text-center color-orange bottommargin&quot;&gt;Allah SWT Akan Memampukan Hamba-Nya yang Terpanggil&lt;br /&gt; Bukan Memanggil Hambanya yang Mampu&lt;/h3&gt;\r\n&lt;img class=&quot;img-simulasi-2&quot; src=&quot;images/simulasi/simulasi-17.png&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-09 12:45:00', 'Y', '2018-01-09 19:56:03', '2018-01-09 19:56:03'),
(247, 'Profile', 'profil_perusahaan', '&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;box-img-about&quot;&gt;&lt;img src=&quot;http://sinarkabah.com/demo/images/profile.jpg&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Sinar Kabah Semesta mempunyai makna bahwa energi Baitullah itu membawa kebaikan dan keberkahan yang tidak bisa di lukiskan besarnya kepada siapapun, yang lurus niatnya semata- mata hanya karena Allah mau dan berbuat dalam upaya menghadirkan diri menjadi tamu-Nya ke dua kota suci yaitu Mekah dan Madinah..&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Hal yang mulia juga tentunya ketika kami menjadi bagian yang terlibat dalam mensukseskan keberangkatan serta kepulangan para Tamu Allah.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Atas dasar itulah PT.Sinar Kabah Semesta hadir sebagai salah satu pilihan yang insyaAllah mempunyai komitmen tinggi terhadap pelayanan dan akurasi waktu yang di sesuaikan dengan kebutuhan jamaah.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Sinar Kabah Semesta yang di pimpin oleh &lt;strong&gt;Bpk. Sukiyanto pangestu&lt;/strong&gt; selaku &lt;em&gt;&lt;strong&gt;Direktur Utama&lt;/strong&gt;&lt;/em&gt; serta di dukung dengan seluruh Team direksi dan support yang sangat berpengalaman selama lebih sepuluh tahun di industri ini, maka sudah semestinya PT. Sinar Kabah Semesta menjadi sangat layak untuk memberikan jaminan pelayanan yang terbaik.&lt;/p&gt;\r\n&lt;/div&gt;', '0', '0', '-', 'admin', 0, '-', '-', '-', '', '', '2018-01-12 01:45:00', 'Y', '2018-01-12 08:55:47', '2018-04-05 10:41:15'),
(248, 'Visi Misi', 'visi_dan_misi', '&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;h3 class=&quot;m-b-1&quot;&gt;Visi :&lt;/h3&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Menjadi penyelenggara Haji Khusus dan Umrah Terbesar dan terpercaya dengan pelayanan berkualitas dan bimbingan ibadah sesuai Al Qur&amp;rsquo;an dan Sunnah yang shahih.&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Memberikan pelayanan perjalanan ibadah secara komperhensif dan integral dengan berorientasi kepada kepuasan jamaah.&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Memiliki Sumber Daya Insani yang bertaqwa, loyal kepada perusahaan, amanah, profesional serta ditopang oleh sistem IT yang handal dan mekanisme kerja kondusif, efektif dan efisien.&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Inovatif, progresif dan bekerja keras untuk memberikan hasil yang terbaik terhadap segenap Bidang yang terkait&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;h3 class=&quot;m-b-1&quot;&gt;Misi :&lt;/h3&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Membentuk sebuah komunitas besar pecinta taubat,pengamal kebaikan,serta pensyiar islam sejati&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Senantiasa berupaya untuk berpegang teguh kepada prinsip-prinsip ajaran Islam dalam semua aspek baik individu maupun operasional perusahaan.&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Senantiasa memberikan manfaat yang sebesar-besarnya dan berkesinambungan bagi segenap pemangku kepentingan di PT. Sinar Kabah Semesta&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Ber Islam dengan benar bersama,sukses dunia akhirat bersama (Semua Kita Sukses)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, 'visi dan misi', '-', '-', '', '', '2018-01-12 02:00:00', 'Y', '2018-01-12 09:06:07', '2018-04-06 01:51:28'),
(249, 'Legalitas Perusahaan', 'legalitas_perusahaan', '&lt;ol type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;&lt;strong&gt;Nama Perusahaan :&lt;/strong&gt; PT. Sinar Kabah Semesta&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;&lt;strong&gt;Owner / Pemilik / Direktur :&lt;/strong&gt; Sukiyanto Pangestu&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;&lt;strong&gt;SIUP :&lt;/strong&gt; 511.3/SIUP/052/SKR/2018&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;&lt;strong&gt;TDP :&lt;/strong&gt; 060614601350&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;&lt;strong&gt;NO. IZIN PARIWISATA :&lt;/strong&gt; 094/DUP/DPMPTSP-PPK/2018&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;&lt;strong&gt;NPWP :&lt;/strong&gt; 84.064.943.8-307.000&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;&lt;strong&gt;IZIN KEMENAG :&lt;/strong&gt; 568/2017&lt;/li&gt;\r\n&lt;/ol&gt;', '0', '0', 'assets/images/master_content/1522954772.jpeg', 'admin', 0, 'legalitas perusahaan', '-', '-', '', '', '2018-01-12 02:30:00', 'Y', '2018-01-12 09:31:47', '2018-04-06 01:59:32'),
(250, 'Struktur Citra Sagara Inten', 'struktur_organisasi', '&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center m-b-1&quot;&gt;&lt;strong&gt;Komisaris 1&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Lucy Mulyawati&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center m-b-1&quot;&gt;&lt;strong&gt;Komisaris 2&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Yanti Laelasari&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last&quot;&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center m-b-1&quot;&gt;&lt;strong&gt;Komisaris 3&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Rudi Hidayat, SH&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1 &quot;&gt;&lt;strong&gt;Direktur Utama&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Giri ginanjar, ST&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_fourth&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Direktur Operasional&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Jaya Sasmita, S.Pd.I&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_fourth&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Direktur Keuangan&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Dra, Ai Habibah&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_fourth&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Direktur Marketing&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Ust. Ade Ahmad Sadili&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_fourth col_last&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Direktur SDM &lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Andriansyah. S.Pd&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Manager Keuangan&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Moch. Ridwan&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;IT&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Budi Rahmat, ST&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Humas&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;Ust. Ropi Abdul Basit&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Logistik&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;. . . .&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Team Kreatif&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;. . . .&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last&quot;&gt;\r\n&lt;h4 class=&quot;text-center font-weight-400 m-b-1&quot;&gt;&lt;strong&gt;Lawyer&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;hr class=&quot;hr-dashed-2 m-b-1 m-t-1&quot; /&gt;\r\n&lt;h4 class=&quot;font-weight-400 text-center&quot;&gt;. . . .&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;clear&quot;&gt;&amp;nbsp;&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 02:30:00', 'Y', '2018-01-12 09:34:20', '2018-01-12 09:34:20'),
(251, 'MARKETING PLAN', 'marketing_plan', '&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-300 width-850 width-mobile text-center&quot;&gt;PT. Citra Sagara Inten adalah perusahaan yang bergerak dalam bidang Technology Micro Payment dan Jasa Marketing Biro Perjalanan wisata dan Umroh dengan konsep memberikan Solusi kemudahan untuk kaum Muslimin dan Muslimat yang memiliki AZAM (niat yang kuat) untuk Ibadah Haji &amp;amp; Umroh, dengan dukungan technology yang canggih berbasis e-commerce (Pemasaran melalui Internet) Citra Sagara Inten membangun kemitraan yang berlandaskan azas bagi hasil yang saling menguntungkan (mudhorobah) dengan VISI MISI untuk meningkatkan Tarap hidup Kaum Muslimin dan Muslimat dengan Paket kemitraan yang disesuaikan pada kemampuan financial :&lt;/p&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 03:00:00', 'Y', '2018-01-12 10:12:21', '2018-01-12 10:12:21'),
(252, 'KEUNTUNGAN', 'keuntungan', '&lt;p&gt;-&lt;/p&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 03:45:00', 'Y', '2018-01-12 10:57:26', '2018-01-12 11:02:49'),
(253, 'UPGRADE PAKET PREMIUM', 'upgrade_paket_premium', '&lt;div class=&quot;col_full m-t-3 bottommargin&quot;&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-300 width-850 width-mobile text-center&quot;&gt;Ketika sudah mendapatkan Ujroh (Bonus) yang cukup di Paket Reguler anda dapat Upgrade dipaket Premium dengan Keuntungan :&lt;/p&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 04:00:00', 'Y', '2018-01-12 11:11:49', '2018-01-12 11:14:58'),
(254, 'Mekanisme Pembayaran Ujroh', 'mekanisme_pembayaran_ujroh', '&lt;div class=&quot;col_full m-b-0&quot;&gt;\r\n&lt;h4 class=&quot;h-line color-purple&quot;&gt;Mekanisme Pembayaran Ujroh&lt;/h4&gt;\r\n&lt;div class=&quot;d-line&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-500 m-b-1&quot;&gt;Ujroh Dibayar&lt;/p&gt;\r\n&lt;ul class=&quot;ul-custom font-size-15 m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;&lt;strong&gt;PAKET REGULER (Harian)&lt;/strong&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;strong&gt;PAKET PREMIUM (Mingguan)&lt;/strong&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;strong&gt;ROYALTI SHARING PROFIT (Bulanan)&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p class=&quot;font-size-15 m-b-0&quot;&gt;Dengan limit minimal transfer cash (uang) :&lt;/p&gt;\r\n&lt;ul class=&quot;ul-custom font-size-15 m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Cash (uang) : &lt;strong&gt;Rp. 100.000,-&lt;/strong&gt;&lt;/li&gt;\r\n&lt;li&gt;Deposit PPOB : &lt;strong&gt;Rp. 20.000,-&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Ujroh/Komisi &lt;em&gt;(Cash)&lt;/em&gt;,&lt;br /&gt; Hak bagi hasil 15 %, &lt;br /&gt; dan Auto Reward 20 % (Pemotongan Auto Reward Khusus Paket Premium hanya sampai Level 7)&lt;/p&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Untuk klaim bonus prestasi 7 s/d 14 hari kerja &lt;br /&gt; Klaim bonus prestasi yang di uangkan dikenakan pinalti sebesar 15% kecuali bonus prestasi Umroh &amp;amp; Haji tidak bisa di uangkan.&lt;/p&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 04:15:00', 'Y', '2018-01-12 11:16:08', '2018-01-12 11:16:08'),
(255, 'PT. CITRA SAGARA INTEN', 'konsep_program_umrah', '&lt;hr class=&quot;hr-dashed-1&quot; /&gt;\r\n&lt;div class=&quot;col_full tp-mbl&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;PT. CITRA SAGARA INTEN &lt;br /&gt; Dengan Konsep Program Umrah &amp;amp; Mengumrohkan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full text-center&quot;&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-300&quot;&gt;Sudahkah Anda MengUmrohkan Orang Yang Melahirkan Anda &amp;hellip;?&lt;br /&gt; Atau anda Ingin MengUmrohkan Guru Ngaji anda&amp;hellip;&amp;hellip;?&lt;br /&gt; Marbot, dan Berbagi dengan sesama Muslim&lt;/p&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-700 color-orange&quot;&gt;WUJUDKAN IMPIAN ANDA BERSAMA KAMI&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;hr class=&quot;hr-dashed-1&quot; /&gt;\r\n&lt;h3 class=&quot;text-center color-purple&quot;&gt;INGAT&lt;/h3&gt;\r\n&lt;div class=&quot;fancy-title title-double-border title-center&quot;&gt;\r\n&lt;h4&gt;Bukan Hanya sekedar Join &lt;strong&gt;BISNIS&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;fancy-title title-double-border title-center&quot;&gt;\r\n&lt;h4&gt;Tetapi ada sisi Ibadahnya Tentu Kita jangan Pernah main main dengan &lt;strong&gt;IBADAH&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;fancy-title title-double-border title-center&quot;&gt;\r\n&lt;h4&gt;Selain Beli &lt;strong&gt;LISENCI KEAGENAN&lt;/strong&gt; anda sudah mendaptkan &lt;strong&gt;Bilyet/Voucher UMROH&lt;/strong&gt;&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;fancy-title title-double-border title-center&quot;&gt;\r\n&lt;h4&gt;Bukan sekedar rekrut, tetapi ada kekuatan &lt;strong&gt;SYIAR&lt;/strong&gt; &amp;amp; saling mengingatkan untuk ibadah Umrah &amp;amp; Haji (QS Al Hajj 27)&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;fancy-title title-double-border title-center&quot;&gt;\r\n&lt;h4&gt;Bukan bonus tapi &lt;strong&gt;UJROH&lt;/strong&gt; (upah penjualan)&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;fancy-title title-double-border title-center&quot;&gt;\r\n&lt;h4&gt;Bukan bonus tapi &lt;strong&gt;UJROH&lt;/strong&gt; (upah penjualan)&lt;/h4&gt;\r\n&lt;/div&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Mari kita saling mengingatkan beribadah &amp;amp; membantu mereka yang memiliki niat yang kuat ke Baitullah&lt;/h3&gt;\r\n&lt;div class=&quot;col_one_third text-center-responsive&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;&lt;strong&gt;Mudah&lt;/strong&gt;, Jika Anda mau&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third text-center-responsive&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;&lt;strong&gt;Mudah&lt;/strong&gt;, Jika Anda memiliki Azzam (niat yang kuat)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last text-center-responsive&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;&lt;strong&gt;Mudah&lt;/strong&gt;, Jika Anda yakin dengan pertolongan Allah&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', 'assets/images/master_content/1515742751.png', 'admin', 0, 'Dengan Konsep Program Umrah &amp; Mengumrohkan', '-', '-', '', '', '2018-01-12 07:30:00', 'Y', '2018-01-12 14:34:29', '2018-01-12 14:39:11'),
(256, 'CATATAN', 'plan_catatan', '&lt;p class=&quot;font-size-20 font-weight-300 width-850 width-mobile text-center&quot;&gt;PT.CITRA SAGARA INTEN tidak menjanjikan siapapun untuk dapat penghasilan Tanpa pengembangan usaha, karena Citra Sagara Inten Bukan BISNIS INVESTASI tanpa kerja atau untung-untungan, Sistem pemasaran Citra Sagara Inten Membutuhkan pengembangan usaha dan kerjasamayang solid saling bantu membantu&lt;/p&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 07:30:00', 'Y', '2018-01-12 14:40:30', '2018-01-12 14:40:37'),
(257, 'PAKET TUNAI', 'paket_tunai', '&lt;hr class=&quot;hr-dashed-1&quot; /&gt;\r\n&lt;div class=&quot;col_full tp-mbl&quot;&gt;\r\n&lt;h2 class=&quot;text-center &quot;&gt;Pastikan perjalanan &lt;strong&gt;Ibadah Haji&lt;/strong&gt; &amp;amp; &lt;strong&gt;Umrah&lt;/strong&gt; Anda&lt;br /&gt; Bersama Program Baraya CSI&lt;/h2&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;div class=&quot;div-layanan&quot;&gt;&lt;img class=&quot;circle&quot; src=&quot;images/icon/icon-1.jpg&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-300 m-auto&quot;&gt;Jama&amp;rsquo;ah akan selalu dibimbing Oleh Para Ustad Yang berkomitmen Sunah Rasulullah SAW&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;div class=&quot;div-layanan&quot;&gt;&lt;img class=&quot;circle&quot; src=&quot;images/icon/icon-4.jpg&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-300 m-auto&quot;&gt;Menggunakan Akomondasi Hotel Berbintang Yang dekat dengan Masjidil Haram &amp;amp; Masjid Nabawi&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;div class=&quot;div-layanan&quot;&gt;&lt;img class=&quot;circle&quot; src=&quot;images/icon/icon-3.jpg&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-300 m-auto&quot;&gt;Menggunakan Penerbangan terjadwal dengan Pelayanan Standar Internasional&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;div class=&quot;div-layanan&quot;&gt;&lt;img class=&quot;circle&quot; src=&quot;images/icon/icon-2.jpg&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-300 m-auto&quot;&gt;Provider Visa Umroh untuk Menjamin Kepastian Keberangkatan sesuai Jadwal&lt;/p&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 07:30:00', 'Y', '2018-01-12 14:44:36', '2018-01-12 14:48:41'),
(258, 'Harga Paket Tunai', 'harga_paket_tunai', '&lt;div class=&quot;col_full text-center bottommargin width-850 width-mobile bttm-mbl topmargin&quot;&gt;\r\n&lt;p class=&quot;font-size-20 font-weight-500 m-b-0&quot;&gt;Jamaah dapat memilih sesuai dengan kemampuan Financialnya agar setiap lapisan masyarakat muslim dapat dan dimudahkan untuk berangkat ibadah Umroh &amp;nbsp;ke tanah suci&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;p&gt;PAKET PROMOJADWAL KEBERANGKATAN&lt;/p&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n&lt;thead&gt;&lt;/thead&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;HARGA&lt;/td&gt;\r\n&lt;td&gt;IDR. 20.500.000,-&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;PERJALANAN&lt;/td&gt;\r\n&lt;td&gt;9 Hari&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;FASILITAS HOTEL&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;text-center&quot; colspan=&quot;3&quot;&gt;&lt;strong&gt;Catatan :&lt;/strong&gt; Sewaktu-waktu Harga Berubah&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;p&gt;PAKET TUNAIJADWAL KEBERANGKATAN&lt;/p&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n&lt;thead&gt;&lt;/thead&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;HARGA MULAI&lt;/td&gt;\r\n&lt;td&gt;IDR. 23.000.000,-&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;PERJALANAN&lt;/td&gt;\r\n&lt;td&gt;9 Hari&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;FASILITAS HOTEL&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;text-center&quot; colspan=&quot;3&quot;&gt;&lt;strong&gt;Catatan :&lt;/strong&gt; Sewaktu-waktu Harga Berubah&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;p&gt;PAKET PLUS TURKEYJADWAL KEBERANGKATAN&lt;/p&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n&lt;thead&gt;&lt;/thead&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;HARGA&lt;/td&gt;\r\n&lt;td&gt;IDR. 25.000.000&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;PERJALANAN&lt;/td&gt;\r\n&lt;td&gt;10 Hari&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;FASILITAS HOTEL&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;text-center&quot; colspan=&quot;3&quot;&gt;&lt;strong&gt;Catatan :&lt;/strong&gt; Sewaktu-waktu Harga Berubah&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;p&gt;PAKET CICILAN (Tabungan)JADWAL KEBERANGKATAN&lt;/p&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n&lt;thead&gt;&lt;/thead&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;JANGKA WAKTU&lt;/td&gt;\r\n&lt;td&gt;36 BULAN&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;TABUNGAN&lt;/td&gt;\r\n&lt;td&gt;Rp. 595.000,-&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;BOOKING FEE&lt;/td&gt;\r\n&lt;td&gt;Rp. 1.000.000,-&lt;/td&gt;\r\n&lt;td&gt;&amp;nbsp;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;text-center&quot; colspan=&quot;3&quot;&gt;&lt;strong&gt;Catatan :&lt;/strong&gt; Sewaktu-waktu Harga Berubah&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 07:30:00', 'Y', '2018-01-12 14:46:27', '2018-01-12 14:46:27'),
(259, 'Embarkasi', 'embarkasi', '&lt;div class=&quot;col_full m-b-0 tp-mbl topmargin&quot;&gt;\r\n&lt;h4 class=&quot;h-line color-purple&quot;&gt;Embarkasi&lt;/h4&gt;\r\n&lt;div class=&quot;d-line&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full nobottommargin&quot;&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0&quot;&gt;Embarkasi Jakarta&lt;/p&gt;\r\n&lt;img class=&quot;img-fly&quot; src=&quot;images/fly-green.png&quot; alt=&quot;&quot; /&gt;&lt;hr class=&quot;hr-dashed-1&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_full nobottommargin&quot;&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0&quot;&gt;Embarkasi Surabaya&lt;/p&gt;\r\n&lt;img class=&quot;img-fly&quot; src=&quot;images/fly-red.png&quot; alt=&quot;&quot; /&gt;&lt;hr class=&quot;hr-dashed-1&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_full nobottommargin&quot;&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0&quot;&gt;Embarkasi Singapura&lt;/p&gt;\r\n&lt;img class=&quot;img-fly&quot; src=&quot;images/fly-red.png&quot; alt=&quot;&quot; /&gt;&lt;hr class=&quot;hr-dashed-1&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_full nobottommargin&quot;&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0&quot;&gt;Embarkasi Makassar&lt;/p&gt;\r\n&lt;img class=&quot;img-fly&quot; src=&quot;images/fly-red.png&quot; alt=&quot;&quot; /&gt;&lt;hr class=&quot;hr-dashed-1&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_full nobottommargin &quot;&gt;\r\n&lt;p class=&quot;font-size-20 m-b-0&quot;&gt;Embarkasi Medan&lt;/p&gt;\r\n&lt;img class=&quot;img-fly&quot; src=&quot;images/fly-red.png&quot; alt=&quot;&quot; /&gt;&lt;hr class=&quot;hr-dashed-1&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_full nobottommargin&quot;&gt;\r\n&lt;div class=&quot;div-fly&quot;&gt;&lt;img class=&quot;img-fly-2&quot; src=&quot;images/fly-green.png&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;: Memiliki ruang tunggu/lounge VIP sendiri&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full bottommargin&quot;&gt;\r\n&lt;div class=&quot;div-fly&quot;&gt;&lt;img class=&quot;img-fly-2&quot; src=&quot;images/fly-red.png&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;p class=&quot;float-left bottommargin-lg font-size-15&quot;&gt;: Coming Soon Embarkasi&lt;/p&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 07:45:00', 'Y', '2018-01-12 14:51:07', '2018-01-12 14:51:07'),
(260, 'Persyaratan Umroh', 'persyaratan_umroh', '&lt;div class=&quot;col_full m-b-0 tp-mbl topmargin&quot;&gt;\r\n&lt;h4 class=&quot;h-line color-purple&quot;&gt;Persyaratan Umroh&lt;/h4&gt;\r\n&lt;div class=&quot;d-line&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;ul class=&quot;ul-custom font-size-17 m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Daftar dan mengisi formulir&lt;/li&gt;\r\n&lt;li&gt;Passport Asli yang Masih berlaku minimal 8 bulan sebelum keberangkatan&lt;/li&gt;\r\n&lt;li&gt;Nama di Passport Minimal 3 suku kata&lt;/li&gt;\r\n&lt;li&gt;Photo copy KTP yang masih berlaku&lt;/li&gt;\r\n&lt;li&gt;Photo Copy KK&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;ul class=&quot;ul-custom font-size-17 m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Surat Mahrom&lt;/li&gt;\r\n&lt;li&gt;Photo Copy Akta Lahir&lt;/li&gt;\r\n&lt;li&gt;Photo Copy Surat Nikah&lt;/li&gt;\r\n&lt;li&gt;Pas Photo Ukuran 4 X 6 (4 lembar)&lt;/li&gt;\r\n&lt;li&gt;Buku Kuning&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-0 tp-mbl topmargin&quot;&gt;\r\n&lt;h4 class=&quot;h-line color-purple&quot;&gt;Harga Termasuk&lt;/h4&gt;\r\n&lt;div class=&quot;d-line&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;ul class=&quot;ul-custom font-size-17 m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Tiket Umroh PP&lt;/li&gt;\r\n&lt;li&gt;Akomondasi Hotel berbintang&lt;/li&gt;\r\n&lt;li&gt;Transportasi Bus ber AC&lt;/li&gt;\r\n&lt;li&gt;Ziarah Dan City Tour&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;ul class=&quot;ul-custom font-size-17 m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Makan 3 X Menu Indonesia&lt;/li&gt;\r\n&lt;li&gt;Muthawwief&lt;/li&gt;\r\n&lt;li&gt;Air Zam-Zam 5 Liter&lt;/li&gt;\r\n&lt;li&gt;Visa Umroh&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-0 tp-mbl topmargin&quot;&gt;\r\n&lt;h4 class=&quot;h-line color-purple&quot;&gt;Harga Tidak Termasuk&lt;/h4&gt;\r\n&lt;div class=&quot;d-line&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;ul class=&quot;ul-custom font-size-17 m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Airport Handling&lt;/li&gt;\r\n&lt;li&gt;Pembuatan Pasport&lt;/li&gt;\r\n&lt;li&gt;Biaya Mahrom&lt;/li&gt;\r\n&lt;li&gt;Kelebihan Bagasi&lt;/li&gt;\r\n&lt;li&gt;Keperluan Pribadi&lt;/li&gt;\r\n&lt;li&gt;Suntik Vaksin Meningitis&lt;/li&gt;\r\n&lt;li&gt;Penambahan Hari&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 07:45:00', 'Y', '2018-01-12 14:51:54', '2018-01-12 14:51:54'),
(261, 'Penduduk Indonesia Kurang Lebih 250 Juta Jiwa Lebih dari 200 jiwa beraga islam', 'penduduk_indonesia', '&lt;div class=&quot;container-fullwidth clearfix&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-left&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas Pesawat yang Digunakan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;h4-custom text-center font-weight-300&quot;&gt;Garuda Indonesia&lt;/h4&gt;\r\n&lt;img class=&quot;img-fasility&quot; src=&quot;images/garuda.png&quot; alt=&quot;Garuda Indonesia&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;h4-custom text-center font-weight-300&quot;&gt;Saudi Arabian Airlines&lt;/h4&gt;\r\n&lt;img class=&quot;img-fasility&quot; src=&quot;images/saudi-arabia.png&quot; alt=&quot;Saudi Arabia&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last&quot;&gt;\r\n&lt;h4 class=&quot;h4-custom text-center font-weight-300&quot;&gt;Etihad Airways&lt;/h4&gt;\r\n&lt;img class=&quot;img-fasility&quot; src=&quot;images/etihad.png&quot; alt=&quot;Etihad Airlines&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;container-fullwidth clearfix&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-left&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas Hotel Berbintang&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;h4-custom text-center font-weight-300&quot;&gt;Ibis Styles Hotel&lt;/h4&gt;\r\n&lt;img class=&quot;img-fasility&quot; src=&quot;images/hotel-3.png&quot; alt=&quot;Ibis Styles Hotel&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third&quot;&gt;\r\n&lt;h4 class=&quot;h4-custom text-center font-weight-300&quot;&gt;Alaf Bakkah Hotel&lt;/h4&gt;\r\n&lt;img class=&quot;img-fasility&quot; src=&quot;images/hotel-4.png&quot; alt=&quot;Alaf Bakkah Hotel&quot; /&gt;&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last&quot;&gt;\r\n&lt;h4 class=&quot;h4-custom text-center font-weight-300&quot;&gt;Hilton Makkah Hotel&lt;/h4&gt;\r\n&lt;img class=&quot;img-fasility&quot; src=&quot;images/hotel-5.png&quot; alt=&quot;Hilton Makkah Hotel&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;', '0', '0', 'assets/images/master_content/1515743567.jpg', 'admin', 0, '-', '-', '-', '', '', '2018-01-12 07:45:00', 'Y', '2018-01-12 14:52:46', '2018-01-12 14:53:29'),
(262, 'CPayment', 'cpayment', '&lt;div class=&quot;col_full tp-mbl&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-weight-400 font-size-20 text-center&quot;&gt;&lt;strong&gt;CPayment&lt;/strong&gt; adalah merk dagang dari &lt;strong&gt;PT. Citra Sagara Inten&lt;/strong&gt;&lt;br /&gt; Sebuah perusahaan yang bergerak dalam bidang Technology Mikro Payment dan Jasa Marketing Biro Perjalanan Umroh, CSI memberikan fasilitas kemudahan dan keuntungan kepada seluruh jama\'ah mengubah pengeluaran menjadi keuntungan dengan fasilitas layanan :&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Pembayaran Tagihan Rekening Listrik PLN&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Penjualan Token Prabayar PLN (Pulsa Listrik)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Pembayaran rekening Telephone dan Speedy (Telkom)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Penjualan Voucher Pulsa Elektronik (Semua Operator)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;\r\n&lt;ul class=&quot;ul-custom&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Pembayaran Rekening PDAM&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Leasing&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Transfer dan Ticketing (Tiket Kereta Api / Pesawat)&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;Iuran BPJS&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;li class=&quot;m-b-1&quot;&gt;\r\n&lt;h3 class=&quot;h3-custom font-size-20 font-weight-400&quot;&gt;dll&lt;/h3&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;', '0', '0', 'assets/images/master_content/1515756254.png', 'admin', 0, '-', '-', '-', '', '', '2018-01-12 11:15:00', 'Y', '2018-01-12 18:20:35', '2018-01-12 18:24:14'),
(263, 'Kemudahan Transaksi CPayment', 'kemudahan_transaksi_cpayment', '&lt;h2 class=&quot;text-center font-weight-300 width-850 color-purple&quot;&gt;Nikmati semua kemudahan transaksi &lt;strong&gt;CPayment&lt;/strong&gt;&lt;br /&gt; langsung melalui gadget Anda :&lt;/h2&gt;\r\n&lt;div class=&quot;line-mini bottommargin&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;div class=&quot;col_two_fifth&quot;&gt;\r\n&lt;h3 class=&quot;font-weight-400 text-center&quot;&gt;Download Aplikasi Cpayment Melalui Google Play Store&lt;/h3&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;&lt;a title=&quot;Akses aplikasi android&quot; href=&quot;https://play.google.com/store/apps/details?id=com.barayacsi.epay&amp;amp;hl=en&quot;&gt; &lt;img class=&quot;icon-situs&quot; src=&quot;images/icon/icon-playstore.png&quot; alt=&quot;&quot; /&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;h4 class=&quot;m-b-1&quot;&gt;Cara Transaksi :&lt;/h4&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;ul class=&quot;ul-custom font-size-17 m-b-1 font-weight-400&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Login menggunakan akun member Anda&lt;/li&gt;\r\n&lt;li&gt;Pilih menu pembayaran sesuai kebutuhan Anda&lt;/li&gt;\r\n&lt;li&gt;Ikuti petunjuk pada menu FAQ&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_fifth&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;div class=&quot;col_two_fifth col_last&quot;&gt;&lt;hr class=&quot;line-2 bottommargin&quot; /&gt;\r\n&lt;h3 class=&quot;font-weight-400 text-center&quot;&gt;Akses web &lt;strong&gt;BarayaCSI.net&lt;/strong&gt; melalui Browser HP/PC Anda&lt;/h3&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;&lt;a title=&quot;akses web www.barayacsi.net&quot; href=&quot;http://barayacsi.net&quot; target=&quot;_blank&quot;&gt; &lt;img class=&quot;icon-situs&quot; src=&quot;images/icon/icon-web.png&quot; alt=&quot;&quot; /&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;h4 class=&quot;m-b-1&quot;&gt;Cara Transaksi :&lt;/h4&gt;\r\n&lt;div class=&quot;col_full&quot;&gt;\r\n&lt;ul class=&quot;ul-custom font-size-17 m-b-1 font-weight-400&quot; type=&quot;none&quot;&gt;\r\n&lt;li&gt;Login menggunakan akun member Anda&lt;/li&gt;\r\n&lt;li&gt;Pilih menu pembayaran sesuai kebutuhan Anda&lt;/li&gt;\r\n&lt;li&gt;Ikuti petunjuk pada menu panduan&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;span class=&quot;font-weight-300&quot;&gt;&lt;em&gt;Untuk langkah transaksi lengkapnya, Anda dapat &lt;a title=&quot;Silakan Login&quot; href=&quot;https://barayacsi.net/&quot; target=&quot;_blank&quot;&gt;&lt;strong&gt;&lt;span style=&quot;text-decoration: underline;&quot;&gt;login&lt;/span&gt;&lt;/strong&gt;&lt;/a&gt; terlebih dahulu, kemudian menuju menu &lt;a title=&quot;Menuju menu panduan setelah login&quot; href=&quot;https://barayacsi.net/voffice/panduan/show&quot; target=&quot;_blank&quot;&gt;&lt;strong&gt;&lt;span style=&quot;text-decoration: underline;&quot;&gt;panduan&lt;/span&gt;&lt;/strong&gt;&lt;/a&gt; &lt;/em&gt;&lt;/span&gt;&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 11:15:00', 'Y', '2018-01-12 18:21:45', '2018-01-12 18:21:45');
INSERT INTO `site_news` (`news_id`, `news_title`, `news_permalink`, `news_content`, `news_meta_tags`, `news_meta_description`, `news_photo`, `news_input_by`, `news_number_of_read`, `news_label_1`, `news_label_2`, `news_label_3`, `news_external_link`, `news_external_link_label`, `news_timestamp`, `news_is_deleted`, `news_input_datetime`, `news_update_datetime`) VALUES
(264, 'Itenerary', 'itenerary', '&lt;div class=&quot;col_full m-b-0 tp-mbl&quot;&gt;\r\n&lt;h4 class=&quot;h-line color-purple&quot;&gt;Itenerary&lt;/h4&gt;\r\n&lt;div class=&quot;d-line&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_one_third m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;1&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;JAKARTA-JEDAH&amp;ndash;MADINAH&lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Berkumpul di Jakarta (Bandara Soekarno Hata) terbang Ke Jedah (transeat) sampai di hotel Istirahat&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;2&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;Ziarah Kemakam Rasulullah SAW&lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Raudah (Mesjid Nabawi)&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;3&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;Ziarah Ke Masjid&lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Ziarah ke masjid Quba, Mesjid Qiblatain, Lalu Ke Jabal Uhud Lanjut Ke Pasar qurma Setelah Shalat Ashar Berjama&amp;rsquo;ah Ikut Tausiah&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_one_third m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;4&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;MADINAH-MEKAH&lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Check Out Hotel, Shalat Jama di Mesjid Nabawi Menuju BIRR ALIY untuk ambil Miqat lalu Berangkat ke Mekah, sampai di Mekah Check in Hotel dan istirahat sejenak, lalu lanjut Melaksanakan TAWAF, SA&amp;rsquo;I dan TAHALUL&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;5&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;Memperbanyak Ibadah &lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Di Masjidil Haram&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;6&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;Ziarah Kota&lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Ziarah kota Mekah, Jabal Tsur, Jabal Nur, Mina Musdalifah dan mengambil Miqat Untuk Umroh Ke 2 di Dja&amp;rsquo;ronah/Hudaibiyah&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_one_third m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;7&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;Merperbanyak Ibadah &lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Di Masjidil Haram&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;8&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;Jalan-jalan&lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;Berangkat menuju Shopping Centre dan Mesjid Terapung Lanjut ke Jeddah&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_one_third col_last m-b-1&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-3&quot;&gt;\r\n&lt;div class=&quot;feature-box feature-box-custom fbox-plain&quot;&gt;\r\n&lt;div class=&quot;fbox-icon&quot;&gt;\r\n&lt;ul class=&quot;process-steps process-2 bottommargin bttm-1 clearfix&quot;&gt;\r\n&lt;li class=&quot;active&quot;&gt;&lt;a class=&quot;i-circled i-alt divcenter bgcolor&quot; href=&quot;#&quot;&gt;9&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;font-size-15 m-b-1 color-grey-dark&quot;&gt;&lt;strong&gt;JEDDAH-JAKARTA&lt;/strong&gt;&lt;/p&gt;\r\n&lt;hr class=&quot;hr-dashed-1 m-t-0 m-b-0&quot; /&gt;\r\n&lt;p class=&quot;font-size-15&quot;&gt;(Insya Allah Atas Izin dan Ridhonya tiba dengan selamat dan Mabrur)&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-12 11:15:00', 'Y', '2018-01-12 18:25:58', '2018-01-12 18:25:58'),
(265, 'Manivest Keberangkatan', 'manivest_keberangkatan', '&lt;p&gt;&lt;span style=&quot;color: #414141; font-size: 14px; font-family: Lato, sans-serif;&quot;&gt;Info belum tersedia&lt;/span&gt;&lt;/p&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-13 14:30:00', 'Y', '2018-01-13 21:39:01', '2018-01-13 21:51:25'),
(266, 'Data Jamaah', 'data_jamaah', '&lt;p&gt;&lt;span style=&quot;color: #414141; font-size: 14px; font-family: Lato, sans-serif;&quot;&gt;Info belum tersedia&lt;/span&gt;&lt;/p&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-13 14:45:00', 'Y', '2018-01-13 21:51:52', '2018-01-13 21:52:08'),
(268, 'Umrah Binary Jamaah', 'umrah_binary_jamaah', '&lt;p&gt;-&lt;/p&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-01-14 07:00:00', 'Y', '2018-01-14 14:06:23', '2018-01-14 14:26:30'),
(279, 'Mengapa Harus Sinar Kabah Semesta (SKS)?', 'global_plugin', '						<div class=\"col-md-4 col-sm-6 m-b-0\">\r\n							<div class=\"panel panel-success\">\r\n								<div class=\"panel-heading\">\r\n									<h3 class=\"panel-title\">Harga Kompetitif</h3>\r\n									<i class=\"fa fa-usd fa-absolute\"></i>\r\n								</div>\r\n								<div class=\"panel-body panel-body-custom\">\r\n									Harga Paket Umroh yang kami tawarkan  berpariasi dan harga yang sangat terjangkau dengan fasilitas terbaik,jujur dan Bersaing.\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class=\"col-md-4 col-sm-6 m-b-0\">\r\n							<div class=\"panel panel-success\">\r\n								<div class=\"panel-heading\">\r\n									<h3 class=\"panel-title\">Layanan Prima</h3>\r\n									<i class=\"fa fa-thumbs-up fa-absolute\"></i>\r\n								</div>\r\n								<div class=\"panel-body panel-body-custom\">\r\n									Kami lebih mengutamakan Pelayanan Prima dalam melayani jama\'ah dari tanah air ketanah suci sampai kembali lagi ke tanah air.\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class=\"col-md-4 col-sm-6 m-b-0\">\r\n							<div class=\"panel panel-success\">\r\n								<div class=\"panel-heading\">\r\n									<h3 class=\"panel-title\">Berpengalaman</h3>\r\n									<i class=\"fa fa-file-text-o fa-absolute\"></i>\r\n								</div>\r\n								<div class=\"panel-body panel-body-custom\">\r\n									Dengan tenaga Ahli profesional dan berpengalaman dalam bidang nya  insyaAllah kami siap melayani  anda dalam segala hal.\r\n								</div>\r\n							</div>\r\n						</div>', '0', '0', NULL, 'admin', 0, 'Mengapa Harus Sinar Kabah Semesta (SKS)?', '-', '-', '', '', '2018-04-02 03:00:00', 'Y', '2018-04-02 10:08:55', '2018-04-04 08:17:06'),
(280, 'menu footer', 'menu_footer', '&lt;div class=&quot;footer-widgets-wrap clearfix&quot;&gt;\r\n&lt;div class=&quot;col-md-3&quot;&gt;\r\n&lt;h4 class=&quot;m-b-1 border-bottom&quot;&gt;INFORMASI&lt;/h4&gt;\r\n&lt;div class=&quot;widget widget_links clearfix m-t-1&quot;&gt;\r\n&lt;ul class=&quot;m-b-m&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;tentang-kami.html&quot;&gt;Tentang Kami&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;metode-pembayaran.html&quot;&gt;Metode Pembayaran&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;contact.html&quot;&gt;Hubungi Kami&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;gallery.html&quot;&gt;Gallery&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;syarat-ketentuan.html&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;kebijakan-privasi.html&quot;&gt;Kebijakan Privasi&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;https://eservices.haj.gov.sa/eservices3/pages/VisaPaymentInquiry/VisaInquiry.xhtml?dswid=-7810&quot; target=&quot;_blank&quot;&gt;Cek Visa (biaya tambahan)&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-3&quot;&gt;\r\n&lt;h4 class=&quot;m-b-1 border-bottom&quot;&gt;PANDUAN&lt;/h4&gt;\r\n&lt;div class=&quot;widget widget_links clearfix m-t-1&quot;&gt;\r\n&lt;ul class=&quot;m-b-m&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-pembayaran-online.html&quot;&gt;Pembayaran Online&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-registrasi-keagenan.html&quot;&gt;Registrasi Keagenan&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-paket-umrah.html&quot;&gt;Panduan Transaksi Paket Umrah&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-paket-umrah.html&quot;&gt;Panduan Transaksi Paket Umrah Group&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-paket-umrah.html&quot;&gt;Panduan Transaksi Visa&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-paket-umrah.html&quot;&gt;Panduan Transaksi Paket LA&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-paket-umrah.html&quot;&gt;Panduan Transaksi Tiket Pesawat&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-3&quot;&gt;\r\n&lt;h4 class=&quot;m-b-1 border-bottom&quot;&gt;KONTAK&lt;/h4&gt;\r\n&lt;div class=&quot;widget widget_links clearfix m-t-1&quot;&gt;\r\n&lt;ul class=&quot;m-b-m&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-contact-us&quot;&gt;&lt;strong&gt;Alamat Kontor Pusat&lt;/strong&gt;&lt;br /&gt; Green Palm Residence&lt;br /&gt; Jl. RA. Abu samah palembang&lt;/li&gt;\r\n&lt;li class=&quot;li-contact-us&quot;&gt;&lt;strong&gt;Alamat Kontor Cabang&lt;/strong&gt;&lt;br /&gt; Jl. Kimaja komp. Ruko blok AA No 5 Way Halim bandar Lampung&lt;/li&gt;\r\n&lt;li class=&quot;li-contact-us&quot;&gt;+687756789234&lt;/li&gt;\r\n&lt;li class=&quot;li-contact-us&quot;&gt;0711-5710107&lt;/li&gt;\r\n&lt;li class=&quot;li-contact-us&quot;&gt;sks-indonesia@gmail.com&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-3 text-center m-t-logo-footer&quot;&gt;&lt;img class=&quot;center-mobile&quot; src=&quot;images/logo-footer-sks.png&quot; alt=&quot;&quot; /&gt;\r\n&lt;div class=&quot;row&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-3 text-center&quot;&gt;&lt;img class=&quot;center-mobile&quot; src=&quot;&amp;lt;?php echo $themes_inc; ?&amp;gt;images/logo-footer-sks.png&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, 'menu footer', '-', '-', '', '', '2018-04-02 07:15:00', 'Y', '2018-04-02 14:29:06', '2018-04-04 16:07:20'),
(281, 'Video', 'video', '&lt;p&gt;-&lt;/p&gt;', '0', '0', 'assets/images/master_content/1522823954.jpeg', 'admin', 0, '-', '-', '-', '', '', '2018-04-04 02:00:00', 'Y', '2018-04-04 09:16:12', '2018-04-04 13:39:40'),
(282, 'Acara pra manasik perdana bersama calon jamaah umroh tgl 8 april 2018', 'photo', '&lt;p&gt;-&lt;/p&gt;', 'Acara, pra, manasik, perdana, bersama, calon, jamaah, umroh, tgl, 8, april, 2018,', 'Acara pra manasik perdana bersama calon jamaah umroh tgl 8 april 2018', 'assets/images/master_content/1522827046.jpg', 'admin', 0, 'Acara pra manasik perdana bersama calon jamaah umroh tgl 8 april 2018', '-', '-', '', '', '2018-04-04 02:15:00', 'Y', '2018-04-04 09:18:37', '2018-04-04 14:37:15'),
(283, 'Brosur', 'brosur', '&lt;p&gt;-&lt;/p&gt;', '0', '0', 'assets/images/master_content/1522820242.jpg', 'admin', 0, '-', '-', '-', '', '', '2018-04-04 03:15:00', 'Y', '2018-04-04 10:17:55', '2018-04-04 12:37:22'),
(284, 'Pendukung', 'pendukung', '&lt;p&gt;-&lt;/p&gt;', 'pendukung,sks', 'pendukung', 'assets/images/master_content/1522829371.jpg', 'admin', 0, 'Pendukung', '-', '-', '', '', '2018-04-04 08:00:00', 'Y', '2018-04-04 15:09:31', '2018-04-04 15:09:31'),
(285, 'Maskapai', 'maskapai', '&lt;p&gt;-&lt;/p&gt;', 'maskapai,sks', 'maskapai', 'assets/images/master_content/1522832348.png', 'admin', 0, 'maskapai', '-', '-', '', '', '2018-04-04 08:45:00', 'Y', '2018-04-04 15:59:08', '2018-04-04 15:59:08'),
(286, 'Hemat', 'hemat', '&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Travel Bag 24&amp;rdquo; ( Koper )&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Syal SKS&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mini Bag&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mukena/Ihram &amp;amp; Sabuk&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Seragam pria/ wanita ( Batik )&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Id Card Gantung&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Do&amp;rsquo;a&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Alumni&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Stiker Tag Travel Bag&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Persyaratan Peserta&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KTP.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KARTU KELUARGA (KSK).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU NIKAH ASLI (apabila suami istri).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;AKTE KELAHIRAN (apabila usia jamaah dibawah 18 tahun atau belum memiliki KTP).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PASPOR DENGAN 3 NAMA DAN MASIH BERLAKU SAMPAI DENGAN 7 BULAN SETELAH TANGGAL KEPULANGAN.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU VAKSIN MENINGITIS YANG MASIH BERLAKU.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;SURAT MAHRAM (apabila wanita dengan usia di bawah 45 tahun berangkat tanpa muhrim dan jamaah usia dibawah 18 tahun).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;6 LEMBAR PAS FOTO UKURAN 3 X 4 DAN UKURAN 4 X 6 (latar foto berwarna putih, tegak lurus melihat ke depan, jilbab harus sempurna, tidak boleh kelihatan leher dan rambut bagi jamaah wanita, tidak memakai penutup kepala bagi jamaah pria, tidak boleh memakai kaca mata).&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;&lt;!-- &lt;div class=&quot;col-md-6&quot;&gt;\r\n								&lt;p class=&quot;m-b-1&quot;&gt;&lt;b&gt;Keterangan Program :&lt;/b&gt;&lt;/p&gt;\r\n								&lt;ol type=&quot;1&quot;&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;UMROH PROGRAM 9 HARI (5 MALAM DI MEKAH DAN 2 MALAM DI MADINAH) DENGAN TUJUAN AWAL DI MEKAH.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;JADWAL DAN RUTE PENERBANGAN DAPAT BERUBAH SEWAKTU-WAKTU TERGANTUNG KEBIJAKAN MASKAPAI.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;JADWAL DAN RUTE PENERBANGAN DAPAT BERUBAH SEWAKTU-WAKTU TERGANTUNG KEBIJAKAN MASKAPAI.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;PEMBAYARAN DAPAT DILAKUKAN 2 MACAM : PEMBAYARAN TUNAI &amp; PEMBAYARAN KREDIT&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;UANG MUKA UNTUK PEMESANAN PAKET Rp. 5.000.000,-&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;DAPATKAN VOUCHER UP TO Rp. 1.500.000,-&lt;/li&gt;\r\n								&lt;/ol&gt;\r\n							&lt;/div&gt; --&gt;\r\n&lt;div class=&quot;col-md-12&quot;&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&lt;strong&gt;Harga Belum Termasuk :&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN PASPOR.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;VAKSIN MANINGITIS.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TIPS UNTUK TOUR LEADER, MUNTHAWIF, PORTER DAN SOPIR SEBESAR RP.100.000,-.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGELUARAN PRIBADI (telephone, laundry dan sebagainya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN SURAT MAHRAM SEBESAR RP.300.000,- (bila ada).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TAMBAHAN BIAYA VISA SEBESAR 563 USD (biaya KBSA) DAN 2.100 SAR (apabila jamaah telah berangkat umroh pada musim uroh 1438 H dan setelahnya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;ASURANSI UMROH SEBESAR RP.150.000,-.&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '1439 H,Fajar Bade\'a 1,Rawase Safeer,--,--,--', 'paket murah umroh promo reguler', 'assets/images/master_content/1523065126.jpg', 'admin', 8, 'Rp. 21.000.000', '-', '-', '', '', '2018-04-04 09:15:00', 'Y', '2018-04-04 16:17:54', '2018-04-07 09:12:27'),
(287, 'Ekonomi', 'ekonomi', '&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Travel Bag 24&amp;rdquo; ( Koper )&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Syal SKS&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mini Bag&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mukena/Ihram &amp;amp; Sabuk&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Seragam pria/ wanita ( Batik )&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Id Card Gantung&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Do&amp;rsquo;a&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Alumni&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Stiker Tag Travel Bag&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Persyaratan Peserta&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KTP.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KARTU KELUARGA (KSK).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU NIKAH ASLI (apabila suami istri).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;AKTE KELAHIRAN (apabila usia jamaah dibawah 18 tahun atau belum memiliki KTP).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PASPOR DENGAN 3 NAMA DAN MASIH BERLAKU SAMPAI DENGAN 7 BULAN SETELAH TANGGAL KEPULANGAN.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU VAKSIN MENINGITIS YANG MASIH BERLAKU.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;SURAT MAHRAM (apabila wanita dengan usia di bawah 45 tahun berangkat tanpa muhrim dan jamaah usia dibawah 18 tahun).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;6 LEMBAR PAS FOTO UKURAN 3 X 4 DAN UKURAN 4 X 6 (latar foto berwarna putih, tegak lurus melihat ke depan, jilbab harus sempurna, tidak boleh kelihatan leher dan rambut bagi jamaah wanita, tidak memakai penutup kepala bagi jamaah pria, tidak boleh memakai kaca mata).&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;&lt;!-- &lt;div class=&quot;col-md-6&quot;&gt;\r\n								&lt;p class=&quot;m-b-1&quot;&gt;&lt;b&gt;Keterangan Program :&lt;/b&gt;&lt;/p&gt;\r\n								&lt;ol type=&quot;1&quot;&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;UMROH PROGRAM 9 HARI (5 MALAM DI MEKAH DAN 2 MALAM DI MADINAH) DENGAN TUJUAN AWAL DI MEKAH.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;JADWAL DAN RUTE PENERBANGAN DAPAT BERUBAH SEWAKTU-WAKTU TERGANTUNG KEBIJAKAN MASKAPAI.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;JADWAL DAN RUTE PENERBANGAN DAPAT BERUBAH SEWAKTU-WAKTU TERGANTUNG KEBIJAKAN MASKAPAI.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;PEMBAYARAN DAPAT DILAKUKAN 2 MACAM : PEMBAYARAN TUNAI &amp; PEMBAYARAN KREDIT&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;UANG MUKA UNTUK PEMESANAN PAKET Rp. 5.000.000,-&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;DAPATKAN VOUCHER UP TO Rp. 1.500.000,-&lt;/li&gt;\r\n								&lt;/ol&gt;\r\n							&lt;/div&gt; --&gt;\r\n&lt;div class=&quot;col-md-12&quot;&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&lt;strong&gt;Harga Belum Termasuk :&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN PASPOR.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;VAKSIN MANINGITIS.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TIPS UNTUK TOUR LEADER, MUNTHAWIF, PORTER DAN SOPIR SEBESAR RP.100.000,-.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGELUARAN PRIBADI (telephone, laundry dan sebagainya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN SURAT MAHRAM SEBESAR RP.300.000,- (bila ada).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TAMBAHAN BIAYA VISA SEBESAR 563 USD (biaya KBSA) DAN 2.100 SAR (apabila jamaah telah berangkat umroh pada musim uroh 1438 H dan setelahnya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;ASURANSI UMROH SEBESAR RP.150.000,-.&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '1439 H,Tharawat Andalusia,Odst Madinah,--,--,--', 'paket ekonomi umroh murah', 'assets/images/master_content/1523067725.jpg', 'admin', 2, 'Rp. 22.000.000', '-', '-', '', '', '2018-04-04 09:15:00', 'Y', '2018-04-04 16:18:46', '2018-04-07 09:32:33'),
(288, 'Bisnis', 'bisnis', '&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Travel Bag 24&amp;rdquo; ( Koper )&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Syal SKS&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mini Bag&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mukena/Ihram &amp;amp; Sabuk&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Seragam pria/ wanita ( Batik )&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Id Card Gantung&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Do&amp;rsquo;a&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Alumni&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Stiker Tag Travel Bag&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Persyaratan Peserta&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KTP.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KARTU KELUARGA (KSK).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU NIKAH ASLI (apabila suami istri).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;AKTE KELAHIRAN (apabila usia jamaah dibawah 18 tahun atau belum memiliki KTP).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PASPOR DENGAN 3 NAMA DAN MASIH BERLAKU SAMPAI DENGAN 7 BULAN SETELAH TANGGAL KEPULANGAN.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU VAKSIN MENINGITIS YANG MASIH BERLAKU.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;SURAT MAHRAM (apabila wanita dengan usia di bawah 45 tahun berangkat tanpa muhrim dan jamaah usia dibawah 18 tahun).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;6 LEMBAR PAS FOTO UKURAN 3 X 4 DAN UKURAN 4 X 6 (latar foto berwarna putih, tegak lurus melihat ke depan, jilbab harus sempurna, tidak boleh kelihatan leher dan rambut bagi jamaah wanita, tidak memakai penutup kepala bagi jamaah pria, tidak boleh memakai kaca mata).&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;&lt;!-- &lt;div class=&quot;col-md-6&quot;&gt;\r\n								&lt;p class=&quot;m-b-1&quot;&gt;&lt;b&gt;Keterangan Program :&lt;/b&gt;&lt;/p&gt;\r\n								&lt;ol type=&quot;1&quot;&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;UMROH PROGRAM 9 HARI (5 MALAM DI MEKAH DAN 2 MALAM DI MADINAH) DENGAN TUJUAN AWAL DI MEKAH.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;JADWAL DAN RUTE PENERBANGAN DAPAT BERUBAH SEWAKTU-WAKTU TERGANTUNG KEBIJAKAN MASKAPAI.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;JADWAL DAN RUTE PENERBANGAN DAPAT BERUBAH SEWAKTU-WAKTU TERGANTUNG KEBIJAKAN MASKAPAI.&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;PEMBAYARAN DAPAT DILAKUKAN 2 MACAM : PEMBAYARAN TUNAI &amp; PEMBAYARAN KREDIT&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;UANG MUKA UNTUK PEMESANAN PAKET Rp. 5.000.000,-&lt;/li&gt;\r\n									&lt;li class=&quot;li-list-border&quot;&gt;DAPATKAN VOUCHER UP TO Rp. 1.500.000,-&lt;/li&gt;\r\n								&lt;/ol&gt;\r\n							&lt;/div&gt; --&gt;\r\n&lt;div class=&quot;col-md-12&quot;&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&lt;strong&gt;Harga Belum Termasuk :&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN PASPOR.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;VAKSIN MANINGITIS.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TIPS UNTUK TOUR LEADER, MUNTHAWIF, PORTER DAN SOPIR SEBESAR RP.100.000,-.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGELUARAN PRIBADI (telephone, laundry dan sebagainya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN SURAT MAHRAM SEBESAR RP.300.000,- (bila ada).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TAMBAHAN BIAYA VISA SEBESAR 563 USD (biaya KBSA) DAN 2.100 SAR (apabila jamaah telah berangkat umroh pada musim uroh 1438 H dan setelahnya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;ASURANSI UMROH SEBESAR RP.150.000,-.&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '1439 H,Makarim Ajyad,Mukhtara International,--,--,--', 'paket bisnis umroh murah', 'assets/images/master_content/1522835905.jpg', 'admin', 4, 'Rp. 21.000.000', '-', '-', '', '', '2018-04-04 09:15:00', 'Y', '2018-04-04 16:20:35', '2018-04-07 09:31:44'),
(289, 'Information', 'information', '&lt;div class=&quot;col-md-3&quot;&gt;\r\n&lt;h4 class=&quot;m-b-1 border-bottom&quot;&gt;INFORMASI&lt;/h4&gt;\r\n&lt;div class=&quot;widget widget_links clearfix m-t-1&quot;&gt;\r\n&lt;ul class=&quot;m-b-m&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;../../../master_content/archives/about?v=static&quot;&gt;Tentang Kami&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;metode-pembayaran.html&quot;&gt;Metode Pembayaran&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;contact.html&quot;&gt;Hubungi Kami&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;gallery.html&quot;&gt;Gallery&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;syarat-ketentuan.html&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;kebijakan-privasi.html&quot;&gt;Kebijakan Privasi&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;https://eservices.haj.gov.sa/eservices3/pages/VisaPaymentInquiry/VisaInquiry.xhtml?dswid=-7810&quot; target=&quot;_blank&quot;&gt;Cek Visa (biaya tambahan)&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', 'information,sks,', 'information', NULL, 'admin', 0, 'information', '-', '-', '', '', '2018-04-04 12:15:00', 'Y', '2018-04-04 19:22:49', '2018-04-04 19:42:39'),
(290, 'Panduan', 'panduan', '&lt;div class=&quot;col-md-3&quot;&gt;\r\n&lt;h4 class=&quot;m-b-1 border-bottom&quot;&gt;PANDUAN&lt;/h4&gt;\r\n&lt;div class=&quot;widget widget_links clearfix m-t-1&quot;&gt;\r\n&lt;ul class=&quot;m-b-m&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;pembayaran.html&quot;&gt;Tata Cara Pembayaran&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan-registrasi-keagenan.html&quot;&gt;Registrasi Keagenan&lt;/a&gt;&lt;/li&gt;\r\n&lt;li class=&quot;li-konten-foter&quot;&gt;&lt;a href=&quot;panduan.html&quot;&gt;Panduan&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', 'panduan,sks', 'panduan sks', NULL, 'admin', 0, 'panduan', '-', '-', '', '', '2018-04-04 12:15:00', 'Y', '2018-04-04 19:23:56', '2018-04-04 19:23:56'),
(291, 'Manajemen', 'manajemen', '&lt;div class=&quot;col_full m-b-0&quot;&gt;\r\n&lt;div id=&quot;oc-clients-full&quot; class=&quot;owl-carousel owl-carousel-full image-carousel carousel-widget&quot; data-margin=&quot;30&quot; data-nav=&quot;true&quot; data-pagi=&quot;false&quot; data-loop=&quot;true&quot; data-autoplay=&quot;5000&quot; data-items-xxs=&quot;3&quot; data-items-xs=&quot;3&quot; data-items-sm=&quot;4&quot; data-items-md=&quot;4&quot; data-items-lg=&quot;5&quot;&gt;\r\n&lt;div class=&quot;oc-item&quot;&gt;&lt;img class=&quot;img-manajemen&quot; src=&quot;http://sinarkabah.com/demo/images/profile.jpg&quot; alt=&quot;&quot; /&gt;\r\n&lt;p class=&quot;m-b-0 text-center font-weight-600 m-t-1&quot;&gt;Direktur&lt;/p&gt;\r\n&lt;p class=&quot;text-center&quot;&gt;Sukiyanto Pangestu&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;oc-item&quot;&gt;&lt;img class=&quot;img-manajemen&quot; src=&quot;http://sinarkabah.com/demo/images/profile.jpg&quot; alt=&quot;&quot; /&gt;\r\n&lt;p class=&quot;m-b-0 text-center font-weight-600 m-t-1&quot;&gt;Manajer&lt;/p&gt;\r\n&lt;p class=&quot;text-center&quot;&gt;-&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;oc-item&quot;&gt;&lt;img class=&quot;img-manajemen&quot; src=&quot;http://sinarkabah.com/demo/images/profile.jpg&quot; alt=&quot;&quot; /&gt;\r\n&lt;p class=&quot;m-b-0 text-center font-weight-600 m-t-1&quot;&gt;Sekretaris&lt;/p&gt;\r\n&lt;p class=&quot;text-center&quot;&gt;-&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;oc-item&quot;&gt;&lt;img class=&quot;img-manajemen&quot; src=&quot;http://sinarkabah.com/demo/images/profile.jpg&quot; alt=&quot;&quot; /&gt;\r\n&lt;p class=&quot;m-b-0 text-center font-weight-600 m-t-1&quot;&gt;Bendahara&lt;/p&gt;\r\n&lt;p class=&quot;text-center&quot;&gt;-&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', 'manajement,sks', 'manajemen', NULL, 'admin', 0, 'manajemen', '-', '-', '', '', '2018-04-05 02:45:00', 'Y', '2018-04-05 10:07:28', '2018-04-06 01:44:02'),
(292, 'Rekening PT. SINAR KABAH SEMESTA', 'rekening_pt_sinar_kabah_semesta', '&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-8 col-md-offset-2&quot;&gt;\r\n&lt;div class=&quot;col_half&quot;&gt;&lt;img class=&quot;img-bank&quot; src=&quot;http://sinarkabah.com/demo/images/bca.png&quot; alt=&quot;&quot; /&gt;\r\n&lt;h2 class=&quot;text-center m-b-1&quot;&gt;BCA&lt;/h2&gt;\r\n&lt;p class=&quot;text-center font-size-22&quot;&gt;8490372777&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_half col_last&quot;&gt;&lt;img class=&quot;img-bank&quot; src=&quot;http://sinarkabah.com/demo/images/mandiri.png&quot; alt=&quot;&quot; /&gt;\r\n&lt;h2 class=&quot;text-center m-b-1&quot;&gt;Mandiri&lt;/h2&gt;\r\n&lt;p class=&quot;text-center font-size-22&quot;&gt;1130060016007&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '0', '0', NULL, 'admin', 0, '-', '-', '-', '', '', '2018-04-05 19:30:00', 'Y', '2018-04-06 02:39:19', '2018-04-06 03:46:09'),
(293, '91 Calon Umrah Asal Sumatera Barat Telantar di Malaysia', '91_calon_umrah_asal_sumatera_barat_telantar_di_malaysia', '&lt;p class=&quot;m-b-1&quot;&gt;Sebanyak 91 calon jemaah umrah asal Sumatera Barat telantar di Malaysia setelah dijanjikan akan diberangkatkan ke Tanah Suci pada 26 Maret 2018. Calon jemaah umrah itu berangkat melalui biro perjalanan umrah PT Bumi Minang Pertiwi (BMP).&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Pimpinan biro perjalanan umrah PT Rindu Baitulah, Epi Santoso, menerangkan pihaknya bekerja sama dengan PT BMP untuk memberangkatkan 91 orang jemaah. Mereka sudah membayar lunas paket senilai Rp 1,7 miliar atau Rp 19 juta per orang, tetapi batal berangkat.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Atas kejadian itu, Direktur Utama BMP Edi Kurniawan menjanjikan akan memulangkan jemaah umrah dari Malaysia paling lambat pada 5 April 2018.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Beberapa hari ini kami akan lakukan pemulangan jemaah di Malaysia yang dibagi dalam beberapa grup,&quot; kata Edi di Padang, Minggu, 1 April 2018, usai dipanggil oleh Kementerian Agama Sumbar, dilansir &lt;em&gt;Antara&lt;/em&gt;.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Ia menyebutkan pada Senin (2/4/2018) akan dipulangkan satu grup dari Malaysia, menyusul kemudian tiga grup dipulangkan pada Selasa, 3 April 2018 dan satu grup lagi pada Kamis, 5 April 2018. Ia berjanji angoota jemaah yang tertunda keberangkatannya akan dijadwalkan ulang pada Oktober, November, dan Desember 2018.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Permasalahan yang membuat angoota jemaah telantar bermula dari proses konfirmasi deposit atas tiket yang terkendala. Pihak BMP mengklaim sudah menyerahkan deposit untuk pembayaran tiket pesawat jemaah kepada sebuah biro perjalanan di Malaysia.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Namun, pihak biro perjalanan di Malaysia tidak mengonfirmasi pembayaran tiket tersebut. Akibatnya, perjalanan jemaah umrah ke Tanah Suci tidak dapat dilanjutkan.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Kami sudah menjelaskan ini kepada jemaah,&quot; ujar dia.&lt;/p&gt;', '0', 'Sebanyak 91 calon jemaah umrah asal Sumatera Barat telantar di Malaysia setelah dijanjikan akan diberangkatkan ke Tanah Suci pada 26 Maret 2018. Calon jemaah umrah itu berangkat melalui biro', 'assets/images/master_content/1523075217.jpg', 'admin', 2, 'Liputan6.com, Padang', '-', '-', '', '', '2018-04-05 20:30:00', 'Y', '2018-04-06 03:44:59', '2018-04-01 13:34:00'),
(294, 'Jinem', 'jinem', '&lt;p&gt;Jl.sukkomulyo pertengahan yogyakarta&lt;/p&gt;', '0', 'jinemgaul@gmail.com', NULL, 'admin', 0, '083746577334', '-', '-', '', '', '2018-04-06 07:15:00', 'Y', '2018-04-06 14:25:17', '2018-04-06 14:32:10'),
(295, 'Eksekutif', 'eksekutif', '&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Travel Bag 24&amp;rdquo; ( Koper )&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Syal SKS&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mini Bag&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mukena/Ihram &amp;amp; Sabuk&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Seragam pria/ wanita ( Batik )&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Id Card Gantung&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Do&amp;rsquo;a&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Alumni&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Stiker Tag Travel Bag&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Persyaratan Peserta&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KTP.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KARTU KELUARGA (KSK).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU NIKAH ASLI (apabila suami istri).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;AKTE KELAHIRAN (apabila usia jamaah dibawah 18 tahun atau belum memiliki KTP).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PASPOR DENGAN 3 NAMA DAN MASIH BERLAKU SAMPAI DENGAN 7 BULAN SETELAH TANGGAL KEPULANGAN.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU VAKSIN MENINGITIS YANG MASIH BERLAKU.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;SURAT MAHRAM (apabila wanita dengan usia di bawah 45 tahun berangkat tanpa muhrim dan jamaah usia dibawah 18 tahun).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;6 LEMBAR PAS FOTO UKURAN 3 X 4 DAN UKURAN 4 X 6 (latar foto berwarna putih, tegak lurus melihat ke depan, jilbab harus sempurna, tidak boleh kelihatan leher dan rambut bagi jamaah wanita, tidak memakai penutup kepala bagi jamaah pria, tidak boleh memakai kaca mata).&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-12&quot;&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&lt;strong&gt;Harga Belum Termasuk :&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN PASPOR.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;VAKSIN MANINGITIS.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TIPS UNTUK TOUR LEADER, MUNTHAWIF, PORTER DAN SOPIR SEBESAR RP.100.000,-.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGELUARAN PRIBADI (telephone, laundry dan sebagainya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN SURAT MAHRAM SEBESAR RP.300.000,- (bila ada).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TAMBAHAN BIAYA VISA SEBESAR 563 USD (biaya KBSA) DAN 2.100 SAR (apabila jamaah telah berangkat umroh pada musim uroh 1438 H dan setelahnya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;ASURANSI UMROH SEBESAR RP.150.000,-.&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '1439 H,Anjum / Setaraf,Hayyat International,--,--,--', 'paket eksekutif umrah murah', 'assets/images/master_content/1523068872.jpg', 'admin', 0, 'Rp. 24.100.000', '-', '-', '', '', '2018-04-07 02:30:00', 'Y', '2018-04-07 09:40:23', '2018-04-07 09:41:11');
INSERT INTO `site_news` (`news_id`, `news_title`, `news_permalink`, `news_content`, `news_meta_tags`, `news_meta_description`, `news_photo`, `news_input_by`, `news_number_of_read`, `news_label_1`, `news_label_2`, `news_label_3`, `news_external_link`, `news_external_link_label`, `news_timestamp`, `news_is_deleted`, `news_input_datetime`, `news_update_datetime`) VALUES
(296, 'VIP', 'vip', '&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Travel Bag 24&amp;rdquo; ( Koper )&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Syal SKS&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mini Bag&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mukena/Ihram &amp;amp; Sabuk&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Seragam pria/ wanita ( Batik )&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Id Card Gantung&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Do&amp;rsquo;a&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Alumni&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Stiker Tag Travel Bag&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Persyaratan Peserta&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KTP.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KARTU KELUARGA (KSK).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU NIKAH ASLI (apabila suami istri).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;AKTE KELAHIRAN (apabila usia jamaah dibawah 18 tahun atau belum memiliki KTP).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PASPOR DENGAN 3 NAMA DAN MASIH BERLAKU SAMPAI DENGAN 7 BULAN SETELAH TANGGAL KEPULANGAN.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU VAKSIN MENINGITIS YANG MASIH BERLAKU.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;SURAT MAHRAM (apabila wanita dengan usia di bawah 45 tahun berangkat tanpa muhrim dan jamaah usia dibawah 18 tahun).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;6 LEMBAR PAS FOTO UKURAN 3 X 4 DAN UKURAN 4 X 6 (latar foto berwarna putih, tegak lurus melihat ke depan, jilbab harus sempurna, tidak boleh kelihatan leher dan rambut bagi jamaah wanita, tidak memakai penutup kepala bagi jamaah pria, tidak boleh memakai kaca mata).&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-12&quot;&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&lt;strong&gt;Harga Belum Termasuk :&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN PASPOR.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;VAKSIN MANINGITIS.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TIPS UNTUK TOUR LEADER, MUNTHAWIF, PORTER DAN SOPIR SEBESAR RP.100.000,-.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGELUARAN PRIBADI (telephone, laundry dan sebagainya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN SURAT MAHRAM SEBESAR RP.300.000,- (bila ada).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TAMBAHAN BIAYA VISA SEBESAR 563 USD (biaya KBSA) DAN 2.100 SAR (apabila jamaah telah berangkat umroh pada musim uroh 1438 H dan setelahnya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;ASURANSI UMROH SEBESAR RP.150.000,-.&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '1439 H,Sofwah Archid / Setaraf,Royal In Rawdah', 'paket vip umrah murah', 'assets/images/master_content/1523069140.jpg', 'admin', 0, 'Rp. 28.150.000', '-', '-', '', '', '2018-04-07 02:30:00', 'Y', '2018-04-07 09:45:40', '2018-04-07 09:45:40'),
(297, 'VVIP', 'vvip', '&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Fasilitas&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Travel Bag 24&amp;rdquo; ( Koper )&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Syal SKS&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mini Bag&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Mukena/Ihram &amp;amp; Sabuk&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Seragam pria/ wanita ( Batik )&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col-md-6&quot;&gt;\r\n&lt;ul class=&quot;m-b-1&quot; type=&quot;none&quot;&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Id Card Gantung&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Do&amp;rsquo;a&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Buku Alumni&lt;/li&gt;\r\n&lt;li class=&quot;li-list&quot;&gt;Stiker Tag Travel Bag&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Persyaratan Peserta&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KTP.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;FOTO COPY KARTU KELUARGA (KSK).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU NIKAH ASLI (apabila suami istri).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;AKTE KELAHIRAN (apabila usia jamaah dibawah 18 tahun atau belum memiliki KTP).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PASPOR DENGAN 3 NAMA DAN MASIH BERLAKU SAMPAI DENGAN 7 BULAN SETELAH TANGGAL KEPULANGAN.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;BUKU VAKSIN MENINGITIS YANG MASIH BERLAKU.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;SURAT MAHRAM (apabila wanita dengan usia di bawah 45 tahun berangkat tanpa muhrim dan jamaah usia dibawah 18 tahun).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;6 LEMBAR PAS FOTO UKURAN 3 X 4 DAN UKURAN 4 X 6 (latar foto berwarna putih, tegak lurus melihat ke depan, jilbab harus sempurna, tidak boleh kelihatan leher dan rambut bagi jamaah wanita, tidak memakai penutup kepala bagi jamaah pria, tidak boleh memakai kaca mata).&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;content-wrap&quot;&gt;\r\n&lt;div class=&quot;container clearfix&quot;&gt;\r\n&lt;div class=&quot;col_full m-b-1&quot;&gt;\r\n&lt;div class=&quot;fancy-title title-dotted-border title-center&quot;&gt;\r\n&lt;h3 class=&quot;text-center&quot;&gt;Syarat &amp;amp; Ketentuan&lt;/h3&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-md-12&quot;&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&lt;strong&gt;Harga Belum Termasuk :&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ol class=&quot;m-b-1&quot; type=&quot;1&quot;&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN PASPOR.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;VAKSIN MANINGITIS.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TIPS UNTUK TOUR LEADER, MUNTHAWIF, PORTER DAN SOPIR SEBESAR RP.100.000,-.&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGELUARAN PRIBADI (telephone, laundry dan sebagainya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;PENGURUSAN SURAT MAHRAM SEBESAR RP.300.000,- (bila ada).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;TAMBAHAN BIAYA VISA SEBESAR 563 USD (biaya KBSA) DAN 2.100 SAR (apabila jamaah telah berangkat umroh pada musim uroh 1438 H dan setelahnya).&lt;/li&gt;\r\n&lt;li class=&quot;li-list-border&quot;&gt;ASURANSI UMROH SEBESAR RP.150.000,-.&lt;/li&gt;\r\n&lt;/ol&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '1439 H,Fairmont / Setaraf,Royal In Rawdah,--,--,--', 'paket murah vvip umrah', 'assets/images/master_content/1523069318.jpg', 'admin', 0, 'Rp. 30.000.000', '-', '-', '', '', '2018-04-07 02:45:00', 'Y', '2018-04-07 09:48:37', '2018-04-07 09:48:37'),
(298, 'UMROH SESUAI SUNNAH DAN AMANAH', 'umroh_sesuai_sunnah_dan_amanah', '&lt;p class=&quot;m-b-1&quot;&gt;Seorang nenek mengembuskan napas terakhirnya dalam perjalanan pulang dari ibadah umrah di Tanah Suci. Dia meninggal dunia sebelum pesawat maskapai Citilink yang ditumpanginya tiba di Bandara Soekarno-Hatta.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Kabid Humas Polda Metro Jaya Kombes Raden Prabowo Argo Yuwono menyampaikan, temuan itu langsung diurus petugas pada Rabu, 7 Februari 2018 sekitar pukul 05.00 WIB sesaat setelah pesawat mendarat.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Korban meninggal dunia diperkirakan 10 jam sebelum pesawat mendarat di Bandara Soekarno Hatta,&quot; tutur Argo saat dikonfirmasi di Jakarta, Kamis (8/2/2018).&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Korban atas nama Supami itu berusia 66 tahun. Menurut keterangan dari saksi, saat korban melaksanakan ibadah umrah di Arab Saudi memang sudah dalam keadaan sakit.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Saat hari terakhir korban menunaikan ibadah umrah, korban mengeluh lemas dan sakit di bagian pinggang,&quot; ucap dia.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Ketika waktunya pulang, korban menuju bandara di Jeddah dengan menggunakan kursi roda. Pertolongan pun segera diberikan saat kondisi kesehatan Supami berangsur menurun.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Di dalam pesawat, korban sudah semakin lemah. Berada di dalam pesawat dengan diberikan oksigen dan CVR, akan tetapi korban tidak tertolong,&quot; kata Argo.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Setibanya di Bandara Soekarno Hatta, petugas langsung melakukan pemeriksaan medis. Oleh dokter yang menangani, korban dinyatakan meninggal dunia disebabkan henti napas dan henti jantung pada 6 Februari 2018 sekitar pukul 18.30 WIB.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Dikeluarkannya Surat Keterangan Kematian Nomor 00399/CC/II/2018 tertanggal 7 Februari 2018,&quot; Argo menandaskan.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Sementara itu, pihak keluarga korban yang diwakili oleh ketua rombongan menolak dilakukan visum luar dan dalam. Rencana jenazah Supami akan dibawa ke rumah duka di kediaman korban yang berada di Semarang.&lt;/p&gt;', 'umroh,amanah,sunnah', 'BISMILLAH..\r\n\r\nMASIH TERSEDIA 5 SEAT LAGI..\r\n\r\nKEBERANGKATAN 02 MEI 2018\r\n\r\nBiaya Rp. 23.5 jt (sdh termasuk perlengkapan umroh) harga start palembang.', 'assets/images/master_content/1523076466.jpg', 'admin', 0, 'Sinar kabah semesta', '-', '-', '', '', '2018-04-07 04:30:00', 'Y', '2018-04-07 11:47:46', '2018-04-06 18:29:00'),
(299, 'Hadiah Umrah bagi Marbut Terbaik di Garut', 'hadiah_umrah_bagi_marbut_terbaik_di_garut', '&lt;p class=&quot;m-b-1&quot;&gt;&lt;strong class=&quot;orange&quot;&gt;Liputan6.com, Bandung - &lt;/strong&gt; Kepala Kepolisian Daerah Jawa Barat Irjen Agung Budi Maryoto memberangkatkan seorang marbut masjid teladan dari Kecamatan Cikajang, Kabupaten Garut, untuk beribadah umrah sebagai hadiah dan penghargaan dari kepolisian.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Ini (hadiah umrah) sebagai bentuk apresiasi Polda Jabar,&quot; kata Agung Budi Maryoto saat bakti sosial dan Deklarasi Anti &quot;Hoax&quot; di Alun-alun Limbangan, Garut, Kamis, 22 Maret 2018, dilansir &lt;em&gt;Antara&lt;/em&gt;.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Ia menuturkan, kepolisian memberikan penghargaan dan bantuan peralatan untuk menunjang kegiatan di masjid kepada tiga orang Dewan Kemakmuran Masjid (DKM) dan tiga marbut atau penjaga masjid di Kabupaten Garut.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Khusus marbut terbaik hasil penilaian polisi, kata Agung, diberi hadiah spesial berupa tiket umrah ke Tanah Suci. Hadiah itu jatuh kepada Deden Sambas sebagai marbut asal Kecamatan Cikajang.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Marbut dan DKM adalah suatu profesi yang sangat mulia, membersihkan masjid, mengingatkan waktu shalat, mengumandangkan azan, menggantikan imam saat berhalangan hadir,&quot; katanya.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Selain penghargaan kepada marbut, Kapolda juga memberikan penghargaan kepada tiga regu Sistem Keamanan Lingkungan (Siskamling) yang dinilai aktif di Kabupaten Garut.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Kapolda secara langsung memberikan sejumlah bantuan yang dibutuhkan warga di Kampung Babakan Rancasaat, Desa Cigagade, Kecamatan Limbangan, sekaligus menyerahkan bantuan renovasi dan bedah rumah tidak layak huni.&lt;/p&gt;', '0', 'Kepala Kepolisian Daerah Jawa Barat Irjen Agung Budi Maryoto memberangkatkan seorang marbut masjid teladan dari Kecamatan Cikajang, Kabupaten Garut, untuk beribadah umrah sebagai hadiah dan ', 'assets/images/master_content/1523080374.jpg', 'admin', 0, 'Liputan6.com, Bandung', '-', '-', '', '', '2018-04-07 04:45:00', 'Y', '2018-04-07 11:49:11', '2018-04-02 13:34:54'),
(300, 'Pulang Umrah, Penumpang Citilink Meninggal Dunia di Pesawat', 'pulang_umrah_penumpang_citilink_meninggal_dunia_di_pesawat', '&lt;p class=&quot;m-b-1&quot;&gt;Seorang nenek mengembuskan napas terakhirnya dalam perjalanan pulang dari ibadah umrah di Tanah Suci. Dia meninggal dunia sebelum pesawat maskapai Citilink yang ditumpanginya tiba di Bandara Soekarno-Hatta.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Kabid Humas Polda Metro Jaya Kombes Raden Prabowo Argo Yuwono menyampaikan, temuan itu langsung diurus petugas pada Rabu, 7 Februari 2018 sekitar pukul 05.00 WIB sesaat setelah pesawat mendarat.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Korban meninggal dunia diperkirakan 10 jam sebelum pesawat mendarat di Bandara Soekarno Hatta,&quot; tutur Argo saat dikonfirmasi di Jakarta, Kamis (8/2/2018).&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Korban atas nama Supami itu berusia 66 tahun. Menurut keterangan dari saksi, saat korban melaksanakan ibadah umrah di Arab Saudi memang sudah dalam keadaan sakit.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Saat hari terakhir korban menunaikan ibadah umrah, korban mengeluh lemas dan sakit di bagian pinggang,&quot; ucap dia.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Ketika waktunya pulang, korban menuju bandara di Jeddah dengan menggunakan kursi roda. Pertolongan pun segera diberikan saat kondisi kesehatan Supami berangsur menurun.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Di dalam pesawat, korban sudah semakin lemah. Berada di dalam pesawat dengan diberikan oksigen dan CVR, akan tetapi korban tidak tertolong,&quot; kata Argo.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Setibanya di Bandara Soekarno Hatta, petugas langsung melakukan pemeriksaan medis. Oleh dokter yang menangani, korban dinyatakan meninggal dunia disebabkan henti napas dan henti jantung pada 6 Februari 2018 sekitar pukul 18.30 WIB.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;&quot;Dikeluarkannya Surat Keterangan Kematian Nomor 00399/CC/II/2018 tertanggal 7 Februari 2018,&quot; Argo menandaskan.&lt;/p&gt;\r\n&lt;p class=&quot;m-b-1&quot;&gt;Sementara itu, pihak keluarga korban yang diwakili oleh ketua rombongan menolak dilakukan visum luar dan dalam. Rencana jenazah Supami akan dibawa ke rumah duka di kediaman korban yang berada di Semarang.&lt;/p&gt;', '0', 'Seorang nenek mengembuskan napas terakhirnya dalam perjalanan pulang dari ibadah umrah di Tanah Suci. Dia meninggal dunia sebelum pesawat maskapai Citilink yang ditumpanginya tiba di Bandara', 'assets/images/master_content/1523079097.jpg', 'admin', 0, 'Liputan6.com, Jakarta', '-', '-', '', '', '2018-04-07 05:15:00', 'Y', '2018-04-07 12:31:37', '2018-04-02 13:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `site_news_category`
--

CREATE TABLE `site_news_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_title` varchar(200) NOT NULL,
  `category_permalink` varchar(200) NOT NULL,
  `category_content` text,
  `category_input_by` varchar(100) NOT NULL,
  `category_meta_description` varchar(200) DEFAULT NULL,
  `category_meta_tags` varchar(200) DEFAULT NULL,
  `category_photo` varchar(200) DEFAULT NULL,
  `category_type` enum('static','article','product','gallery','archives_tag','event','widget') NOT NULL DEFAULT 'article',
  `category_input_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_news_category`
--

INSERT INTO `site_news_category` (`category_id`, `category_title`, `category_permalink`, `category_content`, `category_input_by`, `category_meta_description`, `category_meta_tags`, `category_photo`, `category_type`, `category_input_timestamp`) VALUES
(1, 'Download', 'download', '<p>-</p>', 'admin', '', '', '', 'widget', '2018-01-12 17:02:56'),
(2, 'Brosur', 'brosur', '<p>-</p>', 'admin', 'galery brosur sks', 'galery,brosur,sks', '', 'gallery', '2018-04-04 05:16:47'),
(3, 'Info', 'news', '<p>-</p>', 'admin', '', '', '', 'article', '2018-01-11 15:11:40'),
(4, 'Itenerary', 'itenerary', '<p>-</p>', 'admin', '', '', NULL, 'static', '2018-01-11 15:11:28'),
(5, 'PPOB', 'ppob', '<p>-</p>', 'admin', '', '', '', 'static', '2018-01-11 15:11:17'),
(6, 'Layanan', 'layanan', '<p>-</p>', 'admin', '', '', '', 'static', '2018-01-11 15:10:59'),
(7, 'Marketing Plan', 'marketing_plan', '<p>-</p>', 'admin', '', '', '-', 'static', '2018-01-11 15:10:33'),
(8, 'About', 'about', '<p>-</p>', 'admin', '', '', '-', 'static', '2018-01-11 15:10:07'),
(9, 'Home', 'home', '<p>-</p>', 'admin', '', '', NULL, 'static', '2018-01-11 15:08:54'),
(10, 'Paket Umrah Promo', 'paket_umrah_promo', '<p>-</p>', 'admin', 'paket umrah promo', 'paket,umrah,promo', 'assets/images/master_content_category/1522636354.jpg', 'product', '2018-04-07 01:08:57'),
(11, 'Paket Umrah Reguler', 'paket_umrah_reguler', '<p>-</p>', 'admin', 'paket umrah reguler', 'paket,umrah,reguler', 'assets/images/master_content_category/1522636417.jpg', 'product', '2018-04-05 03:13:06'),
(12, 'Photo', 'photo', '<p>-</p>', 'admin', 'galeri photo', 'galery,photo,sks', '', 'gallery', '2018-04-04 05:16:55'),
(13, 'Video', 'video', '<p>-</p>', 'admin', 'galery video sks', 'galery,video,sks', '', 'gallery', '2018-04-04 05:17:01'),
(15, 'Paket Umrah Plus', 'paket_umrah_plus', '<p>-</p>', 'admin', 'paket umrah plus', 'paket, umrah, plus', NULL, 'product', '2018-04-04 06:33:35'),
(16, 'Paket Umrah Ramadhan', 'paket_umrah_ramadhan', '<p>-</p>', 'admin', 'paket umrah ramadhan', 'paket, umrah, ramadhan', NULL, 'article', '2018-04-04 06:34:25'),
(17, 'Berita', 'berita', '<p>-</p>', 'admin', 'berita sinar kabah semesta terbaru', 'berita,sinarkabah,terbaru', NULL, 'article', '2018-04-05 20:37:50'),
(18, 'Perwakilan', 'perwakilan', '<p>-</p>', 'admin', 'perwakilan', 'perwakilan', NULL, 'article', '2018-04-06 07:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `site_news_counter`
--

CREATE TABLE `site_news_counter` (
  `news_counter_id` int(11) NOT NULL,
  `news_counter_news_id` int(10) UNSIGNED NOT NULL,
  `news_counter_ip_address` int(10) UNSIGNED NOT NULL,
  `news_counter_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_news_counter`
--

INSERT INTO `site_news_counter` (`news_counter_id`, `news_counter_news_id`, `news_counter_ip_address`, `news_counter_timestamp`) VALUES
(1, 224, 2130706433, '2016-11-16 04:35:24'),
(2, 225, 2130706433, '2016-11-16 05:52:35'),
(3, 224, 2130706433, '2016-11-16 06:31:58'),
(4, 237, 2130706433, '2016-11-16 08:06:06'),
(5, 225, 2130706433, '2016-11-16 08:21:15'),
(6, 237, 2130706433, '2016-11-16 09:04:03'),
(7, 237, 609291457, '2016-11-22 03:29:43'),
(8, 237, 1941100608, '2016-11-22 06:46:05'),
(9, 237, 1941100608, '2016-11-22 07:31:39'),
(10, 237, 1941100608, '2016-11-22 08:07:57'),
(11, 237, 2130706433, '2016-11-24 13:38:41'),
(12, 237, 2130706433, '2016-11-24 16:40:50'),
(13, 237, 2130706433, '2016-11-25 14:55:28'),
(14, 237, 3036065587, '2016-11-30 10:56:32'),
(15, 237, 1920798411, '2016-11-30 11:53:01'),
(16, 237, 609194421, '2016-12-01 02:27:08'),
(17, 237, 609194421, '2016-12-01 03:19:49'),
(18, 237, 609296583, '2016-12-08 04:27:21'),
(19, 237, 1113647755, '2016-12-14 10:37:39'),
(20, 237, 1113647755, '2016-12-15 03:26:38'),
(21, 237, 609309578, '2016-12-15 10:59:35'),
(22, 237, 1113647755, '2016-12-15 11:15:58'),
(23, 216, 2107776554, '2016-12-15 16:27:50'),
(24, 216, 1266556098, '2016-12-15 16:28:00'),
(25, 237, 1113647755, '2016-12-16 08:57:01'),
(26, 237, 1113647755, '2016-12-16 10:24:24'),
(27, 223, 1113647755, '2016-12-16 10:25:32'),
(28, 223, 3098368712, '2016-12-16 10:25:36'),
(29, 223, 1161804924, '2016-12-16 10:25:42'),
(30, 223, 1161804905, '2016-12-16 10:25:43'),
(31, 237, 1113647755, '2016-12-16 10:59:36'),
(32, 237, 1113647755, '2016-12-19 01:38:35'),
(33, 237, 1113647755, '2016-12-19 09:10:08'),
(34, 237, 1113647755, '2016-12-20 02:49:19'),
(35, 237, 1113647755, '2016-12-20 04:32:55'),
(36, 237, 1113647755, '2016-12-20 05:10:54'),
(37, 217, 1113647755, '2016-12-20 06:20:41'),
(38, 217, 3098368712, '2016-12-20 06:20:44'),
(39, 237, 1113647755, '2016-12-20 08:05:44'),
(40, 237, 1113647755, '2016-12-20 12:54:14'),
(41, 220, 1113647755, '2016-12-21 08:19:55'),
(42, 220, 1266556098, '2016-12-21 08:19:59'),
(43, 237, 1113647755, '2016-12-21 09:57:10'),
(44, 237, 1113647755, '2016-12-21 11:05:38'),
(45, 237, 1113647755, '2016-12-22 07:52:36'),
(46, 237, 1920796774, '2016-12-22 08:07:17'),
(47, 237, 1113647755, '2016-12-22 09:43:43'),
(48, 220, 1113647755, '2016-12-22 10:21:14'),
(49, 237, 1920797386, '2016-12-22 13:37:51'),
(50, 237, 1113647755, '2016-12-23 04:51:09'),
(51, 237, 3399665252, '2016-12-23 12:30:21'),
(52, 237, 3076213508, '2016-12-23 12:30:22'),
(53, 237, 1113647755, '2016-12-24 04:57:29'),
(54, 237, 1920806902, '2016-12-24 05:09:17'),
(55, 237, 1113647755, '2016-12-24 05:41:20'),
(56, 237, 1113647755, '2016-12-26 06:49:22'),
(57, 237, 1113647755, '2016-12-27 02:21:09'),
(58, 237, 1113647755, '2016-12-28 07:37:37'),
(59, 218, 1113647755, '2016-12-29 07:32:35'),
(60, 237, 1113647755, '2017-01-03 07:04:01'),
(61, 220, 1113647755, '2017-01-13 12:05:41'),
(62, 237, 1113647755, '2017-01-13 12:06:42'),
(63, 237, 1113647755, '2017-01-16 07:56:59'),
(64, 237, 1113647755, '2017-01-23 13:05:16'),
(65, 220, 1113647755, '2017-01-24 06:39:11'),
(66, 237, 1113647755, '2017-01-24 06:42:22'),
(67, 213, 1113647755, '2017-01-24 07:38:11'),
(68, 217, 3393065523, '2017-01-24 08:10:35'),
(69, 213, 918957570, '2017-01-24 08:29:29'),
(70, 218, 1113647755, '2017-02-03 06:20:54'),
(71, 237, 1113647755, '2017-02-16 08:02:59'),
(72, 213, 3393065292, '2017-02-23 05:22:56'),
(73, 237, 3393065292, '2017-02-23 05:29:57'),
(74, 237, 1893183425, '2017-02-27 10:51:07'),
(75, 237, 1893182991, '2017-02-27 11:02:32'),
(76, 237, 1113647755, '2017-02-28 10:18:19'),
(77, 237, 1113647755, '2017-03-17 11:10:55'),
(78, 237, 1986043433, '2017-03-22 03:03:20'),
(79, 237, 1113647755, '2017-04-10 01:02:32'),
(80, 237, 1113647755, '2017-04-10 03:52:50'),
(81, 237, 1113647755, '2017-04-10 09:30:13'),
(82, 237, 1113647755, '2017-04-13 12:20:30'),
(83, 237, 1113647755, '2017-04-18 08:10:52'),
(84, 237, 1113647755, '2017-04-26 07:01:50'),
(85, 218, 1113647755, '2017-05-08 04:31:54'),
(86, 220, 1113647755, '2017-05-08 04:36:15'),
(87, 218, 3393065411, '2017-05-08 04:42:07'),
(88, 217, 1113647755, '2017-05-08 04:45:53'),
(89, 219, 3393065411, '2017-05-08 05:16:08'),
(90, 237, 1113647755, '2017-05-09 09:30:06'),
(91, 237, 1941106603, '2017-05-10 13:04:24'),
(92, 220, 1113647755, '2017-05-12 08:27:09'),
(93, 237, 1113647755, '2017-05-26 06:25:09'),
(94, 220, 1113647755, '2017-05-26 08:07:20'),
(95, 217, 2660019765, '2017-06-07 00:37:57'),
(96, 237, 2660017193, '2017-06-07 00:38:15'),
(97, 237, 609278016, '2017-06-17 05:38:16'),
(98, 237, 1848792376, '2017-06-17 05:43:47'),
(99, 237, 609306571, '2017-07-15 12:11:27'),
(100, 237, 3680249206, '2017-07-17 03:48:47'),
(101, 220, 1113647755, '2017-11-29 00:55:54'),
(102, 237, 1113647755, '2017-11-29 00:59:40'),
(103, 237, 2130706433, '2018-01-08 05:22:23'),
(104, 237, 2130706433, '2018-01-08 12:46:50'),
(105, 237, 2130706433, '2018-01-08 17:28:32'),
(106, 221, 2130706433, '2018-01-08 17:28:50'),
(107, 286, 2130706433, '2018-04-06 08:28:03'),
(108, 293, 2130706433, '2018-04-06 08:29:14'),
(109, 286, 2130706433, '2018-04-06 09:03:54'),
(110, 286, 2130706433, '2018-04-06 09:33:56'),
(111, 286, 2130706433, '2018-04-06 10:04:31'),
(112, 286, 2130706433, '2018-04-06 10:40:16'),
(113, 287, 2130706433, '2018-04-06 10:49:01'),
(114, 287, 2130706433, '2018-04-06 11:19:53'),
(115, 288, 2130706433, '2018-04-06 12:02:58'),
(116, 288, 2130706433, '2018-04-06 15:33:38'),
(117, 286, 2130706433, '2018-04-06 15:38:09'),
(118, 288, 2130706433, '2018-04-07 01:20:29'),
(119, 286, 2130706433, '2018-04-07 01:39:26'),
(120, 286, 2130706433, '2018-04-07 02:10:47'),
(121, 288, 2130706433, '2018-04-07 02:37:02'),
(122, 293, 2130706433, '2018-04-07 04:13:07');

-- --------------------------------------------------------

--
-- Table structure for table `site_news_more_images`
--

CREATE TABLE `site_news_more_images` (
  `news_more_images_id` int(10) UNSIGNED NOT NULL,
  `news_more_images_news_id` int(10) UNSIGNED NOT NULL,
  `news_more_images_file` varchar(200) NOT NULL,
  `news_more_images_is_active` int(1) NOT NULL,
  `news_more_images_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_news_more_images`
--

INSERT INTO `site_news_more_images` (`news_more_images_id`, `news_more_images_news_id`, `news_more_images_file`, `news_more_images_is_active`, `news_more_images_timestamp`) VALUES
(1, 238, 'assets/images/master_content/14824957751.jpg', 1, '2016-12-23 12:22:55'),
(2, 234, 'assets/images/master_content/14825502421.jpg', 1, '2016-12-24 03:30:42'),
(3, 213, 'assets/images/master_content/1485243861.jpg', 1, '2017-01-24 07:44:21'),
(4, 213, 'assets/images/master_content/1485243929.jpg', 1, '2017-01-24 07:45:29'),
(5, 234, 'assets/images/master_content/14914681201.JPG', 1, '2017-04-06 08:42:00'),
(6, 234, 'assets/images/master_content/14914681202.JPG', 1, '2017-04-06 08:42:00'),
(11, 283, 'assets/images/master_content/1522820243.jpg', 1, '2018-04-04 05:37:23'),
(16, 284, 'assets/images/master_content/1522829372.jpg', 1, '2018-04-04 08:09:32'),
(17, 284, 'assets/images/master_content/15228293721.jpg', 1, '2018-04-04 08:09:32'),
(18, 284, 'assets/images/master_content/15228293722.jpg', 1, '2018-04-04 08:09:32'),
(19, 285, 'assets/images/master_content/15228323481.png', 1, '2018-04-04 08:59:08'),
(20, 285, 'assets/images/master_content/15228323482.png', 1, '2018-04-04 08:59:08'),
(21, 285, 'assets/images/master_content/15228323483.png', 1, '2018-04-04 08:59:08'),
(22, 285, 'assets/images/master_content/15228323484.png', 1, '2018-04-04 08:59:08'),
(23, 285, 'assets/images/master_content/15228323485.png', 1, '2018-04-04 08:59:08'),
(24, 285, 'assets/images/master_content/15228323486.png', 1, '2018-04-04 08:59:08'),
(25, 285, 'assets/images/master_content/15228323487.png', 1, '2018-04-04 08:59:08'),
(26, 285, 'assets/images/master_content/15228323488.png', 1, '2018-04-04 08:59:08'),
(27, 249, 'assets/images/master_content/15229547721.jpeg', 1, '2018-04-05 18:59:32'),
(28, 249, 'assets/images/master_content/1522954773.jpg', 1, '2018-04-05 18:59:33'),
(29, 249, 'assets/images/master_content/1522954773.jpeg', 1, '2018-04-05 18:59:33'),
(30, 249, 'assets/images/master_content/15229547731.jpeg', 1, '2018-04-05 18:59:33'),
(31, 249, 'assets/images/master_content/15229547732.jpeg', 1, '2018-04-05 18:59:33'),
(32, 249, 'assets/images/master_content/15229547733.jpeg', 1, '2018-04-05 18:59:33'),
(33, 249, 'assets/images/master_content/15229547734.jpeg', 1, '2018-04-05 18:59:33'),
(34, 286, 'assets/images/master_content/15230651261.jpg', 1, '2018-04-06 09:36:09'),
(35, 286, 'assets/images/master_content/15230651262.jpg', 1, '2018-04-06 10:11:02'),
(37, 286, 'assets/images/master_content/15230651263.jpg', 1, '2018-04-07 01:38:46'),
(38, 287, 'assets/images/master_content/15230677251.jpg', 1, '2018-04-07 02:22:05'),
(39, 287, 'assets/images/master_content/15230677252.jpg', 1, '2018-04-07 02:22:05'),
(40, 287, 'assets/images/master_content/1523067726.jpg', 1, '2018-04-07 02:22:06'),
(41, 288, 'assets/images/master_content/1523068280.jpg', 1, '2018-04-07 02:31:20'),
(42, 288, 'assets/images/master_content/15230682801.jpg', 1, '2018-04-07 02:31:20'),
(43, 288, 'assets/images/master_content/15230682802.jpg', 1, '2018-04-07 02:31:20'),
(44, 295, 'assets/images/master_content/15230688721.jpg', 1, '2018-04-07 02:41:12'),
(45, 295, 'assets/images/master_content/15230688722.jpg', 1, '2018-04-07 02:41:12'),
(46, 295, 'assets/images/master_content/1523068873.jpg', 1, '2018-04-07 02:41:13'),
(47, 296, 'assets/images/master_content/15230691401.jpg', 1, '2018-04-07 02:45:40'),
(48, 296, 'assets/images/master_content/15230691402.jpg', 1, '2018-04-07 02:45:40'),
(49, 296, 'assets/images/master_content/15230691403.jpg', 1, '2018-04-07 02:45:40'),
(50, 297, 'assets/images/master_content/15230693181.jpg', 1, '2018-04-07 02:48:38'),
(51, 297, 'assets/images/master_content/15230693182.jpg', 1, '2018-04-07 02:48:38'),
(52, 297, 'assets/images/master_content/15230693183.jpg', 1, '2018-04-07 02:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `site_news_optional`
--

CREATE TABLE `site_news_optional` (
  `news_optional_news_id` int(11) NOT NULL,
  `news_optional_label_1` varchar(255) DEFAULT NULL,
  `news_optional_label_2` varchar(255) DEFAULT NULL,
  `news_optional_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_news_relation`
--

CREATE TABLE `site_news_relation` (
  `news_category_id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_news_relation`
--

INSERT INTO `site_news_relation` (`news_category_id`, `news_id`) VALUES
(1, 268),
(2, 283),
(3, 265),
(3, 266),
(3, 280),
(3, 292),
(4, 264),
(5, 262),
(5, 263),
(6, 257),
(6, 258),
(6, 259),
(6, 260),
(6, 261),
(7, 251),
(7, 252),
(7, 253),
(7, 254),
(7, 255),
(7, 256),
(8, 247),
(8, 248),
(8, 249),
(8, 250),
(8, 291),
(9, 241),
(9, 242),
(9, 243),
(9, 244),
(9, 245),
(9, 246),
(9, 279),
(9, 284),
(9, 285),
(9, 289),
(9, 290),
(11, 286),
(11, 287),
(11, 288),
(11, 295),
(11, 296),
(11, 297),
(12, 282),
(13, 281),
(17, 293),
(17, 298),
(17, 299),
(17, 300),
(18, 294);

-- --------------------------------------------------------

--
-- Table structure for table `site_news_tab`
--

CREATE TABLE `site_news_tab` (
  `tab_id` int(10) UNSIGNED NOT NULL,
  `tab_news_id` int(10) UNSIGNED NOT NULL,
  `tab_title` varchar(255) NOT NULL,
  `tab_content` text NOT NULL,
  `tab_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_news_tab`
--

INSERT INTO `site_news_tab` (`tab_id`, `tab_news_id`, `tab_title`, `tab_content`, `tab_timestamp`) VALUES
(1, 251, 'PAKET REGULAR', 'Rp. 199.000,-\r\nDengan Membeli Lisenci Hak Usaha Keagenan anda langsung mendapatkan Bilyet/Voucher Umroh sebesar Rp. 199.000,- \r\nBilyet/ Voucher ini otomatis akan mengurangi/memotong harga paket Umroh , tidak ada sistem hangus, kapanpun tetap bisa digunakan dan juga bisa diwariskan atau dipindahtangankan.', '2018-01-12 03:20:33'),
(2, 251, 'AKAD KEAGENAN', 'Wakalah Taswiq Bil Ujroh\r\nAdalah sebuah akad dimana seseorang menjadi wakalah/wakil (agen) penjualan dengan mendapatkan ujroh (bonus) ketika dapat membantu penjualan Produk dari Perusahaan, dalam hal ini wakil penjualan adalah seluruh jama&rsquo;ah Citra Sagara Inten.\r\n\r\nFasilitas :\r\n\r\nLisensi Hak Usaha Keagenan (Personal Pranchise)\r\nBillyet /Voucher Umroh Senilai Rp. 199.000,-\r\nFasilitas Aplikasi Micro Payment PPOB (Payment Point Online Bank)\r\nVirtual Office (Management Agen di Internet)\r\nPembinaan dan Training', '2018-01-12 03:20:33'),
(3, 252, 'UJROH RIAYAH (Komisi Referensi)', 'Rp. 50.000,-\r\nRincian :\r\n\r\nRp. 40.000,- Cash\r\n\r\nRp. 10.000,- Deposit\r\n\r\nUjroh Riayah ini akan Anda dapatkan setiap mereferensikan Jama&rsquo;ah baru tanpa batas , dengan besaran Ujroh Riayah: Rp. 50.000,-', '2018-01-12 03:58:32'),
(4, 252, 'UJROH TANMIYAH (Komisi Perkembangan Agen)', 'Rp. 40.000,-\r\nRincian :\r\n\r\nRp. 30.000,- Cash\r\n\r\nRp. 10.000,- Deposit\r\n\r\nUjroh Tanmiyah ini akan Anda dapatkan dari pengembangan agen &amp; terjadi pasangan dibawah jaringan group anda sebesar Rp. 40.000,- per pasang dengan batas 25 Pasang perhari (potensi Komisi Rp. 1.000.000,- perhari).', '2018-01-12 03:58:32'),
(5, 252, 'KOMISI PENJUALAN LANGSUNG', 'Rp. 1.000.000,-\r\nKomisi Penjualan Langsung ini didapatkan ketika agen menjual paket Umroh baik Umroh Tunai, Umroh Promo, atau Umroh Plus Turkey sebesar Rp. 1.000.000,-', '2018-01-12 03:58:32'),
(6, 253, 'UJROH RIAYAH (Komisi Referensi)', 'Rp. 350.000,- (unlimited)\r\nUjroh Riayah ini akan Anda dapatkan setiap terjadi Upgrade dibawah group binaan anda dengan besaran Ujroh Riayah: Rp. 350.000,- (tanpa batas).', '2018-01-12 04:13:27'),
(7, 253, 'UJROH TANMIYAH (Komisi Perkembangan)', 'Rp. 100.000 (20 Pasang perhari / Potensi 2 Juta perhari)\r\nUjroh Tanmiah ini akan Anda dapatkan dari perkembangan agen dan terjadi pasangan dibawah jaringan group anda sebesar Rp. 100.000,- per pasang dengan batas 20 pasang perhari (potensi komisi Rp. 2.000.000,- perhari).', '2018-01-12 04:13:27'),
(8, 253, 'ROYALTY PERKEMBANGAN', 'Rp. 2.500,- Pertitik s/d 20 Level\r\nRoyalty pengembangan group agen sebesar Rp. 2.500,- pertitik yang berkembang di group binaannya sampai level 20.', '2018-01-12 04:13:28'),
(9, 253, 'ROYALTY PENJUALAN GROUP (Share Profit)', 'Rp. 50.000,- per titik s/d 20 Level\r\nRoyalty Penjualan Group (Share Profit) ini didapat ketika agen dibawah group binaan anda dapat menjual paket Umroh baik Umroh Tunai, Umroh Promo, atau Umroh Plus Turkey sebesar Rp. 50.000,- pertitik sampai kedalaman 20 level.', '2018-01-12 04:13:28'),
(10, 253, 'JAWA&rsquo;IZ / REWARD (Akumulasi)', 'Jawa&rsquo;iz (bonus prestasi) akan didapatkan dari pencapaian penjualan paket Umroh &amp; jumlah perkembangan group :', '2018-01-12 04:13:28'),
(11, 258, 'PAKET TUNAI DP Rp. 6.000.000,-', 'Dp Umroh Rp. 6.000.000,- adalah Booking Seat dengan Billyet Resmi dari Perusahaan.\r\nSetelah pelunasan Paket Umroh sesuai pilihan paket , jama&rsquo;ah langsung manasik &amp; berangkat.', '2018-01-12 07:49:55'),
(12, 258, 'PAKET TUNAI DP Rp. 11.000.000,-', 'Dp Umroh Rp.11.000.000,- adalah Booking Seat dengan Billyet Resmi dari Perusahaan dan Langsung Mendapatkan Souvenir Keberangkatan.\r\nSetelah pelunasan Paket Umroh sesuai pilihan paket , jama&rsquo;ah langsung manasik &amp; berangkat.', '2018-01-12 07:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `site_news_ticker`
--

CREATE TABLE `site_news_ticker` (
  `news_ticker_id` int(10) UNSIGNED NOT NULL,
  `news_ticker_title` varchar(200) NOT NULL,
  `news_ticker_link` varchar(100) DEFAULT NULL,
  `news_ticker_input_by` varchar(100) NOT NULL,
  `news_ticker_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_slider`
--

CREATE TABLE `site_slider` (
  `slider_id` int(10) UNSIGNED NOT NULL,
  `slider_slider_category_id` int(10) UNSIGNED NOT NULL,
  `slider_title` varchar(50) NOT NULL,
  `slider_short_description` text,
  `slider_link` varchar(100) DEFAULT NULL,
  `slider_youtube_id` varchar(100) DEFAULT NULL,
  `slider_image_src` varchar(100) DEFAULT NULL,
  `slider_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_slider`
--

INSERT INTO `site_slider` (`slider_id`, `slider_slider_category_id`, `slider_title`, `slider_short_description`, `slider_link`, `slider_youtube_id`, `slider_image_src`, `slider_timestamp`) VALUES
(6, 1, '3 Panggilan Allah', '', '', '', 'assets/images/slider/slider-3.jpg', '2016-06-20 15:02:42'),
(7, 1, 'Masjidil Haram & Masjid Nabawi', '', '', '', 'assets/images/slider/4.jpg', '2016-12-15 04:42:18'),
(9, 1, 'Umrah Menghapus Dosa', '', '', '', 'assets/images/slider/3.jpg', '2017-04-06 08:34:22'),
(10, 1, 'Haji dan Umrah', '', '', '', 'assets/images/slider/22.jpg', '2018-01-13 00:50:24'),
(11, 1, 'Solusi Mudah Manuju Makkah', '', '', '', 'assets/images/slider/12.jpg', '2018-01-13 00:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `site_slider_category`
--

CREATE TABLE `site_slider_category` (
  `slider_category_id` int(3) UNSIGNED NOT NULL,
  `slider_category_name` varchar(50) NOT NULL,
  `slider_category_width` int(5) UNSIGNED DEFAULT '1024',
  `slider_category_height` int(4) UNSIGNED DEFAULT '648',
  `slider_category_is_compress` enum('Y','N') NOT NULL COMMENT 'Y:compress image sesuai width dan height; N: tampilkan image asli',
  `slider_category_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_slider_category`
--

INSERT INTO `site_slider_category` (`slider_category_id`, `slider_category_name`, `slider_category_width`, `slider_category_height`, `slider_category_is_compress`, `slider_category_timestamp`) VALUES
(1, 'Slider Utama + Video', 1500, 1000, 'N', '2015-06-08 07:51:52');

-- --------------------------------------------------------

--
-- Table structure for table `site_social_media`
--

CREATE TABLE `site_social_media` (
  `social_media_id` int(10) UNSIGNED NOT NULL,
  `social_media_name` varchar(50) NOT NULL,
  `social_media_link` varchar(100) NOT NULL,
  `social_media_type` enum('twitter','facebook','gplus','vimeo','instagram','youtube','linkedin') DEFAULT NULL,
  `social_media_icon_src` varchar(100) NOT NULL,
  `social_media_is_published` enum('Y','N') NOT NULL DEFAULT 'Y',
  `social_media_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_social_media`
--

INSERT INTO `site_social_media` (`social_media_id`, `social_media_name`, `social_media_link`, `social_media_type`, `social_media_icon_src`, `social_media_is_published`, `social_media_timestamp`) VALUES
(1, 'KlinikSyifa.id', 'https://www.facebook.com/kliniksyifa.id', 'facebook', 'icon-facebook', 'Y', '2015-09-10 04:21:22'),
(2, '@kliniksyifa', 'https://www.instagram.com/kliniksyifa/', 'instagram', 'icon-instagram', 'Y', '2015-09-10 04:21:22');

-- --------------------------------------------------------

--
-- Table structure for table `site_testimonial`
--

CREATE TABLE `site_testimonial` (
  `testimonial_id` int(10) UNSIGNED NOT NULL,
  `testimonial_full_name` varchar(100) NOT NULL,
  `testimonial_title` varchar(30) DEFAULT NULL COMMENT 'Jabatan. misal: CEO Facebook, Founder Microsoft',
  `testimonial_avatar_src` varchar(100) DEFAULT NULL,
  `testimonial_value` text NOT NULL,
  `testimonial_is_published` enum('Y','N') NOT NULL,
  `testimonial_input_date` date NOT NULL,
  `testimonial_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_testimonial`
--

INSERT INTO `site_testimonial` (`testimonial_id`, `testimonial_full_name`, `testimonial_title`, `testimonial_avatar_src`, `testimonial_value`, `testimonial_is_published`, `testimonial_input_date`, `testimonial_timestamp`) VALUES
(1, 'Joko', 'asdas', 'assets/images/testimonial/avatar.jpg', '<p><span  #222222; font-family: Consolas, \";\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quibusdam ipsa neque cupiditate, quisquam beatae nam molestias quidem. Atque distinctio culpa eaque pariatur dolorum sunt sequi eum, tempore nemo fugit!</span></p>', 'Y', '2018-04-04', '2018-04-04 01:33:48'),
(2, 'Ani', 'asdas asdasd', 'assets/images/testimonial/avatar1.jpg', '<p><span  #222222; font-family: Consolas, \";\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quibusdam ipsa neque cupiditate, quisquam beatae nam molestias quidem. Atque distinctio culpa eaque pariatur dolorum sunt sequi eum, tempore nemo fugit!</span></p>', 'Y', '2018-04-04', '2018-04-04 01:34:23'),
(4, 'Ria', 'dhfbs hsdhf', 'assets/images/testimonial/avatar3.jpg', '<p><span  #222222; font-family: Consolas, \";\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quibusdam ipsa neque cupiditate, quisquam beatae nam molestias quidem. Atque distinctio culpa eaque pariatur dolorum sunt sequi eum, tempore nemo fugit!</span></p>', 'Y', '2018-04-04', '2018-04-04 01:34:58');

-- --------------------------------------------------------

--
-- Table structure for table `site_youtube`
--

CREATE TABLE `site_youtube` (
  `youtube_pk_id` int(10) UNSIGNED NOT NULL,
  `youtube_id` varchar(20) NOT NULL,
  `youtube_title` varchar(100) DEFAULT NULL,
  `youtube_permalink` varchar(150) NOT NULL,
  `youtube_description` text,
  `youtube_input_by` varchar(100) NOT NULL,
  `youtube_input_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_youtube`
--

INSERT INTO `site_youtube` (`youtube_pk_id`, `youtube_id`, `youtube_title`, `youtube_permalink`, `youtube_description`, `youtube_input_by`, `youtube_input_timestamp`) VALUES
(1, 'U2zHHo8XWOo', 'Bagaimana FCTC Melindungi Anak', 'bagaimana_fctc_melindungi_anak', '<p>Menegakkan peraturan tegas tidak menjual rokok kepada anak.</p>', 'admin', '2015-10-10 11:12:26'),
(2, 'htQ0AReaGnw', 'FCTC Untuk Indonesia', '', '<p>#FCTCuntukIndonesia adalah gerakan sosial mendukung pemerintah Indonesia menandatangani FCTC (framework convention on tobacco control) untuk melindungi generasi bangsa masa kini dan masa mendatang dari dampak konsumsi rokok dan paparan asap rokok.</p>', 'admin', '2015-06-16 14:16:40'),
(3, 'V8SfJbeXyXE', 'Apa Itu FCTC?', 'apa_itu_fctc', '<p>APA ITU FCTC ? FCTC atau Framework Convention on Tobacco Control merupakan perjanjian internasional tentang kesehatan masyarakat yang disepakati oleh Negara-negara anggota Organisasi Kesehatan Dunia (WHO). Bertujuan untuk melindungi generasi masa kini dan masa mendatang dari dampak konsumsi rokok dan paparan asap rokok.</p>', 'admin', '2015-10-10 11:13:52');

-- --------------------------------------------------------

--
-- Table structure for table `sys_form`
--

CREATE TABLE `sys_form` (
  `form_id` int(10) UNSIGNED NOT NULL,
  `form_jenis_laporan` varchar(100) NOT NULL,
  `form_waktu_kejadian` varchar(100) NOT NULL,
  `form_tanggal_kejadian` date NOT NULL,
  `form_kabupaten_kota` varchar(100) NOT NULL,
  `form_kecamatan` varchar(100) NOT NULL,
  `form_instansi_terkait` varchar(100) NOT NULL,
  `form_korban_meninggal` varchar(100) NOT NULL,
  `form_korban_hilang` varchar(100) NOT NULL,
  `form_korban_terluka` varchar(100) NOT NULL,
  `form_korban_menderita` varchar(100) NOT NULL,
  `form_korban_sakit` varchar(100) NOT NULL,
  `form_fasilitas_rusak` varchar(100) NOT NULL,
  `form_fasilitas_rusak_ringan` varchar(100) NOT NULL,
  `form_fasilitas_lainnya` varchar(100) NOT NULL,
  `form_fasilitas_kesehatan` varchar(100) NOT NULL,
  `form_kondisi` varchar(100) NOT NULL,
  `form_area` varchar(100) NOT NULL,
  `form_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_member`
--

CREATE TABLE `sys_member` (
  `member_person_id` int(10) UNSIGNED NOT NULL,
  `member_email` varchar(100) NOT NULL,
  `member_password` varchar(150) DEFAULT NULL,
  `member_has_joined` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'N:belum gabung; Y: telah gabung',
  `member_last_login_datetime` datetime DEFAULT NULL,
  `member_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_member`
--

INSERT INTO `sys_member` (`member_person_id`, `member_email`, `member_password`, `member_has_joined`, `member_last_login_datetime`, `member_timestamp`) VALUES
(4, 'tejo.murti@inolabs.net', '827ccb0eea8a706c4c34a16891f84e7b', 'Y', '2015-07-02 11:40:00', '2015-06-15 15:15:15'),
(5, 'dian.mitra@inolabs.net', '827ccb0eea8a706c4c34a16891f84e7b', 'Y', NULL, '2015-06-16 04:15:41'),
(6, 'dian.mitra2@inolabs.net', '827ccb0eea8a706c4c34a16891f84e7b', 'Y', NULL, '2015-06-16 04:18:32'),
(7, 'dian.mitra3@inolabs.net', '827ccb0eea8a706c4c34a16891f84e7b', 'Y', NULL, '2015-06-16 04:23:33'),
(9, 'tejo.murti4@inolabs.net', 'd41d8cd98f00b204e9800998ecf8427e', 'N', NULL, '2015-06-19 04:38:14'),
(10, 'tejo.murti4@inolabs.net', NULL, 'N', NULL, '2015-06-19 04:39:04'),
(11, 'tejo.murti4@inolabs.net', NULL, 'N', NULL, '2015-06-19 04:39:28'),
(12, 'tejo.murti11@inolabs.net', NULL, 'N', NULL, '2015-06-19 06:32:26'),
(13, 'tejo.murti@inolabs.net', NULL, 'N', NULL, '2015-06-19 06:36:17'),
(14, 'tejo.murti@inolabs.net', NULL, 'N', NULL, '2015-06-21 14:40:10'),
(15, 'tejo.murti@inolabs.net', NULL, 'N', NULL, '2015-06-22 03:35:09'),
(21, 'tejo.murti@inolabs.net', NULL, 'N', NULL, '2015-06-22 15:09:34'),
(24, 'tejo.murti@inolabs.net', NULL, 'N', NULL, '2015-06-24 23:07:56'),
(26, 'wibowo@insanmedika.com', NULL, 'N', NULL, '2015-07-02 07:37:37'),
(28, 'tejo.murti22@inolabs.net', 'f0008885eb985d0f12f557af025970c2', 'N', NULL, '2015-10-17 17:07:16'),
(29, 'zola@mail.com', '6401d05c4f97d18d56818c9e569c4822', 'N', NULL, '2015-10-17 17:10:31'),
(30, 'info@toyotacawang.com', '4b581c8506da363bc9649b218a1d0a85', 'N', NULL, '2015-10-18 16:51:43'),
(31, 'tejomurti@mail.com', '4d1e2f703138e60262771334effbe955', 'N', NULL, '2015-10-27 07:42:25'),
(32, 'tejo.murt3i@inolabs.net', '87a1725519dd557462e564bbd5d6f252', 'N', NULL, '2015-10-27 07:43:11');

-- --------------------------------------------------------

--
-- Table structure for table `sys_order`
--

CREATE TABLE `sys_order` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_full_name` varchar(100) NOT NULL,
  `order_email` varchar(100) NOT NULL,
  `order_birth_date` date NOT NULL,
  `order_appointment_date` date NOT NULL,
  `order_request_time` varchar(20) NOT NULL,
  `order_service_name` varchar(100) NOT NULL,
  `order_phone_number` varchar(20) NOT NULL,
  `order_view_by` varchar(50) DEFAULT NULL,
  `order_view_datetime` datetime DEFAULT NULL,
  `order_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_status` enum('read','unread') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_order`
--

INSERT INTO `sys_order` (`order_id`, `order_full_name`, `order_email`, `order_birth_date`, `order_appointment_date`, `order_request_time`, `order_service_name`, `order_phone_number`, `order_view_by`, `order_view_datetime`, `order_timestamp`, `order_status`) VALUES
(6, 'Armanda Agnieyudha', 'ari.armanda@inolabs.net', '1994-11-12', '0000-00-00', '13.00 - 15.30', 'POLI GIGI', '085735662006', 'admin', '2016-11-22 11:08:36', '2016-11-22 03:35:26', 'read'),
(7, 'Armanda Agnieyudha', 'ari.armanda@inolabs.net', '1994-11-12', '0000-00-00', '15.30 - 18.30', 'POLI GIGI', '085735662006', NULL, NULL, '2016-11-22 04:02:15', 'unread'),
(8, 'sdfsd', 'fsdf@gmail.com', '2016-11-08', '0000-00-00', '13.00 - 15.30', 'POLI NEURO (SYARAF)', '325432', NULL, NULL, '2016-11-24 15:24:15', 'unread'),
(9, 'dian', 'dian.mitra@inolabs.net', '1990-03-07', '0000-00-00', '13.00 - 15.30', 'POLI THT', '12432342', NULL, NULL, '2016-11-24 16:30:15', 'unread'),
(10, 'Ghadira Al Thafana', 'dian.mitra@inolabs.net', '2016-09-23', '2016-11-26', '13.00 - 15.30', 'POLI GIGI', '085267608865', 'admin', '2016-11-25 21:53:26', '2016-11-25 14:53:07', 'read'),
(11, 'Win', 'zaharaulia@gmail.com', '1992-01-15', '2016-12-16', '15.30 - 18.30', 'POLI GIGI', '085261141870', 'admin', '2016-12-15 11:07:54', '2016-12-15 04:07:22', 'read'),
(12, 'sari', 'zaharaulia@gmail.com', '1994-01-01', '2016-12-19', '13.00 - 15.30', 'POLI CARDIO (OTOT)', '085261141870', NULL, NULL, '2016-12-15 04:11:44', 'unread');

-- --------------------------------------------------------

--
-- Table structure for table `sys_polling_answer`
--

CREATE TABLE `sys_polling_answer` (
  `polling_answer_id` int(10) UNSIGNED NOT NULL,
  `polling_answer_polling_question_id` int(11) NOT NULL,
  `polling_answer_name` varchar(255) NOT NULL,
  `polling_answer_order_number` int(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_polling_category`
--

CREATE TABLE `sys_polling_category` (
  `polling_category_id` int(10) UNSIGNED NOT NULL,
  `polling_category_name` varchar(50) NOT NULL,
  `polling_category_input_by` varchar(100) NOT NULL,
  `polling_category_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_polling_question`
--

CREATE TABLE `sys_polling_question` (
  `polling_question_id` int(10) UNSIGNED NOT NULL,
  `polling_question_polling_category_id` int(10) UNSIGNED NOT NULL,
  `polling_question_name` varchar(255) NOT NULL,
  `polling_question_order_number` int(3) NOT NULL DEFAULT '1',
  `polling_question_is_mandatory` enum('Y','N') NOT NULL DEFAULT 'N',
  `polling_question_input_by` varchar(100) NOT NULL,
  `polling_question_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_polling_statistik_detail`
--

CREATE TABLE `sys_polling_statistik_detail` (
  `spsd_spsh_id` int(10) UNSIGNED NOT NULL,
  `spsd_polling_question_id` int(10) UNSIGNED NOT NULL,
  `spsd_polling_answer_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_polling_statistik_header`
--

CREATE TABLE `sys_polling_statistik_header` (
  `spsh_id` int(11) NOT NULL,
  `spsh_polling_category_id` int(10) UNSIGNED NOT NULL,
  `spsh_person_id` int(10) UNSIGNED NOT NULL,
  `spsh_note` varchar(255) DEFAULT NULL,
  `spsh_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_product`
--

CREATE TABLE `sys_product` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_permalink` varchar(200) NOT NULL,
  `product_overview` text NOT NULL,
  `product_content` text NOT NULL,
  `product_current_price` decimal(15,2) NOT NULL,
  `product_old_price` decimal(15,2) NOT NULL,
  `product_discount_rupiah` decimal(15,2) NOT NULL DEFAULT '0.00',
  `product_discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `product_meta_tags` varchar(300) NOT NULL,
  `product_meta_desc` tinytext NOT NULL,
  `product_image` varchar(200) NOT NULL,
  `product_is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `product_last_update_datetime` datetime NOT NULL,
  `product_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_product`
--

INSERT INTO `sys_product` (`product_id`, `product_name`, `product_permalink`, `product_overview`, `product_content`, `product_current_price`, `product_old_price`, `product_discount_rupiah`, `product_discount_percent`, `product_meta_tags`, `product_meta_desc`, `product_image`, `product_is_active`, `product_last_update_datetime`, `product_timestamp`) VALUES
(7, 'PAKET PERAWAT MEDIS 1 BULAN', 'paket_perawat_medis_1_bulan', 'Mendapatkan 1 orang perawat, bebas biaya administrasi & transportasi, garansi 2 kali penukaran.\nPromo : Diskon 5% bagi pemegang kartu kredit BCA (berlaku hingga 31 Desember 2015)', '&lt;ol&gt;\n&lt;li&gt;Menggunakan alat medis seperti sunction, nebulizer, ventilator, oxygen, NGT, Kateter dll.&lt;/li&gt;\n&lt;li&gt;Dalam perawatan intensif (total care) dengan kondisi bedrest&lt;/li&gt;\n&lt;li&gt;Sudah memiliki alat medis sendiri tanpa didatangkan dari INSAN MEDIKA&lt;/li&gt;\n&lt;/ol&gt;\n&lt;p&gt;&amp;nbsp;Mendapatkan 1 orang perawat, bebas biaya administrasi &amp;amp; transportasi, garansi 2 kali penukaran.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;Promo : Diskon 5% bagi pemegang kartu kredit BCA (berlaku hingga 31 Desember 2015)&lt;/p&gt;', '7200000.00', '8000000.00', '0.00', '10.00', 'Mendapatkan 1 orang perawat, bebas biaya administrasi & transportasi, garansi 2 kali penukaran.\nPromo : Diskon 5% bagi pemegang kartu kredit BCA (berlaku hingga 31 Desember 2015)', 'Mendapatkan 1 orang perawat, bebas biaya administrasi & transportasi, garansi 2 kali penukaran.\nPromo : Diskon 5% bagi pemegang kartu kredit BCA (berlaku hingga 31 Desember 2015)', 'assets/images/product/Happy_Nurses.jpg.opt425x282o0,0s425x282_.jpg', 'Y', '2015-06-22 21:39:46', '2015-06-09 04:09:37'),
(9, 'PAKET PERAWAT MEDIS 6 BULAN', 'paket_perawat_medis_6_bulan', '', '&lt;ol&gt;\n&lt;li&gt;Menggunakan alat medis seperti sunction, nebulizer, ventilator, oxygen, NGT, Kateter dll.&lt;/li&gt;\n&lt;li&gt;Dalam perawatan intensif (total care) dengan kondisi bedrest&lt;/li&gt;\n&lt;li&gt;Sudah memiliki alat medis sendiri tanpa didatangkan dari INSAN MEDIKA&lt;/li&gt;\n&lt;/ol&gt;\n&lt;p&gt;Mendapatkan 1 orang perawat, bebas biaya administrasi &amp;amp; transportasi, garansi 3 kali penukaran, mendapat kartu privilage Gold.&lt;/p&gt;\n&lt;p&gt;Privilage card Gold berlaku hingga 1 tahun.&lt;/p&gt;\n&lt;p&gt;Promo : Diskon 10% bagi pemegang kartu kredit BCA cicilan 0% 6 bulan untuk &lt;em&gt;&amp;nbsp;nursing package &lt;/em&gt;(berlaku hingga 31 Desember 2015)&lt;/p&gt;', '35000000.00', '35000000.00', '0.00', '0.00', '', '', 'assets/images/product/photodune-4550767-doctor-or-nurse-talking-to-senior-woman-with-touch-pad-computer-m-11.jpg', 'Y', '2015-06-09 11:12:55', '2015-06-09 04:12:25'),
(10, 'PAKET PERAWAT  MEDIS 1 TAHUN', 'paket_perawat__medis_1_tahun', '', '&lt;p&gt;Mendapatkan 1 orang perawat, bebas biaya administrasi &amp;amp; transportasi, garansi 5 kali penukaran, Kunjungan dokter 2 minggu sekali, mendapat kartu privilage Platinum.&lt;/p&gt;\n&lt;p&gt;Privilege card Platinum berlaku 2 tahun.&lt;/p&gt;\n&lt;p&gt;Promo : Diskon 10% bagi pemegang kartu kredit BCA cicilan 0% 12 bulan untuk &lt;em&gt;nursing package&lt;/em&gt; (berlaku hingga 31 Desember 2015).&lt;/p&gt;\n&lt;ol&gt;\n&lt;li&gt;Menggunakan alat medis seperti sunction, nebulizer, ventilator, oxygen, NGT, Kateter dll.&lt;/li&gt;\n&lt;li&gt;Dalam perawatan intensif (total care) dengan kondisi bedrest&lt;/li&gt;\n&lt;li&gt;Sudah memiliki alat medis sendiri tanpa didatangkan dari INSAN MEDIKA&lt;/li&gt;\n&lt;/ol&gt;', '70000000.00', '70000000.00', '0.00', '0.00', '', '', 'assets/images/product/bigstock-Portrait-of-happy-extended-fam-59094062-900x390.jpg', 'Y', '2015-06-22 14:37:54', '2015-06-09 04:14:49'),
(11, 'PERAWAT MEDIS BULANAN NON PAKET', 'perawat_medis_bulanan_non_paket', '', '&lt;ol&gt;\n&lt;li&gt;Menggunakan alat medis seperti sunction, nebulizer, ventilator, oxygen, NGT, Kateter dll.&lt;/li&gt;\n&lt;li&gt;Dalam perawatan intensif (total care) dengan kondisi bedrest&lt;/li&gt;\n&lt;li&gt;Sudah memiliki alat medis sendiri tanpa didatangkan dari INSAN MEDIKA&lt;/li&gt;\n&lt;/ol&gt;\n&lt;p&gt;Membayar biaya administrasi 2,5 juta (berlaku tiap 6 bulan) sudah termasuk transport, garansi 2 x penukaran dalam 6 bulan, mendapat kartu privilage Silver).&lt;/p&gt;\n&lt;p&gt;Privilage card Silver berlaku hingga 6 bulan (perpanjang 6 bulan jika membayar administrasi 2,5 juta).&lt;/p&gt;\n&lt;p&gt;Promo : Tidak ada promo.&lt;/p&gt;', '6000000.00', '6000000.00', '0.00', '0.00', '', '', 'assets/images/product/elderly_carer_1242887a.jpg', 'Y', '2015-06-09 11:18:27', '2015-06-09 04:17:50');

-- --------------------------------------------------------

--
-- Table structure for table `sys_product_more_images`
--

CREATE TABLE `sys_product_more_images` (
  `product_more_images_id` int(10) UNSIGNED NOT NULL,
  `product_more_images_product_id` int(10) UNSIGNED NOT NULL,
  `product_more_images_file` varchar(200) NOT NULL,
  `product_more_images_is_active` int(1) NOT NULL,
  `product_more_images_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_product_tags`
--

CREATE TABLE `sys_product_tags` (
  `product_tags_id` int(10) UNSIGNED NOT NULL,
  `product_tags_name` varchar(200) NOT NULL,
  `product_tags_permalink` varchar(200) NOT NULL,
  `product_tags_create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_product_tags_detail`
--

CREATE TABLE `sys_product_tags_detail` (
  `tags_detail_id` int(11) NOT NULL,
  `tags_detail_tags_id` int(10) UNSIGNED NOT NULL,
  `tags_detail_product_id` int(10) UNSIGNED NOT NULL,
  `tags_detail_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_request_time`
--

CREATE TABLE `sys_request_time` (
  `request_time_id` int(10) UNSIGNED NOT NULL,
  `request_time_label` varchar(20) NOT NULL,
  `request_time_is_show` enum('Y','N') NOT NULL DEFAULT 'Y',
  `request_time_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_request_time`
--

INSERT INTO `sys_request_time` (`request_time_id`, `request_time_label`, `request_time_is_show`, `request_time_timestamp`) VALUES
(6, '09.00 - 12.00', 'Y', '2016-12-20 04:25:49'),
(7, '12.00 - 15.00', 'Y', '2016-12-20 04:26:00'),
(8, '15.00 - 18.00', 'Y', '2016-12-20 04:26:07'),
(9, '18.00 - 21.00', 'Y', '2016-12-20 04:26:13');

-- --------------------------------------------------------

--
-- Table structure for table `sys_schedule`
--

CREATE TABLE `sys_schedule` (
  `schedule_id` int(10) UNSIGNED NOT NULL,
  `schedule_service_id` int(10) UNSIGNED NOT NULL,
  `schedule_service_name` varchar(50) NOT NULL,
  `schedule_queue_value` varchar(15) NOT NULL,
  `schedule_dokter_name` varchar(100) NOT NULL,
  `schedule_datetime` datetime NOT NULL,
  `schedule_sync_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel jadwal antrian';

--
-- Dumping data for table `sys_schedule`
--

INSERT INTO `sys_schedule` (`schedule_id`, `schedule_service_id`, `schedule_service_name`, `schedule_queue_value`, `schedule_dokter_name`, `schedule_datetime`, `schedule_sync_datetime`) VALUES
(44, 24, 'THT', 'T 001', 'Ashri Yudhistira', '2016-12-15 11:52:35', '2016-12-15 13:20:04'),
(45, 11, 'Estetika', 'K 001', 'Putri Wulandari', '2016-12-15 12:00:49', '2016-12-15 13:20:05'),
(46, 14, 'Penyakit Dalam', 'D 002', 'Dian Anindita Lubis', '2016-12-15 12:48:18', '2016-12-15 13:20:06'),
(47, 27, 'Cardio', 'C 008', 'Sutomo Kasiman', '2016-12-15 13:40:13', '2016-12-15 13:41:51'),
(48, 26, 'Gigi', 'G 003', 'Aditya Rachmawati', '2016-12-15 13:42:04', '2016-12-15 13:41:51'),
(49, 14, 'Penyakit Dalam', 'D 003', 'Dian Anindita Lubis', '2016-12-15 13:43:14', '2016-12-15 13:44:30'),
(50, 14, 'Penyakit Dalam', 'D 004', 'Dian Anindita Lubis', '2016-12-15 13:46:33', '2016-12-15 13:47:31'),
(51, 24, 'THT', 'T 002', 'Ashri Yudhistira', '2016-12-15 13:55:26', '2016-12-15 13:59:40'),
(52, 27, 'Cardio', 'C 009', 'Sutomo Kasiman', '2016-12-15 14:01:41', '2016-12-15 14:01:28'),
(53, 24, 'THT', 'T 003', 'Ashri Yudhistira', '2016-12-15 14:01:48', '2016-12-15 14:01:34'),
(54, 25, 'Neurologi', 'N 003', 'Hasan Sjahrir', '2016-12-15 14:01:57', '2016-12-15 14:01:42'),
(55, 27, 'Cardio', 'C 005', 'dr. Yuke Sarastri, Sp.JP', '2016-12-15 14:02:03', '2016-12-15 14:01:49'),
(56, 27, 'Cardio', 'C 011', 'Sutomo Kasiman', '2016-12-15 14:05:06', '2016-12-15 14:04:52'),
(57, 24, 'THT', 'T 005', 'Ashri Yudhistira', '2016-12-15 14:05:15', '2016-12-15 14:05:01'),
(58, 24, 'THT', 'T 004', 'Ashri Yudhistira', '2016-12-15 14:12:55', '2016-12-15 14:12:41'),
(59, 27, 'Cardio', 'C 010', 'Sutomo Kasiman', '2016-12-15 14:13:12', '2016-12-15 14:12:58'),
(60, 24, 'THT', 'T 006', 'Ashri Yudhistira', '2016-12-15 14:13:27', '2016-12-15 14:13:13'),
(61, 27, 'Cardio', 'C 012', 'Sutomo Kasiman', '2016-12-15 14:13:58', '2016-12-15 14:13:44'),
(62, 24, 'THT', 'T 001', 'Ashri Yudhistira', '2016-12-15 11:52:35', '2016-12-15 15:40:20'),
(63, 11, 'Estetika', 'K 001', 'Putri Wulandari', '2016-12-15 12:00:49', '2016-12-15 15:40:21'),
(64, 14, 'Penyakit Dalam', 'D 002', 'Dian Anindita Lubis', '2016-12-15 12:48:18', '2016-12-15 15:40:21'),
(65, 25, 'Neurologi', 'N 004', 'dr. R.A Dwi Pujiastuti, Sp.S', '2016-12-15 14:15:32', '2016-12-15 15:40:22'),
(66, 27, 'Cardio', 'C 005', 'dr. Yuke Sarastri, Sp.JP', '2016-12-15 15:52:29', '2016-12-15 15:52:30'),
(67, 27, 'Cardio', 'C 008', 'Prof. Dr. Sutomo Kasiman, Sp.JP(K), FIHA, FAsCC', '2016-12-15 15:52:43', '2016-12-15 15:52:43'),
(68, 25, 'Neurologi', 'N 003', 'Hasan Sjahrir', '2016-12-15 16:11:41', '2016-12-15 16:11:42'),
(69, 27, 'Cardio', 'C 009', 'dr. Yuke Sarastri, Sp.JP', '2016-12-15 16:31:16', '2016-12-15 16:31:17'),
(70, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira, Sp.THT-KL', '2016-12-15 16:55:31', '2016-12-15 16:55:31'),
(71, 24, 'THT', 'T 003', 'dr. Ashri Yudhistira, Sp.THT-KL', '2016-12-15 19:40:49', '2016-12-15 19:40:50'),
(72, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati, Sp.Ort', '2016-12-15 20:09:31', '2016-12-15 20:09:32'),
(73, 14, 'Penyakit Dalam', 'D 003', 'dr. Dian Anindita Lubis, Sp.PD', '2016-12-15 23:27:41', '2016-12-15 23:27:42'),
(74, 11, 'Estetika', 'K 002', 'Putri Wulandari', '2016-12-15 23:13:02', '2016-12-15 23:32:19'),
(75, 26, 'Gigi', 'G 004', 'Aditya Rachmawati', '2016-12-15 23:23:31', '2016-12-15 23:32:20'),
(76, 14, 'Penyakit Dalam', 'D 005', 'Dian Anindita Lubis', '2016-12-15 23:29:26', '2016-12-15 23:32:21'),
(77, 14, 'Penyakit Dalam', 'D 006', 'Dian Anindita Lubis', '2016-12-15 23:32:19', '2016-12-15 23:32:22'),
(78, 25, 'Neurologi', 'N 004', 'R.A Dwi Pujiastuti', '2016-12-15 23:35:22', '2016-12-15 23:35:07'),
(79, 24, 'THT', 'T 007', 'Ashri Yudhistira', '2016-12-15 23:38:17', '2016-12-15 23:38:03'),
(80, 27, 'Cardio', 'C 013', 'Sutomo Kasiman', '2016-12-15 23:38:49', '2016-12-15 23:38:36'),
(81, 12, 'Obgyn', 'O 002', 'dr. Yudha Sudewo, Sp.OG', '2016-12-16 16:14:22', '2016-12-16 16:15:26'),
(82, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari', '2016-12-16 16:08:36', '2016-12-16 16:15:26'),
(83, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo, Sp.OG', '2016-12-16 16:07:36', '2016-12-16 16:15:27'),
(84, 26, 'Gigi', 'G 001', 'drg. Brian', '2016-12-16 16:06:29', '2016-12-16 16:15:28'),
(85, 27, 'Cardio', 'C 003', 'dr. Yuke Sarastri, Sp.JP', '2016-12-16 16:05:04', '2016-12-16 16:15:28'),
(86, 12, 'Obgyn', 'O 003', 'dr. Yudha Sudewo, Sp.OG', '2016-12-16 16:15:28', '2016-12-16 16:15:29'),
(87, 27, 'Cardio', 'C 002', 'Prof. Dr. Sutomo Kasiman, Sp.JP(K), FIHA, FAsCC', '2016-12-16 16:06:38', '2016-12-16 16:15:29'),
(88, 27, 'Cardio', 'C 003', 'dr. Yuke Sarastri, Sp.JP', '2016-12-16 16:05:04', '2016-12-16 16:15:30'),
(89, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri, Sp.JP', '2016-12-16 14:12:54', '2016-12-16 16:15:30'),
(90, 27, 'Cardio', 'C 002', 'Prof. Dr. Sutomo Kasiman, Sp.JP(K), FIHA, FAsCC', '2016-12-16 16:06:38', '2016-12-16 16:15:30'),
(91, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti, Sp.S', '2016-12-16 10:00:04', '2016-12-16 16:15:31'),
(92, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri, Sp.JP', '2016-12-16 14:12:54', '2016-12-16 16:15:31'),
(93, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2016-12-16 10:15:51', '2016-12-16 16:15:31'),
(94, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti, Sp.S', '2016-12-16 10:00:04', '2016-12-16 16:15:35'),
(95, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2016-12-16 10:41:53', '2016-12-16 16:15:35'),
(96, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2016-12-16 10:15:51', '2016-12-16 16:15:35'),
(97, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2016-12-16 10:41:53', '2016-12-16 16:15:36'),
(98, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2016-12-16 10:41:53', '2016-12-16 16:15:36'),
(99, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2016-12-16 10:46:50', '2016-12-16 16:15:36'),
(100, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2016-12-16 10:46:50', '2016-12-16 16:15:37'),
(101, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2016-12-16 10:59:27', '2016-12-16 16:15:37'),
(102, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2016-12-16 10:59:27', '2016-12-16 16:15:37'),
(103, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2016-12-16 12:58:31', '2016-12-16 16:15:38'),
(104, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2016-12-16 12:58:31', '2016-12-16 16:15:38'),
(105, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2016-12-16 12:30:28', '2016-12-16 16:15:38'),
(106, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2016-12-16 12:40:51', '2016-12-16 16:15:39'),
(107, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2016-12-16 14:54:26', '2016-12-16 16:15:40'),
(108, 14, 'Penyakit Dalam', 'D 001', 'dr. Dian Anindita Lubis, Sp.PD', '2016-12-16 16:34:14', '2016-12-16 16:34:15'),
(109, 26, 'Gigi', 'G 002', 'drg. Brian', '2016-12-16 16:35:17', '2016-12-16 16:35:18'),
(110, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari', '2016-12-16 16:37:03', '2016-12-16 16:37:19'),
(111, 27, 'Cardio', 'C 001', 'Prof. Dr. Sutomo Kasiman, Sp.JP(K), FIHA, FAsCC', '2016-12-16 17:49:18', '2016-12-16 17:49:19'),
(112, 24, 'THT', 'T 001', 'Ashri Yudhistira', '2016-12-16 20:40:58', '2016-12-16 20:40:59'),
(113, 13, 'Anak', 'A 001', 'Inke Nadia Lubis', '2016-12-17 00:24:23', '2016-12-17 00:24:24'),
(114, 11, 'Estetika', 'K 001', 'Putri Wulandari', '2016-12-19 10:09:07', '2016-12-19 10:09:09'),
(115, 26, 'Gigi', 'G 001', 'Aditya Rachmawati', '2016-12-19 10:34:13', '2016-12-19 10:34:15'),
(116, 11, 'Estetika', 'K 001', 'Putri Wulandari', '2016-12-19 11:46:49', '2016-12-19 11:46:51'),
(117, 27, 'Cardio', 'C 001', 'Yuke Sarastri', '2016-12-19 14:19:41', '2016-12-19 14:19:43'),
(118, 11, 'Estetika', 'K 002', 'Putri Wulandari', '2016-12-19 14:57:27', '2016-12-19 14:57:29'),
(119, 11, 'Estetika', 'K 003', 'Putri Wulandari', '2016-12-19 15:01:37', '2016-12-19 15:01:40'),
(120, 27, 'Cardio', 'C 002', 'Yuke Sarastri', '2016-12-19 15:17:03', '2016-12-19 15:17:06'),
(121, 11, 'Estetika', 'K 004', 'Putri Wulandari', '2016-12-19 17:05:50', '2016-12-19 17:05:52'),
(122, 26, 'Gigi', 'G 001', 'Aditya Rachmawati', '2016-12-19 17:49:40', '2016-12-19 17:49:43'),
(123, 11, 'Estetika', 'K 005', 'Putri Wulandari', '2016-12-19 18:46:43', '2016-12-19 18:46:45'),
(124, 26, 'Gigi', 'G 002', 'Aditya Rachmawati', '2016-12-19 20:03:30', '2016-12-19 20:03:32'),
(125, 11, 'Estetika', 'K 001', 'Putri Wulandari', '2016-12-20 12:06:22', '2016-12-20 12:06:25'),
(126, 11, 'Estetika', 'K 002', 'Putri Wulandari', '2016-12-20 14:51:23', '2016-12-20 14:51:25'),
(128, 11, 'Estetika', 'K 003', 'Putri Wulandari', '2016-12-20 14:55:30', '2016-12-20 14:55:32'),
(132, 14, 'Penyakit Dalam', 'D 001', 'dr. dr. Dian Anindita Lubis Sp.PD Sp.PD', '2016-12-20 15:04:39', '2016-12-20 15:04:37'),
(133, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2016-12-20 15:51:29', '2016-12-20 15:51:32'),
(134, 26, 'Gigi', 'G 001', 'drg. drg. Brian Merchantara Winato', '2016-12-20 17:30:05', '2016-12-20 17:30:07'),
(135, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2016-12-20 18:22:21', '2016-12-20 18:22:23'),
(136, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2016-12-21 13:40:49', '2016-12-21 13:40:53'),
(137, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2016-12-21 14:24:34', '2016-12-21 14:24:37'),
(138, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2016-12-21 14:26:26', '2016-12-21 14:26:29'),
(139, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2016-12-21 15:28:14', '2016-12-21 15:28:17'),
(140, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2016-12-21 15:39:26', '2016-12-21 15:39:30'),
(141, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Sp.THT-KL', '2016-12-21 18:47:18', '2016-12-21 18:47:21'),
(142, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Sp.JP', '2016-12-22 16:20:32', '2016-12-22 16:20:32'),
(143, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2016-12-22 17:46:54', '2016-12-22 17:46:54'),
(144, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2016-12-23 15:15:38', '2016-12-23 15:15:39'),
(145, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2016-12-23 15:44:48', '2016-12-23 15:44:48'),
(146, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2016-12-23 16:17:37', '2016-12-23 16:17:37'),
(147, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2016-12-23 16:57:57', '2016-12-23 16:57:58'),
(148, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2016-12-23 17:12:45', '2016-12-23 17:12:45'),
(149, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2016-12-26 11:34:59', '2016-12-26 11:34:59'),
(150, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2016-12-26 12:53:49', '2016-12-26 12:53:49'),
(151, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2016-12-26 13:07:04', '2016-12-26 13:07:05'),
(152, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2016-12-26 13:21:22', '2016-12-26 13:21:22'),
(153, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2016-12-26 13:54:38', '2016-12-26 13:54:38'),
(154, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari', '2016-12-26 14:03:20', '2016-12-26 14:03:20'),
(155, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2016-12-28 16:31:18', '2016-12-28 16:31:20'),
(156, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2016-12-29 13:42:19', '2016-12-29 13:42:21'),
(157, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-01-03 13:18:11', '2017-01-03 13:18:12'),
(158, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-06 11:10:04', '2017-01-06 11:10:04'),
(159, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-06 21:18:48', '2017-01-06 21:18:48'),
(160, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-07 13:18:37', '2017-01-07 13:18:38'),
(161, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-01-09 11:00:07', '2017-01-09 11:00:07'),
(162, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-09 11:07:36', '2017-01-09 11:07:36'),
(163, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-09 11:39:05', '2017-01-09 11:39:06'),
(164, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-09 18:15:24', '2017-01-09 18:15:24'),
(165, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-01-09 21:39:22', '2017-01-09 21:39:22'),
(167, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-10 17:07:41', '2017-01-10 17:07:42'),
(168, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-10 17:40:57', '2017-01-10 17:40:57'),
(169, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-10 17:49:14', '2017-01-10 17:49:14'),
(170, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-11 15:05:31', '2017-01-11 15:05:32'),
(171, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-01-11 16:59:38', '2017-01-11 16:59:39'),
(172, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-11 17:00:40', '2017-01-11 17:00:40'),
(173, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-11 17:19:35', '2017-01-11 17:19:36'),
(174, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-12 13:24:56', '2017-01-12 13:24:57'),
(175, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-01-12 13:37:33', '2017-01-12 13:37:34'),
(176, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-01-12 16:26:26', '2017-01-12 16:26:27'),
(177, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-01-12 16:27:10', '2017-01-12 16:27:11'),
(178, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari', '2017-01-12 16:43:22', '2017-01-12 16:43:22'),
(179, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-01-12 18:23:37', '2017-01-12 18:23:38'),
(180, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-13 12:18:47', '2017-01-13 12:18:48'),
(181, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-13 15:43:32', '2017-01-13 15:43:33'),
(182, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-01-13 16:59:21', '2017-01-13 16:59:22'),
(183, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-01-13 17:00:25', '2017-01-13 17:00:27'),
(184, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-14 10:31:17', '2017-01-14 10:31:19'),
(185, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Sp.JP', '2017-01-16 14:33:28', '2017-01-16 14:33:30'),
(186, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-01-16 17:31:23', '2017-01-16 17:31:24'),
(187, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-16 20:27:52', '2017-01-16 20:27:54'),
(188, 27, 'Cardio', 'C 002', 'dr. Yuke Sarastri Sp.JP', '2017-01-16 20:50:44', '2017-01-16 20:50:46'),
(189, 26, 'Gigi', 'G 001', 'drg Aditya Rachmawati Sp.Ort', '2007-04-26 13:31:59', '2017-01-17 10:06:21'),
(190, 25, 'Neurologi', 'N 001', 'dr R.A Dwi Pujiastuti Sp.S', '2007-04-26 13:32:49', '2017-01-17 10:07:08'),
(191, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-17 10:12:54', '2017-01-17 10:12:56'),
(192, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-01-17 13:30:57', '2017-01-17 13:30:58'),
(193, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-17 13:40:28', '2017-01-17 13:40:30'),
(194, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-01-17 13:51:39', '2017-01-17 13:51:41'),
(195, 25, 'Neurologi', 'N 002', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-01-17 14:09:29', '2017-01-17 14:09:30'),
(196, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-01-17 14:18:18', '2017-01-17 14:18:20'),
(197, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-01-17 15:13:15', '2017-01-17 15:13:16'),
(198, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-01-17 15:33:17', '2017-01-17 15:33:19'),
(199, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari', '2017-01-17 15:50:49', '2017-01-17 15:50:51'),
(200, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-01-17 16:43:24', '2017-01-17 16:43:26'),
(201, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari', '2017-01-17 16:50:29', '2017-01-17 16:50:31'),
(202, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-17 19:22:11', '2017-01-17 19:22:13'),
(203, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-17 19:23:09', '2017-01-17 19:23:10'),
(204, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-18 15:24:32', '2017-01-18 15:24:34'),
(205, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-18 15:48:40', '2017-01-18 15:48:42'),
(206, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-01-18 15:56:27', '2017-01-18 15:56:29'),
(207, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-18 17:04:12', '2017-01-18 17:04:14'),
(208, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-01-18 17:14:47', '2017-01-18 17:14:48'),
(209, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-01-18 17:15:38', '2017-01-18 17:15:40'),
(210, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-18 18:59:23', '2017-01-18 18:59:25'),
(211, 24, 'THT', 'T 001', 'dr. M. Fiza Fadly Siregar Sp.THT-KL', '2017-01-18 20:44:36', '2017-01-18 20:44:37'),
(212, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-01-19 12:50:54', '2017-01-19 12:50:54'),
(213, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-01-19 12:55:11', '2017-01-19 12:55:11'),
(214, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-01-19 13:24:13', '2017-01-19 13:24:13'),
(215, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-01-19 14:59:46', '2017-01-19 14:59:47'),
(216, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-01-19 16:11:52', '2017-01-19 16:11:52'),
(217, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-19 16:32:48', '2017-01-19 16:32:49'),
(218, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-19 17:12:33', '2017-01-19 17:12:33'),
(219, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-01-19 17:22:41', '2017-01-19 17:22:41'),
(220, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-01-19 20:45:07', '2017-01-19 20:45:07'),
(221, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-01-20 10:24:47', '2017-01-20 10:24:47'),
(222, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-20 15:06:23', '2017-01-20 15:06:22'),
(223, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-20 16:22:55', '2017-01-20 16:22:55'),
(224, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-20 17:40:25', '2017-01-20 17:40:25'),
(225, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-01-20 18:15:08', '2017-01-20 18:15:07'),
(226, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-20 18:27:25', '2017-01-20 18:27:25'),
(227, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-23 13:04:30', '2017-01-23 13:04:31'),
(228, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-23 19:09:58', '2017-01-24 08:46:39'),
(229, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-01-24 14:32:05', '2017-01-24 14:32:06'),
(230, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-01-24 16:31:42', '2017-01-24 16:31:42'),
(231, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-01-24 16:34:19', '2017-01-24 16:34:19'),
(232, 28, 'Laboratorium', '003', 'Viki Dwi Setyantoro', '2017-01-25 14:15:09', '2017-01-25 14:15:10'),
(233, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-25 15:44:02', '2017-01-25 15:44:03'),
(234, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-01-25 16:06:06', '2017-01-25 16:06:06'),
(235, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-25 17:33:16', '2017-01-25 17:33:17'),
(236, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-01-25 17:44:56', '2017-01-25 17:44:57'),
(237, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-01-25 18:19:25', '2017-01-25 18:19:25'),
(238, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-26 12:02:21', '2017-01-26 12:02:22'),
(239, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-26 15:23:42', '2017-01-26 15:23:43'),
(240, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-01-26 17:52:21', '2017-01-26 17:52:23'),
(241, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-27 15:25:54', '2017-01-27 15:25:55'),
(242, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-27 16:31:00', '2017-01-27 16:31:01'),
(243, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-01-27 16:31:44', '2017-01-27 16:31:45'),
(244, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-01-27 16:39:01', '2017-01-27 16:39:02'),
(245, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-01-27 16:51:26', '2017-01-27 16:51:27'),
(246, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-01-28 12:10:09', '2017-01-28 12:10:10'),
(247, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-01-28 12:24:42', '2017-01-28 12:24:43'),
(248, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-01-28 12:26:02', '2017-01-28 12:26:03'),
(249, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-30 15:48:31', '2017-01-30 15:48:33'),
(250, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-01-30 18:18:23', '2017-01-30 18:18:25'),
(251, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-01-30 18:52:41', '2017-01-30 18:52:43'),
(252, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-01 10:29:49', '2017-02-01 10:29:51'),
(253, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-01 10:32:13', '2017-02-01 10:32:15'),
(254, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-01 15:21:45', '2017-02-01 15:21:48'),
(255, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-01 16:12:18', '2017-02-01 16:12:20'),
(256, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-01 17:42:39', '2017-02-01 17:42:41'),
(257, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-01 18:47:47', '2017-02-01 18:47:49'),
(258, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-02-01 18:54:30', '2017-02-01 18:54:32'),
(259, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-02-01 23:04:16', '2017-02-01 23:04:18'),
(260, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-02 10:45:55', '2017-02-02 10:45:56'),
(261, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-02 10:54:42', '2017-02-02 10:54:42'),
(262, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-02 16:48:04', '2017-02-02 16:48:04'),
(263, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-02-02 17:55:14', '2017-02-02 17:55:15'),
(264, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-02-02 21:24:53', '2017-02-02 21:24:53'),
(265, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-02 21:29:21', '2017-02-02 21:29:21'),
(266, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-03 12:13:14', '2017-02-03 12:13:14'),
(267, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-03 15:56:29', '2017-02-03 15:56:29'),
(268, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-03 15:57:05', '2017-02-03 15:57:05'),
(269, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-06 13:41:51', '2017-02-06 13:41:52'),
(270, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-06 15:09:54', '2017-02-06 15:09:56'),
(271, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-06 15:11:02', '2017-02-06 15:11:03'),
(272, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-06 15:11:45', '2017-02-06 15:11:46'),
(273, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-06 15:28:43', '2017-02-06 15:28:44'),
(274, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-02-06 16:50:17', '2017-02-06 16:50:18'),
(275, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-06 17:05:28', '2017-02-06 17:05:29'),
(276, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-02-06 17:35:03', '2017-02-06 17:35:04'),
(277, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-06 18:27:58', '2017-02-06 18:27:59'),
(278, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-06 18:32:15', '2017-02-06 18:32:16'),
(279, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-07 16:31:23', '2017-02-07 16:31:24'),
(280, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-07 19:33:21', '2017-02-07 19:33:22'),
(281, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-02-07 19:41:28', '2017-02-07 19:41:28'),
(282, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-08 12:50:26', '2017-02-08 12:50:27'),
(283, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-08 12:50:55', '2017-02-08 12:50:57'),
(284, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-02-08 17:32:28', '2017-02-08 17:32:30'),
(285, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-08 19:16:18', '2017-02-08 19:16:20'),
(286, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-02-08 19:46:12', '2017-02-08 19:46:14'),
(287, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-08 20:08:02', '2017-02-08 20:08:04'),
(288, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-09 12:37:12', '2017-02-09 12:37:13'),
(289, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-09 12:45:00', '2017-02-09 12:45:01'),
(290, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-09 13:41:04', '2017-02-09 13:41:05'),
(291, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-09 15:04:52', '2017-02-09 15:04:54'),
(292, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-02-09 15:56:06', '2017-02-09 15:56:08'),
(293, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-09 16:06:34', '2017-02-09 16:06:36'),
(294, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-02-09 16:17:53', '2017-02-09 16:17:54'),
(295, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-09 19:05:40', '2017-02-09 19:05:42'),
(296, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-10 11:46:18', '2017-02-10 11:46:20'),
(297, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-10 15:09:12', '2017-02-10 15:09:14'),
(298, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-10 15:10:02', '2017-02-10 15:10:03'),
(299, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-02-10 15:59:53', '2017-02-10 15:59:55'),
(300, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-10 16:09:51', '2017-02-10 16:09:53'),
(301, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-02-10 18:44:47', '2017-02-10 18:44:49'),
(302, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-02-10 18:46:28', '2017-02-10 18:46:29'),
(303, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-11 14:52:42', '2017-02-11 14:52:44'),
(304, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-11 19:29:13', '2017-02-11 19:29:15'),
(305, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-11 19:34:29', '2017-02-11 19:34:31'),
(306, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-11 19:34:35', '2017-02-11 19:34:36'),
(307, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-13 11:43:47', '2017-02-13 11:43:50'),
(308, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-13 13:24:43', '2017-02-13 13:24:46'),
(309, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-13 14:02:41', '2017-02-13 14:02:47'),
(310, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-13 14:06:12', '2017-02-13 14:06:14'),
(311, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-13 14:07:57', '2017-02-13 14:08:00'),
(312, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-13 14:36:35', '2017-02-13 14:36:38'),
(313, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-13 17:55:33', '2017-02-13 17:55:36'),
(314, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-13 18:10:17', '2017-02-13 18:10:19'),
(315, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-14 12:12:05', '2017-02-14 12:12:07'),
(316, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-14 14:24:42', '2017-02-14 14:24:44'),
(317, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-14 15:10:28', '2017-02-14 15:10:30'),
(318, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-14 15:37:32', '2017-02-14 15:37:33'),
(319, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-02-14 16:16:59', '2017-02-14 16:17:01'),
(320, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-14 17:51:27', '2017-02-14 17:51:29'),
(321, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-14 18:49:00', '2017-02-14 18:49:02'),
(322, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-14 19:43:14', '2017-02-14 19:43:16'),
(323, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-02-14 19:44:21', '2017-02-14 19:44:22'),
(324, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-14 19:45:21', '2017-02-14 19:45:23'),
(325, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-02-14 19:48:57', '2017-02-14 19:48:59'),
(326, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-14 20:47:42', '2017-02-14 20:47:44'),
(327, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-15 14:35:50', '2017-02-15 14:35:52'),
(328, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-15 14:51:12', '2017-02-15 14:51:14'),
(329, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-15 15:00:34', '2017-02-15 15:00:37'),
(330, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-15 15:03:48', '2017-02-15 15:03:51'),
(331, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-15 15:15:48', '2017-02-15 15:15:50'),
(332, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-02-15 15:18:44', '2017-02-15 15:18:46'),
(333, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-02-15 15:20:55', '2017-02-15 15:20:57'),
(334, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-02-15 15:21:33', '2017-02-15 15:21:35'),
(335, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-02-15 15:22:14', '2017-02-15 15:22:16'),
(336, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-15 17:37:42', '2017-02-15 17:37:44'),
(337, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-15 17:38:02', '2017-02-15 17:38:04'),
(338, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-15 19:18:22', '2017-02-15 19:18:25'),
(339, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-16 09:55:24', '2017-02-16 09:55:25'),
(340, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-02-16 11:28:53', '2017-02-16 11:28:53'),
(341, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-16 17:29:50', '2017-02-16 17:29:50'),
(342, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-16 18:36:30', '2017-02-16 18:36:30'),
(343, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-17 14:47:25', '2017-02-17 14:47:25'),
(344, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-17 15:17:01', '2017-02-17 15:17:00'),
(345, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-17 16:42:55', '2017-02-17 16:42:55'),
(346, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-17 17:45:53', '2017-02-17 17:45:53'),
(347, 24, 'THT', 'T 003', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-17 18:59:39', '2017-02-17 18:59:38'),
(348, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-17 19:12:58', '2017-02-17 19:12:57'),
(349, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-17 19:30:04', '2017-02-17 19:30:03'),
(350, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-17 19:30:54', '2017-02-17 19:30:53'),
(351, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-18 11:15:30', '2017-02-18 11:15:30'),
(352, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-02-18 12:55:29', '2017-02-18 12:55:29'),
(353, 27, 'Cardio', 'C 002', 'dr. Yuke Sarastri Sp.JP', '2017-02-18 12:56:21', '2017-02-18 12:56:21'),
(354, 27, 'Cardio', 'C 003', 'dr. Yuke Sarastri Sp.JP', '2017-02-18 12:56:43', '2017-02-18 12:56:43'),
(355, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-18 15:05:58', '2017-02-18 15:05:58'),
(356, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-20 11:28:54', '2017-02-20 11:28:55'),
(357, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-20 17:15:58', '2017-02-20 17:15:58'),
(358, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-20 18:43:05', '2017-02-20 18:43:05'),
(359, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-21 16:34:58', '2017-02-21 16:34:59'),
(360, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-21 19:43:35', '2017-02-21 19:43:36'),
(361, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-22 12:16:57', '2017-02-22 12:16:58'),
(362, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-02-22 12:25:41', '2017-02-22 12:25:42'),
(363, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-22 12:42:36', '2017-02-22 12:42:38'),
(364, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-22 15:48:23', '2017-02-22 15:48:24'),
(365, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-22 16:11:16', '2017-02-22 16:11:17'),
(366, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-22 16:12:04', '2017-02-22 16:12:05'),
(367, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-02-22 16:47:58', '2017-02-22 16:48:00'),
(368, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-23 12:13:51', '2017-02-23 12:13:53'),
(369, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-23 12:14:55', '2017-02-23 12:14:56'),
(370, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-23 14:14:08', '2017-02-23 14:14:10'),
(371, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-23 16:16:18', '2017-02-23 16:16:19'),
(372, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-23 16:16:24', '2017-02-23 16:16:26'),
(373, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-02-23 17:06:03', '2017-02-23 17:06:05'),
(374, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-02-23 19:28:17', '2017-02-23 19:28:18'),
(375, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-24 18:05:47', '2017-02-24 18:05:48'),
(376, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-25 12:58:24', '2017-02-25 12:58:26'),
(377, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-02-25 13:00:08', '2017-02-25 13:00:09'),
(378, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-25 13:00:27', '2017-02-25 13:00:29'),
(379, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-25 13:03:08', '2017-02-25 13:03:09'),
(380, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-02-25 15:03:25', '2017-02-25 15:03:27'),
(381, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-02-25 16:04:34', '2017-02-25 16:04:36'),
(382, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-02-25 16:31:06', '2017-02-25 16:31:08'),
(383, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-02-25 16:31:28', '2017-02-25 16:31:30'),
(384, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-27 12:32:13', '2017-02-27 12:32:16'),
(385, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-27 12:34:55', '2017-02-27 12:34:58'),
(386, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-27 18:46:41', '2017-02-27 18:46:44'),
(387, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-02-28 15:05:10', '2017-02-28 15:05:12'),
(388, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-02-28 17:00:19', '2017-02-28 17:00:21'),
(389, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-02-28 17:00:59', '2017-02-28 17:01:00'),
(390, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-28 19:44:58', '2017-02-28 19:45:00'),
(391, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-28 20:22:15', '2017-02-28 20:22:17'),
(392, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-02-28 20:52:21', '2017-02-28 20:52:23'),
(393, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-03-01 14:42:19', '2017-03-01 14:42:21'),
(394, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-03-01 17:01:53', '2017-03-01 17:01:56'),
(395, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-03-01 18:44:35', '2017-03-01 18:44:37'),
(396, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-01 19:26:11', '2017-03-01 19:26:13'),
(397, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-01 20:40:11', '2017-03-01 20:40:13'),
(398, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-02 12:44:56', '2017-03-02 12:44:56'),
(399, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-02 14:38:45', '2017-03-02 14:38:44'),
(400, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-02 14:40:04', '2017-03-02 14:40:04'),
(401, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-03-02 16:55:21', '2017-03-02 16:55:21'),
(402, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-02 19:32:28', '2017-03-02 19:32:28'),
(403, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-03-03 18:36:10', '2017-03-03 18:36:10'),
(404, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-03-04 12:28:41', '2017-03-04 12:28:42'),
(405, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-03-04 12:33:23', '2017-03-04 12:33:23'),
(406, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-05 14:06:35', '2017-03-05 14:06:36'),
(407, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-06 14:53:21', '2017-03-06 14:53:22'),
(408, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-06 14:53:53', '2017-03-06 14:53:54'),
(409, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-03-07 08:42:19', '2017-03-07 08:42:21'),
(410, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-07 15:29:55', '2017-03-07 15:29:56'),
(411, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-07 17:10:13', '2017-03-07 17:10:14'),
(412, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-07 18:17:54', '2017-03-07 18:17:55'),
(413, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-07 21:22:18', '2017-03-07 21:22:19'),
(414, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-08 09:43:56', '2017-03-08 09:43:57'),
(415, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-08 11:52:20', '2017-03-08 11:52:21'),
(416, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-03-08 11:53:46', '2017-03-08 11:53:47'),
(417, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-08 13:36:27', '2017-03-08 13:36:28'),
(418, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-03-08 13:49:51', '2017-03-08 13:49:52'),
(419, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-03-08 15:54:55', '2017-03-08 15:55:57'),
(420, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-09 14:40:46', '2017-03-09 14:40:47'),
(421, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-09 19:47:30', '2017-03-09 19:47:31'),
(422, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-10 10:33:57', '2017-03-10 10:33:59'),
(423, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-03-10 21:44:38', '2017-03-10 21:44:39'),
(424, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-13 18:12:40', '2017-03-13 18:12:42'),
(425, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-03-13 19:13:29', '2017-03-13 19:13:30'),
(426, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-13 20:29:24', '2017-03-13 20:29:25'),
(427, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-14 17:13:18', '2017-03-14 17:13:21'),
(428, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-14 19:11:33', '2017-03-14 19:11:35'),
(429, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-03-14 20:03:16', '2017-03-14 20:03:18'),
(430, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-15 14:15:06', '2017-03-15 14:15:08'),
(431, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-15 15:13:19', '2017-03-15 15:13:22'),
(432, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-03-15 17:50:39', '2017-03-15 17:50:41'),
(433, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-15 17:51:39', '2017-03-15 17:51:42'),
(434, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-03-15 18:32:24', '2017-03-15 18:32:27'),
(435, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-03-15 18:39:21', '2017-03-15 18:39:23'),
(436, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-15 19:03:53', '2017-03-15 19:03:55'),
(437, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-16 14:10:47', '2017-03-16 14:10:47'),
(438, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-16 14:11:27', '2017-03-16 14:11:27'),
(439, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-16 14:11:36', '2017-03-16 14:11:36'),
(440, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-03-16 15:21:03', '2017-03-16 15:21:03'),
(441, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-03-16 18:38:16', '2017-03-16 18:38:16'),
(442, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-16 20:38:11', '2017-03-16 20:38:11'),
(443, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-16 20:38:32', '2017-03-16 20:38:32'),
(444, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-03-16 21:48:28', '2017-03-16 21:48:28'),
(445, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-03-17 11:34:24', '2017-03-17 11:34:24'),
(446, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-17 15:36:20', '2017-03-17 15:36:21'),
(447, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-03-17 15:58:17', '2017-03-17 15:58:18'),
(448, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-17 18:51:49', '2017-03-17 18:51:49'),
(449, 24, 'THT', 'T 003', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-03-17 18:57:36', '2017-03-17 18:57:36'),
(450, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-03-17 21:20:30', '2017-03-17 21:20:30'),
(451, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-20 16:28:32', '2017-03-20 16:28:33'),
(452, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-20 18:04:01', '2017-03-20 18:04:01'),
(453, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-20 18:43:25', '2017-03-20 18:43:26'),
(454, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-20 18:44:00', '2017-03-20 18:44:01'),
(455, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-20 19:36:38', '2017-03-20 19:36:39'),
(456, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-03-20 19:44:49', '2017-03-20 19:44:50'),
(457, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-21 11:19:54', '2017-03-21 11:19:55'),
(458, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-21 12:43:23', '2017-03-21 12:43:24'),
(459, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-03-21 15:27:01', '2017-03-21 15:27:02'),
(460, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-21 17:04:31', '2017-03-21 17:04:32'),
(461, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-21 18:01:41', '2017-03-21 18:01:43'),
(462, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-21 18:45:44', '2017-03-21 18:45:45'),
(463, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-03-21 19:39:28', '2017-03-21 19:39:29'),
(464, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-03-21 20:22:58', '2017-03-21 20:22:59'),
(465, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-22 12:43:24', '2017-03-22 12:43:25'),
(466, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-03-22 13:08:07', '2017-03-22 13:08:09'),
(467, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-22 13:45:57', '2017-03-22 13:45:58'),
(468, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-22 14:04:16', '2017-03-22 14:04:18'),
(469, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-03-22 14:23:21', '2017-03-22 14:23:23'),
(470, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-03-22 15:26:26', '2017-03-22 15:26:27'),
(471, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-22 18:43:28', '2017-03-22 18:43:29'),
(472, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-03-22 19:31:02', '2017-03-22 19:31:03'),
(473, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-03-23 20:59:35', '2017-03-23 20:59:37'),
(474, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-24 19:59:27', '2017-03-24 19:59:28'),
(475, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-24 20:00:16', '2017-03-24 20:00:17'),
(476, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-25 11:59:44', '2017-03-25 11:59:45'),
(477, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-25 12:00:41', '2017-03-25 12:00:43'),
(478, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-26 08:29:22', '2017-03-26 09:03:38'),
(479, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-26 08:30:00', '2017-03-26 09:03:38'),
(480, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-03-26 14:05:43', '2017-03-26 14:05:45'),
(481, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-03-26 13:48:24', '2017-03-26 14:05:46'),
(482, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-26 14:04:31', '2017-03-26 14:05:47'),
(483, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-27 11:30:28', '2017-03-27 11:30:30'),
(484, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-27 12:54:31', '2017-03-27 12:54:33'),
(485, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-03-27 14:13:37', '2017-03-27 14:13:39'),
(486, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-03-27 14:39:18', '2017-03-27 14:39:20'),
(487, 27, 'Cardio', 'C 003', 'Prof. Dr. Sutomo Kasiman Sp.JP(K), FIHA, FACC, FAsCC', '2017-03-28 10:54:46', '2017-03-28 10:57:52'),
(488, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-03-28 11:12:06', '2017-03-28 11:12:08'),
(489, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-03-28 11:30:40', '2017-03-28 11:30:42'),
(490, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-03-28 16:29:41', '2017-03-28 16:29:43'),
(491, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-03-29 09:15:33', '2017-03-29 09:29:23'),
(492, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-03-29 19:26:26', '2017-03-29 19:26:29'),
(493, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-03-30 17:47:43', '2017-03-30 17:47:43'),
(494, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-03-30 17:50:08', '2017-03-30 17:50:07'),
(495, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-03-30 17:50:57', '2017-03-30 17:50:56'),
(496, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-03-30 18:04:36', '2017-03-30 18:04:36'),
(497, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-04-01 15:57:59', '2017-04-01 15:57:59'),
(498, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-01 20:25:01', '2017-04-01 20:25:01'),
(499, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-03 11:30:29', '2017-04-03 11:30:30'),
(500, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-04-03 13:33:52', '2017-04-03 13:33:53'),
(501, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-03 13:34:38', '2017-04-03 13:34:38'),
(502, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-03 13:35:25', '2017-04-03 13:35:26'),
(503, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-03 15:39:42', '2017-04-03 15:39:43'),
(504, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-04-03 16:20:33', '2017-04-03 16:20:34'),
(505, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-04-03 17:45:17', '2017-04-03 17:45:18'),
(506, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-03 19:08:16', '2017-04-03 19:08:16'),
(507, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-03 19:08:37', '2017-04-03 19:08:38'),
(508, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-04 11:53:52', '2017-04-04 11:53:53'),
(509, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-04 12:57:19', '2017-04-04 12:57:20'),
(510, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-04 12:58:50', '2017-04-04 12:58:51'),
(511, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-04-04 13:50:15', '2017-04-04 13:50:16'),
(512, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-04 14:15:17', '2017-04-04 14:15:18'),
(513, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-04-04 14:53:27', '2017-04-04 14:53:28'),
(514, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-04-04 14:53:33', '2017-04-04 14:53:36'),
(515, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-04-04 18:00:51', '2017-04-04 18:00:52'),
(516, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-04 18:16:23', '2017-04-04 18:16:24'),
(517, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-04 18:52:22', '2017-04-04 18:52:23'),
(518, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-04-04 18:58:22', '2017-04-04 18:58:23'),
(519, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-05 12:02:25', '2017-04-05 12:02:26'),
(520, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-05 12:02:44', '2017-04-05 12:02:45'),
(521, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-05 12:18:25', '2017-04-05 12:18:26'),
(522, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-04-05 12:54:04', '2017-04-05 12:54:05'),
(523, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-05 13:21:42', '2017-04-05 13:21:43'),
(524, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-04-05 14:11:41', '2017-04-05 14:11:42'),
(525, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-04-05 14:11:47', '2017-04-05 14:11:48'),
(526, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-04-05 14:18:47', '2017-04-05 14:18:48'),
(527, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari', '2017-04-05 14:18:53', '2017-04-05 14:18:54'),
(528, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-04-05 14:50:09', '2017-04-05 14:50:10'),
(529, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-04-05 15:05:02', '2017-04-05 15:05:04'),
(530, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-06 12:28:50', '2017-04-06 12:28:52'),
(531, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-06 12:44:39', '2017-04-06 12:44:41'),
(532, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-06 12:55:40', '2017-04-06 12:55:42'),
(533, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-04-06 19:03:47', '2017-04-06 19:03:49'),
(534, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-06 19:22:49', '2017-04-06 19:22:51'),
(535, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-06 19:24:36', '2017-04-06 19:24:38');
INSERT INTO `sys_schedule` (`schedule_id`, `schedule_service_id`, `schedule_service_name`, `schedule_queue_value`, `schedule_dokter_name`, `schedule_datetime`, `schedule_sync_datetime`) VALUES
(536, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-04-07 14:09:29', '2017-04-07 14:09:29'),
(537, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-07 19:03:41', '2017-04-07 19:03:41'),
(538, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-04-08 08:56:54', '2017-04-08 08:56:55'),
(539, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-08 11:31:36', '2017-04-08 11:31:36'),
(540, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-08 11:34:44', '2017-04-08 11:34:45'),
(541, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-04-08 13:04:51', '2017-04-08 13:04:52'),
(542, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-08 13:06:33', '2017-04-08 13:06:34'),
(543, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-10 14:36:30', '2017-04-10 14:36:31'),
(544, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-04-10 15:07:51', '2017-04-10 15:07:52'),
(545, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-10 15:31:07', '2017-04-10 15:31:08'),
(546, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-10 15:31:32', '2017-04-10 15:31:33'),
(547, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-04-10 15:42:13', '2017-04-10 15:42:14'),
(548, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-10 17:17:43', '2017-04-10 17:17:44'),
(549, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-10 17:43:30', '2017-04-10 17:43:32'),
(550, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-04-10 18:15:20', '2017-04-10 18:15:21'),
(551, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-04-10 18:16:57', '2017-04-10 18:16:58'),
(552, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-10 18:29:14', '2017-04-10 18:29:16'),
(553, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-10 18:30:10', '2017-04-10 18:30:11'),
(554, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-11 17:29:26', '2017-04-11 17:29:27'),
(555, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-11 19:08:53', '2017-04-11 19:08:54'),
(556, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-11 19:50:38', '2017-04-11 19:50:39'),
(557, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-04-11 19:52:16', '2017-04-11 19:52:17'),
(558, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-12 11:51:05', '2017-04-12 11:51:06'),
(559, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-12 14:21:40', '2017-04-12 14:21:41'),
(560, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-04-12 14:43:31', '2017-04-12 14:43:33'),
(561, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-04-12 20:29:00', '2017-04-12 20:29:02'),
(562, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-04-13 11:30:14', '2017-04-13 11:30:16'),
(563, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-13 12:50:16', '2017-04-13 12:50:18'),
(564, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-13 15:30:00', '2017-04-13 15:30:03'),
(565, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-13 15:30:15', '2017-04-13 15:30:17'),
(566, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-04-13 16:21:54', '2017-04-13 16:21:58'),
(567, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-13 17:02:15', '2017-04-13 17:02:18'),
(568, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-04-13 18:37:58', '2017-04-13 18:38:01'),
(569, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-04-13 18:57:49', '2017-04-13 18:57:52'),
(570, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-13 19:33:55', '2017-04-13 19:33:58'),
(571, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-13 19:35:27', '2017-04-13 19:35:30'),
(572, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-13 19:37:18', '2017-04-13 19:37:21'),
(573, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-04-13 19:41:31', '2017-04-13 19:41:35'),
(574, 27, 'Cardio', 'C 003', 'dr Anggia Lubis Sp.JP', '2017-04-13 19:49:06', '2017-04-13 19:49:09'),
(575, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-04-13 21:10:12', '2017-04-13 21:10:16'),
(576, 26, 'Gigi', 'G 006', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-13 21:27:12', '2017-04-13 21:27:15'),
(577, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-04-15 13:02:32', '2017-04-15 13:02:32'),
(578, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-15 13:07:34', '2017-04-15 13:07:35'),
(579, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-04-15 13:08:01', '2017-04-15 13:08:02'),
(580, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-04-15 13:09:34', '2017-04-15 13:09:34'),
(581, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-04-15 13:09:53', '2017-04-15 13:09:53'),
(582, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-04-15 13:17:34', '2017-04-15 13:17:34'),
(583, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-15 13:30:45', '2017-04-15 13:30:46'),
(584, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-15 13:59:10', '2017-04-15 13:59:11'),
(585, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-04-15 14:02:23', '2017-04-15 14:02:23'),
(586, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-15 14:33:29', '2017-04-15 14:33:29'),
(587, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-04-15 14:33:48', '2017-04-15 14:33:49'),
(588, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-04-15 14:58:09', '2017-04-15 14:58:09'),
(589, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-17 12:16:24', '2017-04-17 12:16:25'),
(590, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-17 12:16:41', '2017-04-17 12:16:42'),
(591, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-04-17 12:17:14', '2017-04-17 12:17:15'),
(592, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-17 12:17:39', '2017-04-17 12:17:39'),
(593, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-04-17 14:36:48', '2017-04-17 14:36:49'),
(594, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-04-17 16:47:39', '2017-04-17 16:47:41'),
(595, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-04-17 16:55:13', '2017-04-17 16:55:14'),
(596, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-04-18 16:29:59', '2017-04-18 16:30:00'),
(597, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-18 19:45:00', '2017-04-18 19:45:01'),
(598, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-04-19 10:13:10', '2017-04-19 10:13:11'),
(599, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-04-19 11:21:45', '2017-04-19 11:21:47'),
(600, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-04-19 15:30:36', '2017-04-19 15:30:37'),
(601, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-19 16:37:59', '2017-04-19 16:38:01'),
(602, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-19 16:37:19', '2017-04-19 16:38:01'),
(603, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-19 18:48:37', '2017-04-19 18:48:38'),
(604, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-19 17:33:48', '2017-04-19 18:48:39'),
(605, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-20 11:40:54', '2017-04-20 11:40:56'),
(606, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-20 18:55:00', '2017-04-20 18:55:02'),
(607, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-20 18:55:42', '2017-04-20 18:55:44'),
(608, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-20 20:13:35', '2017-04-20 20:13:37'),
(609, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-20 20:14:24', '2017-04-20 20:14:26'),
(610, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-21 20:34:22', '2017-04-21 20:34:23'),
(611, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-22 15:00:31', '2017-04-22 15:00:33'),
(612, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-24 14:26:30', '2017-04-24 14:26:32'),
(613, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-24 14:59:45', '2017-04-24 14:59:46'),
(614, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-24 15:19:13', '2017-04-24 15:19:15'),
(615, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-04-25 13:05:33', '2017-04-25 13:05:35'),
(616, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-25 13:43:26', '2017-04-25 13:43:28'),
(617, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-25 15:11:29', '2017-04-25 15:11:31'),
(618, 26, 'Gigi', 'G 007', 'drg. Muhammad Aidil Nasution', '2017-04-25 15:47:13', '2017-04-25 15:47:15'),
(619, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-04-25 15:50:30', '2017-04-25 15:50:31'),
(620, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-04-25 17:14:12', '2017-04-25 17:14:14'),
(621, 26, 'Gigi', 'G 008', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-25 18:33:26', '2017-04-25 18:33:27'),
(622, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-04-25 19:18:58', '2017-04-25 19:18:59'),
(623, 26, 'Gigi', 'G 009', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-25 20:31:23', '2017-04-25 20:31:24'),
(624, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-26 11:58:36', '2017-04-26 11:58:38'),
(625, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-04-26 16:35:20', '2017-04-26 16:35:22'),
(626, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-04-26 19:59:49', '2017-04-26 19:59:51'),
(627, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-27 13:16:36', '2017-04-27 13:16:39'),
(628, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-04-27 16:15:58', '2017-04-27 16:16:01'),
(629, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-04-27 17:43:45', '2017-04-27 17:43:48'),
(630, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-27 18:36:19', '2017-04-27 18:36:22'),
(631, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-04-27 19:21:05', '2017-04-27 19:21:07'),
(632, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-27 19:37:02', '2017-04-27 19:37:05'),
(633, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-04-28 11:12:25', '2017-04-28 11:12:25'),
(634, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-28 18:50:41', '2017-04-28 18:50:41'),
(635, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-28 19:08:39', '2017-04-28 19:08:40'),
(636, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-28 19:51:16', '2017-04-28 19:51:17'),
(637, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-04-29 14:17:10', '2017-04-29 14:17:11'),
(638, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-04-29 14:40:15', '2017-04-29 14:40:15'),
(639, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-30 15:37:41', '2017-04-30 15:37:42'),
(640, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-04-30 15:39:37', '2017-04-30 15:39:37'),
(641, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-05-02 14:03:46', '2017-05-02 14:03:46'),
(642, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-05-02 15:17:16', '2017-05-02 15:17:17'),
(643, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-05-02 16:42:39', '2017-05-02 16:42:40'),
(644, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-02 17:36:05', '2017-05-02 17:36:06'),
(645, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-02 19:53:27', '2017-05-02 19:53:27'),
(646, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-02 19:52:45', '2017-05-02 19:53:28'),
(647, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-05-02 20:19:39', '2017-05-02 20:19:40'),
(648, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-05-03 13:38:43', '2017-05-03 13:38:43'),
(649, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-03 18:44:44', '2017-05-03 18:44:44'),
(650, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-03 20:14:14', '2017-05-03 20:14:15'),
(651, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-04 11:23:10', '2017-05-04 11:23:11'),
(652, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-04 11:29:59', '2017-05-04 11:30:00'),
(653, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-04 11:30:06', '2017-05-04 11:30:07'),
(654, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-05-04 14:39:42', '2017-05-04 14:39:44'),
(655, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-05-04 15:49:44', '2017-05-04 15:49:44'),
(656, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari', '2017-05-04 16:56:14', '2017-05-04 16:56:15'),
(657, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari', '2017-05-04 17:39:15', '2017-05-04 17:39:15'),
(658, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-05-04 18:41:03', '2017-05-04 18:41:04'),
(659, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-05-04 18:43:32', '2017-05-04 18:43:33'),
(660, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-05-04 18:44:23', '2017-05-04 18:44:24'),
(661, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-04 19:30:29', '2017-05-04 19:30:30'),
(662, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-05 13:04:33', '2017-05-05 13:04:34'),
(663, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-05 13:05:21', '2017-05-05 13:05:22'),
(664, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-05 17:13:57', '2017-05-05 17:13:59'),
(665, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-06 11:21:49', '2017-05-06 11:21:51'),
(666, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-06 13:47:20', '2017-05-06 13:47:21'),
(667, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-06 13:48:17', '2017-05-06 13:48:19'),
(668, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-06 16:04:32', '2017-05-06 16:04:33'),
(669, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-05-08 16:00:23', '2017-05-08 16:00:25'),
(670, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-05-08 18:19:10', '2017-05-08 18:19:12'),
(671, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-08 18:23:31', '2017-05-08 18:23:33'),
(672, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-05-08 20:30:52', '2017-05-08 20:30:54'),
(673, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-05-09 15:45:20', '2017-05-09 15:45:22'),
(674, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-09 15:57:29', '2017-05-09 15:57:30'),
(675, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-09 15:58:35', '2017-05-09 15:58:36'),
(676, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-09 16:01:09', '2017-05-09 16:01:10'),
(677, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-05-09 16:08:21', '2017-05-09 16:08:23'),
(678, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-05-09 16:11:04', '2017-05-09 16:11:06'),
(679, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-05-09 17:30:23', '2017-05-09 17:30:25'),
(680, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-09 18:22:25', '2017-05-09 18:22:26'),
(681, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-09 19:39:56', '2017-05-09 19:39:57'),
(682, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-09 20:29:58', '2017-05-09 20:29:59'),
(683, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-10 11:20:47', '2017-05-10 11:20:50'),
(684, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-10 11:21:42', '2017-05-10 11:21:45'),
(685, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-10 13:34:08', '2017-05-10 13:34:11'),
(686, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-05-10 16:41:47', '2017-05-10 16:41:50'),
(687, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-05-10 16:42:51', '2017-05-10 16:42:54'),
(688, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-05-10 16:43:15', '2017-05-10 16:43:17'),
(689, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-05-10 16:43:41', '2017-05-10 16:43:44'),
(690, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-10 18:41:59', '2017-05-10 18:42:01'),
(691, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-05-10 19:12:17', '2017-05-10 19:12:19'),
(692, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-12 12:43:11', '2017-05-12 12:43:11'),
(693, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-12 12:44:31', '2017-05-12 12:44:31'),
(694, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-12 13:37:02', '2017-05-12 13:37:02'),
(695, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-05-12 14:07:50', '2017-05-12 14:07:50'),
(696, 24, 'THT', 'T 001', 'dr Farrel M.Ked(ORL-HNS), Sp.THT-KL', '2017-05-12 15:15:41', '2017-05-12 15:15:42'),
(697, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-12 16:14:55', '2017-05-12 16:14:56'),
(698, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-05-12 16:33:35', '2017-05-12 16:33:36'),
(699, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-05-12 17:12:53', '2017-05-12 17:12:54'),
(700, 26, 'Gigi', 'G 006', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-12 18:33:33', '2017-05-12 18:33:33'),
(701, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-13 10:58:18', '2017-05-13 10:58:23'),
(702, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-14 10:20:11', '2017-05-14 10:20:11'),
(703, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-14 10:28:54', '2017-05-14 10:28:54'),
(704, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-15 11:32:49', '2017-05-15 11:32:50'),
(705, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-15 11:47:50', '2017-05-15 11:47:51'),
(706, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-15 11:59:19', '2017-05-15 11:59:20'),
(707, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-05-15 13:32:19', '2017-05-15 13:32:20'),
(708, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-05-15 15:40:06', '2017-05-15 15:40:07'),
(709, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-15 15:53:54', '2017-05-15 15:53:55'),
(710, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-05-15 18:25:47', '2017-05-15 18:25:48'),
(711, 26, 'Gigi', 'G 006', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-15 18:50:48', '2017-05-15 18:50:49'),
(712, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-16 11:57:00', '2017-05-16 11:57:01'),
(713, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-16 12:03:55', '2017-05-16 12:03:56'),
(714, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-05-16 19:07:33', '2017-05-16 19:07:34'),
(715, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-16 19:20:37', '2017-05-16 19:20:38'),
(716, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-05-16 19:44:09', '2017-05-16 19:44:09'),
(717, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-17 15:08:49', '2017-05-17 15:08:51'),
(718, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-17 17:15:24', '2017-05-17 17:15:25'),
(719, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-05-18 15:37:51', '2017-05-18 15:37:52'),
(720, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-18 15:58:57', '2017-05-18 15:58:58'),
(721, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-18 20:23:08', '2017-05-18 20:23:10'),
(722, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-05-20 10:29:20', '2017-05-20 10:29:21'),
(723, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-22 10:34:57', '2017-05-22 10:34:59'),
(724, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-22 11:54:04', '2017-05-22 11:54:07'),
(725, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-22 11:57:12', '2017-05-22 11:57:14'),
(726, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-22 16:32:19', '2017-05-22 16:32:21'),
(727, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-22 17:18:07', '2017-05-22 17:18:10'),
(728, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-05-22 19:11:14', '2017-05-22 19:11:17'),
(729, 26, 'Gigi', 'G 005', 'drg. Muhammad Aidil Nasution', '2017-05-22 19:15:04', '2017-05-22 19:15:07'),
(730, 26, 'Gigi', 'G 006', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-22 19:54:39', '2017-05-22 19:54:42'),
(731, 26, 'Gigi', 'G 007', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-22 19:55:48', '2017-05-22 19:55:50'),
(732, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-05-23 01:32:47', '2017-05-23 01:32:44'),
(733, 12, 'Obgyn', 'O 002', 'dr Ray C Barus Sp.OG', '2017-05-23 11:06:51', '2017-05-23 11:06:53'),
(734, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-23 11:25:03', '2017-05-23 11:25:05'),
(735, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-23 11:26:53', '2017-05-23 11:26:55'),
(736, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-05-23 15:13:45', '2017-05-23 15:13:47'),
(737, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-05-23 15:17:11', '2017-05-23 15:17:13'),
(738, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-05-23 15:17:48', '2017-05-23 15:17:50'),
(739, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-05-23 15:17:55', '2017-05-23 15:17:57'),
(740, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-23 17:43:31', '2017-05-23 17:43:33'),
(741, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-05-23 18:34:15', '2017-05-23 18:34:17'),
(742, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-05-23 18:35:06', '2017-05-23 18:35:08'),
(743, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-23 19:00:45', '2017-05-23 19:00:47'),
(744, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-24 10:55:51', '2017-05-24 10:55:54'),
(745, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari', '2017-05-24 10:57:20', '2017-05-24 10:57:23'),
(746, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari', '2017-05-24 13:07:09', '2017-05-24 13:07:12'),
(747, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari', '2017-05-24 13:14:01', '2017-05-24 13:14:04'),
(748, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari', '2017-05-24 14:18:30', '2017-05-24 14:18:32'),
(749, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-05-24 14:22:16', '2017-05-24 14:22:19'),
(750, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-05-24 14:27:54', '2017-05-24 14:27:57'),
(751, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari', '2017-05-24 16:40:12', '2017-05-24 16:40:14'),
(752, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari', '2017-05-24 16:54:23', '2017-05-24 16:54:25'),
(753, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari', '2017-05-24 16:54:28', '2017-05-24 16:54:30'),
(754, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-05-24 19:22:18', '2017-05-24 19:22:20'),
(755, 24, 'THT', 'T 001', 'dr Farrel M.Ked(ORL-HNS), Sp.THT-KL', '2017-05-26 13:00:37', '2017-05-26 13:00:37'),
(756, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-05-26 14:39:31', '2017-05-26 14:39:30'),
(757, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-05-26 15:04:14', '2017-05-26 15:04:14'),
(758, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari', '2017-05-26 15:23:37', '2017-05-26 15:23:37'),
(759, 24, 'THT', 'T 002', 'dr Farrel M.Ked(ORL-HNS), Sp.THT-KL', '2017-05-26 15:48:35', '2017-05-26 15:48:34'),
(760, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-26 17:44:18', '2017-05-26 17:44:18'),
(761, 24, 'THT', 'T 003', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-26 19:20:48', '2017-05-26 19:20:48'),
(762, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-05-27 14:32:43', '2017-05-29 08:52:37'),
(763, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-05-29 10:41:07', '2017-05-29 10:41:08'),
(764, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-29 12:16:04', '2017-05-29 12:16:05'),
(765, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-29 14:57:58', '2017-05-29 14:57:58'),
(766, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-05-29 15:33:22', '2017-05-29 15:33:22'),
(767, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-29 15:47:21', '2017-05-29 15:47:22'),
(768, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-29 17:35:35', '2017-05-29 17:35:35'),
(769, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-29 17:51:25', '2017-05-29 17:51:25'),
(770, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-29 18:09:09', '2017-05-29 18:09:09'),
(771, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-30 08:20:55', '2017-05-30 08:43:45'),
(772, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-30 10:56:16', '2017-05-30 10:56:17'),
(773, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-30 12:52:28', '2017-05-30 12:52:29'),
(774, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-30 13:12:41', '2017-05-30 13:12:42'),
(775, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-30 13:12:56', '2017-05-30 13:12:57'),
(776, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-30 15:59:51', '2017-05-30 15:59:52'),
(777, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-05-30 16:53:47', '2017-05-30 16:53:48'),
(778, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-30 17:00:40', '2017-05-30 17:00:41'),
(779, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-05-30 17:29:15', '2017-05-30 17:29:16'),
(780, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-30 18:06:36', '2017-05-30 18:06:37'),
(781, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-30 18:08:03', '2017-05-30 18:08:04'),
(782, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-05-30 19:59:32', '2017-05-30 19:59:33'),
(783, 12, 'Obgyn', 'O 002', 'dr. Yudha Sudewo Sp.OG', '2017-05-31 10:12:20', '2017-05-31 10:12:22'),
(784, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-05-31 10:40:36', '2017-05-31 10:40:37'),
(785, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-05-31 10:53:46', '2017-05-31 10:53:47'),
(786, 12, 'Obgyn', 'O 003', 'dr. Yudha Sudewo Sp.OG', '2017-05-31 11:23:25', '2017-05-31 11:23:26'),
(787, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-05-31 15:00:57', '2017-05-31 15:00:58'),
(788, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-31 16:19:17', '2017-05-31 16:19:18'),
(789, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-05-31 16:29:15', '2017-05-31 16:29:16'),
(790, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-05-31 17:47:19', '2017-05-31 17:47:20'),
(791, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-05-31 18:57:05', '2017-05-31 18:57:06'),
(792, 27, 'Cardio', 'C 001', 'Prof. Dr. Sutomo Kasiman Sp.JP(K), FIHA, FACC, FAsCC', '2017-05-31 18:58:38', '2017-05-31 18:58:40'),
(793, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-05-31 20:54:52', '2017-05-31 20:54:53'),
(794, 26, 'Gigi', 'G 004', 'drg. Brian Merchantara Winato', '2017-05-31 21:14:47', '2017-05-31 21:14:48'),
(795, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-06-02 09:59:48', '2017-06-02 09:59:50'),
(796, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-02 13:20:07', '2017-06-02 13:20:09'),
(797, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-02 13:38:11', '2017-06-02 13:38:13'),
(798, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-06-02 14:08:54', '2017-06-02 14:08:56'),
(799, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-02 17:37:17', '2017-06-02 17:37:18'),
(800, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-02 17:38:35', '2017-06-02 17:38:37'),
(801, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-06-03 10:08:26', '2017-06-03 10:08:29'),
(802, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-06-03 10:26:20', '2017-06-03 10:26:23'),
(803, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-03 10:57:52', '2017-06-03 11:58:33'),
(804, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-05 10:35:50', '2017-06-05 10:35:53'),
(805, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-05 11:27:43', '2017-06-05 11:27:46'),
(806, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-05 11:49:59', '2017-06-05 11:50:02'),
(807, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-05 14:54:24', '2017-06-05 14:54:29'),
(808, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-05 14:54:32', '2017-06-05 14:54:34'),
(809, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-06-06 09:35:47', '2017-06-06 09:35:49'),
(810, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-06-06 09:37:29', '2017-06-06 09:37:31'),
(811, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-06 10:45:02', '2017-06-06 10:45:04'),
(812, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-06 10:45:40', '2017-06-06 10:45:42'),
(813, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-06 13:24:44', '2017-06-06 13:24:45'),
(814, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-07 12:47:42', '2017-06-07 12:47:45'),
(815, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-07 13:10:05', '2017-06-07 13:10:07'),
(816, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-07 18:26:18', '2017-06-07 18:26:20'),
(817, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-07 18:24:06', '2017-06-07 18:26:21'),
(818, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-08 13:19:41', '2017-06-08 13:19:42'),
(819, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-08 14:13:56', '2017-06-08 14:13:56'),
(820, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-08 18:16:11', '2017-06-08 18:16:11'),
(821, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-06-08 19:53:01', '2017-06-08 20:43:04'),
(822, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-06-09 08:48:17', '2017-06-09 08:48:18'),
(823, 25, 'Neurologi', 'N 002', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-06-09 09:40:35', '2017-06-09 09:40:35'),
(824, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-09 11:47:53', '2017-06-09 11:47:53'),
(825, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-09 11:57:10', '2017-06-09 11:57:10'),
(826, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-09 11:57:21', '2017-06-09 11:57:22'),
(827, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-09 17:45:20', '2017-06-09 17:45:21'),
(828, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-06-09 19:39:47', '2017-06-09 19:39:47'),
(829, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-10 11:24:25', '2017-06-10 11:24:26'),
(830, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-10 11:34:12', '2017-06-10 11:34:13'),
(831, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-10 12:01:31', '2017-06-10 12:01:32'),
(832, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-10 13:08:12', '2017-06-10 13:08:13'),
(833, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-10 13:38:50', '2017-06-10 13:38:51'),
(834, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-10 13:39:38', '2017-06-10 13:39:39'),
(835, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-10 13:39:59', '2017-06-10 13:40:00'),
(836, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-10 14:42:23', '2017-06-10 14:42:23'),
(837, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-12 12:30:31', '2017-06-12 12:30:32'),
(838, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-12 14:42:56', '2017-06-12 14:42:57'),
(839, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-12 14:45:57', '2017-06-12 14:45:58'),
(840, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-12 14:46:26', '2017-06-12 14:46:27'),
(841, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-12 15:27:08', '2017-06-12 15:27:09'),
(842, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-12 16:15:05', '2017-06-12 16:15:01'),
(843, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-12 17:32:11', '2017-06-12 17:32:12'),
(844, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-12 17:46:22', '2017-06-12 17:46:23'),
(845, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-12 18:01:58', '2017-06-12 18:01:59'),
(846, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-06-12 19:46:08', '2017-06-12 19:46:09'),
(847, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-13 10:40:05', '2017-06-13 10:40:06'),
(848, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-13 10:40:28', '2017-06-13 10:40:29'),
(849, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-13 10:42:12', '2017-06-13 10:42:13'),
(850, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-13 11:51:51', '2017-06-13 11:51:52'),
(851, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-13 13:53:47', '2017-06-13 13:53:48'),
(852, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-13 17:43:45', '2017-06-13 17:43:46'),
(853, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-06-14 09:48:25', '2017-06-14 09:48:27'),
(854, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-14 12:29:01', '2017-06-14 12:29:02'),
(855, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-14 12:29:39', '2017-06-14 12:29:41'),
(856, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-14 12:30:23', '2017-06-14 12:30:24'),
(857, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-14 17:11:42', '2017-06-14 17:11:44'),
(858, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-06-15 16:19:04', '2017-06-15 16:19:06'),
(859, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-06-15 17:08:49', '2017-06-15 17:08:50'),
(860, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-16 09:46:10', '2017-06-16 09:46:12'),
(861, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-16 09:47:05', '2017-06-16 09:47:07'),
(862, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-16 09:58:16', '2017-06-16 09:58:18'),
(863, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-16 10:05:01', '2017-06-16 10:05:03'),
(864, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-16 10:25:05', '2017-06-16 10:25:07'),
(865, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-16 14:05:24', '2017-06-16 14:05:26'),
(866, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-16 14:19:01', '2017-06-16 14:19:02'),
(867, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-06-16 14:27:20', '2017-06-16 14:27:22'),
(868, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-16 16:55:48', '2017-06-16 16:55:49'),
(869, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-16 17:31:19', '2017-06-16 17:31:21'),
(870, 26, 'Gigi', 'G 005', 'drg. Brian Merchantara Winato', '2017-06-16 18:50:50', '2017-06-16 18:50:52'),
(871, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-17 10:00:53', '2017-06-17 10:00:55'),
(872, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-17 11:46:59', '2017-06-17 11:47:01'),
(873, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-17 11:49:22', '2017-06-17 11:49:24'),
(874, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-17 12:10:08', '2017-06-17 12:10:10'),
(875, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-17 13:30:32', '2017-06-17 13:30:34'),
(876, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-17 14:40:26', '2017-06-17 14:40:28'),
(877, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-17 14:45:29', '2017-06-17 14:45:31'),
(878, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-18 17:44:16', '2017-06-18 17:44:18'),
(879, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-18 17:44:23', '2017-06-18 17:44:24'),
(880, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-18 17:44:41', '2017-06-18 17:44:43'),
(881, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 09:49:26', '2017-06-19 09:49:28'),
(882, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 13:41:59', '2017-06-19 13:42:01'),
(883, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 14:05:45', '2017-06-19 14:05:47'),
(884, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 14:06:19', '2017-06-19 14:06:21'),
(885, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 14:32:52', '2017-06-19 14:32:54'),
(886, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 14:36:58', '2017-06-19 14:36:59'),
(887, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 15:51:08', '2017-06-19 15:51:09'),
(888, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-19 16:45:20', '2017-06-19 16:45:21'),
(889, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-19 16:53:13', '2017-06-19 16:53:15'),
(890, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-19 17:15:16', '2017-06-19 17:15:18'),
(891, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-19 17:47:08', '2017-06-19 17:47:10'),
(892, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-06-20 11:22:36', '2017-06-20 11:22:38'),
(893, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-20 12:00:29', '2017-06-20 12:00:31'),
(894, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-20 12:01:08', '2017-06-20 12:01:10'),
(895, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-20 15:16:06', '2017-06-20 15:16:09'),
(896, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-20 15:16:34', '2017-06-20 15:16:37'),
(897, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-06-20 15:27:30', '2017-06-20 15:27:33'),
(898, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-20 17:00:03', '2017-06-20 17:00:05'),
(899, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-06-20 18:27:18', '2017-06-20 18:27:21'),
(900, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-06-20 19:45:08', '2017-06-20 19:45:11'),
(901, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-06-21 08:58:37', '2017-06-21 08:58:40'),
(902, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-21 11:48:44', '2017-06-21 11:48:46'),
(903, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-21 11:49:46', '2017-06-21 11:49:48'),
(904, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-21 12:26:12', '2017-06-21 12:26:14'),
(905, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-06-21 15:14:34', '2017-06-21 15:14:37'),
(906, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-06-21 15:24:34', '2017-06-21 15:24:36'),
(907, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-21 16:01:10', '2017-06-21 16:01:12'),
(908, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-21 16:03:41', '2017-06-21 16:03:44'),
(909, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-21 17:49:52', '2017-06-21 17:49:54'),
(910, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-06-22 14:06:19', '2017-06-22 14:06:21'),
(911, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-06-22 15:18:52', '2017-06-22 15:18:54'),
(912, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-06-22 18:43:39', '2017-06-22 18:43:42'),
(913, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-23 11:55:09', '2017-06-23 11:55:12'),
(914, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-29 11:25:12', '2017-06-29 11:25:17'),
(915, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-06-29 11:25:23', '2017-06-29 11:25:27'),
(916, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-07-03 08:57:27', '2017-07-03 08:57:29'),
(917, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-07-03 13:11:43', '2017-07-03 13:11:44'),
(918, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-07-03 13:52:21', '2017-07-03 13:52:22'),
(919, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-07-03 13:52:37', '2017-07-03 13:52:38'),
(920, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-07-03 14:17:31', '2017-07-03 14:17:32'),
(921, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-03 17:59:50', '2017-07-03 17:59:51'),
(922, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-03 18:00:28', '2017-07-03 18:00:29'),
(923, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-07-04 15:50:35', '2017-07-04 15:50:35'),
(924, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-04 15:52:45', '2017-07-04 15:52:46'),
(925, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-04 18:08:21', '2017-07-04 18:08:21'),
(926, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-05 12:03:45', '2017-07-05 12:03:46'),
(927, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-07-05 17:22:42', '2017-07-05 17:22:43'),
(928, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-06 11:54:12', '2017-07-06 11:54:13'),
(929, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-06 12:13:55', '2017-07-06 12:13:56'),
(930, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-07-06 15:31:19', '2017-07-06 15:31:21'),
(931, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-06 15:33:32', '2017-07-06 15:33:34'),
(932, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-07-07 08:49:15', '2017-07-07 08:49:16'),
(933, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-07 12:30:15', '2017-07-07 12:30:16'),
(934, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-07-07 14:11:00', '2017-07-07 14:11:01'),
(935, 14, 'Penyakit Dalam', 'D 002', 'dr. Dhini Silvana Sp.PD', '2017-07-07 14:31:55', '2017-07-07 14:31:55'),
(936, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-07 14:40:51', '2017-07-07 14:40:52'),
(937, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-07 16:29:20', '2017-07-07 16:29:21'),
(938, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-07-07 18:42:09', '2017-07-07 18:42:10'),
(939, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-07-08 11:29:32', '2017-07-08 11:29:34'),
(940, 25, 'Neurologi', 'N 002', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-07-08 12:05:42', '2017-07-08 12:05:43'),
(941, 25, 'Neurologi', 'N 003', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-07-08 12:06:40', '2017-07-08 12:06:41'),
(942, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-08 13:01:55', '2017-07-08 13:01:57'),
(943, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-07-10 09:05:19', '2017-07-10 09:05:21'),
(944, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-07-10 09:13:20', '2017-07-10 09:13:22'),
(945, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-07-10 09:27:03', '2017-07-10 09:27:05'),
(946, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-07-10 09:40:56', '2017-07-10 09:40:58'),
(947, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-10 10:17:19', '2017-07-10 10:17:21'),
(948, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-10 10:18:00', '2017-07-10 10:18:02'),
(949, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-10 13:41:53', '2017-07-10 13:41:54'),
(950, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-10 14:09:12', '2017-07-10 14:09:14'),
(951, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-10 14:15:08', '2017-07-10 14:15:10'),
(952, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-10 14:15:33', '2017-07-10 14:15:35'),
(953, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-10 14:25:49', '2017-07-10 14:25:51'),
(954, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-10 15:13:21', '2017-07-10 15:13:23'),
(955, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-10 17:06:39', '2017-07-10 17:06:40'),
(956, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-07-10 17:47:24', '2017-07-10 17:47:26'),
(957, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-07-11 14:28:12', '2017-07-11 14:28:14'),
(958, 27, 'Cardio', 'C 002', 'dr. Yuke Sarastri Sp.JP', '2017-07-11 20:08:43', '2017-07-11 20:08:46'),
(959, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-07-12 09:54:07', '2017-07-12 09:54:09'),
(960, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 12:26:18', '2017-07-12 12:26:20'),
(961, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 12:27:13', '2017-07-12 12:27:15'),
(962, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 12:27:54', '2017-07-12 12:27:56'),
(963, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 12:28:46', '2017-07-12 12:28:48'),
(964, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 12:32:24', '2017-07-12 12:32:26'),
(965, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 14:17:41', '2017-07-12 14:17:43'),
(966, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 14:17:47', '2017-07-12 14:17:48'),
(967, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-07-12 14:33:18', '2017-07-12 14:33:19'),
(968, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-07-12 14:39:25', '2017-07-12 14:39:27'),
(969, 24, 'THT', 'T 003', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-07-12 15:40:07', '2017-07-12 15:40:09'),
(970, 11, 'Estetika', 'K 011', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 16:21:57', '2017-07-12 16:21:59'),
(971, 11, 'Estetika', 'K 012', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 17:02:25', '2017-07-12 17:02:27'),
(972, 11, 'Estetika', 'K 013', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-12 17:15:03', '2017-07-12 17:15:05'),
(973, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-07-12 18:18:04', '2017-07-12 18:18:06'),
(974, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-12 19:35:40', '2017-07-12 19:35:41'),
(975, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-07-13 14:03:46', '2017-07-13 14:03:48'),
(976, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-13 19:10:40', '2017-07-13 19:10:42'),
(977, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-14 12:59:11', '2017-07-14 12:59:14'),
(978, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-14 14:48:32', '2017-07-14 14:48:35'),
(979, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-14 14:49:06', '2017-07-14 14:49:08'),
(980, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-14 14:49:34', '2017-07-14 14:49:37'),
(981, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-14 15:09:58', '2017-07-14 15:10:01'),
(982, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-14 15:55:29', '2017-07-14 15:55:31'),
(983, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-14 18:34:57', '2017-07-14 18:34:59'),
(984, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-14 18:55:10', '2017-07-14 18:55:12'),
(985, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-15 10:35:23', '2017-07-15 10:35:27'),
(986, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-15 13:45:11', '2017-07-15 13:45:14'),
(987, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-07-17 09:05:34', '2017-07-17 09:05:34'),
(988, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-17 11:34:07', '2017-07-17 11:34:07'),
(989, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-17 12:34:52', '2017-07-17 12:34:52'),
(990, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-17 12:51:22', '2017-07-17 12:51:22'),
(991, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-17 12:52:27', '2017-07-17 12:52:27'),
(992, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-17 12:54:00', '2017-07-17 12:54:00'),
(993, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-17 18:47:53', '2017-07-17 18:47:53'),
(994, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-18 18:33:29', '2017-07-18 18:33:30'),
(995, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-19 12:01:21', '2017-07-19 12:01:21'),
(996, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-19 20:19:40', '2017-07-19 20:19:41'),
(997, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-07-20 16:12:51', '2017-07-20 16:12:52'),
(998, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-07-20 18:38:23', '2017-07-20 18:38:24'),
(999, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-20 19:44:08', '2017-07-20 19:44:08'),
(1000, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 12:14:51', '2017-07-21 12:14:51'),
(1001, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 13:57:39', '2017-07-21 13:57:39'),
(1002, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 14:21:42', '2017-07-21 14:21:43');
INSERT INTO `sys_schedule` (`schedule_id`, `schedule_service_id`, `schedule_service_name`, `schedule_queue_value`, `schedule_dokter_name`, `schedule_datetime`, `schedule_sync_datetime`) VALUES
(1003, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 15:35:42', '2017-07-21 15:35:44'),
(1004, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 15:36:04', '2017-07-21 15:36:05'),
(1005, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 15:44:15', '2017-07-21 15:44:16'),
(1006, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 15:45:20', '2017-07-21 15:45:20'),
(1007, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-21 16:37:32', '2017-07-21 16:37:33'),
(1008, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-07-22 11:21:07', '2017-07-22 11:21:08'),
(1009, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-24 12:23:54', '2017-07-24 12:23:55'),
(1010, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-24 12:30:03', '2017-07-24 12:30:04'),
(1011, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-24 13:18:49', '2017-07-24 13:18:50'),
(1012, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-24 13:33:54', '2017-07-24 13:33:55'),
(1013, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-07-24 19:31:47', '2017-07-24 19:31:48'),
(1014, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-25 08:17:28', '2017-07-25 08:17:32'),
(1015, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-07-25 19:43:43', '2017-07-25 19:43:45'),
(1016, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-25 20:10:46', '2017-07-25 20:10:48'),
(1017, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-07-26 09:09:33', '2017-07-26 09:09:36'),
(1018, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-26 11:06:21', '2017-07-26 11:06:24'),
(1019, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-26 11:06:44', '2017-07-26 11:06:46'),
(1020, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-26 11:06:50', '2017-07-26 11:06:52'),
(1021, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-07-26 12:28:07', '2017-07-26 12:28:09'),
(1022, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-26 14:40:08', '2017-07-26 14:40:10'),
(1023, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 10:45:43', '2017-07-28 10:45:43'),
(1024, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 10:46:16', '2017-07-28 10:46:16'),
(1025, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 10:47:11', '2017-07-28 10:47:11'),
(1026, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 10:53:37', '2017-07-28 10:53:37'),
(1027, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 10:59:50', '2017-07-28 10:59:50'),
(1028, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 11:23:58', '2017-07-28 11:23:57'),
(1029, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 11:24:04', '2017-07-28 11:24:03'),
(1030, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 15:30:46', '2017-07-28 15:30:47'),
(1031, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-07-28 16:21:48', '2017-07-28 16:21:48'),
(1032, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-28 17:57:38', '2017-07-28 17:57:37'),
(1033, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-28 18:35:56', '2017-07-28 18:35:56'),
(1034, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-07-28 18:39:20', '2017-07-28 18:39:19'),
(1035, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-07-28 19:10:59', '2017-07-28 19:10:59'),
(1036, 26, 'Gigi', 'G 005', 'drg. Muhammad Aidil Nasution', '2017-07-28 22:36:56', '2017-07-28 22:36:56'),
(1037, 26, 'Gigi', 'G 006', 'drg. Muhammad Aidil Nasution', '2017-07-28 22:45:06', '2017-07-28 22:45:06'),
(1038, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-31 17:46:19', '2017-07-31 17:46:20'),
(1039, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-31 18:50:17', '2017-07-31 18:50:18'),
(1040, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-07-31 19:21:01', '2017-07-31 19:21:01'),
(1041, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-08-01 18:24:06', '2017-08-01 18:24:07'),
(1042, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-08-01 19:05:10', '2017-08-01 19:05:10'),
(1043, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-01 19:07:32', '2017-08-01 19:07:33'),
(1044, 27, 'Cardio', 'C 003', 'dr Anggia Lubis Sp.JP', '2017-08-01 19:45:57', '2017-08-01 19:45:57'),
(1045, 27, 'Cardio', 'C 004', 'dr Anggia Lubis Sp.JP', '2017-08-01 20:35:02', '2017-08-01 20:35:03'),
(1046, 27, 'Cardio', 'C 005', 'dr Anggia Lubis Sp.JP', '2017-08-01 20:36:09', '2017-08-01 20:36:10'),
(1047, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-08-02 09:08:04', '2017-08-02 09:08:06'),
(1048, 25, 'Neurologi', 'N 002', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-08-02 09:15:51', '2017-08-02 09:15:53'),
(1049, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-08-02 09:39:27', '2017-08-02 09:39:28'),
(1050, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-02 10:54:36', '2017-08-02 10:54:38'),
(1051, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-02 10:55:18', '2017-08-02 10:55:20'),
(1052, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-02 11:32:54', '2017-08-02 11:32:55'),
(1053, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-02 12:47:41', '2017-08-02 12:47:42'),
(1054, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-08-02 16:23:46', '2017-08-02 16:23:49'),
(1055, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-02 16:35:55', '2017-08-02 16:35:56'),
(1056, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-08-02 16:44:56', '2017-08-02 16:44:57'),
(1057, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-02 18:12:00', '2017-08-02 18:12:01'),
(1058, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-03 08:58:27', '2017-08-03 08:58:28'),
(1059, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-08-03 17:15:05', '2017-08-03 17:15:07'),
(1060, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-03 18:36:57', '2017-08-03 18:36:57'),
(1061, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-03 18:41:07', '2017-08-03 18:41:07'),
(1062, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-03 18:42:09', '2017-08-03 18:42:09'),
(1063, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-03 22:09:16', '2017-08-03 22:09:17'),
(1064, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-03 22:09:35', '2017-08-03 22:09:36'),
(1065, 26, 'Gigi', 'G 006', 'drg. Brian Merchantara Winato', '2017-08-03 22:10:27', '2017-08-03 22:10:28'),
(1066, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-08-04 09:14:54', '2017-08-04 09:14:56'),
(1067, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-04 11:33:16', '2017-08-04 11:33:18'),
(1068, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-04 11:34:12', '2017-08-04 11:34:14'),
(1069, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-04 12:20:04', '2017-08-04 12:20:06'),
(1070, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-08-04 12:46:55', '2017-08-04 12:46:56'),
(1071, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-08-04 14:15:41', '2017-08-04 14:15:44'),
(1072, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-08-04 15:58:10', '2017-08-04 15:58:11'),
(1073, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-04 16:11:57', '2017-08-04 16:11:59'),
(1074, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-04 18:05:01', '2017-08-04 18:05:03'),
(1075, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-04 18:41:42', '2017-08-04 18:41:43'),
(1076, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-08-05 09:26:04', '2017-08-05 09:26:06'),
(1077, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-08-05 10:37:34', '2017-08-05 10:37:36'),
(1078, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-08-05 10:37:49', '2017-08-05 10:37:51'),
(1079, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-05 18:38:27', '2017-08-05 18:38:30'),
(1080, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-05 18:40:32', '2017-08-05 18:40:34'),
(1081, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-07 11:22:46', '2017-08-07 11:22:48'),
(1082, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-07 12:31:13', '2017-08-07 12:31:16'),
(1083, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-07 12:31:44', '2017-08-07 12:31:46'),
(1084, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-07 12:38:05', '2017-08-07 12:38:07'),
(1085, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-07 14:12:13', '2017-08-07 14:12:16'),
(1086, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-08-07 18:46:07', '2017-08-07 18:46:09'),
(1087, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-08-07 18:50:13', '2017-08-07 18:50:16'),
(1088, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-07 19:36:35', '2017-08-07 19:36:37'),
(1089, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-08-07 21:22:45', '2017-08-07 21:22:47'),
(1090, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-08 16:49:23', '2017-08-08 16:49:25'),
(1091, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-09 10:56:46', '2017-08-09 10:56:50'),
(1092, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-09 12:39:32', '2017-08-09 12:39:35'),
(1093, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-08-09 13:36:28', '2017-08-09 13:36:31'),
(1094, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-08-09 14:22:10', '2017-08-09 14:22:13'),
(1095, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-08-09 18:00:58', '2017-08-09 18:01:01'),
(1096, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-08-09 18:31:38', '2017-08-09 18:31:41'),
(1097, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-09 18:31:46', '2017-08-09 18:31:49'),
(1098, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-08-10 10:19:02', '2017-08-10 10:19:03'),
(1099, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-08-10 13:45:08', '2017-08-10 13:45:00'),
(1100, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-10 14:16:32', '2017-08-10 14:16:32'),
(1101, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-08-10 17:01:38', '2017-08-10 17:01:38'),
(1102, 24, 'THT', 'T 003', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-08-10 17:53:05', '2017-08-10 17:53:04'),
(1103, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-11 14:00:22', '2017-08-11 14:00:22'),
(1104, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-11 16:34:40', '2017-08-11 16:34:40'),
(1105, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-08-11 19:20:02', '2017-08-11 19:20:02'),
(1106, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-14 11:47:02', '2017-08-14 11:47:03'),
(1107, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-08-14 14:27:54', '2017-08-14 14:27:54'),
(1108, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-08-14 16:14:01', '2017-08-14 16:14:03'),
(1109, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-15 18:45:10', '2017-08-15 18:45:10'),
(1110, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-08-15 18:54:11', '2017-08-15 18:54:12'),
(1111, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-15 20:08:51', '2017-08-15 20:08:51'),
(1112, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-08-16 09:02:15', '2017-08-16 09:02:16'),
(1113, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-16 13:49:46', '2017-08-16 13:49:48'),
(1114, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-16 13:50:41', '2017-08-16 13:50:42'),
(1115, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-08-16 15:34:27', '2017-08-16 15:34:28'),
(1116, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-08-16 21:16:08', '2017-08-16 21:16:09'),
(1117, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-08-18 09:11:54', '2017-08-18 09:11:55'),
(1118, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-18 12:07:59', '2017-08-18 12:08:00'),
(1119, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-08-18 12:39:59', '2017-08-18 12:40:00'),
(1120, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-18 13:11:51', '2017-08-18 13:11:52'),
(1121, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-18 13:13:07', '2017-08-18 13:13:09'),
(1122, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-18 13:14:18', '2017-08-18 13:14:19'),
(1123, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-18 13:14:51', '2017-08-18 13:14:52'),
(1124, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-08-18 14:31:11', '2017-08-18 14:31:13'),
(1125, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-08-18 14:39:48', '2017-08-18 14:39:49'),
(1126, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-18 14:39:54', '2017-08-18 14:39:55'),
(1127, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-18 14:41:29', '2017-08-18 14:41:30'),
(1128, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-08-18 15:25:35', '2017-08-18 15:25:36'),
(1129, 26, 'Gigi', 'G 005', 'drg. Muhammad Aidil Nasution', '2017-08-18 15:38:43', '2017-08-18 15:38:45'),
(1130, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-08-18 16:24:48', '2017-08-18 16:24:49'),
(1131, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-19 14:03:59', '2017-08-19 14:04:01'),
(1132, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-19 14:04:24', '2017-08-19 14:04:26'),
(1133, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-08-19 14:05:29', '2017-08-19 14:05:31'),
(1134, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-21 14:24:16', '2017-08-21 14:24:18'),
(1135, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-21 14:26:28', '2017-08-21 14:26:30'),
(1136, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-08-21 15:01:45', '2017-08-21 15:01:47'),
(1137, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-21 16:26:19', '2017-08-21 16:26:21'),
(1138, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-08-21 18:35:47', '2017-08-21 18:35:50'),
(1139, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-08-22 14:57:00', '2017-08-22 14:57:03'),
(1140, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-22 18:12:51', '2017-08-22 18:12:53'),
(1141, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-08-22 19:55:31', '2017-08-22 19:55:34'),
(1142, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-08-22 20:08:44', '2017-08-22 20:08:46'),
(1143, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-23 12:24:48', '2017-08-23 12:24:51'),
(1144, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-23 12:41:23', '2017-08-23 12:41:26'),
(1145, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-23 14:43:53', '2017-08-23 14:43:56'),
(1146, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-23 15:12:38', '2017-08-23 15:12:41'),
(1147, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-23 15:12:59', '2017-08-23 15:13:02'),
(1148, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-23 15:58:23', '2017-08-23 15:58:26'),
(1149, 27, 'Cardio', 'C 001', 'Prof. Dr. Sutomo Kasiman Sp.JP(K), FIHA, FACC, FAsCC', '2017-08-23 17:14:48', '2017-08-23 17:14:50'),
(1150, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-23 18:31:06', '2017-08-23 18:31:09'),
(1151, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-23 19:26:15', '2017-08-23 19:26:17'),
(1152, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-24 18:46:36', '2017-08-24 18:46:37'),
(1153, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-24 19:25:35', '2017-08-24 19:25:36'),
(1154, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-24 20:08:09', '2017-08-24 20:08:10'),
(1155, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-24 20:31:58', '2017-08-24 20:31:59'),
(1156, 26, 'Gigi', 'G 005', 'drg. Brian Merchantara Winato', '2017-08-24 20:38:45', '2017-08-24 20:38:45'),
(1157, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-25 11:29:11', '2017-08-25 11:29:11'),
(1158, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-25 15:28:54', '2017-08-25 15:28:55'),
(1159, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-25 15:54:47', '2017-08-25 15:54:47'),
(1160, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-25 17:49:49', '2017-08-25 17:49:50'),
(1161, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-25 18:24:37', '2017-08-25 18:24:38'),
(1162, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-25 18:25:53', '2017-08-25 18:25:53'),
(1163, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-25 19:24:38', '2017-08-25 19:24:38'),
(1164, 26, 'Gigi', 'G 006', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-25 20:23:26', '2017-08-25 20:23:27'),
(1165, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-08-26 14:32:13', '2017-08-26 14:32:13'),
(1166, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-08-26 15:30:23', '2017-08-26 15:30:23'),
(1167, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-08-26 16:13:12', '2017-08-26 16:13:12'),
(1168, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-28 11:24:51', '2017-08-28 11:24:52'),
(1169, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-28 11:56:05', '2017-08-28 11:56:06'),
(1170, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-28 11:56:32', '2017-08-28 11:56:33'),
(1171, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-28 13:12:25', '2017-08-28 13:12:26'),
(1172, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-28 13:13:53', '2017-08-28 13:13:54'),
(1173, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-28 13:19:35', '2017-08-28 13:19:36'),
(1174, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-08-28 15:33:03', '2017-08-28 15:33:04'),
(1175, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-28 15:36:50', '2017-08-28 15:36:51'),
(1176, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-28 16:40:10', '2017-08-28 16:40:11'),
(1177, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-28 17:55:45', '2017-08-28 17:55:46'),
(1178, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-08-28 20:01:35', '2017-08-28 20:01:36'),
(1179, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-29 19:34:53', '2017-08-29 19:34:54'),
(1180, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-30 12:08:25', '2017-08-30 12:08:27'),
(1181, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-30 12:09:21', '2017-08-30 12:09:23'),
(1182, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-30 12:09:52', '2017-08-30 12:09:54'),
(1183, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-30 12:19:30', '2017-08-30 12:19:32'),
(1184, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-08-30 15:13:36', '2017-08-30 15:13:37'),
(1185, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-08-30 15:52:46', '2017-08-30 15:52:47'),
(1186, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-08-31 14:01:57', '2017-08-31 14:01:58'),
(1187, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-08-31 18:57:16', '2017-08-31 18:57:17'),
(1188, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-09-02 12:36:36', '2017-09-02 12:36:38'),
(1189, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-02 15:51:24', '2017-09-02 15:51:26'),
(1190, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-02 16:53:37', '2017-09-02 16:53:39'),
(1191, 27, 'Cardio', 'C 001', 'Prof. Dr. Sutomo Kasiman Sp.JP(K), FIHA, FACC, FAsCC', '2017-09-04 12:04:05', '2017-09-04 12:04:08'),
(1192, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-04 12:16:59', '2017-09-04 12:17:02'),
(1193, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-04 16:48:19', '2017-09-04 16:48:21'),
(1194, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-09-04 17:12:40', '2017-09-04 17:12:42'),
(1195, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-09-04 17:46:32', '2017-09-04 17:46:35'),
(1196, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-09-04 19:13:06', '2017-09-04 19:13:09'),
(1197, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-06 13:42:50', '2017-09-06 13:42:53'),
(1198, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-06 13:44:10', '2017-09-06 13:44:13'),
(1199, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-06 14:52:06', '2017-09-06 14:52:08'),
(1200, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-06 14:52:25', '2017-09-06 14:52:28'),
(1201, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-06 20:02:09', '2017-09-06 20:02:12'),
(1202, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-07 12:39:33', '2017-09-07 12:39:33'),
(1203, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-09-07 13:56:51', '2017-09-07 13:56:51'),
(1204, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-08 10:20:28', '2017-09-08 10:20:29'),
(1205, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-08 12:10:21', '2017-09-08 12:10:22'),
(1206, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-08 12:10:29', '2017-09-08 12:10:30'),
(1207, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-08 12:10:38', '2017-09-08 12:10:38'),
(1208, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-09-08 15:45:38', '2017-09-08 15:45:39'),
(1209, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-09-08 19:19:13', '2017-09-08 19:19:14'),
(1210, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-09-08 19:48:08', '2017-09-08 19:48:08'),
(1211, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-09 15:03:40', '2017-09-09 15:03:41'),
(1212, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-09 15:05:50', '2017-09-09 15:05:51'),
(1213, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-10 12:04:16', '2017-09-10 12:04:18'),
(1214, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-10 12:05:06', '2017-09-10 12:05:07'),
(1215, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-09-11 09:43:48', '2017-09-11 09:43:51'),
(1216, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-09-11 10:23:19', '2017-09-11 10:23:21'),
(1217, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-11 10:23:25', '2017-09-11 10:23:26'),
(1218, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-11 11:21:55', '2017-09-11 11:21:56'),
(1219, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-11 13:59:48', '2017-09-11 13:59:51'),
(1220, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-11 14:07:18', '2017-09-11 15:41:13'),
(1221, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-11 17:39:29', '2017-09-11 17:39:29'),
(1222, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-11 19:36:54', '2017-09-11 19:36:55'),
(1223, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-09-12 08:58:46', '2017-09-12 08:58:48'),
(1224, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-12 11:49:52', '2017-09-12 11:49:53'),
(1225, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-12 11:57:56', '2017-09-12 11:57:57'),
(1226, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-09-12 14:01:49', '2017-09-12 14:01:50'),
(1227, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-09-12 15:14:25', '2017-09-12 15:14:26'),
(1228, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-12 20:14:02', '2017-09-12 20:14:03'),
(1229, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-09-13 09:26:25', '2017-09-13 09:26:27'),
(1230, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 12:03:00', '2017-09-13 12:03:02'),
(1231, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 12:03:31', '2017-09-13 12:03:32'),
(1232, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 16:56:15', '2017-09-13 16:56:17'),
(1233, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 16:57:22', '2017-09-13 16:57:23'),
(1234, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 16:58:08', '2017-09-13 16:58:09'),
(1235, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 16:59:36', '2017-09-13 16:59:37'),
(1236, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 17:00:19', '2017-09-13 17:00:21'),
(1237, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 17:01:13', '2017-09-13 17:01:14'),
(1238, 11, 'Estetika', 'K 011', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 17:02:51', '2017-09-13 17:02:52'),
(1239, 11, 'Estetika', 'K 012', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 17:03:31', '2017-09-13 17:03:32'),
(1240, 11, 'Estetika', 'K 013', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 17:04:00', '2017-09-13 17:04:01'),
(1241, 11, 'Estetika', 'K 014', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-13 17:05:00', '2017-09-13 17:05:01'),
(1242, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-13 18:28:28', '2017-09-13 18:28:29'),
(1243, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-13 20:11:29', '2017-09-13 20:11:31'),
(1244, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-14 13:37:14', '2017-09-14 13:37:16'),
(1245, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-09-14 14:01:23', '2017-09-14 14:01:25'),
(1246, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-09-14 14:04:32', '2017-09-14 14:04:34'),
(1247, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-09-14 14:39:34', '2017-09-14 14:39:36'),
(1248, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-14 16:07:38', '2017-09-14 16:07:40'),
(1249, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-14 16:11:07', '2017-09-14 16:11:08'),
(1250, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-09-14 17:30:38', '2017-09-14 17:30:40'),
(1251, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-09-14 20:01:53', '2017-09-14 20:01:55'),
(1252, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-15 11:37:52', '2017-09-15 11:37:53'),
(1253, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-15 11:38:03', '2017-09-15 11:38:05'),
(1254, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-15 14:18:02', '2017-09-15 14:18:04'),
(1255, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-15 14:54:00', '2017-09-15 14:54:02'),
(1256, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-15 15:25:19', '2017-09-15 15:25:21'),
(1257, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-09-15 16:28:01', '2017-09-15 16:28:03'),
(1258, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-09-15 16:35:16', '2017-09-15 16:35:19'),
(1259, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-15 19:34:43', '2017-09-15 19:34:44'),
(1260, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-09-16 13:29:10', '2017-09-16 13:29:12'),
(1261, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-17 12:48:34', '2017-09-17 12:48:37'),
(1262, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-17 12:49:24', '2017-09-17 12:49:27'),
(1263, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-18 10:56:42', '2017-09-18 10:56:47'),
(1264, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-18 11:29:50', '2017-09-18 11:29:53'),
(1265, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-18 11:42:03', '2017-09-18 11:42:05'),
(1266, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-09-18 15:19:29', '2017-09-18 15:19:32'),
(1267, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-18 19:54:30', '2017-09-18 19:54:33'),
(1268, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-18 19:55:41', '2017-09-18 19:55:43'),
(1269, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-09-19 17:40:06', '2017-09-19 17:40:08'),
(1270, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-19 18:13:56', '2017-09-19 18:13:58'),
(1271, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-09-19 19:14:49', '2017-09-19 19:14:50'),
(1272, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-09-19 19:41:47', '2017-09-19 19:41:49'),
(1273, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-09-19 21:02:19', '2017-09-19 21:02:21'),
(1274, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-09-20 10:39:15', '2017-09-20 10:39:18'),
(1275, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 12:20:49', '2017-09-20 12:20:52'),
(1276, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 12:57:01', '2017-09-20 12:57:04'),
(1277, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 12:57:29', '2017-09-20 12:57:32'),
(1278, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 12:58:06', '2017-09-20 12:58:09'),
(1279, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 14:58:25', '2017-09-20 14:58:28'),
(1280, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 15:04:12', '2017-09-20 15:04:14'),
(1281, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 15:04:40', '2017-09-20 15:04:42'),
(1282, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 15:05:14', '2017-09-20 15:05:16'),
(1283, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 15:07:45', '2017-09-20 15:07:48'),
(1284, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-20 15:51:08', '2017-09-20 15:51:11'),
(1285, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-09-20 20:41:34', '2017-09-20 20:41:38'),
(1286, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-09-22 09:34:33', '2017-09-22 09:34:33'),
(1287, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-09-22 10:06:26', '2017-09-22 10:06:26'),
(1288, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-22 19:10:03', '2017-09-22 19:10:03'),
(1289, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-09-25 18:24:22', '2017-09-25 18:24:24'),
(1290, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-09-25 18:36:44', '2017-09-25 18:36:45'),
(1291, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-09-25 19:43:51', '2017-09-25 19:43:52'),
(1292, 26, 'Gigi', 'G 004', 'drg. Brian Merchantara Winato', '2017-09-25 21:08:59', '2017-09-25 21:09:00'),
(1293, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-02 11:42:25', '2017-10-02 11:42:29'),
(1294, 27, 'Cardio', 'C 003', 'dr Anggia Lubis Sp.JP', '2017-09-26 19:17:58', '2017-10-02 11:42:30'),
(1295, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-09-26 18:59:59', '2017-10-02 11:42:31'),
(1296, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-09-26 17:12:47', '2017-10-02 11:42:31'),
(1297, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-26 18:09:06', '2017-10-02 11:42:32'),
(1298, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-09-26 18:33:36', '2017-10-02 11:42:33'),
(1299, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-09-26 17:53:27', '2017-10-02 11:42:33'),
(1300, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-27 11:22:25', '2017-10-02 11:42:34'),
(1301, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-27 11:25:21', '2017-10-02 11:42:35'),
(1302, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-09-27 11:27:59', '2017-10-02 11:42:35'),
(1303, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-02 13:50:36', '2017-10-02 13:50:38'),
(1304, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-27 11:50:44', '2017-10-02 13:50:39'),
(1305, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-27 14:57:02', '2017-10-02 13:50:39'),
(1306, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-27 14:03:38', '2017-10-02 13:50:40'),
(1307, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-09-27 21:55:10', '2017-10-02 13:50:41'),
(1308, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-09-28 10:41:04', '2017-10-02 13:50:41'),
(1309, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-09-28 16:32:40', '2017-10-02 13:50:42'),
(1310, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-29 14:13:41', '2017-10-02 13:50:43'),
(1311, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-09-29 14:14:06', '2017-10-02 13:50:43'),
(1312, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-09-29 16:39:16', '2017-10-02 13:50:44'),
(1313, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-02 13:52:27', '2017-10-02 13:52:30'),
(1314, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-09-29 17:24:56', '2017-10-02 13:52:30'),
(1315, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-09-29 18:27:26', '2017-10-02 13:52:31'),
(1316, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-09-29 18:36:20', '2017-10-02 13:52:32'),
(1317, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-29 19:22:50', '2017-10-02 13:52:32'),
(1318, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-29 19:46:21', '2017-10-02 13:52:33'),
(1319, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-09-29 20:40:26', '2017-10-02 13:52:34'),
(1320, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-09-30 11:25:57', '2017-10-02 13:52:34'),
(1321, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-30 12:26:17', '2017-10-02 13:52:35'),
(1322, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-30 12:26:40', '2017-10-02 13:52:36'),
(1323, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-02 13:53:25', '2017-10-02 13:53:27'),
(1324, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-09-30 13:35:35', '2017-10-02 13:53:28'),
(1325, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-10-02 09:18:41', '2017-10-02 13:53:28'),
(1326, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-02 13:54:27', '2017-10-02 13:54:30'),
(1327, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-02 13:56:13', '2017-10-02 13:56:15'),
(1328, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-02 14:48:40', '2017-10-02 14:48:42'),
(1329, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-02 17:47:25', '2017-10-02 17:47:29'),
(1330, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-02 18:54:17', '2017-10-02 18:54:19'),
(1331, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-04 14:28:14', '2017-10-04 14:28:17'),
(1332, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-10-04 15:46:32', '2017-10-04 15:46:34'),
(1333, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-04 17:52:39', '2017-10-04 17:52:42'),
(1334, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-10-04 18:29:05', '2017-10-04 18:29:08'),
(1335, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-10-04 20:47:11', '2017-10-04 20:47:13'),
(1336, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-05 10:55:00', '2017-10-05 10:55:01'),
(1337, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-10-05 16:14:30', '2017-10-05 16:14:30'),
(1338, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-05 19:46:28', '2017-10-05 19:46:27'),
(1339, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:15:36', '2017-10-06 12:15:36'),
(1340, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:26:18', '2017-10-06 12:26:18'),
(1341, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:27:04', '2017-10-06 12:27:04'),
(1342, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:30:26', '2017-10-06 12:30:25'),
(1343, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:31:25', '2017-10-06 12:31:25'),
(1344, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:36:24', '2017-10-06 12:36:24'),
(1345, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:37:24', '2017-10-06 12:37:24'),
(1346, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:37:48', '2017-10-06 12:37:48'),
(1347, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:39:31', '2017-10-06 12:39:30'),
(1348, 11, 'Estetika', 'K 012', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 12:52:28', '2017-10-06 12:52:28'),
(1349, 11, 'Estetika', 'K 011', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 14:48:16', '2017-10-06 14:48:16'),
(1350, 11, 'Estetika', 'K 013', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-06 15:23:08', '2017-10-06 15:23:07'),
(1351, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-10-07 13:30:11', '2017-10-07 13:30:11'),
(1352, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-09 16:16:41', '2017-10-09 16:16:43'),
(1353, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-09 16:37:26', '2017-10-09 16:37:27'),
(1354, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-09 17:40:15', '2017-10-09 17:40:15'),
(1355, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-10-09 18:05:02', '2017-10-09 18:05:02'),
(1356, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-09 18:22:40', '2017-10-09 18:22:41'),
(1357, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-09 19:16:18', '2017-10-09 19:16:19'),
(1358, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-09 18:47:37', '2017-10-09 19:16:19'),
(1359, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-10-09 19:31:32', '2017-10-09 19:31:32'),
(1360, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-09 19:44:24', '2017-10-09 19:44:24'),
(1361, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-10 13:12:35', '2017-10-10 13:12:36'),
(1362, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-10 13:23:11', '2017-10-10 13:23:12'),
(1363, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-10 13:46:22', '2017-10-10 13:46:23'),
(1364, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-10 13:51:57', '2017-10-10 13:51:58'),
(1365, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-10 17:37:00', '2017-10-10 17:37:01'),
(1366, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-10 17:54:29', '2017-10-10 17:54:30'),
(1367, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-10 18:00:35', '2017-10-10 18:00:36'),
(1368, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-10 18:41:28', '2017-10-10 18:41:29'),
(1369, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-10-11 20:32:13', '2017-10-11 20:32:16'),
(1370, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-12 12:43:01', '2017-10-12 12:43:03'),
(1371, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-10-12 15:07:02', '2017-10-12 15:07:03'),
(1372, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-12 15:32:31', '2017-10-12 15:32:33'),
(1373, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-10-12 16:59:30', '2017-10-12 16:59:32'),
(1374, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-10-12 17:25:32', '2017-10-12 17:25:34'),
(1375, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-13 18:02:08', '2017-10-13 18:02:10'),
(1376, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-13 18:57:40', '2017-10-13 18:57:41'),
(1377, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-13 19:47:38', '2017-10-13 19:47:40'),
(1378, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-10-13 19:54:09', '2017-10-13 19:54:11'),
(1379, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-14 15:13:54', '2017-10-14 15:13:57'),
(1380, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-14 15:37:09', '2017-10-14 15:37:12'),
(1381, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-16 10:29:07', '2017-10-16 10:29:10'),
(1382, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-16 13:19:26', '2017-10-16 13:19:30'),
(1383, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-16 17:18:52', '2017-10-16 17:18:56'),
(1384, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-16 19:25:57', '2017-10-16 19:26:01'),
(1385, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-10-17 15:11:08', '2017-10-17 15:11:12'),
(1386, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-10-17 17:48:33', '2017-10-17 17:48:36'),
(1387, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-10-17 17:51:30', '2017-10-17 17:51:33'),
(1388, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-10-17 17:55:10', '2017-10-17 17:55:13'),
(1389, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-17 18:26:16', '2017-10-17 18:26:19'),
(1390, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-10-17 19:13:31', '2017-10-17 19:13:33'),
(1391, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-10-17 19:17:03', '2017-10-17 19:17:06'),
(1392, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-17 19:35:37', '2017-10-17 19:35:40'),
(1393, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-10-17 19:46:33', '2017-10-17 19:46:36'),
(1394, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-18 15:06:56', '2017-10-18 15:06:59'),
(1395, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-18 15:07:23', '2017-10-18 15:07:26'),
(1396, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-10-18 16:37:02', '2017-10-18 16:37:05'),
(1397, 27, 'Cardio', 'C 002', 'dr. Yuke Sarastri Sp.JP', '2017-10-18 17:05:56', '2017-10-18 17:05:59'),
(1398, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-10-18 20:32:04', '2017-10-18 20:32:07'),
(1399, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-19 14:51:51', '2017-10-19 14:51:51'),
(1400, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-10-19 15:05:42', '2017-10-19 15:05:42'),
(1401, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-19 19:24:05', '2017-10-20 08:10:35'),
(1402, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-10-20 21:42:17', '2017-10-20 21:42:18'),
(1403, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-10-20 21:46:55', '2017-10-20 21:46:55'),
(1404, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-10-20 21:46:05', '2017-10-20 21:46:56'),
(1405, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-10-23 09:00:45', '2017-10-23 09:00:47'),
(1406, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-23 18:20:54', '2017-10-23 18:20:55'),
(1407, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-23 18:36:51', '2017-10-23 18:36:51'),
(1408, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-24 11:42:52', '2017-10-24 11:42:52'),
(1409, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-24 11:50:27', '2017-10-24 11:50:27'),
(1410, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-24 11:56:39', '2017-10-24 11:56:40'),
(1411, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-24 11:57:46', '2017-10-24 11:57:47'),
(1412, 26, 'Gigi', 'G 001', 'drg Rizky Vira Novella', '2017-10-24 12:04:59', '2017-10-24 12:05:00'),
(1413, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-24 13:41:29', '2017-10-24 13:41:29'),
(1414, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-10-24 14:12:11', '2017-10-24 14:12:12'),
(1415, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-10-24 16:45:34', '2017-10-24 16:45:35'),
(1416, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-10-24 19:13:48', '2017-10-24 19:13:48'),
(1417, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-25 17:36:35', '2017-10-25 17:36:37'),
(1418, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-10-26 12:35:45', '2017-10-26 12:35:47'),
(1419, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-10-26 20:06:43', '2017-10-26 20:06:44'),
(1420, 26, 'Gigi', 'G 001', 'drg Rizky Vira Novella', '2017-10-27 12:30:51', '2017-10-27 12:30:52'),
(1421, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-10-27 13:10:20', '2017-10-27 13:10:21'),
(1422, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-27 13:23:59', '2017-10-27 13:24:00'),
(1423, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-27 13:31:58', '2017-10-27 13:31:59'),
(1424, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-10-27 17:50:26', '2017-10-27 17:50:28'),
(1425, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-30 08:32:02', '2017-10-30 08:32:05'),
(1426, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-30 12:24:41', '2017-10-30 12:24:44'),
(1427, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-30 16:02:33', '2017-10-30 16:02:36'),
(1428, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-10-30 17:36:35', '2017-10-30 17:36:38'),
(1429, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-10-30 18:22:52', '2017-10-30 18:22:54'),
(1430, 26, 'Gigi', 'G 002', 'drg. Brian Merchantara Winato', '2017-10-30 20:44:34', '2017-10-30 20:44:36'),
(1431, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-10-31 12:03:18', '2017-10-31 12:03:21'),
(1432, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-10-31 15:55:29', '2017-10-31 15:55:32'),
(1433, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-10-31 17:41:51', '2017-10-31 17:41:53'),
(1434, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-10-31 17:56:21', '2017-10-31 17:56:24'),
(1435, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-11-01 19:14:34', '2017-11-01 19:14:37'),
(1436, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-01 20:09:48', '2017-11-01 20:09:51'),
(1437, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-02 12:10:09', '2017-11-02 12:10:10'),
(1438, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-02 19:33:12', '2017-11-02 19:33:12'),
(1439, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-02 19:33:44', '2017-11-02 19:33:44'),
(1440, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-02 19:34:11', '2017-11-02 19:34:10'),
(1441, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-11-03 08:52:40', '2017-11-03 08:52:42'),
(1442, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-03 11:50:21', '2017-11-03 11:50:22'),
(1443, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-11-03 17:07:28', '2017-11-03 17:07:29'),
(1444, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-03 17:59:45', '2017-11-03 17:59:46'),
(1445, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-11-03 19:15:58', '2017-11-03 19:15:59'),
(1446, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-03 19:17:30', '2017-11-03 19:17:31'),
(1447, 27, 'Cardio', 'C 003', 'dr Anggia Lubis Sp.JP', '2017-11-03 19:49:53', '2017-11-03 19:49:53'),
(1448, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-11-04 11:30:58', '2017-11-04 11:30:59'),
(1449, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-04 12:44:46', '2017-11-04 12:44:47'),
(1450, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-04 12:46:47', '2017-11-04 12:46:48'),
(1451, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-04 12:49:30', '2017-11-04 12:49:31'),
(1452, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-04 13:17:49', '2017-11-04 13:17:50'),
(1453, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-04 14:14:24', '2017-11-04 14:14:25');
INSERT INTO `sys_schedule` (`schedule_id`, `schedule_service_id`, `schedule_service_name`, `schedule_queue_value`, `schedule_dokter_name`, `schedule_datetime`, `schedule_sync_datetime`) VALUES
(1454, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-04 14:17:15', '2017-11-04 14:17:16'),
(1455, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-11-06 09:27:17', '2017-11-06 09:27:20'),
(1456, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-06 13:17:43', '2017-11-06 13:17:46'),
(1457, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-06 13:33:44', '2017-11-06 13:33:46'),
(1458, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-06 13:44:40', '2017-11-06 13:44:42'),
(1459, 26, 'Gigi', 'G 003', 'drg Rizky Vira Novella', '2017-11-06 13:47:54', '2017-11-06 13:47:56'),
(1460, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-06 16:24:53', '2017-11-06 16:24:56'),
(1461, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-06 17:18:03', '2017-11-06 17:18:05'),
(1462, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-06 17:22:21', '2017-11-06 17:22:24'),
(1463, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-06 18:16:14', '2017-11-06 18:16:17'),
(1464, 24, 'THT', 'T 003', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-06 19:59:52', '2017-11-06 19:59:55'),
(1465, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-07 14:13:44', '2017-11-07 14:13:47'),
(1466, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-07 15:42:03', '2017-11-07 15:42:05'),
(1467, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2017-11-07 18:06:33', '2017-11-07 18:06:35'),
(1468, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-11-07 19:35:00', '2017-11-07 19:35:03'),
(1469, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-08 13:01:18', '2017-11-08 13:01:21'),
(1470, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-08 15:03:31', '2017-11-08 15:03:33'),
(1471, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-11-08 15:57:33', '2017-11-08 15:57:36'),
(1472, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-08 16:56:59', '2017-11-08 16:57:02'),
(1473, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-08 16:57:11', '2017-11-08 16:57:13'),
(1474, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-08 19:18:37', '2017-11-08 19:18:40'),
(1475, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-11-08 19:43:18', '2017-11-08 19:43:21'),
(1476, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-11-09 13:21:13', '2017-11-09 13:21:13'),
(1477, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-09 14:34:09', '2017-11-09 14:34:09'),
(1478, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-09 14:46:34', '2017-11-09 14:46:34'),
(1479, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-09 18:14:46', '2017-11-09 18:14:46'),
(1480, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-09 18:18:02', '2017-11-09 18:18:03'),
(1481, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-09 19:34:37', '2017-11-09 19:34:37'),
(1482, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-09 19:34:55', '2017-11-09 19:34:55'),
(1483, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-10 09:28:29', '2017-11-10 09:28:30'),
(1484, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-10 13:16:25', '2017-11-10 13:16:25'),
(1485, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-10 16:22:17', '2017-11-10 16:22:19'),
(1486, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-10 17:02:23', '2017-11-10 17:02:24'),
(1487, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-11-10 19:12:04', '2017-11-10 19:12:04'),
(1488, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-11 14:27:25', '2017-11-11 14:27:26'),
(1489, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-11 14:28:18', '2017-11-11 14:28:19'),
(1490, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-11 14:50:57', '2017-11-11 14:50:58'),
(1491, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-11 16:15:23', '2017-11-11 16:15:24'),
(1492, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-13 13:33:50', '2017-11-13 13:33:52'),
(1493, 26, 'Gigi', 'G 001', 'drg Rizky Vira Novella', '2017-11-13 13:59:29', '2017-11-13 13:59:30'),
(1494, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-13 14:01:48', '2017-11-13 14:01:49'),
(1495, 14, 'Penyakit Dalam', 'D 001', 'dr. Dhini Silvana Sp.PD', '2017-11-13 15:24:24', '2017-11-13 15:24:26'),
(1496, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-13 15:49:24', '2017-11-13 15:49:26'),
(1497, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-13 16:27:26', '2017-11-13 16:27:27'),
(1498, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-11-13 16:32:23', '2017-11-13 16:32:25'),
(1499, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-11-13 16:57:05', '2017-11-13 16:57:06'),
(1500, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-11-13 18:56:00', '2017-11-13 18:56:01'),
(1501, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-14 14:37:02', '2017-11-14 14:37:05'),
(1502, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-14 16:43:03', '2017-11-14 16:43:05'),
(1503, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-14 18:20:04', '2017-11-14 18:20:06'),
(1504, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-14 18:27:33', '2017-11-14 18:27:36'),
(1505, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-14 19:39:31', '2017-11-14 19:39:34'),
(1506, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-14 20:32:33', '2017-11-14 20:32:35'),
(1507, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-15 16:05:22', '2017-11-15 16:05:25'),
(1508, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-11-15 19:33:51', '2017-11-15 19:33:53'),
(1509, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-16 13:25:01', '2017-11-16 13:25:02'),
(1510, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-16 15:05:26', '2017-11-16 15:05:26'),
(1511, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-16 15:05:50', '2017-11-16 15:05:50'),
(1512, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-16 15:07:36', '2017-11-16 15:07:36'),
(1513, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-16 16:36:29', '2017-11-16 16:36:30'),
(1514, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-16 18:29:48', '2017-11-16 18:29:49'),
(1515, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-16 19:44:51', '2017-11-16 19:44:51'),
(1516, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-11-16 20:05:01', '2017-11-16 20:05:01'),
(1517, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-11-17 08:57:44', '2017-11-17 08:57:46'),
(1518, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-17 09:18:50', '2017-11-17 09:18:51'),
(1519, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-11-17 10:00:20', '2017-11-17 10:00:21'),
(1520, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-17 12:23:52', '2017-11-17 12:23:53'),
(1521, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-17 12:51:40', '2017-11-17 12:51:41'),
(1522, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-18 14:07:12', '2017-11-18 14:07:13'),
(1523, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-18 14:18:32', '2017-11-18 14:18:32'),
(1524, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-18 14:29:17', '2017-11-18 14:29:17'),
(1525, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-18 14:30:02', '2017-11-18 14:30:03'),
(1526, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-18 14:30:17', '2017-11-18 14:30:18'),
(1527, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-18 14:44:26', '2017-11-18 14:44:27'),
(1528, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-11-20 09:47:09', '2017-11-20 09:47:12'),
(1529, 26, 'Gigi', 'G 001', 'drg Rizky Vira Novella', '2017-11-20 11:29:35', '2017-11-20 11:29:35'),
(1530, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-20 12:43:40', '2017-11-20 12:43:41'),
(1531, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-20 12:44:48', '2017-11-20 12:44:48'),
(1532, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-20 15:48:03', '2017-11-20 15:48:04'),
(1533, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-20 16:11:01', '2017-11-20 16:11:02'),
(1534, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-11-20 17:16:09', '2017-11-20 17:16:10'),
(1535, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-20 17:29:32', '2017-11-20 17:29:32'),
(1536, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-11-20 18:00:36', '2017-11-20 18:00:36'),
(1537, 26, 'Gigi', 'G 003', 'drg. Brian Merchantara Winato', '2017-11-20 20:17:30', '2017-11-20 20:17:30'),
(1538, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-21 17:38:01', '2017-11-21 17:38:01'),
(1539, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-21 19:17:10', '2017-11-21 19:17:10'),
(1540, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-11-22 09:54:25', '2017-11-22 09:54:26'),
(1541, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-22 13:03:52', '2017-11-22 13:03:52'),
(1542, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-22 13:59:13', '2017-11-22 13:59:14'),
(1543, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-22 15:30:35', '2017-11-22 15:30:36'),
(1544, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-22 17:02:49', '2017-11-22 17:02:48'),
(1545, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-11-22 18:47:04', '2017-11-22 18:47:04'),
(1546, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-11-22 20:00:17', '2017-11-22 20:00:17'),
(1547, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-23 12:03:11', '2017-11-23 12:03:12'),
(1548, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-11-23 14:48:20', '2017-11-23 14:48:21'),
(1549, 26, 'Gigi', 'G 004', 'drg. Brian Merchantara Winato', '2017-11-23 21:22:38', '2017-11-23 21:22:39'),
(1550, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-24 11:45:47', '2017-11-24 11:45:48'),
(1551, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-24 11:46:08', '2017-11-24 11:46:09'),
(1552, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-24 14:54:26', '2017-11-24 14:54:27'),
(1553, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-24 14:54:56', '2017-11-24 14:54:57'),
(1554, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-24 15:49:01', '2017-11-24 15:49:03'),
(1555, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-24 18:30:23', '2017-11-24 18:30:25'),
(1556, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-24 18:59:39', '2017-11-24 18:59:40'),
(1557, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-25 12:26:27', '2017-11-25 12:26:28'),
(1558, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-25 15:17:37', '2017-11-25 15:17:38'),
(1559, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-25 20:27:29', '2017-11-25 20:27:30'),
(1560, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-27 12:28:50', '2017-11-27 12:28:53'),
(1561, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-27 13:15:59', '2017-11-27 13:16:02'),
(1562, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-27 13:39:41', '2017-11-27 13:39:44'),
(1563, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-27 14:23:09', '2017-11-27 14:23:12'),
(1564, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-27 14:24:11', '2017-11-27 14:24:13'),
(1565, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-27 14:24:38', '2017-11-27 14:24:41'),
(1566, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-27 15:46:54', '2017-11-27 15:46:57'),
(1567, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-27 18:46:21', '2017-11-27 18:46:24'),
(1568, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-11-27 18:51:26', '2017-11-27 18:51:29'),
(1569, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-27 19:09:32', '2017-11-27 19:09:35'),
(1570, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-28 12:42:00', '2017-11-28 12:42:03'),
(1571, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-28 12:44:40', '2017-11-28 12:44:42'),
(1572, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-28 13:00:52', '2017-11-28 13:00:55'),
(1573, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-28 14:40:38', '2017-11-28 14:40:41'),
(1574, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-28 16:39:07', '2017-11-28 16:39:10'),
(1575, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-28 16:39:52', '2017-11-28 16:39:55'),
(1576, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-28 17:40:31', '2017-11-28 17:40:33'),
(1577, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-29 12:10:53', '2017-11-29 12:10:58'),
(1578, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-29 13:19:20', '2017-11-29 13:19:24'),
(1579, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-29 13:31:38', '2017-11-29 13:31:41'),
(1580, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-29 15:28:31', '2017-11-29 15:28:34'),
(1581, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-29 15:28:57', '2017-11-29 15:29:00'),
(1582, 26, 'Gigi', 'G 001', 'drg. Brian Merchantara Winato', '2017-11-29 18:04:13', '2017-11-29 18:04:16'),
(1583, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-29 19:25:49', '2017-11-29 19:25:53'),
(1584, 12, 'Obgyn', 'O 001', 'dr. Yudha Sudewo Sp.OG', '2017-11-30 10:31:47', '2017-11-30 10:31:47'),
(1585, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-11-30 11:52:36', '2017-11-30 11:52:36'),
(1586, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-11-30 14:45:53', '2017-11-30 14:45:55'),
(1587, 12, 'Obgyn', 'O 002', 'dr Ray C Barus Sp.OG', '2017-11-30 14:52:33', '2017-11-30 14:52:34'),
(1588, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-11-30 14:56:35', '2017-11-30 14:56:35'),
(1589, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-11-30 19:32:37', '2017-11-30 19:32:38'),
(1590, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-02 14:12:28', '2017-12-02 14:12:30'),
(1591, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-12-02 14:13:51', '2017-12-02 14:13:52'),
(1592, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-02 17:19:02', '2017-12-02 17:19:02'),
(1593, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-11 19:09:53', '2017-12-18 08:55:04'),
(1594, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-15 16:08:01', '2017-12-18 08:55:05'),
(1595, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 18:57:21', '2017-12-18 08:55:06'),
(1596, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-12-18 08:55:02', '2017-12-18 08:55:07'),
(1597, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 15:58:26', '2017-12-18 08:55:07'),
(1598, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-12 14:03:11', '2017-12-18 08:55:08'),
(1599, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-03 11:51:32', '2017-12-18 08:55:09'),
(1600, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-03 11:54:41', '2017-12-18 08:55:10'),
(1601, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-12-04 09:32:42', '2017-12-18 08:55:10'),
(1602, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-04 10:20:27', '2017-12-18 08:55:11'),
(1603, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-04 10:20:46', '2017-12-18 08:55:12'),
(1604, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-04 10:19:36', '2017-12-18 08:55:13'),
(1605, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-04 11:48:31', '2017-12-18 08:55:14'),
(1606, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-04 14:56:07', '2017-12-18 08:55:15'),
(1607, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-04 14:57:38', '2017-12-18 08:55:16'),
(1608, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-12-04 17:15:36', '2017-12-18 08:55:16'),
(1609, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-04 18:27:30', '2017-12-18 08:55:17'),
(1610, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-12-04 18:37:51', '2017-12-18 08:55:18'),
(1611, 12, 'Obgyn', 'O 002', 'dr Ray C Barus Sp.OG', '2017-12-04 17:34:40', '2017-12-18 08:55:19'),
(1612, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-05 13:49:54', '2017-12-18 08:55:20'),
(1613, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-05 14:00:44', '2017-12-18 09:01:23'),
(1614, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-05 16:14:57', '2017-12-18 09:01:23'),
(1615, 26, 'Gigi', 'G 002', 'drg Wira Agustina', '2017-12-05 17:22:52', '2017-12-18 09:01:24'),
(1616, 26, 'Gigi', 'G 003', 'drg Wira Agustina', '2017-12-05 17:24:04', '2017-12-18 09:01:25'),
(1617, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-12-05 17:15:00', '2017-12-18 09:01:25'),
(1618, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-12-06 14:26:11', '2017-12-18 09:01:26'),
(1619, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-06 15:48:17', '2017-12-18 09:01:27'),
(1620, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-06 15:47:14', '2017-12-18 09:01:28'),
(1621, 24, 'THT', 'T 001', 'dr Farrel M.Ked(ORL-HNS), Sp.THT-KL', '2017-12-06 18:01:46', '2017-12-18 09:01:29'),
(1622, 27, 'Cardio', 'C 002', 'dr Anggia Lubis Sp.JP', '2017-12-06 17:21:21', '2017-12-18 09:01:29'),
(1623, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-06 18:33:42', '2017-12-18 09:15:13'),
(1624, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-06 18:40:45', '2017-12-18 09:15:14'),
(1625, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-07 12:49:20', '2017-12-18 09:15:14'),
(1626, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-07 18:02:24', '2017-12-18 09:15:15'),
(1627, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-07 18:06:20', '2017-12-18 09:15:16'),
(1628, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-07 19:05:24', '2017-12-18 09:15:16'),
(1629, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-07 18:04:32', '2017-12-18 09:15:17'),
(1630, 25, 'Neurologi', 'N 001', 'Prof. Dr. dr. Hasan Sjahrir Sp.S(K)', '2017-12-08 09:18:29', '2017-12-18 09:15:18'),
(1631, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-08 09:51:35', '2017-12-18 09:15:19'),
(1632, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-08 12:42:26', '2017-12-18 09:15:20'),
(1633, 27, 'Cardio', 'C 001', 'dr. Joy Wulansari Purba Sp.JP', '2017-12-08 12:12:04', '2017-12-18 09:25:00'),
(1634, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-08 12:42:59', '2017-12-18 09:25:01'),
(1635, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-08 13:00:53', '2017-12-18 09:25:02'),
(1636, 26, 'Gigi', 'G 005', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-08 14:53:22', '2017-12-18 09:25:03'),
(1637, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-08 14:56:25', '2017-12-18 09:25:04'),
(1638, 26, 'Gigi', 'G 006', 'drg Wira Agustina', '2017-12-08 15:54:42', '2017-12-18 09:25:04'),
(1639, 26, 'Gigi', 'G 008', 'drg. Brian Merchantara Winato', '2017-12-08 21:13:08', '2017-12-18 09:25:05'),
(1640, 26, 'Gigi', 'G 007', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-08 21:16:29', '2017-12-18 09:25:06'),
(1641, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-09 11:56:49', '2017-12-18 09:25:07'),
(1642, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-11 14:05:21', '2017-12-18 09:25:07'),
(1643, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-11 14:05:58', '2017-12-18 09:26:01'),
(1644, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-11 14:06:26', '2017-12-18 09:26:04'),
(1645, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-11 14:04:20', '2017-12-18 09:26:04'),
(1646, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-11 16:32:23', '2017-12-18 09:26:05'),
(1647, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-12-11 17:29:36', '2017-12-18 09:26:06'),
(1648, 12, 'Obgyn', 'O 002', 'dr Ray C Barus Sp.OG', '2017-12-11 17:05:26', '2017-12-18 09:26:07'),
(1649, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-12-11 18:44:14', '2017-12-18 09:26:08'),
(1650, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-11 19:34:35', '2017-12-18 09:26:08'),
(1651, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-12 10:07:48', '2017-12-18 09:26:09'),
(1652, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-12 11:27:50', '2017-12-18 09:26:10'),
(1653, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-12 14:32:57', '2017-12-18 09:26:19'),
(1654, 26, 'Gigi', 'G 001', 'drg Wira Agustina', '2017-12-12 15:22:20', '2017-12-18 09:26:20'),
(1655, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-12 16:42:49', '2017-12-18 09:26:21'),
(1656, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-12-12 17:24:46', '2017-12-18 09:26:22'),
(1657, 28, 'Laboratorium', '003', 'Viki Dwi Setyantoro', '2017-12-12 17:27:19', '2017-12-18 09:26:23'),
(1658, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 14:41:34', '2017-12-18 09:26:23'),
(1659, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 12:27:29', '2017-12-18 09:26:24'),
(1660, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 15:22:22', '2017-12-18 09:26:25'),
(1661, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-12-13 17:15:03', '2017-12-18 09:26:26'),
(1662, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 17:32:43', '2017-12-18 09:26:26'),
(1663, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 18:53:57', '2017-12-18 09:26:34'),
(1664, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 18:54:48', '2017-12-18 09:26:35'),
(1665, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 18:56:34', '2017-12-18 09:26:35'),
(1666, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-13 19:16:05', '2017-12-18 09:26:36'),
(1667, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-13 17:42:47', '2017-12-18 09:26:37'),
(1668, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-12 14:42:29', '2017-12-18 09:26:38'),
(1669, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-14 15:01:58', '2017-12-18 09:26:38'),
(1670, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-14 18:36:59', '2017-12-18 09:26:39'),
(1671, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-14 17:29:27', '2017-12-18 09:26:40'),
(1672, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-14 18:29:48', '2017-12-18 09:26:41'),
(1673, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-12-14 19:45:58', '2017-12-18 09:26:46'),
(1674, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-15 13:44:30', '2017-12-18 09:26:47'),
(1675, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-15 13:44:58', '2017-12-18 09:26:48'),
(1676, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-15 13:45:44', '2017-12-18 09:26:49'),
(1677, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-15 15:55:57', '2017-12-18 09:26:49'),
(1678, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-15 19:51:22', '2017-12-18 09:26:50'),
(1679, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-15 19:51:48', '2017-12-18 09:26:51'),
(1680, 26, 'Gigi', 'G 001', 'drg Wira Agustina', '2017-12-18 11:17:47', '2017-12-18 11:17:49'),
(1681, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-18 11:42:39', '2017-12-18 11:42:41'),
(1682, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-18 14:44:37', '2017-12-18 14:44:39'),
(1683, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-18 14:49:14', '2017-12-18 14:49:15'),
(1684, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-18 15:28:42', '2017-12-18 15:28:44'),
(1685, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-18 16:28:26', '2017-12-18 16:28:28'),
(1686, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-18 18:55:40', '2017-12-18 18:55:42'),
(1687, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-18 19:31:50', '2017-12-18 19:31:52'),
(1688, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-18 19:58:13', '2017-12-18 19:58:14'),
(1689, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 11:41:54', '2017-12-19 11:41:57'),
(1690, 26, 'Gigi', 'G 001', 'drg Wira Agustina', '2017-12-19 12:47:37', '2017-12-19 12:47:39'),
(1691, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 13:01:40', '2017-12-19 13:01:43'),
(1692, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 13:05:46', '2017-12-19 13:05:48'),
(1693, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 13:07:08', '2017-12-19 13:07:11'),
(1694, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 13:09:40', '2017-12-19 13:09:42'),
(1695, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 13:34:34', '2017-12-19 13:34:36'),
(1696, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 13:56:01', '2017-12-19 13:56:03'),
(1697, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-19 14:30:55', '2017-12-19 14:30:57'),
(1698, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-19 17:14:07', '2017-12-19 17:14:10'),
(1699, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-12-19 18:10:44', '2017-12-19 18:10:46'),
(1700, 26, 'Gigi', 'G 004', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-19 18:35:44', '2017-12-19 18:35:47'),
(1701, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-20 10:12:36', '2017-12-20 10:12:55'),
(1702, 26, 'Gigi', 'G 005', 'drg. Brian Merchantara Winato', '2017-12-19 20:48:13', '2017-12-20 10:12:56'),
(1703, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-20 14:46:13', '2017-12-20 14:46:16'),
(1704, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-20 14:47:29', '2017-12-20 14:51:02'),
(1705, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-20 15:08:09', '2017-12-20 15:08:13'),
(1706, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-20 15:19:36', '2017-12-20 15:19:40'),
(1707, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-20 16:32:16', '2017-12-20 16:32:20'),
(1708, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-20 16:32:25', '2017-12-20 16:32:28'),
(1709, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-12-20 17:32:39', '2017-12-20 17:32:43'),
(1710, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-20 18:58:42', '2017-12-20 18:58:46'),
(1711, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-21 11:52:52', '2017-12-21 11:52:52'),
(1712, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-21 13:00:43', '2017-12-21 13:00:43'),
(1713, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-21 13:00:49', '2017-12-21 13:00:49'),
(1714, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-21 13:59:49', '2017-12-21 13:59:49'),
(1715, 26, 'Gigi', 'G 003', 'drg. Muhammad Aidil Nasution', '2017-12-21 15:48:59', '2017-12-21 15:48:59'),
(1716, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-21 18:49:49', '2017-12-21 18:49:48'),
(1717, 26, 'Gigi', 'G 002', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-21 18:52:38', '2017-12-21 18:52:38'),
(1718, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-22 11:43:59', '2017-12-22 11:44:00'),
(1719, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-22 14:41:13', '2017-12-22 14:41:14'),
(1720, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-22 14:42:28', '2017-12-22 14:42:29'),
(1721, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-22 14:42:30', '2017-12-22 14:42:31'),
(1722, 24, 'THT', 'T 001', 'dr. M. Fiza Fadly Siregar', '2017-12-22 15:28:45', '2017-12-22 15:28:46'),
(1723, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-22 15:49:59', '2017-12-22 15:50:01'),
(1724, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-22 16:05:54', '2017-12-22 16:05:55'),
(1725, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-22 16:06:01', '2017-12-22 16:06:02'),
(1726, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-22 16:09:50', '2017-12-22 16:09:51'),
(1727, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-23 14:49:15', '2017-12-23 14:49:17'),
(1728, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-26 11:43:05', '2017-12-26 11:43:07'),
(1729, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-26 11:50:42', '2017-12-26 11:50:45'),
(1730, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-26 12:04:49', '2017-12-26 12:04:52'),
(1731, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-26 12:14:51', '2017-12-26 12:14:54'),
(1732, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-26 13:25:22', '2017-12-26 13:25:24'),
(1733, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-26 13:57:01', '2017-12-26 13:57:03'),
(1734, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-12-27 10:51:57', '2017-12-27 10:52:00'),
(1735, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-27 12:00:59', '2017-12-27 12:01:02'),
(1736, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-12-27 12:50:10', '2017-12-27 12:50:13'),
(1737, 26, 'Gigi', 'G 001', 'drg Wira Agustina', '2017-12-27 14:31:41', '2017-12-27 14:31:44'),
(1738, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-27 16:00:01', '2017-12-27 16:00:04'),
(1739, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-27 16:10:13', '2017-12-27 16:10:16'),
(1740, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2017-12-27 16:26:44', '2017-12-27 16:26:46'),
(1741, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-27 16:37:55', '2017-12-27 16:37:58'),
(1742, 24, 'THT', 'T 002', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2017-12-27 17:07:56', '2017-12-27 17:07:58'),
(1743, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2017-12-27 17:26:40', '2017-12-27 17:26:43'),
(1744, 27, 'Cardio', 'C 002', 'dr. Yuke Sarastri Sp.JP', '2017-12-27 17:54:48', '2017-12-27 17:54:50'),
(1745, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-27 18:19:09', '2017-12-27 18:19:12'),
(1746, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-28 11:14:43', '2017-12-28 11:14:43'),
(1747, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-28 12:19:31', '2017-12-28 12:19:32'),
(1748, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-28 12:26:11', '2017-12-28 12:26:12'),
(1749, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-28 13:07:49', '2017-12-28 13:07:49'),
(1750, 24, 'THT', 'T 001', 'dr Farrel M.Ked(ORL-HNS), Sp.THT-KL', '2017-12-28 13:45:30', '2017-12-28 13:45:31'),
(1751, 24, 'THT', 'T 002', 'dr Farrel M.Ked(ORL-HNS), Sp.THT-KL', '2017-12-28 13:46:09', '2017-12-28 13:46:09'),
(1752, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-28 13:49:38', '2017-12-28 13:49:38'),
(1753, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-28 13:50:31', '2017-12-28 13:50:31'),
(1754, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-28 14:02:34', '2017-12-28 14:02:34'),
(1755, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-28 15:18:58', '2017-12-28 15:18:58'),
(1756, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-28 15:20:09', '2017-12-28 15:20:10'),
(1757, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-29 13:04:37', '2017-12-29 13:04:38'),
(1758, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-29 13:29:27', '2017-12-29 13:29:27'),
(1759, 26, 'Gigi', 'G 002', 'drg Wira Agustina', '2017-12-29 13:43:58', '2017-12-29 13:43:59'),
(1760, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-29 13:51:17', '2017-12-29 13:51:17'),
(1761, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-29 14:21:51', '2017-12-29 14:21:52'),
(1762, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2017-12-29 15:02:17', '2017-12-29 15:02:17'),
(1763, 28, 'Laboratorium', '002', 'Viki Dwi Setyantoro', '2017-12-29 15:09:44', '2017-12-29 15:09:44'),
(1764, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-29 15:46:32', '2017-12-29 15:46:33'),
(1765, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-12-29 17:07:42', '2017-12-29 17:07:43'),
(1766, 26, 'Gigi', 'G 003', 'drg. Aditya Rachmawati Sp.Ort', '2017-12-29 18:28:09', '2017-12-29 18:28:10'),
(1767, 26, 'Gigi', 'G 004', 'drg. Muhammad Aidil Nasution', '2017-12-29 18:49:23', '2017-12-29 18:49:24'),
(1768, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2017-12-30 12:06:07', '2017-12-30 12:06:08'),
(1769, 25, 'Neurologi', 'N 001', 'dr. R.A Dwi Pujiastuti Sp.S', '2017-12-30 12:17:13', '2017-12-30 12:17:14'),
(1770, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 12:36:10', '2017-12-30 12:36:11'),
(1771, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 12:42:44', '2017-12-30 12:42:45'),
(1772, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 13:29:10', '2017-12-30 13:29:11'),
(1773, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 13:34:25', '2017-12-30 13:34:26'),
(1774, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 13:39:05', '2017-12-30 13:39:06'),
(1775, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 13:40:57', '2017-12-30 13:40:58'),
(1776, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 13:42:37', '2017-12-30 13:42:38'),
(1777, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 13:42:43', '2017-12-30 13:42:44'),
(1778, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 13:42:47', '2017-12-30 13:42:48'),
(1779, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2017-12-30 14:32:16', '2017-12-30 14:32:17'),
(1780, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 14:52:39', '2017-12-30 14:52:40'),
(1781, 11, 'Estetika', 'K 012', 'dr. Putri Wulandari Dipl. AAAM(US)', '2017-12-30 16:10:14', '2017-12-30 16:10:15'),
(1782, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-02 12:29:55', '2018-01-02 12:29:58'),
(1783, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-02 13:40:02', '2018-01-02 13:40:04'),
(1784, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-02 15:14:43', '2018-01-02 15:14:45'),
(1785, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-02 15:40:03', '2018-01-02 15:40:05'),
(1786, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2018-01-02 16:00:48', '2018-01-02 16:00:50'),
(1787, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2018-01-02 16:12:09', '2018-01-02 16:12:11'),
(1788, 26, 'Gigi', 'G 002', 'drg. Muhammad Aidil Nasution', '2018-01-02 16:46:21', '2018-01-02 16:46:23'),
(1789, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2018-01-02 17:18:08', '2018-01-02 17:18:10'),
(1790, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-02 18:07:37', '2018-01-02 18:07:39'),
(1791, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-02 18:08:16', '2018-01-02 18:08:18'),
(1792, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-03 13:00:49', '2018-01-03 13:00:52'),
(1793, 12, 'Obgyn', 'O 001', 'dr Ray C Barus Sp.OG', '2018-01-03 16:06:32', '2018-01-03 16:06:34'),
(1794, 12, 'Obgyn', 'O 002', 'dr Ray C Barus Sp.OG', '2018-01-03 17:03:42', '2018-01-03 17:03:44'),
(1795, 27, 'Cardio', 'C 001', 'dr Anggia Lubis Sp.JP', '2018-01-03 18:08:50', '2018-01-03 18:08:52'),
(1796, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2018-01-03 18:46:24', '2018-01-03 18:46:26'),
(1797, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-04 14:21:11', '2018-01-04 14:21:11'),
(1798, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-04 15:41:26', '2018-01-04 15:41:26'),
(1799, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-04 15:41:37', '2018-01-04 15:41:37'),
(1800, 26, 'Gigi', 'G 001', 'drg. Aditya Rachmawati Sp.Ort', '2018-01-04 20:27:35', '2018-01-04 20:27:36'),
(1801, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 11:52:12', '2018-01-05 11:52:13'),
(1802, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 12:03:40', '2018-01-05 12:03:41'),
(1803, 11, 'Estetika', 'K 008', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 12:05:05', '2018-01-05 12:05:05'),
(1804, 11, 'Estetika', 'K 009', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 12:10:21', '2018-01-05 12:10:22'),
(1805, 11, 'Estetika', 'K 007', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 12:17:26', '2018-01-05 12:17:27'),
(1806, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 12:17:35', '2018-01-05 12:17:36'),
(1807, 11, 'Estetika', 'K 010', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 14:12:12', '2018-01-05 14:12:13'),
(1808, 27, 'Cardio', 'C 001', 'dr. Yuke Sarastri Sp.JP', '2018-01-05 14:53:07', '2018-01-05 14:53:08'),
(1809, 11, 'Estetika', 'K 011', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 16:25:50', '2018-01-05 16:25:51'),
(1810, 11, 'Estetika', 'K 012', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 17:24:05', '2018-01-05 17:24:06'),
(1811, 26, 'Gigi', 'G 001', 'drg. Muhammad Aidil Nasution', '2018-01-05 18:12:34', '2018-01-05 18:12:35'),
(1812, 11, 'Estetika', 'K 013', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 19:00:05', '2018-01-05 19:00:06'),
(1813, 11, 'Estetika', 'K 014', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 19:00:26', '2018-01-05 19:00:26'),
(1814, 11, 'Estetika', 'K 015', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-05 19:00:44', '2018-01-05 19:00:45'),
(1815, 24, 'THT', 'T 001', 'dr. Ashri Yudhistira Mked (ORL-HNS), Sp.THT-KL, FICS', '2018-01-05 19:20:04', '2018-01-05 19:20:04'),
(1816, 28, 'Laboratorium', '001', 'Viki Dwi Setyantoro', '2018-01-05 20:33:58', '2018-01-05 20:33:59'),
(1817, 11, 'Estetika', 'K 002', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-06 13:26:26', '2018-01-06 13:26:28'),
(1818, 11, 'Estetika', 'K 001', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-06 13:40:58', '2018-01-06 13:40:59'),
(1819, 11, 'Estetika', 'K 003', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-06 15:49:44', '2018-01-06 15:49:45'),
(1820, 11, 'Estetika', 'K 004', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-06 15:49:51', '2018-01-06 15:49:52'),
(1821, 11, 'Estetika', 'K 005', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-06 15:49:58', '2018-01-06 15:50:00'),
(1822, 11, 'Estetika', 'K 006', 'dr. Putri Wulandari Dipl. AAAM(US)', '2018-01-06 15:50:09', '2018-01-06 15:50:11');

-- --------------------------------------------------------

--
-- Table structure for table `sys_schedule_dokter`
--

CREATE TABLE `sys_schedule_dokter` (
  `schedule_id` int(10) UNSIGNED NOT NULL,
  `schedule_dokter_id` int(10) UNSIGNED NOT NULL,
  `schedule_dokter_name` varchar(100) NOT NULL,
  `schedule_kind_service_id` int(10) UNSIGNED NOT NULL,
  `schedule_kind_service_name` varchar(100) NOT NULL,
  `schedule_datetime_start` datetime NOT NULL,
  `schedule_datetime_end` datetime NOT NULL,
  `schedule_sync_datetime` datetime DEFAULT NULL,
  `schedule_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_schedule_dokter`
--

INSERT INTO `sys_schedule_dokter` (`schedule_id`, `schedule_dokter_id`, `schedule_dokter_name`, `schedule_kind_service_id`, `schedule_kind_service_name`, `schedule_datetime_start`, `schedule_datetime_end`, `schedule_sync_datetime`, `schedule_timestamp`) VALUES
(9, 67, 'dr. Ashri Yudhistira, Sp.THT-KL', 24, 'THT', '2016-12-16 16:00:00', '2016-12-16 18:00:00', '2016-12-16 16:20:28', '2016-12-16 09:20:28'),
(8, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-16 18:00:00', '2016-12-16 21:00:00', '2016-12-16 16:20:27', '2016-12-16 09:20:28'),
(7, 23, 'Prof. Dr. Sutomo Kasiman, Sp.JP(K), FIHA, FAsCC', 27, 'Cardio', '2016-12-16 16:00:00', '2016-12-16 16:01:41', '2016-12-16 16:20:27', '2016-12-16 09:20:27'),
(10, 24, 'dr. Yudha Sudewo, Sp.OG', 12, 'Obgyn', '2016-12-16 18:28:00', '2016-12-16 21:00:00', '2016-12-16 16:20:28', '2016-12-16 09:20:29'),
(11, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2016-12-16 12:00:00', '2016-12-16 18:00:00', '2016-12-16 16:20:29', '2016-12-16 09:20:29'),
(12, 70, 'dr. R.A Dwi Pujiastuti, Sp.S', 25, 'Neurologi', '2016-12-16 12:00:00', '2016-12-16 15:00:00', '2016-12-16 16:20:29', '2016-12-16 09:20:30'),
(13, 37, 'Prof. Dr. dr. Hasan Sjahrir, Sp.S(K)', 25, 'Neurologi', '2016-12-16 18:00:00', '2016-12-16 20:00:00', '2016-12-16 16:20:30', '2016-12-16 09:20:30'),
(14, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2016-12-16 15:30:00', '2016-12-16 20:30:00', '2016-12-16 16:20:30', '2016-12-16 09:20:31'),
(15, 69, 'dr. Inke Nadia Lubis, Sp.A', 13, 'Anak', '2016-12-16 10:30:00', '2016-12-16 12:00:00', '2016-12-16 16:20:31', '2016-12-16 09:20:31'),
(16, 71, 'dr. Dian Anindita Lubis, Sp.PD', 14, 'Penyakit Dalam', '2016-12-16 11:00:00', '2016-12-16 13:00:00', '2016-12-16 16:20:31', '2016-12-16 09:20:32'),
(17, 97, 'drg. Brian', 26, 'Gigi', '2016-12-16 14:00:00', '2016-12-16 20:30:00', '2016-12-16 17:31:46', '2016-12-16 09:20:35'),
(18, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2016-12-19 15:30:00', '2016-12-19 20:30:00', '2016-12-19 16:17:52', '2016-12-19 03:34:07'),
(19, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2016-12-19 11:00:00', '2016-12-19 18:00:00', '2016-12-19 11:46:32', '2016-12-19 04:46:34'),
(20, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-19 18:00:00', '2016-12-19 20:00:00', '2016-12-19 14:48:20', '2016-12-19 07:09:53'),
(37, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2016-12-23 12:02:00', '2016-12-23 17:02:00', '2016-12-23 12:03:09', '2016-12-23 05:03:08'),
(22, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2016-12-20 11:00:00', '2016-12-20 18:00:00', '2016-12-20 11:19:03', '2016-12-20 04:19:05'),
(24, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2016-12-20 16:00:00', '2016-12-20 18:00:00', '2016-12-20 16:07:28', '2016-12-20 09:07:30'),
(25, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-20 18:20:00', '2016-12-20 20:26:49', '2016-12-20 20:26:50', '2016-12-20 11:32:43'),
(38, 114, 'Joy Wulansari', 27, 'Cardio', '2016-12-23 14:00:00', '2016-12-23 17:00:00', '2016-12-23 14:01:13', '2016-12-23 07:01:13'),
(27, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2016-12-21 12:35:00', '2016-12-21 18:00:00', '2016-12-21 12:36:00', '2016-12-21 05:36:03'),
(28, 97, 'drg. Brian', 26, 'Gigi', '2016-12-21 11:00:00', '2016-12-21 15:30:00', '2016-12-21 14:57:23', '2016-12-21 07:35:40'),
(29, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-21 18:00:00', '2016-12-21 20:11:26', '2016-12-21 20:11:26', '2016-12-21 11:11:07'),
(30, 67, 'dr. Ashri Yudhistira, Sp.THT-KL', 24, 'THT', '2016-12-21 18:46:00', '2016-12-21 20:00:00', '2016-12-21 18:46:58', '2016-12-21 11:47:01'),
(31, 23, 'Prof. Dr. Sutomo Kasiman, Sp.JP(K), FIHA, FAsCC', 27, 'Cardio', '2016-12-22 10:22:00', '2016-12-22 12:56:00', '2016-12-22 12:52:49', '2016-12-22 03:22:26'),
(32, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2016-12-22 11:13:00', '2016-12-22 18:00:00', '2016-12-22 11:14:01', '2016-12-22 04:14:01'),
(39, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-23 18:00:00', '2016-12-23 20:10:57', '2016-12-23 20:10:58', '2016-12-23 11:01:56'),
(34, 97, 'drg. Brian', 26, 'Gigi', '2016-12-22 15:00:00', '2016-12-22 17:00:00', '2016-12-22 15:04:41', '2016-12-22 08:04:41'),
(35, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-22 18:00:00', '2016-12-22 20:01:10', '2016-12-22 20:01:10', '2016-12-22 11:04:59'),
(36, 114, 'dr. Joy Wulansari, Sp.JP', 27, 'Cardio', '2016-12-23 11:11:00', '2016-12-23 11:15:00', '2016-12-23 11:12:06', '2016-12-23 04:12:06'),
(40, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2016-12-26 23:00:00', '2016-12-26 18:00:00', '2016-12-26 11:34:44', '2016-12-26 04:34:45'),
(41, 114, 'Joy Wulansari', 27, 'Cardio', '2016-12-27 14:00:00', '2016-12-27 17:00:00', '2016-12-27 14:04:43', '2016-12-27 07:04:44'),
(42, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-27 18:00:00', '2016-12-27 20:00:00', '2016-12-27 18:22:44', '2016-12-27 11:22:45'),
(43, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-28 14:00:00', '2016-12-28 16:00:00', '2016-12-28 14:10:10', '2016-12-28 07:10:12'),
(44, 67, 'Ashri Yudhistira', 24, 'THT', '2016-12-28 17:39:00', '2016-12-28 17:39:00', '2016-12-28 17:39:44', '2016-12-28 10:39:45'),
(45, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2016-12-29 14:00:00', '2016-12-29 17:00:00', '2016-12-29 14:01:01', '2016-12-29 07:01:03'),
(46, 97, 'drg. Brian', 26, 'Gigi', '2016-12-30 13:00:00', '2016-12-30 17:00:00', '2016-12-30 13:41:45', '2016-12-30 06:41:45'),
(47, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-01-02 14:00:00', '2017-01-02 17:00:00', '2017-01-02 14:57:41', '2017-01-02 07:57:41'),
(48, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-06 11:34:00', '2017-01-06 18:00:00', '2017-01-06 11:34:52', '2017-01-06 04:34:52'),
(49, 97, 'drg. Brian', 26, 'Gigi', '2017-01-06 14:00:00', '2017-01-06 17:00:00', '2017-01-06 14:16:29', '2017-01-06 07:16:22'),
(50, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-06 19:18:00', '2017-01-06 19:18:00', '2017-01-06 19:18:02', '2017-01-06 12:18:01'),
(51, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2017-01-07 14:37:00', '2017-01-07 14:38:31', '2017-01-07 14:38:31', '2017-01-07 07:38:29'),
(52, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-09 10:10:00', '2017-01-09 06:00:00', '2017-01-09 10:03:26', '2017-01-09 03:03:26'),
(53, 97, 'drg. Brian', 26, 'Gigi', '2017-01-09 14:00:00', '2017-01-09 17:00:00', '2017-01-09 15:00:03', '2017-01-09 05:24:25'),
(54, 114, 'Joy Wulansari', 27, 'Cardio', '2017-01-09 14:00:00', '2017-01-09 17:13:40', '2017-01-09 17:13:40', '2017-01-09 07:13:53'),
(55, 167, 'Aidil', 26, 'Gigi', '2017-01-09 14:00:00', '2017-01-09 18:00:00', '2017-01-09 16:38:08', '2017-01-09 09:38:08'),
(56, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-09 19:59:00', '2017-01-09 19:59:00', '2017-01-09 19:59:11', '2017-01-09 12:59:10'),
(57, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-10 12:03:00', '2017-01-10 18:00:00', '2017-01-10 13:08:10', '2017-01-10 06:08:11'),
(58, 167, 'Aidil', 26, 'Gigi', '2017-01-10 14:17:00', '2017-01-10 18:00:00', '2017-01-10 14:17:37', '2017-01-10 07:17:37'),
(59, 114, 'Joy Wulansari', 27, 'Cardio', '2017-01-10 14:00:00', '2017-01-10 17:11:26', '2017-01-10 17:11:27', '2017-01-10 08:34:10'),
(60, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-11 12:00:00', '2017-01-11 18:00:00', '2017-01-11 13:04:35', '2017-01-11 06:04:35'),
(61, 167, 'Aidil', 26, 'Gigi', '2017-01-11 14:19:00', '2017-01-11 18:00:00', '2017-01-11 14:19:25', '2017-01-11 07:19:26'),
(62, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2017-01-11 18:00:00', '2017-01-11 19:27:43', '2017-01-11 19:27:43', '2017-01-11 12:03:40'),
(63, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-12 11:00:00', '2017-01-12 18:00:00', '2017-01-12 10:17:59', '2017-01-12 03:18:00'),
(64, 114, 'Joy Wulansari', 27, 'Cardio', '2017-01-12 15:09:00', '2017-01-12 17:00:00', '2017-01-12 15:09:33', '2017-01-12 08:09:34'),
(65, 167, 'Aidil', 26, 'Gigi', '2017-01-12 15:12:00', '2017-01-12 18:00:00', '2017-01-12 15:12:14', '2017-01-12 08:12:15'),
(66, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-12 17:02:00', '2017-01-12 17:02:00', '2017-01-12 17:01:54', '2017-01-12 10:01:54'),
(67, 97, 'drg. Brian', 26, 'Gigi', '2017-01-12 18:23:00', '2017-01-12 21:00:00', '2017-01-12 18:23:22', '2017-01-12 11:23:22'),
(68, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-13 12:00:00', '2017-01-13 18:00:00', '2017-01-13 11:52:31', '2017-01-13 04:52:32'),
(69, 97, 'drg. Brian', 26, 'Gigi', '2017-01-13 11:00:00', '2017-01-13 18:00:00', '2017-01-13 15:45:16', '2017-01-13 08:45:17'),
(70, 167, 'Aidil', 26, 'Gigi', '2017-01-13 17:00:00', '2017-01-13 21:00:00', '2017-01-13 17:34:00', '2017-01-13 10:34:01'),
(71, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-13 18:57:00', '2017-01-13 19:57:00', '2017-01-13 18:57:28', '2017-01-13 11:57:29'),
(72, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-14 10:00:00', '2017-01-14 14:30:00', '2017-01-14 10:31:06', '2017-01-14 03:31:07'),
(73, 97, 'drg. Brian', 26, 'Gigi', '2017-01-16 11:00:00', '2017-01-16 02:00:00', '2017-01-16 13:49:52', '2017-01-16 06:49:54'),
(74, 114, 'Joy Wulansari', 27, 'Cardio', '2017-01-16 14:00:00', '2017-01-16 17:00:00', '2017-01-16 14:01:38', '2017-01-16 07:01:39'),
(75, 167, 'Aidil', 26, 'Gigi', '2017-01-16 14:00:00', '2017-01-16 18:00:00', '2017-01-16 14:31:45', '2017-01-16 07:31:46'),
(76, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-16 15:31:00', '2017-01-16 20:30:00', '2017-01-16 15:31:16', '2017-01-16 08:31:17'),
(77, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2017-01-16 19:30:00', '2017-01-16 21:00:00', '2017-01-16 19:36:35', '2017-01-16 12:36:36'),
(78, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-17 10:12:00', '2017-01-17 18:00:00', '2017-01-17 10:12:26', '2017-01-17 03:12:28'),
(79, 114, 'Joy Wulansari', 27, 'Cardio', '2017-01-17 14:00:00', '2017-01-17 17:00:00', '2017-01-17 14:28:13', '2017-01-17 07:28:15'),
(80, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-17 14:31:00', '2017-01-17 17:31:00', '2017-01-17 14:31:51', '2017-01-17 07:31:52'),
(81, 167, 'Aidil', 26, 'Gigi', '2017-01-17 15:01:00', '2017-01-17 18:00:00', '2017-01-17 15:01:53', '2017-01-17 08:01:55'),
(82, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-18 11:55:00', '2017-01-18 18:00:00', '2017-01-18 11:55:45', '2017-01-18 04:55:46'),
(83, 97, 'drg. Brian', 26, 'Gigi', '2017-01-18 11:00:00', '2017-01-18 14:00:00', '2017-01-18 12:02:07', '2017-01-18 05:02:08'),
(84, 167, 'Aidil', 26, 'Gigi', '2017-01-18 14:19:00', '2017-01-18 18:00:00', '2017-01-18 14:19:15', '2017-01-18 07:19:16'),
(85, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-18 20:00:00', '2017-01-18 21:00:00', '2017-01-18 20:07:58', '2017-01-18 08:57:36'),
(86, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-19 11:00:00', '2017-01-19 18:00:00', '2017-01-19 12:50:14', '2017-01-19 05:50:14'),
(87, 114, 'Joy Wulansari', 27, 'Cardio', '2017-01-19 14:00:00', '2017-01-19 18:02:26', '2017-01-19 18:02:27', '2017-01-19 07:39:17'),
(88, 97, 'drg. Brian', 26, 'Gigi', '2017-01-19 17:20:00', '2017-01-19 21:00:00', '2017-01-19 17:21:11', '2017-01-19 10:21:11'),
(89, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-01-19 18:02:00', '2017-01-19 20:02:00', '2017-01-19 18:02:57', '2017-01-19 11:02:57'),
(90, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-20 12:19:00', '2017-01-20 18:00:00', '2017-01-20 12:20:08', '2017-01-20 05:20:08'),
(91, 114, 'Joy Wulansari', 27, 'Cardio', '2017-01-20 14:00:00', '2017-01-20 17:00:00', '2017-01-20 14:48:37', '2017-01-20 07:48:37'),
(92, 97, 'drg. Brian', 26, 'Gigi', '2017-01-20 14:10:00', '2017-01-20 18:10:00', '2017-01-20 15:11:07', '2017-01-20 08:11:07'),
(93, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-20 18:06:00', '2017-01-20 20:06:00', '2017-01-20 18:07:01', '2017-01-20 11:07:01'),
(94, 167, 'Aidil', 26, 'Gigi', '2017-01-20 18:00:00', '2017-01-20 21:00:00', '2017-01-20 20:10:08', '2017-01-20 13:10:08'),
(95, 167, 'Aidil', 26, 'Gigi', '2017-01-23 14:00:00', '2017-01-23 18:00:00', '2017-01-23 14:04:09', '2017-01-23 07:04:10'),
(96, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2017-01-23 17:00:00', '2017-01-23 21:00:00', '2017-01-23 16:12:36', '2017-01-23 09:12:37'),
(97, 167, 'Aidil', 26, 'Gigi', '2017-01-24 13:56:00', '2017-01-24 18:00:00', '2017-01-24 13:56:19', '2017-01-24 06:56:20'),
(98, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-25 12:00:00', '2017-01-25 18:00:00', '2017-01-25 12:33:01', '2017-01-25 05:33:01'),
(99, 167, 'Aidil', 26, 'Gigi', '2017-01-25 14:05:00', '2017-01-25 18:00:00', '2017-01-25 14:06:36', '2017-01-25 07:06:36'),
(100, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-25 17:12:00', '2017-01-25 21:00:00', '2017-01-25 17:12:15', '2017-01-25 10:12:15'),
(101, 167, 'Aidil', 26, 'Gigi', '2017-01-26 15:15:00', '2017-01-26 17:39:00', '2017-01-26 17:39:00', '2017-01-26 08:15:28'),
(102, 97, 'drg. Brian', 26, 'Gigi', '2017-01-26 17:39:00', '2017-01-26 17:39:00', '2017-01-26 17:39:29', '2017-01-26 10:39:30'),
(103, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-27 14:27:00', '2017-01-27 17:27:00', '2017-01-27 16:30:39', '2017-01-27 09:30:40'),
(104, 167, 'Aidil', 26, 'Gigi', '2017-01-27 16:50:00', '2017-01-27 20:19:14', '2017-01-27 20:19:14', '2017-01-27 09:50:42'),
(105, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-28 12:00:00', '2017-01-28 14:00:00', '2017-01-28 12:59:05', '2017-01-28 05:59:06'),
(106, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-30 12:00:00', '2017-01-30 18:00:00', '2017-01-30 11:49:37', '2017-01-30 04:49:39'),
(107, 167, 'Aidil', 26, 'Gigi', '2017-01-30 14:44:00', '2017-01-30 18:00:00', '2017-01-30 14:52:14', '2017-01-30 07:44:19'),
(108, 67, 'Ashri Yudhistira', 24, 'THT', '2017-01-30 18:18:00', '2017-01-30 18:18:00', '2017-01-30 18:18:13', '2017-01-30 11:18:15'),
(109, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-01-31 12:00:00', '2017-01-31 18:00:00', '2017-01-31 12:18:34', '2017-01-31 05:18:36'),
(110, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-01 11:00:00', '2017-02-01 18:00:00', '2017-02-01 10:29:19', '2017-02-01 03:29:21'),
(111, 167, 'Aidil', 26, 'Gigi', '2017-02-01 14:25:00', '2017-02-01 18:00:00', '2017-02-01 14:26:55', '2017-02-01 07:26:57'),
(112, 97, 'drg. Brian', 26, 'Gigi', '2017-02-01 23:04:00', '2017-02-01 23:04:00', '2017-02-01 23:04:03', '2017-02-01 16:04:04'),
(113, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-02 10:39:00', '2017-02-02 17:39:00', '2017-02-02 12:39:51', '2017-02-02 05:39:52'),
(114, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-02 21:07:00', '2017-02-02 21:30:00', '2017-02-02 21:07:15', '2017-02-02 14:07:15'),
(115, 97, 'drg. Brian', 26, 'Gigi', '2017-02-02 21:22:00', '2017-02-02 21:22:00', '2017-02-02 21:22:13', '2017-02-02 14:22:13'),
(116, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-03 12:00:00', '2017-02-03 18:00:00', '2017-02-03 15:00:52', '2017-02-03 05:04:57'),
(117, 97, 'drg. Brian', 26, 'Gigi', '2017-02-03 14:36:00', '2017-02-03 18:36:00', '2017-02-03 14:36:28', '2017-02-03 07:36:28'),
(118, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-06 11:00:00', '2017-02-06 18:00:00', '2017-02-06 10:37:17', '2017-02-06 03:37:18'),
(119, 167, 'Aidil', 26, 'Gigi', '2017-02-06 14:20:00', '2017-02-06 18:00:00', '2017-02-06 14:20:23', '2017-02-06 07:20:24'),
(120, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-06 14:28:00', '2017-02-06 17:28:00', '2017-02-06 14:29:04', '2017-02-06 07:29:05'),
(121, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-07 11:00:00', '2017-02-07 18:00:00', '2017-02-07 11:14:06', '2017-02-07 04:14:07'),
(122, 167, 'Aidil', 26, 'Gigi', '2017-02-07 14:30:00', '2017-02-07 18:00:00', '2017-02-07 14:30:12', '2017-02-07 07:30:12'),
(123, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-07 16:31:00', '2017-02-07 16:31:00', '2017-02-07 16:31:14', '2017-02-07 09:31:14'),
(124, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-08 11:00:00', '2017-02-08 18:00:00', '2017-02-08 12:50:16', '2017-02-08 05:50:18'),
(125, 167, 'Aidil', 26, 'Gigi', '2017-02-08 14:18:00', '2017-02-08 18:00:00', '2017-02-08 14:18:16', '2017-02-08 07:18:18'),
(126, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-09 12:00:00', '2017-02-09 17:00:00', '2017-02-09 11:01:29', '2017-02-09 04:01:30'),
(127, 167, 'Aidil', 26, 'Gigi', '2017-02-09 14:38:00', '2017-02-09 18:00:00', '2017-02-09 14:38:57', '2017-02-09 07:38:59'),
(128, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-10 11:00:00', '2017-02-10 18:00:00', '2017-02-10 11:17:31', '2017-02-10 04:17:33'),
(129, 114, 'Joy Wulansari', 27, 'Cardio', '2017-02-10 14:00:00', '2017-02-10 17:00:00', '2017-02-10 14:29:11', '2017-02-10 07:29:12'),
(130, 97, 'drg. Brian', 26, 'Gigi', '2017-02-10 15:23:00', '2017-02-10 19:30:10', '2017-02-10 19:30:10', '2017-02-10 08:23:19'),
(131, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-11 11:00:00', '2017-02-11 18:00:00', '2017-02-11 14:52:28', '2017-02-11 07:52:30'),
(132, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-13 11:00:00', '2017-02-13 18:00:00', '2017-02-13 11:01:42', '2017-02-13 04:01:44'),
(133, 97, 'drg. Brian', 26, 'Gigi', '2017-02-13 10:46:00', '2017-02-13 18:00:00', '2017-02-13 13:46:33', '2017-02-13 06:46:35'),
(134, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-14 11:00:00', '2017-02-14 17:00:00', '2017-02-14 10:43:35', '2017-02-14 03:43:37'),
(135, 167, 'Aidil', 26, 'Gigi', '2017-02-14 14:00:00', '2017-02-14 18:00:00', '2017-02-14 15:54:34', '2017-02-14 08:54:36'),
(136, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-14 16:55:00', '2017-02-14 16:55:00', '2017-02-14 16:55:26', '2017-02-14 09:55:27'),
(137, 97, 'drg. Brian', 26, 'Gigi', '2017-02-15 10:28:00', '2017-02-15 14:28:00', '2017-02-15 14:29:01', '2017-02-15 07:29:03'),
(138, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-15 16:59:00', '2017-02-15 16:59:00', '2017-02-15 16:59:32', '2017-02-15 09:59:34'),
(139, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-16 10:30:00', '2017-02-16 17:00:00', '2017-02-16 09:44:32', '2017-02-16 02:44:32'),
(140, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-16 15:01:00', '2017-02-16 18:00:00', '2017-02-16 16:38:18', '2017-02-16 08:01:39'),
(141, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-17 11:00:00', '2017-02-17 18:00:00', '2017-02-17 11:32:58', '2017-02-17 04:32:58'),
(142, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-17 18:41:00', '2017-02-17 19:41:00', '2017-02-17 18:41:29', '2017-02-17 11:41:29'),
(143, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-02-18 08:00:00', '2017-02-18 11:00:00', '2017-02-18 08:48:28', '2017-02-18 01:48:28'),
(144, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-18 11:00:00', '2017-02-18 15:00:00', '2017-02-18 11:10:29', '2017-02-18 04:10:29'),
(145, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-18 14:24:00', '2017-02-18 14:24:00', '2017-02-18 14:24:19', '2017-02-18 07:24:19'),
(146, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-20 11:00:00', '2017-02-20 18:00:00', '2017-02-20 11:28:23', '2017-02-20 04:28:24'),
(147, 167, 'Aidil', 26, 'Gigi', '2017-02-20 14:00:00', '2017-02-20 18:00:00', '2017-02-20 14:48:54', '2017-02-20 07:48:54'),
(148, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-21 11:00:00', '2017-02-21 18:00:00', '2017-02-21 13:57:35', '2017-02-21 06:57:36'),
(149, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-22 11:00:00', '2017-02-22 18:00:00', '2017-02-22 10:57:40', '2017-02-22 03:57:42'),
(150, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-22 16:03:00', '2017-02-22 16:03:00', '2017-02-22 16:04:01', '2017-02-22 09:04:02'),
(151, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-23 11:00:00', '2017-02-23 18:00:00', '2017-02-23 11:15:04', '2017-02-23 04:15:05'),
(152, 97, 'drg. Brian', 26, 'Gigi', '2017-02-23 16:28:00', '2017-02-23 20:30:00', '2017-02-23 16:28:43', '2017-02-23 09:28:44'),
(153, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-23 16:45:00', '2017-02-23 16:45:00', '2017-02-23 16:45:18', '2017-02-23 09:45:19'),
(154, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-24 11:00:00', '2017-02-24 18:00:00', '2017-02-24 11:31:32', '2017-02-24 04:31:33'),
(155, 97, 'drg. Brian', 26, 'Gigi', '2017-02-24 15:41:00', '2017-02-24 18:41:00', '2017-02-24 15:41:32', '2017-02-24 08:41:33'),
(156, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-25 10:00:00', '2017-02-25 15:00:00', '2017-02-25 10:45:17', '2017-02-25 03:45:20'),
(157, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-27 11:00:00', '2017-02-27 18:00:00', '2017-02-27 12:17:27', '2017-02-27 05:17:30'),
(158, 167, 'Aidil', 26, 'Gigi', '2017-02-27 14:00:00', '2017-02-27 18:00:00', '2017-02-27 15:06:21', '2017-02-27 08:06:23'),
(159, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-02-28 11:00:00', '2017-02-28 18:00:00', '2017-02-28 12:48:14', '2017-02-28 05:48:15'),
(160, 67, 'Ashri Yudhistira', 24, 'THT', '2017-02-28 16:43:00', '2017-02-28 16:43:00', '2017-02-28 16:43:36', '2017-02-28 09:43:37'),
(161, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-01 11:00:00', '2017-03-01 18:00:00', '2017-03-01 11:43:51', '2017-03-01 04:43:53'),
(162, 67, 'Ashri Yudhistira', 24, 'THT', '2017-03-01 14:31:00', '2017-03-01 14:31:00', '2017-03-01 14:31:45', '2017-03-01 07:31:47'),
(163, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-02 11:00:00', '2017-03-02 18:00:00', '2017-03-02 12:00:31', '2017-03-02 05:00:31'),
(164, 97, 'drg. Brian', 26, 'Gigi', '2017-03-02 14:12:00', '2017-03-02 18:12:00', '2017-03-02 14:12:41', '2017-03-02 07:12:40'),
(165, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-03 11:00:00', '2017-03-03 18:00:00', '2017-03-03 11:37:50', '2017-03-03 04:37:50'),
(166, 97, 'drg. Brian', 26, 'Gigi', '2017-03-03 15:21:00', '2017-03-03 18:21:00', '2017-03-03 15:21:06', '2017-03-03 08:21:05'),
(167, 167, 'Aidil', 26, 'Gigi', '2017-03-04 10:45:00', '2017-03-04 10:45:00', '2017-03-04 10:46:09', '2017-03-04 03:46:10'),
(168, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-04 10:00:00', '2017-03-04 14:00:00', '2017-03-04 10:53:03', '2017-03-04 03:53:03'),
(169, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-05 12:00:00', '2017-03-05 14:00:00', '2017-03-05 13:31:54', '2017-03-05 06:31:55'),
(170, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-06 12:00:00', '2017-03-06 17:00:00', '2017-03-06 11:41:01', '2017-03-06 04:41:02'),
(171, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-07 11:00:00', '2017-03-07 18:00:00', '2017-03-07 13:37:33', '2017-03-07 06:37:34'),
(172, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-08 09:00:00', '2017-03-08 18:00:00', '2017-03-08 09:43:23', '2017-03-08 02:43:24'),
(173, 167, 'Aidil', 26, 'Gigi', '2017-03-08 16:16:00', '2017-03-08 16:16:00', '2017-03-08 16:16:39', '2017-03-08 09:16:40'),
(174, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-09 11:00:00', '2017-03-09 18:00:00', '2017-03-09 14:33:23', '2017-03-09 07:33:24'),
(175, 167, 'Aidil', 26, 'Gigi', '2017-03-10 19:12:00', '2017-03-10 19:12:00', '2017-03-10 19:12:49', '2017-03-10 12:12:49'),
(176, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-13 11:00:00', '2017-03-13 18:00:00', '2017-03-13 11:12:04', '2017-03-13 04:12:06'),
(177, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-03-13 19:00:00', '2017-03-13 20:00:00', '2017-03-13 18:13:17', '2017-03-13 11:13:19'),
(178, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-14 23:00:00', '2017-03-14 18:00:00', '2017-03-14 11:32:58', '2017-03-14 04:33:01'),
(179, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-15 11:00:00', '2017-03-15 18:00:00', '2017-03-15 14:05:27', '2017-03-15 07:05:29'),
(180, 167, 'Aidil', 26, 'Gigi', '2017-03-15 15:49:00', '2017-03-15 18:00:00', '2017-03-15 16:16:01', '2017-03-15 09:15:48'),
(181, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-16 11:00:00', '2017-03-16 18:00:00', '2017-03-16 11:57:24', '2017-03-16 04:57:24'),
(182, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-03-16 18:00:00', '2017-03-16 20:00:00', '2017-03-16 18:45:09', '2017-03-16 11:45:08'),
(183, 167, 'Aidil', 26, 'Gigi', '2017-03-16 21:47:00', '2017-03-16 21:47:00', '2017-03-16 21:48:09', '2017-03-16 14:48:08'),
(184, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-17 11:00:00', '2017-03-17 18:00:00', '2017-03-17 11:31:59', '2017-03-17 04:32:00'),
(185, 67, 'Ashri Yudhistira', 24, 'THT', '2017-03-17 11:34:00', '2017-03-17 11:34:00', '2017-03-17 11:34:13', '2017-03-17 04:34:13'),
(186, 97, 'drg. Brian', 26, 'Gigi', '2017-03-17 14:12:00', '2017-03-17 18:12:00', '2017-03-17 15:12:48', '2017-03-17 08:12:48'),
(187, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-03-17 18:09:00', '2017-03-17 20:09:00', '2017-03-17 18:09:18', '2017-03-17 11:09:18'),
(188, 167, 'Aidil', 26, 'Gigi', '2017-03-17 18:00:00', '2017-03-17 21:00:00', '2017-03-17 19:11:02', '2017-03-17 12:11:02'),
(189, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-20 11:28:00', '2017-03-20 17:28:00', '2017-03-20 11:28:45', '2017-03-20 04:28:46'),
(190, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2017-03-20 17:00:00', '2017-03-20 20:00:00', '2017-03-20 17:28:44', '2017-03-20 10:28:44'),
(191, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-03-20 18:00:00', '2017-03-20 20:00:00', '2017-03-20 18:44:34', '2017-03-20 11:44:01'),
(192, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-21 11:00:00', '2017-03-21 18:00:00', '2017-03-21 12:28:16', '2017-03-21 04:18:49'),
(193, 167, 'Aidil', 26, 'Gigi', '2017-03-21 14:00:00', '2017-03-21 18:00:00', '2017-03-21 15:25:14', '2017-03-21 08:25:15'),
(194, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-03-21 18:16:00', '2017-03-21 20:16:00', '2017-03-21 18:16:31', '2017-03-21 11:16:32'),
(195, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-22 11:00:00', '2017-03-22 18:00:00', '2017-03-22 11:35:56', '2017-03-22 04:35:57'),
(196, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-03-22 13:00:00', '2017-03-22 14:00:00', '2017-03-22 12:41:05', '2017-03-22 05:41:06'),
(197, 97, 'drg. Brian', 26, 'Gigi', '2017-03-22 12:05:00', '2017-03-22 18:05:00', '2017-03-22 14:05:27', '2017-03-22 07:05:28'),
(198, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-23 14:38:00', '2017-03-23 14:38:00', '2017-03-23 14:38:35', '2017-03-23 07:38:37'),
(199, 167, 'Aidil', 26, 'Gigi', '2017-03-23 18:00:00', '2017-03-23 21:00:00', '2017-03-23 19:04:48', '2017-03-23 07:51:15'),
(200, 97, 'drg. Brian', 26, 'Gigi', '2017-03-24 16:11:00', '2017-03-24 18:11:00', '2017-03-24 16:11:23', '2017-03-24 09:11:24'),
(201, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-25 11:00:00', '2017-03-25 15:00:00', '2017-03-25 11:51:21', '2017-03-25 04:51:23'),
(202, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-26 08:00:00', '2017-03-26 12:00:00', '2017-03-27 08:49:25', '2017-03-27 01:49:27'),
(203, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-27 11:00:00', '2017-03-27 18:00:00', '2017-03-27 11:15:50', '2017-03-27 04:15:51'),
(204, 97, 'drg. Brian', 26, 'Gigi', '2017-03-27 14:58:00', '2017-03-27 18:58:00', '2017-03-27 14:58:05', '2017-03-27 07:58:06'),
(205, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-03-28 11:00:00', '2017-03-28 17:00:00', '2017-03-28 11:24:26', '2017-03-28 04:24:28'),
(206, 97, 'drg. Brian', 26, 'Gigi', '2017-03-30 17:41:00', '2017-03-30 20:41:00', '2017-03-30 17:41:06', '2017-03-30 10:41:05'),
(207, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-03-30 18:03:00', '2017-03-30 20:03:00', '2017-03-30 18:04:22', '2017-03-30 11:04:21'),
(208, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-03 10:16:00', '2017-04-03 18:00:00', '2017-04-03 11:30:21', '2017-04-03 03:17:16'),
(209, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-04 11:00:00', '2017-04-04 18:00:00', '2017-04-04 11:13:49', '2017-04-04 04:13:50'),
(210, 167, 'Aidil', 26, 'Gigi', '2017-04-04 14:00:00', '2017-04-04 18:00:00', '2017-04-04 15:44:13', '2017-04-04 08:44:13'),
(211, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-04-04 18:04:00', '2017-04-04 19:30:00', '2017-04-04 18:04:37', '2017-04-04 11:04:38'),
(212, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-05 11:02:00', '2017-04-05 17:02:00', '2017-04-05 12:02:17', '2017-04-05 05:02:18'),
(213, 114, 'Joy Wulansari', 27, 'Cardio', '2017-04-05 14:00:00', '2017-04-05 17:00:00', '2017-04-05 13:49:21', '2017-04-05 06:49:22'),
(214, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-06 11:00:00', '2017-04-06 18:00:00', '2017-04-06 11:33:19', '2017-04-06 04:33:20'),
(215, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-04-06 18:30:00', '2017-04-06 20:30:00', '2017-04-06 18:31:39', '2017-04-06 11:31:41'),
(216, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-07 11:00:00', '2017-04-07 18:00:00', '2017-04-07 13:57:33', '2017-04-07 06:57:32'),
(217, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-10 11:00:00', '2017-04-10 18:00:00', '2017-04-10 12:25:07', '2017-04-10 05:25:08'),
(218, 67, 'Ashri Yudhistira', 24, 'THT', '2017-04-10 15:07:00', '2017-04-10 16:07:00', '2017-04-10 15:07:37', '2017-04-10 08:07:38'),
(219, 72, 'dr. Yuke Sarastri, Sp.JP', 27, 'Cardio', '2017-04-11 19:00:00', '2017-04-11 20:00:00', '2017-04-11 18:51:43', '2017-04-11 11:51:43'),
(220, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-12 11:00:00', '2017-04-12 18:00:00', '2017-04-12 11:50:50', '2017-04-12 04:50:51'),
(221, 97, 'drg. Brian', 26, 'Gigi', '2017-04-12 14:05:00', '2017-04-12 18:05:00', '2017-04-12 14:05:43', '2017-04-12 07:05:44'),
(222, 97, 'drg. Brian', 26, 'Gigi', '2017-04-13 11:29:00', '2017-04-13 11:29:00', '2017-04-13 11:29:53', '2017-04-13 04:29:55'),
(223, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-04-13 19:03:00', '2017-04-13 19:30:00', '2017-04-13 19:03:31', '2017-04-13 12:03:28'),
(224, 167, 'Aidil', 26, 'Gigi', '2017-04-15 13:17:00', '2017-04-15 17:00:00', '2017-04-15 13:17:23', '2017-04-15 06:17:23'),
(225, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-17 11:00:00', '2017-04-17 18:00:00', '2017-04-17 11:09:09', '2017-04-17 04:09:10'),
(226, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-19 13:00:00', '2017-04-19 17:00:00', '2017-04-19 12:56:04', '2017-04-19 05:56:05'),
(227, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-20 11:00:00', '2017-04-20 15:55:00', '2017-04-20 10:56:04', '2017-04-20 03:56:06'),
(228, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-21 11:00:00', '2017-04-21 18:00:00', '2017-04-21 13:20:41', '2017-04-21 06:20:42'),
(229, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-24 12:00:00', '2017-04-24 18:00:00', '2017-04-24 12:37:08', '2017-04-24 05:37:09'),
(230, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-25 11:00:00', '2017-04-25 18:00:00', '2017-04-25 12:40:43', '2017-04-25 05:40:44'),
(231, 97, 'drg. Brian', 26, 'Gigi', '2017-04-25 13:05:00', '2017-04-25 18:05:00', '2017-04-25 13:05:25', '2017-04-25 06:05:27'),
(232, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-26 11:00:00', '2017-04-26 18:00:00', '2017-04-26 11:58:29', '2017-04-26 04:58:31'),
(233, 97, 'drg. Brian', 26, 'Gigi', '2017-04-26 15:09:00', '2017-04-26 18:09:00', '2017-04-26 17:19:41', '2017-04-26 10:19:42'),
(234, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-27 11:00:00', '2017-04-27 18:00:00', '2017-04-27 13:16:30', '2017-04-27 06:16:32'),
(235, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-04-27 18:45:00', '2017-04-27 19:45:00', '2017-04-27 18:44:46', '2017-04-27 11:44:48'),
(236, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-04-28 11:00:00', '2017-04-28 18:00:00', '2017-04-28 11:12:17', '2017-04-28 04:12:17'),
(237, 407, 'Ray C Barus', 12, 'Obgyn', '2017-05-02 13:57:00', '2017-05-02 15:00:00', '2017-05-02 13:58:17', '2017-05-02 06:58:17'),
(238, 114, 'Joy Wulansari', 27, 'Cardio', '2017-05-02 15:00:00', '2017-05-02 17:00:00', '2017-05-02 14:50:18', '2017-05-02 07:50:18'),
(239, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-05-02 19:31:00', '2017-05-02 21:00:00', '2017-05-02 19:31:33', '2017-05-02 12:31:33'),
(240, 97, 'drg. Brian', 26, 'Gigi', '2017-05-03 11:03:00', '2017-05-03 11:03:00', '2017-05-03 11:03:03', '2017-05-03 04:03:03'),
(241, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-04 10:00:00', '2017-05-04 09:00:00', '2017-05-04 10:22:51', '2017-05-04 03:22:52'),
(242, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-05 12:00:00', '2017-05-05 17:00:00', '2017-05-05 12:52:19', '2017-05-05 05:52:21'),
(243, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-06 11:00:00', '2017-05-06 15:00:00', '2017-05-06 11:00:25', '2017-05-06 04:00:27'),
(244, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-05-08 16:00:00', '2017-05-08 20:00:00', '2017-05-08 15:08:06', '2017-05-08 08:08:07'),
(245, 167, 'Aidil', 26, 'Gigi', '2017-05-09 14:20:00', '2017-05-09 18:00:00', '2017-05-09 14:20:56', '2017-05-09 07:20:58'),
(246, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-05-09 16:08:00', '2017-05-09 17:08:00', '2017-05-09 16:08:57', '2017-05-09 09:08:58'),
(247, 97, 'drg. Brian', 26, 'Gigi', '2017-05-10 12:52:00', '2017-05-10 12:52:00', '2017-05-10 12:52:38', '2017-05-10 05:52:41'),
(248, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-12 11:00:00', '2017-05-12 18:00:00', '2017-05-12 11:28:42', '2017-05-12 04:28:43'),
(249, 97, 'drg. Brian', 26, 'Gigi', '2017-05-12 14:07:00', '2017-05-12 14:07:00', '2017-05-12 14:07:32', '2017-05-12 07:07:32'),
(250, 320, 'Farrel', 24, 'THT', '2017-05-12 15:19:00', '2017-05-12 16:00:00', '2017-05-12 15:20:01', '2017-05-12 08:20:01'),
(251, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-13 10:58:00', '2017-05-13 10:58:00', '2017-05-13 10:58:13', '2017-05-13 03:58:13'),
(252, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-14 09:44:00', '2017-05-14 09:44:00', '2017-05-14 09:44:57', '2017-05-14 02:44:57'),
(253, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-15 11:00:00', '2017-05-15 17:00:00', '2017-05-15 11:30:51', '2017-05-15 04:30:52'),
(254, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-16 23:00:00', '2017-05-16 17:00:00', '2017-05-16 11:37:52', '2017-05-16 04:37:53'),
(255, 167, 'Aidil', 26, 'Gigi', '2017-05-18 15:34:00', '2017-05-18 18:00:00', '2017-05-18 15:34:13', '2017-05-18 08:34:15'),
(256, 167, 'Aidil', 26, 'Gigi', '2017-05-20 10:23:00', '2017-05-20 10:23:00', '2017-05-20 10:23:29', '2017-05-20 03:23:30'),
(257, 37, 'Prof. Dr. dr. Hasan Sjahrir, Sp.S(K)', 25, 'Neurologi', '2017-05-22 09:00:00', '2017-05-22 10:00:00', '2017-05-22 09:04:08', '2017-05-22 02:04:10'),
(258, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-22 11:00:00', '2017-05-22 16:00:00', '2017-05-22 14:28:19', '2017-05-22 07:28:21'),
(259, 167, 'Aidil', 26, 'Gigi', '2017-05-22 14:50:00', '2017-05-22 14:50:00', '2017-05-22 14:55:37', '2017-05-22 07:55:39'),
(260, 67, 'Ashri Yudhistira', 24, 'THT', '2017-05-22 17:51:00', '2017-05-22 19:51:00', '2017-05-22 17:53:45', '2017-05-22 10:53:47'),
(261, 407, 'Ray C Barus', 12, 'Obgyn', '2017-05-23 10:55:00', '2017-05-23 12:00:00', '2017-05-23 11:06:30', '2017-05-23 04:06:32'),
(262, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-23 11:00:00', '2017-05-23 18:00:00', '2017-05-23 11:24:48', '2017-05-23 04:24:50'),
(263, 97, 'drg. Brian', 26, 'Gigi', '2017-05-23 15:13:00', '2017-05-23 15:13:00', '2017-05-23 15:13:28', '2017-05-23 08:13:30'),
(264, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-24 09:00:00', '2017-05-24 18:00:00', '2017-05-24 10:30:34', '2017-05-24 03:30:37'),
(265, 97, 'drg. Brian', 26, 'Gigi', '2017-05-24 14:22:00', '2017-05-24 14:22:00', '2017-05-24 14:22:08', '2017-05-24 07:22:10'),
(266, 167, 'Aidil', 26, 'Gigi', '2017-05-24 15:20:00', '2017-05-24 15:20:00', '2017-05-24 15:28:18', '2017-05-24 08:28:20'),
(267, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-26 11:00:00', '2017-05-26 16:00:00', '2017-05-26 12:17:04', '2017-05-26 05:17:04'),
(268, 320, 'Farrel', 24, 'THT', '2017-05-26 13:00:00', '2017-05-26 15:00:00', '2017-05-26 13:27:07', '2017-05-26 05:47:09'),
(269, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-05-26 14:00:00', '2017-05-26 20:00:00', '2017-05-26 14:14:24', '2017-05-26 07:14:24'),
(270, 97, 'drg. Brian', 26, 'Gigi', '2017-05-26 15:04:00', '2017-05-26 15:04:00', '2017-05-26 15:04:10', '2017-05-26 08:04:09'),
(271, 97, 'drg. Brian', 26, 'Gigi', '2017-05-29 15:33:00', '2017-05-29 15:33:00', '2017-05-29 15:33:15', '2017-05-29 08:33:15'),
(272, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-30 10:32:00', '2017-05-30 16:00:00', '2017-05-30 10:35:14', '2017-05-30 03:35:15'),
(273, 167, 'Aidil', 26, 'Gigi', '2017-05-30 19:38:00', '2017-05-30 19:38:00', '2017-05-30 19:41:01', '2017-05-30 12:41:02'),
(274, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-05-31 09:30:00', '2017-05-31 16:00:00', '2017-05-31 09:25:37', '2017-05-31 02:25:38'),
(275, 24, 'dr. Yudha Sudewo, Sp.OG', 12, 'Obgyn', '2017-05-31 11:08:00', '2017-05-31 11:08:00', '2017-05-31 11:08:42', '2017-05-31 04:08:43'),
(276, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-02 09:00:00', '2017-06-02 16:00:00', '2017-06-02 10:58:08', '2017-06-02 03:58:10'),
(277, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-03 11:00:00', '2017-06-03 13:00:00', '2017-06-04 15:26:42', '2017-06-04 08:26:44'),
(278, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-05 10:30:00', '2017-06-05 16:30:00', '2017-06-05 10:25:33', '2017-06-05 03:25:35'),
(279, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-06 10:44:00', '2017-06-06 16:00:00', '2017-06-06 10:44:54', '2017-06-06 03:44:56'),
(280, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-08 11:00:00', '2017-06-08 15:00:00', '2017-06-08 10:40:43', '2017-06-08 03:40:43'),
(281, 167, 'Aidil', 26, 'Gigi', '2017-06-09 18:59:00', '2017-06-09 21:00:00', '2017-06-09 19:37:39', '2017-06-09 12:37:31'),
(282, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-10 13:07:00', '2017-06-10 13:07:00', '2017-06-10 13:07:48', '2017-06-10 06:07:48'),
(283, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-12 10:14:00', '2017-06-12 17:14:00', '2017-06-12 10:14:37', '2017-06-12 03:14:38'),
(284, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-13 10:26:00', '2017-06-13 16:00:00', '2017-06-13 10:30:46', '2017-06-13 03:30:47'),
(285, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-14 11:19:00', '2017-06-14 15:00:00', '2017-06-14 11:19:32', '2017-06-14 04:19:34'),
(286, 167, 'Aidil', 26, 'Gigi', '2017-06-15 16:48:00', '2017-06-15 18:00:00', '2017-06-15 16:48:56', '2017-06-15 09:48:57'),
(287, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-16 09:00:00', '2017-06-16 12:00:00', '2017-06-16 09:45:33', '2017-06-16 02:45:35'),
(288, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-17 09:00:00', '2017-06-17 12:00:00', '2017-06-17 09:31:50', '2017-06-17 02:31:51'),
(289, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2017-06-17 14:45:00', '2017-06-17 14:45:00', '2017-06-17 14:45:23', '2017-06-17 07:45:25'),
(290, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-19 11:00:00', '2017-06-19 16:00:00', '2017-06-19 11:08:34', '2017-06-19 04:08:36'),
(291, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-20 10:00:00', '2017-06-20 16:00:00', '2017-06-20 10:49:08', '2017-06-20 03:49:10'),
(292, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-21 10:00:00', '2017-06-21 16:00:00', '2017-06-21 11:25:20', '2017-06-21 04:25:22'),
(293, 67, 'Ashri Yudhistira', 24, 'THT', '2017-06-21 15:11:00', '2017-06-21 15:11:00', '2017-06-21 15:14:14', '2017-06-21 08:14:16'),
(294, 167, 'Aidil', 26, 'Gigi', '2017-06-22 14:05:00', '2017-06-22 18:00:00', '2017-06-22 14:06:11', '2017-06-22 07:06:14'),
(295, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-06-29 11:21:00', '2017-06-29 13:00:00', '2017-06-29 11:21:33', '2017-06-29 04:21:38'),
(296, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-03 11:00:00', '2017-07-03 18:00:00', '2017-07-03 10:44:53', '2017-07-03 03:44:54'),
(297, 167, 'Aidil', 26, 'Gigi', '2017-07-03 14:14:00', '2017-07-03 18:00:00', '2017-07-03 14:14:49', '2017-07-03 07:14:49'),
(298, 167, 'Aidil', 26, 'Gigi', '2017-07-04 14:00:00', '2017-07-04 18:00:00', '2017-07-04 14:55:50', '2017-07-04 07:55:50'),
(299, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-05 11:00:00', '2017-07-05 14:00:00', '2017-07-05 11:00:31', '2017-07-05 04:00:32'),
(300, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-06 11:00:00', '2017-07-06 17:00:00', '2017-07-06 11:12:34', '2017-07-06 04:12:35'),
(301, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-07 10:00:00', '2017-07-07 18:00:00', '2017-07-07 12:15:20', '2017-07-07 05:15:20'),
(302, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-07-07 14:00:00', '2017-07-07 20:00:00', '2017-07-07 13:53:05', '2017-07-07 06:53:06'),
(303, 407, 'Ray C Barus', 12, 'Obgyn', '2017-07-10 09:39:00', '2017-07-10 10:39:00', '2017-07-10 09:39:27', '2017-07-10 02:39:29'),
(304, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-10 12:00:00', '2017-07-10 15:00:00', '2017-07-10 12:17:29', '2017-07-10 05:17:31'),
(305, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-12 11:00:00', '2017-07-12 18:00:00', '2017-07-12 11:28:20', '2017-07-12 04:28:22'),
(306, 167, 'Aidil', 26, 'Gigi', '2017-07-12 14:14:00', '2017-07-12 18:00:00', '2017-07-12 14:14:49', '2017-07-12 07:14:40'),
(307, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-14 10:16:00', '2017-07-14 15:16:00', '2017-07-14 10:17:02', '2017-07-14 03:17:04'),
(308, 167, 'Aidil', 26, 'Gigi', '2017-07-17 14:00:00', '2017-07-17 18:00:00', '2017-07-17 15:23:12', '2017-07-17 08:23:11'),
(309, 167, 'Aidil', 26, 'Gigi', '2017-07-18 14:00:00', '2017-07-18 18:00:00', '2017-07-18 14:48:22', '2017-07-18 07:48:23'),
(310, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-21 23:00:00', '2017-07-21 17:15:12', '2017-07-21 17:15:12', '2017-07-21 04:09:35'),
(311, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-24 11:00:00', '2017-07-24 18:00:00', '2017-07-24 11:59:59', '2017-07-24 05:00:00'),
(312, 67, 'Ashri Yudhistira', 24, 'THT', '2017-07-25 19:00:00', '2017-07-25 19:32:00', '2017-07-25 19:35:59', '2017-07-25 12:36:01'),
(313, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-26 10:04:00', '2017-07-26 14:04:00', '2017-07-26 11:02:16', '2017-07-26 04:02:18'),
(314, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-28 10:00:00', '2017-07-28 17:00:00', '2017-07-28 10:35:04', '2017-07-28 03:35:03'),
(315, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-07-28 18:00:00', '2017-07-28 19:00:00', '2017-07-28 18:00:22', '2017-07-28 11:00:21'),
(316, 167, 'Aidil', 26, 'Gigi', '2017-07-28 19:10:00', '2017-07-28 21:00:00', '2017-07-28 19:10:53', '2017-07-28 12:10:53'),
(317, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-07-31 11:00:00', '2017-07-31 18:00:00', '2017-07-31 11:01:13', '2017-07-31 04:01:14'),
(318, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-02 11:12:00', '2017-08-02 15:12:00', '2017-08-02 10:13:13', '2017-08-02 03:13:14'),
(319, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-04 11:00:00', '2017-08-04 18:00:00', '2017-08-04 11:00:52', '2017-08-04 04:00:54'),
(320, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-07 10:00:00', '2017-08-07 18:00:00', '2017-08-07 10:58:42', '2017-08-07 03:58:44'),
(321, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-08-07 18:42:00', '2017-08-07 19:00:00', '2017-08-07 18:42:27', '2017-08-07 11:42:29'),
(322, 97, 'drg. Brian', 26, 'Gigi', '2017-08-07 21:22:00', '2017-08-07 21:22:00', '2017-08-07 21:22:41', '2017-08-07 14:22:42'),
(323, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-09 12:19:00', '2017-08-09 12:19:00', '2017-08-09 12:19:40', '2017-08-09 05:19:43'),
(324, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-11 11:00:00', '2017-08-11 17:00:00', '2017-08-11 12:18:16', '2017-08-11 05:18:16'),
(325, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-16 12:29:00', '2017-08-16 12:29:00', '2017-08-16 12:29:08', '2017-08-16 05:29:08'),
(326, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-18 10:24:00', '2017-08-18 17:24:00', '2017-08-18 10:24:36', '2017-08-18 03:24:37'),
(327, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-21 11:00:00', '2017-08-21 17:00:00', '2017-08-21 11:00:16', '2017-08-21 04:00:19'),
(328, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-23 12:24:00', '2017-08-23 13:24:00', '2017-08-23 12:24:40', '2017-08-23 05:24:43'),
(329, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-25 10:00:00', '2017-08-25 17:00:00', '2017-08-25 10:59:42', '2017-08-25 03:59:43'),
(330, 167, 'Aidil', 26, 'Gigi', '2017-08-26 14:02:00', '2017-08-26 17:00:00', '2017-08-26 14:03:02', '2017-08-26 07:03:02'),
(331, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-08-28 11:00:00', '2017-08-28 14:00:00', '2017-08-28 11:08:38', '2017-08-28 04:08:39'),
(332, 167, 'Aidil', 26, 'Gigi', '2017-08-30 15:14:00', '2017-08-30 15:14:00', '2017-08-30 15:52:06', '2017-08-30 08:52:07'),
(333, 167, 'Aidil', 26, 'Gigi', '2017-08-31 14:00:00', '2017-08-31 14:00:00', '2017-08-31 14:00:56', '2017-08-31 07:00:57'),
(334, 167, 'Aidil', 26, 'Gigi', '2017-09-02 10:36:00', '2017-09-02 10:36:00', '2017-09-02 12:36:24', '2017-09-02 05:36:25'),
(335, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-04 11:00:00', '2017-09-04 17:00:00', '2017-09-04 11:29:12', '2017-09-04 04:29:14'),
(336, 167, 'Aidil', 26, 'Gigi', '2017-09-04 14:00:00', '2017-09-04 18:00:00', '2017-09-04 17:42:52', '2017-09-04 10:42:54'),
(337, 656, 'Rizky Vira Novella', 26, 'Gigi', '2017-09-05 10:00:00', '2017-09-05 13:57:20', '2017-09-05 13:57:21', '2017-09-05 04:39:09'),
(338, 167, 'Aidil', 26, 'Gigi', '2017-09-05 14:00:00', '2017-09-05 18:00:00', '2017-09-05 14:40:25', '2017-09-05 07:40:27'),
(339, 167, 'Aidil', 26, 'Gigi', '2017-09-08 15:00:00', '2017-09-08 18:00:00', '2017-09-08 15:13:08', '2017-09-08 08:13:08'),
(340, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-09-08 19:19:00', '2017-09-08 20:19:00', '2017-09-08 19:19:02', '2017-09-08 12:19:03'),
(341, 67, 'Ashri Yudhistira', 24, 'THT', '2017-09-08 19:39:00', '2017-09-08 20:30:00', '2017-09-08 19:44:05', '2017-09-08 12:44:05'),
(342, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-11 11:00:00', '2017-09-11 17:00:00', '2017-09-11 11:05:16', '2017-09-11 04:05:17'),
(343, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-13 11:34:00', '2017-09-13 13:34:00', '2017-09-13 11:34:36', '2017-09-13 04:34:37'),
(344, 167, 'Aidil', 26, 'Gigi', '2017-09-13 14:00:00', '2017-09-13 18:00:00', '2017-09-13 15:32:39', '2017-09-13 08:32:40'),
(345, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-14 13:33:00', '2017-09-14 13:33:00', '2017-09-14 13:33:59', '2017-09-14 06:34:00'),
(346, 167, 'Aidil', 26, 'Gigi', '2017-09-14 14:00:00', '2017-09-14 18:00:00', '2017-09-14 14:37:57', '2017-09-14 07:37:59'),
(347, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-15 10:00:00', '2017-09-15 17:00:00', '2017-09-15 11:04:25', '2017-09-15 04:04:27'),
(348, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-17 12:25:00', '2017-09-17 13:25:00', '2017-09-17 12:25:31', '2017-09-17 05:25:33'),
(349, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-18 11:29:00', '2017-09-18 14:29:00', '2017-09-18 11:29:44', '2017-09-18 04:29:47'),
(350, 97, 'drg. Brian', 26, 'Gigi', '2017-09-19 21:02:00', '2017-09-19 21:02:00', '2017-09-19 21:02:05', '2017-09-19 14:02:06'),
(351, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-20 11:31:00', '2017-09-20 14:31:00', '2017-09-20 11:31:15', '2017-09-20 04:31:17'),
(352, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-22 12:46:00', '2017-09-22 17:00:00', '2017-09-22 13:03:29', '2017-09-22 06:03:28'),
(353, 167, 'Aidil', 26, 'Gigi', '2017-09-25 15:11:00', '2017-09-25 15:11:00', '2017-09-25 15:11:49', '2017-09-25 08:11:50'),
(354, 97, 'drg. Brian', 26, 'Gigi', '2017-09-25 19:56:00', '2017-09-25 19:56:00', '2017-09-25 19:56:12', '2017-09-25 12:56:13'),
(355, 167, 'Aidil', 26, 'Gigi', '2017-09-26 15:21:00', '2017-09-26 18:00:00', '2017-10-02 14:34:01', '2017-10-02 07:34:03'),
(356, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-09-27 11:46:00', '2017-09-27 14:46:00', '2017-10-02 14:34:02', '2017-10-02 07:34:04'),
(357, 167, 'Aidil', 26, 'Gigi', '2017-10-03 14:00:00', '2017-10-03 18:00:00', '2017-10-03 14:50:15', '2017-10-03 07:50:17'),
(358, 167, 'Aidil', 26, 'Gigi', '2017-10-04 14:15:00', '2017-10-04 18:00:00', '2017-10-04 14:17:05', '2017-10-04 07:17:08'),
(359, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-10-06 14:48:00', '2017-10-06 14:48:00', '2017-10-06 14:48:08', '2017-10-06 07:48:07'),
(360, 167, 'Aidil', 26, 'Gigi', '2017-10-07 13:05:00', '2017-10-07 15:00:00', '2017-10-07 13:05:46', '2017-10-07 06:05:45'),
(361, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-10-10 12:00:00', '2017-10-10 16:00:00', '2017-10-10 12:03:22', '2017-10-10 05:03:23'),
(362, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-10-12 11:00:00', '2017-10-12 11:58:00', '2017-10-12 11:58:46', '2017-10-12 04:58:47'),
(363, 167, 'Aidil', 26, 'Gigi', '2017-10-12 14:00:00', '2017-10-12 18:00:00', '2017-10-12 14:39:14', '2017-10-12 07:39:15'),
(364, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-10-13 19:35:00', '2017-10-13 20:00:00', '2017-10-13 19:35:35', '2017-10-13 12:35:37'),
(365, 167, 'Aidil', 26, 'Gigi', '2017-10-17 14:59:00', '2017-10-17 18:00:00', '2017-10-17 14:59:15', '2017-10-17 07:59:19'),
(366, 71, 'Dhini Silvana', 14, 'Penyakit Dalam', '2017-10-17 19:11:00', '2017-10-17 20:00:00', '2017-10-17 19:11:25', '2017-10-17 12:11:28'),
(367, 656, 'Rizky Vira Novella', 26, 'Gigi', '2017-10-18 10:40:00', '2017-10-18 14:00:00', '2017-10-18 10:40:57', '2017-10-18 03:41:00'),
(368, 97, 'drg. Brian', 26, 'Gigi', '2017-10-18 20:32:00', '2017-10-18 20:32:00', '2017-10-18 20:31:56', '2017-10-18 13:31:59'),
(369, 167, 'Aidil', 26, 'Gigi', '2017-10-19 14:58:00', '2017-10-19 18:00:00', '2017-10-19 14:58:48', '2017-10-19 07:58:48'),
(370, 97, 'drg. Brian', 26, 'Gigi', '2017-10-20 21:42:00', '2017-10-20 21:42:00', '2017-10-20 21:42:08', '2017-10-20 14:42:08'),
(371, 656, 'Rizky Vira Novella', 26, 'Gigi', '2017-10-24 10:35:00', '2017-10-24 14:00:00', '2017-10-24 10:35:02', '2017-10-24 03:35:02'),
(372, 167, 'Aidil', 26, 'Gigi', '2017-10-24 14:00:00', '2017-10-24 18:00:00', '2017-10-24 14:54:20', '2017-10-24 07:54:20'),
(373, 656, 'Rizky Vira Novella', 26, 'Gigi', '2017-10-27 10:00:00', '2017-10-27 14:00:00', '2017-10-27 12:30:05', '2017-10-27 05:30:06'),
(374, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-10-30 12:00:00', '2017-10-30 17:00:00', '2017-10-30 12:24:32', '2017-10-30 05:24:34'),
(375, 167, 'Aidil', 26, 'Gigi', '2017-10-30 14:00:00', '2017-10-30 18:00:00', '2017-10-30 15:28:14', '2017-10-30 08:28:16'),
(376, 97, 'drg. Brian', 26, 'Gigi', '2017-10-30 20:44:00', '2017-10-30 20:44:00', '2017-10-30 20:44:25', '2017-10-30 13:44:28'),
(377, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-10-31 12:00:00', '2017-10-31 17:00:00', '2017-10-31 12:02:27', '2017-10-31 05:02:30'),
(378, 167, 'Aidil', 26, 'Gigi', '2017-10-31 14:00:00', '2017-10-31 18:00:00', '2017-10-31 15:59:39', '2017-10-31 08:59:41'),
(379, 167, 'Aidil', 26, 'Gigi', '2017-11-02 15:23:00', '2017-11-02 15:23:00', '2017-11-02 15:23:04', '2017-11-02 08:23:04');
INSERT INTO `sys_schedule_dokter` (`schedule_id`, `schedule_dokter_id`, `schedule_dokter_name`, `schedule_kind_service_id`, `schedule_kind_service_name`, `schedule_datetime_start`, `schedule_datetime_end`, `schedule_sync_datetime`, `schedule_timestamp`) VALUES
(380, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-04 12:40:00', '2017-11-04 14:40:00', '2017-11-04 12:40:54', '2017-11-04 05:40:55'),
(381, 656, 'Rizky Vira Novella', 26, 'Gigi', '2017-11-06 10:44:00', '2017-11-06 13:44:00', '2017-11-06 13:44:08', '2017-11-06 06:44:10'),
(382, 167, 'Aidil', 26, 'Gigi', '2017-11-06 14:00:00', '2017-11-06 18:00:00', '2017-11-06 15:00:02', '2017-11-06 07:59:53'),
(383, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-06 15:16:00', '2017-11-06 15:16:00', '2017-11-06 15:16:56', '2017-11-06 08:16:58'),
(384, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-07 14:50:00', '2017-11-07 14:50:00', '2017-11-07 14:50:59', '2017-11-07 07:51:01'),
(385, 167, 'Aidil', 26, 'Gigi', '2017-11-07 14:00:00', '2017-11-07 18:00:00', '2017-11-07 15:42:20', '2017-11-07 08:42:23'),
(386, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-08 13:01:00', '2017-11-08 13:01:00', '2017-11-08 13:01:11', '2017-11-08 06:01:14'),
(387, 167, 'Aidil', 26, 'Gigi', '2017-11-08 14:00:00', '2017-11-08 18:00:00', '2017-11-08 15:08:12', '2017-11-08 08:08:14'),
(388, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-09 13:16:00', '2017-11-09 13:16:00', '2017-11-09 13:16:23', '2017-11-09 06:16:23'),
(389, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-10 11:29:00', '2017-11-10 11:29:00', '2017-11-10 11:30:09', '2017-11-10 04:30:09'),
(390, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-13 10:47:00', '2017-11-13 10:47:00', '2017-11-13 10:47:55', '2017-11-13 03:47:56'),
(391, 656, 'Rizky Vira Novella', 26, 'Gigi', '2017-11-13 13:44:00', '2017-11-13 13:44:00', '2017-11-13 13:44:28', '2017-11-13 06:44:29'),
(392, 407, 'Ray C Barus', 12, 'Obgyn', '2017-11-13 16:05:00', '2017-11-13 18:05:00', '2017-11-13 16:05:15', '2017-11-13 09:05:16'),
(393, 167, 'Aidil', 26, 'Gigi', '2017-11-16 19:59:00', '2017-11-16 19:59:00', '2017-11-16 20:04:52', '2017-11-16 13:04:52'),
(394, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-18 13:02:00', '2017-11-18 13:02:00', '2017-11-18 13:02:40', '2017-11-18 06:02:40'),
(395, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-20 10:33:00', '2017-11-20 14:33:00', '2017-11-20 12:43:31', '2017-11-20 05:43:32'),
(396, 167, 'Aidil', 26, 'Gigi', '2017-11-20 14:00:00', '2017-11-20 18:00:00', '2017-11-20 14:53:06', '2017-11-20 07:53:06'),
(397, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-22 11:05:00', '2017-11-22 14:30:00', '2017-11-22 11:06:12', '2017-11-22 04:06:12'),
(398, 167, 'Aidil', 26, 'Gigi', '2017-11-23 14:00:00', '2017-11-23 18:00:00', '2017-11-23 14:44:07', '2017-11-23 07:44:08'),
(399, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-24 11:45:00', '2017-11-24 15:45:00', '2017-11-24 11:45:32', '2017-11-24 04:45:34'),
(400, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-25 12:26:00', '2017-11-25 13:26:00', '2017-11-25 12:26:19', '2017-11-25 05:26:20'),
(401, 36, 'drg. Aditya Rachmawati, Sp.Ort', 26, 'Gigi', '2017-11-25 17:39:00', '2017-11-25 17:39:00', '2017-11-25 17:38:57', '2017-11-25 10:38:58'),
(402, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-27 11:26:00', '2017-11-27 11:26:00', '2017-11-27 11:27:00', '2017-11-27 04:27:03'),
(403, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-28 11:16:00', '2017-11-28 11:16:00', '2017-11-28 11:16:42', '2017-11-28 04:16:45'),
(404, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-11-30 11:50:00', '2017-11-30 11:50:00', '2017-11-30 11:50:18', '2017-11-30 04:50:18'),
(405, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-04 11:25:00', '2017-12-04 17:25:00', '2017-12-18 11:06:17', '2017-12-18 04:06:18'),
(406, 167, 'Aidil', 26, 'Gigi', '2017-12-04 14:08:00', '2017-12-04 18:00:00', '2017-12-18 11:06:17', '2017-12-18 04:06:19'),
(407, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-05 11:59:00', '2017-12-05 16:00:00', '2017-12-18 11:06:18', '2017-12-18 04:06:20'),
(408, 814, 'Wira Agustina', 26, 'Gigi', '2017-12-06 10:37:00', '2017-12-06 10:37:00', '2017-12-18 11:06:19', '2017-12-18 04:06:20'),
(409, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-06 15:45:00', '2017-12-06 15:45:00', '2017-12-18 11:06:21', '2017-12-18 04:06:22'),
(410, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-07 13:01:00', '2017-12-07 13:31:00', '2017-12-18 11:06:21', '2017-12-18 04:06:23'),
(411, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-11 11:13:00', '2017-12-11 15:13:00', '2017-12-18 11:06:22', '2017-12-18 04:06:24'),
(412, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-12 11:05:00', '2017-12-12 14:05:00', '2017-12-18 11:06:33', '2017-12-18 04:06:34'),
(413, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-13 12:25:00', '2017-12-13 12:25:00', '2017-12-18 11:06:34', '2017-12-18 04:06:35'),
(414, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-14 15:01:00', '2017-12-14 15:01:00', '2017-12-18 11:06:34', '2017-12-18 04:06:37'),
(415, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-15 11:19:00', '2017-12-15 11:19:00', '2017-12-19 11:41:44', '2017-12-19 04:41:46'),
(416, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-18 11:06:00', '2017-12-18 16:06:00', '2017-12-19 11:41:45', '2017-12-19 04:41:47'),
(417, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-19 11:41:00', '2017-12-19 16:41:00', '2017-12-19 11:41:46', '2017-12-19 04:41:48'),
(418, 167, 'Aidil', 26, 'Gigi', '2017-12-20 14:00:00', '2017-12-20 18:00:00', '2017-12-20 16:08:47', '2017-12-20 09:08:50'),
(419, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-21 11:42:00', '2017-12-21 11:42:00', '2017-12-21 11:42:59', '2017-12-21 04:42:58'),
(420, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-22 19:04:00', '2017-12-22 19:04:00', '2017-12-22 19:04:32', '2017-12-22 12:04:32'),
(421, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-26 11:40:00', '2017-12-26 15:45:00', '2017-12-26 11:41:09', '2017-12-26 04:41:12'),
(422, 167, 'Aidil', 26, 'Gigi', '2017-12-27 17:26:00', '2017-12-27 17:26:00', '2017-12-27 17:26:27', '2017-12-27 10:26:29'),
(423, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-28 10:34:00', '2017-12-28 10:34:00', '2017-12-28 10:34:28', '2017-12-28 03:34:29'),
(424, 167, 'Aidil', 26, 'Gigi', '2017-12-29 18:48:00', '2017-12-29 18:48:00', '2017-12-29 18:48:34', '2017-12-29 11:48:34'),
(425, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2017-12-30 12:02:00', '2017-12-30 12:02:00', '2017-12-30 12:03:31', '2017-12-30 05:03:32'),
(426, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2018-01-02 11:56:00', '2018-01-02 11:56:00', '2018-01-02 11:57:03', '2018-01-02 04:57:05'),
(427, 167, 'Aidil', 26, 'Gigi', '2018-01-02 15:11:00', '2018-01-02 15:11:00', '2018-01-02 15:12:14', '2018-01-02 08:12:16'),
(428, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2018-01-03 12:38:00', '2018-01-03 12:38:00', '2018-01-03 12:38:44', '2018-01-03 05:38:46'),
(429, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2018-01-04 11:18:00', '2018-01-04 11:18:00', '2018-01-04 11:19:02', '2018-01-04 04:19:02'),
(430, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2018-01-05 11:04:00', '2018-01-05 11:04:00', '2018-01-05 11:08:46', '2018-01-05 04:08:47'),
(431, 167, 'Aidil', 26, 'Gigi', '2018-01-05 16:02:00', '2018-01-05 16:02:00', '2018-01-05 16:37:55', '2018-01-05 09:37:56'),
(432, 68, 'dr. Putri Wulandari', 11, 'Estetika', '2018-01-06 11:24:00', '2018-01-06 11:24:00', '2018-01-06 11:24:54', '2018-01-06 04:24:56');

-- --------------------------------------------------------

--
-- Table structure for table `sys_service`
--

CREATE TABLE `sys_service` (
  `service_id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(100) NOT NULL,
  `service_is_show` enum('Y','N') NOT NULL DEFAULT 'Y',
  `service_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_service`
--

INSERT INTO `sys_service` (`service_id`, `service_name`, `service_is_show`, `service_timestamp`) VALUES
(11, 'Klinik Cardio', 'Y', '2016-12-20 04:27:09'),
(12, 'Klinik THT', 'Y', '2016-12-20 04:27:16'),
(13, 'Klinik Estetika', 'Y', '2016-12-20 04:27:24'),
(14, 'Klinik Gigi', 'Y', '2016-12-20 04:27:30'),
(15, 'Klinik Obgyn', 'Y', '2016-12-20 04:27:39'),
(16, 'Klinik Saraf', 'Y', '2016-12-20 04:27:46'),
(17, 'Klinik Penyakit Dalam', 'Y', '2016-12-20 04:27:54'),
(18, 'Klinik Anak', 'Y', '2016-12-20 04:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `sys_sms`
--

CREATE TABLE `sys_sms` (
  `sms_id` int(10) UNSIGNED NOT NULL,
  `sms_mobilephone` varchar(20) NOT NULL,
  `sms_text` text NOT NULL,
  `sms_status` enum('not_sent','sent','failed') NOT NULL DEFAULT 'not_sent',
  `sms_try_to_send` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sms_type` enum('reguler','masking') NOT NULL DEFAULT 'reguler',
  `sms_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_store`
--

CREATE TABLE `sys_store` (
  `store_id` int(10) UNSIGNED NOT NULL,
  `store_type` enum('center','branch') NOT NULL DEFAULT 'branch',
  `store_name` varchar(200) NOT NULL,
  `store_address` varchar(255) DEFAULT NULL,
  `store_telephone_number` varchar(100) DEFAULT NULL,
  `store_is_active` char(1) NOT NULL DEFAULT 'Y',
  `store_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `store_admin_username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_struktur`
--

CREATE TABLE `sys_struktur` (
  `struktur_id` int(10) UNSIGNED NOT NULL,
  `struktur_par_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `struktur_nama_jabatan` varchar(100) NOT NULL,
  `struktur_nama_kepala` varchar(100) DEFAULT NULL,
  `struktur_pangkat` varchar(100) DEFAULT NULL,
  `struktur_nip` varchar(50) DEFAULT NULL,
  `struktur_profil_singkat` text,
  `struktur_urutan` int(2) NOT NULL DEFAULT '1',
  `struktur_facebook` varchar(100) DEFAULT NULL,
  `struktur_gplus` varchar(100) DEFAULT NULL,
  `struktur_twitter` varchar(100) DEFAULT NULL,
  `struktur_linkedin` varchar(100) DEFAULT NULL,
  `struktur_foto` varchar(100) DEFAULT NULL,
  `struktur_input_by` varchar(50) NOT NULL,
  `struktur_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_struktur`
--

INSERT INTO `sys_struktur` (`struktur_id`, `struktur_par_id`, `struktur_nama_jabatan`, `struktur_nama_kepala`, `struktur_pangkat`, `struktur_nip`, `struktur_profil_singkat`, `struktur_urutan`, `struktur_facebook`, `struktur_gplus`, `struktur_twitter`, `struktur_linkedin`, `struktur_foto`, `struktur_input_by`, `struktur_timestamp`) VALUES
(31, 0, 'Directur CV Care for Asia', 'Prof. Dr. Sutomo Kasiman, Sp.JP(K), FIHA, FACC, FAsCC', '-', '-', 'CV Care for Asia merupakan institusi yang mengelola bisnis kesehatan dengan nama Klinik Syifa.\nKlinik Syifa berkiprah dalam pelayanan klinik rawat jalan dengan beberapa pelayanan khusus/ spesialisasi yang mutakhir.\nPelayanan ini ditujukan pada masyarakat Indonesia dan asing pada umumnya dan Medan pada khususnya.', 1, '', '', '', '', 'assets/images/struktur/kasimans.jpg', 'admin', '2016-12-20 07:57:48'),
(32, 31, 'Dokter Spesialis THT', 'dr. Ashri Yudhistira, Sp.THT-KL', '-', '-', '', 1, '', '', '', '', '-', 'admin', '2016-12-20 07:58:03'),
(33, 32, 'Dokter Spesialis Cardio', 'dr. Yuke Sarastri, Sp.JP', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/yuke.jpg', 'admin', '2016-12-20 07:58:19'),
(34, 32, 'Dokter Spesialis Obgyn', 'dr. Yudha Sudewo, Sp.OG', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/IMG_1777_._.JPG', 'admin', '2016-12-20 07:58:34'),
(37, 32, 'Dokter Spesialis Saraf', 'dr. R.A Dwi Pujiastuti, Sp.S', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/pudji.jpg', 'admin', '2016-12-20 08:00:10'),
(38, 32, 'Dokter Spesialis Estetika', 'dr. Putri Wulandari', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/putri.jpg', 'admin', '2016-12-20 08:00:43'),
(39, 32, 'Dokter Spesialis Gigi', 'drg. Aditya Rachmawati, Sp.Ort', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/tya.JPG', 'admin', '2016-12-20 08:00:58'),
(40, 32, 'Dokter Spesialis Penyakit Dalam', 'dr. Dhini Silvana, Sp.PD', '-', '-', '', 1, '', '', '', '', NULL, 'admin', '2016-12-20 08:03:01'),
(41, 32, 'Dokter Spesialis Anak', 'dr. Inke Nadia Lubis, Sp.A', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/inke2.jpg', 'admin', '2016-12-20 08:03:37'),
(42, 33, 'Dokter Spesialis Cardio', 'dr. Joy Wulansari, Sp.JP', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/IMG-20170113-WA0001.jpg', 'admin', '2016-12-20 08:04:05'),
(43, 37, 'Dokter Spesialis Saraf', 'Prof. Dr. dr. Hasan Sjahrir, Sp.S(K)', '-', '-', '', 1, '', '', '', '', 'assets/images/struktur/image1.JPG', 'admin', '2016-12-20 08:04:46'),
(44, 39, 'Dokter Spesialis Gigi', 'drg. Brian Merchantara Winato', '-', '-', '', 1, '', '', '', '', NULL, 'admin', '2016-12-20 08:05:59'),
(45, 39, 'Dokter Spesialis Gigi', 'drg. Muhammad Aidil Nasution', '-', '-', '', 1, '', '', '', '', NULL, 'admin', '2017-01-13 02:59:54');

-- --------------------------------------------------------

--
-- Table structure for table `sys_verification_code`
--

CREATE TABLE `sys_verification_code` (
  `verification_code_id` int(10) UNSIGNED NOT NULL,
  `verification_code_email` varchar(255) NOT NULL,
  `verification_code_mobilephone` varchar(25) NOT NULL,
  `verification_code_value` char(5) NOT NULL,
  `verification_code_ref` varchar(100) NOT NULL,
  `verification_code_expired_datetime` datetime NOT NULL,
  `verification_code_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_token`
--
ALTER TABLE `api_token`
  ADD PRIMARY KEY (`token_id`);

--
-- Indexes for table `ecom_customer`
--
ALTER TABLE `ecom_customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `customer_person_id` (`customer_person_id`);

--
-- Indexes for table `ecom_medical_detail`
--
ALTER TABLE `ecom_medical_detail`
  ADD PRIMARY KEY (`medical_detail_id`),
  ADD KEY `medical_detail_medical_header_id` (`medical_detail_medical_header_id`,`medical_detail_medical_master_person_id`);

--
-- Indexes for table `ecom_medical_header`
--
ALTER TABLE `ecom_medical_header`
  ADD PRIMARY KEY (`medical_header_id`),
  ADD KEY `medical_header_person_id` (`medical_header_person_id`,`medical_header_position_id`);

--
-- Indexes for table `ecom_payment`
--
ALTER TABLE `ecom_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `payment_termin_id` (`payment_termin_id`);

--
-- Indexes for table `ecom_transaction`
--
ALTER TABLE `ecom_transaction`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `transaction_medical_header_id` (`transaction_medical_header_id`);

--
-- Indexes for table `ecom_transaction_detail`
--
ALTER TABLE `ecom_transaction_detail`
  ADD PRIMARY KEY (`transaction_detail_id`);

--
-- Indexes for table `ecom_transaction_perawat`
--
ALTER TABLE `ecom_transaction_perawat`
  ADD PRIMARY KEY (`transaction_perawat_id`);

--
-- Indexes for table `ecom_transaction_status`
--
ALTER TABLE `ecom_transaction_status`
  ADD PRIMARY KEY (`transaction_status_id`);

--
-- Indexes for table `ecom_transaction_termin`
--
ALTER TABLE `ecom_transaction_termin`
  ADD PRIMARY KEY (`termin_id`),
  ADD KEY `termin_transaction_id` (`termin_transaction_id`);

--
-- Indexes for table `form_run`
--
ALTER TABLE `form_run`
  ADD PRIMARY KEY (`run_id`);

--
-- Indexes for table `master_activation`
--
ALTER TABLE `master_activation`
  ADD KEY `activation_id` (`activation_id`,`activation_company_id`);

--
-- Indexes for table `master_position`
--
ALTER TABLE `master_position`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `master_printer`
--
ALTER TABLE `master_printer`
  ADD PRIMARY KEY (`printer_id`);

--
-- Indexes for table `master_shift`
--
ALTER TABLE `master_shift`
  ADD PRIMARY KEY (`shift_id`);

--
-- Indexes for table `notification_list`
--
ALTER TABLE `notification_list`
  ADD PRIMARY KEY (`notification_list_id`);

--
-- Indexes for table `ref_area`
--
ALTER TABLE `ref_area`
  ADD PRIMARY KEY (`area_id`),
  ADD KEY `area_longitude` (`area_longitude`),
  ADD KEY `area_country_id` (`area_country_id`);

--
-- Indexes for table `ref_bank`
--
ALTER TABLE `ref_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `ref_country`
--
ALTER TABLE `ref_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `site_administrator`
--
ALTER TABLE `site_administrator`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `admin_group_id` (`admin_group_id`),
  ADD KEY `admin_person_id` (`admin_person_id`);

--
-- Indexes for table `site_administrator_group`
--
ALTER TABLE `site_administrator_group`
  ADD PRIMARY KEY (`admin_group_id`);

--
-- Indexes for table `site_administrator_menu`
--
ALTER TABLE `site_administrator_menu`
  ADD PRIMARY KEY (`menu_administrator_id`);

--
-- Indexes for table `site_administrator_privilege`
--
ALTER TABLE `site_administrator_privilege`
  ADD PRIMARY KEY (`administrator_privilege_id`),
  ADD KEY `administrator_privilege_administrator_group_id` (`administrator_privilege_administrator_group_id`,`administrator_privilege_administrator_menu_id`);

--
-- Indexes for table `site_administrator_privilege_label`
--
ALTER TABLE `site_administrator_privilege_label`
  ADD PRIMARY KEY (`privilege_label_id`);

--
-- Indexes for table `site_administrator_store`
--
ALTER TABLE `site_administrator_store`
  ADD PRIMARY KEY (`administrator_store_id`);

--
-- Indexes for table `site_ask`
--
ALTER TABLE `site_ask`
  ADD PRIMARY KEY (`ask_id`),
  ADD KEY `ask_par_id` (`ask_par_id`);

--
-- Indexes for table `site_bank_account`
--
ALTER TABLE `site_bank_account`
  ADD PRIMARY KEY (`bank_account_id`);

--
-- Indexes for table `site_chat_ym`
--
ALTER TABLE `site_chat_ym`
  ADD PRIMARY KEY (`chat_ym_id`);

--
-- Indexes for table `site_configuration`
--
ALTER TABLE `site_configuration`
  ADD UNIQUE KEY `configuration_index` (`configuration_index`);

--
-- Indexes for table `site_download`
--
ALTER TABLE `site_download`
  ADD PRIMARY KEY (`download_id`),
  ADD KEY `download_news_id` (`download_foreign_id`),
  ADD KEY `download_news_id_2` (`download_news_id`);

--
-- Indexes for table `site_job`
--
ALTER TABLE `site_job`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `site_job_applicant`
--
ALTER TABLE `site_job_applicant`
  ADD PRIMARY KEY (`job_applicant_id`),
  ADD KEY `job_aplicant_job_id` (`job_applicant_job_id`),
  ADD KEY `job_aplicant_person_id` (`job_applicant_person_id`);

--
-- Indexes for table `site_job_applicant_reply`
--
ALTER TABLE `site_job_applicant_reply`
  ADD PRIMARY KEY (`job_applicant_reply_id`),
  ADD KEY `job_applicant_reply_job_applicant_id` (`job_applicant_reply_job_applicant_id`);

--
-- Indexes for table `site_job_counter`
--
ALTER TABLE `site_job_counter`
  ADD PRIMARY KEY (`job_counter_id`),
  ADD KEY `job_counter_job_id` (`job_counter_job_id`);

--
-- Indexes for table `site_news`
--
ALTER TABLE `site_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `site_news_category`
--
ALTER TABLE `site_news_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `site_news_counter`
--
ALTER TABLE `site_news_counter`
  ADD PRIMARY KEY (`news_counter_id`),
  ADD KEY `news_counter_news_id` (`news_counter_news_id`);

--
-- Indexes for table `site_news_more_images`
--
ALTER TABLE `site_news_more_images`
  ADD PRIMARY KEY (`news_more_images_id`),
  ADD KEY `news_more_images_news_id` (`news_more_images_news_id`);

--
-- Indexes for table `site_news_relation`
--
ALTER TABLE `site_news_relation`
  ADD KEY `news_category_id` (`news_category_id`,`news_id`);

--
-- Indexes for table `site_news_tab`
--
ALTER TABLE `site_news_tab`
  ADD PRIMARY KEY (`tab_id`),
  ADD KEY `tab_news_id` (`tab_news_id`);

--
-- Indexes for table `site_news_ticker`
--
ALTER TABLE `site_news_ticker`
  ADD PRIMARY KEY (`news_ticker_id`);

--
-- Indexes for table `site_slider`
--
ALTER TABLE `site_slider`
  ADD PRIMARY KEY (`slider_id`),
  ADD KEY `slider_slider_category_id` (`slider_slider_category_id`);

--
-- Indexes for table `site_slider_category`
--
ALTER TABLE `site_slider_category`
  ADD PRIMARY KEY (`slider_category_id`);

--
-- Indexes for table `site_social_media`
--
ALTER TABLE `site_social_media`
  ADD PRIMARY KEY (`social_media_id`);

--
-- Indexes for table `site_testimonial`
--
ALTER TABLE `site_testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `site_youtube`
--
ALTER TABLE `site_youtube`
  ADD PRIMARY KEY (`youtube_pk_id`);

--
-- Indexes for table `sys_form`
--
ALTER TABLE `sys_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `sys_member`
--
ALTER TABLE `sys_member`
  ADD KEY `member_person_id` (`member_person_id`);

--
-- Indexes for table `sys_order`
--
ALTER TABLE `sys_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `sys_polling_answer`
--
ALTER TABLE `sys_polling_answer`
  ADD PRIMARY KEY (`polling_answer_id`),
  ADD KEY `polling_answer_polling_question_id` (`polling_answer_polling_question_id`);

--
-- Indexes for table `sys_polling_category`
--
ALTER TABLE `sys_polling_category`
  ADD PRIMARY KEY (`polling_category_id`);

--
-- Indexes for table `sys_polling_question`
--
ALTER TABLE `sys_polling_question`
  ADD PRIMARY KEY (`polling_question_id`),
  ADD KEY `polling_question_polling_category_id` (`polling_question_polling_category_id`);

--
-- Indexes for table `sys_polling_statistik_detail`
--
ALTER TABLE `sys_polling_statistik_detail`
  ADD KEY `spsd_spsh_id` (`spsd_spsh_id`,`spsd_polling_answer_id`),
  ADD KEY `spsd_polling_question_id` (`spsd_polling_question_id`);

--
-- Indexes for table `sys_polling_statistik_header`
--
ALTER TABLE `sys_polling_statistik_header`
  ADD PRIMARY KEY (`spsh_id`),
  ADD KEY `spsh_people_id` (`spsh_person_id`),
  ADD KEY `spsh_polling_category_id` (`spsh_polling_category_id`);

--
-- Indexes for table `sys_product`
--
ALTER TABLE `sys_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `sys_product_more_images`
--
ALTER TABLE `sys_product_more_images`
  ADD PRIMARY KEY (`product_more_images_id`),
  ADD KEY `product_more_images_product_id` (`product_more_images_product_id`);

--
-- Indexes for table `sys_product_tags`
--
ALTER TABLE `sys_product_tags`
  ADD PRIMARY KEY (`product_tags_id`);

--
-- Indexes for table `sys_product_tags_detail`
--
ALTER TABLE `sys_product_tags_detail`
  ADD PRIMARY KEY (`tags_detail_id`),
  ADD KEY `tags_detail_tags_id` (`tags_detail_tags_id`,`tags_detail_product_id`),
  ADD KEY `tags_detail_product_id` (`tags_detail_product_id`);

--
-- Indexes for table `sys_request_time`
--
ALTER TABLE `sys_request_time`
  ADD PRIMARY KEY (`request_time_id`);

--
-- Indexes for table `sys_schedule`
--
ALTER TABLE `sys_schedule`
  ADD PRIMARY KEY (`schedule_id`),
  ADD KEY `schedule_service_id` (`schedule_service_id`);

--
-- Indexes for table `sys_schedule_dokter`
--
ALTER TABLE `sys_schedule_dokter`
  ADD PRIMARY KEY (`schedule_id`),
  ADD KEY `schedule_dokter_id` (`schedule_dokter_id`);

--
-- Indexes for table `sys_service`
--
ALTER TABLE `sys_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `sys_sms`
--
ALTER TABLE `sys_sms`
  ADD PRIMARY KEY (`sms_id`);

--
-- Indexes for table `sys_store`
--
ALTER TABLE `sys_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `sys_struktur`
--
ALTER TABLE `sys_struktur`
  ADD PRIMARY KEY (`struktur_id`);

--
-- Indexes for table `sys_verification_code`
--
ALTER TABLE `sys_verification_code`
  ADD PRIMARY KEY (`verification_code_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_token`
--
ALTER TABLE `api_token`
  MODIFY `token_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_customer`
--
ALTER TABLE `ecom_customer`
  MODIFY `customer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_medical_detail`
--
ALTER TABLE `ecom_medical_detail`
  MODIFY `medical_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_medical_header`
--
ALTER TABLE `ecom_medical_header`
  MODIFY `medical_header_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_payment`
--
ALTER TABLE `ecom_payment`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_transaction`
--
ALTER TABLE `ecom_transaction`
  MODIFY `transaction_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_transaction_detail`
--
ALTER TABLE `ecom_transaction_detail`
  MODIFY `transaction_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_transaction_perawat`
--
ALTER TABLE `ecom_transaction_perawat`
  MODIFY `transaction_perawat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_transaction_status`
--
ALTER TABLE `ecom_transaction_status`
  MODIFY `transaction_status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ecom_transaction_termin`
--
ALTER TABLE `ecom_transaction_termin`
  MODIFY `termin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `form_run`
--
ALTER TABLE `form_run`
  MODIFY `run_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_position`
--
ALTER TABLE `master_position`
  MODIFY `position_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_printer`
--
ALTER TABLE `master_printer`
  MODIFY `printer_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_shift`
--
ALTER TABLE `master_shift`
  MODIFY `shift_id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_list`
--
ALTER TABLE `notification_list`
  MODIFY `notification_list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ref_area`
--
ALTER TABLE `ref_area`
  MODIFY `area_id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ref_bank`
--
ALTER TABLE `ref_bank`
  MODIFY `bank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ref_country`
--
ALTER TABLE `ref_country`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_administrator`
--
ALTER TABLE `site_administrator`
  MODIFY `admin_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `site_administrator_group`
--
ALTER TABLE `site_administrator_group`
  MODIFY `admin_group_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `site_administrator_menu`
--
ALTER TABLE `site_administrator_menu`
  MODIFY `menu_administrator_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT for table `site_administrator_privilege`
--
ALTER TABLE `site_administrator_privilege`
  MODIFY `administrator_privilege_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4071;
--
-- AUTO_INCREMENT for table `site_administrator_privilege_label`
--
ALTER TABLE `site_administrator_privilege_label`
  MODIFY `privilege_label_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300;
--
-- AUTO_INCREMENT for table `site_administrator_store`
--
ALTER TABLE `site_administrator_store`
  MODIFY `administrator_store_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `site_ask`
--
ALTER TABLE `site_ask`
  MODIFY `ask_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `site_bank_account`
--
ALTER TABLE `site_bank_account`
  MODIFY `bank_account_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_chat_ym`
--
ALTER TABLE `site_chat_ym`
  MODIFY `chat_ym_id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_download`
--
ALTER TABLE `site_download`
  MODIFY `download_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `site_job`
--
ALTER TABLE `site_job`
  MODIFY `job_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_job_applicant`
--
ALTER TABLE `site_job_applicant`
  MODIFY `job_applicant_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_job_applicant_reply`
--
ALTER TABLE `site_job_applicant_reply`
  MODIFY `job_applicant_reply_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_job_counter`
--
ALTER TABLE `site_job_counter`
  MODIFY `job_counter_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_news`
--
ALTER TABLE `site_news`
  MODIFY `news_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;
--
-- AUTO_INCREMENT for table `site_news_category`
--
ALTER TABLE `site_news_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `site_news_counter`
--
ALTER TABLE `site_news_counter`
  MODIFY `news_counter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `site_news_more_images`
--
ALTER TABLE `site_news_more_images`
  MODIFY `news_more_images_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `site_news_tab`
--
ALTER TABLE `site_news_tab`
  MODIFY `tab_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `site_news_ticker`
--
ALTER TABLE `site_news_ticker`
  MODIFY `news_ticker_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_slider`
--
ALTER TABLE `site_slider`
  MODIFY `slider_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `site_slider_category`
--
ALTER TABLE `site_slider_category`
  MODIFY `slider_category_id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_social_media`
--
ALTER TABLE `site_social_media`
  MODIFY `social_media_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `site_testimonial`
--
ALTER TABLE `site_testimonial`
  MODIFY `testimonial_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site_youtube`
--
ALTER TABLE `site_youtube`
  MODIFY `youtube_pk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sys_form`
--
ALTER TABLE `sys_form`
  MODIFY `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_order`
--
ALTER TABLE `sys_order`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sys_polling_answer`
--
ALTER TABLE `sys_polling_answer`
  MODIFY `polling_answer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_polling_category`
--
ALTER TABLE `sys_polling_category`
  MODIFY `polling_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_polling_question`
--
ALTER TABLE `sys_polling_question`
  MODIFY `polling_question_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_polling_statistik_header`
--
ALTER TABLE `sys_polling_statistik_header`
  MODIFY `spsh_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_product`
--
ALTER TABLE `sys_product`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sys_product_more_images`
--
ALTER TABLE `sys_product_more_images`
  MODIFY `product_more_images_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_product_tags`
--
ALTER TABLE `sys_product_tags`
  MODIFY `product_tags_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_product_tags_detail`
--
ALTER TABLE `sys_product_tags_detail`
  MODIFY `tags_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_request_time`
--
ALTER TABLE `sys_request_time`
  MODIFY `request_time_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sys_schedule`
--
ALTER TABLE `sys_schedule`
  MODIFY `schedule_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1823;
--
-- AUTO_INCREMENT for table `sys_schedule_dokter`
--
ALTER TABLE `sys_schedule_dokter`
  MODIFY `schedule_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433;
--
-- AUTO_INCREMENT for table `sys_service`
--
ALTER TABLE `sys_service`
  MODIFY `service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `sys_sms`
--
ALTER TABLE `sys_sms`
  MODIFY `sms_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_store`
--
ALTER TABLE `sys_store`
  MODIFY `store_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_struktur`
--
ALTER TABLE `sys_struktur`
  MODIFY `struktur_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `sys_verification_code`
--
ALTER TABLE `sys_verification_code`
  MODIFY `verification_code_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `sys_product_more_images`
--
ALTER TABLE `sys_product_more_images`
  ADD CONSTRAINT `sys_product_more_images_ibfk_1` FOREIGN KEY (`product_more_images_product_id`) REFERENCES `sys_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
